﻿#include <iostream>
#include <cstdio>
#include <cuda_runtime.h>
#define SUPPORT_UNICODE_LIB 0
#include "../99_AuxStuff/MyLibs/Utility/Util.hpp"
#include <gl/GL.h>
#include <cmath>
#include <vector>

const double PI = 4 * std::atan(1);

constexpr UINT WIN_WIDTH = 800;
constexpr UINT WIN_HEIGHT = 600;
constexpr UINT WIN_X = 100;
constexpr UINT WIN_Y = 100;

constexpr int MAX_EXTENT = 1;
constexpr int MIN_EXTENT = -1;

class Wave {
    const double m_delta = 0.1, m_max_extentX = MAX_EXTENT, m_min_extentX = MIN_EXTENT, m_max_extentY = MAX_EXTENT, m_min_extentY = MIN_EXTENT;
    double m_max_extentY_current = m_max_extentY, m_min_extentY_current = m_min_extentY;
    int m_sign = 1;
    double angleToX(double angle)
    {
        return (angle*m_max_extentX + (2 * PI - angle)*m_min_extentX) / (2 * PI);
    }
    double valToY(double val)
    {
        return ((val + 1)*m_max_extentY_current - (val - 1)*m_min_extentY_current) / 2;
    }
public:
    Wave() = default;
    Wave(double initial_extent)
    {
        m_max_extentY_current = initial_extent;
        m_min_extentY_current = -1*initial_extent;
    }

    void advanceTime()
    {
        m_max_extentY_current += (m_sign*m_delta);
        m_min_extentY_current -= (m_sign*m_delta);
    }

    void plot()
    {
        if (m_max_extentY_current >= m_max_extentY)
        {
            m_sign = -1;
        }
        
        if(m_max_extentY_current <= m_min_extentY)
        {
            m_sign = 1;
        }

        glPointSize(3.0);
        //glBegin(GL_POINTS);
        for (double angle = 0; angle < 2* PI; angle += m_delta)
        {
            auto val = std::sin(angle);
            //angle to X coord
            auto X = angleToX(angle);
            //val to Y coord
            auto Y = valToY(val);
            glVertex2d(X, Y);
        }
        //glEnd();
    }
};

std::vector<Wave> m_waves;

void createWaves(int numWaves)
{
    double extentDelta = 1.0 / numWaves;
    double extent = 0;
    for (int i = 0; i < numWaves; i++)
    {
        extent += extentDelta;
        m_waves.push_back(Wave(extent));
    }
}

class MyOglWindow : public util::OglWindow
{
    // Inherited via OglWindow
    const char * GetMessageMap() override
    {
        return nullptr;
    }

    void Display() override
    {


        glClear(GL_COLOR_BUFFER_BIT);
        glBegin(GL_POINTS);
        for(auto& wave : m_waves)
        {
            wave.advanceTime();
            wave.plot();
        }
        glEnd();
        SwapBuffers(m_Hdc);
    }

    LRESULT WndProcPost(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
    {
        return LRESULT(0);
    }

    LRESULT WndProcPre(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
    {
        return LRESULT();
    }

    void Resize(int width, int height) override
    {
        if (height == 0)
            height = 1;

        glViewport(0, 0, width, height);

        // glMatrixMode(GL_PROJECTION);
        // glLoadIdentity();
        //
        // gluPerspective(45, ((GLfloat)width) / height, 0.0, 100.0f);
    }
public:
    MyOglWindow(TCHAR * className, UINT x, UINT y, UINT width, UINT height, HINSTANCE hInstance) : OglWindow(className, x, y, width, height, hInstance)
    {
    }

    // Inherited via OglWindow
    virtual void Update() override
    {
    }
};

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
    TCHAR AppName[] = TEXT("Line_RectangleTriangle");
    //MessageBoxA(NULL, " t - triangle \n r - rectangle \n b - blue \n w - white \n Up - zoom out \n Down - zoom in", "Info", MB_OK);
    MyOglWindow myOglWnd(AppName, WIN_X, WIN_Y, WIN_WIDTH, WIN_HEIGHT, hInstance);
    createWaves(50000);
    myOglWnd.InitAndShowWindow();
    myOglWnd.RunGameLoop();
    return 0;
}