#include <stdio.h>

int main()
{
    printf("\n");
    printf("Hello world ! \n\n\n");
    int a  = 13;
    printf("Integer Decimal value of 'A' = %d\n", a);
    printf("Integer Octal value of 'A' = %o\n", a);
    printf("Integer Hexadecimal value of 'A' (lower case) = %x\n", a);
    printf("Integer Hexadecimal value of 'A' (upper case) = %X\n", a);
    
    char ch = 'P';
    printf("character ch = %c\n", ch);
    
    char str[] = "string sample";
    printf("string str = %s\n\n", str);
    
    long num = 30121995L;
    printf("Long integer = %ld\n", num);
    
    unsigned int b = 7;
    printf("Unsigned integer b = %u\n\n", b);
    
    float f_num = 3012.1995f;
    printf("Floating point number with just %%f 'f_num' = %f\n\n", f_num);
    printf("Floating point number with  %%4.2f 'f_num' = %4.2f\n\n", f_num);
    printf("Floating point number with  %%2.5f 'f_num' = %2.5f\n\n", f_num);
    
    double d_pi = 3.14159265358979323846;
    printf("Double floating point number without exponential %g\n", d_pi);
    printf("Double floating point number with exponential (lower case) %e\n", d_pi);
    printf("Double floating point number with exponential (upper case) %E\n", d_pi);
    printf("Double floating point number hex value (lower case) %a\n", d_pi);
    printf("Double floating point number hex value (upper case) %A\n", d_pi);
    
    return 0;
}