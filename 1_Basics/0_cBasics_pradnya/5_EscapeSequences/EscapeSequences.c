#include <stdio.h>

int main(void)
{
    printf("\n\n");
    printf("Going to the next line using \\n escape sequence\n\n");
    printf("Demonstrating \t horizontal \t tab \t using \t \\t escape sequence\n\n");
    printf("\"This is a double qouted output\" created using \\\" \\\" escape sequences\n\n");
    printf("\'This is a double qouted output\' created using \\\' \\\' escape sequences\n\n");
    printf("BACKSPACE turned to BACKSPACE\b using \\b escape sequence\n\n");
    
    printf("\r Demonstrating carriage return using \\r escape sequence\n\n");
    printf(" Demonstrating \r carriage return using \\r escape sequence\n\n");
    printf(" Demonstrating carriage \r return using \\r escape sequence\n\n");
    
    printf("Demonstrating \x41 Using \\xhh escape sequence\n\n");
    printf("Demonstrating \102 Using \\xhh escape sequence\n\n");
    
    return 0;
}