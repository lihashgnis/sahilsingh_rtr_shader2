#include <stdio.h>

#define MY_PI 3.1415926535897932

//unnamed enums
enum 
{
    SUNDAY,
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY,
};

enum {
    JANUARY = 1,
    FEBRUARY,
    MARCH,
    APRIL,
    MAY,
    JUNE,
    JULY,
    AUGUST,
    SEPTEMBER,
    NOVEMBER,
    DECEMBER
};


//named enums
enum Numbers
{
    ONE,
    TWO,
    THREE,
    FOUR,
    FIVE =5,
    SIX,
    SEVEN,
    EIGHT,
    NINE,
    TEN
};

enum boolean
{
    TRUE = 1,
    FALSE = 0
};

int main(void)
{
    const double epsilon = 00000.1;
    
    printf("\n\n");
    printf("local constant epsilon =%f\n\n", epsilon);
    
    printf("Sunday is day number = %d\n", SUNDAY);
    printf("Monday is day number = %d\n", MONDAY);
    printf("Tuesday is day number = %d\n", TUESDAY);
    printf("Wednesday is day number = %d\n", WEDNESDAY);
    printf("Thursday is day number = %d\n", THURSDAY);
    printf("Friday is day number = %d\n", FRIDAY);
    printf("Saturday is day number = %d\n", SATURDAY);
    
    printf("\n\n");
    
    printf("One is enum number = %d\n", ONE);
    printf("Two is enum number = %d\n", TWO);
    printf("Three is enum number = %d\n", THREE);
    printf("Four is enum number = %d\n", FOUR);
    printf("Five is enum number = %d\n", FIVE);
    printf("Six is enum number = %d\n", SIX);
    printf("Seven is enum number = %d\n", SEVEN);
    printf("Eight is enum number = %d\n", EIGHT);
    printf("Nine is enum number = %d\n", NINE);
    printf("Ten is enum number = %d\n", TEN);
    
    printf("\n\n");
    
    printf("January is month number = %d\n", JANUARY);
    printf("February is month number = %d\n", FEBRUARY);
    printf("March is month number = %d\n", MARCH);
    printf("April is month number = %d\n", APRIL);
    printf("May is month number = %d\n", MAY);
    printf("June is month number = %d\n", JUNE);
    printf("July is month number = %d\n", JULY);
    printf("August is month number = %d\n", AUGUST);
    printf("September is month number = %d\n", SEPTEMBER);
    printf("November is month number = %d\n", NOVEMBER);
    printf("December is month number = %d\n", DECEMBER);
    
    printf("\n\n");
    
    printf("Value of TRUE is = %d\n", TRUE);
    printf("Value of FALSE is = %d\n", FALSE);
    
    printf("\n\n");
    
    printf("MY_PI macro value = %.101f\n", MY_PI);
    printf("Area of circle with radius 2 units = %f\n", (MY_PI * 2.0f * 2.0f));
    
    return 0;
}