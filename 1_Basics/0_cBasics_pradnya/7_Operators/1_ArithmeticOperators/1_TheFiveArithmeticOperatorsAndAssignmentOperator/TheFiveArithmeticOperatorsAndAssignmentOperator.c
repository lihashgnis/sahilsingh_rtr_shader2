#include <stdio.h>

int main(void)
{
    int a;
    int b;
    int result;
    
    
    printf("\n\n");
    printf("enter a number (A) : ");
    scanf("%d", &a);
    
    printf("\n\n");
    printf("enter another number (B) : ");
    scanf("%d", &b);
    
    printf("\n\n");
    
    result = a + b;
    printf(" Addition of A = %d, and B = %d is = %d\n", a, b, result);
    
    result = a - b;
    printf(" Subtraction of A = %d, and B = %d is = %d\n", a, b, result);
    
    result = a * b;
    printf(" Multiplication of A = %d, and B = %d is = %d\n", a, b, result);
    
    result = a / b;
    printf(" Division of A = %d, and B = %d is = %d\n", a, b, result);
    
    result = a % b;
    printf(" Division of A = %d, and B = %d gives remainder = %d\n", a, b, result);
    
    printf("\n\n");
    return 0;
}