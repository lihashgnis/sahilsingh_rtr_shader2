#include <stdio.h>

int main(void)
{
    int a, b, result;
    
    printf("\n\n");
    printf("Enter 1st integer : ");
    scanf("%d", &a);
    
    printf("Enter 2nd integer : ");
    scanf("%d", &b);
    
    printf("\n\n");
    printf("1 indicates TRUE\n 0 indicated FALSE\n\n");
    
    result = a < b;
    printf("(A < B) A = %d is less than B = %d \t Answer = %d\n", a, b, result);
    
    result = a > b;
    printf("(A > B) A = %d is greater than B = %d \t Answer = %d\n", a, b, result);
    
    result = a <= b;
    printf("(A <= B) A = %d is less than or equal to B = %d \t Answer = %d\n", a, b, result);
    
    result = a >= b;
    printf("(A >= B) A = %d is greater than or equal to B = %d \t Answer = %d\n", a, b, result);
    
    result = a == b;
    printf("(A == B) A = %d is equal to B = %d \t Answer = %d\n", a, b, result);
    
    result = a != b;
    printf("(A != B) A = %d is not equal to B = %d \t Answer = %d\n", a, b, result);
    
    return 0;
}