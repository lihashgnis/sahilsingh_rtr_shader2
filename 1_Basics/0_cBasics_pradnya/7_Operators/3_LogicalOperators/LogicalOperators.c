#include <stdio.h>

int main(void)
{
    int a, b, c, result;
    
    printf("\n\n");
    printf("Enter first integer\n");
    scanf("%d", &a);
    
    printf("Enter second integer\n");
    scanf("%d", &b);
    
    printf("Enter third integer\n");
    scanf("%d", &c);
    
    printf("\n\n");
    
    printf("0 indicates FALSE\n1 indicates TRUE\n\n");
    
    result = (a <= b) && (b != c);
    printf("(a <= b) && (b != c) answer = %d\n", result);
    
    result = (b <= a) || (a == c);
    printf("(b <= a) || (a == c) answer = %d\n", result);
    
    result = !a;
    printf("!a answer = %d\n", result);
    
    result = !b;
    printf("!b answer = %d\n", result);
    
    result = !c;
    printf("!c answer = %d\n", result);
    
    result = (!(a <= b) && !(b != c));
    printf("(!(a <= b) && !(b != c)) answer = %d\n", result);
    
    result = !((b >= a) || (a == c));
    printf("!((b >= a) || (a == c)) answer = %d\n", result);
    
    return 0;
}