#include <stdio.h>

int main(void)
{
    void PrintBinaryFormOfumber();
    
    unsigned int a, b, result;
    
    printf("\n\n");
    printf("Enter an integer : \n");
    scanf("%u", &a);
    printf("Enter another integer : \n");
    scanf("%u", &b);
    
    printf("\n\n");
    
    result = ~a;
    printf("(~a) where A decimal %u, octal %o, hex %x \n", a, a, a);
    printf("gives answer decimal %u, octal %o, hex %x \n", result, result, result);
    PrintBinaryFormOfumber(a);
    PrintBinaryFormOfumber(result);
    
    result = ~b;
    printf("(~b) where B decimal %u, octal %o, hex %x \n", b, b, b);
    printf("gives answer decimal %u, octal %o, hex %x \n", result, result, result);
    PrintBinaryFormOfumber(b);
    PrintBinaryFormOfumber(result);
    
    return 0;
}

void PrintBinaryFormOfumber(unsigned int decimal_number)
{
    unsigned int quotient, remainder;
    unsigned int num;
    unsigned int binary_array[8];
    int i;
    
    for(i = 0; i < 8; i++)
    {
        binary_array[i] = 0;
    }
    
    printf("the binary form of %u is ", decimal_number);
    
    num = decimal_number;
    i = 7;
    while(num != 0)
    {
        quotient = num / 2;
        remainder = num % 2;
        binary_array[i] = remainder;
        num = quotient;
        i--;
    }
    
    for(i = 0; i < 8 ; i++)
    {
        printf("%u", binary_array[i]);
    }
    
    printf("\n\n");
}