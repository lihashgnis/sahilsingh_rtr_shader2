#include <stdio.h>

int main(void)
{
    void PrintBinaryFormOfumber();
    
    unsigned int a, b, result, right_A, right_B;
    
    printf("\n\n");
    printf("Enter an integer : \n");
    scanf("%u", &a);
    printf("Enter another integer : \n");
    scanf("%u", &b);
    
    printf("\n\n");
    
    printf("By how many bits do you want to right shift A = %u? ", a);
    scanf("%u", &right_A);
    result = a >> right_A;
    printf("Right shift A = %u by %u bits gives %u\n", a, right_A, result);
    PrintBinaryFormOfumber(a);
    PrintBinaryFormOfumber(result);
    
    printf("By how many bits do you want to right shift B = %u? ", b);
    scanf("%u", &right_B);
    result = b >> right_B;
    printf("Right shift B = %u by %u bits gives %u\n ", b, right_B, result);
    PrintBinaryFormOfumber(b);
    PrintBinaryFormOfumber(result);
    
    return 0;
}

void PrintBinaryFormOfumber(unsigned int decimal_number)
{
    unsigned int quotient, remainder;
    unsigned int num;
    unsigned int binary_array[8];
    int i;
    
    for(i = 0; i < 8; i++)
    {
        binary_array[i] = 0;
    }
    
    printf("the binary form of %u is ", decimal_number);
    
    num = decimal_number;
    i = 7;
    while(num != 0)
    {
        quotient = num / 2;
        remainder = num % 2;
        binary_array[i] = remainder;
        num = quotient;
        i--;
    }
    
    for(i = 0; i < 8 ; i++)
    {
        printf("%u", binary_array[i]);
    }
    
    printf("\n\n");
}