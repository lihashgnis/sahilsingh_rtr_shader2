#include <stdio.h>

int main(void)
{
    void PrintBinaryFormOfumber();
    
    unsigned int a, b, result, left_A, left_B;
    
    printf("\n\n");
    printf("Enter an integer : \n");
    scanf("%u", &a);
    printf("Enter another integer : \n");
    scanf("%u", &b);
    
    printf("\n\n");
    
    printf("By how many bits do you want to left shift A = %u? ", a);
    scanf("%u", &left_A);
    result = a << left_A;
    printf("left shift A = %u by %u bits gives %u\n", a, left_A, result);
    PrintBinaryFormOfumber(a);
    PrintBinaryFormOfumber(result);
    
    printf("By how many bits do you want to left shift B = %u? ", b);
    scanf("%u", &left_B);
    result = b << left_B;
    printf("left shift B = %u by %u bits gives %u\n ", b, left_B, result);
    PrintBinaryFormOfumber(b);
    PrintBinaryFormOfumber(result);
    
    return 0;
}

void PrintBinaryFormOfumber(unsigned int decimal_number)
{
    unsigned int quotient, remainder;
    unsigned int num;
    unsigned int binary_array[8];
    int i;
    
    for(i = 0; i < 8; i++)
    {
        binary_array[i] = 0;
    }
    
    printf("the binary form of %u is ", decimal_number);
    
    num = decimal_number;
    i = 7;
    while(num != 0)
    {
        quotient = num / 2;
        remainder = num % 2;
        binary_array[i] = remainder;
        num = quotient;
        i--;
    }
    
    for(i = 0; i < 8 ; i++)
    {
        printf("%u", binary_array[i]);
    }
    
    printf("\n\n");
}