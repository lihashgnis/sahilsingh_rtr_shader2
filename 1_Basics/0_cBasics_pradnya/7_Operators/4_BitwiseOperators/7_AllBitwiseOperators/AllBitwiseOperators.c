#include <stdio.h>
int main()
{
    void PrintBinaryFormOfumber();
    unsigned int a, b, right_A, right_B, left_A, left_B, result;
    
    printf("\n\n");
    printf("Enter an integer = ");
    scanf("%u", &a);
    printf("\nEnter another integer = ");
    scanf("%u", &b);
    
    printf("\n\n");
    printf("How many bits to shift A to the right? ");
    scanf("%u", &right_A);
    printf("\nHow many bits to shift B to the right? ");
    scanf("%u", &right_B);
    printf("\nHow many bits to shift A to the left? ");
    scanf("%u", &left_A);
    printf("\nHow many bits to shift B to the left? ");
    scanf("%u", &left_B);
    
    printf("\n\n");
    result = a & b;
    printf("(a & b) gives %u\n", result);
    PrintBinaryFormOfumber(a);
    PrintBinaryFormOfumber(b);
    PrintBinaryFormOfumber(result);
    
    printf("\n\n");
    result = a | b;
    printf("(a | b) gives %u\n", result);
    PrintBinaryFormOfumber(a);
    PrintBinaryFormOfumber(b);
    PrintBinaryFormOfumber(result);
    
    printf("\n\n");
    result = a ^ b;
    printf("(a ^ b) gives %u\n", result);
    PrintBinaryFormOfumber(a);
    PrintBinaryFormOfumber(b);
    PrintBinaryFormOfumber(result);
    
    printf("\n\n");
    result = ~a;
    printf("(~a) gives %u\n", result);
    PrintBinaryFormOfumber(a);
    PrintBinaryFormOfumber(result);
    
    printf("\n\n");
    result = ~b;
    printf("(~b) gives %u\n", result);
    PrintBinaryFormOfumber(b);
    PrintBinaryFormOfumber(result);
    
    printf("\n\n");
    result = a >> right_A;
    printf("Right shift A = %u by %u bits gives %u\n", a, right_A, result);
    PrintBinaryFormOfumber(a);
    PrintBinaryFormOfumber(result);
    
    printf("\n\n");
    result = b >> right_B;
    printf("Right shift B = %u by %u bits gives %u\n", b, right_B, result);
    PrintBinaryFormOfumber(b);
    PrintBinaryFormOfumber(result);
    
    printf("\n\n");
    result = a << left_A;
    printf("Left shift A = %u by %u bits gives %u\n", a, left_A, result);
    PrintBinaryFormOfumber(a);
    PrintBinaryFormOfumber(result);
    
    printf("\n\n");
    result = b << left_B;
    printf("Left shift B = %u by %u bits gives %u\n", b, left_B, result);
    PrintBinaryFormOfumber(b);
    PrintBinaryFormOfumber(result);
    
}


void PrintBinaryFormOfumber(unsigned int decimal_number)
{
    unsigned int quotient, remainder;
    unsigned int num;
    unsigned int binary_array[8];
    int i;
    
    for(i = 0; i < 8; i++)
    {
        binary_array[i] = 0;
    }
    
    printf("the binary form of %u is ", decimal_number);
    
    num = decimal_number;
    i = 7;
    while(num != 0)
    {
        quotient = num / 2;
        remainder = num % 2;
        binary_array[i] = remainder;
        num = quotient;
        i--;
    }
    
    for(i = 0; i < 8 ; i++)
    {
        printf("%u", binary_array[i]);
    }
    
    printf("\n\n");
}