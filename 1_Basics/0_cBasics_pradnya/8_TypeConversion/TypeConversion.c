#include <stdio.h>

int main(void)
{
    int i, j;
    char ch_01, ch_02;
    int a, result_int;
    float f, result_float;
    
    int i_explicit;
    float f_explicit;
    
    printf("\n\n");
    
    
    //interconversion and implicit type casting between char and int types
    i = 70;
    ch_01 = i;
    printf("I = %d\n", i);
    printf("Character I (after ch_01 = i) = %c\n", ch_01);
    
    ch_02 = 'Q';
    j = ch_02;
    printf("Character 2 = %c\n", ch_02);
    printf("J (after j = ch_02) = %d\n", j);
    
    //implicit conversion of int to float
    a = 5;
    f = 7.8f;
    result_float = a + f;
    printf("Integer %d, and float %f when added give a float sum = %f\n", a, f, result_float);
    
    result_int = a + f;
    printf("Integer %d, and float %f when added give an integer sum = %d\n", a, f, result_int);
    
    //explicit type casting
    f_explicit = 30.121995f;
    i_explicit = (int)f_explicit;
    printf("Floating point number type which is going to be casted explicitly = %f to int \n", f_explicit);
    printf("Resultant integer after explicit type casting of %f is %d\n", f_explicit, i_explicit);
    
    return 0;
}