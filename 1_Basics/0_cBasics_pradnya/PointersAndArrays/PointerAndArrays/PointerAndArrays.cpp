// PointerAndArrays.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>

using std::cout;
using std::cin;


#define ARRAY_X_DIM 5
#define ARRAY_Y_DIM 3

//simple 2D array
void pointerDemo1()
{
	int array2D[ARRAY_X_DIM][ARRAY_Y_DIM];

	cout << "Enter array elements\n";

	for (auto it1 = std::begin(array2D); it1 != std::end(array2D); it1++)
	{
		
		for (auto it2 = std::begin(*it1); it2 != std::end(*it1) ; it2++)
		{
			std::cin>> *it2;
		}
	}

	cout << "All elements have been entered. Printing them\n";

	for (auto it1 = std::begin(array2D); it1 != std::end(array2D); it1++)
	{

		for (auto it2 = std::begin(*it1); it2 != std::end(*it1); it2++)
		{
			std::cout << *it2 <<'\n';
		}
	}
}

//2D array - each row of same size and is malloced
void pointerDemo2()
{

	int *array2D[5] = {};

	cout << "Enter array elements\n";

	for (auto it1 = std::begin(array2D); it1 != std::end(array2D); it1++)
	{
		*it1 = new(std::nothrow) int[ARRAY_Y_DIM];
		if (*it1 == nullptr)
		{
			cout << "Memory alloc failed";
			return;
		}

		for (int j = 0; j < ARRAY_Y_DIM ; j++)
		{
			std::cin >> (*it1)[j];
		}
	}

	cout << "All elements have been entered. Printing them\n";

	for (auto it1 = std::begin(array2D); it1 != std::end(array2D); it1++)
	{

		for (int j = 0; j < ARRAY_Y_DIM; j++)
		{
			std::cout << (*it1)[j] << '\n';
		}
	}

	cout << "Freeing memory \n";
	for (auto it1 = std::begin(array2D); it1 != std::end(array2D); it1++)
	{
		delete[] *it1;
		*it1 = nullptr;
	}
}

//2D array contructed completed with malloc. all 1D contituents of same size
void pointerDemo3()
{

	int **array2D =nullptr;

	cout << "Enter array elements\n";

	array2D = new(std::nothrow) int*[ARRAY_X_DIM];

	if (array2D == nullptr)
	{
		cout << "Memory alloc failed";
		return;
	}

	for (int i = 0; i < ARRAY_X_DIM; i++)
	{
		array2D[i] = new(std::nothrow) int[ARRAY_Y_DIM];
		if (array2D[i] == nullptr)
		{
			cout << "Memory alloc failed";
			return;
		}

		for (int j = 0; j < ARRAY_Y_DIM; j++)
		{
			std::cin >> array2D[i][j];
		}
	}

	cout << "All elements have been entered. Printing them\n";

	for (int i = 0; i < ARRAY_X_DIM; i++)
	{

		for (int j = 0; j < ARRAY_Y_DIM; j++)
		{
			std::cout << array2D[i][j] << '\n';
		}
	}

	cout << "Freeing memory \n";
	
	for (int i = 0; i < ARRAY_X_DIM; i++)
	{
		delete[] array2D[i];
		array2D[i] = nullptr;
	}

	delete[] array2D;
	array2D = nullptr;
}

//jagged array - alternative rows have sizez (ARRAY_Y_DIM+1), and (ARRAY_Y_DIM-1)
void pointerDemo4()
{

	int **array2D = nullptr;

	cout << "Enter array elements\n";

	array2D = new(std::nothrow) int*[ARRAY_X_DIM];

	if (array2D == nullptr)
	{
		cout << "Memory alloc failed";
		return;
	}
	int delta = 0;

	for (int i = 0; i < ARRAY_X_DIM; i++)
	{
		if (i % 2 == 0)
			delta = 1;
		else
			delta = -1;

		array2D[i] = new(std::nothrow) int[ARRAY_Y_DIM + delta];
		if (array2D[i] == nullptr)
		{
			cout << "Memory alloc failed";
			return;
		}

		for (int j = 0; j < ARRAY_Y_DIM + delta; j++)
		{
			std::cin >> array2D[i][j];
		}
	}

	cout << "All elements have been entered. Printing them\n";

	for (int i = 0; i < ARRAY_X_DIM; i++)
	{
		if (i % 2 == 0)
			delta = 1;
		else
			delta = -1;

		cout << '\n';
		for (int j = 0; j < ARRAY_Y_DIM + delta; j++)
		{
			std::cout << array2D[i][j] << '\n';
		}
	}

	cout << "Freeing memory \n";

	for (int i = 0; i < ARRAY_X_DIM; i++)
	{
		delete[] array2D[i];
		array2D[i] = nullptr;
	}

	delete[] array2D;
	array2D = nullptr;
}

int main()
{
	int opt = 0;
	bool bContinue = true;
	while (bContinue)
	{
		cout << "Menu \n 1. Pointer Demo1 \n 2. Pointer Demo2 \n 3. Pointer Demo3 \n 4. Pointer Demo4 (jagged arrays) \n Enter any other number to exit \n";
		cin >> opt;
		switch (opt)
		{
		case 1:
			pointerDemo1();
			break;
		case 2:
			pointerDemo2();
			break;
		case 3:
			pointerDemo3();
			break;
		case 4:
			pointerDemo4();
			break;
		default:
			bContinue = false;
			break;
		}
	}
    return 0;
}

