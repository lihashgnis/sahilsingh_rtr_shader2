#include <windows.h>

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam);
void toggleFullscreeen(HWND);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
    static wchar_t appName[] = L"FullScreenDemo123";
    static WNDCLASSEXW wc = {};
    wc.cbSize = sizeof(wc);
    wc.style = CS_HREDRAW | CS_VREDRAW;
    wc.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
    wc.cbClsExtra = 0;
    wc.cbWndExtra = 0;
    wc.lpszClassName = appName;
    wc.hInstance = hInstance;
    wc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
    wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
    wc.lpszMenuName = NULL;
    wc.hCursor = LoadCursor(NULL, IDC_ARROW);
    wc.lpfnWndProc = WndProc;

    if (!RegisterClassExW(&wc))
    {
        MessageBoxA(NULL, "register class", "error", MB_OK);
        return -1;
    }

    //HWND hwnd = CreateWindow(appName, TEXT("FullScreen"), WS_OVERLAPPEDWINDOW, 0, 0, 800, 800, NULL, NULL, hInstance, 0);
    HWND hwnd = CreateWindowW(appName, L"My Appp", WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, NULL, NULL, hInstance, NULL);
    auto err = GetLastError();
    err;

    ShowWindow(hwnd, iCmdShow);
    UpdateWindow(hwnd);

    MSG msg;
    while (GetMessage(&msg, 0, 0, 0))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }
    return (int)msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
    switch (iMsg)
    {
    case WM_CHAR:
    {
        switch (wParam)
        {
        case 'f':
            //[[fallthrough]]
        case 'F':
        {
            toggleFullscreeen(hwnd);
        }
        break;
        default:
            break;
        }
    }
    break;
    default:
        break;
    }
    return DefWindowProc(hwnd, iMsg, wParam, lParam);
}

void toggleFullscreeen(HWND hwnd)
{
    static bool bIsFullScreen = false;
    static WINDOWPLACEMENT wpPrev;
    DWORD dwStyle = 0;;

    if (bIsFullScreen == false)
    {
        dwStyle = GetWindowLong(hwnd, GWL_STYLE);
        
        if (dwStyle & WS_OVERLAPPEDWINDOW)
        {
            MONITORINFO mi = {};
            mi.cbSize = sizeof(mi);

            if (GetWindowPlacement(hwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(hwnd, MONITORINFOF_PRIMARY), &mi))
            {
                SetWindowLong(hwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
                SetWindowPos(hwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left,
                    mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
                ShowCursor(FALSE);
                bIsFullScreen = true;
            }
        }
    }
    else
    {
        SetWindowLong(hwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
        SetWindowPlacement(hwnd, &wpPrev);
        SetWindowPos(hwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
        ShowCursor(TRUE);
        bIsFullScreen = FALSE;
    }
}