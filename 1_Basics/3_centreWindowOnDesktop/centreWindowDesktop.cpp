#include<windows.h>

constexpr UINT WIN_LEFT  =  100;
constexpr UINT WIN_RIGHT =  100;
constexpr UINT WIN_WIDTH = 800;
constexpr UINT WIN_HEIGHT = 800;

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
void ToggleCentre(HWND hwnd);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
    TCHAR AppName[] = "CentreWindowsDesktop";

    WNDCLASSEX wndclass;
    wndclass.cbSize = sizeof(wndclass);
    wndclass.lpfnWndProc = WndProc;
    wndclass.hInstance = hInstance;
    wndclass.lpszMenuName = NULL;
    wndclass.lpszClassName = AppName;
    wndclass.cbClsExtra = 0;
    wndclass.cbWndExtra = 0;
    wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
    wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
    wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
    wndclass.style = CS_HREDRAW | CS_VREDRAW;
    wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);

    if (!RegisterClassEx(&wndclass))
    {
        MessageBoxA(NULL, "Register class failed", "Pause", MB_OK);
        return -1;
    }

    HWND hwnd = CreateWindow(AppName, AppName, WS_OVERLAPPEDWINDOW, WIN_LEFT, WIN_RIGHT, WIN_WIDTH, WIN_HEIGHT, NULL, NULL, hInstance, 0);

    ShowWindow(hwnd, iCmdShow);
    UpdateWindow(hwnd);

    MSG msg;
    while (GetMessage(&msg, 0, 0, 0))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return (int)msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
    switch (iMsg)
    {
    case WM_CHAR:
    {
        switch (wParam)
        {
        case 'c':
        case 'C':
        {
            ToggleCentre(hwnd);
        }
        break;
        default:
            break;
        }
    }
    break;
    case WM_DESTROY:
    {
        PostQuitMessage(0);
    }
    break;
    default:
        break;
    }
    return DefWindowProc(hwnd, iMsg, wParam, lParam);
}

void ToggleCentre(HWND hwnd) {
    static bool isCentre = false;
    static RECT oldRect = {};

    MONITORINFO mi;

    if (isCentre == false)
    {
        WINDOWPLACEMENT wp;
        mi.cbSize = sizeof(mi);

        if (GetWindowPlacement(hwnd, &wp) && GetMonitorInfo(MonitorFromWindow(hwnd, MONITORINFOF_PRIMARY), &mi))
        {
            GetWindowRect(hwnd, &oldRect);
            
            
            auto& monRc = mi.rcMonitor;
            auto top = (monRc.bottom + monRc.top) / 2.0 - WIN_HEIGHT/2.0;
            auto left = (monRc.right + monRc.left) / 2.0 - WIN_WIDTH / 2.0;

            SetWindowPos(hwnd, HWND_TOP, left, top, 0, 0, SWP_NOSIZE);
            isCentre = true;
        }
    }
    else
    {
        SetWindowPos(hwnd, HWND_TOP, oldRect.left, oldRect.top, 0, 0, SWP_NOSIZE);
        isCentre = false;
    }
}