#include <Windows.h>
#include <gl/GL.h>

#pragma comment (lib, "OpenGl32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

HDC gHdc = NULL;
HGLRC gHrc = NULL;

bool gbActiveWindow = false;

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
    TCHAR AppName[] = "OpenGlSingleBuffer";
    
    WNDCLASSEX wc = {};
    wc.cbSize = sizeof(wc);
    wc.cbClsExtra = 0;
    wc.cbWndExtra = 0;
    wc.lpfnWndProc = WndProc;
    wc.hInstance = hInstance;
    wc.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
    wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
    wc.lpszClassName = AppName;
    wc.style(CS_HREDRAW | CS_VREDRAW | CS_OWNDC)

    if (!RegisterClassEx(&wc))
    {

    }
    

}

int Initialize(HWND hwnd)
{
    PIXELFORMATDESCRIPTOR pfd;
    ZeroMemory((void*)&pfd, NULL, sizeof(PIXELFORMATDESCRIPTOR));
    pfd.nSize = sizeof(pfd);
    pfd.nVersion = 1;
    pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL;
    pfd.iPixelType = PFD_TYPE_RGBA;
    pfd.cColorBits = 32;
    pfd.cRedBits = 8;
    pfd.cGreenBits = 8;
    pfd.cBlueBits = 8;
    pfd.cAlphaBits = 8;

    auto hdc = GetDC(hwnd);
    auto iPixelFormatIndex = ChoosePixelFormat(hdc, &pfd);

    if (iPixelFormatIndex == 0)
    {
        return -1;
    }

    if (SetPixelFormat(hdc, iPixelFormatIndex, &pfd) == false)
    {
        return -2;
    }

    auto hrc = wglCreateContext(hdc);//create rendering context

    if (hrc == NULL)
    {
        return -3;
    }

    if (wglMakeCurrent(hdc, hrc) == false)//associate the rendering context to current thread
    {
        return -4;
    }

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
   // resize();
}