#include<windows.h>

constexpr UINT WIN_LEFT = 100;
constexpr UINT WIN_RIGHT = 100;
constexpr UINT WIN_WIDTH = 800;
constexpr UINT WIN_HEIGHT = 800;

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
void TextOut(HWND hwnd);

int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
    TCHAR AppName[] = "TextColorAssignment";
    WNDCLASSEX wc = {};
    wc.cbSize = sizeof(wc);
    wc.lpfnWndProc = WndProc;
    wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
    wc.hInstance = hInstance;
    wc.style = CS_HREDRAW | CS_VREDRAW;
    wc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
    wc.lpszClassName = AppName;

    if (!RegisterClassEx(&wc))
    {
        MessageBoxA(NULL, "RegisterClassEx() failed", "Error", MB_OK);
        return -1;
    }

    HWND hwnd = CreateWindow(AppName, AppName, WS_OVERLAPPEDWINDOW, WIN_LEFT, WIN_RIGHT, WIN_WIDTH, WIN_HEIGHT, NULL, NULL, hInstance, 0);

    //ShowWindow(hwnd, iCmdShow);
    //UpdateWindow(hwnd);

    MSG msg;

    while (GetMessage(&msg, 0, 0, 0))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return (int)msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
    switch (iMsg)
    {
    case WM_CREATE:
    {
        ShowWindow(hwnd, SW_NORMAL);
        UpdateWindow(hwnd);
        TextOut(hwnd);
    }
    break;
    case WM_PAINT:
    {
        TextOut(hwnd);
    }
    break;
    case WM_DESTROY:
    {
        PostQuitMessage(0);
    }
    break;
    }
    return DefWindowProc(hwnd, iMsg, wParam, lParam);
}

void TextOut(HWND hwnd)
{
    RECT rc;
    GetClientRect(hwnd, &rc);
    HDC hDC = GetDC(hwnd);
    SetBkColor(hDC, RGB(0, 0, 0));
    SetTextColor(hDC, RGB(0, 255, 0));
    DrawText(hDC, TEXT("Hello World"), -1, &rc, DT_SINGLELINE | DT_CENTER | DT_VCENTER);
    ReleaseDC(hwnd, hDC);
}