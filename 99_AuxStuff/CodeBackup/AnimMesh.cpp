#include "AnimMesh.h"
#include "OpenGLState.h"
#include <regex>

void AnimMesh::CalculateCombinedTransforms(std::shared_ptr<AnimBone> node)
{
    if (node->GetParent())
    {
        node->GetCombinedTransformRef() = node->GetLocalTransformRef() * node->GetParent()->GetCombinedTransformRef();
    }
    else//root node
    {
        node->GetCombinedTransformRef() = node->GetLocalTransformRef();
    }
}

void AnimMesh::Process()
{
    if (nullptr == m_animModel)
    {
        THROW_DEBUG_EXCEPTION("m_animModel is null. Attach() not called?");
    }

    bool retflag;
    ProcessVertices(retflag);
    DEBUG_ASSERT(retflag == false, "ProcessVertices() failed")
    ProcessBones(retflag);
    DEBUG_ASSERT(retflag == false, "ProcessVertices() failed")
    if (retflag) return;

    InitShader();
    CreateVertexVao();

    //AnimModel::DumpGraphvizDotFile("bonesTree.dot", m_skeletonRoot);
}

void AnimMesh::ProcessVertices(bool& retflag)
{
    auto numVertices = m_aiMeshRef->mNumVertices;
    m_vPosition                     = std::vector<V3GLf>                              (numVertices);
    m_vTexCoord                     = std::vector<V2GLf>                              (numVertices);
    m_vNormals                      = std::vector<V3GLf>                              (numVertices);
    m_vJointIndices                 = std::vector<V4GLi>                              (numVertices);
    m_vJointWeights                 = std::vector<V4GLf>                              (numVertices);
    m_vJointIndicesWeights_shadow   = std::vector<std::vector<std::pair<float, int>>> (numVertices);

    for (unsigned int i = 0; i < numVertices; i++)
    {
        aiVector3D aiVert = m_aiMeshRef->mVertices[i];
        m_vPosition[i].i() = aiVert[0];
        m_vPosition[i].j() = aiVert[1];
        m_vPosition[i].k() = aiVert[2];

        aiVector3D aiNormals = m_aiMeshRef->mNormals[i];
        m_vNormals[i] = V3GLf{ aiNormals[0], aiNormals[1] , aiNormals[2] };
    }

    for (unsigned int i = 0; i < m_aiMeshRef->mNumFaces; i++)
    {
        aiFace aiFace = m_aiMeshRef->mFaces[i];

        DEBUG_ASSERT(aiFace.mNumIndices == 3, "assumption: aiProcess_Triangulate flag is used, hence all faces should be triangles");

        for (unsigned int j = 0; j < aiFace.mNumIndices; j++)
        {
            m_indices.push_back(aiFace.mIndices[j]);
        }
    }
    m_vertexProcessingDone = true;
    retflag = false;
}

GLuint AnimMesh::CreateVertexVao()
{
    DEBUG_MSG_INFO("AnimMesh::CreateVertexVao enter");
    glGenVertexArrays(1, &m_vao);
    glBindVertexArray(m_vao);
    {
        /*1. POSITION*/
        DEBUG_MSG_INFO("AnimMesh::CreateVertexVao POSITION data");
        glGenBuffers(1, &m_vboPosition);
        glBindBuffer(GL_ARRAY_BUFFER, m_vboPosition);
        {
            //sanity check - glBufferData() expects C like layout of data, this may not be true in case of virtual functions, etc. in C++ class.
            DEBUG_ASSERT(std::is_standard_layout<std::remove_reference<decltype(m_vPosition[0])>>::value, "glBufferData() is going to fail. class data not in contigous memory locations.");
            glBufferData(GL_ARRAY_BUFFER, m_vPosition.size() * sizeof(m_vPosition[0]), m_vPosition.data(), GL_STATIC_DRAW);
            glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
            glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
        }

        /*2. TEXTURE COORDINATES*/
        DEBUG_MSG_INFO("AnimMesh::CreateVertexVao TEXTURE data");
        glGenBuffers(1, &m_vboTexCoord);
        glBindBuffer(GL_ARRAY_BUFFER, m_vboTexCoord);
        {
            //sanity check - glBufferData() expects C like layout of data, this may not be true in case of virtual functions, etc. in C++ class.
            DEBUG_ASSERT(std::is_standard_layout<std::remove_reference<decltype(m_vTexCoord[0])>>::value, "glBufferData() is going to fail. class data not in contigous memory locations.");
            glBufferData(GL_ARRAY_BUFFER, m_vTexCoord.size() * sizeof(m_vTexCoord[0]), m_vTexCoord.data(), GL_STATIC_DRAW);
            glVertexAttribPointer(AMC_ATTRIBUTE_TEXCOORD0, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
            glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXCOORD0);
        }

        /*3. NORMALS*/
        DEBUG_MSG_INFO("AnimMesh::CreateVertexVao NORMAL data");
        glGenBuffers(1, &m_vboNormals);
        glBindBuffer(GL_ARRAY_BUFFER, m_vboNormals);
        {
            //sanity check - glBufferData() expects C like layout of data, this may not be true in case of virtual functions, etc. in C++ class.
            DEBUG_ASSERT(std::is_standard_layout<std::remove_reference<decltype(m_vNormals[0])>>::value, "glBufferData() is going to fail. class data not in contigous memory locations.");
            glBufferData(GL_ARRAY_BUFFER, m_vNormals.size() * sizeof(m_vNormals[0]), m_vNormals.data(), GL_STATIC_DRAW);
            glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
            glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
        }

        /*4. JOINT INDICES*/
        DEBUG_MSG_INFO("AnimMesh::CreateVertexVao JOINT INDICES data");
        glGenBuffers(1, &m_vboJointIndices);
        glBindBuffer(GL_ARRAY_BUFFER, m_vboJointIndices);
        {
            //sanity check - glBufferData() expects C like layout of data, this may not be true in case of virtual functions, etc. in C++ class.
            DEBUG_ASSERT(std::is_standard_layout<std::remove_reference<decltype(m_vJointIndices[0])>>::value, "glBufferData() is going to fail. class data not in contigous memory locations.");
            glBufferData(GL_ARRAY_BUFFER, m_vJointIndices.size() * sizeof(m_vJointIndices[0]), m_vJointIndices.data(), GL_STATIC_DRAW);
            glVertexAttribIPointer(AMC_ATTRIBUTE_JOINT_INDICES, 4, GL_INT, 0, nullptr);
            glEnableVertexAttribArray(AMC_ATTRIBUTE_JOINT_INDICES);
        }

        /*5. Joint Weights*/
        DEBUG_MSG_INFO("AnimMesh::CreateVertexVao JOINT WEIGHTS data");
        glGenBuffers(1, &m_vboJointWeights);
        glBindBuffer(GL_ARRAY_BUFFER, m_vboJointWeights);
        {
            //sanity check - glBufferData() expects C like layout of data, this may not be true in case of virtual functions, etc. in C++ class.
            DEBUG_ASSERT(std::is_standard_layout<std::remove_reference<decltype(m_vJointWeights[0])>>::value, "glBufferData() is going to fail. class data not in contigous memory locations.");
            glBufferData(GL_ARRAY_BUFFER, m_vJointWeights.size() * sizeof(m_vJointWeights[0]), m_vJointWeights.data(), GL_STATIC_DRAW);
            glVertexAttribPointer(AMC_ATTRIBUTE_JOINT_WEIGHTS, 4, GL_FLOAT, GL_FALSE, 0, nullptr);
            glEnableVertexAttribArray(AMC_ATTRIBUTE_JOINT_WEIGHTS);
        }

        /*6. Face Indices*/
        DEBUG_MSG_INFO("AnimMesh::CreateVertexVao FACE INDICES data");
        glGenBuffers(1, &m_vboFaceIndices);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_vboFaceIndices);
        {
            //sanity check - glBufferData() expects C like layout of data, this may not be true in case of virtual functions, etc. in C++ class.
            DEBUG_ASSERT(std::is_standard_layout<std::remove_reference<decltype(m_indices[0])>>::value, "glBufferData() is going to fail. class data not in contigous memory locations.");
            glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_indices.size() * sizeof(m_indices[0]), m_indices.data(), GL_STATIC_DRAW);
        }
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    }
    glBindVertexArray(0);

    DEBUG_MSG_INFO("AnimMesh::CreateVertexVao exit");
    return m_vao;
}

bool AnimMesh::InitShader()
{
    DEBUG_MSG_INFO("AnimMesh::InitShader enter");
    GLuint vertexShaderObject = 0;
    GLuint fragmentShaderObject = 0;

    /*1. VERTEX SHADER*/
    std::string vertexShaderSourceCode =
        R"(#version 450 core

                #define CONFIG_MAX_BONES_PER_VERTEX 4

                in vec4  vPosition;
                in vec2  vTexCoord; 
                in vec3  vNormals;
                in ivec4 vJointIndices;
                in vec4  vJointWeights;

                uniform mat4 u_m_matrix; //model matrix
                uniform mat4 u_v_matrix; //view matrix
                uniform mat4 u_p_matrix; //projection matrix
                
                uniform mat4 u_jointTransforms[REGEX_REPLACE_VAR_NUM_BONES1];//REGEX_REPLACE_VAR_NUM_BONES1 will be replaced by string substitution

                flat out int error;

                void main()
                {
                    error = 0;
                    vec4 totalLocalPos = vec4(0.0);
                    float totweight = 0.0;
                    for(int i = 0; i < CONFIG_MAX_BONES_PER_VERTEX; i++)
                    {
                        vec4 localPos = u_jointTransforms[vJointIndices[i]] * vPosition;
                        totalLocalPos += localPos * vJointWeights[i];
                        totweight += vJointWeights[i];
                    } 

                    if((vJointIndices[0] < 0))
                    {
                        error = 120;
                    } 
                     else if(vJointIndices[0] > 116800)
                     {
                                error = 121;
                       }
                    //else if((vJointIndices[1] > 67) || (vJointIndices[1] < 0))
                    //{
                    //    error = 121;
                    //} else
                    // if((vJointIndices[2] > 67) || (vJointIndices[2] < 0))
                    //{
                    //    error = 122;
                    //} else
                    // if((vJointIndices[3] > 67) || (vJointIndices[3] < 0))
                    //{
                    //    error = 123;
                    //}
                   
                    
                    //if(u_jointTransforms[vJointIndices[0]] != mat4(1.0))
                    //{
                    //    error = 120;
                    //} else
                    // if(u_jointTransforms[vJointIndices[1]] != mat4(1.0))
                    //{
                    //    error = 121;
                    //} else
                    // if(u_jointTransforms[vJointIndices[2]] != mat4(1.0))
                    //{
                    //    error = 122;
                    //} else
                    // if(u_jointTransforms[vJointIndices[3]] != mat4(1.0))
                    //{
                    //    error = 123;
                    //}

                    //if(totweight == 1.0)
                    //{
                    //    error = 120;
                    //}
                    //else
                    //if(totweight > 1.0)
                    //{
                    //    error = 121;
                    //}
                    ////else if(isinf(totweight))
                    //else if(totweight < 1.0)
                    //{
                    //    error = 122;
                    //}

                    gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * totalLocalPos;
                    //gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * vPosition;
                }
                )";

    std::regex numBones("REGEX_REPLACE_VAR_NUM_BONES1");
    vertexShaderSourceCode = std::regex_replace(vertexShaderSourceCode, numBones, std::to_string(m_numBones));
    DEBUG_ASSERT(m_numBones != 0, "m_numBones : no bones in this mesh of the model ?");

    vertexShaderObject = Util::generateShader(GL_VERTEX_SHADER, vertexShaderSourceCode.c_str());

    /*2. FRAGMENT SHADER*/
    const GLchar* fragmentShaderSourceCode =
        R"(#version 450 core

               out vec4 FragColor;
               flat in int error;

               void main()
                {
                    if(error == 120)
                    {
                        FragColor = vec4(12.0/255.0, 43.0/255.0, 199.0/255.0, 1.0);//blue
                    }else
                    if(error == 121)
                    {
                        FragColor = vec4(235.0/255.0, 16.0/255.0, 206.0/255.0, 1.0);//pink
                    }
                    else if(error == 122)
                    {
                        FragColor = vec4(154.0/255.0, 232.0/255.0, 9.0/255.0, 1.0);//green
                    }
                    else if(error == 123)
                    {
                        FragColor = vec4(235.0/255.0, 225.0/255.0, 35.0/255.0, 1.0);//yellow
                    }
                    else
                    {
                        FragColor = vec4(1.0);
                    }
                }
                )";
    fragmentShaderObject = Util::generateShader(GL_FRAGMENT_SHADER, fragmentShaderSourceCode);

    m_shaderProgramObject = glCreateProgram();

    glAttachShader(m_shaderProgramObject, vertexShaderObject);
    glAttachShader(m_shaderProgramObject, fragmentShaderObject);

    //pre link
    glBindAttribLocation(m_shaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(m_shaderProgramObject, AMC_ATTRIBUTE_TEXCOORD0, "vTexCoord");
    glBindAttribLocation(m_shaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vNormals");
    glBindAttribLocation(m_shaderProgramObject, AMC_ATTRIBUTE_JOINT_INDICES, "vJointIndices");
    glBindAttribLocation(m_shaderProgramObject, AMC_ATTRIBUTE_JOINT_WEIGHTS, "vJointWeights");

    //link program
    glLinkProgram(m_shaderProgramObject);

    //check for linking errors

    GLint iProgramLinkStatus = 0;
    GLint iInfoLogLength = 0;
    GLchar* szInfoLog = NULL;

    glGetProgramiv(m_shaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);

    if (iProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(m_shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

        if (iInfoLogLength > 0)
        {
            szInfoLog = (GLchar*)malloc(iInfoLogLength);

            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(m_shaderProgramObject, iInfoLogLength, &written, szInfoLog);
                UTIL_SET_BREAKPOINT;
                free(szInfoLog);
                DEBUG_ASSERT(false, "shader linking failed");
                //uninitialize();
                exit(0);
            }
        }
    }

    //post link
    m_u_m_matrix = glGetUniformLocation(m_shaderProgramObject, "u_m_matrix");
    m_u_v_matrix = glGetUniformLocation(m_shaderProgramObject, "u_v_matrix");
    m_u_p_matrix = glGetUniformLocation(m_shaderProgramObject, "u_p_matrix");
    m_u_jointTransforms_location = glGetUniformLocation(m_shaderProgramObject, "u_jointTransforms");
    
    DEBUG_MSG_INFO("AnimMesh::InitShader exit");
    return true;
}

void AnimMesh::Render()
{
    DEBUG_MSG_INFO("AnimMesh::Render");
    ComputeSkeletonTransforms();

    glUseProgram(m_shaderProgramObject);

    //matrices
    mat4 modelMatrix = mat4::identity(), viewMatrix = openGLState.Getcamera().getViewMatrix(), projectionMatrix = openGLState.GetProjectionMatrix();
    modelMatrix = translate(33.0f, 61.0f, 1.0f);
    //modelMatrix = scale(1000.0f, 1000.0f, 1000.0f);
    //TODO: add code to calculate and sent bone transform matrix code
    
    glUniformMatrix4fv(m_u_m_matrix, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(m_u_v_matrix, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(m_u_p_matrix, 1, GL_FALSE, projectionMatrix);
    DEBUG_MSG_INFO("AnimMesh::Render sending bones matrices");
    glUniformMatrix4fv(m_u_jointTransforms_location, m_boneNames.size(), GL_FALSE, m_u_jointTransforms_Matrices[0]);


    glBindVertexArray(m_vao);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_vboFaceIndices);

    glDrawElements(GL_TRIANGLES, m_indices.size(), GL_UNSIGNED_INT, nullptr);


}

/*build skeleton
    Logic -
    go through list of bones of the mesh, keep on attaching them to corresponding nodes.
    nodes have hierarchy information, bones (aiBones) do not.
    Once all bones are attached, parse nodes from top, creating one bone in skeleton for each node
    which has a bone attached.
    aiProcess_PopulateArmatureData seems to help in this, but I haven't been able to figure out how.
    */
void AnimMesh::ProcessBones(bool& retflag)
{
    retflag = true;

    if (m_vertexProcessingDone == false)
    {
        ProcessVertices(retflag);

        DEBUG_ASSERT(retflag == false, "ProcessVertices() failed")
    }

    
    m_animModel->ClearAllLinkedBonesFromNode();
    m_numBones = m_aiMeshRef->mNumBones;
    m_u_jointTransforms_Matrices.reset(new mat4[m_numBones]);
    
    memset(m_u_jointTransforms_Matrices.get(), 0, sizeof(mat4)*m_numBones);

    for (unsigned int i = 0; i < m_numBones; i++)
    {
        aiBone* bone = m_aiMeshRef->mBones[i];
        std::shared_ptr<AnimNode> node = m_animModel->GetNodeByName(bone->mName.C_Str());
        node->AttachBone(bone);
        m_boneNames.push_back(bone->mName.C_Str());

        for (unsigned int j = 0; j < bone->mNumWeights; j++)
        {
            aiVertexWeight vWeight = bone->mWeights[j];
            auto vertexId = vWeight.mVertexId;
            
            m_vJointIndicesWeights_shadow[vertexId].push_back(std::make_pair(vWeight.mWeight, i));
            
        }
    }

    //sort m_vJointIndicesWeights_shadow for each vertex on the basis of weight
    for (int i = 0; i < m_vJointIndicesWeights_shadow.size(); i++)
    {
        auto& vertexData = m_vJointIndicesWeights_shadow[i];
        std::sort(vertexData.begin(), vertexData.end(), [](const std::pair<float, int>& a, const std::pair<float, int>& b)
            {
                return b.second < a.second; //sort in descending order
            });
        
        m_vJointWeights[i] = V4GLf(0.0);
        m_vJointIndices[i] = V4GLi(0.0);

        for (int j = 0; j < CONFIG_MAX_BONES_PER_VERTEX && j < vertexData.size() ; j++)
        {
            m_vJointWeights[i][j] = vertexData[j].first;
            m_vJointIndices[i][j] = vertexData[j].second;
        }
    }

    //sahils-debug remove this
    //for (int i = 0; i < m_vJointIndices.size(); i++)
    //{
    //    OutputDebugStringA(std::to_string(i).c_str());
    //    OutputDebugStringA(" ");
    //    OutputDebugStringA(std::to_string(m_vJointIndices[i][0]).c_str());
    //    OutputDebugStringA(" ");
    //    OutputDebugStringA(std::to_string(m_vJointIndices[i][1]).c_str());
    //    OutputDebugStringA(" ");
    //    OutputDebugStringA(std::to_string(m_vJointIndices[i][2]).c_str());
    //    OutputDebugStringA(" ");
    //    OutputDebugStringA(std::to_string(m_vJointIndices[i][3]).c_str());
    //    OutputDebugStringA("\n");
    //}

    int* p = &(m_vJointIndices.data()[0][0]);
    int j = 0;
    for (int i = 0; p <= &(m_vJointIndices.end()-1)[0][3]; i++)
    {
        if ((i) % 4 == 0)
        {

            OutputDebugStringA("\n");
            OutputDebugStringA(std::to_string(j).c_str());
            j++;
        }
        
        OutputDebugStringA(" ");
        OutputDebugStringA(std::to_string(*p).c_str());
        p++;
    }

    for (auto& vertexWeights : m_vJointWeights)
    {
        GLfloat sum = vertexWeights[0] + vertexWeights[1] + vertexWeights[2] + vertexWeights[3];
        if (sum > 0)
        {
            vertexWeights = vertexWeights / sum;
        }
    }

    /*FIND root bone in AnimMode tree
    logic - find any bone node, traverse upwards till we find an AnimNode, which satisfies at least 1 of the 2 criteria
    1. Parent isn't a bone node
    2. Does't have a parent
    */
    auto& nodeMap = m_animModel->GetNodeMapRef();
    std::shared_ptr<AnimNode> anyBoneNode = nullptr;
    std::shared_ptr<AnimNode> rootBoneNode = nullptr;

    for (auto& ele : nodeMap)
    {
        if (AnimNodeType::BONE == ele.second->GetNodeType())
        {
            anyBoneNode = ele.second;
            break;
        }
    }

    if (anyBoneNode == nullptr)
    {
        THROW_DEBUG_EXCEPTION("no bone node found");
        return;
    }

    while (true)
    {
        if (anyBoneNode == nullptr)
        {
            THROW_DEBUG_EXCEPTION("Unexpected exception");
            return;
        }

        if (anyBoneNode->GetParent() == nullptr || anyBoneNode->GetParent()->GetNodeType() != AnimNodeType::BONE)
        {
            break;
        }
        anyBoneNode = anyBoneNode->GetParent();
    }

    if (anyBoneNode == nullptr)
    {
        THROW_DEBUG_EXCEPTION("Unexpected exception");
        return;
    }

    rootBoneNode = anyBoneNode;

    ConstructSkeleton(m_skeletonRoot, rootBoneNode, nullptr);
    retflag = false;
}


void AnimMesh::ConstructSkeleton(std::shared_ptr<AnimBone> bone, std::shared_ptr<AnimNode> animNode, std::shared_ptr<AnimBone> pParentBone)
{
    if (!bone)
    {
        THROW_DEBUG_EXCEPTION("bone - empty pointer, dynamic_cast<> failed?");
        return;
    }

    bone->GetNameRef() = animNode->GetNameCopy();
    bone->SetParent(pParentBone);
    bone->GetLocalTransformRef() = animNode->GetLocalTransformCopy();
    bone->GetCombinedTransformRef() = animNode->GetCombinedTransformCopy();
    bone->SetNodeType(AnimNodeType::BONE);

    m_boneMap[bone->GetNameRef()] = bone;

    auto& animNodeChildren = animNode->GetChildrenRef();
    size_t numChildren = animNodeChildren.size();

    if (numChildren > 0)
    {
        bone->GetChildrenRef().reserve(numChildren);

        for (unsigned int i = 0; i < numChildren; i++)
        {
            if (animNodeChildren[i]->GetNodeType() == AnimNodeType::BONE)
            {
                std::shared_ptr<AnimBone> child(new AnimBone);
                bone->GetChildrenRef().push_back(child);
                ConstructSkeleton(std::dynamic_pointer_cast<AnimBone>(bone->GetChildrenRef()[i]), animNodeChildren[i], bone);
            }
        }
    }
}

void AnimMesh::ComputeSkeletonTransforms()
{
    ComputeSkeletonTransformsInternal(m_skeletonRoot);

    for (int i = 0; i < m_boneNames.size(); i++)
    {
        std::shared_ptr<AnimNode> bone = GetBoneByName(m_boneNames[i]);
        m_u_jointTransforms_Matrices[i] = bone->GetCombinedTransformRef();
    }
}

std::shared_ptr<AnimNode> AnimMesh::GetBoneByName(const std::string & nodeName)
{
    auto itr = m_boneMap.find(nodeName);
    if (m_boneMap.end() == itr)
    {
        THROW_DEBUG_EXCEPTION("bone not found")
            return nullptr;
    }
    return itr->second;
}

void AnimMesh::ComputeSkeletonTransformsInternal(std::shared_ptr<AnimBone> bone)
{
    if (!bone)
    {
        return;
    }

    if (bone->GetParent())
    {
        bone->GetLocalTransformRef() *= bone->GetParent()->GetCombinedTransformRef();
    }

    for (auto& child : bone->GetChildrenRef())
    {
        ComputeSkeletonTransformsInternal(std::dynamic_pointer_cast<AnimBone>(child)); //sahils-test check
    }
}

void AnimMesh::Attach(AnimModel* animModel)
{
    m_animModel = animModel;
}