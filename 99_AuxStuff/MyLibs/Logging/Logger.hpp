#pragma once
#include <iostream>
#include <fstream>
#include <chrono>
#include <ctime>

using std::cout;
using std::string;
using std::ofstream;

#define LOG(logLevel, logString)\
    loggerObj.appendlog(LogLevel::logLevel, logString, __FILE__, __LINE__, __func__)

#define CREATE_LOG()\
    static Logger loggerObj;

//using X macros to create enum and its string representation
#define ENUM_TXT \
X(Info) \
X(Debug) \
X(Warning) \
X(Error)    \
X(FatalError) /*FatalError - Program must terminate*/

enum class LogLevel : UINT
{
#define X(arg) arg,
    ENUM_TXT
#undef X
};

static string logLevelChar[] = 
{
#define X(arg) #arg,
    ENUM_TXT
#undef X
};

class Logger {
    ofstream m_LogFile;
    const char* time_now_formatted()
    {
		//auto t = std::time(nullptr);
        //return std::asctime(std::localtime(&t));
		return "0";
    }

    auto epoch_millisecond()
    {
        return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now().time_since_epoch()).count();
    }

    auto logLevelToString(LogLevel logLevel)
    {
        return logLevelChar[(UINT)logLevel];
    }

public:
    Logger()
    {
        m_LogFile.open("RTR_log.txt", std::ios::app);
        m_LogFile << "\n\n##NewLogObject TIME "<< time_now_formatted()<< " ";
        m_LogFile << epoch_millisecond();
        m_LogFile << " ##\n";
    }
    ~Logger()
    {
        m_LogFile.close();
    }

    void appendlog(LogLevel logLevel, string logString, string fileName, long lineNumber, string functionName)
    {
        if (m_LogFile)
        {
			m_LogFile << time_now_formatted() << "[";
			m_LogFile << logLevelToString(logLevel).c_str() << "]" << logString.c_str() << "[" << fileName.c_str() << ":" << lineNumber << ":" << functionName.c_str() << "]\n";
        }
    }
};


