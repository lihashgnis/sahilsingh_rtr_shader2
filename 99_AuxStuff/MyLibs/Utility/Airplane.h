#pragma once

#include "Util.hpp"
#include <random>

constexpr UINT NUM_EXHAUSTS = 3;

namespace util {
    
    class Airplane :
        public SceneObject
    {
        DataTypes::POINT3Dd m_prevPosition;
        bool m_bIsFirstRun; //set m_prevPosition in the first run
        bool m_bProduceParticles; //If set to false, airplane will not produce particles from its exhaust
        bool m_bParticlesDontDie; //If set to true, airplane exhaust particles will not have OBJECT_KILL keyframe added to them  -when we want the exhaust smoke to persist
        DataTypes::POINTd m_vertices[12];//B2 Bomber - Measurements taken in GIMP by measuring pixel coordinates of - https://sflanders.net/wp-content/uploads/2018/05/stealth.png [23 March 2019]
        std::vector<COLORREF> m_ExhaustColors;
    public:
        Airplane();
        ~Airplane();
        void SetProduceParticles(bool bProduceParticles = true)
        {
            m_bProduceParticles = bProduceParticles;
        }

        void SetParticlesDontDie(bool bParticlesDontDie = false)
        {
            m_bParticlesDontDie = bParticlesDontDie;
        }

        // Inherited via SceneObject
        virtual void Draw(AnimationObjectState& state) override;
        virtual void AdvanceTime() override;
    };
}
