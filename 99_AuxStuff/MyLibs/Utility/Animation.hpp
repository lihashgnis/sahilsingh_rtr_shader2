#pragma once
#include "Util.hpp"
#include <vector>
#include <memory>
#include <functional>
#include<string>


namespace util
{
    double operator"" s(unsigned long long x); //representing seconds - just to make code omre readable
    

    double operator"" s(long double x); //representing seconds - just to make code omre readable

    class KeyFrame;
    class Timeline;

    class AnimationObject
    {
        std::vector<KeyFrame> m_keyFramesUnprocessed;// , m_keyFramesProcessed;
        bool m_bIsHidden; //object is hidden - do not render
        bool m_bIsAlive; //object is alive else delete object and remove from render loop. Its is better to kill an object than to hide it if it is no longer going to be shown. This will save resources
        std::shared_ptr<SceneObject> m_sceneObject;
        DataTypes::POINT3Dd m_CurrentPos, m_CurrentScale, m_CurrentRotationVector /*Rotation vector*/;
        OglWindow *m_windowObject;//In case objects want to set/get window properties

        double m_CurrentRotationAngle;
        double m_CurrentTransparency;
        double m_currentTime;
        double m_stepSize;
        
        //For tweening we wan to know the position, etc. at prev frame time
        //so that from current time we can
        //figure out how much we have progressed towards next frame, and interpolate accordingly
        // eg. if very first frame is for scaling 2x, m_PrevFrameScale will
        //be the scale when object first came into timeline.
        //Now if second frame is to traslate, we will see where object was when prev frame came
        //(which was scale fram in this eg.), and compare the time at which it came with current time,
        // and time of next keyframe
        DataTypes::POINT3Dd m_PrevFramePos, m_PrevFrameScale, m_PrevFrameRotationVector;
        double m_PrevFrameRotationAngle;
        double m_PrevFrameTransparency;
        double m_PrevFrameTime;//time at which prev frame was processed - may not be same as time set in keyframe, because of certain delta

        std::shared_ptr<Timeline> m_timeline; //reference to timeline object which contains this animation object
        void TransformObject();

        std::string m_debugName;//Can give this animation object a name, helps in debugging

    public:

        //Can give this animation object a name, helps in debugging
        void SetDebugName(std::string name)
        {
            m_debugName = name;
        }

        void AddKeyFrame(KeyFrame frame);

        //constructor
        AnimationObject(std::shared_ptr<Timeline> timeline, std::shared_ptr<SceneObject> sceneObject, DataTypes::POINT3Dd spawnPos/*Where should the object spwan the first time it appears on screen*/, OglWindow *windowObject = nullptr);

        bool IsHidden()
        {
            return m_bIsHidden;
        }

        bool IsAlive()
        {
            return m_bIsAlive;
        }

        void MarkForDelete()
        {
            m_bIsAlive = false; //so that cleanup can be done, and this object be removed from list of render objects in when this object's state is queried
        }

        int Draw(double currentTime, double stepSize);
        double GetNextRotationAngle(const KeyFrame & nextFrame);
        DataTypes::POINT3Dd GetNextScale(const KeyFrame & nextFrame);
        DataTypes::POINT3Dd GetNextPos(const KeyFrame&);
    };

    class Timeline 
    {
        const double m_totalDuration, m_stepSize;
        double m_currentTime;
        std::vector<AnimationObject> m_objects;
        std::vector<AnimationObject> m_objectsBuff;//When a object being rendered adds new object to timmeline, it is better to store it in a different vector temporarily to avoid races

    public:
        void AddObject(AnimationObject object)
        {
            m_objectsBuff.push_back(object);//do not directly add to m_objects, since when advance() is iterating over m_objects, and new objects are added it would be a race condition
        }

        Timeline(double totalDuration, double stepSize = 0.01) :
            m_totalDuration(totalDuration), m_stepSize(stepSize), m_currentTime(0)
        {

        }

        double GetCurrentTimelineTime() const
        {
            return m_currentTime;
        }

        void SetCurrentTimelineTime(double time)
        {
            m_currentTime = time;
        }

        double GetStepSize() const
        {
            return m_stepSize;
        }

        int advance() 
        {
            if (m_currentTime > m_totalDuration)
                return -1;//end animation

            //Render each object
            for (AnimationObject& obj : m_objects)
            {
                obj.Draw(m_currentTime, m_stepSize);
                OutputDebugStringA(("CurrentTime=" + std::to_string(m_currentTime)).c_str());
                OutputDebugStringA("\n");
            }

            //Remove objects which are marked for deletion
            for (auto itr = m_objects.begin(); itr != m_objects.end();)
            {
                if (itr->IsAlive())
                {
                    itr++;
                }
                else
                {
                    itr = m_objects.erase(itr);
                }
            }

            //If there were new objects created during rendering, add them
            if (m_objectsBuff.size())
            {
                m_objects.insert(std::end(m_objects), std::begin(m_objectsBuff), std::end(m_objectsBuff));
                m_objectsBuff.clear();
            }

            m_currentTime += m_stepSize;
            return 0;
        }
    };

    class KeyFrame
    {
    public:
        typedef std::function<void(std::shared_ptr<SceneObject>, AnimationObjectState)> CALLBACKFUNC;

        enum class TIMETYPE
        {
            RELATIVETIME,
            ABSOLUTETIME
        };

        enum class ACTION
        {
            TRANSLATE_ABSOLUTE, //data will contain absolute final position at the key frame
            TRANSLATE_RELATIVE, // relative to current state of Animation Object
            ROTATE_ANGLE_ABSOLUTE,//rotate angle only, don't tween rotation vector
            ROTATE_ANGLE_RELATIVE,
            SCALE_ABSOLUTE,
            SCALE_RELATIVE,
            OBJECT_SHOW, //object appears on screen - must be rendered now - No tweening
            OBJECT_HIDE,  //object is hidden - don't render - no tweening
            OBJECT_KILL, //When Object is no longer to be shown remove it completely to save processing
            CHANGE_TRANAPARENCY,
            EXECUTE_ARBITRARY_CALLBACK //Execute a user provided function at the given time - can tween, in which case funtion will be exucuted for a time duration -  prototype specified by CALLBACKFUNC typedef
        };

        enum class TWEENTYPE
        {
            NONE,
            LINEAR
        };

    private:
        UINT m_index; //unique index for a given animation object
        double m_time; //time at which this keyframe is set for - set to 2 for eg. when Timeline::m_totalDuration is 10, and we want something to happen at 2

        TIMETYPE m_timeType;

        ACTION m_action;
        
#pragma region KEYFRAME ACTION SPECIFIC DATA
        //translate, rotate, scale specific data
        //Only x will be used in case of transparency
        //in case of rotation stores rotation vector
        DataTypes::POINT3Dd m_data = {};
        double m_rotationAngle;//if this keyframe represents rotation, then angle in radians
        CALLBACKFUNC m_callbackFunction; // for EXECUTE_ARBITRARY_CALLBACK -> the funcn will be called by timeline object
#pragma endregion

        TWEENTYPE m_tweenType;
        bool m_processed;//animation corresponding to the key frame has been done
        
    public:
        ///time at which the key frame action is supposed to occur
        double GetExecutionTime() const
        {
            return m_time;
        }

        auto GetData() const
        {
            return m_data;
        }

        auto GetRotationAngle() const
        {
            return m_rotationAngle;
        }

        ACTION GetActionType() const
        {
            return m_action;
        }

        TWEENTYPE GetTweenType() const
        {
            return m_tweenType;
        }

        //Callback function which is set for EXECUTE_ARBITRARY_CALLBACK action type
        CALLBACKFUNC GetCallback()
        {
            return m_callbackFunction;
        }

        KeyFrame(UINT index, ACTION action, double time, TIMETYPE timeType = TIMETYPE::ABSOLUTETIME, TWEENTYPE tweenType = TWEENTYPE::LINEAR, DataTypes::POINT3Dd data = DataTypes::POINT3Dd{}, double rotationAngle = 0, CALLBACKFUNC callbackFunc = {})
            : m_index(index), m_action(action), m_timeType(timeType), m_tweenType(tweenType), m_processed(false),
            m_time(time), m_data(data), m_rotationAngle(rotationAngle), m_callbackFunction(callbackFunc)
        {

        }
    };
}