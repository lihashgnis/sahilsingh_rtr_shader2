#include "stdafx.h"
#include "TextureSceneObject.h"
#include "../Logging/Logger.hpp"

CREATE_LOG()

using namespace util;

bool util::TextureSceneObject::AddTexture(UINT id, const char imageResourcePath[], Textures::TextureType type, Operation op)
{
    bool res = false;
    if (op == Operation::UNIQUE)
    {
        if (m_IdToTextureMap.find(id) != m_IdToTextureMap.end())
        {
            res = false;
            throw std::exception("Trying to overwrite in unique operation");
        }
    }
    GLuint tex;
    int width=0, height=0;
    if (!Textures::LoadTexture(&tex, imageResourcePath, type, &width, &height))
    {
        res = false;
        LOG(Error, "Texture error");
    }
    else
    {
        res = true;
        TextureSceneObject::ImageProps p;
        p.aspectRatio = double(width) / height;
        p.texture = tex;
        m_IdToTextureMap[id] = p;
    }
    return true;
}

TextureSceneObject::TextureSceneObject(UINT id, const char imageResourcePath[], Textures::TextureType type)
{
    m_currentImageID = -1;
    if (AddTexture(id, imageResourcePath, type, Operation::UNIQUE))
    {
        m_currentImageID = id;
    }
}

TextureSceneObject::~TextureSceneObject()
{
}

void TextureSceneObject::SetCellHeight(GLfloat height)
{
    m_cellSize.cy = height;
    m_cellSize.cx = 0;//will be taken from image props while drawing
}

//Since same texture scene object can have different images, hence widths may be different for them

GLfloat util::TextureSceneObject::GetCurrentImageCellWidth()
{
    if (m_currentImageID < 0)
    {
        throw std::exception("Texture ID not initialized to a valid value");
        return -1;//intentional - in case someone decides to remove the throw above
    }

    auto itr = m_IdToTextureMap.find(m_currentImageID);
    if (m_IdToTextureMap.end() == itr)
    {
        throw std::exception("Texture ID not found in map");
        return -1;
    }
    m_cellSize.cx = itr->second.aspectRatio * m_cellSize.cy;
    return m_cellSize.cx;
}

void TextureSceneObject::Draw(AnimationObjectState & state)
{
    if (m_currentImageID < 0)
    {
        throw std::exception("Texture ID not initialized to a valid value");
        return;//intentional - in case someone decides to remove the throw above
    }
    
    auto itr = m_IdToTextureMap.find(m_currentImageID);
    if (m_IdToTextureMap.end() == itr)
    {
        throw std::exception("Texture ID not found in map");
        return;
    }
    m_cellSize.cx = itr->second.aspectRatio * m_cellSize.cy;

    glBindTexture(GL_TEXTURE_2D, itr->second.texture);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

    glBegin(GL_QUADS);
    glColor4f(1, 1, 1, 0);
    glTexCoord2f(0, 1);
    glVertex2f(0, 0);
    glTexCoord2f(0, 0);
    glVertex2f(0, -m_cellSize.cy);
    glTexCoord2f(1, 0);
    glVertex2f(m_cellSize.cx, -m_cellSize.cy);
    glTexCoord2f(1, 1);
    glVertex2f(m_cellSize.cx, 0);
    glEnd();
}

void TextureSceneObject::AdvanceTime()
{
    THROW_NOT_IMPLEMENTED
}
