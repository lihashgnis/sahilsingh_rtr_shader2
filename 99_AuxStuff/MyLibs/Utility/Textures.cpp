#include "stdafx.h"
#include "Textures.h"
#include <SOIL/SOIL.h>
#include "../Logging/Logger.hpp"

using namespace util;

CREATE_LOG()


std::map<std::string, GLuint> Textures::m_textureCache;

bool Textures::LoadTexture(GLuint *texture, const char imageResourcePath[], TextureType type, int* pWidth, int* pHeight)
{
    bool bStatus = false;
    int width, height;
    auto it = m_textureCache.find(std::string(imageResourcePath));
    if(m_textureCache.end() != it)
    {
        *texture = it->second;
        bStatus = true;
        return bStatus;
    }

    auto image_data = SOIL_load_image(imageResourcePath, &width, &height, 0, SOIL_LOAD_AUTO);

    if (image_data != NULL)
    {
        bStatus = true;
        if (nullptr != pWidth) { *pWidth = width; }
        if (nullptr != pHeight) { *pHeight = height; }

        //Now we have image data in bmp
        glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
        glGenTextures(1, texture);
        glBindTexture(GL_TEXTURE_2D, *texture);
        /*setting clamping as default for all. One will have to call this function explicitly when using this texture to get a different brhaviour.*/
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

        //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

        GLint components = 0;
        GLenum format = 0;
        switch (type)
        {
        case util::Textures::TextureType::BMP:
            format = GL_BGR_EXT;
            components = 3;
            break;
        case util::Textures::TextureType::PNG:
            format = GL_RGBA;
            components = 4;
            break;
        default:
            break;
        }

        gluBuild2DMipmaps(GL_TEXTURE_2D, components, width, height, format, GL_UNSIGNED_BYTE, image_data);

        SOIL_free_image_data(image_data);
        m_textureCache[std::string(imageResourcePath)] = *texture;
    }
    else
    {//ERROR
        LOG(Error, "SOIL_load_image error");
    }
    return bStatus;
}