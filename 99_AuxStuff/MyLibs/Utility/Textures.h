#pragma once
#include "Util.hpp"
#include <gl/GL.h>
#include <gl/GLU.h>
#include <map>
#include <string>

namespace util {
    class Textures
    {
        //TODO: Implement texture freeing up in such a scenario - may have to keep reference count
        static std::map<std::string, GLuint> m_textureCache;
    public:
        Textures() = delete;
        ~Textures() = delete;
        enum class TextureType {
            BMP,
            PNG
        };
        /*
        Optional parameters - width, and height - return width, and height, if user has specified these parameters
        */
#ifdef _WIN32
        static bool LoadTexture(GLuint *texture, const char imageResourcePath[], TextureType type, int* pWidth = nullptr, int* pHeight = nullptr);
#elif __linux__
#endif
    };
}
