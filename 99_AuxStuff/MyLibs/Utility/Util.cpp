﻿#include "stdafx.h"
#include "Util.hpp"
#include <gl/glew.h>
#include <gl/GL.h>

#pragma comment (lib, "OpenGl32.lib")

#ifndef _PROGRAMMABLE_PIPELINE_
#pragma comment (lib, "glu32.lib")
#endif

#include "..\Logging\Logger.hpp"
#include <exception>

CREATE_LOG();

//TODO: remove this when log lib starts working
#define LOG(x,y) if(LogLevel::Error == x){MessageBoxA(NULL, "", "", MB_OK);}

using namespace util;

void Utility::InitWndClassEx(WNDCLASSEX & wc, TCHAR * AppName, WNDPROC wp, HINSTANCE hInstance)
{
	wc = {};
	wc.cbSize = sizeof(wc);
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.lpfnWndProc = wp;
	wc.hInstance = hInstance;
	wc.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wc.lpszClassName = AppName;
	wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wc.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	
	if (!RegisterClassEx(&wc))
	{
		LOG(LogLevel::Error, "RegisterClassfailed");
	}
}

HWND Utility::CreateOglWindow(TCHAR * className, UINT x, UINT y, UINT width, UINT height, HINSTANCE hInstance)
{
	return CreateWindowEx(WS_EX_APPWINDOW, className, className, WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, x, y, width, height, NULL, NULL, hInstance, NULL);
}

OglWindow::~OglWindow()
{
}

void util::OglWindow::ToggleFullscreeen()
{
    ToggleFullscreeen(m_hwnd);
}

bool util::OglWindow::IsFullScreen()
{
    return m_bIsFullScreen;
}

void OglWindow::EnableAlpha(bool bEnable)
{
    if (bEnable)
    {
        //TODO: LOG that transparency is not fully supported, and may lead to artifacts - alphabets created for dynamic India assignment were created by keeping no transparency in mind
        //The freetype support added later for text rendering supports transparency
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    }
    else
    {
        glDisable(GL_BLEND);
    }
}

OglWindow::OglWindow(TCHAR * className, UINT x, UINT y, UINT width, UINT height, HINSTANCE hInstance)
{
	m_bIsFullScreen = false;
    m_bIsPaused = false;
	m_bActiveWindow = false;
    m_bCapFramerate = false;
    m_bAllowOutOfFocusrender = false;
    m_dwInterFrameTimeMilliSecond = 0;
    m_dwPrevEpoch = 0;
	m_width = width;
	m_height = height;
	Utility::InitWndClassEx(m_wndClassEx, className, OglWindow::OglWndProc, hInstance);
	m_hwnd = Utility::CreateOglWindow(className, x, y, m_width, m_height, hInstance);

	SetProp(m_hwnd, PARENT_OBJECT, this);
    EnableAlpha(false);//Alpha disabled by default
}

void OglWindow::InitAndShowWindow(UINT mask)
{
	int iRet = Initialize(mask);

	const char *str = nullptr;
	if (iRet < 0)
	{
		switch (iRet)
		{
		case -1:
			str = "choose pixel failed";
			break;
		case -2:
			str = "Set pixel format failed";
			break;
		case -3:
			str = "wglCreateContext failed";
			break;
		case -4:
			str = "wglMakeCurrent failed";
			break;
        case -5:
            str = "glewInit failed";
            break;
		default:
			break;
		}
        LOG(LogLevel::FatalError, str);
        Uninitialize();
        exit(1);
	}
	else
	{
		str = "Initialize successful";
	}

	LOG(LogLevel::Debug, str);

	ShowWindow(m_hwnd, SW_SHOWNORMAL);
	SetForegroundWindow(m_hwnd);
	SetFocus(m_hwnd);
}

int OglWindow::Initialize(UINT mask)
{
	PIXELFORMATDESCRIPTOR pfd;
	ZeroMemory((void*)&pfd, NULL, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(pfd);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

    if (mask & INIT_MASK_ENABLE_DEPTH)
    {
        pfd.cDepthBits = 32;
    }

    if (mask & INIT_MASK_ENABLE_STENCIL)
    {
        pfd.cStencilBits = 8;
    }

	m_Hdc = GetDC(m_hwnd);
	auto iPixelFormatIndex = ChoosePixelFormat(m_Hdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		return -1;
	}

	if (SetPixelFormat(m_Hdc, iPixelFormatIndex, &pfd) == false)
	{
		return -2;
	}

	m_Hrc = wglCreateContext(m_Hdc);//create rendering context

	if (m_Hrc == NULL)
	{
		return -3;
	}

	if (wglMakeCurrent(m_Hdc, m_Hrc) == false)//associate the rendering context to current thread
	{
		return -4;
	}

    if (mask & INIT_MASK_ENABLE_PROGRAMMABLE_PIPELINE)//TODO: test code path - both FFP, PP
    {
        if (GLEW_OK != glewInit())
        {
            return -5;
        }
    }
	
    if (mask & INIT_MASK_ENABLE_DEPTH)
    {
        glClearDepth(1.0);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        if (~mask & INIT_MASK_ENABLE_PROGRAMMABLE_PIPELINE)//TODO: test code path - both FFP, PP
        {
            glShadeModel(GL_SMOOTH);
        }
    }
    
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	Resize(m_width, m_height);

    if (mask & INIT_MASK_ENABLE_DEPTH)
    {
        if (~mask & INIT_MASK_ENABLE_PROGRAMMABLE_PIPELINE)//TODO: test code path - both FFP, PP
        {
            glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
        }
    }

    if (mask & INIT_MASK_ENABLE_TEXTURE_2D)
    {
        glEnable(GL_TEXTURE_2D);
    }

	return 0;
}

double OglWindow::GetMinX()
{
    LOG(LogLevel::Warning, L"Depricated");
    return m_minX;
}

double OglWindow::GetMaxX()
{
    LOG(LogLevel::Warning, L"Depricated");
    return m_maxX;
}

double OglWindow::GetMinY()
{
    LOG(LogLevel::Warning, L"Depricated");
    return m_minY;
}

double OglWindow::GetMaxY()
{
    LOG(LogLevel::Warning, L"Depricated");
    return m_maxY;
}

void OglWindow::CalcExtents()
{
    GLint viewport[4];
    glGetIntegerv(GL_VIEWPORT, viewport);

    GLfloat width  = viewport[2];
    GLfloat height = viewport[3];

    if (width <= height)
    {
        auto factor = 1.0f*(((GLfloat)height) / ((GLfloat)width));
        m_minX = -1.0;
        m_maxX = 1.0;
        m_minY = -factor;
        m_maxY = factor;
        m_minZ = -1.0;
        m_maxZ = 1.0;
    }
    else
    {
        auto factor = 1.0f*(((GLfloat)width) / ((GLfloat)height));
        m_minX = -factor;
        m_maxX = factor;
        m_minY = -1.0;
        m_maxY = 1.0f;
        m_minZ = -1.0;
        m_maxZ = 1.0;
    }
}

double OglWindow::GetMinX2()
{
    CalcExtents();
    return m_minX;
}

double OglWindow::GetMaxX2()
{
    CalcExtents();
    return m_maxX;
}

double OglWindow::GetMinY2()
{
    CalcExtents();
    return m_minY;
}

double OglWindow::GetMaxY2()
{
    CalcExtents();
    return m_maxY;
}

double util::OglWindow::GetWindowHeightNDC()
{
    return std::fabs(GetMaxY2() - GetMinY2());
}

double util::OglWindow::GetWindowWidthNDC()
{
    return std::fabs(GetMaxX2() - GetMinX2());
}

double OglWindow::PixelsToNDC(int pixels)
{
    GLint viewport[4];
    glGetIntegerv(GL_VIEWPORT, viewport);
    CalcExtents();
    return ((m_maxX - m_minX) / viewport[3])*pixels;
}

int OglWindow::NDCToPixels(double NDCcoord)
{
    GLint viewport[4];
    glGetIntegerv(GL_VIEWPORT, viewport);
    CalcExtents();
    return (viewport[3] / (m_maxX - m_minX))*NDCcoord;
}

void util::OglWindow::SetClearColor(COLORREF rgba)
{
    glClearColor(GetRValuef(rgba), GetGValuef(rgba), GetBValuef(rgba), GetAValuef(rgba));
}

void OglWindow::Uninitialize()
{
	if (m_bIsFullScreen)
	{
		ToggleFullscreeen(m_hwnd);
	}

	if (wglGetCurrentContext() == m_Hrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (m_Hrc)
	{
		wglDeleteContext(m_Hrc);
		m_Hrc = NULL;
	}

	if (m_Hdc)
	{
		ReleaseDC(m_hwnd, m_Hdc);
		m_Hdc = NULL;
	}

	RemoveProp(m_hwnd, PARENT_OBJECT);
}

void OglWindow::ToggleFullscreeen(HWND hwnd)
{
	static WINDOWPLACEMENT wpPrev;
	DWORD dwStyle = 0;

	if (m_bIsFullScreen == false)
	{
		dwStyle = GetWindowLong(hwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			MONITORINFO mi = {};
			mi.cbSize = sizeof(mi);

			if (GetWindowPlacement(hwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(hwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(hwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(hwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
				ShowCursor(FALSE);
				m_bIsFullScreen = true;
			}
		}
	}
	else
	{
		SetWindowLong(hwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(hwnd, &wpPrev);
		SetWindowPos(hwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
		m_bIsFullScreen = FALSE;
	}
}

void OglWindow::TogglePause()
{
    m_bIsPaused = !m_bIsPaused;
}

void OglWindow::RunGameLoop()
{
	// Game loop

	bool bDone = false;
	MSG msg;

	while (bDone == false)
	{
		if (PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (m_bAllowOutOfFocusrender || (m_bActiveWindow == true))
			{
                DWORD time = 0;
                bool bExecDisplayFunc = true;
                if (m_bCapFramerate)
                {//TODO: log framerate capping in effect
                    bExecDisplayFunc = false;
                    time = timeGetTime();
                    DWORD delta = 0;
                    if (m_dwPrevEpoch > time)//timeGetTime() rolls over every 49 days
                    {
                        THROW_NOT_IMPLEMENTED//Low probability hence not implementing right now. Will do so when this exception is thrown,if ever.
                    }
                    else
                    {
                        delta = time - m_dwPrevEpoch;
                    }

                    if (delta >= m_dwInterFrameTimeMilliSecond)
                    {
                        bExecDisplayFunc = true;
                        m_dwPrevEpoch = time;
                    }
                }

                if (m_bIsPaused)
                {
                    bExecDisplayFunc = false;
                }

                if (bExecDisplayFunc)
                {
                    Display();
                }
                Update();
			}
			else
			{

			}
		}
	}
}

HWND OglWindow::GetHwnd()
{
	return m_hwnd;
}



LRESULT CALLBACK OglWindow::OglWndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	auto that = (OglWindow*)GetProp(hwnd, PARENT_OBJECT);
	
    LRESULT lr = 0;
    if (that)
    {
        lr = that->WndProcPre(hwnd, iMsg, wParam, lParam);
    }

    if (lr < 0)//if wndproc2 returned -ve => user of this lib doesn't want further special processing
    {
        return DefWindowProc(hwnd, iMsg, wParam, lParam);
    }

	switch (iMsg)
	{
    case WM_KEYDOWN:
    {
        if (VK_ESCAPE == wParam)
        {
            if (that->m_bIsFullScreen)
            {
                that->ToggleFullscreeen();
            }
            //DestroyWindow(hwnd);
        }
    }
    break;
	case WM_SETFOCUS:
		that->m_bActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		that->m_bActiveWindow = false;
		break;
	case WM_SIZE:
		that->Resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_ERASEBKGND://dont want this since defwindow proc will post WM_PAINT after processing this
		return 0;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_CHAR:
		switch (wParam)
		{
		case 'f':
			//[[fallthrough]]
		case 'F':
		{
			that->ToggleFullscreeen(hwnd);
		}
		break;
        /*Can pause by pressing P,p, or space bar*/
        case 'p':
            //[[fallthrough]]
        case 'P':
            //[[fallthrough]]
        case ' ':
            that->TogglePause();
            break;
        case 'a':
            glEnable(GL_POINT_SMOOTH);
            glEnable(GL_LINE_SMOOTH);
            glEnable(GL_POLYGON_SMOOTH);
            break;
        case 'd':
            glDisable(GL_POINT_SMOOTH);
            glDisable(GL_LINE_SMOOTH);
            glDisable(GL_POLYGON_SMOOTH);
            break;
		default:
			break;
		}
		break;
	case WM_DESTROY:
		that->Uninitialize();
		PostQuitMessage(0);
		break;
	default:
		break;
	}

    if (that)
    {
        that->WndProcPost(hwnd, iMsg, wParam, lParam);
    }

	return DefWindowProc(hwnd, iMsg, wParam, lParam);
}

void DrawShapes::DrawRectangleLines(double left, double bottom, double right, double top, COLORREF color)
{
    glEnable(GL_LINE_SMOOTH);
    glBegin(GL_LINE_LOOP);
    glColor3f(GetRValuef(color), GetGValuef(color), GetBValuef(color));
    glVertex2f(left, bottom);
    glVertex2f(right, bottom);
    glVertex2f(right, top);
    glVertex2f(left, top);
    glEnd();
}

void DrawShapes::DrawTriangleLines(DataTypes::POINTd p1, DataTypes::POINTd p2, DataTypes::POINTd p3, COLORREF color)
{
    glBegin(GL_LINE_LOOP);
    glColor3f(GetRValuef(color), GetGValuef(color), GetBValuef(color));
    glVertex2f(p1.x, p1.y);
    glVertex2f(p2.x, p2.y);
    glVertex2f(p3.x, p3.y);
    glEnd();
}

void DrawShapes::DrawTriangleSolid(DataTypes::POINTd p1, DataTypes::POINTd p2, DataTypes::POINTd p3, COLORREF color)
{
    glBegin(GL_TRIANGLES);
    glColor3f(GetRValuef(color), GetGValuef(color), GetBValuef(color));
    glVertex2f(p1.x, p1.y);
    glVertex2f(p2.x, p2.y);
    glVertex2f(p3.x, p3.y);
    glEnd();
}

void DrawShapes::DrawEqTriangleByIncentre(DataTypes::Centre centre, DataTypes::Radius radius, COLORREF color)
{
    static const double PI = 4 * atan(1);

    DataTypes::POINTd P1 = {centre.x, centre.y + radius/sin(PI/6)};
    DataTypes::POINTd P2 = {centre.x - radius/tan(PI/6), centre.y - radius};
    DataTypes::POINTd P3 = {centre.x + radius/tan(PI/6), centre.y - radius};
    DrawTriangleLines(P1, P2, P3, color);
}

std::tuple<DataTypes::Centre, DataTypes::Radius> util::DrawShapes::GetTriangleIncentre(DataTypes::POINTd p1, DataTypes::POINTd p2, DataTypes::POINTd p3)
{
    double a = pow(pow((p2.x - p3.x), 2) + pow((p2.y - p3.y), 2), 0.5);
    double b = pow(pow((p1.x - p3.x), 2) + pow((p1.y - p3.y), 2), 0.5);
    double c = pow(pow((p2.x - p1.x), 2) + pow((p2.y - p1.y), 2), 0.5);
    DataTypes::POINTd inCentre = {};
    inCentre.x = (a*p1.x + b * p2.x + c * p3.x) / (a + b + c);
    inCentre.y = (a*p1.y + b * p2.y + c * p3.y) / (a + b + c);

    double s = (a + b + c) / 2;
    double area = pow(s * (s - a)*(s - b)*(s - c), 0.5);
    double inRadius = (2 * area) / (a + b + c);
    return {inCentre, inRadius};
}

void DrawShapes::DrawCircleLines(UINT NUM_PTS, double RADIUS, COLORREF color)
{
    static double PI = 4 * atan(1);
    const double theta_delta = (2.0*PI) / NUM_PTS;
    glBegin(GL_LINE_LOOP);
    glColor3f(GetRValuef(color), GetGValuef(color), GetBValuef(color));
    double x, y;
    double theta = 0.0;
    for (int i = 1; i <= NUM_PTS; i++)
    {
        x = RADIUS * cos(theta); y = RADIUS * sin(theta);
        theta += theta_delta;
        theta = (theta >= 2.0 * PI) ? (2.0*PI) / NUM_PTS : theta;
        glVertex2f(x, y);
    }
    glEnd();
}


void DrawShapes::DrawCircleArcTriangleFan(double RADIUS, double startAngle, double endAngle, COLORREF centreColor, COLORREF boundaryColor, UINT NUM_PTS)
{
    static double PI = 4 * atan(1);
    const double theta_delta = std::fabs(endAngle - startAngle) / NUM_PTS;
    glBegin(GL_TRIANGLE_FAN);
    double x = 0, y = 0;
    double theta = startAngle;

    glColor3f(GetRValuef(centreColor), GetGValuef(centreColor), GetBValuef(centreColor));
    glVertex2f(x, y);
    
    glColor3f(GetRValuef(boundaryColor), GetGValuef(boundaryColor), GetBValuef(boundaryColor));

    for (int i = 0; i < NUM_PTS; i++)
    {
        x = RADIUS * cos(theta); y = RADIUS * sin(theta);
        glVertex2f(x, y);
        theta += theta_delta;
        theta = (theta >= 2.0 * PI) ? theta - 2.0 * PI : theta;
        theta = (theta <= -2.0 * PI) ? theta + 2.0 * PI : theta;
    }

    if (theta <= endAngle)
    {
        x = RADIUS * cos(endAngle); y = RADIUS * sin(endAngle);
        glVertex2f(x, y);
    }
    glEnd();
}

auto DrawShapes::InterpolateColor(double val, double max_val/*Corresponding to col1*/, double min_val/*Corresponding to col2*/, COLORREF col1, COLORREF col2)
{

    auto r1 = GetRValue(col1);
    auto g1 = GetGValue(col1);
    auto b1 = GetBValue(col1);
    auto a1 = GetAValue(col1);

    auto r2 = GetRValue(col2);
    auto g2 = GetGValue(col2);
    auto b2 = GetBValue(col2);
    auto a2 = GetAValue(col2);

    auto w1 = std::fabs(max_val - val);
    auto w2 = std::fabs(val - min_val);

    decltype(r1) r3 = (decltype(r1))((w2 * r1 + w1 * r2) / (w1 + w2));
    decltype(g1) g3 = (decltype(g1))((w2 * g1 + w1 * g2) / (w1 + w2));
    decltype(b1) b3 = (decltype(b1))((w2 * b1 + w1 * b2) / (w1 + w2));
    decltype(a1) a3 = (decltype(a1))((w2 * a1 + w1 * a2) / (w1 + w2));

    return RGBA(r3, g3, b3, a3);
}

void DrawShapes::DrawHollowCircleTriangleStrip(double innerRadius, double outerRadius, double startAngle, double endAngle, COLORREF topColor, COLORREF bottomColor, UINT NUM_PTS)
{
    double x = 0, y = 0;
    static double PI = 4 * atan(1);
    const double theta_delta = std::fabs(endAngle - startAngle) / NUM_PTS;

    double max_y = outerRadius * sin(PI / 2.0);
    double min_y = outerRadius * sin(3.0*(PI / 2.0));

    auto yGradColor = [topColor, bottomColor, max_y, min_y](double y) -> auto
    {
        return InterpolateColor(y, max_y, min_y, topColor, bottomColor);
    };

    double theta = startAngle;
    double r = 0, g = 0, b = 0, a;
    glBegin(GL_TRIANGLE_STRIP);
    for (int i = 0; i < NUM_PTS; i++)
    {
        x = innerRadius * cos(theta); y = innerRadius * sin(theta);
        auto col = yGradColor(y);
        r = GetRValuef(col); g = GetGValuef(col); b = GetBValuef(col); a = GetAValuef(col);
        glColor4f(r, g, b, a);
        glVertex2f(x, y);

        x = outerRadius * cos(theta); y = outerRadius * sin(theta);
        col = yGradColor(y);
        r = GetRValuef(col); g = GetGValuef(col); b = GetBValuef(col); a = GetAValuef(col);
        glColor4f(r, g, b, a);
        glVertex2f(x, y);

        theta += theta_delta;
    }

    {
        x = innerRadius * cos(theta); y = innerRadius * sin(theta);
        auto col = yGradColor(y);
        r = GetRValuef(col); g = GetGValuef(col); b = GetBValuef(col); a = GetAValuef(col);
        glColor4f(r, g, b, a);
        glVertex2f(x, y);

        x = outerRadius * cos(theta); y = outerRadius * sin(theta);
        col = yGradColor(y);
        r = GetRValuef(col); g = GetGValuef(col); b = GetBValuef(col); a = GetAValuef(col);
        glColor4f(r, g, b, a);
        glVertex2f(x, y);
    }
    glEnd();
}

void DrawShapes::DrawGraph(float factor) 
{

    {
        float abscissa = 0;
        float ordinate = 0;

        for (float x = -1.0; x <= 1.0; x += factor)
        {
            if (x > (-factor / 2) && x < (factor / 2))
            {
                glLineWidth(3.0f);
                abscissa = x;
            }
            else
            {
                glLineWidth(1.0f);
            }

            glBegin(GL_LINES);

            if (x > (-factor / 2) && x < (factor / 2))
            {
                glColor3f(0.0f, 1.0f, 0.0f);
            }
            else
            {
                glColor3f(1.0f, 1.0f, 1.0f);
            }

            glVertex2f(x, -1);
            glVertex2f(x, 1);
            glEnd();
        }

        for (float y = -1.0; y <= 1.0; y += factor)
        {
            if (y > (-factor / 2) && y < (factor / 2))
            {
                glLineWidth(3.0f);
                ordinate = y;
            }
            else
            {
                glLineWidth(1.0f);
            }

            glBegin(GL_LINES);

            if (y > (-factor / 2) && y < (factor / 2))
            {
                glColor3f(1.0f, 0.0f, 0.0f);
            }
            else
            {
                glColor3f(1.0f, 1.0f, 1.0f);
            }

            glVertex2f(-1, y);
            glVertex2f(1, y);
            glEnd();
        }

        glLineWidth(3.0f);

        //Y-axis
        glBegin(GL_LINES);
        glColor3f(0.0f, 1.0f, 0.0f);
        glVertex2f(abscissa, -1);
        glVertex2f(abscissa, 1);
        glEnd();

        //X-axis
        glBegin(GL_LINES);
        glColor3f(1.0f, 0.0f, 0.0f);
        glVertex2f(-1, ordinate);
        glVertex2f(1, ordinate);
        glEnd();

        //origin
        glPointSize(3.0f);
        glBegin(GL_POINTS);
        glColor3f(1.0f, 1.0f, 0.0);
        glVertex2f(abscissa, ordinate);
        glEnd();
    }
}

util::ColorWheel::ColorWheel(VLONG numPartitions) : num_partitions(numPartitions), PI(4 * atan(1)), PI2(8 * atan(1))
{
    radius = max_cols_range / PI2;
}

std::tuple<ColorWheel::RED, ColorWheel::GREEN, ColorWheel::BLUE> ColorWheel::MapIndexToColor(VLONG index)//index starting at 0
{
    double theta = ((PI2) / num_partitions)*index;

    if (theta >= PI2)
    {
        theta = 0;
    }

    BLUE b = 0;
    GREEN g = 0;
    RED r = 0;

    if (theta >= 0 && theta < PI2 / 3)//RED-GREEN quadrant
    {
        r = radius * cos(theta);
        g = radius * sin(theta) * cos(PI / 6);
    }
    else if (theta < 2 * (PI2 / 3))//GREEN-BLUE quadrant
    {
        double phi = theta - PI2 / 3;
        double alpha = 2 * (PI2 / 3) - theta;

        g = radius * cos(phi);
        b = radius * cos(alpha);
    }
    else //BLUE-RED quadrant
    {
        double phi = PI2 - theta;
        r = radius * cos(phi);
        b = radius * sin(phi) * cos(PI / 6);
    }

    VLONG color = min_cols_range + (((double)(max_cols_range - min_cols_range)) / num_partitions)*index;

    return { r,g,b };
}

void util::TextHelper::Alphabet::Draw(AnimationObjectState& state)
{
    GLint mMode;
    glGetIntegerv(GL_MATRIX_MODE, &mMode);

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    
    glTranslatef(m_position.x, m_position.y, m_position.z);

    switch (m_char)
    {
    case L'A':
        DrawA(); break;
    case L'D':
        DrawD(); break;
    case L'F':
        DrawF(); break;
    case L'I':
        DrawI(); break;
    case L'N':
        DrawN(); break;
#if SUPPORT_UNICODE_LIB
    case L'Λ':
        DrawΛ(); break;
    case L'א':
        Drawא(); break;
    case L'ב':
        Drawב(state); break;
    case L'ג':
        Drawג(state); break;
    case L'ד':
        Drawד(state); break;
    case L'ה':
        Drawה(state); break;
    case L'ו':
        Drawו(state); break;
#endif
    default:
        THROW_NOT_IMPLEMENTED;
        break;
    }
    glPopMatrix();
    glMatrixMode(mMode);
}

void util::TextHelper::Alphabet::SetFontHeight(double height)
{
    m_cellSize = { m_monospaceRatio*height, height };
}

void util::TextHelper::Alphabet::DrawA()
{
    throw std::exception("Not implemented");
}

void util::TextHelper::Alphabet::DrawF()
{
    glTranslatef(0, -m_cellSize.cy, 0);
    glBegin(GL_QUADS);
        //left quad
        glColor4f(GetRValuef(m_bottom), GetGValuef(m_bottom), GetBValuef(m_bottom), BYTE2FLOAT(m_alpha)*GetAValuef(m_bottom));
        glVertex2f(0, 0);
        glVertex2f(0.2487*m_cellSize.cx, 0);
        glColor4f(GetRValuef(m_top), GetGValuef(m_top), GetBValuef(m_top), BYTE2FLOAT(m_alpha)*GetAValuef(m_top));
        glVertex2f(0.2487*m_cellSize.cx, 1*m_cellSize.cy);
        glVertex2f(0, 1*m_cellSize.cy);

        //right top quad
        glColor4f(GetRValuef(m_top), GetGValuef(m_top), GetBValuef(m_top), BYTE2FLOAT(m_alpha)*GetAValuef(m_top));
        glVertex2f(0.2487*m_cellSize.cx, 1 * m_cellSize.cy);
        auto yPos = 0.8840*m_cellSize.cy;
        auto rgba = DrawShapes::InterpolateColor(yPos, m_cellSize.cy, 0, m_top, m_bottom);
        glColor4f(GetRValuef(rgba), GetGValuef(rgba), GetBValuef(rgba), BYTE2FLOAT(m_alpha)*GetAValuef(rgba));
        glVertex2f(0.2487*m_cellSize.cx, yPos);
        glVertex2f(1*m_cellSize.cx, yPos);
        glColor4f(GetRValuef(m_top), GetGValuef(m_top), GetBValuef(m_top), BYTE2FLOAT(m_alpha)*GetAValuef(m_top));
        glVertex2f(1*m_cellSize.cx, 1*m_cellSize.cy);
        
        //right bottom quad
        yPos = 0.5594*m_cellSize.cy;
        rgba = DrawShapes::InterpolateColor(yPos, m_cellSize.cy, 0, m_top, m_bottom);
        glColor4f(GetRValuef(rgba), GetGValuef(rgba), GetBValuef(rgba), BYTE2FLOAT(m_alpha)*GetAValuef(rgba));
        glVertex2f(0.95989*m_cellSize.cx, yPos);
        glVertex2f(0.2487*m_cellSize.cx, yPos);
        yPos = 0.4463*m_cellSize.cy;
        rgba = DrawShapes::InterpolateColor(yPos, m_cellSize.cy, 0, m_top, m_bottom);
        glColor4f(GetRValuef(rgba), GetGValuef(rgba), GetBValuef(rgba), BYTE2FLOAT(m_alpha)*GetAValuef(rgba));
        glVertex2f(0.2487*m_cellSize.cx, yPos);
        glVertex2f(0.95989*m_cellSize.cx, yPos);

    glEnd();
}

void util::TextHelper::Alphabet::DrawD()
{
    static double PI = 4 * atan(1);

    SolidFillCellWithBackground();

    //DataTypes::POINTd c;//centre of circle
    //c.x = (1 - 0.6948)*m_cellSize.cx;
    //c.y = - 0.5017*m_cellSize.cy;
    
    DataTypes::POINTd p1, p2, p3, p4;
    p1 = {};
    p2.x = 0;
    p2.y = -m_cellSize.cy;
    //p3.x = 0.46*m_cellSize.cx;
    p3.x = 0.311*m_cellSize.cx;
    p3.y = p2.y;
    p4.x = p3.x;
    p4.y = 0;
    DataTypes::POINTd c = { p3.x, (p3.y + p4.y) / 2.0 };

    double innerRadius = 0.5352*m_cellSize.cx;
    double outerRadius = 0.6948*m_cellSize.cx;

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glTranslatef(c.x, c.y, 0);
    COLORREF top = m_top, bottom = m_bottom;

    try 
    {
        DrawShapes::DrawHollowCircleTriangleStrip(innerRadius, outerRadius, 1.5*PI, 0.5*PI, RBG2RGBA(top, m_alpha), RBG2RGBA(bottom, m_alpha));
    }
    catch (std::exception ex)
    {
        auto r = ex.what();
        r;
    }

    glPopMatrix();

    glBegin(GL_QUADS);
    glColor4f(GetRValuef(m_top), GetGValuef(m_top), GetBValuef(m_top), BYTE2FLOAT(m_alpha)*GetAValuef(m_top));
    glVertex2f(p1.x, p1.y);

    glColor4f(GetRValuef(m_bottom), GetGValuef(m_bottom), GetBValuef(m_bottom), BYTE2FLOAT(m_alpha)*GetAValuef(m_bottom));
    glVertex2f(p2.x, p2.y);
    glVertex2f(p3.x, p3.y);

    glColor4f(GetRValuef(m_top), GetGValuef(m_top), GetBValuef(m_top), BYTE2FLOAT(m_alpha)*GetAValuef(m_top));
    glVertex2f(p4.x, p4.y);

    glColor4f(GetRValuef(m_backGround), GetGValuef(m_backGround), GetBValuef(m_backGround), BYTE2FLOAT(m_alpha)*GetAValuef(m_backGround));
    
    p1.x = 0.1878*m_cellSize.cx;
    p1.y = -0.1152*m_cellSize.cy;
    p2.x = p1.x;
    p2.y = -m_cellSize.cy - p1.y;
    //p3.x = 0.46*m_cellSize.cx;
    p3.x = 0.311*m_cellSize.cx;
    p3.y = p2.y;
    p4.x = p3.x;
    p4.y = p1.y;

    glVertex2f(p1.x, p1.y);
    glVertex2f(p2.x, p2.y);
    glVertex2f(p3.x, p3.y);
    glVertex2f(p4.x, p4.y);
    glEnd();
}

void util::TextHelper::Alphabet::DrawI()
{
    
    auto xDelta = m_cellSize.cx / 3.0;
    auto yDelta = m_cellSize.cy / 4.0;
    double x = 0, y = 0, z = 0;

    GradientFillCellH();

    glBegin(GL_QUADS);

    //stencil it without using stencil buffers
    x = y = z = 0;
    glColor3f(GetRValuef(m_backGround), GetGValuef(m_backGround), GetBValuef(m_backGround));
    
    //left flank
    y -= yDelta;
    glVertex3f(x, y, z);
    y -= 2 * yDelta;
    glVertex3f(x, y, z);
    x += xDelta;
    glVertex3f(x, y, z);
    y += 2 * yDelta;
    glVertex3f(x, y, z);

    //right flank
    x = y = z = 0;
    x += 2 * xDelta;
    y -= yDelta;
    glVertex3f(x, y, z);
    y -= 2 * yDelta;
    glVertex3f(x, y, z);
    x += xDelta;
    glVertex3f(x, y, z);
    y += 2 * yDelta;
    glVertex3f(x, y, z);
    glEnd();
}

void util::TextHelper::Alphabet::DrawN()
{
    GradientFillCellH();

    DataTypes::POINTd p1, p2, p3;
    //stencil it without using stencil buffers
    //right triangle
    p1.x = 0.31818 * m_cellSize.cx;
    p1.y = 0;

    p2.x = (1 - 0.23776)*m_cellSize.cx;
    p2.y = -0.7132*m_cellSize.cy;

    p3.x = p2.x;
    p3.y = 0;

    DrawShapes::DrawTriangleSolid(p1, p2, p3, m_backGround);

    //left triangle
    p1.x = 0.23776 * m_cellSize.cx;
    p1.y = -m_cellSize.cy;

    p2.x = (1 - 0.31818)*m_cellSize.cx;
    p2.y = p1.y;

    p3.x = p1.x;
    p3.y = -(1 - 0.7132) * m_cellSize.cy;
    DrawShapes::DrawTriangleSolid(p1, p2, p3, m_backGround);
}

#if SUPPORT_UNICODE_LIB
void util::TextHelper::Alphabet::DrawΛ()
{
    GradientFillCellH();

    DataTypes::POINTd p1, p2, p3;
    //stencil it without using stencil buffers
    //Left triangle
    p1 = { 0, 0 };
    
    p2.x = 0;
    p2.y = -m_cellSize.cy;

    p3.x = 0.358*m_cellSize.cx;
    p3.y = 0;
    DrawShapes::DrawTriangleSolid(p1, p2, p3, m_backGround);

    //right triangle
    p1.x = (1 - 0.358)*m_cellSize.cx;
    p1.y = 0;

    p2.x = m_cellSize.cx;
    p2.y = -m_cellSize.cy;

    p3.x = m_cellSize.cx;
    p3.y = 0;
    DrawShapes::DrawTriangleSolid(p1, p2, p3, m_backGround);

    //bottom triangle
    p1.x = 0.2367*m_cellSize.cx;
    p1.y = -m_cellSize.cy;

    p2.x = m_cellSize.cx - p1.x;
    p2.y = p1.y;

    p3.x = 0.49408 * m_cellSize.cx;
    p3.y = -0.2426 * m_cellSize.cy;
    DrawShapes::DrawTriangleSolid(p1, p2, p3, m_backGround);
}

void util::TextHelper::Alphabet::Drawא()
{
    GradientFillCellH();

    DataTypes::POINTd p1, p2, p3;
    //stencil it without using stencil buffer

#pragma region bottom triangle
    p1.x = 0.2367*m_cellSize.cx;
    p1.y = -m_cellSize.cy;

    p2.x = m_cellSize.cx - p1.x;
    p2.y = p1.y;

    p3.x = 0.49408 * m_cellSize.cx;
    p3.y = -0.2426 * m_cellSize.cy;
    DrawShapes::DrawTriangleSolid(p1, p2, p3, m_backGround);
#pragma endregion

#pragma region draw tricolor lines
    glLineWidth(10);
    glBegin(GL_LINES);
    glColor3f(GetRValuef(ColorWheel::COLORS::INDIA_SAFRON), GetGValuef(ColorWheel::COLORS::INDIA_SAFRON), GetBValuef(ColorWheel::COLORS::INDIA_SAFRON));
    glVertex3f(0,             -m_cellSize.cy * 0.4, 0);
    glVertex3f(m_cellSize.cx, -m_cellSize.cy * 0.4, 0);
    
    glColor3f(1, 1, 1);
    glVertex3f(0,             -m_cellSize.cy * 0.6, 0);
    glVertex3f(m_cellSize.cx, -m_cellSize.cy * 0.6, 0);

    glColor3f(GetRValuef(ColorWheel::COLORS::INDIA_GREEN), GetGValuef(ColorWheel::COLORS::INDIA_GREEN), GetBValuef(ColorWheel::COLORS::INDIA_GREEN));
    glVertex3f(0,             -m_cellSize.cy * 0.8, 0);
    glVertex3f(m_cellSize.cx, -m_cellSize.cy * 0.8, 0);
    glEnd();
#pragma endregion

#pragma region Left triangle
    p1 = { 0, 0 };

    p2.x = 0;
    p2.y = -m_cellSize.cy;

    p3.x = 0.358*m_cellSize.cx;
    p3.y = 0;
    DrawShapes::DrawTriangleSolid(p1, p2, p3, m_backGround);
#pragma endregion

#pragma region right triangle
    p1.x = (1 - 0.358)*m_cellSize.cx;
    p1.y = 0;

    p2.x = m_cellSize.cx;
    p2.y = -m_cellSize.cy;

    p3.x = m_cellSize.cx;
    p3.y = 0;
    DrawShapes::DrawTriangleSolid(p1, p2, p3, m_backGround);
#pragma endregion

}

void TextHelper::Alphabet::Drawב(AnimationObjectState& state)
{
    static UINT currentIteration = 0;

    static auto startTime = 8.1399999999999988; //hardcoded - taken from Dynamic India.cpp //TODO:remove hard coded stuff
    static auto endTime = 8.1399999999999988 + 2; //hardcoded - taken from Dynamic India.cpp //TODO:remove hard coded stuff
    UINT numIterations = (endTime - startTime) / state.m_stepSize;//iterations to reach full opacity

    Alphabet alpha1 = *this;//so that font size, colors, etc. are automatically copied
    alpha1.SetChar(L'D');
    alpha1.Draw(AnimationObjectState{});

    if (currentIteration < numIterations)
    {
        BYTE alpha = 255 - (255.0 / (numIterations - 1.0))*(currentIteration);//not alphabet but alpha chanel
        Alphabet alpha2 = *this;//so that font size, colors, etc. are automatically copied
        alpha2.SetChar(L'D');
        alpha2.SetTopColor(m_backGround);
        alpha2.SetBottomColor(m_backGround);
        alpha2.SetAlpha(alpha);
        alpha2.Draw(AnimationObjectState{});
        currentIteration++;
    }
}

void util::TextHelper::Alphabet::Drawג(AnimationObjectState & state)
{
    auto xDelta = m_cellSize.cx / 3.0;
    auto yDelta = m_cellSize.cy / 4.0;
    double x = 0, y = 0, z = 0;

    glBegin(GL_QUADS);
    x = y = z = 0;

    glVertex3f(x, y, z);
    y -= yDelta;
    glVertex3f(x, y, z);
    x += 3*xDelta;
    glVertex3f(x, y, z);
    y += yDelta;
    glVertex3f(x, y, z);

    x = y = z = 0;

    y -= yDelta;
    x += xDelta;
    glVertex3f(x, y, z);
    y -= 2*yDelta;
    glVertex3f(x, y, z);
    x += xDelta;
    glVertex3f(x, y, z);
    y += 2 * yDelta;
    glVertex3f(x, y, z);

    x = y = z = 0;

    y -= 3*yDelta;
    glVertex3f(x, y, z);
    y -= yDelta;
    glVertex3f(x, y, z);
    x += 3 * xDelta;
    glVertex3f(x, y, z);
    y += yDelta;
    glVertex3f(x, y, z);

    glEnd();
}

void util::TextHelper::Alphabet::Drawד(AnimationObjectState & state)
{
    double x = 0, y = 0, z = 0;

    glBegin(GL_QUADS);
    x = y = z = 0;

    //quad1
    glVertex3f(x, y, z);
    y = -m_cellSize.cy;
    glVertex3f(x, y, z);
    x += 0.23776 * m_cellSize.cx;
    glVertex3f(x, y, z);
    y += m_cellSize.cy;
    glVertex3f(x, y, z);

    //quad2
    glVertex3f(x, y, z);
    y = -(1 - 0.7132) * m_cellSize.cy;
    glVertex3f(x, y, z);
    y = -m_cellSize.cy;
    x = (1 - 0.23776)*m_cellSize.cx;
    glVertex3f(x, y, z);
    y = -0.7132*m_cellSize.cy;
    glVertex3f(x, y, z);

    //quad3
    y = -m_cellSize.cy;
    x = (1 - 0.23776)*m_cellSize.cx;
    glVertex3f(x, y, z);
    x = m_cellSize.cx;
    glVertex3f(x, y, z);
    y = 0;
    glVertex3f(x, y, z);
    x = (1 - 0.23776)*m_cellSize.cx;
    glVertex3f(x, y, z);
    glEnd();

}

void util::TextHelper::Alphabet::Drawה(AnimationObjectState & state)
{
    static double PI = 4 * atan(1);

    DataTypes::POINTd p1, p2, p3, p4;
    p1 = {};
    p2.x = 0;
    p2.y = -m_cellSize.cy;
    //p3.x = 0.46*m_cellSize.cx;
    p3.x = 0.1878*m_cellSize.cx;
    p3.y = p2.y;
    p4.x = p3.x;
    p4.y = 0;
    DataTypes::POINTd c = { p3.x, (p3.y + p4.y) / 2.0 };

    double innerRadius = 0.5352*m_cellSize.cx;
    //double innerRadius = 0.5052*m_cellSize.cx;
    double outerRadius = 0.6948*m_cellSize.cx;

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glTranslatef(c.x, c.y, 0);
    COLORREF top = m_top, bottom = m_bottom;
    DrawShapes::DrawHollowCircleTriangleStrip(innerRadius, outerRadius, 1.5*PI, 0.5*PI, RBG2RGBA(top, m_alpha), RBG2RGBA(bottom, m_alpha));

    glPopMatrix();

    glBegin(GL_QUADS);
    glVertex2f(p1.x, p1.y);

    glVertex2f(p2.x, p2.y);
    glVertex2f(p3.x, p3.y);

    glVertex2f(p4.x, p4.y);

    glEnd();

}

void util::TextHelper::Alphabet::Drawו(AnimationObjectState & state)
{
    double x = 0, y = 0, z = 0;

    glBegin(GL_POLYGON);
    //left polygon
    y = -m_cellSize.cy;
    glVertex3f(x, y, z);
    x = 0.2367*m_cellSize.cx;
    glVertex3f(x, y, z);
    x = 0.49408 * m_cellSize.cx;
    y = -0.2426 * m_cellSize.cy;
    glVertex3f(x, y, z);
    y = 0;
    glVertex3f(x, y, z);
    x = 0.358*m_cellSize.cx;
    glVertex3f(x, y, z);
    glEnd();

    glBegin(GL_POLYGON);
    //right polygon
    x = (1 - 0.358)*m_cellSize.cx;
    glVertex3f(x, y, z);
    x = 0.358*m_cellSize.cx;
    glVertex3f(x, y, z);
    x = 0.49408 * m_cellSize.cx;
    y = -0.2426 * m_cellSize.cy;
    glVertex3f(x, y, z);
    y = -m_cellSize.cy;
    x = m_cellSize.cx - 0.2367*m_cellSize.cx;
    glVertex3f(x, y, z);
    x = m_cellSize.cx;
    glVertex3f(x, y, z);
    glEnd();
}

#endif
void util::TextHelper::Alphabet::SolidFillCellWithBackground()
{
    glBegin(GL_QUADS);
        glColor4f(GetRValuef(m_backGround), GetGValuef(m_backGround), GetBValuef(m_backGround), BYTE2FLOAT(m_alpha)*GetAValuef(m_backGround));
        glVertex2f(0, 0);

        glVertex2f(0, -m_cellSize.cy);
        glVertex2f(m_cellSize.cx, -m_cellSize.cy);

        glVertex2f(m_cellSize.cx, 0);
    glEnd();
}
void util::TextHelper::Alphabet::GradientFillCellH()
{
    glBegin(GL_QUADS);
        glColor4f(GetRValuef(m_top), GetGValuef(m_top), GetBValuef(m_top), BYTE2FLOAT(m_alpha));
        glVertex2f(0, 0);

        glColor4f(GetRValuef(m_bottom), GetGValuef(m_bottom), GetBValuef(m_bottom), BYTE2FLOAT(m_alpha));
        glVertex2f(0, -m_cellSize.cy);
        glVertex2f(m_cellSize.cx, -m_cellSize.cy);

        glColor4f(GetRValuef(m_top), GetGValuef(m_top), GetBValuef(m_top), BYTE2FLOAT(m_alpha));
        glVertex2f(m_cellSize.cx, 0);
    glEnd();
}

void util::TextHelper::Alphabet::AdvanceTime()
{
    THROW_NOT_IMPLEMENTED
}

void TextHelper::SetFontHeight(double height)
{
    m_fontHeight = height;
}

void TextHelper::WriteText(std::wstring line, DataTypes::POINT3Dd relativePos)
{
    m_str = line;

    glPushMatrix();
    glTranslatef(relativePos.x, relativePos.y, relativePos.z);
    for (wchar_t& ch : line)
    {
        Alphabet alpha(ch, m_top, m_bottom, m_stringAlpha);
        alpha.SetFontHeight(m_fontHeight);
        alpha.SetBackGroundColor(m_background);
        alpha.Draw(AnimationObjectState{});
        glTranslatef(m_characterSpacing + alpha.GetCellSize().cx, 0, 0);
    }
    glPopMatrix();
}

double TextHelper::GetOnScreenSize(std::wstring line)
{
    double totSize = 0;
    for (wchar_t& ch : line)
    {
        Alphabet alpha(ch, m_top, m_bottom, m_stringAlpha);
        alpha.SetFontHeight(m_fontHeight);
        totSize += m_characterSpacing + alpha.GetCellSize().cx;
    }
    return totSize;
}

DataTypes::Size2Dd TextHelper::GetMaxHeightCellSize(std::wstring line)
{
    double height = -1;
    DataTypes::Size2Dd cell = {};
    for (wchar_t& ch : line)
    {
        Alphabet alpha(ch, m_top, m_bottom, m_stringAlpha);
        alpha.SetFontHeight(m_fontHeight);

        if (height < alpha.GetCellSize().cy)
        {
            cell = alpha.GetCellSize();
            height = cell.cy;
        }
    }

    return cell;
}
