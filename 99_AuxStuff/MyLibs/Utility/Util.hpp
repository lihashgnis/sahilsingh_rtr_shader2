﻿#pragma once
#include<Windows.h>
#include<functional>
#include <exception>
#include <memory>
#include "Common.h"

#define STRICT

#ifndef SUPPORT_UNICODE_LIB //otherwise cuda files don't compile correctly even if the file is saved as unicode
#define SUPPORT_UNICODE_LIB 1 //ON by defaul unless explicitly turned OFF (for eg. in .cu files)
#endif // ! SUPPORT_UNICODE_LIB //otherwise cuda files don't compile correctly even if the file is saved as unicode

#ifndef NDEBUG
#define THROW_NOT_IMPLEMENTED {throw std::exception("Not implemented");}
#else
#define THROW_NOT_IMPLEMENTED ;
#endif

#ifndef NDEBUG
#define ASSERT(x,y) \
if(false==(x)) {throw std::exception(y);}
#else
#define ASSERT(x) ;
#endif

//A lot of times during debugging, run once is required
#define RUN_ONCE_START \
{\
static bool runOnceVariable = false;\
if(false == runOnceVariable) \
{


#define RUN_ONCE_END \
runOnceVariable = true;\
}\
}

#define PARENT_OBJECT TEXT("OglWindowParentObjectDataTypes::pointer")

//colorref is 4 bytes, RGB() macro uses only 3 of those bytes. Hack it to add alpha chanel
// a - alpha. we are really storing opacity. Opacity is being stored instead of alpha since RGB() stores 0 in those bits, we want full opaque at that point
//#define RGBA(r,g,b,a)  ((COLORREF)(((BYTE)(r)|((WORD)((BYTE)(g))<<8))|(((DWORD)(BYTE)(b))<<16))|(((DWORD)(BYTE)(255-a))<<24)))
#define RGBA(r,g,b,a)     ((COLORREF)(((BYTE)(r)|((WORD)((BYTE)(g))<<8))|(((DWORD)(BYTE)(b))<<16) | (((DWORD)(BYTE)(255-a))<<24)))
#define GetRValuef(x)       (GetRValue(x)/255.0)
#define GetGValuef(x)       (GetGValue(x)/255.0)
#define GetBValuef(x)       (GetBValue(x)/255.0)
#define GetOValue(x)        (LOBYTE((x)>>24)) //Opacity
#define GetAValue(x)        (255 - GetOValue(x))//since opcaity is stored by RGBA, and not alpha
#define GetAValuef(x)       (GetAValue(x)/255.0)//since opcaity is stored by RGBA, and not alpha
#define RBG2RGBA(rgb, a)    (COLORREF)((DWORD)rgb | (((DWORD)(BYTE)(255-a))<<24) | [rgb](){if(GetOValue(rgb)!=0){throw std::exception("rgb should not contain opacity value");}; return 0;}()) //TODO: this won't give correct result when rgb also has pre existing alpha value
#define BYTE2FLOAT(x)       (x/255.0)

//Init masks
#define INIT_MASK_ENABLE_DEPTH 0b1
#define INIT_MASK_ENABLE_STENCIL 0b10
#define INIT_MASK_ENABLE_TEXTURE_2D 0b100
#define INIT_MASK_ENABLE_PROGRAMMABLE_PIPELINE 0b1000 //also means that fixed function related code will get disabled



namespace util {
    /*Animation object takes care of rotation, scaling, etc. of a scene object embedded in it.
    The object itself is unaware of these traslations. This struct will carry data to objects while drawing, in case the object
    wants to refer to it.
    */
    struct AnimationObjectState;

	class Utility {
	public:
		static void InitWndClassEx(WNDCLASSEX &wc, TCHAR* AppName, WNDPROC wp, HINSTANCE hInstance);
		static HWND CreateOglWindow(TCHAR * className, UINT x, UINT y, UINT width, UINT height, HINSTANCE hInstance);

	};

    namespace DataTypes
    {
        //'d' stands for double
        struct POINTd {
            double x, y;
        };

        typedef POINTd POINT2Dd;

        struct POINT3Dd {
            double x, y, z;
            bool operator==(POINT3Dd& that)
            {
                if (std::fabs(x - that.x) > std::numeric_limits<double>::epsilon())
                    return false;
                if (std::fabs(y - that.y) > std::numeric_limits<double>::epsilon())
                    return false;
                if (std::fabs(z - that.z) > std::numeric_limits<double>::epsilon())
                    return false;
                return true;
            }

            bool operator!=(POINT3Dd& that)
            {
                return !(*this == that);
            }
        };

        struct Size2Dd {
            double cx, cy;
        };

        typedef POINTd Centre;
        typedef double Radius;
    }

	class OglWindow
	{
	protected:
		virtual const char* GetMessageMap() = 0;
		virtual void Display() = 0;
		virtual void Update() = 0;
		virtual void Resize(int width, int height) = 0;
		int Initialize(UINT mask = 0);
		void Uninitialize();
        void ToggleFullscreeen(HWND hwnd);
		virtual LRESULT WndProcPre(HWND, UINT, WPARAM, LPARAM) = 0;//called before processing by base class
		virtual LRESULT WndProcPost(HWND, UINT, WPARAM, LPARAM) = 0;

		WNDCLASSEX m_wndClassEx;
		HWND m_hwnd;
		HDC m_Hdc;
		HGLRC m_Hrc;
		bool m_bIsFullScreen, m_bActiveWindow;
        bool m_bIsPaused;
        bool m_bAllowOutOfFocusrender; //normally any rendering is done only when window is active (in focus)
#pragma region frame limitter
        bool m_bCapFramerate;//If true, the max rate at which application renders is capped - m_dwInterFrameTimeMilliSecond
        DWORD m_dwInterFrameTimeMilliSecond; //In milliseconds - used to limit max frame rate
        DWORD m_dwPrevEpoch; //time in ms when last display call was invoked
#pragma endregion
		UINT m_width, m_height;
        double m_minX, m_maxX; //Minimum, and maximum clipping extents along X axis (-1,1) by default;
        double m_minY, m_maxY; //Minimum, and maximum clipping extents along X axis (-1,1) by default;
        double m_minZ, m_maxZ; //Minimum, and maximum clipping extents along X axis (-1,1) by default;
        void CalcExtents();
	public:
        void ToggleFullscreeen();
        void TogglePause();
        bool IsFullScreen();
        void EnableAlpha(bool bEnable);

		OglWindow(TCHAR* className, UINT x, UINT y, UINT width, UINT height, HINSTANCE hInstance);
		void InitAndShowWindow(UINT mask = 0);//MASK to customize init behaviour, one of INIT_MASK_*
		~OglWindow();
		static LRESULT CALLBACK OglWndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam);
		void RunGameLoop();
		HWND GetHwnd();

        void SetCapFrameRate(DWORD interFrameTimeMilliSeconds/*millisecond*/, bool bCapFrameRate = false)
        {
            if (bCapFrameRate && (interFrameTimeMilliSeconds == 0))
            {
                throw std::exception("interFrameTimeMilliSeconds CanotbeZero");
            }

            m_dwPrevEpoch = 0;
            m_bCapFramerate = bCapFrameRate;
            m_dwInterFrameTimeMilliSecond = interFrameTimeMilliSeconds;
        }

        void AllowOutOfFocusRender(bool bAllow = false)
        {
            m_bAllowOutOfFocusrender = bAllow;
        }

        double GetMinX();

        double GetMaxX();

        double GetMinY();

        double GetMaxY();

        //Newer versions
        double GetMinX2();

        double GetMaxX2();

        double GetMinY2();

        double GetMaxY2();

        double GetWindowHeightNDC();

        double GetWindowWidthNDC();

        //Pixels on screen to normalized coordinates that openGL uses
        //Should behave same for X, and Y directions
        double PixelsToNDC(int pixels);
        //normalized coordinates that openGL uses to pixels on screen
        //Should behave same for X, and Y directions
        int NDCToPixels(double NDCcoord);

        void SetClearColor(COLORREF rgba);

	};

    class DrawShapes {
    public:
        DrawShapes() = delete;
        /* @brief Draws a circle made up of small line segments
         * @param[IN] NUM_LINE : number lines used to make the circle. Higher the number, the will be approximation of a circle.
         * @RADIUS[IN] radius of the circle
        */
        static void DrawCircleLines(UINT NUM_LINE, double RADIUS, COLORREF color = RGB(255, 255, 255));
        static void DrawCircleArcTriangleFan(double RADIUS, double startAngle, double endAngle, COLORREF centreColor = RGB(255, 255, 255), COLORREF boundaryColor = RGB(255, 255, 255), UINT NUM_PTS = 100);
        static auto InterpolateColor(double val, double max_val/*Corresponding to col1*/, double min_val/*Corresponding to col2*/, COLORREF col1, COLORREF col2);
        static void DrawHollowCircleTriangleStrip(double innerRadius, double outerRadius, double startAngle, double endAngle, COLORREF topColor = RGB(255, 255, 255), COLORREF bottomColor = RGB(255, 255, 255), UINT NUM_PTS = 100);
        static void DrawRectangleLines(double left, double bottom, double right, double top, COLORREF color = RGB(255, 255, 255));
        static void DrawTriangleLines(DataTypes::POINTd p1, DataTypes::POINTd p2, DataTypes::POINTd p3, COLORREF color = RGB(255, 255, 255));
        static void DrawTriangleSolid(DataTypes::POINTd p1, DataTypes::POINTd p2, DataTypes::POINTd p3, COLORREF color);
        static void DrawEqTriangleByIncentre(DataTypes::Centre centre, DataTypes::Radius, COLORREF color = RGB(255, 255, 255));
        static std::tuple<DataTypes::Centre, DataTypes::Radius> GetTriangleIncentre(DataTypes::POINTd p1, DataTypes::POINTd p2, DataTypes::POINTd p3);
        static void DrawGraph(float factor);
    };

    class Math {
    public:

    };

    class ColorWheel {
    public:
        typedef long long VLONG;
        const VLONG max_cols_range = 256 * 256 * 256;
        const VLONG min_cols_range = 0;
        VLONG num_partitions = 0;
        VLONG radius;
        typedef BYTE RED;
        typedef BYTE GREEN;
        typedef BYTE BLUE;
        const double PI;
        const double PI2;

        ColorWheel(VLONG numPartitions);

        struct COLORS
        {
            static constexpr COLORREF INDIA_SAFRON = RGB(0xFF, 0x99, 0x33);
            static constexpr COLORREF INDIA_GREEN = RGB(0x13, 0x88, 0x08);
            static constexpr COLORREF WHITE = RGB(255, 255, 255);
        };

        std::tuple<RED, GREEN, BLUE> MapIndexToColor(VLONG index);
    };

    //every object to be drawn on screen must be derived from this
    class SceneObject {
    protected:
        UINT m_zOrder;
        DataTypes::POINT3Dd m_position;
        BYTE m_alpha;/*Alpha can be part of COLORREF or m_alpha. m_alpha is for entire object*/
        DataTypes::Size2Dd m_cellSize;
    public:
        void SetAlpha(BYTE alpha)
        {
            m_alpha = alpha;
        }

        void SetCellSize(const DataTypes::Size2Dd& sz)
        {
            m_cellSize = sz;
        }

        SceneObject() : m_position{ 0,0,0 }, m_zOrder(0), m_alpha(255), m_cellSize{0,0}
        {

        }

        DataTypes::Size2Dd GetCellSize()
        {
            return m_cellSize;
        }

        //The logic to draw whatever any derived class wants to render on screen
        //For eg. Draw() of derived class airplane will draw an airplane, while for face, it will draw a face
        //Most objects are drawn in thirds quadrant of object coordinate system
        //Some, like speech buble are drawn in the first quadrant
        virtual void Draw(AnimationObjectState& state) = 0;

        //29 June 2019 - I don't remember why AdvanceTime() was added, but in all derived classes created
        //during the days this base class was written I see that it hasn't been implemented
        //And either are empty, or throw exception when called
        virtual void AdvanceTime() = 0;
    };

    class TextHelper
    {
        const double m_wordSpacing;
        const double m_characterSpacing;
        COLORREF m_top, m_bottom, m_background; //gradient
        double m_fontHeight = 0.2;
        std::wstring m_str;
        BYTE m_stringAlpha;
    public:
        TextHelper(double wordSpacing, double characterSpacing, COLORREF top = RGB(255, 255, 255), COLORREF bottom = RGB(255, 255, 255), BYTE stringAlpha = 255/*Opaque by default*/) : m_wordSpacing(wordSpacing), m_characterSpacing(characterSpacing), m_top(top), m_bottom(bottom), m_stringAlpha(stringAlpha), m_background(RGB(0, 0, 0))
        {

        }

        void SetBackGroundColor(COLORREF backgroundColor)
        {
            m_background = backgroundColor;
        }

        void SetFontHeight(double height);

        void WriteText(std::wstring line, DataTypes::POINT3Dd relativePos /*Relative to transformation already done on MODELVIEW matrix*/);

        double GetOnScreenSize(std::wstring line);

        ///cell size of the cell with maximum height
        DataTypes::Size2Dd GetMaxHeightCellSize(std::wstring line);


        //Each letter is represented by an object of this class
        // Hebrew alphabets are used for hardcoded customizations 
        //Customizations
        //Alef (א) represents tricolor A in static India assignment
        class Alphabet final : public SceneObject
        {
            COLORREF m_top, m_bottom, m_backGround;
            wchar_t m_char;//character this object represents
            double m_monospaceRatio = 0.72; //Width:Height (Height is longer) - got this by observing consolas font viz also monospace
        public:
            //We do not want anyone to cell arbitray cell size
            //Since we have to maintain aspect ratio for letters
            void SetCellSize(const DataTypes::Size2Dd& sz) = delete;
            Alphabet(wchar_t chr) : Alphabet(chr, RGB(255, 255, 255), RGB(255, 255, 255), 255)
            {
            }

            Alphabet(wchar_t chr, COLORREF top, COLORREF bottom, BYTE objectAlpha) : m_char(chr), m_top(top), m_bottom(bottom), m_backGround(0)
            {
                m_cellSize = { 0.2*m_monospaceRatio, 0.2 };
                m_alpha = objectAlpha;
            }

            void SetBackGroundColor(COLORREF background)
            {
                m_backGround = background;
            }

            void SetTopColor(COLORREF top)
            {
                m_top = top;
            }

            void SetBottomColor(COLORREF bottom)
            {
                m_bottom = bottom;
            }

            void SetChar(wchar_t chr)
            {
                m_char = chr;
            }

            virtual void Draw(AnimationObjectState& state) override;
            void SetFontHeight(double height);
            DataTypes::Size2Dd GetCellSize()
            {
                return m_cellSize;
            }
        private:
            void DrawA();
            void DrawF();
            void DrawD();
            void DrawI();

            /// <summary> Unicode English Alphabet N
            /// </summary>
            void DrawN();
            void SolidFillCellWithBackground();
            void GradientFillCellH();
#if SUPPORT_UNICODE_LIB //otherwise cuda files don't compile correctly even if the file is saved as unicode
            void DrawΛ();
            //Hebrew alpabets are used to cutominzed drawings, eg. א (Alef) is used for Λ with tricolor
            // horizontal lines for India assignments
            /// @brief Alef - Tricolor A (Λ) in static/dynamic India
            void Drawא();
            /*Bet - Fade-in D in Dynamic Idia - A better solution would gave been to use
            Transparancy keyframes, and tweening, but transparency code I wrote isn't working correctly -
            One reason is that all alphabets are being written assuming opacity, on enabling transparency the hidden
            overlapping regions become visible
            */
            void Drawב(AnimationObjectState& state);
            
            /*Gimel - Render I alphabet for stencil
            */
            void Drawג(AnimationObjectState& state);


            /*Dalet - Render N alphabet for stencil
            */
            void Drawד(AnimationObjectState& state);


            /*He - Render D alphabet for stencil
            */
            void Drawה(AnimationObjectState& state);


            /*Vav - Render A alphabet without the horizonatl line for stencil
            */
            void Drawו(AnimationObjectState& state);
#endif
            // Inherited via SceneObject
            virtual void AdvanceTime() override;
        };

    };

    /*Animation object takes care of rotation, scaling, etc. of a scene object embedded in it.
    The object itself is unaware of these traslations. This struct will carry data to objects while drawing, in case the object
    wants to refer to it.
    */
    struct AnimationObjectState
    {
        DataTypes::POINT3Dd m_CurrentPos, m_CurrentScale, m_CurrentRotationVector /*Rotation vector*/;
        double m_CurrentRotationAngle;
        double m_CurrentTransparency;
        double m_CurrentTime;
        double m_stepSize;
        OglWindow *m_windowObject;

        bool m_bIsDataValid;//It is possible that scene objects are called w/o animation or timeline
        AnimationObjectState() :m_bIsDataValid(false)
        {

        }
        AnimationObjectState(DataTypes::POINT3Dd currentPos, DataTypes::POINT3Dd currentScale, DataTypes::POINT3Dd currentRotationVector, double currentRotationAngle, double currentTransparency, double currentTime, double stepSize, OglWindow *window = nullptr) :
            m_CurrentPos(currentPos), m_CurrentScale(currentScale), m_CurrentRotationVector(currentRotationVector),
            m_CurrentRotationAngle(currentRotationAngle), m_CurrentTransparency(currentTransparency), m_CurrentTime(currentTime),
            m_stepSize(stepSize), m_windowObject(window)
        {

        }
    };
}