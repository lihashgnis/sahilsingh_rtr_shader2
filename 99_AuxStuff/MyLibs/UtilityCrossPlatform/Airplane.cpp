﻿#ifdef USING_PRECOMPILED_HEADERS
#include "stdafx.h"
#endif
#include "Airplane.h"
#include "AirplaneExhaustParticle.h"
#include <random>
#include <vector>
#include <GL/gl.h>

using namespace util;

constexpr double EXHAUST_WIDTH = 0.05;
constexpr UINT NUM_PARTICLES = 5;
constexpr UINT NUM_EXHAUST = 3;
constexpr double PADDING_RATIO = 0.1;
constexpr double FONT_RATIO = 0.2;//relative to airplan size


Airplane::Airplane() : 
m_prevPosition{ 0,0,0 },
m_bIsFirstRun(true),
m_bProduceParticles(true),
m_bParticlesDontDie(false),
m_ExhaustColors{ ColorWheel::COLORS::INDIA_SAFRON, ColorWheel::COLORS::WHITE, ColorWheel::COLORS::INDIA_GREEN}
{
    m_cellSize = { 0.2, 0.2 };
    //B2 Bomber - Measurements taken in GIMP by measuring pixel coordinates of - https://sflanders.net/wp-content/uploads/2018/05/stealth.png [23 March 2019]

    m_vertices[0] = { (115 / 115.0)*m_cellSize.cx, (112 / 224.0)*   m_cellSize.cy };
    m_vertices[1] = { (16 / 115.0)* m_cellSize.cx,  (0 / 224.0)*    m_cellSize.cy };
    m_vertices[2] = { (0 / 115.0)*  m_cellSize.cx,   (12 / 224.0)*  m_cellSize.cy };
    m_vertices[3] = { (32 / 115.0)* m_cellSize.cx,  (51 / 224.0)*   m_cellSize.cy };
    m_vertices[4] = { (6 / 115.0)*  m_cellSize.cx,   (81 / 224.0)*  m_cellSize.cy };
    m_vertices[5] = { (20 / 115.0)* m_cellSize.cx,  (96 / 224.0)*   m_cellSize.cy };
    m_vertices[6] = { (3 / 115.0)*  m_cellSize.cx,   (112 / 224.0)* m_cellSize.cy };
    m_vertices[7] = { (20 / 115.0)* m_cellSize.cx,  (128 / 224.0)*  m_cellSize.cy };
    m_vertices[8] = { (6 / 115.0)*  m_cellSize.cx,   (143 / 224.0)* m_cellSize.cy };
    m_vertices[9] = { (32 / 115.0)* m_cellSize.cx,  (173 / 224.0)*  m_cellSize.cy };
    m_vertices[10] = { (0 / 115.0)* m_cellSize.cx,  (212 / 224.0)*  m_cellSize.cy };
    m_vertices[11] = { (16 / 115.0)*m_cellSize.cx, (224 / 224.0)*   m_cellSize.cy };
}


Airplane::~Airplane()
{
}

void util::Airplane::Draw(AnimationObjectState& state)
{
    //glBegin(GL_TRIANGLES);
    //    glColor3f(1, 1, 1);
    //    glVertex2f(0,0.1);
    //    glVertex2f(0,-0.1);
    //    glVertex2f(0.1,0);
    //glEnd();
    //

#define V(i) glVertex2f(m_vertices[i].x, m_vertices[i].y); //vertex
#define T(i, j, k) V(i) V(j) V(k); //triangle

    glPushMatrix();
    glTranslatef(0, -m_cellSize.cy / 2.0, 0);
        glBegin(GL_TRIANGLES);
            glColor3f(186 / 255.0, 226 / 255.0, 238 / 255.0);
            T(0, 1, 3)
            T(1, 2, 3)
            T(0, 3, 5)
            T(3, 4, 5)
            T(0, 5, 7)
            T(5, 6, 7)
            T(0, 7, 9)
            T(7, 8, 9)
            T(0, 9, 11)
            T(9, 10, 11)
        glEnd();
        glBegin(GL_LINES);
        glColor3f(133 / 255.0, 203 / 255.0, 224 / 255.0);
        V(11) V(0)
        V(1) V(0)
        glEnd();
    glPopMatrix();

#undef V
#undef T



    if (m_bIsFirstRun)
    {
        m_prevPosition = state.m_CurrentPos;
        m_bIsFirstRun = false;
    }
    if (m_bProduceParticles && (m_bIsFirstRun || m_prevPosition != state.m_CurrentPos))//don't add particles if plane has come to a halt
    {
        ParticleHelper::GenerateParticles(NUM_PARTICLES, state.m_CurrentTime, state.m_CurrentPos, state.m_CurrentRotationAngle, state.m_CurrentRotationVector, m_cellSize, PADDING_RATIO, NUM_EXHAUST, m_ExhaustColors, m_bParticlesDontDie);
    }
    m_prevPosition = state.m_CurrentPos;

    //write IAF
    TextHelper t(0, 0, RGB(133, 203, 224), RGB(133, 203, 224), 255);
    t.SetBackGroundColor(RGB(186, 226, 238));
    auto fHeight = m_cellSize.cx*FONT_RATIO;
    t.SetFontHeight(fHeight);
    t.WriteText(L"IΛF", { m_cellSize.cx / 4.0, fHeight/2.0 , 0});
}


void util::Airplane::AdvanceTime()
{
}
