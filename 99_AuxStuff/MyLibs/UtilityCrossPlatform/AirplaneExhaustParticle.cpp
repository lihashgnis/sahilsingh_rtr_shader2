#ifdef USING_PRECOMPILED_HEADERS
#include "stdafx.h"
#endif

#include "AirplaneExhaustParticle.h"
#include "Animation.hpp"
#include <random>
#include <vector>
#include <exception>
#include <stdexcept>
#include <GL/gl.h>

using namespace util;

std::shared_ptr<util::Timeline> ParticleHelper::m_timeline;



AirplaneExhaustParticle::AirplaneExhaustParticle(COLORREF color /*= RGB(255, 255, 255)*/) : m_color(color)
{
}


AirplaneExhaustParticle::~AirplaneExhaustParticle()
{
}

void util::AirplaneExhaustParticle::Draw(AnimationObjectState& state)
{
    glBegin(GL_POINTS);
        glColor4f(GetRValuef(m_color), GetGValuef(m_color), GetBValuef(m_color), BYTE2FLOAT(m_alpha)*GetAValuef(m_color));
        glVertex3f(0, 0, 0);
    glEnd();
}

void util::AirplaneExhaustParticle::AdvanceTime()
{
}

void ParticleHelper::GenerateParticles(unsigned long numParticles, double currTime, DataTypes::POINT3Df currPos, double currRotangle, DataTypes::POINT3Df currRotateVector, DataTypes::Size2Df cellSize, double paddingRatio, UINT numExhausts, std::vector<COLORREF> colors, bool bParticlesDontDie)
{
    if (!ParticleHelper::m_timeline)
    {
        throw std::runtime_error("Error! m_timeline not set");
    }

    static std::random_device rd;
    static std::exponential_distribution<float> distxy(100);// 1/100 mean - dist for getting x, y of a particle
    static std::exponential_distribution<float> distt(2.5);// 1/10 mean - dist for getting life of a particle
    static std::mt19937 gen(rd());

    for (int i = 0; i < numParticles/2; i++)
    {

        for (int j = 0; j < numExhausts; j++)
        {
            std::shared_ptr<AirplaneExhaustParticle> part1(new AirplaneExhaustParticle(colors[j]));
            std::shared_ptr<AirplaneExhaustParticle> part2(new AirplaneExhaustParticle(colors[j]));//since it is exponential dist, it will only generate values lying on one side of Y

            float exhaustMid = currPos.y + cellSize.cy/2.0 - (cellSize.cy - cellSize.cy*paddingRatio*2)*(j+1.0)/ NUM_EXHAUSTS;
            
            auto p1x = currPos.x - distxy(gen);
            auto p2x = currPos.x - distxy(gen);
            auto p1t = distt(gen);//life of this particle

            auto p1y = exhaustMid + distxy(gen);
            auto p2y = exhaustMid - distxy(gen);
            auto p2t = distt(gen);//life of this particle

            util::DataTypes::POINT3Df pos1 = { p1x, p1y,0 };
            util::DataTypes::POINT3Df pos2 = { p2x, p2y,0 };

            auto time = currTime + ParticleHelper::m_timeline->GetStepSize(); //schedule to be displayed in next iteration

            AnimationObject ao1(ParticleHelper::m_timeline, part1, pos1);
            ao1.AddKeyFrame(util::KeyFrame(0, util::KeyFrame::ACTION::OBJECT_SHOW, time, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, pos1));
            ao1.AddKeyFrame(util::KeyFrame(1, util::KeyFrame::ACTION::ROTATE_ANGLE_ABSOLUTE, time, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, currRotateVector, currRotangle));
            
            if (!bParticlesDontDie)//don't ever kill this particle
            {
                ao1.AddKeyFrame(util::KeyFrame(2, util::KeyFrame::ACTION::OBJECT_KILL, time + p1t, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE));
            }

            m_timeline->AddObject(ao1);


            AnimationObject ao2(ParticleHelper::m_timeline, part2, pos2);
            ao2.AddKeyFrame(util::KeyFrame(0, util::KeyFrame::ACTION::OBJECT_SHOW, time, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, pos2));
            ao2.AddKeyFrame(util::KeyFrame(1, util::KeyFrame::ACTION::ROTATE_ANGLE_ABSOLUTE, time, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, currRotateVector, currRotangle));

            if (!bParticlesDontDie)//don't ever kill this particle
            {
                ao2.AddKeyFrame(util::KeyFrame(2, util::KeyFrame::ACTION::OBJECT_KILL, time + p2t, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE));
            }

            m_timeline->AddObject(ao2);
        }
    }
}
