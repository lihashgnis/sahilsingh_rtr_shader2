#pragma once
#include "UtilCrossPlatform.hpp"
#include "Animation.hpp"
#include <vector>

#define NUM_EXHAUSTS 3

namespace util{
    class ParticleHelper
    {
        static std::shared_ptr<util::Timeline> m_timeline;
    public:
        static void AttachTimeline(std::shared_ptr<util::Timeline> timeline)
        {
            ParticleHelper::m_timeline = timeline;
        }

        static void GenerateParticles(unsigned long numParticles, double currTime, DataTypes::POINT3Df currPos, double currRotangle = 0, DataTypes::POINT3Df currRotateVector = { 0,0,1 }/*Rotate about z by defualt*/, DataTypes::Size2Df cellSize = { 1,1 }, double paddingRatio = 0.1 /*- as ratio of cell height (our plane is facing right)- Pading on either side of airplane to find position of exhaust*/, UINT numExhausts = NUM_EXHAUSTS, std::vector<COLORREF> colors /*Particle color for each exhaust*/ = {ColorWheel::COLORS::INDIA_SAFRON, ColorWheel::COLORS::WHITE, ColorWheel::COLORS::INDIA_GREEN}, bool bParticlesDontDie = false);
    };

    class AirplaneExhaustParticle :
        public SceneObject
    {
        COLORREF m_color;
    public:
        AirplaneExhaustParticle(COLORREF color = RGB(255, 255, 255));
        ~AirplaneExhaustParticle();

        // Inherited via SceneObject
        virtual void Draw(AnimationObjectState& state) override;
        virtual void AdvanceTime() override;
    };

}