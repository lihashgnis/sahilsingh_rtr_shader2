#ifdef USING_PRECOMPILED_HEADERS
#include "stdafx.h"
#endif
#include "Animation.hpp"
#include <algorithm>
#include <GL/gl.h>
#include <stdexcept>

using namespace util;

CREATE_LOG()

AnimationObject::AnimationObject(std::shared_ptr<Timeline> timeline, std::shared_ptr<SceneObject> sceneObject, DataTypes::POINT3Df spawnPos, OglWindow *windowObject) :
    m_bIsHidden(true)//object is initially hidden
    , m_CurrentPos(spawnPos),
    m_sceneObject(sceneObject),
    m_timeline(timeline),
    m_CurrentRotationVector{ 0,0,0 },
    m_CurrentScale{ 1,1,1 },
    m_PrevFrameTime(0),
    m_CurrentTransparency(1),
    m_PrevFrameTransparency(1),
    m_CurrentRotationAngle(0),
    m_bIsAlive(true),
    m_windowObject(windowObject)
{

}


void AnimationObject::AddKeyFrame(KeyFrame frame)
{
    //sanity check
    if (((KeyFrame::ACTION::OBJECT_SHOW == frame.GetActionType())
        || 
        (KeyFrame::ACTION::OBJECT_HIDE == frame.GetActionType()))
        &&
        (KeyFrame::TWEENTYPE::NONE != frame.GetTweenType()))
    {
        throw std::runtime_error("OBJECT_SHOW/OBJECT_HIDE must not support tweening");
    }

    if (KeyFrame::ACTION::OBJECT_SHOW == frame.GetActionType() &&
        KeyFrame::TWEENTYPE::NONE != frame.GetTweenType())
    {
        throw std::runtime_error("OBJECT_SHOW must not support tweening");
    }
    m_keyFramesUnprocessed.push_back(frame);
}

int AnimationObject::Draw(double currentTime, double stepSize)
{
    m_currentTime = currentTime;
    m_stepSize = stepSize;

    if (0 == m_keyFramesUnprocessed.size())
    {//no more frames to process
        TransformObject();
    }
    else
    {//there are frames which have to be processed
        std::sort(std::begin(m_keyFramesUnprocessed), std::end(m_keyFramesUnprocessed), [](KeyFrame& frame1, KeyFrame& frame2) {return (frame1.GetExecutionTime() < frame2.GetExecutionTime()); });

        std::vector<KeyFrame> nextFrames;

        for (auto itr = m_keyFramesUnprocessed.begin(); itr != m_keyFramesUnprocessed.end();)
        {
            if (std::fabs(itr->GetExecutionTime() - currentTime) < (stepSize / 2.0))//one discrete time instant can have several events
            {
                nextFrames.push_back(*itr);
                itr = m_keyFramesUnprocessed.erase(itr);
            }
            else
            {
                itr++;
            }
        }

        //if at a keyframe time no need for interpolation
        //Some things like showing/hiding an object should be done only at exact keyframe times
        //Other things like translated, scale, etc. can use interpolation
        bool bIsAtFrameEpoch = false;

        if(nextFrames.size() > 0)
        {
            bIsAtFrameEpoch = true;
            //m_timeline->SetCurrentTimelineTime(m_currentTime);
            //m_timeline->SetCurrentTimelineTime(nextFrame.GetExecutionTime());
            //m_currentTime = nextFrame.GetExecutionTime();
        }
        else
        {//no frame is at epoch, just process the next frame
            auto nextFrame = *m_keyFramesUnprocessed.begin();
            for (auto const& frame : m_keyFramesUnprocessed)
            {
                if (frame.GetExecutionTime() == nextFrame.GetExecutionTime())
                {
                    nextFrames.push_back(frame);
                }
            }
            bIsAtFrameEpoch = false;;
        }

        auto const numFrames = nextFrames.size();

        for (int i = 0; i < numFrames; i++)
        {
            auto nextFrame = nextFrames[i];

            //sanity //TODO: this sanity is broken - there can be an event after show/hide, but very close to it, numFrames would be >1 in that case
            // What we really want is that at the time epoch of show/hide there should be no other frame
            //if (KeyFrame::ACTION::OBJECT_SHOW == nextFrame.GetActionType() || KeyFrame::ACTION::OBJECT_HIDE == nextFrame.GetActionType())
            //{//same for OBJECT_KILL
            //    if (numFrames > 1) { throw std::runtime_error("Show and hide times should only have 1 keyframe"); }
            //}

            switch (nextFrame.GetActionType())
            {
            case KeyFrame::ACTION::OBJECT_SHOW:
                if (bIsAtFrameEpoch)
                {
                    m_CurrentPos = nextFrame.GetData();
                    m_bIsHidden = false;
                }
                break;
            case KeyFrame::ACTION::OBJECT_HIDE:
                if (bIsAtFrameEpoch)
                    m_bIsHidden = true;
                break;
            case KeyFrame::ACTION::TRANSLATE_ABSOLUTE:
                if (bIsAtFrameEpoch)
                    m_CurrentPos = nextFrame.GetData();
                else
                    m_CurrentPos = GetNextPos(nextFrame);
                break;
            case KeyFrame::ACTION::SCALE_ABSOLUTE:
                if (bIsAtFrameEpoch)
                    m_CurrentScale = nextFrame.GetData();
                else
                    m_CurrentScale = GetNextScale(nextFrame);
                break;
            case KeyFrame::ACTION::ROTATE_ANGLE_ABSOLUTE:
            {
                if (bIsAtFrameEpoch)
                    m_CurrentRotationAngle = nextFrame.GetRotationAngle();
                else
                    m_CurrentRotationAngle = GetNextRotationAngle(nextFrame);
                m_CurrentRotationVector = nextFrame.GetData();
            }
                break;
            case KeyFrame::ACTION::OBJECT_KILL:
            {
                if (bIsAtFrameEpoch)
                {
                    this->MarkForDelete();// delete this object before next rendering iteration
                }
            }
            break;
            case KeyFrame::ACTION::EXECUTE_ARBITRARY_CALLBACK:
            {
                if (nextFrame.GetTweenType() == KeyFrame::TWEENTYPE::NONE)
                {
                    if (bIsAtFrameEpoch)
                    {
                        if (nextFrame.GetCallback())
                        {
                            AnimationObjectState state(m_CurrentPos, m_CurrentScale, m_CurrentRotationVector, m_CurrentRotationAngle, m_CurrentTransparency, m_currentTime, m_stepSize, m_windowObject);
                            nextFrame.GetCallback()(m_sceneObject, state);
                        }
                        else
                        {
                            throw std::runtime_error("callback not set for EXECUTE_ARBITRARY_CALLBACK action type");
                        }
                    }
                }
                else
                {
                    THROW_NOT_IMPLEMENTED
                }
            }
            break;
            default:
                throw std::runtime_error("Not implemented");
                break;
            }
        }

        if (bIsAtFrameEpoch)
        {
            //m_PrevFrameTime = nextFrame.GetExecutionTime();//TODO: will using m_timeLine.Currenttime be better?
            m_PrevFrameTime = m_currentTime;
            m_PrevFramePos = m_CurrentPos;
            m_PrevFrameRotationAngle = m_CurrentRotationAngle;
            m_PrevFrameRotationVector = m_CurrentRotationVector;
            m_PrevFrameScale = m_CurrentScale;
        }
    }
    TransformObject();
    return 0;
}

double  AnimationObject::GetNextRotationAngle(const KeyFrame& nextFrame)
{
    auto nextRotateAngle = m_CurrentRotationAngle;

    switch (nextFrame.GetTweenType())
    {
    case KeyFrame::TWEENTYPE::LINEAR:
    {
        auto nextFrameRotateAngle = nextFrame.GetRotationAngle();
        auto currentTime = m_currentTime;
        auto nextFrameTime = nextFrame.GetExecutionTime();
        auto w1 = std::fabs(currentTime - m_PrevFrameTime);
        auto w2 = std::fabs(nextFrameTime - currentTime);
        if (nextFrameTime < currentTime)
        {
            //throw std::runtime_error("Error : frameTime < currentTime"); //Log this, don't throw error. It is possible for nextFrame time to be less than currnent time coz of granualrity in time increments
        }

        nextRotateAngle = (w2*m_PrevFrameRotationAngle + w1 * nextFrameRotateAngle) / (w1 + w2);
    }
    break;
    case KeyFrame::TWEENTYPE::NONE:
        return m_PrevFrameRotationAngle;
    default:
        THROW_NOT_IMPLEMENTED;
        break;
    }

    return nextRotateAngle;
}

DataTypes::POINT3Df AnimationObject::GetNextScale(const KeyFrame& nextFrame)
{
    DataTypes::POINT3Df nextScale = m_CurrentScale;

    switch (nextFrame.GetTweenType())
    {
    case KeyFrame::TWEENTYPE::LINEAR:
    {
        auto nextFrameScale = nextFrame.GetData();
        auto currentTime = m_currentTime;
        auto nextFrameTime = nextFrame.GetExecutionTime();
        auto w1 = std::fabs(currentTime - m_PrevFrameTime);
        auto w2 = std::fabs(nextFrameTime - currentTime);
        if (nextFrameTime < currentTime)
        {
            //throw std::runtime_error("Error : frameTime < currentTime"); //Log this, don't throw error. It is possible for nextFrame time to be less than currnent time coz of granualrity in time increments
        }

        nextScale.x = (w2*m_PrevFrameScale.x + w1 * nextFrameScale.x) / (w1 + w2);
        nextScale.y = (w2*m_PrevFrameScale.y + w1 * nextFrameScale.y) / (w1 + w2);
        nextScale.z = (w2*m_PrevFrameScale.z + w1 * nextFrameScale.z) / (w1 + w2);
    }
    break;
    case KeyFrame::TWEENTYPE::NONE:
        return m_CurrentScale;
    default:
        THROW_NOT_IMPLEMENTED;
        break;
    }

    return nextScale;
}

DataTypes::POINT3Df AnimationObject::GetNextPos(const KeyFrame& nextFrame)
{
    DataTypes::POINT3Df nextPos = m_CurrentPos;

    switch (nextFrame.GetTweenType())
    {
    case KeyFrame::TWEENTYPE::LINEAR:
    {
        auto nextFramePos = nextFrame.GetData();
        auto currentTime = m_currentTime;
        auto nextFrameTime = nextFrame.GetExecutionTime();
        auto w1 = std::fabs(currentTime - m_PrevFrameTime);
        auto w2 = std::fabs(nextFrameTime - currentTime);
        if (nextFrameTime < currentTime)
        {
            //throw std::runtime_error("Error : frameTime < currentTime"); //Log this, don't throw error. It is possible for nextFrame time to be less than currnent time coz of granualrity in time increments
        }
        nextPos.x = (w2*m_PrevFramePos.x + w1 * nextFramePos.x) / (w1 + w2);
        nextPos.y = (w2*m_PrevFramePos.y + w1 * nextFramePos.y) / (w1 + w2);
        nextPos.z = (w2*m_PrevFramePos.z + w1 * nextFramePos.z) / (w1 + w2);
    }
        break;
    case KeyFrame::TWEENTYPE::NONE:
        return m_CurrentPos;
    default:
        THROW_NOT_IMPLEMENTED;
        break;
    }
    
    return nextPos;
}

void AnimationObject::TransformObject()//translate, scale, rotate, etc. based on current state of object
{
    if (!m_bIsHidden)
    {
        AnimationObjectState state(m_CurrentPos, m_CurrentScale, m_CurrentRotationVector, m_CurrentRotationAngle, m_CurrentTransparency, m_currentTime, m_stepSize, m_windowObject);
        glPushMatrix();
        glTranslatef(m_CurrentPos.x, m_CurrentPos.y, m_CurrentPos.z);
        glScalef(m_CurrentScale.x, m_CurrentScale.y, m_CurrentScale.z);
        glRotatef(m_CurrentRotationAngle, m_CurrentRotationVector.x, m_CurrentRotationVector.y, m_CurrentRotationVector.z);
        m_sceneObject->Draw(state);
        glPopMatrix();
    }
}


int Timeline::advance()
{
    if (m_currentTime > m_totalDuration)
        return -1;//end animation

    //Render each object
    for (AnimationObject& obj : m_objects)
    {
        obj.Draw(m_currentTime, m_stepSize);
        LOG(Info, ("CurrentTime=" + std::to_string(m_currentTime)).c_str());
    }

    //Remove objects which are marked for deletion
    for (auto itr = m_objects.begin(); itr != m_objects.end();)
    {
        if (itr->IsAlive())
        {
            itr++;
        }
        else
        {
            itr = m_objects.erase(itr);
        }
    }

    //If there were new objects created during rendering, add them
    if (m_objectsBuff.size())
    {
        m_objects.insert(std::end(m_objects), std::begin(m_objectsBuff), std::end(m_objectsBuff));
        m_objectsBuff.clear();
    }

    m_currentTime += m_stepSize;
    return 0;
}

double util::operator""s(unsigned long long x)//representing seconds - just to make code omre readable
{
    return x;
}

double util::operator""s(long double x)
{
    return x;
}