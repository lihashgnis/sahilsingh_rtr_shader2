#pragma once
//forward declarations
namespace util
{
    namespace ProgrammablePipeline
    {
        class Lights;
        class ObjectLightProps;
    }
    class ShaderCodesContainer;
}