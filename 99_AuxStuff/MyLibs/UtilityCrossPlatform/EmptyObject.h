#pragma once
#include "UtilCrossPlatform.hpp"

namespace util {
    /// @brief EmptyObject represents an object which is never to be drawn on screen.
    /// It can be used to execute arbitrary functions not associated with any particular object on screen. for eg. playing of music in DynamicIndia assignment
    class EmptyObject :
        public SceneObject
    {
    public:
        EmptyObject();
        ~EmptyObject();

        // Inherited via SceneObject
        virtual void Draw(AnimationObjectState & state) override;
        virtual void AdvanceTime() override;
    };
}