#ifdef USING_PRECOMPILED_HEADERS
#include "stdafx.h"
#endif
#ifdef _WIN32
#include <Windows.h>
#endif

#include "FreetypeWrapper.h"
#include <stdexcept>
#include <string>
#include <cmath>
#include <memory>
#include <vector>
#include <mutex>
#include <regex>

using namespace util;

FreetypeWrapper* FreetypeWrapper::m_SingletonFreeType = nullptr;


//TODO: add support for multiple fonts - wrapper will then perhaps have one instance for each font
/*Create a new instance if one doesn't exist already, else return existing*/
FreetypeWrapper * util::FreetypeWrapper::init(std::string fontName, double fontSizeNDC, COLORREF fontColor, OglWindow * window, double interLineSpacingNDC)
{
    static std::mutex m;//C++11 memory model will guarantee creation of m without races
    m.lock();
    if (nullptr == m_SingletonFreeType)
    {
        m_SingletonFreeType = new FreetypeWrapper(fontName, fontSizeNDC, fontColor, window, interLineSpacingNDC);
    }
    m.unlock();
    return m_SingletonFreeType;
}

FreetypeWrapper::FreetypeWrapper(std::string fontName, double fontSizeNDC, COLORREF fontColor, OglWindow *windowObject, double interLineSpacing)
{
    m_WindowObject = windowObject;
    m_fontSizeNDC = fontSizeNDC;
    m_FontRGBA = fontColor;

    FT_Library freeTypeLib;

    if (FT_Init_FreeType(&freeTypeLib) != 0)
    {
        throw std::runtime_error("FT_Init_FreeType failed");
    }

    FT_Face fontFace;
    if (0!=FT_New_Face(freeTypeLib, fontName.c_str(), 0, &fontFace))
    {
        throw std::runtime_error("FT_New_Face failed");
    }

    auto fontSizePixels = m_WindowObject->NDCToPixels(m_fontSizeNDC);
    FT_Set_Char_Size(fontFace, fontSizePixels << 6, 0, 96, 0);

    UINT textures[CharRangeToSupport];
    glGenTextures(CharRangeToSupport, textures);

    for (unsigned char c = 1; c <= CharRangeToSupport; c++)
    {
        m_characterProperties[c].texture = textures[c];
        m_characterProperties[c].advanceXPixels = (fontFace->glyph->advance.x >> 6);
        MakeTexture(fontFace, c);
    }

    FT_Done_Face(fontFace);
    FT_Done_FreeType(freeTypeLib);
}

bool FreetypeWrapper::MakeTexture(FT_Face& fontFace, unsigned char c)
{
    if (FT_Load_Glyph(fontFace, FT_Get_Char_Index(fontFace, c), FT_LOAD_DEFAULT))
    {
        throw std::runtime_error("FT_Load_Glyph() failed");
    }

    FT_Glyph glyph;
    if (FT_Get_Glyph(fontFace->glyph, &glyph))
    {
        throw std::runtime_error("FL_Get_Glyph() failed");
    }

    //0 means success
    if (FT_Glyph_To_Bitmap(&glyph, ft_render_mode_normal, 0, 1))
    {
        throw std::runtime_error("FT_Glyph_To_Bitmap() failed");
    }

    FT_BitmapGlyph bitmap_glyph = (FT_BitmapGlyph)glyph;

    m_characterProperties[c].bearingYPixels = bitmap_glyph->top;
    m_characterProperties[c].bearingXPixels = bitmap_glyph->left;

    FT_Bitmap& bitmap = bitmap_glyph->bitmap;

    //Texture dimentions must be a power of 2
    m_characterProperties[c].expandedWidthPixels = pow(2, ceil(log2(bitmap.width)));
    m_characterProperties[c].expandedHeightPixels = pow(2, ceil(log2(bitmap.rows)));


    std::unique_ptr<GLubyte[]> expanded_data(new GLubyte[2 * m_characterProperties[c].expandedWidthPixels*m_characterProperties[c].expandedHeightPixels]);

    for (int j = 0; j < m_characterProperties[c].expandedHeightPixels; j++)
    {
        for (int i = 0; i < m_characterProperties[c].expandedWidthPixels; i++)
        {
            /*https://www.gamedev.net/forums/topic/612246-bug-in-the-freetype-opengl-tutorial/*/
            expanded_data[2 * (i + j * m_characterProperties[c].expandedWidthPixels)] = 255;
            expanded_data[2 * (i + j * m_characterProperties[c].expandedWidthPixels) + 1] = (i >= bitmap.width || j >= bitmap.rows) ? 0 : bitmap.buffer[i + bitmap.width*j];
        }
    }

    glBindTexture(GL_TEXTURE_2D, m_characterProperties[c].texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, m_characterProperties[c].expandedWidthPixels, m_characterProperties[c].expandedHeightPixels, 0, GL_LUMINANCE_ALPHA, GL_UNSIGNED_BYTE, expanded_data.get());

    m_characterProperties[c].originalWidthPixels = bitmap.width;
    m_characterProperties[c].originalHeightPixels = bitmap.rows;

    if (m_characterProperties[c].originalHeightPixels > m_maxHeightPixels)
    {
        m_maxHeightPixels = m_characterProperties[c].originalHeightPixels;
    }

    return true;
}

FreetypeWrapper::~FreetypeWrapper()
{
    std::vector<UINT> textures;//It will be perhaps faster to delete all in a single GPU operation, rather than making multiple calls
    for (auto& chr : m_characterProperties)
    {
        textures.push_back(chr.texture);
        chr.texture = 0;
    }
    glDeleteTextures(textures.size(), textures.data());
}

void util::FreetypeWrapper::processCommand(std::string commandBuffer)
{
    /*IMPORTANT - Save and restore state in printft, for these commands.
                    Else command in one string will affect others.
    */
    /*Commands :*/
    //1. empty
    //    -> empty command, simply return
    //2. COLORREF:RGBA(123,255,101,128)
    //    -> color values 0-255
    /*-> Regex :*/ auto color_regex = std::regex(R"(COLORREF:RGBA\(([0-9]{1,3}),([0-9]{1,3}),([0-9]{1,3}),([0-9]{1,3})\))");
    
    if (commandBuffer.empty())
    {
        return;
    }
    std::smatch matches;
    if (std::regex_search(commandBuffer, matches, color_regex))
    {
        int red = std::stoi(matches[1].str());      ASSERT(red      <= 255 && red   >= 0, "Color value out of range");
        int green = std::stoi(matches[2].str());    ASSERT(green    <= 255 && green >= 0, "Color value out of range");
        int blue = std::stoi(matches[3].str());     ASSERT(blue     <= 255 && blue  >= 0, "Color value out of range");
        int alpha = std::stoi(matches[4].str());    ASSERT(alpha    <= 255 && alpha >= 0, "Color value out of range");
        SetFontColor(RGBA(red, green, blue, alpha));
        return;
    }
    //reached here without being processed
    throw std::runtime_error("Malformed command");
}

void FreetypeWrapper::OutputSingleChar(char c, GLfloat xpos, GLfloat ypos)
{
    GLfloat aspectRatio = m_characterProperties[c].originalWidthPixels/m_characterProperties[c].originalHeightPixels;

    auto charBitmapWidthNDC /*part of bitmap which actually contains character bits (due toexpanded bitmap)*/ = m_characterProperties[c].originalWidthPixels / m_characterProperties[c].expandedWidthPixels;
    auto charBitmapHeightNDC /*part of bitmap which actually contains character bits (due toexpanded bitmap)*/ = m_characterProperties[c].originalHeightPixels / m_characterProperties[c].expandedHeightPixels;
    
    //m_maxHeightPixels -> m_fontSizeNDC
    //originalHeightPixels -> (m_fontSizeNDC/m_maxHeightPixels)*originalHeightPixels;
    auto quadHeightNDC = (m_fontSizeNDC / m_maxHeightPixels)*m_characterProperties[c].originalHeightPixels;
    auto quadWidthtNDC = quadHeightNDC*aspectRatio;
    double yTrans = m_WindowObject->PixelsToNDC(m_characterProperties[c].bearingYPixels - m_characterProperties[c].originalHeightPixels);
    yTrans += -(m_fontSizeNDC - quadHeightNDC);//move donw => along minus y direction
    double xTrans = m_WindowObject->PixelsToNDC(m_characterProperties[c].bearingXPixels);
    glBindTexture(GL_TEXTURE_2D, m_characterProperties[c].texture);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glTranslatef(xTrans, yTrans, 0);
        glBegin(GL_QUADS);
            glColor4f(GetRValuef(m_FontRGBA), GetGValuef(m_FontRGBA), GetBValuef(m_FontRGBA), GetAValuef(m_FontRGBA));
            glTexCoord2f(charBitmapWidthNDC, 0);  glVertex2f(0, 0);
            glTexCoord2f(charBitmapWidthNDC, charBitmapHeightNDC);                         glVertex2f(0, -quadHeightNDC);
            glTexCoord2f(0, charBitmapHeightNDC);              glVertex2f(-quadWidthtNDC, -quadHeightNDC);
            glTexCoord2f(0, 0);  glVertex2f(-quadWidthtNDC, 0);
        glEnd();
    glPopMatrix();
    auto advanceXNDC = m_WindowObject->PixelsToNDC(m_characterProperties[c].advanceXPixels);
    glTranslatef(advanceXNDC, 0, 0);
}

void util::FreetypeWrapper::SetInterLineSpacing(double spaceNDC)
{
    m_interLineSpacing = spaceNDC;
}

void util::FreetypeWrapper::SetFontColor(COLORREF rgba)
{
    m_FontRGBA = rgba;
}

COLORREF util::FreetypeWrapper::GetFontColor()
{
    return m_FontRGBA;
}
