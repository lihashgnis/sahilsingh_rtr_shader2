#pragma once
#ifdef _WIN32
#include <Windows.h>
#endif
#include "UtilCrossPlatform.hpp"
#include <ft2build.h>
#include <freetype/freetype.h>
#include <freetype/ftglyph.h>
#include <freetype/ftoutln.h>
#include <freetype/fttrigon.h>
#include <map>
#include <sstream>
#include <stdexcept>

//We will have chars from 1 to range supported
#define CharRangeToSupport 127

namespace util 
{
    class FreetypeWrapper
    {
        struct charProps
        {
            char c = 0;
            double originalWidthPixels = 0;
            double originalHeightPixels = 0;
            double expandedWidthPixels = 0;
            double expandedHeightPixels = 0;
            double bearingYPixels = 0; //See - https://learnopengl.com/img/in-practice/glyph.png
            double bearingXPixels = 0;
            double advanceXPixels = 0;
            UINT texture = 0;

        };

        charProps m_characterProperties[CharRangeToSupport+1];//since char range is [1, CharRangeToSupport]
        /*Out of all the characters we are supporting,
            which one has maximum height? The font height
            specified by user in NDC should refer to this heigh,
            and other heights shoud be scaled proportionally*/
        double m_maxHeightPixels = 0;
        double m_interLineSpacing = 0.01;

        double m_fontSizeNDC;
        OglWindow* m_WindowObject;//store window objetc to get window specific information - like X, and Y NDC extents
        bool MakeTexture(FT_Face & fontFace, unsigned char c);
        void OutputSingleChar(char c, GLfloat xpos, GLfloat ypos);
        COLORREF m_FontRGBA = RGBA(255,255,255,255);

        //base case of variadic template
        template<typename T>
        std::stringstream printft_internal(T value)
        {
            std::stringstream ss;
            ss << value;
            return ss;
        }

        template<typename T, typename ...Args>
        std::stringstream printft_internal(T value, Args... args)
        {
            std::stringstream ss;
            ss << value << printft_internal(args...).str();
            return ss;
        }

    public:
        void SetInterLineSpacing(double spaceNDC);
        void SetFontColor(COLORREF rgba);
        COLORREF GetFontColor();

        //Print the things specified by args, at the given position
        template<typename ...Args>
        void printft(float xpos, float ypos, Args... args)
        {
            /*Since this is a singleton class, commands of one
            string can change another - for eg. color. we do not want that.
            save and restore previous states after command processing.
            */
            bool commandStarted = false;
            std::string commandBuffer;
            int ignoreCharsForCommand = 0;//number of chars next in stream which we do not want to process for commands

            //State save
            auto fontCol = GetFontColor();

            glMatrixMode(GL_MODELVIEW);
            glPushMatrix();
            glTranslatef(xpos, ypos, 0);
                std::stringstream ss;
                ss << printft_internal(args...).str();
                int lineNum = 0;
                for (char& s : ss.str())
                {
                    if ('\n' == s)
                    {//new line
                        lineNum++;
                        glPopMatrix();
                        glPushMatrix();
                        glTranslatef(xpos, ypos, 0);
                        glTranslatef(0, -(m_fontSizeNDC + m_interLineSpacing)*lineNum, 0);
                    }
                    else if ('\\' == s)
                    {
                        ignoreCharsForCommand++;//ignore char in stream for command processing
                        if (ignoreCharsForCommand > 1)
                        {
                            OutputSingleChar('\\', 0, 0);
                            ignoreCharsForCommand--;
                        }
                    }
                    else if (ignoreCharsForCommand > 0)
                    {
                        OutputSingleChar(s, 0, 0);
                        ignoreCharsForCommand--;
                    }
                    else if ('[' == s)//command has started
                    {
                        commandStarted = true;
                    }
                    else if (']' == s)//command ended
                    {
                        if (false == commandStarted)
                        {
                            throw std::runtime_error(R"(Got command end marker before command start marker. To print markers use escape sequences \[ \])");
                        }
                        else
                        {
                            commandStarted = false;
                            processCommand(commandBuffer);
                            commandBuffer.clear();
                        }
                    }
                    else
                    {
                        if (true == commandStarted)
                        {
                            commandBuffer.push_back(s);
                        }
                        else
                        {
                            OutputSingleChar(s, 0, 0);
                        }
                    }
                }
            glPopMatrix();
            SetFontColor(fontCol);
        }

        /*Create a new instance if one doesn't exist already, else return existing*/
        static FreetypeWrapper* init(std::string fontName, double fontSizeNDC, COLORREF fontColor, OglWindow *window, double interLineSpacingNDC = 0.01);
    protected:
#pragma region enforce singleton
        /*Constructor not public to enforce singleton class - only one instance should exist for a program - use init() instead of direct construction*/
        //Needs access to window object to get window information
        FreetypeWrapper(std::string fontName, double fontSizeNDC, COLORREF fontColor, OglWindow *window, double interLineSpacingNDC = 0.01);
        ~FreetypeWrapper();
        static FreetypeWrapper* m_SingletonFreeType;
#pragma endregion
        void processCommand(std::string commandBuffer);
    };
}