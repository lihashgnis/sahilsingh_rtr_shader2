#include "LinuxCompat.h"
#include "UtilCrossPlatform.hpp"
#include <iostream>

using namespace util;

#ifdef __linux__
void WindowProps::SetDisplay(::Display* pd)
{
    pDisplay = pd;

    if (nullptr == pDisplay)
    {
        mask = mask & (~WndMask(LNX_PDISPLAY));
    }
    else
    {
        mask |= WndMask(LNX_PDISPLAY);
    }
}

::Display* WindowProps::GetDisplay() const
{
    if (WndMask(LNX_PDISPLAY) != (mask & WndMask(LNX_PDISPLAY)))
    {
        THROW_WINDOWPROP_PARAMETER_MISSING
    }
    return pDisplay;
}

void WindowProps::SetXVisualInfo(std::shared_ptr<XVisualInfo> pXV)
{
    pXVisualInfo = pXV;
    if (nullptr == pXVisualInfo)
    {
        mask = mask & (~WndMask(LNX_PXVISUALINFO));
    }
    else
    {
        mask |= WndMask(LNX_PXVISUALINFO);
    }
}

std::shared_ptr<XVisualInfo> WindowProps::GetXVisualInfo() const
{
    if (WndMask(LNX_PXVISUALINFO) != (mask & WndMask(LNX_PXVISUALINFO)))
    {
        THROW_WINDOWPROP_PARAMETER_MISSING
    }
    return pXVisualInfo;
}

void WindowProps::SetGLXContext(GLXContext pGC)
{
    pGlxContext = pGC;
    if (nullptr == pGlxContext)
    {
        mask = mask & (~WndMask(LNX_GLXContext));
    }
    else
    {
        mask |= WndMask(LNX_GLXContext);
    }
}

GLXContext WindowProps::GetGLXContext() const
{
    if (WndMask(LNX_GLXContext) != (mask & WndMask(LNX_GLXContext)))
    {
        THROW_WINDOWPROP_PARAMETER_MISSING
    }
    return pGlxContext;
}

void WindowProps::SetWindow(Window w)
{
    window = w;

    if (0 == window)
    {
        mask = mask & (~WndMask(LNX_WINDOW));
    }
    else
    {
        mask |= WndMask(LNX_WINDOW);
    }
}

Window WindowProps::GetWindow() const
{
    if (WndMask(LNX_WINDOW) != (mask & WndMask(LNX_WINDOW)))
    {
        THROW_WINDOWPROP_PARAMETER_MISSING
    }
    return window;
}


void WindowProps::SetColorMap(Colormap cm)
{
    colorMap = cm;

    if (0 == colorMap)
    {
        mask = mask & (~WndMask(LNX_COLORMAP));
    }
    else
    {
        mask |= WndMask(LNX_COLORMAP);
    }
}

Colormap WindowProps::GetColorMap() const
{
    if (WndMask(LNX_COLORMAP) != (mask & WndMask(LNX_COLORMAP)))
    {
        THROW_WINDOWPROP_PARAMETER_MISSING
    }
    return colorMap;
}

void WindowProps::SetGLXFBConfig(GLXFBConfig fbConfig)
{
    glxFBConfig = fbConfig;
}

GLXFBConfig WindowProps::GetGLXFBConfig()
{
    if (!glxFBConfig)
    {
        THROW_WINDOWPROP_PARAMETER_MISSING
    }

    return glxFBConfig.value();
}
#endif

#ifdef _WIN32
void WindowProps::SetHWND(HWND hwnd)
{
    hWnd = hwnd;
    if (NULL == hWnd)
    {
        mask = mask & (~WndMask(WND_HWND));
    }
    else
    {
        mask |= WndMask(WND_HWND);
    }
}

HWND WindowProps::GetHWND() const
{
    if (WndMask(WND_HWND) != (mask & WndMask(WND_HWND)))
    {
        THROW_WINDOWPROP_PARAMETER_MISSING
    }
    return hWnd;
}
#endif

//common
void WindowProps::SetOGLWindow(OglWindow* oglwnd)
{
    oglWindow = oglwnd;
}

OglWindow* WindowProps::GetOGLWindow()
{
    if (!oglWindow)
    {
        THROW_WINDOWPROP_PARAMETER_MISSING
    }
    return oglWindow.value();
}

void util::WindowProps::SetInitialized()
{
    bInitialized = true;
}

bool util::WindowProps::GetInitialized()
{
    return bInitialized;
}

void util::WindowProps::SetWindowProc(util::WindowProps::WNDPROC proc)
{
    wndProc = proc;
}

util::WindowProps::WNDPROC util::WindowProps::GetWindowProc()
{
    if (!wndProc)
    {
        THROW_WINDOWPROP_PARAMETER_MISSING
    }

    return wndProc.value();
}

#ifdef __linux__
void PostQuitMessage(_In_ int nExitCode)
{
    //Not implemented on Linux - peekmessage takes care of this functionality
    THROW_NOT_IMPLEMENTED
}

BOOL TranslateMessage(_In_ MSG* lpMsg)
{
    XEvent xev = lpMsg->xevent;
    KeySym keysym;

    int wmKey = 0;
    bool bIsWmCharMsg = false;//will cause additional dispatches if true

    //TODO fill following 2 members with correct value if needed in future -  also see bIsWmCharMsg if condition below
    lpMsg->time = 0;
    lpMsg->pt = {};

    switch (xev.type)
    {
    case MapNotify:
    {
        lpMsg->message = WM_CREATE;
        lpMsg->wParam = 0;//Not used in WM_CREATE;
        lpMsg->lParam = (LPARAM)nullptr;//pointer to create struct structure
    }
    break;
    case FocusIn:
    {
        lpMsg->message = WM_SETFOCUS;
        lpMsg->wParam = 0;//used but not filling right now - TODO://fill this info if necessary
        lpMsg->lParam = 0;//Not used
    }
    break;
    case FocusOut:
    {
        lpMsg->message = WM_KILLFOCUS;
        lpMsg->wParam = 0;//used but not filling right now - TODO://fill this info if necessary
        lpMsg->lParam = 0;//Not used
    }
    break;
    case KeyPress:
    {
        lpMsg->message = WM_KEYDOWN;
        lpMsg->lParam = 0;//TODO: fill this member; not using right now - https://docs.microsoft.com/en-us/windows/win32/inputdev/wm-keydown
        keysym = XkbKeycodeToKeysym(lpMsg->wndProps.GetDisplay(), xev.xkey.keycode, 0, 0);

        if (keysym == NoSymbol)
        {
            throw std::runtime_error("XkbKeycodeToKeysym() failed");
        }

        if ((keysym >= XK_A && keysym <= XK_Z) || (keysym >= XK_a && keysym <= XK_z) || (keysym >= XK_0 && keysym <= XK_9))
        {
            bIsWmCharMsg = true;
            wmKey = keysym;
        }

        switch (keysym)
        {
        case XK_Escape:
            wmKey = VK_ESCAPE;
            break;
        default:
            break;
        }
        lpMsg->wParam = wmKey;
    }
    break;
    case ButtonPress:
    {
        switch (xev.xbutton.button)
        {
        case Button1://Left mouse button
        {
            lpMsg->message = WM_LBUTTONDOWN;
            lpMsg->wParam = 0; //TODO: fill this if required
            //lpMsg->lParam = (xev.xbutton.y) << 16 | (xev.xbutton.x);//TODO: on windows the coords are relative to client area, here x, and y are absolute coordintes
            lpMsg->lParam = MAKELPARAM(xev.xbutton.x, xev.xbutton.y);//TODO: on windows the coords are relative to client area, here x, and y are absolute coordintes
        }
        break;
        case Button2://Middle mouse button
        {
            lpMsg->message = WM_MBUTTONDOWN;
            lpMsg->wParam = 0; //TODO: fill this if required
            //lpMsg->lParam = (xev.xbutton.y) << 16 | (xev.xbutton.x);//TODO: on windows the coords are relative to client area, here x, and y are absolute coordintes
            lpMsg->lParam = MAKELPARAM(xev.xbutton.x, xev.xbutton.y);//TODO: on windows the coords are relative to client area, here x, and y are absolute coordintes
        }
        break;
        case Button3://Right mouse button
        {
            lpMsg->message = WM_RBUTTONDOWN;
            lpMsg->wParam = 0; //TODO: fill this if required
            //lpMsg->lParam = (xev.xbutton.y) << 16 | (xev.xbutton.x);//TODO: on windows the coords are relative to client area, here x, and y are absolute coordintes
            lpMsg->lParam = MAKELPARAM(xev.xbutton.x, xev.xbutton.y);//TODO: on windows the coords are relative to client area, here x, and y are absolute coordintes
        }
        break;
        default:
            break;
        }
    }
    break;
    case MotionNotify://mouse move
    {
        lpMsg->message = WM_MOUSEMOVE;
        lpMsg->wParam = 0; //TODO: fill this if required
        //lpMsg->lParam = (xev.xbutton.y) << 16 | (xev.xbutton.x);//TODO: on windows the coords are relative to client area, here x, and y are absolute coordintes
        lpMsg->lParam = MAKELPARAM(xev.xbutton.x, xev.xbutton.y);//TODO: on windows the coords are relative to client area, here x, and y are absolute coordintes
    }
    break;
    case ConfigureNotify://change in such as size, position, border width, and stacking order
    {
        //Generate WM_SIZE, and WM_MOVE

        int x = xev.xconfigure.x;//relative to parent window
        int y = xev.xconfigure.y;//relative to parent window
        int winWidth = xev.xconfigure.width;
        int winHeight = xev.xconfigure.height;

        //WM_SIZE
        lpMsg->message = WM_SIZE;
        lpMsg->wParam = 0; //TODO: fill this if required
        //lpMsg->lParam = (winHeight) << 16 | (winWidth);
        lpMsg->lParam = MAKELPARAM(winWidth, winHeight);
        DispatchMessage(lpMsg);

        //WM_MOVE
        lpMsg->message = WM_MOVE;
        lpMsg->wParam = 0; //Not used
        lpMsg->lParam = (y) << 8 | (x);
        DispatchMessage(lpMsg);

    }
    break;
    case Expose://WM_PAINT
    {
        lpMsg->message = WM_PAINT;
        lpMsg->wParam = 0; //Not used
        lpMsg->lParam = 0; //Not used
    }
    break;
    default:
        break;
    }

    if (bIsWmCharMsg)
    {
        MSG msg = {};
        msg.message = WM_CHAR;
        msg.wParam = wmKey;
        msg.lParam = 0; //TODO: fill this if required

        //TODO fill following 2 members with correct value if needed in future
        msg.time = 0;
        msg.pt = {};

        DispatchMessage(&msg);
        return TRUE;
    }
    else if (WM_KEYDOWN == lpMsg->message || WM_KEYUP == lpMsg->message || WM_SYSKEYDOWN == lpMsg->message || WM_SYSKEYUP == lpMsg->message)
    {
        return TRUE; //See https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-translatemessage#return-value
    }
    else
    {
        return FALSE;
    }
}

LRESULT DispatchMessage(const MSG* lpMsg)
{
    auto wp = globalWindowPropsCache.GetValue(PARENT_OBJECT);
    if (!wp)
    {
        THROW_WINDOWPROP_PARAMETER_MISSING
    }

    util::WindowProps::WNDPROC wndProc = wp->GetWindowProc();
    //prototype : LRESULT(WindowProps&, UINT, WPARAM, LPARAM)
    wndProc(wp.value(), lpMsg->message, lpMsg->wParam, lpMsg->lParam);
    return LRESULT();
}

BOOL PeekMessage(_Out_ MSG* lpMsg, _In_opt_ HWND, _In_ UINT, _In_ UINT, _In_ UINT)
{
    //initialize with null
    lpMsg->message = WM_NULL;
    lpMsg->wParam = 0;
    lpMsg->lParam = 0;

    auto wp = globalWindowPropsCache.GetValue(PARENT_OBJECT);
    if (!wp)
    {
        THROW_WINDOWPROP_PARAMETER_MISSING
    }

    lpMsg->wndProps = wp.value();

    int numPending = XPending(lpMsg->wndProps.GetDisplay());

    if (numPending > 0)
    {
        XEvent xev;
        XNextEvent(lpMsg->wndProps.GetDisplay(), &xev);
        lpMsg->xevent = xev;
        switch (xev.type)
        {
        case 33://WM_CLOSE //TODO: have an implementation of this on the lines of windows's implementation
        {

        }
        [[fallthrough]] ;
        case DestroyNotify://WM_DESTROY
        {
            lpMsg->message = WM_QUIT;
        }
        break;
        default:
            break;
        }
        return TRUE;
    }
    else
        return FALSE;
}
#endif

bool util::SwapBuffersCompat()
{
    auto wp = globalWindowPropsCache.GetValue(PARENT_OBJECT);
    if (!wp)
    {
        THROW_WINDOWPROP_PARAMETER_MISSING
    }

#ifdef _WIN32
    HDC hDC = GetDC(wp->GetHWND());
    if (NULL == hDC)
    {
        throw std::runtime_error("Couldn't get DC");
    }
    SwapBuffers(hDC);
#endif
#ifdef __linux__
    glXSwapBuffers(wp->GetDisplay(), wp->GetWindow());
#endif
    return true;
}

util::WindowPropsCache util::globalWindowPropsCache;

void util::WindowPropsCache::AddToCache(std::string key, WindowProps& val, util::WindowPropsCache::CacheOperationType opType /*= WindowPropsCache::CacheOperationType::UNIQUE_VALUE*/)
{
    if (opType == util::WindowPropsCache::CacheOperationType::UNIQUE_VALUE)
    {
        if (m_wndProps.end() != m_wndProps.find(key))
        {
            throw std::runtime_error("duplicate entry!");
        }
    }
    m_wndProps[key] = val;
}

std::optional<WindowProps> util::WindowPropsCache::GetValue(std::string key)
{
    if (m_wndProps.end() != m_wndProps.find(key))
    {
        return m_wndProps[key];
    }
    return std::nullopt;
}

void util::WindowPropsCache::EraseValue(std::string key)
{
    auto itr = m_wndProps.find(key);
    if (m_wndProps.end() != itr)
    {
        m_wndProps.erase(itr);
    }
}
