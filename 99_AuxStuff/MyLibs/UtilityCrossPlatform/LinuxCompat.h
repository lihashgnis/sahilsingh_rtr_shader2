#pragma once
/*
Helper stuff for Linux/Windows cross compatibility
*/

#ifdef _WIN32
#include<Windows.h>
#pragma comment(linker,"/subsystem:windows")
#endif

//common
#include <map>
#include <optional>
#include <functional>
#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>

#ifdef __linux__
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>
#include <GL/glx.h>
#include <memory>
#include <cstddef>

#define WinMain(...) main()

typedef unsigned int UINT;
typedef __uint32_t DWORD;
typedef __uint16_t WORD;

typedef __uint8_t  BYTE;

/*C:\Program Files (x86)\Windows Kits\10\Include\10.0.17763.0\shared\basetsd.h*/
#if defined(_WIN64) 
typedef __uint64_t INT_PTR, * PINT_PTR;
typedef unsigned __uint64_t UINT_PTR, * PUINT_PTR;

typedef __uint64_t LONG_PTR, * PLONG_PTR;
typedef unsigned __uint64_t ULONG_PTR, * PULONG_PTR;

#define __int3264   __int64

#else
typedef int INT_PTR, * PINT_PTR;
typedef unsigned int UINT_PTR, * PUINT_PTR;

typedef long LONG_PTR, * PLONG_PTR;
typedef unsigned long ULONG_PTR, * PULONG_PTR;

#define __int3264   __int32

#endif

typedef ULONG_PTR DWORD_PTR, * PDWORD_PTR;

/*C:\Program Files (x86)\Windows Kits\10\Include\10.0.17763.0\shared\minwindef.h*/
typedef UINT_PTR            WPARAM;
typedef LONG_PTR            LPARAM;
typedef LONG_PTR            LRESULT;

typedef int                 BOOL;
#ifndef FALSE
#define FALSE               0
#endif
#ifndef TRUE
#define TRUE                1
#endif

#define MAKELONG(a, b)      ((LONG)(((WORD)(((DWORD_PTR)(a)) & 0xffff)) | ((DWORD)((WORD)(((DWORD_PTR)(b)) & 0xffff))) << 16))

/*C:\Program Files (x86)\Windows Kits\10\Include\10.0.17134.0\um\winnt.h*/
typedef char CHAR;
typedef short SHORT;
typedef long LONG;

#define CALLBACK  __stdcall

#define MAKEWORD(a, b)      ((WORD)(((BYTE)(((DWORD_PTR)(a)) & 0xff)) | ((WORD)((BYTE)(((DWORD_PTR)(b)) & 0xff))) << 8))
#define MAKELONG(a, b)      ((LONG)(((WORD)(((DWORD_PTR)(a)) & 0xffff)) | ((DWORD)((WORD)(((DWORD_PTR)(b)) & 0xffff))) << 16))
#define LOWORD(l)           ((WORD)(((DWORD_PTR)(l)) & 0xffff))
#define HIWORD(l)           ((WORD)((((DWORD_PTR)(l)) >> 16) & 0xffff))
#define LOBYTE(w)           ((BYTE)(((DWORD_PTR)(w)) & 0xff))
#define HIBYTE(w)           ((BYTE)((((DWORD_PTR)(w)) >> 8) & 0xff))

#define DECLARE_HANDLE(name) struct name##__{int unused;}; typedef struct name##__ *name

/*C:\Program Files (x86)\Windows Kits\10\Include\10.0.17763.0\shared\windef.h*/
typedef DWORD   COLORREF;

DECLARE_HANDLE(HWND);

/*C:\Program Files (x86)\Windows Kits\10\Include\10.0.17763.0\um\wingdi.h*/
#define RGB(r,g,b)          ((COLORREF)(((BYTE)(r)|((WORD)((BYTE)(g))<<8))|(((DWORD)(BYTE)(b))<<16)))
#define GetRValue(rgb)      (LOBYTE(rgb))
#define GetGValue(rgb)      (LOBYTE(((WORD)(rgb)) >> 8))
#define GetBValue(rgb)      (LOBYTE((rgb)>>16))

#ifdef _UNICODE
typedef wchar_t TCHAR;
#define TEXT(x) L##x
#define _T(x) L##x
#else
typedef char TCHAR;
#define TEXT(x) x
#define _T(x) x
#endif

#define _In_ 
#define _Out_
#define _Out_opt_
#define _In_opt_

#endif //__linux__

//MACROS
#define WndMask(x) ((UINT)(util::WindowProps::WindowPropValidMembers:: x ))
#ifndef NDEBUG
#define THROW_WINDOWPROP_PARAMETER_MISSING {throw std::runtime_error("WindowProps parameter missing");}
#else
#define THROW_WINDOWPROP_PARAMETER_MISSING ;
#endif


//common
namespace util {
    class WindowProps;
    class OglWindow;

    class WindowPropsCache
    {
        std::map<std::string, WindowProps> m_wndProps;
    public:
        enum class CacheOperationType
        {
            UNIQUE_VALUE,
            REPLACE_VALUE
        };

        void AddToCache(std::string key, WindowProps& val, WindowPropsCache::CacheOperationType opType = WindowPropsCache::CacheOperationType::UNIQUE_VALUE);

        std::optional<WindowProps> GetValue(std::string key);

        void EraseValue(std::string key);
    };

    extern WindowPropsCache globalWindowPropsCache;


    //Works on both win32, and Linux
    bool SwapBuffersCompat();
}

namespace util {

    /*To move around the data associated with a window*/
    class WindowProps
    {
        //common
        int windowPoxX = 0;
        int windowPoxY = 0;
        int windowWidth = 0;
        int windowHeight = 0;
        std::optional<OglWindow*> oglWindow;
        bool bInitialized = false;//In OglWndProc() we should call derived class methods only once contructor has executed.
                                    //During constructor execution, the derived class methods are not there, and
                                    //base class methods are pure virtual, hence calling them will crash program

#ifdef _WIN32
    public:
        typedef WNDPROC WNDPROC;
    private:
        HWND hWnd = 0;
        UINT iMsg = 0;
        WPARAM wParam;
        LPARAM lParam = 0;
        std::optional <WNDPROC> wndProc;
        HINSTANCE hInstance = 0;
#endif

#ifdef __linux__
    public:
        typedef std::function<LRESULT(WindowProps&, UINT, WPARAM, LPARAM)> WNDPROC;
    private:
        ::Display* pDisplay;
        std::shared_ptr<XVisualInfo> pXVisualInfo;
        Colormap colorMap;
        Window window;
        GLXContext pGlxContext;
        std::optional<WNDPROC> wndProc;
        std::optional<GLXFBConfig> glxFBConfig;
#endif
    public:
#ifdef __linux__
        void SetDisplay(::Display* pd);
        ::Display* GetDisplay() const;
        void SetXVisualInfo(std::shared_ptr<XVisualInfo> pXV);
        std::shared_ptr<XVisualInfo> GetXVisualInfo() const;
        void SetGLXContext(GLXContext pGC);
        GLXContext GetGLXContext() const;
        void SetWindow(Window w);
        Window GetWindow() const;
        void SetColorMap(Colormap cm);
        Colormap GetColorMap() const;
        void SetGLXFBConfig(GLXFBConfig fbConfig);
        GLXFBConfig GetGLXFBConfig();
#endif
#ifdef _WIN32
        void SetHWND(HWND);
        HWND GetHWND() const;
#endif
        void SetOGLWindow(OglWindow* oglwnd);
        OglWindow* GetOGLWindow();

        void SetInitialized();
        bool GetInitialized();

        enum class WindowPropValidMembers : UINT {
            //Windows
            WND_HWND            = 0b1,
            WND_IMSG            = 0b10,
            WND_WPARAM          = 0b100,
            WND_LPARAM          = 0b1000,
            WND_WNDPROC         = 0b10000,
            WND_HINSTANCE       = 0b100000,
            //Linux
            LNX_PDISPLAY        = 0b1000000,
            LNX_PXVISUALINFO    = 0b10000000,
            LNX_COLORMAP        = 0b100000000,
            LNX_WINDOW          = 0b1000000000,
            LNX_GLXContext      = 0b10000000000,
        };

        UINT mask = 0;//which members are valid?

        WindowProps()
        {
            Reset();
        }

        void Reset()
        {
            oglWindow.reset();
            bInitialized = false;
#ifdef _WIN32
            hWnd = 0;
            iMsg = 0;
            wParam;
            lParam = 0;
            wndProc.reset();
            hInstance = 0;

#endif
#ifdef __linux__
            pDisplay = nullptr;
            pXVisualInfo.reset();
            Colormap colorMap = {};
            Window window = {};
            pGlxContext = nullptr;
            wndProc.reset();
#endif

            windowPoxX = 0;
            windowPoxY = 0;
            windowWidth = 0;
            windowHeight = 0;
        }

        void SetWindowProc(WNDPROC proc);

        WNDPROC GetWindowProc();
    };

}

#ifdef __linux__
/*C:\Program Files(x86)\Windows Kits\10\Include\10.0.17134.0\shared\windef.h*/
typedef struct tagPOINT
{
    LONG  x;
    LONG  y;
} POINT;

/*C:\Program Files (x86)\Windows Kits\10\Include\10.0.17134.0\um\WinUser.h*/
//windows messages
#define WM_NULL                         0x0000
#define WM_CREATE                       0x0001
#define WM_DESTROY                      0x0002
#define WM_MOVE                         0x0003
#define WM_SIZE                         0x0005
#define WM_SETFOCUS                     0x0007
#define WM_KILLFOCUS                    0x0008
#define WM_PAINT                        0x000F
#define WM_CLOSE                        0x0010
#define WM_QUIT                         0x0012
#define WM_ERASEBKGND                   0x0014
#define WM_KEYDOWN                      0x0100
#define WM_KEYUP                        0x0101
#define WM_CHAR                         0x0102
#define WM_SYSKEYDOWN                   0x0104
#define WM_SYSKEYUP                     0x0105
#define WM_MOUSEMOVE                    0x0200
#define WM_LBUTTONDOWN                  0x0201
#define WM_LBUTTONUP                    0x0202
#define WM_LBUTTONDBLCLK                0x0203
#define WM_RBUTTONDOWN                  0x0204
#define WM_RBUTTONUP                    0x0205
#define WM_RBUTTONDBLCLK                0x0206
#define WM_MBUTTONDOWN                  0x0207
#define WM_MBUTTONUP                    0x0208
#define WM_MBUTTONDBLCLK                0x0209

//key codes
#define VK_ESCAPE         0x1B
/*
 * VK_0 - VK_9 are the same as ASCII '0' - '9' (0x30 - 0x39)
 * 0x3A - 0x40 : unassigned
 * VK_A - VK_Z are the same as ASCII 'A' - 'Z' (0x41 - 0x5A)
 */

//Other constants
#define PM_REMOVE           0x0001

#define MAKELPARAM(l, h)      ((LPARAM)(DWORD)MAKELONG(l, h))

typedef struct tagMSG {
    util::WindowProps wndProps;
    UINT        message;
    WPARAM      wParam;
    LPARAM      lParam;
    DWORD       time;
    POINT       pt;
    XEvent xevent;
#ifdef _MAC
    DWORD       lPrivate;
#endif
} MSG;

//prototypes
LRESULT DispatchMessage(const MSG* lpMsg);

BOOL TranslateMessage(_In_ MSG* lpMsg);

BOOL PeekMessage(_Out_ MSG* lpMsg, _In_opt_ HWND, _In_ UINT, _In_ UINT, _In_ UINT);

void PostQuitMessage(_In_ int nExitCode);
#endif //__linux__