#ifdef USING_PRECOMPILED_HEADERS
#include "stdafx.h"
#endif

#include "UtilCrossPlatform.hpp"
#include "ProgrammablePipeline.h"

using namespace util;

CREATE_LOG()

#define NORMALIZE_VEC3(x, y, z) \
vmath::vec3 v1(x, y, z); \
auto v2 = vmath::normalize(v1); \
x = v2[0]; \
y = v2[1]; \
z = v2[2];

int PP::Lights::GetMaxLights()
{
    return MAX_LIGHTS;
}

std::string PP::Lights::GetLightVShaderCodeMainCode()
{
    const char* lighShaderPartialCode =
#include "OpenGL/Shaders/Misc/lightsCode.vert"

        return std::string(lighShaderPartialCode);
}


std::string PP::Lights::GetLightVShaderCodeVariables()
{
    const char* lighShaderPartialCode =
#include "OpenGL/Shaders/Misc/lightsVariables.vert"

        return std::string(lighShaderPartialCode);
}

std::string PP::Lights::GetLightFShaderCodeMainCode()
{
    const char* lighShaderPartialCode =
#include "OpenGL/Shaders/Misc/lightsCode.frag"

        return std::string(lighShaderPartialCode);
}

std::string PP::Lights::GetLightFShaderCodeVariables()
{
    const char* lighShaderPartialCode =
#include "OpenGL/Shaders/Misc/lightsVariables.frag"

        return std::string(lighShaderPartialCode);
}

//void PP::Lights::ConfigureLight(const LightProps& props)
//{
//    ConfigureLight(m_currentLight, props);
//}

util::ProgrammablePipeline::ObjectLightProps::ObjectLightProps() : ObjectLightProps(vmath::vec3(1,1,1), vmath::vec3(1, 1, 1), vmath::vec3(1, 1, 1), 0.0)
{
}


util::ProgrammablePipeline::ObjectLightProps::ObjectLightProps(vmath::vec3 KA, vmath::vec3 KD, vmath::vec3 KS, float shininess)
{
	m_KA = KA;
	m_KD = KD;
	m_KS = KS;
    m_shininess = shininess;
}		   

util::ProgrammablePipeline::LightProps::LightProps() : LightProps(vmath::vec4(0,0,0,0), vmath::vec3(0, 0, 0), vmath::vec3(0, 0, 0), vmath::vec3(0, 0, 0))
{
}

util::ProgrammablePipeline::LightProps::LightProps(vmath::vec4 lightPosition, vmath::vec3 LA, vmath::vec3 LD, vmath::vec3 LS)
{
    m_LightPosition = lightPosition;
    m_LA = LA;
    m_LD = LD;
    m_LS = LS;
}

void PP::Lights::ConfigureLight(const unsigned int lightNum, const LightProps& props)
{
    if (lightNum > MAX_LIGHTS)
    {
        THROW_RUNTIME_ERROR("lightNum > MAX_LIGHTS");
    }

    m_Lights[lightNum] = props;
    m_bLightConfigured.set(lightNum, true);
}

//void PP::Lights::SetCurrentLight(const unsigned int lightNum)
//{
//    if (lightNum > MAX_LIGHTS)
//    {
//        THROW_RUNTIME_ERROR("lightNum > MAX_LIGHTS");
//    }
//
//    //m_currentLight = lightNum;
//}


bool PP::Lights::EnableLight(const unsigned int lightNum, bool bEnable)
{
    if (lightNum > MAX_LIGHTS)
    {
        THROW_RUNTIME_ERROR("lightNum > MAX_LIGHTS");
    }

    if (!m_bLightConfigured.test(lightNum))
    {
        THROW_RUNTIME_ERROR("Light must be configured before enabling it");
    }

    bool prevState = m_bLightEnabled.test(lightNum);
    m_bLightEnabled.set(lightNum, bEnable);
    return prevState;
}

void PP::Lights::SetGlobalLightSupportPP(bool bAreLightsSupported, unsigned int numLightsToUse)
{
    if (bAreLightsSupported && (0 == numLightsToUse))
    {
        THROW_RUNTIME_ERROR("If lights are to be suppored, then at least 1 light must be used");
    }

    m_bAreLightsSupported = bAreLightsSupported;
    m_numLightsToUse = numLightsToUse;
}

const std::vector<std::string> PP::Lights::GetLightUniforms(bool bIndividualArrayElements)
{
    if (true == bIndividualArrayElements)
    {
        static auto lightUniforms1 = GetOrFlushLightUniforms(false, bIndividualArrayElements);//static since uniforms may not change during run time
        return lightUniforms1;
    }
    else if (false == bIndividualArrayElements)
    {
        static auto lightUniforms2 = GetOrFlushLightUniforms(false, bIndividualArrayElements);//static since uniforms may not change during run time
        return lightUniforms2;
    }
    THROW_RUNTIME_ERROR("code should never reach here");
}

void PP::Lights::Flush()
{
    if (nullptr == m_pShaderContainer)
    {
        THROW_RUNTIME_ERROR("m_pShaderContainer pointer not set.");
    }
    GetOrFlushLightUniforms(true);
}

bool PP::Lights::GetGlobalLightSupportPP()
{
    return m_bAreLightsSupported;
}

PP::Lights::Lights()
{
    Reset();
}

PP::Lights::~Lights()
{

}


void PP::Lights::Reset()
{
    m_numLightsToUse = 0;
    m_bAreLightsSupported = false;
    m_bLightEnabled.reset();
    m_bLightConfigured.reset();
    m_Lights.reserve(MAX_LIGHTS);
    for (int i = 0; i < MAX_LIGHTS; i++)
    {
        m_Lights.push_back(LightProps());
    }
}

bool PP::Lights::GlobalLightSwich(bool bTurnOn)
{
    bool prevVal = m_bGlobalLightsSwitch;
    m_bGlobalLightsSwitch = bTurnOn;
    return prevVal;
}

void PP::Lights::SetShaderContainerPtr(util::ShaderCodesContainer* pShaderContainer)
{
    m_pShaderContainer = pShaderContainer;
}

const std::vector<std::string> PP::Lights::GetOrFlushLightUniforms(bool bFlush, bool bIndividualArrayElements)
{
    std::vector<std::string> uniforms = { "u_bGlobalLightSwitch", "u_numLightsToUse", "u_KA_vec3", "u_KD_vec3", "u_KS_vec3", "u_shininess"};

    if (bFlush == true)
    {
        //KA, KD, KS, u_shininess are sent through DrawObject(), and not here
        glUniform1i(m_pShaderContainer->GetUniformLocation("u_bGlobalLightSwitch"), m_bGlobalLightsSwitch?1:0);
        glUniform1i(m_pShaderContainer->GetUniformLocation("u_numLightsToUse"), m_numLightsToUse);
    }

    if (false == bIndividualArrayElements)
    {
        uniforms.push_back("u_bIsLightOn[MAX_LIGHTS]");
        uniforms.push_back("u_LA_vec3[MAX_LIGHTS]");
        uniforms.push_back("u_LD_vec3[MAX_LIGHTS]");
        uniforms.push_back("u_LS_vec3[MAX_LIGHTS]");
        uniforms.push_back("u_lightPosition_vec4[MAX_LIGHTS]");
        uniforms.push_back("u_default_color[MAX_LIGHTS]");
    }

    for (int i = 0; i < MAX_LIGHTS; i++)
    {
        if (true == bIndividualArrayElements)
        {
            uniforms.push_back("u_bIsLightOn[" + std::to_string(i) + "]");
            uniforms.push_back("u_LA_vec3[" + std::to_string(i) + "]");
            uniforms.push_back("u_LD_vec3[" + std::to_string(i) + "]");
            uniforms.push_back("u_LS_vec3[" + std::to_string(i) + "]");
            uniforms.push_back("u_lightPosition_vec4[" + std::to_string(i) + "]");
            uniforms.push_back("u_default_color[" + std::to_string(i) + "]");
        }

        if (i < m_numLightsToUse)
        {
            if (!m_bLightConfigured[i])
            {
                THROW_RUNTIME_ERROR("at least 1 light between 0 to (m_numLightsToUse-1) not configured");
            }
        }

        if (bFlush == true)
        {
            glUniform1i(m_pShaderContainer->GetUniformLocation("u_bIsLightOn[" + std::to_string(i) + "]"), m_bLightEnabled[i]);
            glUniform3f(m_pShaderContainer->GetUniformLocation("u_LA_vec3[" + std::to_string(i) + "]"), m_Lights[i].m_LA[0], m_Lights[i].m_LA[1], m_Lights[i].m_LA[2]);
            glUniform3f(m_pShaderContainer->GetUniformLocation("u_LD_vec3[" + std::to_string(i) + "]"), m_Lights[i].m_LD[0], m_Lights[i].m_LD[1], m_Lights[i].m_LD[2]);
            glUniform3f(m_pShaderContainer->GetUniformLocation("u_LS_vec3[" + std::to_string(i) + "]"), m_Lights[i].m_LS[0], m_Lights[i].m_LS[1], m_Lights[i].m_LS[2]);
            glUniform4f(m_pShaderContainer->GetUniformLocation("u_lightPosition_vec4[" + std::to_string(i) + "]"), m_Lights[i].m_LightPosition[0], m_Lights[i].m_LightPosition[1], m_Lights[i].m_LightPosition[2], m_Lights[i].m_LightPosition[3]);
			glUniform3f(m_pShaderContainer->GetUniformLocation("u_default_color[" + std::to_string(i) + "]"), m_Lights[i].m_default_color[0], m_Lights[i].m_default_color[1], m_Lights[i].m_default_color[2]);
        }
    }

    return uniforms;
}

PP::MatrixUniform4::MatrixUniform4() : MatrixUniform4::MatrixUniform4(-1)
{

}

PP::MatrixUniform4::MatrixUniform4(const GLint uniformLocation) : m_uniformLocation(uniformLocation)
{
    m_mat4State = vmath::mat4::identity();
}

PP::MatrixUniform4::MatrixUniform4(const GLint uniformLocation, vmath::mat4 matrixData) : MatrixUniform4::MatrixUniform4(uniformLocation)
{
    m_mat4State = matrixData;
}

void PP::MatrixUniform4::LoadMatrixData(MatrixUniform4 newMatrix)
{
    if ((m_mat4State.height() != newMatrix.m_mat4State.height()) || (m_mat4State.width() != newMatrix.m_mat4State.width()))
    {
        THROW_RUNTIME_ERROR("Matrix dimentions must match"); //probably will never be hit, but may be hit if code changes
    }

    m_mat4State = newMatrix.m_mat4State;
}

vmath::mat4 PP::MatrixUniform4::GetMatrixData()
{
    return m_mat4State;
}

void PP::MatrixUniform4::LoadMatrixData(vmath::mat4 newMatrix)
{
    if ((m_mat4State.height() != newMatrix.height()) || (m_mat4State.width() != newMatrix.width()))
    {
        THROW_RUNTIME_ERROR("Matrix dimentions must match"); //probably will never be hit, but may be hit if code changes
    }

    m_mat4State = newMatrix;
}

void PP::MatrixUniform4::Translate(GLfloat x, GLfloat y, GLfloat z)
{
    vmath::mat4 translateMatrix = vmath::translate(x, y, z);
    vmath::mat4 newMatrix = m_mat4State * translateMatrix;//order of multiplication - https://www.khronos.org/registry/OpenGL-Refpages/gl2.1/xhtml/glMultMatrix.xml
    m_mat4State = newMatrix;
}

void PP::MatrixUniform4::Rotate(GLfloat angle, GLfloat x, GLfloat y, GLfloat z)
{
    NORMALIZE_VEC3(x, y, z);

    vmath::mat4 rotateMatrix = vmath::rotate(angle, x, y, z);
    vmath::mat4 newMatrix = m_mat4State * rotateMatrix;
    m_mat4State = newMatrix;
}

void PP::MatrixUniform4::LoadIdentiy()
{
    m_mat4State = vmath::mat4::identity();
}

PP::MatrixUniform4 PP::MatrixUniform4::Perspective(float fovy, float aspect, float n, float f)
{
    PP::MatrixUniform4 newMatrix;
    newMatrix.LoadMatrixData(vmath::perspective(fovy, aspect, n, f));
    return newMatrix;
}

void PP::MatrixUniform4::Flush(const GLint uniformLocation)
{
    GLint uniformLoc = (m_uniformLocation == -1) ? uniformLocation : m_uniformLocation;
    
    if (uniformLoc == -1)
    {
        THROW_RUNTIME_ERROR("uniform location not known");
    }

    glUniformMatrix4fv(uniformLoc, 1, GL_FALSE, m_mat4State);
}

PP::MatrixUniform4 PP::MatrixUniform4::operator*(MatrixUniform4 matrix2)
{
    PP::MatrixUniform4 newMatrix;
    newMatrix.LoadMatrixData(this->m_mat4State * matrix2.m_mat4State);
    return newMatrix;
}
