#pragma once

/*
 * @brief Helper code for programmable pipeline (PP). Some helper code for PP already exists
 * in "UtilCrossPlatform.hpp", eg. ShaderCodesContainer, etc.
 * To prevent any more crowding of the already crowded "UtilCrossPlatform.hpp", this new file
 * is created.
 */

#include "Common.h"
#include "UtilCrossPlatform.hpp"
#include "../Logging/Logger.hpp"
#include "vmath.h"
#include <GL/gl.h>
#include <vector>
#include <bitset>

#define MAX_LIGHTS 10

namespace util
{
    namespace ProgrammablePipeline
    {
        /*
        * add delimiters so that version section can be identified, 
        * and other code doing string manipulation doesn't mess with it
        */
        struct ShaderCodeSectionDelimiters
        {
            std::string startDelimiter;
            std::string endDelimiter;
        };

        namespace ShaderCodeSectionDelimitersList
        {
            static const ShaderCodeSectionDelimiters versionSection = { "//JhrCN[Delimiter][VersionSectionStart]" , "//JhrCN[Delimiter][VersionSectionEnd]" };
            static const ShaderCodeSectionDelimiters lightVarsSection = { "//JhrCN[Delimiter][LightVariablesSectionStart]" , "//JhrCN[Delimiter][LightVariablesSectionEnd]" };

            //inside void main(){} body, marking the points wher code inside main starts and ends - helpful for code insertion inside main body
            static const ShaderCodeSectionDelimiters mainSection = { "//JhrCN[Delimiter][MainSectionStart]" , "//JhrCN[Delimiter][MainSectionEnd]" };
        };


        struct LightProps
        {
            vmath::vec4 m_LightPosition = {};
            vmath::vec3 m_LA = {};
            vmath::vec3 m_LD = {};
            vmath::vec3 m_LS = {};
            vmath::vec3 m_default_color = {1.0,1.0,1.0};//default color to give when all lights are off
            LightProps();
            LightProps(vmath::vec4 lightPosition, vmath::vec3 LA, vmath::vec3 LD, vmath::vec3 LS);
        };

        struct ObjectLightProps//to be used in object info
        {
            vmath::vec3 m_KA = {};
            vmath::vec3 m_KD = {};
            vmath::vec3 m_KS = {};
            float m_shininess;

			ObjectLightProps();
			ObjectLightProps(vmath::vec3 KA, vmath::vec3 KD, vmath::vec3 KS, float shininess);
        };

        class Lights
        {
        public:

            //void ConfigureLight(const LightProps& props);

            void ConfigureLight(const unsigned int lightNum, const LightProps& props);

            //void SetCurrentLight(const unsigned int lightNum); what does current light even mean?

            bool EnableLight(const unsigned int lightNum, bool bEnable = true);

            /*
            * @brief : Common code for light which goes in all vertex shaders. This 
            * code goes inside void main()
            */
            static std::string GetLightVShaderCodeMainCode();
            static std::string GetLightFShaderCodeMainCode();

            /*
            * global variables, etc. for lights
            */
            static std::string GetLightVShaderCodeVariables();
            static std::string GetLightFShaderCodeVariables();

            /*
            * @brief : Maximum number of lights which can be supported, the actual number of lights being used
            * by an application may be lesser
            */
            static int GetMaxLights();

            /*
            * @brief - if light is to be suported or not - for code checks, etc.
            * may be true even if all lights are disabled via global light switch
            */
            bool GetGlobalLightSupportPP();
            void SetGlobalLightSupportPP(bool bAreLightsSupported, unsigned int numLightsToUse);

            void Flush();
            /*
            * param[IN] bIndividualArrayElements: if true, then array[size] will be returned as array[0], array[1], etc.
            * This disctinction is necessary since we need array[size] form to verify shader code, but need
            * the latter representation for glGetUniformLocation()
            */
            const std::vector<std::string> GetLightUniforms(bool bIndividualArrayElements = true);


            /*
            * pointer to util::ShaderCodesContainer class needed to access uniform locations, etc.
            */
            void SetShaderContainerPtr(util::ShaderCodesContainer*);

            void Reset();

            /*
            * Global ON / OFF switch for all lights (per-shader).
            * If this off, all lights would be OFF,
            * individual ON / OFF state of each ligh wont matter
            */
            bool GlobalLightSwich(bool bTurnOn);
            Lights();
            ~Lights();
        private:
            util::ShaderCodesContainer* m_pShaderContainer = nullptr;//pointer to util::ShaderCodesContainer class needed to access uniform locations, etc.
            //unsigned int m_currentLight = 0;//light 0 as the default light
            std::vector<LightProps> m_Lights;
            std::bitset<MAX_LIGHTS> m_bLightEnabled;//ON/OFF state for each light which of the lights from m_Lights has the user enabled
            unsigned int m_numLightsToUse = 0; //user wants to use 0 to m_numLightsToUse-1 lights out of MAX_LIGHTS

            /*
            * A light must be configured before enabling it. It can be turned ON/OFF
            * several times after configuring it once
            */
            std::bitset<MAX_LIGHTS> m_bLightConfigured;

            /*
            * @brief - if light is to be suported or not - for code checks, etc.
            * may be true even if all lights are disabled via global light switch
            */
            bool m_bAreLightsSupported = false;

            bool m_bGlobalLightsSwitch = false; //Global ON / OFF switch for all lights (per-shader). If this off, all lights would be OFF, individual ON / OFF state of each ligh wont matter

            //Get all uniforms for lights, or flush them to shader - wanted to keep the list of uniforms at one place
            //Even though Although get and flush are unrelated. Hence this method is not static, even though
            //the list of uniforms are constant for all instances of the object.
            //param[IN] bIndividualArrayElements: if true, then array[size] will be returned as array[0], array[1], etc.
            //This disctinction is necessary since we need array[size] form to verify shader code, but need
            //the latter representation for glGetUniformLocation()
            const std::vector<std::string> GetOrFlushLightUniforms(bool bFlush = false, bool bIndividualArrayElements = true);
        };

        /*
        * @brief Container for simplifying matrix operations. OpenGL FFP (Fixed function pipeline)
        * has functions like glTranslate(), glLoadIdentity(), etc. Using it over the time
        * has made my mind think that to be easier. Hence this class will help emulate that 
        * behaviour.
        * However there will be no concept of Matrix mode - GL_MODELVIEW, etc. Instead this
        * class will simply track and uniform matrix variable, it's upto the user how
        * to use it.
        * eg. MatrixUniform4 ModelView(...); ModelView.translate(x,y,z). For this class
        * The matrix is simply linked to a uniform matrix variable. What that variable
        * is used for anywhere is of no concern.
        * MatrixUniform4 - 4x4 matrix
        */
        class MatrixUniform4 {
            vmath::mat4 m_mat4State;//4x4 matrix which will change state on operations - eg. translate, etc.
            const GLint m_uniformLocation;
        public:
            /* 
            * @brief constructor
            * @param[IN] uniformName : name of the uniform variable in shader
            * which an object of this class will track.
            * Will be used in GetUniformLocation(), etc.
            * @param[IN] uniformLocation : location of the uniform variable in shader of current program
            * @param[IN] matrixData  : Initial value of m_mat4State, if not specified, identity is loaded (delegating contructor)
            */
            MatrixUniform4();
            MatrixUniform4(const GLint uniformLocation);
            MatrixUniform4(const GLint uniformLocation, vmath::mat4 matrixData);
            /*
            * @brief Replace current contents of m_mat4State with
            * data given by newMatrix
            */
            void LoadMatrixData(vmath::mat4 newMatrix);

            /*
            * @brief Replace current contents of m_mat4State with
            * data given by newMatrix.
            * DO NOT COPY OTHER MEMBERS OF THE CLASS LIKE UNIFORM
            * LOCATION, etc.
            */
            void LoadMatrixData(MatrixUniform4 newMatrix);

            /*
            * @brief Get the underlying m_mat4State
            */
            vmath::mat4 GetMatrixData();

            /*
            * @brief similar to glTranslatef() in FFP
            */
            void Translate(GLfloat x, GLfloat y, GLfloat z);
            void Rotate(GLfloat angle, GLfloat x, GLfloat y, GLfloat z);
            void LoadIdentiy();

            /*
            * @brief : Thin wrapper over vmath::Perspective()
            * returns a new matrix, but doesn't modify matrix in *this
            */
            static MatrixUniform4 Perspective(float fovy, float aspect, float n, float f);

            /*
            * @brief : Send data to GPU
            * Idea is that user will perform various operations
            * on the matrix, and then send the final version to
            * the uniform variable in shader, rather than sending 
            * data after every operation.
            * @param[IN Optional] uniformLocation : uniform to which the data is to be sent
            * must specify uniformLocation if default contructor is used - MatrixUniform4();
            * for object creation.
            */
            void Flush(const GLint uniformLocation = -1);

            /*
            * @brief : returns this->m_mat4State * matrix2.m_mat4State;
            * DOES NOT MODIFY this->m_mat4State ;
            */
            MatrixUniform4 operator*(MatrixUniform4 matrix2);

            void operator=(MatrixUniform4&) = delete;//use LoadMatrixData() instead
            void operator=(vmath::mat4&) = delete;//use LoadMatrixData() instead
        };
    }
}

namespace PP = util::ProgrammablePipeline;