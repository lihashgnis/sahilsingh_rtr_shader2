#pragma once
/*
class TextureSceneObject : Class for showing arbitrary image on screen
using textures.
This class simply draws given texture on a quad.
Can hold several images for a quad, one of which will be visible
at a time.
Named texture scene object to emphasize that this is derived
from scene object and is a part of animation infra, and that
it is NOT RELATED TO texture objects OpenGL concept - https://www.khronos.org/opengl/wiki/Texture#Texture_Objects
*/

#include <map>
#include "UtilCrossPlatform.hpp"
#include "Textures.h"

namespace util {
    class TextureSceneObject :
        public SceneObject
    {
    public:
        struct ImageProps
        {
            GLuint texture; //Texture identifier, as understood by opengl - glGenTetures()
            double aspectRatio; //every image may have different aspect ratio

            ImageProps()
            {
                texture = 0;
            }
        };
    protected:
        /*A single textures scene object
        can contain multiple texture images, and
        the image currently shown can be varied.
        The image which user wants to show would
        be identified by user given ID.
        */
        std::map<UINT, ImageProps> m_IdToTextureMap;
        //ID of the image currently being shown
        int m_currentImageID;
        //NO LONGER RELEVANT - as each image will have it's own aspect ratio. Width to height ratio of the cell
        //GLfloat m_cellAspectRatio = 0;
    public:
        enum class Operation
        {
            REPLACE,/*We want to replace texture associated with a given ID. 
                    If no texture is associated then simply assign.
                    */
            UNIQUE
        };
        
        //Returns false on failure
        bool AddTexture(UINT id, const char imageResourcePath[], Textures::TextureType type, Operation op);

        TextureSceneObject() = delete;
        //At least 1 texture should be present, others can be added with AddTexture()
        TextureSceneObject(UINT id, const char imageResourcePath[], Textures::TextureType type);
        ~TextureSceneObject();

        //void SetCellAspectRatio(GLfloat aspectRatio);//Aspect ratio will be calcuated automatically - see AddTexture()

        //Set cell size, using the aspect ratio, and height
        void SetCellHeight(GLfloat height);

        //Since same texture scene object can have different images, hence widths may be different for them
        GLfloat GetCurrentImageCellWidth();

        //We do not want anyone to set arbitray cell size
        //Since we have to maintain aspect ratio for textures
        void SetCellSize(const DataTypes::Size2Dd& sz) = delete;
        
        //since a single texture scene object can have different images, hence
        //cell cize wont be constant. GetCellSize() has constant cell size semantics
        DataTypes::Size2Dd GetCellSize() = delete;

        // Inherited via SceneObject
        virtual void Draw(AnimationObjectState & state) override;
        virtual void AdvanceTime() override;
    };
}