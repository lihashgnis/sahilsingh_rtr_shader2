#pragma once
#include "UtilCrossPlatform.hpp"
#include <GL/gl.h>
#include <GL/glu.h>
#include <map>
#include <string>

namespace util {
    class Textures
    {
        //TODO: Implement texture freeing up in such a scenario - may have to keep reference count
        static std::map<std::string, GLuint> m_textureCache;
    public:
        Textures() = delete;
        ~Textures() = delete;
        enum class TextureType {
            BMP,
            PNG
        };
        /*
        Optional parameters - width, and height - return width, and height, if user has specified these parameters
        */
        static bool LoadTexture(_In_ GLuint *texture, _In_ const char imageResourcePath[], _In_ TextureType type, _Out_opt_ int* pWidth = nullptr, _Out_opt_ int* pHeight = nullptr);
    };
}
