﻿#ifdef USING_PRECOMPILED_HEADERS
#include "stdafx.h"
#endif
#include "UtilCrossPlatform.hpp"
#include <stdexcept>
#include <climits>
#include <sstream>
#include <algorithm>

#ifdef _WIN32
#include <OpenGL-Registry-master/api/GL/wglext.h>
#endif

#pragma comment (lib, "OpenGl32.lib")

#ifndef _PROGRAMMABLE_PIPELINE_
#pragma comment (lib, "glu32.lib")
#endif

#include "../Logging/Logger.hpp"
#include <exception>
#include "UtilCrossPlatform.hpp"

#include <fstream>

CREATE_LOG();


#define ATTRIBUTE_DUMP_FILE_NAME R"(attributeDump.txt)"
#define SHADER_DUMP_FILE_NAME R"(shaderDump.txt)"

//TODO: remove this when log lib starts working
#define LOG(x,y) if(LogLevel::FatalError == x){throw std::runtime_error("runtime error");}

using namespace util;

int ShaderCodesContainer::m_shaderVersion = -1;
bool ShaderCodesContainer::m_bAllowShaderVersionToBeSet = true;

#ifdef _WIN32
void OglWindow::InitWndClassEx(WNDCLASSEX& wc, TCHAR* AppName, WNDPROC wp)
{
    wc = {};
    wc.cbSize = sizeof(wc);
    wc.cbClsExtra = 0;
    wc.cbWndExtra = 0;
    wc.lpfnWndProc = wp;
    wc.hInstance = ::GetModuleHandle(NULL);
    wc.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
    wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
    wc.lpszClassName = AppName;
    wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
    wc.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
    wc.hCursor = LoadCursor(NULL, IDC_ARROW);

    if (!RegisterClassEx(&wc))
    {
        LOG(LogLevel::FatalError, "RegisterClassfailed");
    }
}
#endif

WindowProps OglWindow::CreateOglWindow(TCHAR* applicationName, UINT x, UINT y, UINT width, UINT height)
{
    WindowProps props = {};
#ifdef _WIN32
    HINSTANCE hInstance = ::GetModuleHandle(NULL);
    props.SetHWND(CreateWindowEx(WS_EX_APPWINDOW, applicationName, applicationName, WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, x, y, width, height, NULL, NULL, hInstance, NULL));
#endif
    return props;
}

OglWindow::~OglWindow()
{
}

void util::OglWindow::ToggleFullscreeen()
{
#ifdef _WIN32
    ToggleFullscreeenWindows(m_WindowProps.GetHWND());
#endif
#ifdef __linux__
    ToggleFullscreeenLinux();
#endif

}

void util::OglWindow::DestroyWindow()
{
#ifdef _WIN32
    ::DestroyWindow(m_WindowProps.GetHWND());
#endif
#ifdef __linux__
    DestroyWindowLinux();
#endif
}


bool util::OglWindow::IsFullScreen()
{
    return m_bIsFullScreen;
}

void OglWindow::EnableAlpha(bool bEnable)
{
    if (bEnable)
    {
        //TODO: LOG that transparency is not fully supported, and may lead to artifacts - alphabets created for dynamic India assignment were created by keeping no transparency in mind
        //The freetype support added later for text rendering supports transparency
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    }
    else
    {
        glDisable(GL_BLEND);
    }
}

OglWindow::OglWindow(TCHAR* applicationName, UINT x, UINT y, UINT width, UINT height)
{
    m_bIsFullScreen = false;
    m_bIsPaused = false;
    m_bActiveWindow = false;
    m_bCapFramerate = false;
    m_bAllowOutOfFocusrender = false;
    m_dwInterFrameTimeMilliSecond = 0;
    m_dwPrevEpoch = {};
    m_posx = x;
    m_posy = y;
    m_width = width;
    m_height = height;
    m_wndName = applicationName;
    m_InitMask = 0;

    m_WindowProps = {};
    m_WindowProps.SetWindowProc((util::WindowProps::WNDPROC)OglWindow::OglWndProc);
#ifdef _WIN32
    m_WindowProps.SetWindowProc((util::WindowProps::WNDPROC)OglWindow::OglWndProcWindows);
    OglWindow::InitWndClassEx(m_wndClassEx, applicationName, OglWindow::OglWndProcWindows);
#endif
    m_WindowProps.SetOGLWindow(this);
    globalWindowPropsCache.AddToCache(PARENT_OBJECT, m_WindowProps);

#ifdef _WIN32
    m_WindowProps.SetHWND(CreateOglWindow(applicationName, x, y, m_width, m_height).GetHWND());
#endif

    m_oglVersion_major = -1;//default value set in OglWindow::InitAndShowWindow()
    m_oglVersion_minor = -1;//default value set in OglWindow::InitAndShowWindow()

    EnableAlpha(false);//Alpha disabled by default
}

void OglWindow::InitAndShowWindow(UINT mask, int major, int minor)
{
    m_oglVersion_major = major;
    m_oglVersion_minor = minor;

    SetShaderVersion(ShaderCodesContainer::OGL2GLSLVersion(m_oglVersion_major *10 + m_oglVersion_minor));

    m_InitMask = mask;

    if (m_InitMask & INIT_MASK_DUMP_ATTRIBUTES)
    {
        std::ofstream of(ATTRIBUTE_DUMP_FILE_NAME, std::ios::trunc);
        of.close();
    }

    if (m_InitMask & INIT_MASK_DUMP_SHADER_CODE)
    {
        std::ofstream of(SHADER_DUMP_FILE_NAME, std::ios::trunc);
        of.close();
    }

    int iRet = Initialize(mask);
#ifdef _WIN32
    const char* str = nullptr;
    if (iRet < 0)
    {
        switch (iRet)
        {
        case -1:
            str = "choose pixel failed";
            break;
        case -2:
            str = "Set pixel format failed";
            break;
        case -3:
            str = "wglCreateContext failed";
            break;
        case -4:
            str = "wglMakeCurrent failed";
            break;
        case -5:
            str = "glewInit failed";
            break;
        default:
            break;
        }
        LOG(LogLevel::FatalError, str);
        Uninitialize();
        exit(1);
    }
    else
    {
        str = "Initialize successful";
    }

    LOG(LogLevel::Debug, str);

    ShowWindow(m_WindowProps.GetHWND(), SW_SHOWNORMAL);
    SetForegroundWindow(m_WindowProps.GetHWND());
    SetFocus(m_WindowProps.GetHWND());
#endif
}

int OglWindow::Initialize(UINT mask)
{
#ifdef _WIN32
    InitializeWindows(mask);
#endif
#ifdef __linux__
    InitializeLinux(mask);
#endif

    //code common to both
    if (mask & INIT_MASK_ENABLE_PROGRAMMABLE_PIPELINE)//TODO: test code path - both FFP, PP
    {
        if (GLEW_OK != glewInit())
        {
            return -5;
        }
    }

    if (mask & INIT_MASK_ENABLE_DEPTH)
    {
        glClearDepth(1.0);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        if (~mask & INIT_MASK_ENABLE_PROGRAMMABLE_PIPELINE)//TODO: test code path - both FFP, PP
        {
            glShadeModel(GL_SMOOTH);
        }
    }

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

    Resize(m_width, m_height);

    if (mask & INIT_MASK_ENABLE_DEPTH)
    {
        if (~mask & INIT_MASK_ENABLE_PROGRAMMABLE_PIPELINE)//TODO: test code path - both FFP, PP
        {
            glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
        }
    }

    if (mask & INIT_MASK_ENABLE_TEXTURE_2D)
    {
        glEnable(GL_TEXTURE_2D);
    }

    m_WindowProps.SetInitialized();//constructor execution over, now it will be safe to call derived class methods
    globalWindowPropsCache.AddToCache(PARENT_OBJECT, m_WindowProps, util::WindowPropsCache::CacheOperationType::REPLACE_VALUE);
    return 0;
}


#ifdef __linux__

void OglWindow::XWindowsManualHighVisual()
{
    GLXFBConfig* pGlxFbConfig = nullptr;
    int iNumFbConfigs = 0;
    const int frameBufferAttributes[] = {
        /*https://www.khronos.org/registry/OpenGL-Refpages/gl2.1/xhtml/glXChooseFBConfig.xml*/
        GLX_X_RENDERABLE, True, //Config which is capable of rendering on XWindow
        GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT,
        GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR,
        GLX_RED_SIZE, 8,
        GLX_GREEN_SIZE, 8,
        GLX_BLUE_SIZE, 8,
        GLX_ALPHA_SIZE, 8,
        GLX_DEPTH_SIZE, 24,
        GLX_STENCIL_SIZE, 8,
        GLX_DOUBLEBUFFER, True,
        None
    };

    int defaultScreen = XDefaultScreen(m_WindowProps.GetDisplay());
    pGlxFbConfig = glXChooseFBConfig(m_WindowProps.GetDisplay(), defaultScreen, frameBufferAttributes, &iNumFbConfigs);
    LOG(LogLevel::Info, (L"Number of suitable FB configs found(iNumFbConfigs): " + std::to_wstring(iNumFbConfigs)).c_str());

    int bestFBConfig = -1, bestNumSamples = -1;
    int wosrtFBConfig = -1, worstNumSamples = INT_MAX;

    for (int i = 0; i < iNumFbConfigs; i++)
    {
        XVisualInfo* pTempVisualInfo = glXGetVisualFromFBConfig(m_WindowProps.GetDisplay(), pGlxFbConfig[i]);
        if(nullptr != pTempVisualInfo)
        {
            int sampleBuffers = 0, samples = 0;
            
            //Get number of supported sample buffers
            glXGetFBConfigAttrib(m_WindowProps.GetDisplay(), pGlxFbConfig[i], GLX_SAMPLE_BUFFERS, &sampleBuffers);
            //Get number of samples
            glXGetFBConfigAttrib(m_WindowProps.GetDisplay(), pGlxFbConfig[i], GLX_SAMPLES, &samples);
            LOG(LogLevel::Info, (L"FB Config " + std::to_wstring(i) + " : GLX_SAMPLE_BUFFERS : " std::to_wstring(sampleBuffers) + " : GLX_SAMPLES : " + std::to_wstring(samples)).c_str());
            
            if(sampleBuffers || (bestFBConfig < 0))
            {
                bestNumSamples = std::max(bestNumSamples, samples);
                bestFBConfig = (bestNumSamples == samples)? i : bestFBConfig;
            }
            
            if(wosrtFBConfig < 0)
            {
                worstNumSamples = std::min(worstNumSamples, samples);
                wosrtFBConfig = (worstNumSamples == samples)? i : wosrtFBConfig;
            }
            
            XFree(pTempVisualInfo);
            pTempVisualInfo = nullptr;
        }
    }

    GLXFBConfig bestGLXFBConfig = pGlxFbConfig[bestFBConfig];
    m_WindowProps.SetGLXFBConfig(bestGLXFBConfig);

    typedef GLXContext(*glXCreateContextAttribsARBPROC)(::Display*, GLXFBConfig, GLXContext, Bool, const int*);//https://www.khronos.org/registry/OpenGL/extensions/ARB/GLX_ARB_create_context.txt
    glXCreateContextAttribsARBPROC glxCreateContextAttribsARB = nullptr;
    glxCreateContextAttribsARB = (decltype(glxCreateContextAttribsARB))glXGetProcAddressARB((GLubyte*)"glXCreateContextAttribsARB");
    
    if(!glxCreateContextAttribsARB)
    {
        THROW_RUNTIME_ERROR("glXGetProcAddressARB() failed for glXCreateContextAttribsARB - Probably native driver is not installed, and the default driver is being used");
    }
    
    int attribs[] = 
    {
        GLX_CONTEXT_MAJOR_VERSION_ARB, m_oglVersion_major,//set below
        GLX_CONTEXT_MINOR_VERSION_ARB, m_oglVersion_minor,//set belowglXCreateContextAttribsARB
        GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
        None
    };
    
    //TODO: remove following if above attribs list compiles fine
    //if((GLX_CONTEXT_MAJOR_VERSION_ARB != attribs[0]) || (GLX_CONTEXT_MINOR_VERSION_ARB != attribs[2]))
    //{
    //    THROW_RUNTIME_ERROR("attribs order changed. Cannot set versions.")
    //}
    //attribs[0] = m_oglVersion_major;
    //attribs[2] = m_oglVersion_minor;
    
    GLXContext glxContext = glxCreateContextAttribsARB(m_WindowProps.GetDisplay(), m_WindowProps.GetGLXFBConfig(), 0, True, attribs);
    if(NULL == glxContext)
    {
     //We didn't get the requested OGL version
     //Ask for whichever is highest
     //Don't ask for core profile
     const int attribs[] = {
         GLX_CONTEXT_MAJOR_VERSION_ARB, 1,
         GLX_CONTEXT_MINOR_VERSION_ARB, 0,
         None
     };
     
     glxContext = glxCreateContextAttribsARB(m_WindowProps.GetDisplay(), m_WindowProps.GetGLXFBConfig(), 0, True, attribs);
     if(NULL == glxContext)
     {//bail out if still error
         THROW_RUNTIME_ERROR("second attempt of glXCreateContextAttribsARB() failed too");
     }
    }

    m_WindowProps.SetGLXContext(glxContext);

    //check if the context we got is direct or not - sir's comment -> "Check if the context we got is hardware rendering context or not"
    if(!glXIsDirect(m_WindowProps.GetDisplay(), m_WindowProps.GetGLXContext()))
    {
        //sir's comment - the obtained context isn't H/W rendering capable
        //My understanding - indirect context is required when X server is remote. so probably not related to hardware or not OR they
        //both may be synonymous
        LOG(LogLevel::Debug, L"Didn't get direct context.");
    }
    else
    {
        LOG(LogLevel::Debug, L"Got direct context.");
    }
    
}

int OglWindow::InitializeLinux(UINT mask)
{
    XSetWindowAttributes winAttribs = {};
    int defaultScreen = 0;
    //int defaultDepth = 0;
    int styleMask = 0;

    //code
    ::Display* pDisplay = XOpenDisplay(NULL);
    ASSERT(nullptr != pDisplay, "XOpenDisplay() error");
    m_WindowProps.SetDisplay(pDisplay);

    defaultScreen = XDefaultScreen(m_WindowProps.GetDisplay());
    //defaultDepth = XDefaultDepth(m_WindowProps.GetDisplay(), defaultScreen);
    //
    //std::shared_ptr<XVisualInfo> pXVisualInfo(new XVisualInfo);
    //ASSERT(nullptr != pXVisualInfo, "error allocaing memory");
    //m_WindowProps.SetXVisualInfo(pXVisualInfo);
    //auto status = XMatchVisualInfo(m_WindowProps.GetDisplay(), defaultScreen, defaultDepth, TrueColor, m_WindowProps.GetXVisualInfo().get());
    //ASSERT(0 != status, "XMatchVisualInfo error");
    int frameBufferAttributes[] = {
        GLX_RGBA,
        GLX_RED_SIZE, 8,
        GLX_GREEN_SIZE, 8,
        GLX_BLUE_SIZE, 8,
        GLX_ALPHA_SIZE, 8,
        GLX_DOUBLEBUFFER, True,
        GLX_DEPTH_SIZE, 24,
        None
    };
    
    
    auto pXVisualInfoRaw = glXChooseVisual(m_WindowProps.GetDisplay(), defaultScreen, frameBufferAttributes);
    
    ASSERT(nullptr != pXVisualInfoRaw, "glXChooseVisual() failed");
    std::shared_ptr<XVisualInfo> pXVisualInfo;
    pXVisualInfo.reset(pXVisualInfoRaw);
    m_WindowProps.SetXVisualInfo(pXVisualInfo);

    winAttribs.border_pixel = 0;
    winAttribs.background_pixmap = 0;
    Colormap colormap = XCreateColormap(m_WindowProps.GetDisplay(), RootWindow(m_WindowProps.GetDisplay(), m_WindowProps.GetXVisualInfo()->screen), m_WindowProps.GetXVisualInfo()->visual, AllocNone);
    ASSERT(0 != colormap, "XCreateColormap error");
    winAttribs.colormap = colormap;
    winAttribs.background_pixel = BlackPixel(m_WindowProps.GetDisplay(), defaultScreen);
    winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask | FocusChangeMask;

    m_WindowProps.SetColorMap(winAttribs.colormap);

    styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

    auto window = XCreateWindow(m_WindowProps.GetDisplay(), RootWindow(m_WindowProps.GetDisplay(), m_WindowProps.GetXVisualInfo()->screen), m_posx, m_posy, m_width, m_height, 0, m_WindowProps.GetXVisualInfo()->depth, InputOutput, m_WindowProps.GetXVisualInfo()->visual, styleMask, &winAttribs);
    ASSERT(0 != window, "XCreateWindow() failed");
    m_WindowProps.SetWindow(window);
    XStoreName(m_WindowProps.GetDisplay(), m_WindowProps.GetWindow(), m_wndName.c_str());//set window's name

    Atom windowManagerDelete = XInternAtom(m_WindowProps.GetDisplay(), "WM_DELETE_WINDOW", True);//for 33 event
    XSetWMProtocols(m_WindowProps.GetDisplay(), m_WindowProps.GetWindow(), &windowManagerDelete, 1);

    XMapWindow(m_WindowProps.GetDisplay(), m_WindowProps.GetWindow());
    
    if(INIT_XWINDOWS_DISABLE_ADVANCED_FB_CONFIG_INIT & mask)//disabled by default
    {
        GLXContext glxContext = glXCreateContext(m_WindowProps.GetDisplay(), m_WindowProps.GetXVisualInfo().get(), NULL, GL_TRUE);
        ASSERT(nullptr != glxContext, "glXCreateContext() failed");
        m_WindowProps.SetGLXContext(glxContext);
    }
    else
    {
        XWindowsManualHighVisual();
    }

    glXMakeCurrent(m_WindowProps.GetDisplay(), m_WindowProps.GetWindow(), m_WindowProps.GetGLXContext());
    return 0;
}
#endif //__linux__

#ifdef _WIN32
int OglWindow::InitializeWindows(UINT mask)
{
    PIXELFORMATDESCRIPTOR pfd;
    ZeroMemory((void*)& pfd, sizeof(PIXELFORMATDESCRIPTOR));
    pfd.nSize = sizeof(pfd);
    pfd.nVersion = 1;
    pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
    pfd.iPixelType = PFD_TYPE_RGBA;
    pfd.cColorBits = 32;
    pfd.cRedBits = 8;
    pfd.cGreenBits = 8;
    pfd.cBlueBits = 8;
    pfd.cAlphaBits = 8;

    if (mask & INIT_MASK_ENABLE_DEPTH)
    {
        pfd.cDepthBits = 32;
    }

    if (mask & INIT_MASK_ENABLE_STENCIL)
    {
        pfd.cStencilBits = 8;
    }

    m_Hdc = GetDC(m_WindowProps.GetHWND());
    auto iPixelFormatIndex = ChoosePixelFormat(m_Hdc, &pfd);

    if (iPixelFormatIndex == 0)
    {
        return -1;
    }

    if (SetPixelFormat(m_Hdc, iPixelFormatIndex, &pfd) == false)
    {
        return -2;
    }

    m_Hrc = wglCreateContext(m_Hdc);//create rendering context

    if (m_Hrc == NULL)
    {
        return -3;
    }

    if (wglMakeCurrent(m_Hdc, m_Hrc) == false)//associate the rendering context to current thread
    {
        return -4;
    }

    //following tutorial at https://sites.google.com/site/opengltutorialsbyaks/introduction-to-opengl-4-1---tutorial-05
    if (mask & INIT_MASK_ENABLE_ARB_DEBUG)
    {
        int attribs[] =
        {
            WGL_CONTEXT_MAJOR_VERSION_ARB, m_oglVersion_major,//set below
            WGL_CONTEXT_MINOR_VERSION_ARB, m_oglVersion_minor,//set belowglXCreateContextAttribsARB
            WGL_CONTEXT_PROFILE_MASK_ARB, WGL_CONTEXT_CORE_PROFILE_BIT_ARB,
            WGL_CONTEXT_FLAGS_ARB, WGL_CONTEXT_DEBUG_BIT_ARB,
            0
        };

        PFNWGLCREATECONTEXTATTRIBSARBPROC wglCreateContextAttribsARB = nullptr;
        wglCreateContextAttribsARB = (PFNWGLCREATECONTEXTATTRIBSARBPROC)wglGetProcAddress("wglCreateContextAttribsARB");

        if (nullptr != wglCreateContextAttribsARB)
        {
            HGLRC newHrc = wglCreateContextAttribsARB(m_Hdc,0, attribs);
            if (NULL != newHrc)
            {
                wglMakeCurrent(m_Hdc, newHrc);
                wglDeleteContext(m_Hrc);
                m_Hrc = newHrc;

                //register callback
                //code from https://sites.google.com/site/opengltutorialsbyaks/introduction-to-opengl-4-1---tutorial-05
                typedef void (*PFNGLDEBUGMESSAGECALLBACKARBPROC) (GLDEBUGPROCARB callback, void* userParam);
                PFNGLDEBUGMESSAGECALLBACKARBPROC  glDebugMessageCallbackARB = nullptr;
                glDebugMessageCallbackARB = (PFNGLDEBUGMESSAGECALLBACKARBPROC)wglGetProcAddress("glDebugMessageCallbackARB");
                if (nullptr != glDebugMessageCallbackARB)
                {
                    glDebugMessageCallbackARB((GLDEBUGPROCARB)&OglWindow::OpenGlDebugCallBack, nullptr);
                }

            }
            else
            {
                LOG(LogLevel::Error, "wglCreateContextAttribsARB() failed");
            }

        }
    }

    return 0;
}
#endif

void OglWindow::OpenGlDebugCallBack(unsigned int source, unsigned int type, unsigned int id, unsigned int severity, int length, const char* message, void* userParam)
{
#ifdef _WIN32
    char* str = new char[length + 1];
    strcpy_s(str, length + 1, message);

    OutputDebugStringA("\n##ARB_DEBUG_START##\n");
    OutputDebugStringA(str);
    OutputDebugStringA("\n##ARB_DEBUG_END##\n");
#endif
}

float OglWindow::GetMinX()
{
    LOG(LogLevel::Warning, L"Depricated");
    return m_minX;
}

float OglWindow::GetMaxX()
{
    LOG(LogLevel::Warning, L"Depricated");
    return m_maxX;
}

float OglWindow::GetMinY()
{
    LOG(LogLevel::Warning, L"Depricated");
    return m_minY;
}

float OglWindow::GetMaxY()
{
    LOG(LogLevel::Warning, L"Depricated");
    return m_maxY;
}

void OglWindow::CalcExtents()
{
    GLint viewport[4];
    glGetIntegerv(GL_VIEWPORT, viewport);

    GLfloat width =  (GLfloat)viewport[2];
    GLfloat height = (GLfloat)viewport[3];

    if (width <= height)
    {
        auto factor = 1.0f * (((GLfloat)height) / ((GLfloat)width));
        m_minX = -1.0;
        m_maxX = 1.0;
        m_minY = -factor;
        m_maxY = factor;
        m_minZ = -1.0;
        m_maxZ = 1.0;
    }
    else
    {
        auto factor = 1.0f * (((GLfloat)width) / ((GLfloat)height));
        m_minX = -factor;
        m_maxX = factor;
        m_minY = -1.0;
        m_maxY = 1.0f;
        m_minZ = -1.0;
        m_maxZ = 1.0;
    }
}

float OglWindow::GetMinX2()
{
    CalcExtents();
    return m_minX;
}

float OglWindow::GetMaxX2()
{
    CalcExtents();
    return m_maxX;
}

float OglWindow::GetMinY2()
{
    CalcExtents();
    return m_minY;
}

float OglWindow::GetMaxY2()
{
    CalcExtents();
    return m_maxY;
}

double util::OglWindow::GetWindowHeightNDC()
{
    return std::fabs(GetMaxY2() - GetMinY2());
}

double util::OglWindow::GetWindowWidthNDC()
{
    return std::fabs(GetMaxX2() - GetMinX2());
}

double OglWindow::PixelsToNDC(int pixels)
{
    GLint viewport[4];
    glGetIntegerv(GL_VIEWPORT, viewport);
    CalcExtents();
    return ((m_maxX - m_minX) / viewport[3]) * pixels;
}

int OglWindow::NDCToPixels(double NDCcoord)
{
    GLint viewport[4];
    glGetIntegerv(GL_VIEWPORT, viewport);
    CalcExtents();
    return int((viewport[3] / (m_maxX - m_minX)) * NDCcoord);
}

void util::OglWindow::SetClearColor(COLORREF rgba)
{
    glClearColor(GetRValuef(rgba), GetGValuef(rgba), GetBValuef(rgba), GetAValuef(rgba));
}

void OglWindow::Uninitialize()
{
    if (m_InitMask & INIT_MASK_ENABLE_PROGRAMMABLE_PIPELINE)
    {
        for (auto& shaderProgram : m_shaderCodesMap)
        {
            auto objectsIDs = shaderProgram.second.GetAllObjectIDs();
            if (objectsIDs.empty())
            {
                LOG(LogLevel::Debug, "No objects present");
            }
            else
            {
                for (const auto& objID : objectsIDs)
                {
                    auto attribs = shaderProgram.second.GetAllAttribsForObject(objID);
                    if (!attribs.empty())
                    {
                        for (const auto& attrib : attribs)
                        {
                            glDeleteBuffers(1, &(attrib.vbo));//TODO: optimize - all buffers can be deleted at once
                        }
                    }
                    else
                    {
                        LOG(LogLevel::Debug, "No atriibutes for object with ID: " + std::to_string(objID));
                    }

                    GLuint vao = shaderProgram.second.GetObjectVao(objID);
                    glDeleteVertexArrays(1, &vao);
                }
            }
        }

        for (auto& shaderProgram : m_shaderCodesMap)
        {
            auto prog = GetShaderProgramWithProgramID(shaderProgram.first);
            if (prog)
            {
                glUseProgram(prog.value());
                GLint numAttahcedShaders = 0;
                glGetProgramiv(prog.value(), GL_ATTACHED_SHADERS, &numAttahcedShaders);
                std::unique_ptr<GLuint[]> pShaders(new GLuint[numAttahcedShaders]);
                if (pShaders)
                {
                    glGetAttachedShaders(prog.value(), numAttahcedShaders, &numAttahcedShaders, pShaders.get());
                    for (int i = 0; i < numAttahcedShaders; i++)
                    {
                        GLuint shader = pShaders.get()[i];
                        glDetachShader(prog.value(), shader);
                        glDeleteShader(shader);
                    }
                }
                glUseProgram(0);
                glDeleteProgram(prog.value());
                m_shaderCodesMap[shaderProgram.first].Reset();
            }
        }
    }

    if (m_bIsFullScreen)
    {
        ToggleFullscreeen();
    }
#ifdef _WIN32
    if (wglGetCurrentContext() == m_Hrc)
    {
        wglMakeCurrent(NULL, NULL);
    }

    if (m_Hrc)
    {
        wglDeleteContext(m_Hrc);
        m_Hrc = NULL;
    }

    if (m_Hdc)
    {
        ReleaseDC(m_WindowProps.GetHWND(), m_Hdc);
        m_Hdc = NULL;
    }
#endif

#ifdef __linux__
    GLXContext currentContext = glXGetCurrentContext();
    if ((nullptr != currentContext) && (currentContext == m_WindowProps.GetGLXContext()))
    {
        glXMakeCurrent(m_WindowProps.GetDisplay(), 0, 0);
    }

    if (nullptr != m_WindowProps.GetGLXContext())
    {
        glXDestroyContext(m_WindowProps.GetDisplay(), m_WindowProps.GetGLXContext());
        m_WindowProps.SetGLXContext(nullptr);
    }

    if (0 != m_WindowProps.GetWindow())
    {
        XDestroyWindow(m_WindowProps.GetDisplay(), m_WindowProps.GetWindow());
        m_WindowProps.SetWindow(0);
    }

    if (0 != m_WindowProps.GetColorMap())
    {
        XFreeColormap(m_WindowProps.GetDisplay(), m_WindowProps.GetColorMap());
        m_WindowProps.SetColorMap(0);
    }

    if (nullptr != m_WindowProps.GetXVisualInfo())
    {
        m_WindowProps.SetXVisualInfo(nullptr);
    }

    if (nullptr != m_WindowProps.GetDisplay())
    {
        XCloseDisplay(m_WindowProps.GetDisplay());
        m_WindowProps.SetDisplay(nullptr);
    }
#endif
}

#ifdef _WIN32
void OglWindow::ToggleFullscreeenWindows(HWND hwnd)
{
    static WINDOWPLACEMENT wpPrev;
    DWORD dwStyle = 0;

    if (m_bIsFullScreen == false)
    {
        dwStyle = GetWindowLong(hwnd, GWL_STYLE);

        if (dwStyle & WS_OVERLAPPEDWINDOW)
        {
            MONITORINFO mi = {};
            mi.cbSize = sizeof(mi);

            if (GetWindowPlacement(hwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(hwnd, MONITORINFOF_PRIMARY), &mi))
            {
                SetWindowLong(hwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
                SetWindowPos(hwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left,
                    mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
                ShowCursor(FALSE);
                m_bIsFullScreen = true;
            }
        }
    }
    else
    {
        SetWindowLong(hwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
        SetWindowPlacement(hwnd, &wpPrev);
        SetWindowPos(hwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
        ShowCursor(TRUE);
        m_bIsFullScreen = FALSE;
    }
}
#endif

#ifdef __linux__
void OglWindow::ToggleFullscreeenLinux()
{
    Atom wm_state = {};
    Atom fullscreen = {};
    XEvent xev = {};

    //make sure we have required parameters
    if (false == (m_WindowProps.mask & WndMask(LNX_PDISPLAY)) && (m_WindowProps.mask & WndMask(LNX_WINDOW)) && (m_WindowProps.mask & WndMask(LNX_PXVISUALINFO)))
    {
        THROW_WINDOWPROP_PARAMETER_MISSING;
        return;
    }

    wm_state = XInternAtom(m_WindowProps.GetDisplay(), "_NET_WM_STATE", False);

    xev.type = ClientMessage;
    xev.xclient.window = m_WindowProps.GetWindow();
    xev.xclient.message_type = wm_state;
    xev.xclient.format = 32;
    xev.xclient.data.l[0] = m_bIsFullScreen ? 0 : 1;

    fullscreen = XInternAtom(m_WindowProps.GetDisplay(), "_NET_WM_STATE_FULLSCREEN", False);
    xev.xclient.data.l[1] = fullscreen;
    XSendEvent(m_WindowProps.GetDisplay(), RootWindow(m_WindowProps.GetDisplay(), m_WindowProps.GetXVisualInfo()->screen), False, StructureNotifyMask, &xev);

    m_bIsFullScreen = !m_bIsFullScreen;
}

void util::OglWindow::DestroyWindowLinux()
{
    XDestroyWindow(m_WindowProps.GetDisplay(), m_WindowProps.GetWindow());
}
#endif

void OglWindow::TogglePause()
{
    m_bIsPaused = !m_bIsPaused;
}

void OglWindow::RunGameLoop()
{
    // Game loop

    bool bDone = false;
    MSG msg;

    while (bDone == false)
    {

        if (PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
        {
            if (msg.message == WM_QUIT)
                bDone = true;
            else
            {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
            }
        }
        else
        {
            if (m_bAllowOutOfFocusrender || (m_bActiveWindow == true))
            {
                std::chrono::steady_clock::time_point time = {};
                bool bExecDisplayFunc = true;
                if (m_bCapFramerate)
                {//TODO: log framerate capping in effect
                    bExecDisplayFunc = false;
                    time = std::chrono::steady_clock::now();
                    double deltaMs = std::chrono::duration<double, std::milli>(time - m_dwPrevEpoch).count();

                    if (deltaMs >= m_dwInterFrameTimeMilliSecond)
                    {
                        bExecDisplayFunc = true;
                        m_dwPrevEpoch = time;
                    }
                }

                if (m_bIsPaused)
                {
                    bExecDisplayFunc = false;
                }

                if (bExecDisplayFunc)
                {
                    Display();
                }
                Update();
            }
            else
            {

            }
        }
    }
}

#ifdef _WIN32
HWND OglWindow::GetHwnd()
{
    return m_WindowProps.GetHWND();
}
#endif

void util::OglWindow::SetCapFrameRate(DWORD interFrameTimeMilliSeconds, bool bCapFrameRate)
{
    if (bCapFrameRate && (interFrameTimeMilliSeconds == 0))
    {
        throw std::runtime_error("interFrameTimeMilliSeconds CanotbeZero");
    }

    m_dwPrevEpoch = {};
    m_bCapFramerate = bCapFrameRate;
    m_dwInterFrameTimeMilliSecond = interFrameTimeMilliSeconds;
}

void util::OglWindow::AllowOutOfFocusRender(bool bAllow)
{
    m_bAllowOutOfFocusrender = bAllow;
}

#ifdef _WIN32
LRESULT CALLBACK OglWindow::OglWndProcWindows(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
    auto op = globalWindowPropsCache.GetValue(PARENT_OBJECT);
    if (!op)
    {
        THROW_WINDOWPROP_PARAMETER_MISSING
    }
#ifdef _WIN32
    op->SetHWND(hwnd);//in WM_CREATE, etc. we would not have windows handle in cache, since that we only add once create window is over
#endif
    return OglWndProc(op.value(), iMsg, wParam, lParam);
}
#endif

LRESULT OglWindow::OglWndProc(WindowProps& props, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
    OglWindow* that = nullptr;

    if (props.GetInitialized())
    {
        //we should only call derived class constructor once constructor execution is over
        that = props.GetOGLWindow();
    }

    LRESULT lr = 0;
    if (that)
    {
        lr = that->WndProcPre(props, iMsg, wParam, lParam);
    }

    if (lr < 0)//if wndproc2 returned -ve => user of this lib doesn't want further special processing
    {
#ifdef _WIN32
        return DefWindowProc(props.GetHWND(), iMsg, wParam, lParam);
#endif
    }

    switch (iMsg)
    {
    case WM_KEYDOWN:
    {
        if (VK_ESCAPE == wParam)
        {
            if (that->m_bIsFullScreen)
            {
                that->ToggleFullscreeen();
            }
            that->DestroyWindow();
        }
    }
    break;
    case WM_SETFOCUS:
        that->m_bActiveWindow = true;
        break;
    case WM_KILLFOCUS:
        that->m_bActiveWindow = false;
        break;
    case WM_SIZE:
        that->Resize(LOWORD(lParam), HIWORD(lParam));
        break;
    case WM_ERASEBKGND://dont want this since defwindow proc will post WM_PAINT after processing this
        return 0;
    case WM_CLOSE:
        that->DestroyWindow();
        break;
    case WM_CHAR:
        switch (wParam)
        {
        case 'f':
            //[[fallthrough]]
        case 'F':
        {
            that->ToggleFullscreeen();
        }
        break;
        /*Can pause by pressing P,p, or space bar*/
        case 'p':
            //[[fallthrough]]
        case 'P':
            //[[fallthrough]]
        case ' ':
            that->TogglePause();
            break;
        case 'a':
            glEnable(GL_POINT_SMOOTH);
            glEnable(GL_LINE_SMOOTH);
            glEnable(GL_POLYGON_SMOOTH);
            break;
        case 'd':
            glDisable(GL_POINT_SMOOTH);
            glDisable(GL_LINE_SMOOTH);
            glDisable(GL_POLYGON_SMOOTH);
            break;
        default:
            break;
        }
        break;
    case WM_DESTROY:
        that->Uninitialize();
        PostQuitMessage(0);
        break;
    default:
        break;
    }

    if (that)
    {
        that->WndProcPost(props, iMsg, wParam, lParam);
    }
#ifdef _WIN32
    return DefWindowProc(props.GetHWND(), iMsg, wParam, lParam);
#else
    return 0;
#endif
}


util::ShaderCodesContainer::ShaderCodesContainer()
{
    Reset();
}

util::ShaderCodesContainer::~ShaderCodesContainer()
{
    Reset();
}

std::optional<std::string> util::ShaderCodesContainer::GetVertexShader()
{
    return m_vertexShaderCode;
}

void ShaderCodesContainer::SetVertexShaderCode(std::string code)
{
    m_vertexShaderCode = code;
}
std::optional<std::string> util::ShaderCodesContainer::GetFragmentShader()
{
    return m_fragmentShaderCode;
}
void ShaderCodesContainer::SetFragmentShaderCode(std::string code)
{
    m_fragmentShaderCode = code;
}

std::optional<GLint> util::ShaderCodesContainer::GetShaderProgram()
{
    return m_shaderProgram;
}

void util::ShaderCodesContainer::SetShaderProgram(GLint progObject)
{
    m_shaderProgram = progObject;
}

void util::ShaderCodesContainer::ResetShaderProgram()
{
    m_vertexShaderCode.reset();
    m_fragmentShaderCode.reset();
    m_shaderProgram.reset();

    for (auto& val : m_objectInfoMap)
    {
        val.second.m_attribs.clear();
    }
    m_objectInfoMap.clear();
}

void util::ShaderCodesContainer::AddUniform(std::string shaderVariableName, bool bSkipSanityCheck)
{
    if (true == bSkipSanityCheck)
    {
        //sanity - check if this variable exists in vertex shader
        if (!GetVertexShader())
        {
            THROW_VERTEXT_OR_FRAGMENT_SHADER_MISSING;
        }
        std::string vs = GetVertexShader().value();
        if (vs.npos == vs.find(shaderVariableName))
        {
            //variable not found in VS, check FS
            std::string fs = GetFragmentShader().value();
            if (fs.npos == fs.find(shaderVariableName))
            {
                THROW_UNEXPECTED_PARAMETER
            }
        }
    }

    //Dont add anything to m_uniformLoc. We dont have programobject yet - that we'll have after initShaders().
    m_uniformNames.push_back(shaderVariableName);
}

void util::ShaderCodesContainer::SetAttribVbo(DataTypes::ObjectID objectID, VertextAttributesEnum type, GLuint vbo)
{

    auto itr = m_objectInfoMap.find(objectID);

    if (m_objectInfoMap.end() == itr)
    {
        THROW_RUNTIME_ERROR("Unknown object ID");
    }

    for (auto& attrib : itr->second.m_attribs)
    {
        if (attrib.type == type)
        {
            attrib.vbo = vbo;
            return;
        }
    }

    THROW_RUNTIME_ERROR("Attribute not found");
}

bool util::ShaderCodesContainer::GetLightsSupported()
{
    return m_Lights.GetGlobalLightSupportPP();
}

ProgrammablePipeline::Lights& util::ShaderCodesContainer::GetShaderLights()
{
    return m_Lights;
}

void util::ShaderCodesContainer::SetLightsSupported(bool bAreLightsSupported, unsigned int numLightsToUse)
{
    m_Lights.SetGlobalLightSupportPP(bAreLightsSupported, numLightsToUse);
}

void util::ShaderCodesContainer::Reset()
{

    m_uniformLoc.clear();
    m_uniformNames.clear();

    for (auto& obj : m_objectInfoMap)
    {
        obj.second.m_attribs.clear();
    }

    m_objectInfoMap.clear();

    //DO NOT RESET the following, see their docs
    //ShaderCodesContainer::m_shaderVersion = 140;
    //ShaderCodesContainer::m_bAllowShaderVersionToBeSet = true;

    m_Lights.Reset();

    ResetShaderProgram();
}

void util::ShaderCodesContainer::CreateNewObject(DataTypes::ObjectID objectID, ShaderCodesContainer::ObjectDrawType objectDrawType, util::ProgrammablePipeline::ObjectLightProps objectLightProps)
{
    CreateNewObject(objectID, objectDrawType, true, objectLightProps);
}

void util::ShaderCodesContainer::CreateNewObject(DataTypes::ObjectID objectID, ShaderCodesContainer::ObjectDrawType objectDrawType, bool bIsLightSupported, util::ProgrammablePipeline::ObjectLightProps objectLightProps)
{
    auto itr = m_objectInfoMap.find(objectID);
    if (m_objectInfoMap.end() != itr)
    {
        THROW_RUNTIME_ERROR("Object Already Exists");
    }

    util::ShaderCodesContainer::ObjectInfo oi;
    oi.objectID = objectID;
    oi.m_objectDrawType = objectDrawType;
    oi.m_bIsLightSupported = bIsLightSupported;
    oi.m_lightProps = objectLightProps;
    m_objectInfoMap[objectID] = oi;
}

void util::ShaderCodesContainer::CreateNewObject(DataTypes::ObjectID objectID, ShaderCodesContainer::ObjectDrawType objectDrawType)
{
    if (m_Lights.GetGlobalLightSupportPP())
    {
        THROW_RUNTIME_ERROR("Lights are supported, must specify light properties for object - use another overload for this method");
    }

    CreateNewObject(objectID, objectDrawType, false, util::ProgrammablePipeline::ObjectLightProps());
}

void util::ShaderCodesContainer::SetShaderContainerPtr(util::ShaderCodesContainer* pShaderContainer)
{
    m_Lights.SetShaderContainerPtr(pShaderContainer);
}

bool util::ShaderCodesContainer::GlobalLightSwich(bool bTurnOn)
{
    return m_Lights.GlobalLightSwich(bTurnOn);
}

void util::ShaderCodesContainer::FlushLight()
{
    m_Lights.Flush();
}

util::ProgrammablePipeline::ObjectLightProps util::ShaderCodesContainer::GetObjectLightProperties(DataTypes::ObjectID objectID)
{
    auto itr = m_objectInfoMap.find(objectID);
    if (m_objectInfoMap.end() == itr)
    {
        THROW_RUNTIME_ERROR("Object ID not found. Perhaps CreateNewObject() not called.")
    }

    return itr->second.m_lightProps;
}

ShaderCodesContainer::ObjectDrawType util::ShaderCodesContainer::GetObjectDrawType(DataTypes::ObjectID objectID)
{
    auto itr = m_objectInfoMap.find(objectID);
    if (m_objectInfoMap.end() == itr)
    {
        THROW_RUNTIME_ERROR("Object ID not found. Perhaps CreateNewObject() not called.")
    }

    return itr->second.m_objectDrawType;
}

bool util::ShaderCodesContainer::EnableLight(const unsigned int lightNum, const bool bEnable)
{
    return m_Lights.EnableLight(lightNum, bEnable);
}

void util::ShaderCodesContainer::ConfigureLight(const unsigned int lightNum, const util::ProgrammablePipeline::LightProps& props)
{
    m_Lights.ConfigureLight(lightNum, props);
}

void util::ShaderCodesContainer::AddAttribute(DataTypes::ObjectID objectID, VertextAttributesEnum attrType, std::string shaderVariableName, std::vector<GLfloat> dataf, GLsizei stride, GLsizei components, AttributeBindTarget bindTarget, std::vector<GLuint> dataui)
{
    //sanity check 1 - if this variable exists in vertex shader
    if (!GetVertexShader())
    {
        THROW_VERTEXT_OR_FRAGMENT_SHADER_MISSING;
    }

    if (AttributeBindTarget::BT_ELEMENT_ARRAY_BUFFER != bindTarget)//Indices are used with glDrawElements() for index mode drawing, they do not correspond to a shader variable name
    {
        std::string vs = GetVertexShader().value();
        if (vs.npos == vs.find(shaderVariableName))
        {
            THROW_UNEXPECTED_PARAMETER
        }
    }

    if ((dataf.size() > 0) && (dataui.size() > 0))
    {
        THROW_RUNTIME_ERROR("Data can be only of 1 type, hence only 1 buffer should be filled");
    }

    if ((dataf.size() == 0) && (dataui.size() == 0))
    {
        THROW_RUNTIME_ERROR("Data cannot be both zero");
    }

    /*
    * sanity check 2 - if we have n components per vertext. 
    * eg. 3 for position - x, y, z
    * eg. 4 for color - RGBA
    * Thus the number of elements in dataf should be an integral multiple
    * of number of components
    */
    if ((dataf.size() > 0) && (dataf.size() % components != 0))
    {
        THROW_UNEXPECTED_PARAMETER
    }
    else if ((dataui.size() > 0) && (dataui.size() % components != 0))
    {
        THROW_UNEXPECTED_PARAMETER
    }

    VertexAttributeStruct v;
    v.type = attrType;
    v.shaderVariableName = shaderVariableName;
    v.dataf = dataf;
    v.dataui = dataui;
    v.stride = stride;
    v.components = components;
    v.vbo = -1;
    v.numVertices = 0;
    if (dataf.size() > 1)
    {
        v.numVertices = dataf.size() / components;
    }
    else if(dataui.size() > 1)
    {
        v.numVertices = dataui.size() / components;
    }
    else
    {
        THROW_RUNTIME_ERROR("Unexpected - at least 1 of dataf, dataui must have data");
    }

    v.bindTarget = bindTarget;

    auto itr = m_objectInfoMap.find(objectID);
    if (m_objectInfoMap.end() == itr)
    {
        THROW_RUNTIME_ERROR("Object ID not found. Perhaps CreateNewObject() not called.")
    }
    else
    {
        itr->second.m_attribs.push_back(v);
    }
}

util::ShaderCodesContainer::VertexAttributeStruct util::ShaderCodesContainer::GetAttribute(DataTypes::ObjectID objectID, util::ShaderCodesContainer::VertextAttributesEnum attrType)
{
    auto itr = m_objectInfoMap.find(objectID);
    if (m_objectInfoMap.end() == itr)
    {
        THROW_RUNTIME_ERROR("object not found");
    }
    else
    {
        for (const auto& attrib : itr->second.m_attribs)
        {
            if (attrib.type == attrType)
            {
                return attrib;
            }
        }
        THROW_RUNTIME_ERROR("attribute not found");
    }

    THROW_RUNTIME_ERROR("unexpected error");
}


void util::ShaderCodesContainer::SetObjectVao(DataTypes::ObjectID objectID, GLuint vao)
{
    auto itr = m_objectInfoMap.find(objectID);

    if (m_objectInfoMap.end() == itr)
    {
        THROW_RUNTIME_ERROR("Unknown Object ID");
    }

    itr->second.vao = vao;
}

GLuint util::ShaderCodesContainer::GetObjectVao(DataTypes::ObjectID objectID)
{
    auto itr = m_objectInfoMap.find(objectID);

    if (m_objectInfoMap.end() == itr)
    {
        THROW_RUNTIME_ERROR("Unknown Object ID");
    }

    return itr->second.vao;
}


std::vector<util::ShaderCodesContainer::VertexAttributeStruct> util::ShaderCodesContainer::GetAllAttribsForObject(DataTypes::ObjectID objectID)
{
    auto itr = m_objectInfoMap.find(objectID);

    if (m_objectInfoMap.end() == itr)
    {
        THROW_RUNTIME_ERROR("Unknown Object ID");
    }

    return itr->second.m_attribs;
}


std::vector<DataTypes::ObjectID> util::ShaderCodesContainer::GetAllObjectIDs()
{
    std::vector<DataTypes::ObjectID> objectIDs;
    for (const auto& ele : m_objectInfoMap)
    {
        objectIDs.push_back(ele.first);
    }
    return objectIDs;
}

std::vector<std::string> util::ShaderCodesContainer::GetAllUniformNames()
{
    return m_uniformNames;
}

void util::ShaderCodesContainer::SetUniformLocation(std::string name, GLint location)
{
    m_uniformLoc[name] = location;
}

GLint util::ShaderCodesContainer::GetUniformLocation(std::string name)
{
    auto ele = m_uniformLoc.find(name);
    if (m_uniformLoc.end() == ele)
    {
        if (m_Lights.GetGlobalLightSupportPP())
        {
            THROW_RUNTIME_ERROR("Uniform not found");
        }
        else
        {
            static const auto lightUniforms = m_Lights.GetLightUniforms();
            //if lights are disabled, then we may not have uniforms corresponding for them
            if (lightUniforms.end() != std::find(lightUniforms.begin(), lightUniforms.end(), name))
            {
                return -1;//This uniform is a uniform for light
            }
            else
            {
                THROW_RUNTIME_ERROR("Uniform not found");
            }
        }
    }
    else
    {
        return ele->second;
    }
    return -1;
}

void util::ShaderCodesContainer::SetShaderVersion(int version)
{
    if (m_bAllowShaderVersionToBeSet || (ShaderCodesContainer::m_shaderVersion == version))
    {
        ShaderCodesContainer::m_shaderVersion = version;
        m_bAllowShaderVersionToBeSet = false;
    }else if(ShaderCodesContainer::m_shaderVersion != version)
    {
        THROW_RUNTIME_ERROR("Cannot change shader version now");
    }
}

int util::ShaderCodesContainer::GetShaderVersion()
{
    return ShaderCodesContainer::m_shaderVersion;
}

int util::ShaderCodesContainer::GLSL2OGLVersion(int version)
{
#define X(a,b) case b: return a; break;
    switch (version)
    {
        OGL_GLSHVERSION_MAP
    default:
        THROW_RUNTIME_ERROR("OGL2GLSLVersion() error");
            break;
    }
#undef X
    THROW_RUNTIME_ERROR("OGL2GLSLVersion() error");
    return INT_MIN;
}

int util::ShaderCodesContainer::OGL2GLSLVersion(int version)
{
#define X(a,b) case a: return b; break;
    switch (version)
    {
        OGL_GLSHVERSION_MAP
    default:
        THROW_RUNTIME_ERROR("OGL2GLSLVersion() error");
            break;
    }
#undef X
    THROW_RUNTIME_ERROR("OGL2GLSLVersion() error");
    return INT_MIN;

}

std::tuple<bool, std::string> util::OglWindow::CheckProgramLinkStatus(GLuint shaderObject)
{
    return CheckCompileOrLinkStatus(shaderObject, GL_LINK_STATUS);
}

std::tuple<bool, std::string> util::OglWindow::CheckShaderCompileStatus(GLuint shaderObject)
{
    return CheckCompileOrLinkStatus(shaderObject, GL_COMPILE_STATUS);
}

std::tuple<bool, std::string> util::OglWindow::CheckCompileOrLinkStatus(GLuint iObject, int statusToCheck)
{
    bool retval = true;
    std::string logBuf;
    GLint iStatus = 0;

    if (GL_COMPILE_STATUS == statusToCheck)
    {
        glGetShaderiv(iObject, statusToCheck, &iStatus);
    }
    else if (GL_LINK_STATUS == statusToCheck)
    {
        glGetProgramiv(iObject, statusToCheck, &iStatus);
    }
    else
    {
        THROW_UNEXPECTED_PARAMETER
    }
    if (GL_FALSE == iStatus)
    {//Error in compilation
        retval = false;
        GLint iLogLength = 0;
        if (GL_COMPILE_STATUS == statusToCheck)
        {
            glGetShaderiv(iObject, GL_INFO_LOG_LENGTH, &iLogLength);
        }
        else if (GL_LINK_STATUS == statusToCheck)
        {
            glGetProgramiv(iObject, GL_INFO_LOG_LENGTH, &iLogLength);
        }
        else
        {
            THROW_UNEXPECTED_PARAMETER
        }

        if (iLogLength > 0)
        {
            std::unique_ptr<GLchar[]> szInfoLog(new GLchar[iLogLength]);
            if (!szInfoLog)
            {
                THROW_MALLOC_ERROR;
            }
            GLsizei charsWritten = 0;
            if (GL_COMPILE_STATUS == statusToCheck)
            {
                glGetShaderInfoLog(iObject, iLogLength, &charsWritten, szInfoLog.get());
            }
            else if (GL_LINK_STATUS == statusToCheck)
            {
                glGetProgramiv(iObject, GL_INFO_LOG_LENGTH, &iLogLength);
                glGetProgramInfoLog(iObject, iLogLength, &charsWritten, szInfoLog.get());
            }
            else
            {
                THROW_UNEXPECTED_PARAMETER
            }
            logBuf = szInfoLog.get();
        }
    }
    return { retval, logBuf };
}

unsigned int util::OglWindow::GetNumShaderPrograms()
{
    return (unsigned int)m_shaderCodesMap.size();
}

void util::OglWindow::CreateNewShaderProgram(util::DataTypes::ProgramID programID, bool bAreLightsSupported, unsigned int numLightsToUse)
{
    auto itr = m_shaderCodesMap.find(programID);

    if (itr != m_shaderCodesMap.end())
    {
        THROW_RUNTIME_ERROR("Program with the given ID already exists");
    }

    ShaderCodesContainer scode;
    scode.SetLightsSupported(bAreLightsSupported, numLightsToUse);
    m_shaderCodesMap[programID] = scode;

    SetCurrentShaderProgram(programID);
}

void util::OglWindow::SetCurrentShaderProgram(util::DataTypes::ProgramID programID)
{
    auto itr = m_shaderCodesMap.find(programID);

    if (itr == m_shaderCodesMap.end())
    {
        THROW_RUNTIME_ERROR("Program with the given ID not found");
    }

    m_currentProgramID = programID;
}

void util::OglWindow::ProcessFragmentShaderForLight(std::string& fsCode)
{
    //sanity check - we have to insert light specific code inside main, for that we will need marker
    auto mainBeginDelim = util::ProgrammablePipeline::ShaderCodeSectionDelimitersList::mainSection.startDelimiter;
    /*messing with find*///std::for_each(mainBeginDelim.begin(), mainBeginDelim.end(), [](char& c) {if (c == '\r' || c == '\n') { c = '\0'; }});//get rid of terminating \n - unix/windows line endings
    if (fsCode.npos == fsCode.find(mainBeginDelim))
    {
        THROW_RUNTIME_ERROR("the shader must have a delimiter to indicate the place where void main(){ function body starts, so that light specific code can be inserted");
    }

    const auto& FSLightCode = util::ProgrammablePipeline::Lights::GetLightFShaderCodeVariables();

    int maxNumLights = util::ProgrammablePipeline::Lights::GetMaxLights();
    std::string newFsCode, line;
    std::istringstream iss(fsCode);
    bool bFoundVersionSectionEnd = false;
    int mainFound = 0;

    while (std::getline(iss, line))
    {
        newFsCode.append(line);
        newFsCode.append("\n");

        if ((!bFoundVersionSectionEnd) && (line.npos != line.find(util::ProgrammablePipeline::ShaderCodeSectionDelimitersList::versionSection.endDelimiter)))
        {
            bFoundVersionSectionEnd = true;
            newFsCode.append("\n");
            newFsCode.append(util::ProgrammablePipeline::ShaderCodeSectionDelimitersList::lightVarsSection.startDelimiter);
            newFsCode.append("\n");
            newFsCode.append("#define LIGHTS_SUPPORTED 1 \n");
            newFsCode.append("#define MAX_LIGHTS " + std::to_string(maxNumLights) + "\n");
            newFsCode.append(FSLightCode);
            newFsCode.append("\n");
            newFsCode.append(util::ProgrammablePipeline::ShaderCodeSectionDelimitersList::lightVarsSection.endDelimiter);
            newFsCode.append("\n");
        }

        if (line.npos != line.find(mainBeginDelim))
        {
            mainFound++;
            newFsCode.append(util::ProgrammablePipeline::Lights::GetLightFShaderCodeMainCode());
        }
    }

    //sanity check - main begin delimiter should be found exactly once - HAS BEEN HIT (at least once the sanity was useful in finding bugs)
    if (mainFound != 1)
    {
        THROW_RUNTIME_ERROR("Error in  main detection logic - either no main() found, or more than 1 main found");
    }

    fsCode = newFsCode;
}

void util::OglWindow::ProcessVertexShaderForLight(std::string& vsCode, util::ShaderCodesContainer& shaderCodeC)
{
    /*
    * CONSTRAINTS ON VERTEX SHADER
    * 1. must have version section, start of main() body marked by delimiters
    * 2. must have vNormal (vec3), and vPosition (vec4) input variables
    */

    //sanity check - for lights we need u_m_matrix, u_v_matrix i.e. model, and view matrices separately
    if ((vsCode.npos == vsCode.find("u_m_matrix")) || (vsCode.npos == vsCode.find("u_v_matrix")))
    {
        THROW_RUNTIME_ERROR("one or both missing - u_m_matrix, u_v_matrix");
    }

    //sanity check - vNormal and vPosition must exist in shader. They are used for lighting calculations
    if ((vsCode.npos == vsCode.find("vNormal")) || (vsCode.npos == vsCode.find("vPosition")))//for light normals
    {
        THROW_RUNTIME_ERROR("vNormal and vPosition must exist in shader.");
    }

    //sanity check - we have to insert light specific code inside main, for that we will need marker
    auto mainBeginDelim = util::ProgrammablePipeline::ShaderCodeSectionDelimitersList::mainSection.startDelimiter;
    /*messing with find*///std::for_each(mainBeginDelim.begin(), mainBeginDelim.end(), [](char& c) {if (c == '\r' || c == '\n') { c = '\0'; }});//get rid of terminating \n - unix/windows line endings
    auto strPos = vsCode.find(mainBeginDelim);
    if (vsCode.npos == strPos)
    {
        THROW_RUNTIME_ERROR("the shader must have a delimiter to indicate the place where void main(){ function body starts, so that light specific code can be inserted");
    }

    const auto& VSLightCode = util::ProgrammablePipeline::Lights::GetLightVShaderCodeVariables();

    //sanity - make sure all uniforms we want exists in code
    auto lightUniforms = shaderCodeC.GetShaderLights().GetLightUniforms(false);
    for (const auto& uniform : lightUniforms)
    {
        if (VSLightCode.npos == VSLightCode.find(uniform))
        {
            THROW_RUNTIME_ERROR("uniform not found");
        }
    }

    int maxNumLights = util::ProgrammablePipeline::Lights::GetMaxLights();

    std::string newVsCode, line;
    std::istringstream iss(vsCode);

    //we want to skip version string section before adding code for lights
    bool bFoundVersionSectionEnd = false;
    int mainFound = 0;
    while (std::getline(iss, line))
    {
        newVsCode.append(line);
        newVsCode.append("\n");
        if ((!bFoundVersionSectionEnd) && (line.npos != line.find(util::ProgrammablePipeline::ShaderCodeSectionDelimitersList::versionSection.endDelimiter)))
        {
            bFoundVersionSectionEnd = true;
            newVsCode.append("\n");
            newVsCode.append(util::ProgrammablePipeline::ShaderCodeSectionDelimitersList::lightVarsSection.startDelimiter);
            newVsCode.append("\n");
            newVsCode.append("#define LIGHTS_SUPPORTED 1 \n");
            newVsCode.append("#define MAX_LIGHTS " + std::to_string(maxNumLights) + "\n");
            newVsCode.append(VSLightCode);
            newVsCode.append("\n");
            newVsCode.append(util::ProgrammablePipeline::ShaderCodeSectionDelimitersList::lightVarsSection.endDelimiter);
            newVsCode.append("\n");
        }

        if (line.npos != line.find(mainBeginDelim))
        {
            mainFound++;
            newVsCode.append(util::ProgrammablePipeline::Lights::GetLightVShaderCodeMainCode());
        }
    }

    //sanity check - main begin delimiter should be found exactly once
    if (mainFound != 1)
    {
        THROW_RUNTIME_ERROR("Error in  main detection logic - either no main() found, or more than 1 main found");
    }

    vsCode = newVsCode;

    shaderCodeC.SetVertexShaderCode(vsCode);

    //Add uniforms for light
    lightUniforms = shaderCodeC.GetShaderLights().GetLightUniforms(true);
    for (const auto& uniform : lightUniforms)
    {
        shaderCodeC.AddUniform(uniform);
    }

    SetShaderContainerPtr(&shaderCodeC);
}

void util::OglWindow::InitShaders()
{

    if (!(m_InitMask & INIT_MASK_ENABLE_PROGRAMMABLE_PIPELINE))
    {
        THROW_USING_PPCODE_FROM_FFP_CONTEXT;
    }

    for (auto& shaderProgram : m_shaderCodesMap)
    {
        //We should at least have vertex, and fragment shaders set
        if (!GetVertexShaderWithProgramID(shaderProgram.first) || !GetFragmentShaderWithProgramID(shaderProgram.first))
        {
            THROW_VERTEXT_OR_FRAGMENT_SHADER_MISSING;
        }

#pragma region vertex shader
        GLuint vertexShaderObject = 0;
        {
            vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
            auto shader_str = GetVertexShaderWithProgramID(shaderProgram.first).value();

            auto shader_str_c = shader_str.c_str();
            glShaderSource(vertexShaderObject, 1, &shader_str_c, nullptr);
            if (m_InitMask & INIT_MASK_DUMP_SHADER_CODE)
            {
                std::ofstream of(SHADER_DUMP_FILE_NAME, std::ios::app);
                of << "\n\nvertex shader:\n ";
                of << shader_str;
                of.close();
            }

            glCompileShader(vertexShaderObject);

            //CheckCompileStatus
            auto [bStatus, logBuf] = CheckShaderCompileStatus(vertexShaderObject);

            if (false == bStatus)
            {
                LOG(LogLevel::FatalError, logBuf);
                THROW_SHADER_COMPILE_FAILED
            }
        }
#pragma endregion

#pragma region fragment shader
        GLuint fragmentShaderObject = 0;
        {
            fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
            auto shader_str = GetFragmentShaderWithProgramID(shaderProgram.first).value();
            auto shader_str_c = shader_str.c_str();
            glShaderSource(fragmentShaderObject, 1, &shader_str_c, nullptr);
            if (m_InitMask & INIT_MASK_DUMP_SHADER_CODE)
            {
                std::ofstream of(SHADER_DUMP_FILE_NAME, std::ios::app);
                of << "\n\nfragment shader:\n ";
                of << shader_str;
                of.close();
            }

            glCompileShader(fragmentShaderObject);

            //CheckCompileStatus
            auto [bStatus, logBuf] = CheckShaderCompileStatus(fragmentShaderObject);

            if (false == bStatus)
            {
#ifdef _WIN32
                OutputDebugStringA(logBuf.c_str());
#endif
                LOG(LogLevel::FatalError, logBuf);
                THROW_SHADER_COMPILE_FAILED
            }
        }

        GLint shaderProgramObject = glCreateProgram();
        if (0 == shaderProgramObject)
        {
            THROW_RUNTIME_ERROR("glCreateProgram() failed")
        }
        glAttachShader(shaderProgramObject, vertexShaderObject);
        glAttachShader(shaderProgramObject, fragmentShaderObject);

        //specify attributes
        auto objectsIDs = shaderProgram.second.GetAllObjectIDs();

        if (objectsIDs.empty())
        {
            LOG(LogLevel::Debug, "No objects present");
        }
        else
        {
            for (const auto& objID : objectsIDs) {

                /*
                * One  vao for each object
                * each vao to have as many buffers as no. of attributes in object
                */
                //vao
                GLuint vao = 0;;
                glGenVertexArrays(1, &vao);
                glBindVertexArray(vao);
                shaderProgram.second.SetObjectVao(objID, vao);

                auto attribs = shaderProgram.second.GetAllAttribsForObject(objID);
                auto numAttribs = attribs.size();

                //vbo
                std::unique_ptr<GLuint[]>  vbo(new GLuint[numAttribs]);
                glGenBuffers(numAttribs, vbo.get());

                int iAttrib = 0;
                for (const auto& attrib : attribs)
                {
                    auto err = glGetError(); err;
                    //associate vbo to each attribute
                    /*
                    * TIP : Instead of using glBindAttribLocation() one can use "(location=0) in vec4 position;" in shader to specify the
                    * location of attribute directly (OpenGL/GLSL 3.3 onwards, GLES 3.0/GLSL 300 es onwards).
                    * https://stackoverflow.com/a/4638906/981766
                    * https://stackoverflow.com/a/24485141/981766
                    * It is possible to bind locations of uniform variables too (if so, this must be done before linking) using
                    * glBindUniformLocation()
                    * https://src.chromium.org/viewvc/chrome/trunk/src/gpu/GLES2/extensions/CHROMIUM/CHROMIUM_bind_uniform_location.txt
                    */
                    if (ShaderCodesContainer::AttributeBindTarget::BT_ARRAY_BUFFER == attrib.bindTarget)
                    {
                        glBindAttribLocation(shaderProgramObject, (GLuint)attrib.type, attrib.shaderVariableName.c_str());
                    }

                    glBindBuffer((GLenum)attrib.bindTarget, vbo[iAttrib]);

                    if (attrib.dataf.size() > 0)
                    {
                        glBufferData((GLenum)attrib.bindTarget, attrib.dataf.size() * sizeof(attrib.dataf[0]), attrib.dataf.data(), GL_STATIC_DRAW);
                    } else if (attrib.dataui.size() > 0)
                    {
                        glBufferData((GLenum)attrib.bindTarget, attrib.dataui.size() * sizeof(attrib.dataui[0]), attrib.dataui.data(), GL_STATIC_DRAW);
                    }

                    if (ShaderCodesContainer::AttributeBindTarget::BT_ARRAY_BUFFER == attrib.bindTarget)
                    {
                        if (attrib.dataf.size() > 0)
                        {
                            glVertexAttribPointer((GLuint)attrib.type, attrib.components, GL_FLOAT, GL_FALSE, attrib.stride, nullptr);
                        }
                        else if (attrib.dataui.size() > 0)
                        {
                            glVertexAttribPointer((GLuint)attrib.type, attrib.components, GL_UNSIGNED_INT, GL_FALSE, attrib.stride, nullptr);
                        }
                        glEnableVertexAttribArray((GLuint)attrib.type);
                    }

                    glBindBuffer((GLenum)attrib.bindTarget, 0);

                    shaderProgram.second.SetAttribVbo(objID, attrib.type, vbo[iAttrib]);
                    iAttrib++;
                }
                glBindVertexArray(0);
            }
        }

        glLinkProgram(shaderProgramObject);

        //CheckLinkStatus
        auto [bStatus, logBuf] = CheckProgramLinkStatus(shaderProgramObject);

        if (false == bStatus)
        {
            LOG(LogLevel::FatalError, logBuf);
            THROW_SHADER_LINK_FAILED
        }
        shaderProgram.second.SetShaderProgram(shaderProgramObject);

        //uniforms
        for (auto uni : shaderProgram.second.GetAllUniformNames())
        {
            GLint loc = glGetUniformLocation(shaderProgramObject, uni.c_str());

            if (-1 == loc)
            {
                THROW_RUNTIME_ERROR("glGetUniformLocation() failed - Cannot find uniform. Careful that OGL may optimize away unused variables");
            }

            shaderProgram.second.SetUniformLocation(uni, loc);
        }
    }
#pragma endregion
}

void util::OglWindow::CreateNewObject(DataTypes::ObjectID objectID, ShaderCodesContainer::ObjectDrawType objectDrawType, util::ProgrammablePipeline::ObjectLightProps objectLightProps)
{
    util::DataTypes::ProgramID programID = m_currentProgramID;

    auto itr = m_shaderCodesMap.find(programID);
    if (itr == m_shaderCodesMap.end())
    {
        THROW_RUNTIME_ERROR("Program ID not found. Perhaps CreateNewShaderProgram() wasn't called for this program ID");
    }

    itr->second.CreateNewObject(objectID, objectDrawType, objectLightProps);
}
void util::OglWindow::CreateNewObject(DataTypes::ObjectID objectID, ShaderCodesContainer::ObjectDrawType objectDrawType)
{
    util::DataTypes::ProgramID programID = m_currentProgramID;

    auto itr = m_shaderCodesMap.find(programID);
    if (itr == m_shaderCodesMap.end())
    {
        THROW_RUNTIME_ERROR("Program ID not found. Perhaps CreateNewShaderProgram() wasn't called for this program ID");
    }

    itr->second.CreateNewObject(objectID, objectDrawType);
}

void util::OglWindow::SetShaderContainerPtr(util::ShaderCodesContainer* pShaderContainer)
{
    util::DataTypes::ProgramID programID = m_currentProgramID;

    auto itr = m_shaderCodesMap.find(programID);
    if (itr == m_shaderCodesMap.end())
    {
        THROW_RUNTIME_ERROR("Program ID not found. Perhaps CreateNewShaderProgram() wasn't called for this program ID");
    }

    return itr->second.SetShaderContainerPtr(pShaderContainer);
}


bool util::OglWindow::GetGlobalLightSupportPP()
{
    util::DataTypes::ProgramID programID = m_currentProgramID;

    auto itr = m_shaderCodesMap.find(programID);
    if (itr == m_shaderCodesMap.end())
    {
        THROW_RUNTIME_ERROR("Program ID not found. Perhaps CreateNewShaderProgram() wasn't called for this program ID");
    }

    return itr->second.GetLightsSupported();
}

bool util::OglWindow::GlobalLightSwich(bool bTurnOn)
{
    util::DataTypes::ProgramID programID = m_currentProgramID;

    auto itr = m_shaderCodesMap.find(programID);
    if (itr == m_shaderCodesMap.end())
    {
        THROW_RUNTIME_ERROR("Program ID not found. Perhaps CreateNewShaderProgram() wasn't called for this program ID");
    }

    return itr->second.GlobalLightSwich(bTurnOn);
}

void util::OglWindow::FlushLight()
{
    util::DataTypes::ProgramID programID = m_currentProgramID;

    auto itr = m_shaderCodesMap.find(programID);
    if (itr == m_shaderCodesMap.end())
    {
        THROW_RUNTIME_ERROR("Program ID not found. Perhaps CreateNewShaderProgram() wasn't called for this program ID");
    }

    itr->second.FlushLight();
}

util::ProgrammablePipeline::ObjectLightProps util::OglWindow::GetObjectLightProperties(DataTypes::ObjectID objectID)
{
    util::DataTypes::ProgramID programID = m_currentProgramID;

    auto itr = m_shaderCodesMap.find(programID);
    if (itr == m_shaderCodesMap.end())
    {
        THROW_RUNTIME_ERROR("Program ID not found. Perhaps CreateNewShaderProgram() wasn't called for this program ID");
    }

    return itr->second.GetObjectLightProperties(objectID);
}

ShaderCodesContainer::ObjectDrawType util::OglWindow::GetObjectDrawType(DataTypes::ObjectID objectID)
{
    util::DataTypes::ProgramID programID = m_currentProgramID;

    auto itr = m_shaderCodesMap.find(programID);
    if (itr == m_shaderCodesMap.end())
    {
        THROW_RUNTIME_ERROR("Program ID not found. Perhaps CreateNewShaderProgram() wasn't called for this program ID");
    }

    return itr->second.GetObjectDrawType(objectID);
}

void util::OglWindow::DrawObject(DataTypes::ObjectID objectID)
{
    glBindVertexArray(GetObjectVao(objectID));

    auto objectLightProps = GetObjectLightProperties(objectID);
    auto uniformLoc = GetUniformLocation("u_KA_vec3");
    if (-1 != uniformLoc)
    {
        glUniform3f(uniformLoc, objectLightProps.m_KA[0], objectLightProps.m_KA[1], objectLightProps.m_KA[2]);
    }

    uniformLoc = GetUniformLocation("u_KD_vec3");
    if (-1 != uniformLoc)
    {
        glUniform3f(uniformLoc, objectLightProps.m_KD[0], objectLightProps.m_KD[1], objectLightProps.m_KD[2]);
    }

    uniformLoc = GetUniformLocation("u_KS_vec3");
    if (-1 != uniformLoc)
    {
        glUniform3f(uniformLoc, objectLightProps.m_KS[0], objectLightProps.m_KS[1], objectLightProps.m_KS[2]);
    }

    uniformLoc = GetUniformLocation("u_shininess");
    if (-1 != uniformLoc)
    {
        glUniform1f(uniformLoc, objectLightProps.m_shininess);
    }

    if (GetGlobalLightSupportPP())
    {
        FlushLight();
    }

    if (util::ShaderCodesContainer::ObjectDrawType::DT_DrawArrays_Triangles == GetObjectDrawType(objectID))
    {
        auto numVertices = GetAttribute(objectID, util::ShaderCodesContainer::VertextAttributesEnum::AMC_ATTRIBUTE_POSITION).numVertices;
        glDrawArrays(GL_TRIANGLES, 0, numVertices);
    }
    else if (util::ShaderCodesContainer::ObjectDrawType::DT_DrawArrays_TriangleFan == GetObjectDrawType(objectID))
    {
        auto numVertices = GetAttribute(objectID, util::ShaderCodesContainer::VertextAttributesEnum::AMC_ATTRIBUTE_POSITION).numVertices;
        glDrawArrays(GL_TRIANGLE_FAN, 0, numVertices);
    }
    else if (util::ShaderCodesContainer::ObjectDrawType::DT_DrawElements_Triangles == GetObjectDrawType(objectID))
    {
        auto attr = GetAttribute(objectID, util::ShaderCodesContainer::VertextAttributesEnum::AMC_ATTRIBUTE_INDICES);
        auto numVertices = attr.numVertices;
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, attr.vbo);
        glDrawElements(GL_TRIANGLES, numVertices, GL_UNSIGNED_INT, nullptr);
    }
    else
    {
        THROW_RUNTIME_ERROR("Unknown draw type");
    }
}

bool util::OglWindow::EnableLight(const unsigned int lightNum, const bool bEnable)
{
    util::DataTypes::ProgramID programID = m_currentProgramID;

    auto itr = m_shaderCodesMap.find(programID);
    if (itr == m_shaderCodesMap.end())
    {
        THROW_RUNTIME_ERROR("Program ID not found. Perhaps CreateNewShaderProgram() wasn't called for this program ID");
    }
    return itr->second.EnableLight(lightNum, bEnable);
}

void util::OglWindow::ConfigureLight(const unsigned int lightNum, const util::ProgrammablePipeline::LightProps& props)
{
    util::DataTypes::ProgramID programID = m_currentProgramID;

    auto itr = m_shaderCodesMap.find(programID);
    if (itr == m_shaderCodesMap.end())
    {
        THROW_RUNTIME_ERROR("Program ID not found. Perhaps CreateNewShaderProgram() wasn't called for this program ID");
    }
    itr->second.ConfigureLight(lightNum, props);
}

void util::OglWindow::AddAttribute(util::DataTypes::ObjectID objectID, ShaderCodesContainer::VertextAttributesEnum attrType, std::string shaderVariableName, std::vector<GLfloat> dataf, GLsizei stride, GLsizei componets, ShaderCodesContainer::AttributeBindTarget bindTarget, std::vector<GLuint> dataui)
{
    util::DataTypes::ProgramID programID = m_currentProgramID;

    auto itr = m_shaderCodesMap.find(programID);
    if (itr == m_shaderCodesMap.end())
    {
        THROW_RUNTIME_ERROR("Program ID not found. Perhaps CreateNewShaderProgram() wasn't called for this program ID");
    }

    itr->second.AddAttribute(objectID, attrType, shaderVariableName, dataf, stride, componets, bindTarget, dataui);

    if (m_InitMask & INIT_MASK_DUMP_ATTRIBUTES)
    {
        std::ofstream of(ATTRIBUTE_DUMP_FILE_NAME, std::ios::app);
        of << std::fixed;
        of << "\n\n";
        of << "programID: " << programID << "\n";
        of << "objectID: " << objectID << "\n";
        of << "attrType: " << (GLuint)attrType << "\n";
        of << "stride: " << stride << "\n";
        of << "componets: " << componets << "\n";
        of << "shaderVariableName: " << shaderVariableName << "\n";
        of << "bindTarget: " << (GLenum)bindTarget << "\n";
        of << "dataf: ";

        for (const auto& ele : dataf)
        {
            of << ele << "f, ";
        }

        of << "\ndatai: ";

        for (const auto& ele : dataui)
        {
            of << ele;
        }

        of.close();
    }
}


util::ShaderCodesContainer::VertexAttributeStruct util::OglWindow::GetAttribute(DataTypes::ObjectID objectID, util::ShaderCodesContainer::VertextAttributesEnum attrType)
{
    util::DataTypes::ProgramID programID = m_currentProgramID;

    auto itr = m_shaderCodesMap.find(programID);
    if (itr == m_shaderCodesMap.end())
    {
        THROW_RUNTIME_ERROR("Program ID not found. Perhaps CreateNewShaderProgram() wasn't called for this program ID");
    }

    return itr->second.GetAttribute(objectID, attrType);
}

void util::OglWindow::AddUniform(std::string shaderVariableName)
{
    util::DataTypes::ProgramID programID = m_currentProgramID;

    auto itr = m_shaderCodesMap.find(programID);
    if (itr == m_shaderCodesMap.end())
    {
        THROW_RUNTIME_ERROR("Program ID not found. Perhaps CreateNewShaderProgram() wasn't called for this program ID");
    }

    itr->second.AddUniform(shaderVariableName);
}

GLint util::OglWindow::GetUniformLocation(std::string name)
{
    util::DataTypes::ProgramID programID = m_currentProgramID;

    auto itr = m_shaderCodesMap.find(programID);
    if (itr == m_shaderCodesMap.end())
    {
        THROW_RUNTIME_ERROR("Program ID not found. Perhaps CreateNewShaderProgram() wasn't called for this program ID");
    }

    return itr->second.GetUniformLocation(name);
}

void util::OglWindow::SetShaderVersion(int version)
{
    ShaderCodesContainer::SetShaderVersion(version);
}

int util::OglWindow::GetShaderVersion()
{
    return ShaderCodesContainer::GetShaderVersion();
}

void util::OglWindow::SetVertexShaderCode(std::string code)
{
    util::DataTypes::ProgramID progID = m_currentProgramID;

    auto itr = m_shaderCodesMap.find(progID);
    if (itr == m_shaderCodesMap.end())
    {
        THROW_RUNTIME_ERROR("Program ID not found. Perhaps CreateNewShaderProgram() wasn't called for this program ID");
    }

    ShaderCodesContainer::m_bAllowShaderVersionToBeSet = false;

    //version string format - "#version 450 core"
    std::string versionStr = "#version ";
    versionStr += std::to_string(GetShaderVersion());
    /*
    * Specifying profile names started OpenGL 3.2 onwards which has GLSL 150.
    * see https://www.khronos.org/opengl/wiki/OpenGL_Context#OpenGL_3.2_and_Profiles
    */
    if (GetShaderVersion() > 140)
    {
        versionStr += " core ";
    }
    versionStr += "\n";

    auto startDelim = util::ProgrammablePipeline::ShaderCodeSectionDelimitersList::versionSection.startDelimiter;
    auto endDelim = util::ProgrammablePipeline::ShaderCodeSectionDelimitersList::versionSection.endDelimiter;

    code = startDelim + "\n" +versionStr + "\n" + endDelim + "\n" + code;

    if (itr->second.GetLightsSupported())
    {
        ProcessVertexShaderForLight(code, itr->second);
    }

    itr->second.SetVertexShaderCode(code);
}

std::optional<std::string> util::OglWindow::GetVertexShader()
{
    return GetVertexShaderWithProgramID(m_currentProgramID);
}

std::optional<std::string> util::OglWindow::GetVertexShaderWithProgramID(util::DataTypes::ProgramID programID)
{
    auto itr = m_shaderCodesMap.find(programID);
    if (itr == m_shaderCodesMap.end())
    {
        THROW_RUNTIME_ERROR("Program ID not found. Perhaps CreateNewShaderProgram() wasn't called for this program ID");
    }

    return itr->second.GetVertexShader();
}

std::optional<std::string> util::OglWindow::GetFragmentShader()
{
    return GetFragmentShaderWithProgramID(m_currentProgramID);
}

std::optional<std::string> util::OglWindow::GetFragmentShaderWithProgramID(util::DataTypes::ProgramID programID)
{
    auto itr = m_shaderCodesMap.find(programID);
    if (itr == m_shaderCodesMap.end())
    {
        THROW_RUNTIME_ERROR("Program ID not found. Perhaps CreateNewShaderProgram() wasn't called for this program ID");
    }

    return itr->second.GetFragmentShader();
}

void OglWindow::SetFragmentShaderCode( std::string code)
{
    util::DataTypes::ProgramID progID = m_currentProgramID;

    auto itr = m_shaderCodesMap.find(progID);
    if (itr == m_shaderCodesMap.end())
    {
        THROW_RUNTIME_ERROR("Program ID not found. Perhaps CreateNewShaderProgram() wasn't called for this program ID");
    }

    //version string format - "#version 450 core"
    std::string versionStr = "#version ";
    versionStr += std::to_string(GetShaderVersion());

    /*
    * Specifying profile names started OpenGL 3.2 onwards which has GLSL 150.
    * see https://www.khronos.org/opengl/wiki/OpenGL_Context#OpenGL_3.2_and_Profiles
    */
    if (GetShaderVersion() > 140)
    {
        versionStr += " core ";
    }
    versionStr += "\n";

    auto startDelim = util::ProgrammablePipeline::ShaderCodeSectionDelimitersList::versionSection.startDelimiter;
    auto endDelim = util::ProgrammablePipeline::ShaderCodeSectionDelimitersList::versionSection.endDelimiter;

    code = startDelim + "\n" + versionStr + "\n" + endDelim + "\n" + code;

    if (itr->second.GetLightsSupported())
    {
        ProcessFragmentShaderForLight(code);
    }

    itr->second.SetFragmentShaderCode(code);
}

std::optional<GLint> util::OglWindow::GetShaderProgram()
{
    return GetShaderProgramWithProgramID(m_currentProgramID);
}

std::optional<GLint> util::OglWindow::GetShaderProgramWithProgramID(util::DataTypes::ProgramID programID)
{
    util::DataTypes::ProgramID progNumber = programID;

    auto itr = m_shaderCodesMap.find(progNumber);
    if (itr == m_shaderCodesMap.end())
    {
        THROW_RUNTIME_ERROR("Program with the given ID not found")
    }

    return itr->second.GetShaderProgram();
}

GLuint util::OglWindow::GetObjectVao(DataTypes::ObjectID objectID)
{
    util::DataTypes::ProgramID programID = m_currentProgramID;

    auto itr = m_shaderCodesMap.find(programID);
    if (itr == m_shaderCodesMap.end())
    {
        THROW_RUNTIME_ERROR("Program with the given ID not found")
    }

    return itr->second.GetObjectVao(objectID);
}

void DrawShapes::DrawRectangleLines(double left, double bottom, double right, double top, COLORREF color)
{
    glEnable(GL_LINE_SMOOTH);
    glBegin(GL_LINE_LOOP);
    glColor3f(GetRValuef(color), GetGValuef(color), GetBValuef(color));
    glVertex2f(left, bottom);
    glVertex2f(right, bottom);
    glVertex2f(right, top);
    glVertex2f(left, top);
    glEnd();
}

void DrawShapes::DrawTriangleLines(DataTypes::POINTd p1, DataTypes::POINTd p2, DataTypes::POINTd p3, COLORREF color)
{
    glBegin(GL_LINE_LOOP);
    glColor3f(GetRValuef(color), GetGValuef(color), GetBValuef(color));
    glVertex2f(p1.x, p1.y);
    glVertex2f(p2.x, p2.y);
    glVertex2f(p3.x, p3.y);
    glEnd();
}

void DrawShapes::DrawTriangleSolid(DataTypes::POINTd p1, DataTypes::POINTd p2, DataTypes::POINTd p3, COLORREF color)
{
    glBegin(GL_TRIANGLES);
    glColor3f(GetRValuef(color), GetGValuef(color), GetBValuef(color));
    glVertex2f(p1.x, p1.y);
    glVertex2f(p2.x, p2.y);
    glVertex2f(p3.x, p3.y);
    glEnd();
}

void DrawShapes::DrawEqTriangleByIncentre(DataTypes::Centre centre, DataTypes::Radius radius, COLORREF color)
{
    static const double PI = 4 * atan(1);

    DataTypes::POINTd P1 = { centre.x, centre.y + radius / sin(PI / 6) };
    DataTypes::POINTd P2 = { centre.x - radius / tan(PI / 6), centre.y - radius };
    DataTypes::POINTd P3 = { centre.x + radius / tan(PI / 6), centre.y - radius };
    DrawTriangleLines(P1, P2, P3, color);
}

std::tuple<DataTypes::Centre, DataTypes::Radius> util::DrawShapes::GetTriangleIncentre(DataTypes::POINTd p1, DataTypes::POINTd p2, DataTypes::POINTd p3)
{
    double a = pow(pow((p2.x - p3.x), 2) + pow((p2.y - p3.y), 2), 0.5);
    double b = pow(pow((p1.x - p3.x), 2) + pow((p1.y - p3.y), 2), 0.5);
    double c = pow(pow((p2.x - p1.x), 2) + pow((p2.y - p1.y), 2), 0.5);
    DataTypes::POINTd inCentre = {};
    inCentre.x = (a * p1.x + b * p2.x + c * p3.x) / (a + b + c);
    inCentre.y = (a * p1.y + b * p2.y + c * p3.y) / (a + b + c);

    double s = (a + b + c) / 2;
    double area = pow(s * (s - a) * (s - b) * (s - c), 0.5);
    double inRadius = (2 * area) / (a + b + c);
    return { inCentre, inRadius };
}

void DrawShapes::DrawCircleLines(UINT NUM_PTS, double RADIUS, COLORREF color)
{
    static double PI = 4 * atan(1);
    const double theta_delta = (2.0 * PI) / NUM_PTS;
    glBegin(GL_LINE_LOOP);
    glColor3f(GetRValuef(color), GetGValuef(color), GetBValuef(color));
    double x, y;
    double theta = 0.0;
    for (int i = 1; i <= NUM_PTS; i++)
    {
        x = RADIUS * cos(theta); y = RADIUS * sin(theta);
        theta += theta_delta;
        theta = (theta >= 2.0 * PI) ? (2.0 * PI) / NUM_PTS : theta;
        glVertex2f(x, y);
    }
    glEnd();
}


void DrawShapes::DrawCircleArcTriangleFan(double RADIUS, double startAngle, double endAngle, COLORREF centreColor, COLORREF boundaryColor, UINT NUM_PTS)
{
    static double PI = 4 * atan(1);
    const double theta_delta = std::fabs(endAngle - startAngle) / NUM_PTS;
    glBegin(GL_TRIANGLE_FAN);
    double x = 0, y = 0;
    double theta = startAngle;

    glColor3f(GetRValuef(centreColor), GetGValuef(centreColor), GetBValuef(centreColor));
    glVertex2f(x, y);

    glColor3f(GetRValuef(boundaryColor), GetGValuef(boundaryColor), GetBValuef(boundaryColor));

    for (int i = 0; i < NUM_PTS; i++)
    {
        x = RADIUS * cos(theta); y = RADIUS * sin(theta);
        glVertex2f(x, y);
        theta += theta_delta;
        theta = (theta >= 2.0 * PI) ? theta - 2.0 * PI : theta;
        theta = (theta <= -2.0 * PI) ? theta + 2.0 * PI : theta;
    }

    if (theta <= endAngle)
    {
        x = RADIUS * cos(endAngle); y = RADIUS * sin(endAngle);
        glVertex2f(x, y);
    }
    glEnd();
}

auto DrawShapes::InterpolateColor(double val, double max_val/*Corresponding to col1*/, double min_val/*Corresponding to col2*/, COLORREF col1, COLORREF col2)
{

    auto r1 = GetRValue(col1);
    auto g1 = GetGValue(col1);
    auto b1 = GetBValue(col1);
    auto a1 = GetAValue(col1);

    auto r2 = GetRValue(col2);
    auto g2 = GetGValue(col2);
    auto b2 = GetBValue(col2);
    auto a2 = GetAValue(col2);

    auto w1 = std::fabs(max_val - val);
    auto w2 = std::fabs(val - min_val);

    decltype(r1) r3 = (decltype(r1))((w2 * r1 + w1 * r2) / (w1 + w2));
    decltype(g1) g3 = (decltype(g1))((w2 * g1 + w1 * g2) / (w1 + w2));
    decltype(b1) b3 = (decltype(b1))((w2 * b1 + w1 * b2) / (w1 + w2));
    decltype(a1) a3 = (decltype(a1))((w2 * a1 + w1 * a2) / (w1 + w2));

    return RGBA(r3, g3, b3, a3);
}

void DrawShapes::DrawHollowCircleTriangleStrip(double innerRadius, double outerRadius, double startAngle, double endAngle, COLORREF topColor, COLORREF bottomColor, UINT NUM_PTS)
{
    double x = 0, y = 0;
    static double PI = 4 * atan(1);
    const double theta_delta = std::fabs(endAngle - startAngle) / NUM_PTS;

    double max_y = outerRadius * sin(PI / 2.0);
    double min_y = outerRadius * sin(3.0 * (PI / 2.0));

    auto yGradColor = [topColor, bottomColor, max_y, min_y](double y) -> auto
    {
        return InterpolateColor(y, max_y, min_y, topColor, bottomColor);
    };

    double theta = startAngle;
    double r = 0, g = 0, b = 0, a;
    glBegin(GL_TRIANGLE_STRIP);
    for (int i = 0; i < NUM_PTS; i++)
    {
        x = innerRadius * cos(theta); y = innerRadius * sin(theta);
        auto col = yGradColor(y);
        r = GetRValuef(col); g = GetGValuef(col); b = GetBValuef(col); a = GetAValuef(col);
        glColor4f(r, g, b, a);
        glVertex2f(x, y);

        x = outerRadius * cos(theta); y = outerRadius * sin(theta);
        col = yGradColor(y);
        r = GetRValuef(col); g = GetGValuef(col); b = GetBValuef(col); a = GetAValuef(col);
        glColor4f(r, g, b, a);
        glVertex2f(x, y);

        theta += theta_delta;
    }

    {
        x = innerRadius * cos(theta); y = innerRadius * sin(theta);
        auto col = yGradColor(y);
        r = GetRValuef(col); g = GetGValuef(col); b = GetBValuef(col); a = GetAValuef(col);
        glColor4f(r, g, b, a);
        glVertex2f(x, y);

        x = outerRadius * cos(theta); y = outerRadius * sin(theta);
        col = yGradColor(y);
        r = GetRValuef(col); g = GetGValuef(col); b = GetBValuef(col); a = GetAValuef(col);
        glColor4f(r, g, b, a);
        glVertex2f(x, y);
    }
    glEnd();
}

void DrawShapes::DrawGraph(float factor)
{

    {
        float abscissa = 0;
        float ordinate = 0;

        for (float x = -1.0; x <= 1.0; x += factor)
        {
            if (x > (-factor / 2) && x < (factor / 2))
            {
                glLineWidth(3.0f);
                abscissa = x;
            }
            else
            {
                glLineWidth(1.0f);
            }

            glBegin(GL_LINES);

            if (x > (-factor / 2) && x < (factor / 2))
            {
                glColor3f(0.0f, 1.0f, 0.0f);
            }
            else
            {
                glColor3f(1.0f, 1.0f, 1.0f);
            }

            glVertex2f(x, -1);
            glVertex2f(x, 1);
            glEnd();
        }

        for (float y = -1.0; y <= 1.0; y += factor)
        {
            if (y > (-factor / 2) && y < (factor / 2))
            {
                glLineWidth(3.0f);
                ordinate = y;
            }
            else
            {
                glLineWidth(1.0f);
            }

            glBegin(GL_LINES);

            if (y > (-factor / 2) && y < (factor / 2))
            {
                glColor3f(1.0f, 0.0f, 0.0f);
            }
            else
            {
                glColor3f(1.0f, 1.0f, 1.0f);
            }

            glVertex2f(-1, y);
            glVertex2f(1, y);
            glEnd();
        }

        glLineWidth(3.0f);

        //Y-axis
        glBegin(GL_LINES);
        glColor3f(0.0f, 1.0f, 0.0f);
        glVertex2f(abscissa, -1);
        glVertex2f(abscissa, 1);
        glEnd();

        //X-axis
        glBegin(GL_LINES);
        glColor3f(1.0f, 0.0f, 0.0f);
        glVertex2f(-1, ordinate);
        glVertex2f(1, ordinate);
        glEnd();

        //origin
        glPointSize(3.0f);
        glBegin(GL_POINTS);
        glColor3f(1.0f, 1.0f, 0.0);
        glVertex2f(abscissa, ordinate);
        glEnd();
    }
}

util::ColorWheel::ColorWheel(VLONG numPartitions) : num_partitions(numPartitions), PI(4 * atan(1)), PI2(8 * atan(1))
{
    radius = max_cols_range / PI2;
}

std::tuple<ColorWheel::RED, ColorWheel::GREEN, ColorWheel::BLUE> ColorWheel::MapIndexToColor(VLONG index)//index starting at 0
{
    double theta = ((PI2) / num_partitions) * index;

    if (theta >= PI2)
    {
        theta = 0;
    }

    BLUE b = 0;
    GREEN g = 0;
    RED r = 0;

    if (theta >= 0 && theta < PI2 / 3)//RED-GREEN quadrant
    {
        r = radius * cos(theta);
        g = radius * sin(theta) * cos(PI / 6);
    }
    else if (theta < 2 * (PI2 / 3))//GREEN-BLUE quadrant
    {
        double phi = theta - PI2 / 3;
        double alpha = 2 * (PI2 / 3) - theta;

        g = radius * cos(phi);
        b = radius * cos(alpha);
    }
    else //BLUE-RED quadrant
    {
        double phi = PI2 - theta;
        r = radius * cos(phi);
        b = radius * sin(phi) * cos(PI / 6);
    }

    VLONG color = min_cols_range + (((double)(max_cols_range - min_cols_range)) / num_partitions) * index;

    return { r,g,b };
}

void util::TextHelper::Alphabet::Draw(AnimationObjectState& state)
{
    GLint mMode;
    glGetIntegerv(GL_MATRIX_MODE, &mMode);

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();

    glTranslatef(m_position.x, m_position.y, m_position.z);

    switch (m_char)
    {
    case L'A':
        DrawA(); break;
    case L'D':
        DrawD(); break;
    case L'F':
        DrawF(); break;
    case L'I':
        DrawI(); break;
    case L'N':
        DrawN(); break;
#if SUPPORT_UNICODE_LIB
    case L'Λ':
        DrawΛ(); break;
    case L'א':
        Drawא(); break;
    case L'ב':
        Drawב(state); break;
    case L'ג':
        Drawג(state); break;
    case L'ד':
        Drawד(state); break;
    case L'ה':
        Drawה(state); break;
    case L'ו':
        Drawו(state); break;
#endif
    default:
        THROW_NOT_IMPLEMENTED;
        break;
    }
    glPopMatrix();
    glMatrixMode(mMode);
}

void util::TextHelper::Alphabet::SetFontHeight(double height)
{
    m_cellSize = { m_monospaceRatio * height, height };
}

void util::TextHelper::Alphabet::DrawA()
{
    throw std::runtime_error("Not implemented");
}

void util::TextHelper::Alphabet::DrawF()
{
    glTranslatef(0, -m_cellSize.cy, 0);
    glBegin(GL_QUADS);
    //left quad
    glColor4f(GetRValuef(m_bottom), GetGValuef(m_bottom), GetBValuef(m_bottom), BYTE2FLOAT(m_alpha) * GetAValuef(m_bottom));
    glVertex2f(0, 0);
    glVertex2f(0.2487 * m_cellSize.cx, 0);
    glColor4f(GetRValuef(m_top), GetGValuef(m_top), GetBValuef(m_top), BYTE2FLOAT(m_alpha) * GetAValuef(m_top));
    glVertex2f(0.2487 * m_cellSize.cx, 1 * m_cellSize.cy);
    glVertex2f(0, 1 * m_cellSize.cy);

    //right top quad
    glColor4f(GetRValuef(m_top), GetGValuef(m_top), GetBValuef(m_top), BYTE2FLOAT(m_alpha) * GetAValuef(m_top));
    glVertex2f(0.2487 * m_cellSize.cx, 1 * m_cellSize.cy);
    auto yPos = 0.8840 * m_cellSize.cy;
    auto rgba = DrawShapes::InterpolateColor(yPos, m_cellSize.cy, 0, m_top, m_bottom);
    glColor4f(GetRValuef(rgba), GetGValuef(rgba), GetBValuef(rgba), BYTE2FLOAT(m_alpha) * GetAValuef(rgba));
    glVertex2f(0.2487 * m_cellSize.cx, yPos);
    glVertex2f(1 * m_cellSize.cx, yPos);
    glColor4f(GetRValuef(m_top), GetGValuef(m_top), GetBValuef(m_top), BYTE2FLOAT(m_alpha) * GetAValuef(m_top));
    glVertex2f(1 * m_cellSize.cx, 1 * m_cellSize.cy);

    //right bottom quad
    yPos = 0.5594 * m_cellSize.cy;
    rgba = DrawShapes::InterpolateColor(yPos, m_cellSize.cy, 0, m_top, m_bottom);
    glColor4f(GetRValuef(rgba), GetGValuef(rgba), GetBValuef(rgba), BYTE2FLOAT(m_alpha) * GetAValuef(rgba));
    glVertex2f(0.95989 * m_cellSize.cx, yPos);
    glVertex2f(0.2487 * m_cellSize.cx, yPos);
    yPos = 0.4463 * m_cellSize.cy;
    rgba = DrawShapes::InterpolateColor(yPos, m_cellSize.cy, 0, m_top, m_bottom);
    glColor4f(GetRValuef(rgba), GetGValuef(rgba), GetBValuef(rgba), BYTE2FLOAT(m_alpha) * GetAValuef(rgba));
    glVertex2f(0.2487 * m_cellSize.cx, yPos);
    glVertex2f(0.95989 * m_cellSize.cx, yPos);

    glEnd();
}

void util::TextHelper::Alphabet::DrawD()
{
    static double PI = 4 * atan(1);

    SolidFillCellWithBackground();

    //DataTypes::POINTd c;//centre of circle
    //c.x = (1 - 0.6948)*m_cellSize.cx;
    //c.y = - 0.5017*m_cellSize.cy;

    DataTypes::POINTd p1, p2, p3, p4;
    p1 = {};
    p2.x = 0;
    p2.y = -m_cellSize.cy;
    //p3.x = 0.46*m_cellSize.cx;
    p3.x = 0.311 * m_cellSize.cx;
    p3.y = p2.y;
    p4.x = p3.x;
    p4.y = 0;
    DataTypes::POINTd c = { p3.x, (p3.y + p4.y) / 2.0 };

    double innerRadius = 0.5352 * m_cellSize.cx;
    double outerRadius = 0.6948 * m_cellSize.cx;

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glTranslatef(c.x, c.y, 0);
    COLORREF top = m_top, bottom = m_bottom;

    try
    {
        DrawShapes::DrawHollowCircleTriangleStrip(innerRadius, outerRadius, 1.5 * PI, 0.5 * PI, RBG2RGBA(top, m_alpha), RBG2RGBA(bottom, m_alpha));
    }
    catch (std::runtime_error ex)
    {
        auto r = ex.what();
        LOG(LogLevel::Debug, r);
    }

    glPopMatrix();

    glBegin(GL_QUADS);
    glColor4f(GetRValuef(m_top), GetGValuef(m_top), GetBValuef(m_top), BYTE2FLOAT(m_alpha) * GetAValuef(m_top));
    glVertex2f(p1.x, p1.y);

    glColor4f(GetRValuef(m_bottom), GetGValuef(m_bottom), GetBValuef(m_bottom), BYTE2FLOAT(m_alpha) * GetAValuef(m_bottom));
    glVertex2f(p2.x, p2.y);
    glVertex2f(p3.x, p3.y);

    glColor4f(GetRValuef(m_top), GetGValuef(m_top), GetBValuef(m_top), BYTE2FLOAT(m_alpha) * GetAValuef(m_top));
    glVertex2f(p4.x, p4.y);

    glColor4f(GetRValuef(m_backGround), GetGValuef(m_backGround), GetBValuef(m_backGround), BYTE2FLOAT(m_alpha) * GetAValuef(m_backGround));

    p1.x = 0.1878 * m_cellSize.cx;
    p1.y = -0.1152 * m_cellSize.cy;
    p2.x = p1.x;
    p2.y = -m_cellSize.cy - p1.y;
    //p3.x = 0.46*m_cellSize.cx;
    p3.x = 0.311 * m_cellSize.cx;
    p3.y = p2.y;
    p4.x = p3.x;
    p4.y = p1.y;

    glVertex2f(p1.x, p1.y);
    glVertex2f(p2.x, p2.y);
    glVertex2f(p3.x, p3.y);
    glVertex2f(p4.x, p4.y);
    glEnd();
}

void util::TextHelper::Alphabet::DrawI()
{

    auto xDelta = m_cellSize.cx / 3.0;
    auto yDelta = m_cellSize.cy / 4.0;
    double x = 0, y = 0, z = 0;

    GradientFillCellH();

    glBegin(GL_QUADS);

    //stencil it without using stencil buffers
    x = y = z = 0;
    glColor3f(GetRValuef(m_backGround), GetGValuef(m_backGround), GetBValuef(m_backGround));

    //left flank
    y -= yDelta;
    glVertex3f(x, y, z);
    y -= 2 * yDelta;
    glVertex3f(x, y, z);
    x += xDelta;
    glVertex3f(x, y, z);
    y += 2 * yDelta;
    glVertex3f(x, y, z);

    //right flank
    x = y = z = 0;
    x += 2 * xDelta;
    y -= yDelta;
    glVertex3f(x, y, z);
    y -= 2 * yDelta;
    glVertex3f(x, y, z);
    x += xDelta;
    glVertex3f(x, y, z);
    y += 2 * yDelta;
    glVertex3f(x, y, z);
    glEnd();
}

void util::TextHelper::Alphabet::DrawN()
{
    GradientFillCellH();

    DataTypes::POINTd p1, p2, p3;
    //stencil it without using stencil buffers
    //right triangle
    p1.x = 0.31818 * m_cellSize.cx;
    p1.y = 0;

    p2.x = (1 - 0.23776) * m_cellSize.cx;
    p2.y = -0.7132 * m_cellSize.cy;

    p3.x = p2.x;
    p3.y = 0;

    DrawShapes::DrawTriangleSolid(p1, p2, p3, m_backGround);

    //left triangle
    p1.x = 0.23776 * m_cellSize.cx;
    p1.y = -m_cellSize.cy;

    p2.x = (1 - 0.31818) * m_cellSize.cx;
    p2.y = p1.y;

    p3.x = p1.x;
    p3.y = -(1 - 0.7132) * m_cellSize.cy;
    DrawShapes::DrawTriangleSolid(p1, p2, p3, m_backGround);
}

#if SUPPORT_UNICODE_LIB
void util::TextHelper::Alphabet::DrawΛ()
{
    GradientFillCellH();

    DataTypes::POINTd p1, p2, p3;
    //stencil it without using stencil buffers
    //Left triangle
    p1 = { 0, 0 };

    p2.x = 0;
    p2.y = -m_cellSize.cy;

    p3.x = 0.358 * m_cellSize.cx;
    p3.y = 0;
    DrawShapes::DrawTriangleSolid(p1, p2, p3, m_backGround);

    //right triangle
    p1.x = (1 - 0.358) * m_cellSize.cx;
    p1.y = 0;

    p2.x = m_cellSize.cx;
    p2.y = -m_cellSize.cy;

    p3.x = m_cellSize.cx;
    p3.y = 0;
    DrawShapes::DrawTriangleSolid(p1, p2, p3, m_backGround);

    //bottom triangle
    p1.x = 0.2367 * m_cellSize.cx;
    p1.y = -m_cellSize.cy;

    p2.x = m_cellSize.cx - p1.x;
    p2.y = p1.y;

    p3.x = 0.49408 * m_cellSize.cx;
    p3.y = -0.2426 * m_cellSize.cy;
    DrawShapes::DrawTriangleSolid(p1, p2, p3, m_backGround);
}

void util::TextHelper::Alphabet::Drawא()
{
    GradientFillCellH();

    DataTypes::POINTd p1, p2, p3;
    //stencil it without using stencil buffer

#pragma region bottom triangle
    p1.x = 0.2367 * m_cellSize.cx;
    p1.y = -m_cellSize.cy;

    p2.x = m_cellSize.cx - p1.x;
    p2.y = p1.y;

    p3.x = 0.49408 * m_cellSize.cx;
    p3.y = -0.2426 * m_cellSize.cy;
    DrawShapes::DrawTriangleSolid(p1, p2, p3, m_backGround);
#pragma endregion

#pragma region draw tricolor lines
    glLineWidth(10);
    glBegin(GL_LINES);
    glColor3f(GetRValuef(ColorWheel::COLORS::INDIA_SAFRON), GetGValuef(ColorWheel::COLORS::INDIA_SAFRON), GetBValuef(ColorWheel::COLORS::INDIA_SAFRON));
    glVertex3f(0, -m_cellSize.cy * 0.4, 0);
    glVertex3f(m_cellSize.cx, -m_cellSize.cy * 0.4, 0);

    glColor3f(1, 1, 1);
    glVertex3f(0, -m_cellSize.cy * 0.6, 0);
    glVertex3f(m_cellSize.cx, -m_cellSize.cy * 0.6, 0);

    glColor3f(GetRValuef(ColorWheel::COLORS::INDIA_GREEN), GetGValuef(ColorWheel::COLORS::INDIA_GREEN), GetBValuef(ColorWheel::COLORS::INDIA_GREEN));
    glVertex3f(0, -m_cellSize.cy * 0.8, 0);
    glVertex3f(m_cellSize.cx, -m_cellSize.cy * 0.8, 0);
    glEnd();
#pragma endregion

#pragma region Left triangle
    p1 = { 0, 0 };

    p2.x = 0;
    p2.y = -m_cellSize.cy;

    p3.x = 0.358 * m_cellSize.cx;
    p3.y = 0;
    DrawShapes::DrawTriangleSolid(p1, p2, p3, m_backGround);
#pragma endregion

#pragma region right triangle
    p1.x = (1 - 0.358) * m_cellSize.cx;
    p1.y = 0;

    p2.x = m_cellSize.cx;
    p2.y = -m_cellSize.cy;

    p3.x = m_cellSize.cx;
    p3.y = 0;
    DrawShapes::DrawTriangleSolid(p1, p2, p3, m_backGround);
#pragma endregion

}

void TextHelper::Alphabet::Drawב(AnimationObjectState& state)
{
    static UINT currentIteration = 0;

    static auto startTime = 8.1399999999999988; //hardcoded - taken from Dynamic India.cpp //TODO:remove hard coded stuff
    static auto endTime = 8.1399999999999988 + 2; //hardcoded - taken from Dynamic India.cpp //TODO:remove hard coded stuff
    UINT numIterations = (endTime - startTime) / state.m_stepSize;//iterations to reach full opacity

    Alphabet alpha1 = *this;//so that font size, colors, etc. are automatically copied
    alpha1.SetChar(L'D');
    AnimationObjectState s = {};
    alpha1.Draw(s);

    if (currentIteration < numIterations)
    {
        BYTE alpha = 255 - (255.0 / (numIterations - 1.0)) * (currentIteration);//not alphabet but alpha chanel
        Alphabet alpha2 = *this;//so that font size, colors, etc. are automatically copied
        alpha2.SetChar(L'D');
        alpha2.SetTopColor(m_backGround);
        alpha2.SetBottomColor(m_backGround);
        alpha2.SetAlpha(alpha);
        AnimationObjectState s = {};
        alpha2.Draw(s);
        currentIteration++;
    }
}

void util::TextHelper::Alphabet::Drawג(AnimationObjectState& state)
{
    auto xDelta = m_cellSize.cx / 3.0;
    auto yDelta = m_cellSize.cy / 4.0;
    double x = 0, y = 0, z = 0;

    glBegin(GL_QUADS);
    x = y = z = 0;

    glVertex3f(x, y, z);
    y -= yDelta;
    glVertex3f(x, y, z);
    x += 3 * xDelta;
    glVertex3f(x, y, z);
    y += yDelta;
    glVertex3f(x, y, z);

    x = y = z = 0;

    y -= yDelta;
    x += xDelta;
    glVertex3f(x, y, z);
    y -= 2 * yDelta;
    glVertex3f(x, y, z);
    x += xDelta;
    glVertex3f(x, y, z);
    y += 2 * yDelta;
    glVertex3f(x, y, z);

    x = y = z = 0;

    y -= 3 * yDelta;
    glVertex3f(x, y, z);
    y -= yDelta;
    glVertex3f(x, y, z);
    x += 3 * xDelta;
    glVertex3f(x, y, z);
    y += yDelta;
    glVertex3f(x, y, z);

    glEnd();
}

void util::TextHelper::Alphabet::Drawד(AnimationObjectState& state)
{
    double x = 0, y = 0, z = 0;

    glBegin(GL_QUADS);
    x = y = z = 0;

    //quad1
    glVertex3f(x, y, z);
    y = -m_cellSize.cy;
    glVertex3f(x, y, z);
    x += 0.23776 * m_cellSize.cx;
    glVertex3f(x, y, z);
    y += m_cellSize.cy;
    glVertex3f(x, y, z);

    //quad2
    glVertex3f(x, y, z);
    y = -(1 - 0.7132) * m_cellSize.cy;
    glVertex3f(x, y, z);
    y = -m_cellSize.cy;
    x = (1 - 0.23776) * m_cellSize.cx;
    glVertex3f(x, y, z);
    y = -0.7132 * m_cellSize.cy;
    glVertex3f(x, y, z);

    //quad3
    y = -m_cellSize.cy;
    x = (1 - 0.23776) * m_cellSize.cx;
    glVertex3f(x, y, z);
    x = m_cellSize.cx;
    glVertex3f(x, y, z);
    y = 0;
    glVertex3f(x, y, z);
    x = (1 - 0.23776) * m_cellSize.cx;
    glVertex3f(x, y, z);
    glEnd();

}

void util::TextHelper::Alphabet::Drawה(AnimationObjectState& state)
{
    static double PI = 4 * atan(1);

    DataTypes::POINTd p1, p2, p3, p4;
    p1 = {};
    p2.x = 0;
    p2.y = -m_cellSize.cy;
    //p3.x = 0.46*m_cellSize.cx;
    p3.x = 0.1878 * m_cellSize.cx;
    p3.y = p2.y;
    p4.x = p3.x;
    p4.y = 0;
    DataTypes::POINTd c = { p3.x, (p3.y + p4.y) / 2.0 };

    double innerRadius = 0.5352 * m_cellSize.cx;
    //double innerRadius = 0.5052*m_cellSize.cx;
    double outerRadius = 0.6948 * m_cellSize.cx;

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glTranslatef(c.x, c.y, 0);
    COLORREF top = m_top, bottom = m_bottom;
    DrawShapes::DrawHollowCircleTriangleStrip(innerRadius, outerRadius, 1.5 * PI, 0.5 * PI, RBG2RGBA(top, m_alpha), RBG2RGBA(bottom, m_alpha));

    glPopMatrix();

    glBegin(GL_QUADS);
    glVertex2f(p1.x, p1.y);

    glVertex2f(p2.x, p2.y);
    glVertex2f(p3.x, p3.y);

    glVertex2f(p4.x, p4.y);

    glEnd();

}

void util::TextHelper::Alphabet::Drawו(AnimationObjectState& state)
{
    double x = 0, y = 0, z = 0;

    glBegin(GL_POLYGON);
    //left polygon
    y = -m_cellSize.cy;
    glVertex3f(x, y, z);
    x = 0.2367 * m_cellSize.cx;
    glVertex3f(x, y, z);
    x = 0.49408 * m_cellSize.cx;
    y = -0.2426 * m_cellSize.cy;
    glVertex3f(x, y, z);
    y = 0;
    glVertex3f(x, y, z);
    x = 0.358 * m_cellSize.cx;
    glVertex3f(x, y, z);
    glEnd();

    glBegin(GL_POLYGON);
    //right polygon
    x = (1 - 0.358) * m_cellSize.cx;
    glVertex3f(x, y, z);
    x = 0.358 * m_cellSize.cx;
    glVertex3f(x, y, z);
    x = 0.49408 * m_cellSize.cx;
    y = -0.2426 * m_cellSize.cy;
    glVertex3f(x, y, z);
    y = -m_cellSize.cy;
    x = m_cellSize.cx - 0.2367 * m_cellSize.cx;
    glVertex3f(x, y, z);
    x = m_cellSize.cx;
    glVertex3f(x, y, z);
    glEnd();
}

#endif
void util::TextHelper::Alphabet::SolidFillCellWithBackground()
{
    glBegin(GL_QUADS);
    glColor4f(GetRValuef(m_backGround), GetGValuef(m_backGround), GetBValuef(m_backGround), BYTE2FLOAT(m_alpha) * GetAValuef(m_backGround));
    glVertex2f(0, 0);

    glVertex2f(0, -m_cellSize.cy);
    glVertex2f(m_cellSize.cx, -m_cellSize.cy);

    glVertex2f(m_cellSize.cx, 0);
    glEnd();
}
void util::TextHelper::Alphabet::GradientFillCellH()
{
    glBegin(GL_QUADS);
    glColor4f(GetRValuef(m_top), GetGValuef(m_top), GetBValuef(m_top), BYTE2FLOAT(m_alpha));
    glVertex2f(0, 0);

    glColor4f(GetRValuef(m_bottom), GetGValuef(m_bottom), GetBValuef(m_bottom), BYTE2FLOAT(m_alpha));
    glVertex2f(0, -m_cellSize.cy);
    glVertex2f(m_cellSize.cx, -m_cellSize.cy);

    glColor4f(GetRValuef(m_top), GetGValuef(m_top), GetBValuef(m_top), BYTE2FLOAT(m_alpha));
    glVertex2f(m_cellSize.cx, 0);
    glEnd();
}

void util::TextHelper::Alphabet::AdvanceTime()
{
    THROW_NOT_IMPLEMENTED
}

void TextHelper::SetFontHeight(double height)
{
    m_fontHeight = height;
}

void TextHelper::WriteText(std::wstring line, DataTypes::POINT3Dd relativePos)
{
    m_str = line;

    glPushMatrix();
    glTranslatef(relativePos.x, relativePos.y, relativePos.z);
    for (wchar_t& ch : line)
    {
        Alphabet alpha(ch, m_top, m_bottom, m_stringAlpha);
        alpha.SetFontHeight(m_fontHeight);
        alpha.SetBackGroundColor(m_background);
        AnimationObjectState s = {};
        alpha.Draw(s);
        glTranslatef(m_characterSpacing + alpha.GetCellSize().cx, 0, 0);
    }
    glPopMatrix();
}

double TextHelper::GetOnScreenSize(std::wstring line)
{
    double totSize = 0;
    for (wchar_t& ch : line)
    {
        Alphabet alpha(ch, m_top, m_bottom, m_stringAlpha);
        alpha.SetFontHeight(m_fontHeight);
        totSize += m_characterSpacing + alpha.GetCellSize().cx;
    }
    return totSize;
}

DataTypes::Size2Df TextHelper::GetMaxHeightCellSize(std::wstring line)
{
    double height = -1;
    DataTypes::Size2Df cell = {};
    for (wchar_t& ch : line)
    {
        Alphabet alpha(ch, m_top, m_bottom, m_stringAlpha);
        alpha.SetFontHeight(m_fontHeight);

        if (height < alpha.GetCellSize().cy)
        {
            cell = alpha.GetCellSize();
            height = cell.cy;
        }
    }

    return cell;
}

/*GL_QUADS is depricated
* Convert vertices which represent a Quad
*/
std::vector<GLfloat> util::QuadToTriangle(std::vector<GLfloat> QuadVertices, int dimensions)
{
    //Each quad will have 4 vertices
    //Each vertex will have 3 values - x, y, and z
    if (QuadVertices.size() % (dimensions * 4) != 0)
    {
        THROW_RUNTIME_ERROR("QuadVertices - incorrect size");
    }

    std::vector<GLfloat> triangleVertices;
    for (int i = 0; i < QuadVertices.size(); i += (dimensions * 4))
    {//process 1 quad at a time

        //insert 1st vertex
        int v = 0;
        triangleVertices.insert(triangleVertices.end(), std::begin(QuadVertices) + i + v * dimensions, std::begin(QuadVertices) + i + (v + 1)* dimensions);

        //insert 2nd vertex
        v = 1;
        triangleVertices.insert(triangleVertices.end(), std::begin(QuadVertices) + i + v * dimensions, std::begin(QuadVertices) + i + (v + 1) * dimensions);

        //insert 3rd vertex twice
        v = 2;
        triangleVertices.insert(triangleVertices.end(), std::begin(QuadVertices) + i + v * dimensions, std::begin(QuadVertices) + i + (v + 1) * dimensions);
        triangleVertices.insert(triangleVertices.end(), std::begin(QuadVertices) + i + v * dimensions, std::begin(QuadVertices) + i + (v + 1) * dimensions);

        //insert 4th vertex
        v = 3;
        triangleVertices.insert(triangleVertices.end(), std::begin(QuadVertices) + i + v * dimensions, std::begin(QuadVertices) + i + (v + 1) * dimensions);

        //insert 1st vertex again
        v = 0;
        triangleVertices.insert(triangleVertices.end(), std::begin(QuadVertices) + i + v * dimensions, std::begin(QuadVertices) + i + (v + 1) * dimensions);

    }

    return triangleVertices;
}
