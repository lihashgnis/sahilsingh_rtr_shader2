﻿#pragma once
#ifdef _WIN32
#include<Windows.h>
#endif
#include "Common.h"
#include "LinuxCompat.h"
#include <functional>
#include <exception>
#include <stdexcept>
#include <memory>
#include <cmath>
#include <string>
#include <chrono>
#include <optional>
#include <utility>
#include "ProgrammablePipeline.h"

#define STRICT

#define USING_PRECOMPILED_HEADERS
#undef USING_PRECOMPILED_HEADERS//renable precompiled header only when it works fine for both Linux, and Windows

#ifndef SUPPORT_UNICODE_LIB //otherwise cuda files don't compile correctly even if the file is saved as unicode
#define SUPPORT_UNICODE_LIB 1 //ON by defaul unless explicitly turned OFF (for eg. in .cu files)
#endif // ! SUPPORT_UNICODE_LIB //otherwise cuda files don't compile correctly even if the file is saved as unicode

#ifndef NDEBUG
#define THROW_NOT_IMPLEMENTED {throw std::runtime_error("Not implemented");}
#define THROW_WINDOWPROP_PARAMETER_MISSING {throw std::runtime_error("WindowProps parameter missing");}
#define THROW_VERTEXT_OR_FRAGMENT_SHADER_MISSING {throw std::runtime_error("Both Vertex and Fragment Shaders should be specified");}
#define THROW_USING_PPCODE_FROM_FFP_CONTEXT {throw std::runtime_error("Using programmable pileline code when not in PP context - perhaps PP flag wasn't specified in init");}
#define THROW_MALLOC_ERROR {throw std::runtime_error("Error allocating memory");}
#define THROW_SHADER_COMPILE_FAILED {throw std::runtime_error("Shader Compile Failed");}
#define THROW_SHADER_LINK_FAILED {throw std::runtime_error("Shader Link Failed");}
#define THROW_RUNTIME_ERROR(msg) {throw std::runtime_error(msg);}
#define THROW_UNEXPECTED_PARAMETER THROW_RUNTIME_ERROR("Unexpected parameter");
#else
#define THROW_NOT_IMPLEMENTED ;
#define THROW_WINDOWPROP_PARAMETER_MISSING ;
#define THROW_VERTEXT_OR_FRAGMENT_SHADER_MISSING ;
#define THROW_USING_PPCODE_FROM_FFP_CONTEXT ;
#define THROW_MALLOC_ERROR ;
#define THROW_SHADER_COMPILE_FAILED ;
#define THROW_RUNTIME_ERROR(msg) ;
#endif

/* use XMacros
 * https://en.wikipedia.org/wiki/OpenGL_Shading_Language#Versions
 * OpenGL 2.0 writen as 20
 * OpenGL 2.0 supported 110 shader version
 * used in ShaderCodesContainer::OGL2GLSLVersion(), and ShaderCodesContainer::GLSL2OGLVersion()
 */
#define OGL_GLSHVERSION_MAP \
    X(20, 110) \
    X(21, 120) \
    X(30, 130) \
    X(31, 140) \
    X(32, 150) \
    X(33, 330) \
    X(40, 400) \
    X(41, 410) \
    X(42, 420) \
    X(43, 430) \
    X(44, 440) \
    X(45, 450) \
    X(46, 460)

//ASSERT x : expresion to assert. y : error message, in case assertion fails
#ifndef NDEBUG
#define ASSERT(x,y) \
if(false==(x)) {throw std::runtime_error(y);}
#else
#define ASSERT(x) ;
#endif

//A lot of times during debugging, run once is required
#define RUN_ONCE_START \
{\
static bool runOnceVariable = false;\
if(false == runOnceVariable) \
{


#define RUN_ONCE_END \
runOnceVariable = true;\
}\
}

#define PARENT_OBJECT TEXT("OglWindowParentObjectDataTypes::pointer")

//colorref is 4 bytes, RGB() macro uses only 3 of those bytes. Hack it to add alpha chanel
// a - alpha. we are really storing opacity. Opacity is being stored instead of alpha since RGB() stores 0 in those bits, we want full opaque at that point
//#define RGBA(r,g,b,a)  ((COLORREF)(((BYTE)(r)|((WORD)((BYTE)(g))<<8))|(((DWORD)(BYTE)(b))<<16))|(((DWORD)(BYTE)(255-a))<<24)))
#define RGBA(r,g,b,a)     ((COLORREF)(((BYTE)(r)|((WORD)((BYTE)(g))<<8))|(((DWORD)(BYTE)(b))<<16) | (((DWORD)(BYTE)(255-a))<<24)))
#define GetRValuef(x)       (GetRValue(x)/255.0f)
#define GetGValuef(x)       (GetGValue(x)/255.0f)
#define GetBValuef(x)       (GetBValue(x)/255.0f)
#define GetOValue(x)        (LOBYTE((x)>>24)) //Opacity
#define GetAValue(x)        (255 - GetOValue(x))//since opcaity is stored by RGBA, and not alpha
#define GetAValuef(x)       (float(GetAValue(x)/255.0))//since opcaity is stored by RGBA, and not alpha
#define RBG2RGBA(rgb, a)    (COLORREF)((DWORD)rgb | (((DWORD)(BYTE)(255-a))<<24) | [rgb](){if(GetOValue(rgb)!=0){throw std::runtime_error("rgb should not contain opacity value");}; return 0;}()) //TODO: this won't give correct result when rgb also has pre existing alpha value
#define BYTE2FLOAT(x)       (float(x/255.0))

//Init masks
#define INIT_MASK_ENABLE_DEPTH                                      0b1
#define INIT_MASK_ENABLE_STENCIL                                    0b10
#define INIT_MASK_ENABLE_TEXTURE_2D                                 0b100
#define INIT_MASK_ENABLE_PROGRAMMABLE_PIPELINE                      0b1000 //also means that fixed function related code will get disabled
#define INIT_XWINDOWS_DISABLE_ADVANCED_FB_CONFIG_INIT               0b10000 //Uses our custom XWindowsManualHighVisual() to init FBConfig, and not glXCreateContext(), etc. Enabled by default, hence if this mask is set, then glXCreateContext() will be used, and XWindowsManualHighVisual() won't be used. XWindowsManualHighVisual() by default.
#define INIT_MASK_DUMP_ATTRIBUTES                                   0b100000 //It is quite easy to create vertices in C++ using macros (see how cube and pyramid vertices are generated in 06_3DAnimationOfCubeAndPyramid_PP), in java, the macros are not there. If this option is enabled, the vertices are dumped to a file, so that they can then be copied to android projects java code
#define INIT_MASK_ENABLE_ARB_DEBUG                                  0b1000000//enable GL_ARB_debug_output this extension calls a callback on various openGL errors
#define INIT_MASK_DUMP_SHADER_CODE                                  0b10000000 //some programs dynamically generate shader code - eg. lighting programs, etc. This flag dumps the final generated code

namespace util {
    /*Animation object takes care of rotation, scaling, etc. of a scene object embedded in it.
    The object itself is unaware of these traslations. This struct will carry data to objects while drawing, in case the object
    wants to refer to it.
    */
    struct AnimationObjectState;

    namespace DataTypes
    {
        //'d' stands for double
        struct POINTd {
            double x, y;
        };

        typedef POINTd POINT2Dd;

        struct POINT3Dd {
            double x, y, z;
            bool operator==(POINT3Dd& that)
            {
                if (std::fabs(x - that.x) > std::numeric_limits<double>::epsilon())
                    return false;
                if (std::fabs(y - that.y) > std::numeric_limits<double>::epsilon())
                    return false;
                if (std::fabs(z - that.z) > std::numeric_limits<double>::epsilon())
                    return false;
                return true;
            }

            bool operator!=(POINT3Dd& that)
            {
                return !(*this == that);
            }
        };

        struct POINT3Df {
            float x, y, z;
            bool operator==(POINT3Df& that)
            {
                if (std::fabs(x - that.x) > std::numeric_limits<float>::epsilon())
                    return false;
                if (std::fabs(y - that.y) > std::numeric_limits<float>::epsilon())
                    return false;
                if (std::fabs(z - that.z) > std::numeric_limits<float>::epsilon())
                    return false;
                return true;
            }

            bool operator!=(POINT3Df& that)
            {
                return !(*this == that);
            }
        };

        struct Size2Dd {
            double cx, cy;
        };

        struct Size2Df {
            double cx, cy;
        };

        typedef POINTd Centre;
        typedef double Radius;

        //An ID to identify objects being drawn - unique per shader program - there can be multiple shader programs per application
        typedef int ObjectID;

        //There can be multiple shader programs in an application, each identified by an ID
        typedef int ProgramID;
    }

    class ShaderCodesContainer {
        std::optional<std::string> m_vertexShaderCode;
        std::optional<std::string> m_fragmentShaderCode;//IMP : When adding other shaders add checks as have been added for v, and frag - search for string find statements
        std::optional <GLint> m_shaderProgram;
        util::ProgrammablePipeline::Lights m_Lights;

        /*
        * @brief will be used to add version string to shader code
        * All shaders of a shader program, and all shaders program in an application should have same version.
        * Thus it is static;
        * eg. For 4.20 set this to 420
        * default is 140 - the version supported by my ubuntu installation in hyperV (OGL 3.1 supported GLSL 140)
        * default is set in OglWindow::InitAndShowWindow().
        * see https://en.wikipedia.org/wiki/OpenGL_Shading_Language#Versions
        * for a relation between opengl and GLSL version
        * specifying profile eg. core was only supported OpenGL 3.2 onwards, which had glsl 150.
        * Hence, "#version 150 core" is fine, but "#version 140 core" IS NOT, instead just use "#version 150".
        * DO NOT RESET IN ShaderCodesContainer::Reset(), since handled by OglWindow::InitAndShowWindow().
        */
        static int m_shaderVersion;
    public:
        /*
        * @brief We should not allow shader version to change once it has been explicitly set by
        * user. However till the time default shader version is being used (set in Reset() function)
        * a user can change it.
        * A user must set it before adding any shader code, since version is added to code before compilation.
        * DO NOT RESET IN ShaderCodesContainer::Reset(), since handled by OglWindow::InitAndShowWindow().
        */
        static bool m_bAllowShaderVersionToBeSet;

#pragma region LIGHTS FUNCTIONS
        /*
        * Programmable Pipeline support for lights is required. Global ON/OFF switch for all lights (per-shader). If this off, all lights would be OFF, individual ON/OFF state of each ligh wont matter
        */
        void SetLightsSupported(bool bAreLightsSupported, unsigned int numLightsToUse = 0);
        bool GetLightsSupported();
        util::ProgrammablePipeline::Lights& GetShaderLights();
        void SetShaderContainerPtr(util::ShaderCodesContainer*);
        void ConfigureLight(const unsigned int lightNum, const util::ProgrammablePipeline::LightProps& props);
        bool EnableLight(const unsigned int lightNum, const bool bEnable = true);
        util::ProgrammablePipeline::ObjectLightProps GetObjectLightProperties(DataTypes::ObjectID objectID);
        void FlushLight();
        bool GlobalLightSwich(bool bTurnOn);
#pragma endregion

        //Each vertex attribute has to be associated with a user given integer - this integer can be used to refer to the attribute
        enum class VertextAttributesEnum : GLuint {
            AMC_ATTRIBUTE_POSITION = 0,
            AMC_ATTRIBUTE_COLOR,
            AMC_ATTRIBUTE_NORMAL,
            AMC_ATTRIBUTE_TEXTCOORD0,
            AMC_ATTRIBUTE_TEXTCOORD1,
            AMC_ATTRIBUTE_INDICES   //For attributes like indices in indexed drawing mode
        };

        /*
        * @brief: glBindBuffer() can bind a buffer object to several targets - GL_ARRAY_BUFFER, GL_ELEMENT_ARRAY_BUFFER, etc.
        * This enum is used to specify the bind target for each attribute.
        * A separate enum is used rather than directly using the GLenum values,
        * since each bind target requires separate logic, hence will be adding only those
        * targets for which the code to handle them is present.
        * numerically each enum value is equal to corresponding GLenum value, so that
        * it is easy to type cast, and use in glBindBuffer(), etc.
        */
        enum class AttributeBindTarget : GLenum
        {
            BT_ARRAY_BUFFER = GL_ARRAY_BUFFER,
            BT_ELEMENT_ARRAY_BUFFER = GL_ELEMENT_ARRAY_BUFFER
        };

        /*
        * There are several way of drawing an object - ordered draws, indexed draws, etc.
        * These correspond to glDrawArrays(), glDrawElements, etc.
        */
        enum class ObjectDrawType
        {
            DT_DrawArrays_Triangles,        //glDrawArrays(GL_TRIANGLES)
            DT_DrawArrays_TriangleFan,        //glDrawArrays(GL_TRIANGLE_FAN)
            DT_DrawElements_Triangles,      //glDrawElements(GL_TRIANGLES) - indexed drawing mode
        };

        struct VertexAttributeStruct {
            VertextAttributesEnum type;//The ID using which one can refer to this attribute
            std::vector<GLfloat> dataf;//If data is no longer float - also see size bytes parameter in glBufferData
            std::vector<GLuint> dataui;//for data which are not float - eg indices in indexed drawing mode
            std::string shaderVariableName;//Which shader variable does this attribute get bound to
            GLsizei stride;
            AttributeBindTarget bindTarget; //see AttributeBindTarget doc;

            /*
            * components per attribute eg. for position each position attribute will have 3 components - x,y, and z.
            * for RGBA color color attribute of each vertex will have 4 components
            */
            GLsizei components;
            GLuint vbo;//associated vbo
            //offset - add later

            /*
            * @brief : Number of vertices for which data is present in this attribute.
            * to be used as last parameter (count) of glDrawArrays()
            * ASSERT(numVertices == components.dataf.size())
            */
            GLsizei numVertices;
        };


        /*
        * @brief Each object to be draw in PP has a unique object ID.
        * all attributes of an object are identified by different 
        * buffers of a single vao. Thus each object will only have
        * a single vao, but multiple buffers with that vao, one 
        * buffer for each attribute - position, color, etc.
        */
        struct ObjectInfo
        {
            util::DataTypes::ObjectID objectID;
            GLuint vao;
            ObjectDrawType m_objectDrawType;
            util::ProgrammablePipeline::ObjectLightProps m_lightProps = {};
            bool m_bIsLightSupported = false; //if light is supported, ObjectLightProps must be given
            std::vector<VertexAttributeStruct> m_attribs;
        };

        ShaderCodesContainer();
        ~ShaderCodesContainer();

        std::optional<std::string> GetVertexShader();
        void SetVertexShaderCode(std::string code);
        std::optional<std::string> GetFragmentShader();
        void SetFragmentShaderCode(std::string code);
        std::optional<GLint> GetShaderProgram();
        void SetShaderProgram(GLint progObject);
        void ResetShaderProgram();

        /*
        * param[IN] bSkipSanityCheck : for arrays - arr[size], arr[0], arr[1], etc. will
        * not exist in code. MAKE SURE THAT IF THIS IS TRUE, then there are other methods
        * in place to ensure sanity for the skipped part.
        */
        void AddUniform(std::string shaderVariableName, bool bSkipSanityCheck=false);
        void SetAttribVbo(DataTypes::ObjectID objectID, VertextAttributesEnum type, GLuint vbo);
        void Reset();

        /*
        @param[IN] shaderVariableName - name of the variable as it is in shader
        */
        void AddAttribute(DataTypes::ObjectID objectID, VertextAttributesEnum attrType, std::string shaderVariableName, std::vector<GLfloat> dataf, GLsizei stride, GLsizei componets, AttributeBindTarget bindTarget, std::vector<GLuint> dataui = std::vector<GLuint>());
        VertexAttributeStruct GetAttribute(DataTypes::ObjectID objectID, VertextAttributesEnum attrType);

        std::vector<VertexAttributeStruct> GetAllAttribsForObject(DataTypes::ObjectID objectID);

        void SetObjectVao(DataTypes::ObjectID objectID, GLuint vao);
        GLuint GetObjectVao(DataTypes::ObjectID objectID);

        void CreateNewObject(DataTypes::ObjectID objectID, ShaderCodesContainer::ObjectDrawType objectDrawType);
        void CreateNewObject(DataTypes::ObjectID objectID, ShaderCodesContainer::ObjectDrawType objectDrawType, util::ProgrammablePipeline::ObjectLightProps objectLightProps);
        ShaderCodesContainer::ObjectDrawType GetObjectDrawType(DataTypes::ObjectID objectID);
        /*
        * @brief Get IDs of all objects we have info for
        */
        std::vector<DataTypes::ObjectID> GetAllObjectIDs();

        std::vector<std::string> GetAllUniformNames();

        void SetUniformLocation(std::string name, GLint location);

        GLint GetUniformLocation(std::string name);

        static void SetShaderVersion(int version);
        static int GetShaderVersion();

        static int OGL2GLSLVersion(int version);
        static int GLSL2OGLVersion(int version);
    private:
        //A uniform can be referenced using location in shader.
        //GLint - location of shader
        //string - name of variable inside shader, use this same string in cpu code to get location of the shader variable, which will be passed to OGL API
        std::map<std::string, GLint> m_uniformLoc;
        std::vector<std::string> m_uniformNames;

        /*
        * @brief see ObjectInfo doc
        */
        std::map<DataTypes::ObjectID, ObjectInfo> m_objectInfoMap;

        /*
        * private functions which has common code for 2 other overloads ofCreateNewObject()
        */
        void CreateNewObject(DataTypes::ObjectID objectID, ShaderCodesContainer::ObjectDrawType objectDrawType, bool bIsLightSupported,util::ProgrammablePipeline::ObjectLightProps objectLightProps);
    };

	class OglWindow
	{
	protected:
		virtual const char* GetMessageMap() = 0;
		virtual void Display() = 0;
		virtual void Update() = 0;
		virtual void Resize(int width, int height) = 0;
		int Initialize(UINT mask = 0);
#ifdef _WIN32
		int InitializeWindows(UINT mask = 0);
#endif
#ifdef __linux__
		int InitializeLinux(UINT mask = 0);

        /*
        * @brief : We will manually iterate through all frame buffer
        * configs, and select the best one for us.
        * glXChooseVisual() method is older method,
        * and less recommended of the two.
        * https://stackoverflow.com/a/51563500/981766
        * Disable calling this function, and use old
        * method instead by using 
        * INIT_XWINDOWS_DISABLE_ADVANCED_FB_CONFIG_INIT
        * mask bit
        */
        void XWindowsManualHighVisual();
#endif
		void Uninitialize();
#ifdef _WIN32
        void ToggleFullscreeenWindows(HWND hWnd);
        static LRESULT CALLBACK OglWndProcWindows(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam);
#endif
        static LRESULT OglWndProc(WindowProps& props, UINT iMsg, WPARAM wParam, LPARAM lParam);
#ifdef __linux__
        void ToggleFullscreeenLinux();
        void DestroyWindowLinux();
#endif
		virtual LRESULT WndProcPre(WindowProps&, UINT, WPARAM, LPARAM) = 0;//called before processing by base class
		virtual LRESULT WndProcPost(WindowProps&, UINT, WPARAM, LPARAM) = 0;
#ifdef _WIN32
        static void InitWndClassEx(WNDCLASSEX& wc, TCHAR* AppName, WNDPROC wp);
#endif
        static WindowProps CreateOglWindow(TCHAR* applicationName, UINT x, UINT y, UINT width, UINT height);

#ifdef _WIN32
		WNDCLASSEX m_wndClassEx;
		HDC m_Hdc;
		HGLRC m_Hrc;
#endif
        WindowProps m_WindowProps;
		bool m_bIsFullScreen, m_bActiveWindow;
        bool m_bIsPaused;
        bool m_bAllowOutOfFocusrender; //normally any rendering is done only when window is active (in focus)
#pragma region frame limitter
        bool m_bCapFramerate;//If true, the max rate at which application renders is capped - m_dwInterFrameTimeMilliSecond
        DWORD m_dwInterFrameTimeMilliSecond; //In milliseconds - used to limit max frame rate
        std::chrono::steady_clock::time_point m_dwPrevEpoch; //time in ms when last display call was invoked
#pragma endregion
        std::string m_wndName;
		UINT m_width, m_height;
		UINT m_posx, m_posy;
        float m_minX, m_maxX; //Minimum, and maximum clipping extents along X axis (-1,1) by default;
        float m_minY, m_maxY; //Minimum, and maximum clipping extents along X axis (-1,1) by default;
        float m_minZ, m_maxZ; //Minimum, and maximum clipping extents along X axis (-1,1) by default;
        void CalcExtents();
        UINT m_InitMask;//The mast which was used while init

        /*
         * OGL major, and minor versions.
         * eg. for 3.20
         * m_oglVersion_major will be 3
         * m_oglVersion_minor will be 2
         */
        int m_oglVersion_major, m_oglVersion_minor;

#pragma region shader codes
    private:
        //There can be multiple shader programs, each with its own shaders, attributes, uniforms, etc.
        std::map<util::DataTypes::ProgramID, ShaderCodesContainer> m_shaderCodesMap;
        util::DataTypes::ProgramID m_currentProgramID;//Out of all shader programs of an application, which is being used right now
#pragma endregion
        void SetShaderVersion(int version); //Shader version to be set from InitAndShowWindow();

#pragma region LIGHTS FUNCTIONS
    private:
        void ProcessVertexShaderForLight(std::string& vsCode, util::ShaderCodesContainer&);
        void ProcessFragmentShaderForLight(std::string& fsCode);
    public:
        void ConfigureLight(const unsigned int lightNum, const util::ProgrammablePipeline::LightProps& props);
        bool EnableLight(const unsigned int lightNum, const bool bEnable = true);
        util::ProgrammablePipeline::ObjectLightProps GetObjectLightProperties(DataTypes::ObjectID objectID);
        //send light related variables to shader
        void FlushLight();
        bool GlobalLightSwich(bool bTurnOn);
        /*
        * @brief - if light is to be suported or not - for code checks, etc.
        * may be true even if all lights are disabled via global light switch
        */
        bool GetGlobalLightSupportPP();
#pragma endregion
    public:
        void ToggleFullscreeen();
        void DestroyWindow();
        void TogglePause();
        bool IsFullScreen();
        void EnableAlpha(bool bEnable);

		OglWindow(TCHAR* className, UINT x, UINT y, UINT width, UINT height);
        
        /*
        * MASK to customize init behaviour, one of INIT_MASK_*
        * major and minor OpenGL version - see ShaderCodesContainer::m_shaderVersion doc for default value
        * 3.10 by default
        * In class, we were told to use 4.5
        */
		void InitAndShowWindow(UINT mask = 0, int major = 3, int minor = 1);
		~OglWindow();
		void RunGameLoop();

        // GetVertex shader code for program ID given by m_currentProgramID
        std::optional<std::string> GetVertexShader();
        // GetVertex shader code for program ID given by programID - when we do not want to disturb m_currentProgramID
        std::optional<std::string> GetVertexShaderWithProgramID(util::DataTypes::ProgramID programID);

        void SetVertexShaderCode(std::string code);
        std::optional<std::string> GetFragmentShader();
        std::optional<std::string> GetFragmentShaderWithProgramID(util::DataTypes::ProgramID programID);
        void SetFragmentShaderCode(std::string code);

        //for m_currentProgramID
        std::optional<GLint> GetShaderProgram();
        //for any arbitray ProgramID - when we do not want to disturb m_currentProgramID
        std::optional<GLint> GetShaderProgramWithProgramID(util::DataTypes::ProgramID programID);

        //Number of shader programs we have
        unsigned int GetNumShaderPrograms();

        /*
        * param[IN, OPTIONAL] numLightsToUse : user wants to use lights 0 to numLightsToUse -1 lights out of MAX_LIGHTS lights
        */
        void CreateNewShaderProgram(util::DataTypes::ProgramID programID, bool bAreLightsSupported = false, unsigned int numLightsToUse = 0);
        void SetCurrentShaderProgram(util::DataTypes::ProgramID programID);

        std::tuple<bool, std::string> CheckProgramLinkStatus(GLuint shaderObject);
        std::tuple<bool, std::string> CheckShaderCompileStatus(GLuint shaderObject);
        std::tuple<bool, std::string> CheckCompileOrLinkStatus(GLuint shaderObject, int statusToCheck);
        void InitShaders();
        void CreateNewObject(DataTypes::ObjectID objectID, ShaderCodesContainer::ObjectDrawType objectDrawType = ShaderCodesContainer::ObjectDrawType::DT_DrawArrays_Triangles);
        void CreateNewObject(DataTypes::ObjectID objectID, ShaderCodesContainer::ObjectDrawType objectDrawType, util::ProgrammablePipeline::ObjectLightProps);
        ShaderCodesContainer::ObjectDrawType GetObjectDrawType(DataTypes::ObjectID objectID);
        void SetShaderContainerPtr(util::ShaderCodesContainer*);
        void DrawObject(DataTypes::ObjectID objectID);
        void AddAttribute(DataTypes::ObjectID, ShaderCodesContainer::VertextAttributesEnum attrType, std::string shaderVariableName, std::vector<GLfloat> dataf, GLsizei stride, GLsizei componets, ShaderCodesContainer::AttributeBindTarget bindTarget = ShaderCodesContainer::AttributeBindTarget::BT_ARRAY_BUFFER, std::vector<GLuint> dataui = std::vector<GLuint>());
        ShaderCodesContainer::VertexAttributeStruct GetAttribute(DataTypes::ObjectID objectID, ShaderCodesContainer::VertextAttributesEnum attrType);
        void AddUniform(std::string shaderVariableName);
        GLint GetUniformLocation(std::string name);
        int GetShaderVersion();
        GLuint GetObjectVao(DataTypes::ObjectID objectID);

#ifdef _WIN32
		HWND GetHwnd();
#endif

        void SetCapFrameRate(DWORD interFrameTimeMilliSeconds/*millisecond*/, bool bCapFrameRate = false);

        void AllowOutOfFocusRender(bool bAllow = false);

        float GetMinX();

        float GetMaxX();

        float GetMinY();

        float GetMaxY();

        //Newer versions
        float GetMinX2();

        float GetMaxX2();

        float GetMinY2();

        float GetMaxY2();

        double GetWindowHeightNDC();

        double GetWindowWidthNDC();

        //Pixels on screen to normalized coordinates that openGL uses
        //Should behave same for X, and Y directions
        double PixelsToNDC(int pixels);
        //normalized coordinates that openGL uses to pixels on screen
        //Should behave same for X, and Y directions
        int NDCToPixels(double NDCcoord);

        void SetClearColor(COLORREF rgba);

        static void OpenGlDebugCallBack(unsigned int source, unsigned int type, unsigned int id, unsigned int severity, int length, const char* message, void* userParam);

	};

    class DrawShapes {
    public:
        DrawShapes() = delete;
        /* @brief Draws a circle made up of small line segments
         * @param[IN] NUM_LINE : number lines used to make the circle. Higher the number, the will be approximation of a circle.
         * @RADIUS[IN] radius of the circle
        */
        static void DrawCircleLines(UINT NUM_LINE, double RADIUS, COLORREF color = RGB(255, 255, 255));
        static void DrawCircleArcTriangleFan(double RADIUS, double startAngle, double endAngle, COLORREF centreColor = RGB(255, 255, 255), COLORREF boundaryColor = RGB(255, 255, 255), UINT NUM_PTS = 100);
        static auto InterpolateColor(double val, double max_val/*Corresponding to col1*/, double min_val/*Corresponding to col2*/, COLORREF col1, COLORREF col2);
        static void DrawHollowCircleTriangleStrip(double innerRadius, double outerRadius, double startAngle, double endAngle, COLORREF topColor = RGB(255, 255, 255), COLORREF bottomColor = RGB(255, 255, 255), UINT NUM_PTS = 100);
        static void DrawRectangleLines(double left, double bottom, double right, double top, COLORREF color = RGB(255, 255, 255));
        static void DrawTriangleLines(DataTypes::POINTd p1, DataTypes::POINTd p2, DataTypes::POINTd p3, COLORREF color = RGB(255, 255, 255));
        static void DrawTriangleSolid(DataTypes::POINTd p1, DataTypes::POINTd p2, DataTypes::POINTd p3, COLORREF color);
        static void DrawEqTriangleByIncentre(DataTypes::Centre centre, DataTypes::Radius, COLORREF color = RGB(255, 255, 255));
        static std::tuple<DataTypes::Centre, DataTypes::Radius> GetTriangleIncentre(DataTypes::POINTd p1, DataTypes::POINTd p2, DataTypes::POINTd p3);
        static void DrawGraph(float factor);
    };

    class Math {
    public:

    };

    class ColorWheel {
    public:
        typedef long long VLONG;
        const VLONG max_cols_range = 256 * 256 * 256;
        const VLONG min_cols_range = 0;
        VLONG num_partitions = 0;
        VLONG radius;
        typedef BYTE RED;
        typedef BYTE GREEN;
        typedef BYTE BLUE;
        const double PI;
        const double PI2;

        ColorWheel(VLONG numPartitions);

        struct COLORS
        {
            static constexpr COLORREF INDIA_SAFRON = RGB(0xFF, 0x99, 0x33);
            static constexpr COLORREF INDIA_GREEN = RGB(0x13, 0x88, 0x08);
            static constexpr COLORREF WHITE = RGB(255, 255, 255);
        };

        std::tuple<RED, GREEN, BLUE> MapIndexToColor(VLONG index);
    };

    //every object to be drawn on screen must be derived from this
    class SceneObject {
    protected:
        UINT m_zOrder;
        DataTypes::POINT3Dd m_position;
        BYTE m_alpha;/*Alpha can be part of COLORREF or m_alpha. m_alpha is for entire object*/
        DataTypes::Size2Df m_cellSize;
    public:
        void SetAlpha(BYTE alpha)
        {
            m_alpha = alpha;
        }

        void SetCellSize(const DataTypes::Size2Df& sz)
        {
            m_cellSize = sz;
        }

        SceneObject() : m_position{ 0,0,0 }, m_zOrder(0), m_alpha(255), m_cellSize{0,0}
        {

        }

        DataTypes::Size2Df GetCellSize()
        {
            return m_cellSize;
        }

        //The logic to draw whatever any derived class wants to render on screen
        //For eg. Draw() of derived class airplane will draw an airplane, while for face, it will draw a face
        //Most objects are drawn in thirds quadrant of object coordinate system
        //Some, like speech buble are drawn in the first quadrant
        virtual void Draw(AnimationObjectState& state) = 0;

        //29 June 2019 - I don't remember why AdvanceTime() was added, but in all derived classes created
        //during the days this base class was written I see that it hasn't been implemented
        //And either are empty, or throw exception when called
        virtual void AdvanceTime() = 0;
    };

    class TextHelper
    {
        const double m_wordSpacing;
        const double m_characterSpacing;
        COLORREF m_top, m_bottom, m_background; //gradient
        double m_fontHeight = 0.2;
        std::wstring m_str;
        BYTE m_stringAlpha;
    public:
        TextHelper(double wordSpacing, double characterSpacing, COLORREF top = RGB(255, 255, 255), COLORREF bottom = RGB(255, 255, 255), BYTE stringAlpha = 255/*Opaque by default*/) : m_wordSpacing(wordSpacing), m_characterSpacing(characterSpacing), m_top(top), m_bottom(bottom), m_stringAlpha(stringAlpha), m_background(RGB(0, 0, 0))
        {

        }

        void SetBackGroundColor(COLORREF backgroundColor)
        {
            m_background = backgroundColor;
        }

        void SetFontHeight(double height);

        void WriteText(std::wstring line, DataTypes::POINT3Dd relativePos /*Relative to transformation already done on MODELVIEW matrix*/);

        double GetOnScreenSize(std::wstring line);

        ///cell size of the cell with maximum height
        DataTypes::Size2Df GetMaxHeightCellSize(std::wstring line);


        //Each letter is represented by an object of this class
        // Hebrew alphabets are used for hardcoded customizations 
        //Customizations
        //Alef (א) represents tricolor A in static India assignment
        class Alphabet final : public SceneObject
        {
            COLORREF m_top, m_bottom, m_backGround;
            wchar_t m_char;//character this object represents
            double m_monospaceRatio = 0.72; //Width:Height (Height is longer) - got this by observing consolas font viz also monospace
        public:
            //We do not want anyone to cell arbitray cell size
            //Since we have to maintain aspect ratio for letters
            void SetCellSize(const DataTypes::Size2Dd& sz) = delete;
            Alphabet(wchar_t chr) : Alphabet(chr, RGB(255, 255, 255), RGB(255, 255, 255), 255)
            {
            }

            Alphabet(wchar_t chr, COLORREF top, COLORREF bottom, BYTE objectAlpha) : m_char(chr), m_top(top), m_bottom(bottom), m_backGround(0)
            {
                m_cellSize = { 0.2*m_monospaceRatio, 0.2 };
                m_alpha = objectAlpha;
            }

            void SetBackGroundColor(COLORREF background)
            {
                m_backGround = background;
            }

            void SetTopColor(COLORREF top)
            {
                m_top = top;
            }

            void SetBottomColor(COLORREF bottom)
            {
                m_bottom = bottom;
            }

            void SetChar(wchar_t chr)
            {
                m_char = chr;
            }

            virtual void Draw(AnimationObjectState& state) override;
            void SetFontHeight(double height);
            DataTypes::Size2Df GetCellSize()
            {
                return m_cellSize;
            }
        private:
            void DrawA();
            void DrawF();
            void DrawD();
            void DrawI();

            /// <summary> Unicode English Alphabet N
            /// </summary>
            void DrawN();
            void SolidFillCellWithBackground();
            void GradientFillCellH();
#if SUPPORT_UNICODE_LIB //otherwise cuda files don't compile correctly even if the file is saved as unicode
            void DrawΛ();
            //Hebrew alpabets are used to cutominzed drawings, eg. א (Alef) is used for Λ with tricolor
            // horizontal lines for India assignments
            /// @brief Alef - Tricolor A (Λ) in static/dynamic India
            void Drawא();
            /*Bet - Fade-in D in Dynamic Idia - A better solution would gave been to use
            Transparancy keyframes, and tweening, but transparency code I wrote isn't working correctly -
            One reason is that all alphabets are being written assuming opacity, on enabling transparency the hidden
            overlapping regions become visible
            */
            void Drawב(AnimationObjectState& state);
            
            /*Gimel - Render I alphabet for stencil
            */
            void Drawג(AnimationObjectState& state);


            /*Dalet - Render N alphabet for stencil
            */
            void Drawד(AnimationObjectState& state);


            /*He - Render D alphabet for stencil
            */
            void Drawה(AnimationObjectState& state);


            /*Vav - Render A alphabet without the horizonatl line for stencil
            */
            void Drawו(AnimationObjectState& state);
#endif
            // Inherited via SceneObject
            virtual void AdvanceTime() override;
        };

    };

    /*Animation object takes care of rotation, scaling, etc. of a scene object embedded in it.
    The object itself is unaware of these traslations. This struct will carry data to objects while drawing, in case the object
    wants to refer to it.
    */
    struct AnimationObjectState
    {
        DataTypes::POINT3Df m_CurrentPos, m_CurrentScale, m_CurrentRotationVector /*Rotation vector*/;
        double m_CurrentRotationAngle;
        double m_CurrentTransparency;
        double m_CurrentTime;
        double m_stepSize;
        OglWindow *m_windowObject;

        bool m_bIsDataValid;//It is possible that scene objects are called w/o animation or timeline
        AnimationObjectState() :m_bIsDataValid(false)
        {

        }
        AnimationObjectState(DataTypes::POINT3Df currentPos, DataTypes::POINT3Df currentScale, DataTypes::POINT3Df currentRotationVector, double currentRotationAngle, double currentTransparency, double currentTime, double stepSize, OglWindow *window = nullptr) :
            m_CurrentPos(currentPos), m_CurrentScale(currentScale), m_CurrentRotationVector(currentRotationVector),
            m_CurrentRotationAngle(currentRotationAngle), m_CurrentTransparency(currentTransparency), m_CurrentTime(currentTime),
            m_stepSize(stepSize), m_windowObject(window)
        {

        }
    };

    /*GL_QUADS is depricated
    * Convert vertices which represent a Quad
    * dimensions per vertex for eg. for color - r,g,b it will be 3
    * for position x,y,z it will be 3
    * for texture coordinates - s,t it will be 2
    */
    std::vector<GLfloat> QuadToTriangle(std::vector<GLfloat> QuadVertices, int dimensions = 3);
}