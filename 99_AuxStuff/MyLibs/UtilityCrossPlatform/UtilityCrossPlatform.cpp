﻿// UtilityCrossPlatform.cpp : Defines the entry point for the application.
//

#include "UtilityCrossPlatform.h"

using namespace std;
using namespace projectCode;

vmath::mat4 projectCode::inverse(vmath::mat4 mat)
{
    //float a0 = a.data[0] * a.data[5] - a.data[4] * a.data[1];
    float a0 = mat[0][0] * mat[1][1] - mat[1][0] * mat[0][1];

    //float a1 = a.data[0] * a.data[9] - a.data[8] * a.data[1];
    float a1 = mat[0][0] * mat[2][1] - mat[2][0] * mat[0][1];

    //float a2 = a.data[0] * a.data[13] - a.data[12] * a.data[1];
    float a2 = mat[0][0] * mat[3][1] - mat[3][0] * mat[0][1];

    //float a3 = a.data[4] * a.data[9] - a.data[8] * a.data[5];
    float a3 = mat[1][0] * mat[2][1] - mat[2][0] * mat[1][1];

    //float a4 = a.data[4] * a.data[13] - a.data[12] * a.data[5];
    float a4 = mat[1][0] * mat[3][1] - mat[3][0] * mat[1][1];

    //float a5 = a.data[8] * a.data[13] - a.data[12] * a.data[9];
    float a5 = mat[2][0] * mat[3][1] - mat[3][0] * mat[2][1];

    //float b0 = a.data[2] * a.data[7] - a.data[6] * a.data[3];
    float b0 = mat[0][2] * mat[1][3] - mat[1][2] * mat[0][3];

    //float b1 = a.data[2] * a.data[11] - a.data[10] * a.data[3];
    float b1 = mat[0][2] * mat[2][3] - mat[2][2] * mat[0][3];

    //float b2 = a.data[2] * a.data[15] - a.data[14] * a.data[3];
    float b2 = mat[0][2] * mat[3][3] - mat[3][2] * mat[0][3];

    //float b3 = a.data[6] * a.data[11] - a.data[10] * a.data[7];
    float b3 = mat[1][2] * mat[2][3] - mat[2][2] * mat[1][3];

    //float b4 = a.data[6] * a.data[15] - a.data[14] * a.data[7];
    float b4 = mat[1][2] * mat[3][3] - mat[3][2] * mat[1][3];

    //float b5 = a.data[10] * a.data[15] - a.data[14] * a.data[11];
    float b5 = mat[2][2] * mat[3][3] - mat[3][2] * mat[2][3];

    float det = a0 * b5 - a1 * b4 + a2 * b3 + a3 * b2 - a4 * b1 + a5 * b0;

    if (0 == det)
    {
        return vmath::mat4(INFINITY);
    }

    float inv_det = 1.0f / det;

    vmath::mat4 inv;
    inv[0][0] = +mat[1][1] * b5 - mat[2][1] * b4 + mat[3][1] * b3;
    inv[0][1] = -mat[0][1] * b5 + mat[2][1] * b2 - mat[3][1] * b1;
    inv[0][2] = +mat[0][1] * b4 - mat[1][1] * b2 + mat[3][1] * b0;
    inv[0][3] = -mat[0][1] * b3 + mat[1][1] * b1 - mat[2][1] * b0;
    inv[1][0] = -mat[1][0] * b5 + mat[2][0] * b4 - mat[3][0] * b3;
    inv[1][1] = +mat[0][0] * b5 - mat[2][0] * b2 + mat[3][0] * b1;
    inv[1][2] = -mat[0][0] * b4 + mat[1][0] * b2 - mat[3][0] * b0;
    inv[1][3] = +mat[0][0] * b3 - mat[1][0] * b1 + mat[2][0] * b0;
    inv[2][0] = +mat[1][3] * a5 - mat[2][3] * a4 + mat[3][3] * a3;
    inv[2][1] = -mat[0][3] * a5 + mat[2][3] * a2 - mat[3][3] * a1;
    inv[2][2] = +mat[0][3] * a4 - mat[1][3] * a2 + mat[3][3] * a0;
    inv[2][3] = -mat[0][3] * a3 + mat[1][3] * a1 - mat[2][3] * a0;
    inv[3][0] = -mat[1][2] * a5 + mat[2][2] * a4 - mat[3][2] * a3;
    inv[3][1] = +mat[0][2] * a5 - mat[2][2] * a2 + mat[3][2] * a1;
    inv[3][2] = -mat[0][2] * a4 + mat[1][2] * a2 - mat[3][2] * a0;
    inv[3][3] = +mat[0][2] * a3 - mat[1][2] * a1 + mat[2][2] * a0;
    inv *= inv_det;

    return inv;
}

