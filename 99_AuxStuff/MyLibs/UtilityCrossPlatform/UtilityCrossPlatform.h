﻿// UtilityCrossPlatform.h : Include file for standard system include files,
// or project specific include files.

#pragma once

#include <iostream>
#include "C:\R\99_AuxStuff\3rdPartyLibs\redbook\vmath.h"

// TODO: Reference additional headers your program requires here.

//Namespace for code I am writing for RTR project - to distinguish it from my own projects and assignments
namespace projectCode {
    vmath::mat4 inverse(vmath::mat4 mat);
}
