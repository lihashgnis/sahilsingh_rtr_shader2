#ifdef USING_PRECOMPILED_HEADERS
#include "stdafx.h"
#endif
#include "faces.h"
#include "Textures.h"
#include <GL/gl.h>
#include "../Logging/Logger.hpp"
#include <string>
#include "FreetypeWrapper.h"

using namespace util;

CREATE_LOG()

void util::faces2D::AddSpeechBubble(const SpeechBubble & bubble)
{
    m_speechBubbles.push_back(bubble);//Add copy, and not a reference
}

void util::faces2D::EnableSpeechBubble(bool bEnable)
{
    m_bShowSpeechBubles = bEnable;
}

faces2D::faces2D()
{
}


faces2D::~faces2D()
{
}

bool faces2D::SetCurrentExpression(FaceExpressions expression)
{
    m_CurrentExpression = expression;
    return true;
}

void util::faces2D::SetCellAspectRatio(GLfloat aspectRatio)
{
    m_cellAspectRatio = aspectRatio;
}

bool util::faces2D::AddExpression(FaceExpressions expression, const char textureImagePath[], Textures::TextureType type)
{
    if (FaceExpressions::NONE == expression)
    {
        throw std::runtime_error("Cannot set texture for NONE expression. It serves as a sentinel");
    }

    bool retval = false;
    GLuint textureID;
    if (true == Textures::LoadTexture(&textureID, textureImagePath, type))
    {
        m_FaceExpressionToTextureMap[expression] = textureID;
        retval = true;
    }
    return retval;
}

void util::faces2D::SetCellHeight(GLfloat height)
{
    if (m_cellAspectRatio <= 0)
    {
        LOG(Error, "Invalid value for aspect ratio : " + std::to_string(m_cellAspectRatio));
    }

    m_cellSize.cy = height;
    m_cellSize.cx = m_cellAspectRatio * height;
}

unsigned int faces2D::IncrementSpeechBubbleIndex()
{
    m_SpeechBubbleIndex++;
    ASSERT(m_SpeechBubbleIndex < m_speechBubbles.size(), "Speech Bubble Index Overflow");
    return m_SpeechBubbleIndex;//return the current value
}

util::faces2D::SpeechBubble util::faces2D::GetCurrentSpeechBuble()
{
    return m_speechBubbles[m_SpeechBubbleIndex];
}

/* Load the face texture in a trasparent Quad
*/
void util::faces2D::Draw(AnimationObjectState & state)
{
    auto texture = m_FaceExpressionToTextureMap.find(m_CurrentExpression);
    if (m_FaceExpressionToTextureMap.end() == texture)
    {
        throw std::runtime_error("Texture not found");
    }

    glBindTexture(GL_TEXTURE_2D, texture->second);

    glBegin(GL_QUADS);
    glColor4f(1, 1, 1, 1);
    glTexCoord2f(0, 1);
    glVertex2f(0, 0);
    glTexCoord2f(0, 0);
    glVertex2f(0, -m_cellSize.cy);
    glTexCoord2f(1, 0);
    glVertex2f(m_cellSize.cx, -m_cellSize.cy);
    glTexCoord2f(1, 1);
    glVertex2f(m_cellSize.cx, 0);
    glEnd();

    if (true == m_bShowSpeechBubles)
    {
        m_speechBubbles[m_SpeechBubbleIndex].Draw(state);
    }
}

void util::faces2D::AdvanceTime()
{
    THROW_NOT_IMPLEMENTED
}

util::faces2D::SpeechBubble::SpeechBubble(DataTypes::POINT2Dd bubblePosRelative, DataTypes::Size2Dd bubbleSize, const char texturePath[], Textures::TextureType type, SpeechBubleState state, std::string text, std::string fontName, FaceExpressions faceExpression) : m_state(state), m_text(text), m_bubblePosRelative(bubblePosRelative), m_bubbleSize(bubbleSize),
m_fontName(fontName),
m_faceExpression(faceExpression)
{
    SetImage(texturePath, type);
    m_fontHeightNDC = 0.08;//default
    m_fontColor = RGBA(255, 255, 255, 255);//white default
    m_fontDisplacementRelative = { 0,0 }; //no displacement by default
}

void util::faces2D::SpeechBubble::SetFontHeightNDC(double fontHeightNDC)
{
    m_fontHeightNDC = fontHeightNDC;
}

void util::faces2D::SpeechBubble::SetFontColor(COLORREF rgba)
{
    m_fontColor = rgba;
}

//font displacement within the speech bubble - wrt top left

void util::faces2D::SpeechBubble::SetRelativeFontDisplacement(DataTypes::POINT2Dd pt)
{
    m_fontDisplacementRelative = pt;
}

void util::faces2D::SpeechBubble::SetText(std::string text)
{
    m_text = text;
}

bool util::faces2D::SpeechBubble::SetImage(const char texturePath[], Textures::TextureType type)
{
    //TODO: free textures. Get rid of resource leaks - when user tries to change existing texture - take care of caching while freeing
    return Textures::LoadTexture(&m_textureID, texturePath, type);
}

void util::faces2D::SpeechBubble::Draw(AnimationObjectState & state)
{
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
        glTranslatef(m_bubblePosRelative.x, m_bubblePosRelative.y, 0);
        glBindTexture(GL_TEXTURE_2D, m_textureID);
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

        //Draw bubles in first quadrant, so that it grows upwards from face
        glBegin(GL_QUADS);
            glTexCoord2f(0, 1); glVertex2f(0, m_bubbleSize.cy);
            glTexCoord2f(0, 0); glVertex2f(0, 0);
            glTexCoord2f(1, 0); glVertex2f(m_bubbleSize.cx, 0);
            glTexCoord2f(1, 1); glVertex2f(m_bubbleSize.cx, m_bubbleSize.cy);
        glEnd();

        //write text over the speech bubble
        glTranslatef(0, m_bubbleSize.cy, 0);//so that text grows from downwards from top of speech bubble
        glTranslatef(m_fontDisplacementRelative.x, m_fontDisplacementRelative.y, 0);
        auto ft = util::FreetypeWrapper::init(m_fontName, m_fontHeightNDC, m_fontColor, state.m_windowObject);
        ft->printft((float)0, (float)0, m_text);
    glPopMatrix();
}

void util::faces2D::SpeechBubble::AdvanceTime()
{
    THROW_NOT_IMPLEMENTED
}

void util::faces2D::SpeechBubble::SetFaceExpression(FaceExpressions faceExpression)
{
    m_faceExpression = faceExpression;
}

util::faces2D::FaceExpressions util::faces2D::SpeechBubble::GetFaceExpression()
{
    return m_faceExpression;
}

DataTypes::POINT2Dd util::faces2D::SpeechBubble::GetBublePosRelative()
{
    return m_bubblePosRelative;
}

void util::faces2D::SpeechBubble::SetBublePosRelative(DataTypes::POINT2Dd relPos)
{
    m_bubblePosRelative = relPos;
}
