#pragma once

#include "UtilCrossPlatform.hpp"
#include "Textures.h"
#include <map>
#include <GL/gl.h>
#include <GL/glu.h>
#include <vector>

namespace util {
    class faces2D :
        public SceneObject
    {
    public:
        enum class FaceExpressions :UINT {
            NONE,
            HAPPY,
            HAPPY_EYESCLOSED,
            HAPPY_NEUTRAL,
            NEUTRAL_HAPPY,
            NEUTRAL_EYESCLOSED,
            NEUTRAL,
            INQUISITIVE,
            ANNOYED_SLIGHT,
            ANNOYED,
            WORRIED,
            SAD,
            EXCITED,
            ANGRY
        };

#pragma region speech bubble specific code
        //Speech bubble is also inherited from SceneObject, in case it is ever needed to draw them
        //independent of face object
        class SpeechBubble : public SceneObject {
        public:
            enum class SpeechBubleState {
                NOT_VISIBLE,
                SHOWING,
                ANIMATING
            };

            //Prevent default creation, we have some parameters which must be specified
            SpeechBubble() = delete;

            /* @brief constructor
            * @param[IN] bubblePosRelative : relative to the face
            */
            SpeechBubble(DataTypes::POINT2Dd bubblePosRelative, DataTypes::Size2Dd bubbleSize, const char texturePath[], Textures::TextureType type, SpeechBubleState state = SpeechBubleState::NOT_VISIBLE, std::string text = "", std::string fontName = "RobotoMono-Regular.ttf", FaceExpressions faceExpression = FaceExpressions::NONE);

#pragma region text properties
            void SetFontHeightNDC(double fontHeightNDC);
            void SetFontColor(COLORREF rgba);
            //font displacement within the speech bubble - wrt top left
            void SetRelativeFontDisplacement(DataTypes::POINT2Dd pt);
            void SetText(std::string text);
#pragma endregion

            //set speech buble image - over this image, the text will be rendered
            bool SetImage(const char texturePath[], Textures::TextureType type);

            // Inherited via SceneObject
            virtual void Draw(AnimationObjectState & state) override;
            virtual void AdvanceTime() override;
            void SetFaceExpression(FaceExpressions faceExpression);
            FaceExpressions GetFaceExpression();
            DataTypes::POINT2Dd GetBublePosRelative();
            void SetBublePosRelative(DataTypes::POINT2Dd relPos);
        protected:
            SpeechBubleState m_state = SpeechBubleState::NOT_VISIBLE;
            //text to display in the speech buble
            std::string m_text;
            std::string m_fontName;
            double m_fontHeightNDC;
            COLORREF m_fontColor;
            DataTypes::POINT2Dd m_fontDisplacementRelative;//font displacement within the speech bubble - wrt top left
            //The speech buble image -> over which the text would be written
            GLuint m_textureID;
            DataTypes::POINT2Dd m_bubblePosRelative;
            DataTypes::Size2Dd m_bubbleSize;
            //A speech bubble may have an associated expression, this can be queried while rendering the speech bubble
            FaceExpressions m_faceExpression;
        };

        void AddSpeechBubble(const SpeechBubble& bubble);

        /*
        param[IN] bEnable : true if you want to enable showing speech bubles
                            false otherwise
        */
        void EnableSpeechBubble(_In_ bool bEnable = true);
#pragma endregion

        faces2D();
        ~faces2D();

        //Load texture present in RC file, which has the given resource ID
        //Map it to expression type
        // Separate texture for each expression
        //Return true on success;
        bool AddExpression(FaceExpressions expression, const char textureImagePath[], Textures::TextureType type);
        bool SetCurrentExpression(FaceExpressions expression);
        
        void SetCellAspectRatio(GLfloat aspectRatio);

        //Set cell size, using the aspect ratio, and height
        void SetCellHeight(GLfloat height);

        unsigned int IncrementSpeechBubbleIndex();

        SpeechBubble GetCurrentSpeechBuble();

        // Inherited via SceneObject
        virtual void Draw(AnimationObjectState & state) override;
        virtual void AdvanceTime() override;
    protected:
        //Width to height ratio of the cell
        GLfloat m_cellAspectRatio = 0;
#pragma region speech bubble specific code
        bool m_bShowSpeechBubles = false;//Show speech bubles when true
        unsigned int m_SpeechBubbleIndex = 0;//nth speech buble is being shown right now
        std::vector<SpeechBubble> m_speechBubbles;
#pragma endregion

        //We do not want anyone to set arbitray cell size
        //Since we have to maintain aspect ratio for textures
        void SetCellSize(const DataTypes::Size2Dd& sz) = delete;
        FaceExpressions m_CurrentExpression = FaceExpressions::NONE;
        std::map<FaceExpressions, GLuint> m_FaceExpressionToTextureMap;
    };
}