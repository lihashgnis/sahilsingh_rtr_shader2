// Headers
#include<windows.h>
#include<gl/glew.h>
#include<gl/GL.h>
#include<stdio.h>
#include "vmath.h"
#include "Sphere.h"

#pragma comment(lib, "glew32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "Sphere.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

// Namespaces
using namespace vmath;

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

// Global Function Declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
void ToggleFullScreen(void);

// Global Variable Declarations
bool bFullScreen = false;
DWORD dwstyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
bool gbActiveWindow = false;
FILE *gpFile = NULL;

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint gVertexShaderObject_depth;
GLuint gFragmentShaderObject_depth;
GLuint gShaderProgramObject_depth;

GLuint gVertexShaderObject_Quad;
GLuint gFragmentShaderObject_Quad;
GLuint gShaderProgramObject_Quad;


GLuint vao_sphere;
GLuint vbo_position_sphere;
GLuint vbo_normal_sphere;
GLuint vbo_elements_sphere;

GLuint vao_base;
GLuint vbo_position_base;
GLuint vbo_normal_base;

GLuint modelUniform_ADS;
GLuint viewUniform_ADS;
GLuint projectionUniform_ADS;

GLuint modelUniform_Shader;
GLuint viewUniform_Shader;
GLuint projectionUniform_Shader;


// Light-ADS
GLuint laUniform_ADS;
GLuint kaUniform_ADS;
GLuint ldUniform_ADS;
GLuint kdUniform_ADS;
GLuint lsUniform_ADS;
GLuint ksUniform_ADS;
GLuint lightPositionUniform_ADS;
GLuint isLKeyPressed_ADS;
GLfloat materialShininessUniform_ADS;

//Light-Shader
GLuint laUniform_Shader;
GLuint kaUniform_Shader;
GLuint ldUniform_Shader;
GLuint kdUniform_Shader;
GLuint lsUniform_Shader;
GLuint ksUniform_Shader;
GLuint lightPositionUniform_Shader;
GLuint isLKeyPressed_Shader;
GLfloat materialShininessUniform_Shader;

GLuint vao_quad;
GLuint vbo_position_quad;
GLuint vbo_texcoords_quad;
GLuint vbo_normal_quad;
GLuint fbo;
GLuint textureColorBuffer;
GLuint textureDepthBuffer;
GLuint samplerUniform;
GLuint mvpUniform_quad;

struct Light
{
	//int a;
	GLfloat Ambient[4];
	GLfloat Diffuse[4];
	GLfloat Specular[4];
	GLfloat Position[4];
};

struct Light light;

GLfloat materialAmbient[] = { 0.0f, 0.0f, 0.0f, 0.0f };
GLfloat materialDiffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat materialSpecular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat materialShininess = 128.0f;

mat4 perspectiveProjectionMatrix;
mat4 orthoProjectionMatrix;
mat4 modelMatrix;
mat4 viewMatrix;
mat4 projectionMatrix;

bool bLight = false;

// Sphere
float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];
int gNumElements;

// WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// Function Declarations
	int initialize(void);
	void display(void);
	void uninitialize(void);

	// Variable Declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyApp");
	bool bDone = false;
	int iRet = 0;
	CS_OWNDC;

	// Code
	// Initialization of WNDCLASSX
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log file cannot be created!!!"), TEXT("ERROR"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log file is successfully created!!!");
	}

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// Register above class
	RegisterClassEx(&wndclass);

	// Create Window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("ADS Per Fragment Light on Sphere"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	iRet = initialize();
	if (iRet == -1)
	{
		fprintf(gpFile, "\n\nChoose Pixel Format Failed!!!");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf(gpFile, "\n\nSet Pixel Format Failed!!!");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf(gpFile, "\n\nWGL Create Context Failed!!!");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf(gpFile, "\n\nWGL Make Current Failed!!!");
		DestroyWindow(hwnd);
	}
	else if (iRet == -5)
	{
		fprintf(gpFile, "\n\nglewInit() Failed!!!");
		uninitialize();
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf(gpFile, "\n\nInitialization Succeded!!!");
	}
	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	// Game Loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				// Call update here
			}
			// Call display here
			display();
		}
	}

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// Function Declarations
	void resize(int, int);
	//void display(void);
	void uninitialize(void);

	// Code
	switch (iMsg)
	{
	case WM_CREATE: //MessageBox(hwnd, TEXT("This is WM_CREATE"), TEXT("My Message"), MB_OK);
		break;

	case WM_SETFOCUS: gbActiveWindow = true;
		break;

	case WM_KILLFOCUS: gbActiveWindow = false;
		break;

	case WM_SIZE: resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_ERASEBKGND: return (0);

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:	DestroyWindow(hwnd);
			break;

		case 0x46:
			ToggleFullScreen();
			break;
		}
		break;

	case WM_CHAR:
		switch (wParam)
		{
		case 'l':
		case 'L':
			if (bLight == true)
			{
				bLight = false;
			}
			else
			{
				bLight = true;
			}
			break;
		}
		break;

	case WM_CLOSE: DestroyWindow(hwnd);

	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
	// Variable Declarations
	MONITORINFO mi;

	// Code
	if (bFullScreen == false)
	{
		dwstyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwstyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwstyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		bFullScreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwstyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
		bFullScreen = false;
	}
}

int initialize(void)
{
	// Function Declarations
	void resize(int, int);
	void uninitialize(void);
	void ADSPerFragment(GLuint *, GLuint *, GLuint *);
	void Shader(GLuint *, GLuint *, GLuint *);
	void quadShader(GLuint *, GLuint *, GLuint *);

	// Variable Declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;
	GLenum Result;

	// Code
	// Initialize pfd structure
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		return (-1);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return (-2);
	}

	ghrc = wglCreateContext(ghdc);

	if (ghrc == NULL)
	{
		return (-3);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return (-4);
	}

	Result = glewInit();
	if (Result != GLEW_OK)
	{
		return (-5);
	}

	// Lights
	light.Ambient[0] = { 0.0f };
	light.Ambient[1] = { 0.0f };
	light.Ambient[2] = { 0.0f };
	light.Ambient[3] = { 0.0f };

	light.Diffuse[0] = { 1.0f };
	light.Diffuse[1] = { 1.0f };
	light.Diffuse[2] = { 1.0f };
	light.Diffuse[3] = { 1.0f };

	light.Specular[0] = { 1.0f };
	light.Specular[1] = { 1.0f };
	light.Specular[2] = { 1.0f };
	light.Specular[3] = { 1.0f };

	light.Position[0] = { 100.0f };
	light.Position[1] = { 100.0f };
	light.Position[2] = { 100.0f };
	light.Position[3] = { 1.0f };

	ADSPerFragment(&gVertexShaderObject, &gFragmentShaderObject, &gShaderProgramObject);
	Shader(&gVertexShaderObject_depth, &gFragmentShaderObject_depth, &gShaderProgramObject_depth);
	quadShader(&gVertexShaderObject_Quad, &gFragmentShaderObject_Quad, &gShaderProgramObject_Quad);


	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	int gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	const GLfloat QuadVertices[] = { 1.0f, 1.0, 0.0f,
								-1.0f, 1.0f, 0.0f,
								-1.0f, -1.0f, 0.0f,
								1.0f, -1.0f, 0.0f };

	const GLfloat QuadTexcoords[] = { 1.0f, 1.0f,
									0.0f, 1.0f,
									0.0f, 0.0f,
									1.0f, 0.0f };

	const GLfloat quadNormals[] = { 0.0f, 1.0f, 0.0f,
									0.0f, 1.0f, 0.0f,
									0.0f, 1.0f, 0.0f,
									0.0f, 1.0f, 0.0f };

	glGenVertexArrays(1, &vao_quad);
	glBindVertexArray(vao_quad);

	glGenBuffers(1, &vbo_position_quad);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_quad);
	glBufferData(GL_ARRAY_BUFFER, sizeof(QuadVertices), QuadVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &vbo_texcoords_quad);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_texcoords_quad);
	glBufferData(GL_ARRAY_BUFFER, sizeof(QuadTexcoords), QuadTexcoords, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_TEXCOORD0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXCOORD0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &vbo_normal_quad);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_normal_quad);
	glBufferData(GL_ARRAY_BUFFER, sizeof(quadNormals), quadNormals, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	// fbo

	glGenFramebuffers(1, &fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);

	glGenTextures(1, &textureColorBuffer);
	glBindTexture(GL_TEXTURE_2D, textureColorBuffer);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 1024, 1024, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glBindTexture(GL_TEXTURE_2D, 0);

	glGenTextures(1, &textureDepthBuffer);
	glBindTexture(GL_TEXTURE_2D, textureDepthBuffer);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, 1024, 1024, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glBindTexture(GL_TEXTURE_2D, 0);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, textureColorBuffer, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, textureDepthBuffer, 0);

	GLenum DrawBuffers[1] = { GL_COLOR_ATTACHMENT0 };
	glDrawBuffers(1, DrawBuffers);
	//glDrawBuffer(GL_NONE);
	//glReadBuffer(GL_NONE);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		fprintf(gpFile, "\n\nFramebuffer is not complete");
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	// Create vao for sphere
	glGenVertexArrays(1, &vao_sphere);
	glBindVertexArray(vao_sphere);

	glGenBuffers(1, &vbo_position_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_sphere);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &vbo_normal_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_normal_sphere);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &vbo_elements_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	const GLfloat baseVertices[] = { 1.0f, 0.0f, -1.0f,
								-1.0f, 0.0f, -1.0f,
								-1.0f, 0.0f, 1.0f,
								1.0f, 0.0f, 1.0f };

	const GLfloat baseNormal[] = { 0.0f, 1.0f, 0.0f,
								0.0f, 1.0f, 0.0f, 
								0.0f, 1.0f, 0.0f, 
								0.0f, 1.0f, 0.0f };

	glGenVertexArrays(1, &vao_base);
	glBindVertexArray(vao_base);

	glGenBuffers(1, &vbo_position_base);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_base);
	glBufferData(GL_ARRAY_BUFFER, sizeof(baseVertices), baseVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &vbo_normal_base);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_normal_base);
	glBufferData(GL_ARRAY_BUFFER, sizeof(baseNormal), baseNormal, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	glClearDepth(1.0f);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	perspectiveProjectionMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();

	resize(WIN_WIDTH, WIN_HEIGHT);

	return (0);
}

void resize(int width, int height)
{
	// Code 
	if (height == 0)
	{
		height = 1;
	}

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = perspective(45.0f,
		((GLfloat)width) / ((GLfloat)height),
		0.1f,
		100.0f);

	orthoProjectionMatrix = ortho(-10.0f, 10.0f, -10.0f, 10.0f, -10.0f, 10.0f);

	//glBindTexture(GL_TEXTURE_2D, textureColorBuffer);
	//glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
	//glBindTexture(GL_TEXTURE_2D, 0);


	//glBindTexture(GL_TEXTURE_2D, textureDepthBuffer);
	//glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	//glBindTexture(GL_TEXTURE_2D, 0);
}

void display(void)
{
	// Code

	glBindFramebuffer(GL_FRAMEBUFFER,fbo);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glViewport(0, 0, (GLsizei)1024, (GLsizei)1024);
	glUseProgram(gShaderProgramObject);

	// Declaration of matrices
	mat4 translationMatrix;

	// Initialize above matrices to identity
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	translationMatrix = mat4::identity();

	// Sphere

	// Translate
	translationMatrix = translate(0.0f, 0.0f, -0.0f);

	// Matrix Multiplication
	modelMatrix = translationMatrix;
	projectionMatrix = orthoProjectionMatrix;

	viewMatrix=vmath::lookat(vec3(3.0f), vec3(0.0f), vec3(0.0f, 1.0f, 0.0f));

	// Send matrices to shaders
	glUniformMatrix4fv(projectionUniform_ADS, 1, GL_FALSE, projectionMatrix);
	glUniformMatrix4fv(modelUniform_ADS, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(viewUniform_ADS, 1, GL_FALSE, viewMatrix);

	if (bLight == true)
	{
		glUniform1i(isLKeyPressed_ADS, 1);
		glUniform3f(laUniform_ADS, light.Ambient[0], light.Ambient[1], light.Ambient[2]);
		//glUniform3fv(laUniform, 1, light.Ambient);
		glUniform3f(kaUniform_ADS, materialAmbient[0], materialAmbient[1], materialAmbient[2]);
		glUniform3f(ldUniform_ADS, light.Diffuse[0], light.Diffuse[1], light.Diffuse[2]);
		//glUniform3fv(laUniform, 1, light.Diffuse);
		glUniform3f(kdUniform_ADS, materialDiffuse[0], materialDiffuse[1], materialDiffuse[2]);
		glUniform3f(lsUniform_ADS, light.Specular[0], light.Specular[1], light.Specular[2]);
		//glUniform3fv(laUniform, 1, light.Specular);
		glUniform3f(ksUniform_ADS, materialSpecular[0], materialSpecular[1], materialSpecular[2]);
		glUniform4f(lightPositionUniform_ADS, 3.0f, 3.0f, 3.0f, 1.0f);
		glUniform1f(materialShininessUniform_ADS, materialShininess);
	}
	else
	{
		glUniform1i(isLKeyPressed_ADS, 0);
	}

	glBindVertexArray(vao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	modelMatrix = mat4::identity();

	modelMatrix = translate(0.0f, -1.0f, 0.0f) * scale(2.75f, 2.75f, 2.75f);

	glUniformMatrix4fv(modelUniform_ADS, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(viewUniform_ADS, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionUniform_ADS, 1, GL_FALSE, projectionMatrix);

	glBindVertexArray(vao_base);

	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

	glBindVertexArray(0);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	RECT rc = {};
	GetClientRect(ghwnd, &rc);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glViewport(0, 0, rc.right - rc.left, rc.bottom - rc.top);

	glUseProgram(gShaderProgramObject_Quad);

	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	translationMatrix = mat4::identity();

	translationMatrix = translate(0.0f, 0.0f, -3.0f);

	modelViewMatrix = translationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	// Send matrices to shaders
	glUniformMatrix4fv(mvpUniform_quad, 1, GL_FALSE, modelViewProjectionMatrix);

	glBindVertexArray(vao_quad);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textureDepthBuffer);
	glUniform1i(samplerUniform, 0);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);


	glUseProgram(0);
	SwapBuffers(ghdc);
}

void uninitialize(void)
{
	if (vbo_position_sphere)
	{
		glDeleteBuffers(1, &vbo_position_sphere);
		vbo_position_sphere = 0;
	}
	if (vbo_normal_sphere)
	{
		glDeleteBuffers(1, &vbo_normal_sphere);
		vbo_normal_sphere = 0;
	}
	if (vbo_elements_sphere)
	{
		glDeleteBuffers(1, &vbo_elements_sphere);
		vbo_elements_sphere = 0;
	}
	if (vao_sphere)
	{
		glDeleteVertexArrays(1, &vao_sphere);
		vao_sphere = 0;
	}

	glUseProgram(gShaderProgramObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);
	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;
	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;
	glUseProgram(0);


	if (bFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwstyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
	}

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpFile)
	{
		fprintf(gpFile, "\n\nLog file is closed!!!");
		fclose(gpFile);
		gpFile = NULL;
	}
}

void Shader(GLuint *vertexShaderObject, GLuint *fragmentShaderObject, GLuint *shaderProgramObject)
{
	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	// Define Vertex Shader Object
	*vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	// Write Vertex Shader Code
	const GLchar *vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec4 vColor;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform vec3 u_ld;" \
		"uniform vec3 u_kd;" \
		"uniform vec4 u_light_position;" \
		"out vec3 diffuse_color;" \
		"void main(void)" \
		"{" \
		"vec4 eye_coordinates = u_model_matrix * u_view_matrix * vPosition;" \
		"mat3 normal_matrix = mat3(transpose(inverse( u_model_matrix * u_view_matrix)));" \
		"vec3 tNorm = normalize(normal_matrix * vNormal);" \
		"vec3 s = vec3(vec3(u_light_position) - eye_coordinates.xyz);" \
		"s = normalize(s);" \
		"diffuse_color = u_ld * u_kd * max(dot(s,tNorm), 0.0);" \
		"gl_Position = u_projection_matrix  * u_view_matrix * u_model_matrix * vPosition;" \
		"}";

	// Specify source code to vertex shader object
	glShaderSource(*vertexShaderObject,
		1,
		(const GLchar**)&vertexShaderSourceCode,
		NULL);

	// Compile Vertex Shader
	glCompileShader(*vertexShaderObject);
	glGetShaderiv(*vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(*vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(*vertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "\n\nVertex Shader Compilation Log: %s", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	// Fragment Shader
	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	// Define Fragment Shader Object
	*fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	// Write Fragment Shader Code
	const GLchar *fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec3 diffuse_color;" \
		"out vec4 FragColor;" \
		"float near = 0.1;" \
		"float far = 7.0;" \
		"void main(void)" \
		"{" \
		"float z = gl_FragCoord.z * 2.0 - 1.0;" \
		"float depth = ((2.0 * near * far) / (far + near - z * (far - near))) / far;" \
		"FragColor = vec4(vec3(depth), 1.0);" \
		//"FragColor = vec4(diffuse_color, 1.0f);" 
		"}";

	// Specify source code to fragment shader object
	glShaderSource(*fragmentShaderObject,
		1,
		(const GLchar**)&fragmentShaderSourceCode,
		NULL);

	// Compile Fragment Shader
	glCompileShader(*fragmentShaderObject);
	glGetShaderiv(*fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(*fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(*fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "\n\nFragment Shader Compilation Log: %s", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	// Create Shader Program Object
	*shaderProgramObject = glCreateProgram();

	// Attach Vertex Shader
	glAttachShader(*shaderProgramObject, *vertexShaderObject);

	// Attach Fragment Shader
	glAttachShader(*shaderProgramObject, *fragmentShaderObject);

	glBindAttribLocation(*shaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(*shaderProgramObject, AMC_ATTRIBUTE_COLOR, "vColor");
	glBindAttribLocation(*shaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vNormal");

	// Link Shader Program
	glLinkProgram(*shaderProgramObject);

	glGetProgramiv(*shaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(*shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(*shaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "\n\nProgram Link Log: %s", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	modelUniform_Shader = glGetUniformLocation(*shaderProgramObject, "u_model_matrix");
	viewUniform_Shader = glGetUniformLocation(*shaderProgramObject, "u_view_matrix");
	projectionUniform_Shader = glGetUniformLocation(*shaderProgramObject, "u_projection_matrix");

	ldUniform_Shader = glGetUniformLocation(*shaderProgramObject, "u_ld");
	kdUniform_Shader = glGetUniformLocation(*shaderProgramObject, "u_kd");
	lightPositionUniform_Shader = glGetUniformLocation(*shaderProgramObject, "u_light_position");
	
}



void quadShader(GLuint *vertexShaderObject, GLuint *fragmentShaderObject, GLuint *shaderProgramObject)
{
	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	// Define Vertex Shader Object
	*vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	// Write Vertex Shader Code
	const GLchar *vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec2 vTexCoord;" \
		"uniform mat4 u_mvp_matrix;" \
		"out vec2 out_texCoord;" \
		"void main(void)" \
		"{" \
		"gl_Position =u_mvp_matrix* vPosition;" \
		"out_texCoord = vTexCoord;" \
		"}";

	// Specify source code to vertex shader object
	glShaderSource(*vertexShaderObject,
		1,
		(const GLchar**)&vertexShaderSourceCode,
		NULL);

	// Compile Vertex Shader
	glCompileShader(*vertexShaderObject);
	glGetShaderiv(*vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(*vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(*vertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "\n\nVertex Shader Compilation Log: %s", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	// Fragment Shader
	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	// Define Fragment Shader Object
	*fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	// Write Fragment Shader Code
	const GLchar *fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec2 out_texCoord;" \
		"uniform sampler2D u_sampler;"
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"FragColor = texture(u_sampler, out_texCoord);"  \
		//"FragColor = vec4(1.0);"  
		"}";

	// Specify source code to fragment shader object
	glShaderSource(*fragmentShaderObject,
		1,
		(const GLchar**)&fragmentShaderSourceCode,
		NULL);

	// Compile Fragment Shader
	glCompileShader(*fragmentShaderObject);
	glGetShaderiv(*fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(*fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(*fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "\n\nFragment Shader Compilation Log: %s", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	// Create Shader Program Object
	*shaderProgramObject = glCreateProgram();

	// Attach Vertex Shader
	glAttachShader(*shaderProgramObject, *vertexShaderObject);

	// Attach Fragment Shader
	glAttachShader(*shaderProgramObject, *fragmentShaderObject);

	glBindAttribLocation(*shaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(*shaderProgramObject, AMC_ATTRIBUTE_TEXCOORD0, "vTexCoord");

	// Link Shader Program
	glLinkProgram(*shaderProgramObject);

	glGetProgramiv(*shaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(*shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(*shaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "\n\nProgram Link Log: %s", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	mvpUniform_quad = glGetUniformLocation(*shaderProgramObject, "u_mvp_matrix");
	samplerUniform = glGetUniformLocation(*shaderProgramObject, "u_sampler");
}

void ADSPerFragment(GLuint *vertexShaderObject, GLuint *fragmentShaderObject, GLuint *shaderProgramObject)
{
	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	// Define Vertex Shader Object
	*vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	// Write Vertex Shader Code
	const GLchar *vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform int u_l_key_is_pressed;" \
		"uniform vec4 u_light_position;" \
		"out vec3 tNorm;" \
		"out vec3 lightDirection;" \
		"out vec3 viewer_vector;" \
		"void main(void)" \
		"{" \

		"vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;" \
		"tNorm = mat3(u_view_matrix * u_model_matrix) * vNormal;" \
		"lightDirection = vec3(u_light_position - eye_coordinates);" \
		"viewer_vector = vec3(-eye_coordinates.xyz);" \
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";

	// Specify source code to vertex shader object
	glShaderSource(*vertexShaderObject,
		1,
		(const GLchar**)&vertexShaderSourceCode,
		NULL);

	// Compile Vertex Shader
	glCompileShader(*vertexShaderObject);
	glGetShaderiv(*vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(*vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(*vertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "\n\nVertex Shader Compilation Log: %s", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	// Fragment Shader
	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	// Define Fragment Shader Object
	*fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	// Write Fragment Shader Code
	const GLchar *fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"vec3 phong_ads_light;" \
		"in vec3 tNorm;" \
		"in vec3 lightDirection;" \
		"in vec3 viewer_vector;" \
		"uniform int u_l_key_is_pressed;" \
		"uniform vec3 u_la;" \
		"uniform vec3 u_ka;" \
		"uniform vec3 u_ld;" \
		"uniform vec3 u_kd;" \
		"uniform vec3 u_ls;" \
		"uniform vec3 u_ks;" \
		"uniform float u_material_shininess;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"if(u_l_key_is_pressed == 1)" \
		"{" \
		"vec3 normalizedtNorm = normalize(tNorm);" \
		"vec3 normalizedLightDirection = normalize(lightDirection);" \
		"vec3 normalizedviewer_vector = normalize(viewer_vector);" \
		"float tn_dot_ld = max(dot(normalizedLightDirection, normalizedtNorm), 0.0);" \
		"vec3 reflection_vector = reflect(-normalizedLightDirection, normalizedtNorm);" \
		"vec3 ambient = u_la * u_ka;" \
		"vec3 diffuse = u_ld * u_kd * tn_dot_ld;" \
		"vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vector, normalizedviewer_vector), 0.0), u_material_shininess);" \
		"phong_ads_light = ambient + diffuse + specular;" \
		"}" \
		"else" \
		"{" \
		"phong_ads_light = vec3(1.0, 1.0, 1.0);" \
		"}" \
		"FragColor = vec4(phong_ads_light, 1.0f);" \
		"}";

	// Specify source code to fragment shader object
	glShaderSource(*fragmentShaderObject,
		1,
		(const GLchar**)&fragmentShaderSourceCode,
		NULL);

	// Compile Fragment Shader
	glCompileShader(*fragmentShaderObject);
	glGetShaderiv(*fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(*fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(*fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "\n\nFragment Shader Compilation Log: %s", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	// Create Shader Program Object
	gShaderProgramObject = glCreateProgram();

	// Attach Vertex Shader
	glAttachShader(gShaderProgramObject, *vertexShaderObject);

	// Attach Fragment Shader
	glAttachShader(gShaderProgramObject, *fragmentShaderObject);

	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vNormal");

	// Link Shader Program
	glLinkProgram(gShaderProgramObject);

	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "\n\nProgram Link Log: %s", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	modelUniform_ADS = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
	viewUniform_ADS = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
	projectionUniform_ADS = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");
	laUniform_ADS = glGetUniformLocation(gShaderProgramObject, "u_la");
	kaUniform_ADS = glGetUniformLocation(gShaderProgramObject, "u_ka");
	ldUniform_ADS = glGetUniformLocation(gShaderProgramObject, "u_ld");
	kdUniform_ADS = glGetUniformLocation(gShaderProgramObject, "u_kd");
	lsUniform_ADS = glGetUniformLocation(gShaderProgramObject, "u_ls");
	ksUniform_ADS = glGetUniformLocation(gShaderProgramObject, "u_ks");
	lightPositionUniform_ADS = glGetUniformLocation(gShaderProgramObject, "u_light_position");
	materialShininessUniform_ADS = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");
	isLKeyPressed_ADS = glGetUniformLocation(gShaderProgramObject, "u_l_key_is_pressed");
}