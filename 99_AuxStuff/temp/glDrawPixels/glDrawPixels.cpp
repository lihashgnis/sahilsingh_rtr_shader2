﻿#include "../../MyLibs/Utility/Util.hpp"
#include <gl\GL.h>
#include <gl\GLU.h>
#include <cmath>
#include <tuple>
#include <string>
#include <vector>
#include "lib.h"
#include <string>
#include <fstream>

//STRICT to prevent explicit typecasting
#define STRICT

constexpr UINT WIN_WIDTH = 1920;
constexpr UINT WIN_HEIGHT = 1920;
constexpr UINT WIN_X = 100;
constexpr UINT WIN_Y = 100;
constexpr UINT NUM_PTS = 100;
constexpr double TIME_STEP = 0.000001;
//double A = 010 / 42; //wave amplitude
constexpr double Λ = 0.3;//wavelength
constexpr double MAX_Y = 0.5;
constexpr double MIN_Y = -0.5;
constexpr double MAX_Z = 0.5;
constexpr double MIN_Z = -0.5;

double time = 0;
using namespace util;

double g_xRotAngle = 15;
double g_xTranslateZ = -19;

class MyOglWindow : public util::OglWindow
{
    const double PI;
    // Inherited via OglWindow
    virtual const char * GetMessageMap() override
    {
        return nullptr;
    }

    virtual void Display() override
    {
        std::fstream ifs(R"(C:\Users\sahils\Documents\Visual Studio 2017\Projects\RTR2018Batch_GokhaleSir_Pune\MyProjects\OpenGL\Windows\06_FourierWave\1)", std::ios::binary);
        auto bufSize = 3 * WIN_WIDTH*WIN_HEIGHT;
        std::unique_ptr<char> buff(new char[bufSize]);
        ifs.read(buff.get(), bufSize);
        glDrawPixels(WIN_WIDTH, WIN_HEIGHT, GL_RGB, GL_UNSIGNED_BYTE, buff.get());
        SwapBuffers(m_Hdc);
        ifs.close();
    }

    /*virtual void Resize(int width, int height) override
    {
        if (height == 0)
            height = 1;

        glViewport(0, 0, width, height);

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        if (width <= height)
        {
            auto factor = 1.0f*(((GLfloat)height) / ((GLfloat)width));
            m_minX = -1.0;
            m_maxX = 1.0;
            m_minY = -factor;
            m_maxY = factor;
            m_minZ = -1.0;
            m_maxZ = 1.0;
        }
        else
        {
            auto factor = 1.0f*(((GLfloat)width) / ((GLfloat)height));
            m_minX = -factor;
            m_maxX = factor;
            m_minY = -1.0;
            m_maxY = 1.0f;
            m_minZ = -1.0;
            m_maxZ = 1.0;
        }
        glOrtho(m_minX, m_maxX, m_minY, m_maxY, m_minZ, m_maxZ);
    }
    */

    virtual void Resize(int width, int height) override
    {
        if (height == 0)
            height = 1;

        glViewport(0, 0, width, height);

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        gluPerspective(45, ((GLfloat)width) / height, 0.1, 100.0f);
    }

    virtual LRESULT WndProcPre(HWND, UINT iMsg, WPARAM wParam, LPARAM lParam) override
    {
        switch (iMsg)
        {
        case WM_CHAR:
        {
            switch (wParam)
            {
            case 'f':
                //[[fallthrough]]
            case 'F':
                //return LRESULT(-1);
                break;
            default:
                break;
            }
        }
        break;
        }
        return LRESULT();
    }

    virtual LRESULT WndProcPost(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) override
    {
        auto that = (OglWindow*)GetProp(hwnd, PARENT_OBJECT);

        switch (iMsg)
        {
        case WM_KEYDOWN:
        {
            if (VK_ESCAPE == wParam)
            {
                DestroyWindow(hwnd);
            }
            else if (VK_DOWN == wParam)
            {
                if (GetAsyncKeyState(VK_LCONTROL))
                {
                    g_xTranslateZ -= 1;
                }
                else
                {
                    g_xRotAngle -= 5;
                }
            }
            else if (VK_UP == wParam)
            {
                if (GetAsyncKeyState(VK_LCONTROL))
                {
                    g_xTranslateZ += 1;
                }
                else
                {
                    g_xRotAngle += 5;
                }
            }
        }
        break;
        }
        return LRESULT();
    }

public:
    MyOglWindow(TCHAR * className, UINT x, UINT y, UINT width, UINT height, HINSTANCE hInstance) : PI(4 * atan(1)), OglWindow(className, x, y, width, height, hInstance)
    {
    }

    // Inherited via OglWindow
    virtual void Update() override
    {
    }
};

int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
    TCHAR AppName[] = TEXT("FourierWave");
    MyOglWindow myOglWnd(AppName, WIN_X, WIN_Y, WIN_WIDTH, WIN_HEIGHT, hInstance);
    myOglWnd.InitAndShowWindow();
    glClearColor(0, 0, 0, 0);

    //myOglWnd.ToggleFullscreeen();
    //myOglWnd.SetCapFrameRate(660, true);
    myOglWnd.AllowOutOfFocusRender(true);
    myOglWnd.RunGameLoop();
    return 0;
}