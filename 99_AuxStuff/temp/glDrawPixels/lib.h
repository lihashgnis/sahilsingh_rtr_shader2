﻿#pragma once
#include <iostream>
#include <random>
#include <complex>
#include <map>
#include <unordered_map>
#include <tuple>

using namespace std::complex_literals;
using namespace std;

typedef long double vecType;

constexpr vecType LX = 8;
constexpr vecType LZ = 8;
constexpr vecType N = 8;
constexpr vecType M = 8;
constexpr vecType G = 10.0;
constexpr vecType V = 1.0;
constexpr vecType L = ((V*V) / G);
constexpr vecType A = 100;


//https://stackoverflow.com/a/31957574/981766
constexpr std::int32_t ceilc(float num) {
    std::int32_t inum = static_cast<std::int32_t>(num);
    if (num == static_cast<float>(inum)) {
        return inum;
    }
    return inum + (num > 0 ? 1 : 0);
}


class V3
{
public:
    vecType i, j, k;
    V3() : i(0), j(0), k(0)
    {

    }

    V3(vecType a, vecType b, vecType c) : i(a), j(b), k(c)
    {

    }

    V3(const V3& that) //copy contructor
    {
        i = that.i;
        j = that.j;
        k = that.k;
    }

    vecType Magnitude() const
    {
        V3 sqr = *this;
        sqr.i *= sqr.i;
        sqr.j *= sqr.j;
        sqr.k *= sqr.k;

        vecType sum = powl(sqr.i + sqr.j + sqr.k, 0.5);
        return sum;
    }

    bool operator< (const V3& that) const
    {
        return (this->Magnitude() < that.Magnitude());
    }

    bool operator== (const V3& that) const
    {
        return (this->i == that.i && this->j == that.j && this->k == that.k);
    }

    bool operator!= (const V3& that) const
    {
        return !(*this == that);
    }

    V3 Normalize()
    {
        V3 N = *this;
        auto mag = Magnitude();
        N.i /= mag;
        N.j /= mag;
        N.k /= mag;
        return N;
    }

    V3 operator+(V3 that)
    {
        V3 v = *this;
        v.i += that.i;
        v.j += that.j;
        v.k += that.k;
        return v;
    }

    vecType operator*(V3 that)//Dot product
    {
        V3 v = *this;
        v.i *= that.i;
        v.j *= that.j;
        v.k *= that.k;
        return (v.i + v.j + v.k);
    }

    V3 operator-()
    {
        V3 v = *this;
        v.i *= -1;
        v.j *= -1;
        v.k *= -1;
        return v;
    }
};

V3 operator"" i(vecType a)
{
     V3 v(a, 0, 0);
     return v;
}


V3 operator"" j(vecType b)
{
    V3 v(0, b, 0);
    return v;
}

V3 operator"" k(vecType c)
{
    V3 v(0, 0, c);
    return v;
}

//Phillips sectrum
vecType PH(V3 K, vecType a, vecType l, V3 w)
{
    auto k = K.Magnitude();
    auto val = a*expl(-1 / (powl(k*l, 2)))*(powl(fabsl(K.Normalize()*w.Normalize()),2))*(1/powl(k,4));
    return val;
}

constexpr UINT K_X = ceilc(2 * N + 1);
constexpr UINT K_Y = ceilc(2 * M + 1);
std::complex<vecType> K_VALS[K_X][K_Y];

std::complex<vecType> H0(V3 K, vecType a, vecType l, V3 w)
{
    static vecType PI = 4 * atan(1);
    static bool runOnce = false;
    if (!runOnce)
    {
        for(int i = 0; i < K_X; i++)
        for(int j = 0; j < K_Y; j++)
        {
            K_VALS[i][j] = std::complex<vecType>{ -1000, -1000 };
        }
        runOnce = true;
    }

    auto idx = ceilc(((K.i)*LX) / (2 * PI) + N / 2);
    auto idy = ceilc(((K.k)*LZ) / (2 * PI) + M / 2);
    if (K_VALS[idx][idy] != std::complex<vecType>{ -1000, -1000 })
    {
        return K_VALS[idx][idy];
    }

    static std::random_device rd;
    static std::mt19937 gen(rd());
    std::normal_distribution<vecType> nd(0,1);
    auto ξr = nd(gen);
    auto ξi = nd(gen);
    std::complex<vecType> c{0,1};
    auto res = (1 / powl(2, 0.5))*(ξr + ξi*c)*powl(PH(K, a, l, w), 0.5);
    //K_VALS[idx][idy] = res;
    return res;
}

auto ω(vecType k, vecType g)//dispersion relation
{
    return powl(g * k, 0.5);
}

auto HC(V3 K, vecType a, vecType l, V3 w, vecType t, vecType g)//complex
{
    std::complex<vecType> c{ 0,1 };
    auto θ = c* ω(K.Magnitude(), g)*t;
    auto v1 = H0(K, a, l, w)* (exp(θ));
    auto v2 = H0(-K, a, l, w);
    auto v3 = v2;
    v3 = { v2.real(), -v2.imag() };//complex conjugate
    v2 = v3 * (exp(-θ));
    return v1 + v2;
}
