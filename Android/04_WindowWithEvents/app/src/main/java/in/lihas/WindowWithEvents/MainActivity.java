package in.lihas.WindowWithEvents;

//default includes
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

//included by me
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.content.pm.ActivityInfo;
import android.graphics.Color;

public class MainActivity extends AppCompatActivity {

    private MyView myView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);
        
        //get rid of title bar
        //this.requestWindowFeature(Window.FEATURE_NO_TITLE); //This doesn't work, the following works instead
        this.supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        
        //make full screen
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        
        //hide navigation bar
        this.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        //The navigation bar hidden above can reappear if user swips from bottom, and won't go, to make it so that it keeps on hiding, even after reappearing for a short while - 
        this.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION  | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        
        //forced landscape
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        
        //set background color
        this.getWindow().getDecorView().setBackgroundColor(Color.BLACK);
        
        //define out own view
        myView = new MyView(this);
        
        //set this as app's main view
        setContentView(myView);
        
    }//onCreate
    
}
