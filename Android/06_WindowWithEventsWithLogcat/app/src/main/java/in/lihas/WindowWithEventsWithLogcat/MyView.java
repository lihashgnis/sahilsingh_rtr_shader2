package in.lihas.WindowWithEventsWithLogcat;

//default includes
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

//imported by me
import android.content.Context;
import android.view.Gravity;
import android.view.MotionEvent;
import androidx.appcompat.widget.AppCompatTextView;
import android.graphics.Color;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;
import android.os.SystemClock;
import java.lang.System;
import android.widget.Toast;
import android.content.Context;

public class MyView extends AppCompatTextView implements OnGestureListener, OnDoubleTapListener{
    
    private GestureDetector gestureDetector;
    
    public MyView(Context drawingContext)
    {
        super(drawingContext);
        setTextColor(Color.rgb(0,255,0));
        setTextSize(60);
        setGravity(Gravity.CENTER);
        setText("Hello World!!1");
        
        gestureDetector = new GestureDetector(drawingContext, this, null, false);
        gestureDetector.setOnDoubleTapListener(this);
    }
    
    //handling onTouchEvent is important, since it triggers all gesture and tap events
    @Override public boolean onTouchEvent(MotionEvent event)
    {
        //code
        int eventaction = event.getAction();//will not be used in any of our projects
        if(!gestureDetector.onTouchEvent(event))
        {
            super.onTouchEvent(event);
        }
        return true;
    }
    
    @Override public boolean onDoubleTap(MotionEvent e)
    {
        setText("Double Tap");
        return true;
    }
    
    @Override public boolean onDoubleTapEvent(MotionEvent e)
    {
        return true;
    }
    
    @Override public boolean onSingleTapConfirmed(MotionEvent e)
    {
        setText("Single Tap");
        return true;
    }
    
    @Override public boolean onDown(MotionEvent e)
    {
        return true;
    }
    
    @Override public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
    {
        return true;
    }
    
    @Override public void onLongPress(MotionEvent e)
    {
        setText("Long Press");
    }
    
    @Override public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
    {
        return true;
    }
    
    @Override public void onShowPress(MotionEvent e)
    {
        
    }
    
    @Override public boolean onSingleTapUp(MotionEvent e)
    {
        return true;
    }
}