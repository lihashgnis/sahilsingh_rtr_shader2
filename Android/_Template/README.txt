Template created using android studio.
We will use this as a starting point for all future android projects.
This template created from IDE is cabaple of being compiled from commandline (use androix artifacts was checked - we want to use androidx).
Changes done to the project created by Android studio - 
    1. delete folders - .gradle, .idea, app/src/androidtest, app/src/test
    2. remove files  - Window.iml, app/app.iml, app\src\main\res\values\styles.xml, app\src\main\res\values\colors.xml
    3. Edit manifest -
            a) Add tag - <uses-sdk android:targetSdkVersion="26" android:minSdkVersion="18" />
            b) remove - android:supportsRtl, android:allowBackup attributes from <application> tag
            c) change android:theme to "@style/Theme.AppCompat"
    4. Edit build.gradle
            a) remove contents of defaultConfig block, but not the block itself - changes here are handled by uses-sdktage we added to manifest
    5. Edit activity_main.xml
            a) change androidx.constraintlayout.widget.ConstraintLayout to LinearLayout
            b) remove all LinearLayout attributes, except androi namespace definition. Add the following attributes - android:orientation="vertical", android:layout_width="fill_parent", android:layout_height="fill_parent"
            c) remove all attributes of TextView. Add following attributes - android:layout_width="fill_parent", android:layout_height="fill_parent", android:text=""
    6. strings.xml - change app name
    
Commands - 
gradlew.bat clean
gradlew.bat build
adb -d install -r app\build\outputs\apk\debug\app-debug.apk

*The folder strcuture as of now (30/6/2019) builds and install fine using commands above*
*Marking _Template directory readonly to prevent modifications - though git will ignore this attribute while uploading*