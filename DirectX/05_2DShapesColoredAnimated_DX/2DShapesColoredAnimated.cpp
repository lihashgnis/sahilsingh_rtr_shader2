#include <windows.h>
#include <iostream>

#include <d3d11.h>
#include <d3dcompiler.h>

#pragma warning(disable : 4838)
#include "XNAMath_204\xnamath.h"

#pragma comment (lib, "d3d11.lib")
#pragma comment (lib, "d3dcompiler.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//prototypes
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

DWORD gHwnd = NULL;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullScreen = false;

float gClearColor[4];

//DX
IDXGISwapChain* gpIDXGISwapChain = nullptr;
ID3D11Device* gpID3D11Device = nullptr;
ID3D11DeviceContext* gpID3D11DeviceContext = nullptr;
ID3D11RenderTargetView* gpID3D11RenderTargetView = nullptr;

ID3D11VertexShader* gpID3D11VertexShader = nullptr;
ID3D11PixelShader* gpID3D11PixelShader = nullptr;
ID3D11Buffer* gpD3D11Buffer_VertexBuffer = nullptr;
ID3D11InputLayout* gpID3D11InputLayout = nullptr;
ID3D11Buffer* gpID3D11Buffer_ConstantBuffer = nullptr;

struct CBUFFER
{
    XMMATRIX WorldViewProjectionMatrix;
};

XMMATRIX gProjectionMatrix;

//prototypes
HRESULT initialize();
void uninitialize();
HRESULT resize(int, int);
void ToggleFullscreen();

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
    WNDCLASSEX wndClass = {};
    HWND hwnd;
    MSG msg;
    TCHAR szClassName[] = TEXT("Direct3D11");
    bool bDone = false;

    wndClass.cbSize = sizeof(wndClass);
    wndClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
    wndClass.cbClsExtra = 0;
    wndClass.cbWndExtra = 0;
    wndClass.lpfnWndProc = WndProc;
    wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
    wndClass.hInstance = hInstance;
    wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
    wndClass.hCursor = LoadIcon(NULL, IDC_ARROW);
    wndClass.lpszClassName = szClassName;
    wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

    RegisterClassEx(&wndClass);

    hwnd = CreateWindow(szClassName, TEXT("Direct3D11 Window"), WS_OVERLAPPEDWINDOW, 100, 100, WIN_WIDTH, WIN_HEIGHT, NULL, NULL, hInstance, NULL);

    gHwnd = hwnd;

    ShowWindow(hwnd, iCmdShow);
    SetForegroundWindow(hwnd);
    SetFocus(hwnd);


    //initialize
    he = initialize();
    if (FAILED(hr))
    {
        MessageBoxA(NULL, "initialize failed", "Error", MB_OK);
    }

    while (bDone == false)
    {
        if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
        {
            if (msg.message == WM_QUIT)
            {
                bDonw = true
            }
            else
            {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
            }
        }
        else
        {
            display();

            if (gbActiveWindow == true)
            {
                if (gbEscapeKeyIsPressed == true)
                {
                    bDone = true;
                }
            }
        }
    }

    uninitialize();
    return (int)msg.wParam;
}


LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
    HRESULT hr;

    switch (iMsg)
    {
    case WM_ACTIVATE:
    {if (HIWORD(wParam) == 0)
    {
        gbActiveWindow = true;
    }
    else
    {
        gbActiveWindow = false
    }
    }
    break;
    case WM_ERASEBKGND:
        return 0;
    case WM_SIZE:
    {
        if (gpID3D11DeviceContext)
        {
            hr = resize(LOWORD(lParam), HIWORD(lParam));
            if (FAILED(hr))
            {
                MessageBoxA(NULL, "resize failed", "Error", MB_OK);
            }
        }
    }
    break;
    case WM_KEYDOWN:
    {

    }
    default:
        break;
    }
}
