﻿#include "../../../../99_AuxStuff/MyLibs/Utility/Util.hpp"
#include <gl\GL.h>
#include <gl/GLU.h>
#include<cmath>
#include <tuple>
#include <vector>
#include <string>

//STRICT to prevent explicit typecasting
#define STRICT

constexpr UINT WIN_WIDTH = 800;
constexpr UINT WIN_HEIGHT = 800;
constexpr UINT WIN_X = 100;
constexpr UINT WIN_Y = 100;
TCHAR g_AppName[] = TEXT("Bezier curve");
constexpr double BEZIER_DRAW_STEP = 0.01;
constexpr UINT BEZIER_ORDER = 3;
int g_pt_selected = -1; //for allowing pts of bezier curve to be modified by keyboard

/*
     P1                        P2
+-+                         +-+
+X+                         +-+
 X            XXX         XX
 XX     XXXXXXX XXX      XX
  XXX  XX         XX     X
    X XX           XX    X
    XXX             XX   X
   +-X               XX-+X
   +-+                +-+
    P0                  P3
*/

//P0 -> First Anchor point
//P1 -> First Control point
//P2 -> Second Control pint
//P3 -> Second Anchor point
//4 poin bezier - 2 anchor, 2 control
util::DataTypes::POINTd g_pts[4] = { {-0.25, -0.25}, {-0.35, 0.25}, {0.35, 0.25}, {0.25, -0.25}};

UINT g_pointSelected = 0; //current selected point of the 4

UINT F(UINT n)
{
    UINT i = 1;
    while (n)
    {
        i *= n;
        n--;
    }
    return i;
}

UINT C(UINT n, UINT i)
{
    if (i > n)
        throw std::exception("n should be <= i");
    return F(n) / (F(n - i)*F(i));
}

util::DataTypes::POINTd BZ(double t, util::DataTypes::POINTd* pts)
{
    util::DataTypes::POINTd B = {};
    constexpr auto n = BEZIER_ORDER;
    for (int i = 0; i <= BEZIER_ORDER; i++)
    {
        auto coef = C(n, i)*pow((1 - t), n - i)*pow(t, i);
        B.x += coef*pts[i].x;
        B.y += coef*pts[i].y;
    }
    return B;
}

class MyOglWindow : public util::OglWindow
{
    const double PI;
    // Inherited via OglWindow
    virtual const char * GetMessageMap() override
    {
        return nullptr;
    }
    virtual void Display() override
    {
        glClear(GL_COLOR_BUFFER_BIT);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        glPointSize(3);
        
        //DrawBezier curve
        glBegin(GL_LINE_STRIP);
        glColor3f(1, 0, 0);
        for (double i = 0; i <= 1; i+= BEZIER_DRAW_STEP)
        {
            auto B = BZ(i, g_pts);
            glVertex2d(B.x, B.y);
        }
        glEnd();

        //draw control lines
        glBegin(GL_LINES);
        glColor3f(0, 0, 1);
        glVertex2f(g_pts[0].x, g_pts[0].y);
        glVertex2f(g_pts[1].x, g_pts[1].y);

        glVertex2f(g_pts[2].x, g_pts[2].y);
        glVertex2f(g_pts[3].x, g_pts[3].y);
        glEnd();

        //Draw the points
        glBegin(GL_POINTS);
        glColor3f(1, 0, 0);
        glVertex2f(g_pts[0].x, g_pts[0].y);
        glColor3f(0, 1, 0);
        glVertex2f(g_pts[1].x, g_pts[1].y);
        glColor3f(0, 0, 1);
        glVertex2f(g_pts[2].x, g_pts[2].y);
        glColor3f(1, 1, 1);
        glVertex2f(g_pts[3].x, g_pts[3].y);
        //for (auto& pt : g_pts)
        //{
        //    glVertex2f(pt.x, pt.y);
        //}
        glEnd();
        SwapBuffers(m_Hdc);
    }

    virtual void Resize(int width, int height) override
    {
        if (height == 0)
            height = 1;

        glViewport(0, 0, width, height);

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        if (width <= height)
        {
            auto factor = 1.0f*(((GLfloat)height) / ((GLfloat)width));
            glOrtho(-1.0f, 1.0f, -1 * factor, factor, -1.0f, 1.0f);
        }
        else
        {
            auto factor = 1.0f*(((GLfloat)width) / ((GLfloat)height));
            glOrtho(-1 * factor, factor, -1.0f, 1.0f, -1.0f, 1.0f);
        }
    }

    LRESULT WndProcPost(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
    {
        switch (iMsg)
        {
        case WM_KEYDOWN:
        {
            if (VK_RIGHT == wParam)
            {
                if (g_pt_selected >= 0 && g_pt_selected <= BEZIER_ORDER)
                {
                    g_pts[g_pt_selected].x += BEZIER_DRAW_STEP;

                    wchar_t str[2] = {};
                    str[0] = L'→';
                    SetWindowTextW(hwnd, str);
                }
            }
            else if (VK_LEFT == wParam)
            {
                if (g_pt_selected >= 0 && g_pt_selected <= BEZIER_ORDER)
                {
                    g_pts[g_pt_selected].x -= BEZIER_DRAW_STEP;

                    wchar_t str[2] = {};
                    str[0] = L'←';
                    SetWindowTextW(hwnd, str);
                }
            }
            else if (VK_UP == wParam)
            {
                if (g_pt_selected >= 0 && g_pt_selected <= BEZIER_ORDER)
                {
                    g_pts[g_pt_selected].y += BEZIER_DRAW_STEP;

                    wchar_t str[2] = {};
                    str[0] = L'↑';
                    SetWindowTextW(hwnd, str);
                }
            }
            else if (VK_DOWN == wParam)
            {
                if (g_pt_selected >= 0 && g_pt_selected <= BEZIER_ORDER)
                {
                    g_pts[g_pt_selected].y -= BEZIER_DRAW_STEP;

                    wchar_t str[2] = {};
                    str[0] = L'↓';
                    SetWindowTextW(hwnd, str);
                }
                else
                {
                    g_pt_selected = -1;
                }
            }
        }
        break;
        case WM_CHAR:
        {
            char chr = wParam;
            if(chr >= '0' && chr <= std::to_string(BEZIER_ORDER).c_str()[0])
            {
                g_pt_selected = chr - '0';
                char str[2] = {};
                str[0] = chr;
                SetWindowTextA(hwnd, str);
            }
        }
        break;
        }

        return LRESULT(0);
    }

    virtual LRESULT WndProcPre(HWND, UINT, WPARAM, LPARAM) override
    {
        return LRESULT();
    }
public:
    MyOglWindow(TCHAR * className, UINT x, UINT y, UINT width, UINT height, HINSTANCE hInstance) : PI(4 * atan(1)), OglWindow(className, x, y, width, height, hInstance)
    {
    }

    // Inherited via OglWindow
    virtual void Update() override
    {
    }
};

int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
    MessageBoxW(NULL, L"[←, ↑, →, ↓ to move pts] \n[0-9] Select point (subject to max order of bezier func used). \nPress n to select none", L"Usage INFO", MB_OK);
    MyOglWindow myOglWnd(g_AppName, WIN_X, WIN_Y, WIN_WIDTH, WIN_HEIGHT, hInstance);
    myOglWnd.InitAndShowWindow();
    myOglWnd.RunGameLoop();
    return 0;
}