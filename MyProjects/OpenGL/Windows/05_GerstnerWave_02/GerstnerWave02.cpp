﻿#include "../../../../99_AuxStuff/MyLibs/Utility/Util.hpp"
#include <gl\GL.h>
#include <gl\GLU.h>
#include <cmath>
#include <tuple>
#include <string>
#include <vector>

//STRICT to prevent explicit typecasting
#define STRICT

constexpr UINT WIN_WIDTH = 1920;
constexpr UINT WIN_HEIGHT = 1080;
constexpr UINT WIN_X = 100;
constexpr UINT WIN_Y = 100;
constexpr UINT NUM_PTS = 100;
constexpr double TIME_STEP = 0.05;
double A = 0.5 / 42; //wave amplitude
constexpr double Λ = 0.3;//wavelength
constexpr double MAX_Y = 0.5;
constexpr double MIN_Y = -0.5;
constexpr double MAX_Z = 0.5;
constexpr double MIN_Z = -0.5;


using namespace util;

std::vector<util::DataTypes::POINT3Dd> g_pts;


class MyOglWindow : public util::OglWindow
{
    const double PI;
    // Inherited via OglWindow
    virtual const char * GetMessageMap() override
    {
        return nullptr;
    }

    virtual void Display() override
    {
        static bool runOnce = true;
        glClear(GL_COLOR_BUFFER_BIT);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        //gluLookAt(1, 1, 0, 0, 0, 0, -1, 1, 0);
        //glRotatef(45, 1, 0, 0);
        static double time = 0.0;
        time += TIME_STEP;
        glPointSize(3);
        glBegin(GL_POINTS);
        glColor3f(186 / 255.0, 226 / 255.0, 238 / 255.0);
        for (auto x0 : g_pts)
        {
            auto x = x0.x;
            auto y = x0.y;
            auto Λ2 = Λ;
            for (double ai = A; ai <= 1.1*A; ai += 0.05*A)
            {
                Λ2 += 0.1*Λ;
                auto k = 2 * 4 * atan(1) / Λ2;
                auto w = pow(9.8*k/**tanh(k*(MAX_Y - x0.y))*/, 0.5);
                auto f = k*ai;
                f;
                x -= ai * sin(k*x0.x - w * time);
                y += ai * cos(k*x0.x - w * time);
            }
            auto z = x0.z;
            glVertex3f(x, y, z);
        }
        glEnd();
        SwapBuffers(m_Hdc);
        runOnce = false;
    }

    virtual void Resize(int width, int height) override
    {
        if (height == 0)
            height = 1;

        glViewport(0, 0, width, height);

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        if (width <= height)
        {
            auto factor = 1.0f*(((GLfloat)height) / ((GLfloat)width));
            m_minX = -1.0;
            m_maxX = 1.0;
            m_minY = -factor;
            m_maxY = factor;
            m_minZ = -1.0;
            m_maxZ = 1.0;
        }
        else
        {
            auto factor = 1.0f*(((GLfloat)width) / ((GLfloat)height));
            m_minX = -factor;
            m_maxX = factor;
            m_minY = -1.0;
            m_maxY = 1.0f;
            m_minZ = -1.0;
            m_maxZ = 1.0;
        }
        glOrtho(m_minX, m_maxX, m_minY, m_maxY, m_minZ, m_maxZ);
    }

    virtual LRESULT WndProcPre(HWND, UINT iMsg, WPARAM wParam, LPARAM lParam) override
    {
        switch (iMsg)
        {
        case WM_CHAR:
        {
            switch (wParam)
            {
            case 'f':
                //[[fallthrough]]
            case 'F':
                //return LRESULT(-1);
                break;
            default:
                break;
            }
        }
        break;
        }
        return LRESULT();
    }

    virtual LRESULT WndProcPost(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) override
    {
        auto that = (OglWindow*)GetProp(hwnd, PARENT_OBJECT);

        switch (iMsg)
        {
        case WM_KEYDOWN:
        {
            if (VK_ESCAPE == wParam)
            {
                DestroyWindow(hwnd);
            }
        }
        break;
        }
        return LRESULT();
    }

public:
    MyOglWindow(TCHAR * className, UINT x, UINT y, UINT width, UINT height, HINSTANCE hInstance) : PI(4 * atan(1)), OglWindow(className, x, y, width, height, hInstance)
    {
    }

    // Inherited via OglWindow
    virtual void Update() override
    {
    }
};

int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
    TCHAR AppName[] = TEXT("GerstnerWave 02");
    MyOglWindow myOglWnd(AppName, WIN_X, WIN_Y, WIN_WIDTH, WIN_HEIGHT, hInstance);
    myOglWnd.InitAndShowWindow();
    glClearColor(0, 0, 0, 0);

    for (double x = -1; x <= 1; x += 2.0 / NUM_PTS)
        for (double y = MIN_Y; y <= MAX_Y; y += 2.0 / NUM_PTS)
            for (double z = MIN_Z; z <= MAX_Z; z += 2.0 / NUM_PTS)
            {
                g_pts.push_back({ x, y, z });
            }

    //myOglWnd.ToggleFullscreeen();
    myOglWnd.SetCapFrameRate(33, true);
    myOglWnd.RunGameLoop();
    return 0;
}