﻿#include "../../../../99_AuxStuff/MyLibs/Utility/Util.hpp"
#include <gl\GL.h>
#include <gl\GLU.h>
#include <cmath>
#include <tuple>
#include <string>
#include <vector>
#include "lib.h"
#include <string>
#include <fstream>
#include "complex.h"
#include "fftw3.h"
#include <fstream>

#pragma comment (lib, "libfftw3-3.lib")

//STRICT to prevent implicit typecasting
#define STRICT

constexpr UINT WIN_WIDTH = 1920;
constexpr UINT WIN_HEIGHT = 1080;
constexpr UINT WIN_X = 100;
constexpr UINT WIN_Y = 100;
constexpr UINT NUM_PTS = 100;
constexpr double TIME_STEP = 0.000001;
//double A = 010 / 42; //wave amplitude
constexpr double Λ = 0.3;//wavelength
constexpr double MAX_Y = 0.5;
constexpr double MIN_Y = -0.5;
constexpr double MAX_Z = 0.5;
constexpr double MIN_Z = -0.5;

double time = 0;
using namespace util;

double g_xRotAngle = 15;
double g_xTranslateZ = -19;

void saveScreenshotToFile(std::string filename, int windowWidth, int windowHeight);

class MyOglWindow : public util::OglWindow
{
    const double PI;
    // Inherited via OglWindow
    virtual const char * GetMessageMap() override
    {
        return nullptr;
    }

    std::vector<std::complex<vecType>> GetFourierCoefficients(double time)
    {
        V3 K{ 0,0,0 };
        V3 W = { 1,0,0 };

        std::vector<std::complex<vecType>> fourierCoefficient;

        for (vecType n = -N / 2; n < N / 2; n++)
        {
            for (vecType m = -M / 2; m < M / 2; m++)
            {
                auto KX = 2 * PI*n / LX;
                auto KZ = 2 * PI*m / LZ;
                K = { KX, 0, KZ };
                if (K.Magnitude() == 0)
                {
                    continue;
                }
                auto coeff =  HC(K, A, L, W, time, G);
                fourierCoefficient.push_back(coeff);
            }
        }

        return fourierCoefficient;
    }

    void GetInverseFourierTransform(std::vector<std::complex<vecType>> fourierCoefficient)
    {
        int nPoints = fourierCoefficient.size();
        nPoints = 2 << (int(std::ceil(std::log2(nPoints))) - 1); //nearest power of 2

        fftw_complex *in = nullptr, *out = nullptr;
        fftw_plan plan;

        const int sz = sizeof(fftw_complex)*nPoints;

        in  = (fftw_complex *)fftw_malloc(sz);
        out = (fftw_complex *)fftw_malloc(sz);

        memset(in,0, sz);

        int i = 0;
        for (const auto& coeff : fourierCoefficient)
        {
            //https://stackoverflow.com/a/10540346/981766
            in[i][0] = coeff.real();
            in[i][1] = coeff.imag();
            i++;
        }

        //plan = fftw_plan_dft_2d(nPoints, nPoints, in, out, FFTW_BACKWARD, FFTW_ESTIMATE);
        plan = fftw_plan_dft_1d(nPoints, in, out, FFTW_BACKWARD, FFTW_ESTIMATE);
        fftw_execute(plan);
        //sahils-debug remove this
        std::ofstream of("fftw_IDFT.txt", std::ios::app);
        of << std::endl;
        of << "####start####" << std::endl;

        for (int i = 0; i < nPoints; i++)
        {
            of << out[i][0] << "," << out[i][1] << std::endl;
        }

        of << "####end####" << std::endl;
        of.close();

        fftw_destroy_plan(plan);
        fftw_free(in);
        in = nullptr;
        fftw_free(out);
        out = nullptr;
    }


    virtual void Display() override
    {
        glClear(GL_COLOR_BUFFER_BIT);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        V3 ptArray[ceilc(2 * N + 1)][ceilc(2 * M + 1)] = {};
        vecType maxHeight = -100;
        vecType minHeight = 100;
        static vecType PI = 4 * atan(1);
        V3 K{ 0,0,0 }, X{ 0,0,0 };
        V3 W = { 1,0,0 };
        time += TIME_STEP;

        //using fftw library
        auto coeffs = GetFourierCoefficients(time);
        GetInverseFourierTransform(coeffs);

        std::complex<vecType> c{ 0,1 };
        glPointSize(3);
        glTranslatef(0, 0, g_xTranslateZ);
        glRotatef(g_xRotAngle, 1, 0, 0);
        //glBegin(GL_POINTS);

        std::ofstream of("my_IDFT.txt", std::ios::app);
        of << std::endl;
        of << "####start####" << std::endl;

        /*int a = 0, b = 0, cc = 0, d = 0;
        //a
        //129
        //b
        //129
        //cc
        //64
        //d
        //64
        for (vecType x = -LX; x <= LX; x += LX / N)
        {
            a++; b = 0;
            for (vecType z = -LZ; z <= LZ; z += LZ / M)
            {
                b++; cc = 0;
                for (vecType n = -N / 2; n < N / 2; n++)
                {
                    cc++; d = 0;
                    for (vecType m = -M / 2; m < M / 2; m++)
                    {
                        d++;
                    }
                }
            }
        }*/


        int i = -1, j = -1;
        for (vecType x = -LX; x <= LX; x += LX / N)
        {
            i++;
            j = -1;
            for (vecType z = -LZ; z <= LZ; z += LZ / M)
            {
                j++;
                X = { x, 0, z };
                std::complex<vecType> sum{ 0,0 };
                for (vecType n = -N / 2; n < N / 2; n++)
                {
                    for (vecType m = -M / 2; m < M / 2; m++)
                    {
                        auto KX = 2 * PI*n / LX;
                        auto KZ = 2 * PI*m / LZ;
                        K = { KX, 0, KZ };
                        if (K.Magnitude() == 0)
                        {
                            continue;
                        }
                        sum += HC(K, A, L, W, time, G)*exp(c*(K*X));
                    }
                }

                //auto h = abs(sum);
                //auto h = sum.real();
                of << sum.real() << "," << sum.imag() << std::endl;
                vecType h = sum.real() + sum.imag();
                V3 v = { x,h,z };
                if (h < minHeight)
                {
                    minHeight = h;
                }
                if (h > maxHeight)
                {
                    maxHeight = h;
                }
                ptArray[i][j] = v;
                //OutputDebugString(std::to_wstring(h).c_str());
                ///OutputDebugString(L"\n");
                //glVertex3f(x, h, z);
            }
        }

        of << "####end####" << std::endl;
        of.close();
        exit(0);

        //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        glBegin(GL_QUADS);
        double red_step = 1.0 / (i - 1), red = -red_step;
        double blue_step = 1.0 / (j - 1), blue = -blue_step;
        for (int ii = 0; ii <= i - 1; ii += 1)
        {
            red += red_step;
            blue = -blue_step;
            for (int jj = 0; jj <= j - 1; jj += 1)
            {
                blue += blue_step;


                auto v = ptArray[ii][jj];
                double col = (v.j - minHeight) / (maxHeight - minHeight);
                glColor3f(col, col, col);

                //glColor3f(red, 0, blue);
                glVertex3f(v.i, v.j, v.k);

                //glColor3f(red, 0, blue);
                v = ptArray[ii + 1][jj];
                col = (v.j - minHeight) / (maxHeight - minHeight);
                glVertex3f(v.i, v.j, v.k);

                //glColor3f(red, 0, blue);
                v = ptArray[ii + 1][jj + 1];
                col = (v.j - minHeight) / (maxHeight - minHeight);
                glVertex3f(v.i, v.j, v.k);

                //glColor3f(red, 0, blue);
                v = ptArray[ii][jj + 1];
                col = (v.j - minHeight) / (maxHeight - minHeight);
                glVertex3f(v.i, v.j, v.k);
            }
        }
        glEnd();
        SwapBuffers(m_Hdc);
        static int num = 0;
        saveScreenshotToFile(("screenshots\\" + std::to_string(++num) + ".tga"), WIN_WIDTH, WIN_HEIGHT);
    }

    /*virtual void Resize(int width, int height) override
    {
        if (height == 0)
            height = 1;

        glViewport(0, 0, width, height);

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        if (width <= height)
        {
            auto factor = 1.0f*(((GLfloat)height) / ((GLfloat)width));
            m_minX = -1.0;
            m_maxX = 1.0;
            m_minY = -factor;
            m_maxY = factor;
            m_minZ = -1.0;
            m_maxZ = 1.0;
        }
        else
        {
            auto factor = 1.0f*(((GLfloat)width) / ((GLfloat)height));
            m_minX = -factor;
            m_maxX = factor;
            m_minY = -1.0;
            m_maxY = 1.0f;
            m_minZ = -1.0;
            m_maxZ = 1.0;
        }
        glOrtho(m_minX, m_maxX, m_minY, m_maxY, m_minZ, m_maxZ);
    }
    */

    virtual void Resize(int width, int height) override
    {
        if (height == 0)
            height = 1;

        glViewport(0, 0, width, height);

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        gluPerspective(45, ((GLfloat)width) / height, 0.1, 100.0f);
    }

    virtual LRESULT WndProcPre(HWND, UINT iMsg, WPARAM wParam, LPARAM lParam) override
    {
        switch (iMsg)
        {
        case WM_CHAR:
        {
            switch (wParam)
            {
            case 'f':
                //[[fallthrough]]
            case 'F':
                //return LRESULT(-1);
                break;
            default:
                break;
            }
        }
        break;
        }
        return LRESULT();
    }

    virtual LRESULT WndProcPost(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) override
    {
        auto that = (OglWindow*)GetProp(hwnd, PARENT_OBJECT);

        switch (iMsg)
        {
        case WM_KEYDOWN:
        {
            if (VK_ESCAPE == wParam)
            {
                DestroyWindow(hwnd);
            }
            else if (VK_DOWN == wParam)
            {
                if (GetAsyncKeyState(VK_LCONTROL))
                {
                    g_xTranslateZ -= 1;
                }
                else
                {
                    g_xRotAngle -= 5;
                }
            }
            else if (VK_UP == wParam)
            {
                if (GetAsyncKeyState(VK_LCONTROL))
                {
                    g_xTranslateZ += 1;
                }
                else
                {
                    g_xRotAngle += 5;
                }
            }
        }
        break;
        }
        return LRESULT();
    }

public:
    MyOglWindow(TCHAR * className, UINT x, UINT y, UINT width, UINT height, HINSTANCE hInstance) : PI(4 * atan(1)), OglWindow(className, x, y, width, height, hInstance)
    {
    }

    // Inherited via OglWindow
    virtual void Update() override
    {
    }
};

int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
    TCHAR AppName[] = TEXT("FourierWave");
    MyOglWindow myOglWnd(AppName, WIN_X, WIN_Y, WIN_WIDTH, WIN_HEIGHT, hInstance);
    myOglWnd.InitAndShowWindow();
    glClearColor(0, 0, 0, 0);

    //myOglWnd.ToggleFullscreeen();
    //myOglWnd.SetCapFrameRate(660, true);
    myOglWnd.AllowOutOfFocusRender(true);
    myOglWnd.RunGameLoop();
    return 0;
}

void saveScreenshotToFile(std::string filename, int windowWidth, int windowHeight) {
    const int numberOfPixels = windowWidth * windowHeight * 3;
    unsigned char* pixels = new unsigned char[numberOfPixels];

    glPixelStorei(GL_PACK_ALIGNMENT, 1);
    glReadBuffer(GL_FRONT);
    glReadPixels(0, 0, windowWidth, windowHeight, GL_BGR_EXT, GL_UNSIGNED_BYTE, pixels);
    FILE *outputFile = fopen(filename.c_str(), "w");

    short header[] = { 0, 2, 0, 0, 0, 0, (short)windowWidth, (short)windowHeight, 24 };

    fwrite(&header, sizeof(header), 1, outputFile);
    fwrite(pixels, numberOfPixels, 1, outputFile);
    fclose(outputFile);

    printf("Finish writing to file.\n");
    delete[]pixels;
}