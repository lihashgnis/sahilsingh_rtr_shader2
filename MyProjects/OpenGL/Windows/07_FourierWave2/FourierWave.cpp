﻿#include "../../../../99_AuxStuff/MyLibs/Utility/Util.hpp"
#include <gl\GL.h>
#include <gl\GLU.h>
#include <cmath>
#include <tuple>
#include <string>
#include <vector>
#include "lib.h"
#include <string>

//STRICT to prevent explicit typecasting
#define STRICT

constexpr UINT WIN_WIDTH = 1920;
constexpr UINT WIN_HEIGHT = 1080;
constexpr UINT WIN_X = 100;
constexpr UINT WIN_Y = 100;
constexpr UINT NUM_PTS = 100;
constexpr double TIME_STEP = 0.000001;
//double A = 010 / 42; //wave amplitude
constexpr double Λ = 0.3;//wavelength
constexpr double MAX_Y = 0.5;
constexpr double MIN_Y = -0.5;
constexpr double MAX_Z = 0.5;
constexpr double MIN_Z = -0.5;

double time = 0;
using namespace util;

double g_xRotAngle = 15;
double g_xTranslateZ = -19;

vecType g_maxHeight = -100;
vecType g_minHeight = 100;

vecType g_htArray[N][N];

//https://stackoverflow.com/a/31957574/981766
constexpr std::int32_t ceilc(float num) {
    std::int32_t inum = static_cast<std::int32_t>(num);
    if (num == static_cast<float>(inum)) {
        return inum;
    }
    return inum + (num > 0 ? 1 : 0);
}

class MyOglWindow : public util::OglWindow
{
    const vecType PI;
    // Inherited via OglWindow
    virtual const char * GetMessageMap() override
    {
        return nullptr;
    }

    virtual void Display() override
    {
        static bool runOnce = true;
        glClear(GL_COLOR_BUFFER_BIT);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        V3 K{ 0,0,0 }, X{0,0,0};
        time += TIME_STEP;
        glPointSize(3);
        glTranslatef(0, 0, g_xTranslateZ);
        glRotatef(g_xRotAngle, 1, 0, 0);
        //glBegin(GL_POINTS);
        int i = -1, j = -1;
        const std::complex<vecType> c{ 0,1 };
        for (int k = 0; k < N; k++)
        {
            for (int l = 0; l < N; l++)
            {
                //(k,l) represents a coordinate in XZ space
                std::complex<vecType> sum0{ 0,0 };
                for (int n = 0; n < N; n++)
                {
                    std::complex<vecType> sum1{0,0};
                    for (int m = 0; m < N; m++)
                    {
                        sum1+=HC(n, m, time)*exp((c*(2.0*PI*l))/ ((vecType)N));
                    }
                    sum0 += sum1 * exp((c*(2 * PI*n*k)) / ((vecType)N));
                }
                auto h = sum0.real();
                g_htArray[k][l] = h;
                if (h < g_minHeight)
                {
                    g_minHeight = h;
                }
                if (h > g_maxHeight)
                {
                    g_maxHeight = h;
                }
            }
        }
        //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        glBegin(GL_QUADS);
        for (int k = 0; k < N; k++)
        {
            for (int l = 0; l < N; l++)
            {
                auto kk = k;
                auto ll = l;

                auto x = (kk * LXZ)/N;
                auto y = (ll * LXZ)/N;
                auto h = g_htArray[k][l];
                double col = (h - g_minHeight) / (g_maxHeight - g_minHeight);
                glColor3f(col, col, col);
                glVertex3f(x, h, y);

                kk += 1;
                //ll += 1;
                x = (kk * LXZ) / N;
                y = (ll * LXZ) / N;
                h = g_htArray[k][l];
                col = (h - g_minHeight) / (g_maxHeight - g_minHeight);
                glColor3f(col, col, col);
                glVertex3f(x, h, y);

                kk += 1;
                ll += 1;
                x = (kk * LXZ) / N;
                y = (ll * LXZ) / N;
                h = g_htArray[k][l];
                col = (h - g_minHeight) / (g_maxHeight - g_minHeight);
                glColor3f(col, col, col);
                glVertex3f(x, h, y);

                //kk += 1;
                ll += 1;
                x = (kk * LXZ) / N;
                y = (ll * LXZ) / N;
                h = g_htArray[k][l];
                col = (h - g_minHeight) / (g_maxHeight - g_minHeight);
                glColor3f(col, col, col);
                glVertex3f(x, h, y);
            }
        }
        glEnd();
        SwapBuffers(m_Hdc);
        runOnce = false;
    }

    /*virtual void Resize(int width, int height) override
    {
        if (height == 0)
            height = 1;

        glViewport(0, 0, width, height);

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        if (width <= height)
        {
            auto factor = 1.0f*(((GLfloat)height) / ((GLfloat)width));
            m_minX = -1.0;
            m_maxX = 1.0;
            m_minY = -factor;
            m_maxY = factor;
            m_minZ = -1.0;
            m_maxZ = 1.0;
        }
        else
        {
            auto factor = 1.0f*(((GLfloat)width) / ((GLfloat)height));
            m_minX = -factor;
            m_maxX = factor;
            m_minY = -1.0;
            m_maxY = 1.0f;
            m_minZ = -1.0;
            m_maxZ = 1.0;
        }
        glOrtho(m_minX, m_maxX, m_minY, m_maxY, m_minZ, m_maxZ);
    }
    */

    virtual void Resize(int width, int height) override
    {
        if (height == 0)
            height = 1;

        glViewport(0, 0, width, height);

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        gluPerspective(45, ((GLfloat)width) / height, 0.1, 100.0f);
    }

    virtual LRESULT WndProcPre(HWND, UINT iMsg, WPARAM wParam, LPARAM lParam) override
    {
        switch (iMsg)
        {
        case WM_CHAR:
        {
            switch (wParam)
            {
            case 'f':
                //[[fallthrough]]
            case 'F':
                //return LRESULT(-1);
                break;
            default:
                break;
            }
        }
        break;
        }
        return LRESULT();
    }

    virtual LRESULT WndProcPost(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) override
    {
        auto that = (OglWindow*)GetProp(hwnd, PARENT_OBJECT);

        switch (iMsg)
        {
        case WM_KEYDOWN:
        {
            if (VK_ESCAPE == wParam)
            {
                DestroyWindow(hwnd);
            }
            else if (VK_DOWN == wParam)
            {
                if (GetAsyncKeyState(VK_LCONTROL))
                {
                    g_xTranslateZ -= 1;
                }
                else
                {
                    g_xRotAngle -= 5;
                }
            }
            else if (VK_UP == wParam)
            {
                if (GetAsyncKeyState(VK_LCONTROL))
                {
                    g_xTranslateZ += 1;
                }
                else
                {
                    g_xRotAngle += 5;
                }
            }
        }
        break;
        }
        return LRESULT();
    }

public:
    MyOglWindow(TCHAR * className, UINT x, UINT y, UINT width, UINT height, HINSTANCE hInstance) : PI(4 * atan(1)), OglWindow(className, x, y, width, height, hInstance)
    {
    }

    // Inherited via OglWindow
    virtual void Update() override
    {
    }
};

int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
    TCHAR AppName[] = TEXT("FourierWave");
    MyOglWindow myOglWnd(AppName, WIN_X, WIN_Y, WIN_WIDTH, WIN_HEIGHT, hInstance);
    myOglWnd.InitAndShowWindow();
    glClearColor(0, 0, 0, 0);

    //myOglWnd.ToggleFullscreeen();
    //myOglWnd.SetCapFrameRate(660, true);
    myOglWnd.RunGameLoop();
    return 0;
}