﻿#pragma once
#include <iostream>
#include <random>
#include <complex>
#include <map>
#include <tuple>


using namespace std::complex_literals;
using namespace std;

typedef long double vecType;
class V3;

constexpr vecType LXZ = 8;//square ocean - physical dimentions of ocean in XZ plane
constexpr int N = 8;
constexpr vecType G = 9.81;
constexpr vecType V = 1.0;
constexpr vecType L = ((V*V) / G);//Parameter of phillips spectrum (not ocean dimention!)
constexpr vecType A = 10;
static vecType PI = 4 * atan(1);

class V3
{
public:
    vecType i, j, k;
    V3() : i(0), j(0), k(0)
    {

    }

    V3(vecType a, vecType b, vecType c) : i(a), j(b), k(c)
    {

    }

    V3(const V3& that) //copy contructor
    {
        i = that.i;
        j = that.j;
        k = that.k;
    }

    vecType Magnitude() const
    {
        V3 sqr = *this;
        sqr.i *= sqr.i;
        sqr.j *= sqr.j;
        sqr.k *= sqr.k;

        vecType sum = powl(sqr.i + sqr.j + sqr.k, 0.5);
        return sum;
    }

    bool operator< (const V3& that) const
    {
        return (this->Magnitude() < that.Magnitude());
    }

    V3 Normalize()
    {
        V3 N = *this;
        auto mag = Magnitude();
        N.i /= mag;
        N.j /= mag;
        N.k /= mag;
        return N;
    }

    V3 operator+(V3 that)
    {
        V3 v = *this;
        v.i += that.i;
        v.j += that.j;
        v.k += that.k;
        return v;
    }

    vecType operator*(V3 that)//Dot product
    {
        V3 v = *this;
        v.i *= that.i;
        v.j *= that.j;
        v.k *= that.k;
        return (v.i + v.j + v.k);
    }

    V3 operator-()
    {
        V3 v = *this;
        v.i *= -1;
        v.j *= -1;
        v.k *= -1;
        return v;
    }
};

V3 operator"" i(vecType a)
{
     V3 v(a, 0, 0);
     return v;
}


V3 operator"" j(vecType b)
{
    V3 v(0, b, 0);
    return v;
}

V3 operator"" k(vecType c)
{
    V3 v(0, 0, c);
    return v;
}

V3 W = { 1,0,0 }; //wind direction

//Phillips sectrum
vecType PH(V3 K)
{
    auto k = K.Magnitude();
    if (K.i == 0)
    {
        K.i = 0.00001;
    }
    if (K.k == 0)
    {
        K.k = 0.00001;
    }
    auto val = A*expl(-1 / (powl(k*L, 2)))*(powl(fabsl(K.Normalize()*W.Normalize()),2))*(1/powl(k,4));
    return val;
}

std::complex<vecType> H0(int n, int m)
{
    static std::random_device rd;
    static std::mt19937 gen(rd());
    std::normal_distribution<vecType> nd(0,1);
    auto ξr = nd(gen);
    auto ξi = nd(gen);
    std::complex<vecType> c{0,1};
    V3 K = { (2 * PI*n - PI * N) / LXZ, 0, (2 * PI*m - PI * N) / LXZ };
    auto res = (1 / powl(2, 0.5))*(ξr + ξi*c)*powl(PH(K), 0.5);
    return res;
}

auto ω(vecType k, vecType g)//dispersion relation
{
    return powl(g * k, 0.5);
}

auto HC(int n, int m, vecType t)//complex
{
    std::complex<vecType> c{ 0,1 };
    V3 K = { (2 * PI*n - PI * N) / LXZ, 0, (2 * PI*m - PI * N) / LXZ };
    auto θ = c* ω(K.Magnitude(), G)*t;
    auto v1 = H0(n,m)* (exp(θ));
    auto v2 = H0(n,m);
    auto v3 = v2;
    v3 = { v2.real(), -v2.imag() };//complex conjugate
    v2 = v3 * (exp(-θ));
    return v1 + v2;
}