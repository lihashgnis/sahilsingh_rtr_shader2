#include "Fluid.h"
#include <algorithm>

#if FLUIDSIMNUM==1

Fluid::Fluid(vecType timeStep, vecType diffusionrate, vecType viscosity) : m_diffusionRate(diffusionrate), m_timeStep(timeStep), m_viscosity(viscosity)
{
    m_velocityX.resize(gridNumEls);
    m_velocityX.assign(m_velocityX.size(), 0);

    m_velocityY.resize(gridNumEls);
    m_velocityY.assign(m_velocityX.size(), 0);

    m_density.resize(gridNumEls);
    m_density.assign(m_velocityX.size(), 0);


    m_velocityX_prev.resize(gridNumEls);
    m_velocityX_prev.assign(m_velocityX.size(), 0);

    m_velocityY_prev.resize(gridNumEls);
    m_velocityY_prev.assign(m_velocityX.size(), 0);

    m_density_prev.resize(gridNumEls);
    m_density_prev.assign(m_velocityX.size(), 0);
}

Fluid::~Fluid()
{
}



void Fluid::Diffuse(int b, std::vector<vecType>& dest, std::vector<vecType>& src, vecType diff)
{
    vecType a = m_timeStep * diff *gridSize;
    for (int k = 0; k < maxGausSeidelIterations; k++)
    {
        for(int i=1; i <= gridSize; i++)
            for (int j = 1; j <= gridSize; j++)
            {
                dest[IX(i, j)] = (src[IX(i, j)] + a*(dest[IX(i-1,j)] + dest[IX(i + 1, j)] + dest[IX(i, j-1)] + dest[IX(i, j+1)]))/(1 + 4*a);
            }
        SetBounds(b, dest);
    }
}

void Fluid::Advect(int b, std::vector<vecType>& dest, std::vector<vecType>& src, std::vector<vecType>& velocityX, std::vector<vecType>& velocityY)
{
    auto dt0 = m_timeStep * gridSize;

    for (int i = 1; i <= gridSize; i++)
        for (int j = 1; j <= gridSize; j++)
        {
            vecType x = i - dt0 * velocityX[IX(i, j)];
            vecType y = j - dt0 * velocityY[IX(i, j)];
            x = std::clamp(x, 0.5, gridSize + 0.5);
            y = std::clamp(y, 0.5, gridSize + 0.5);

            int i0 = (int)x;
            int i1 = i0 + 1;
            int j0 = (int)y;
            int j1 = j0 + 1;

            vecType s1 = x - i0;
            vecType s0 = 1 - s1;
            vecType t1 = y - j0;
            vecType t0 = 1 - t1;

            dest[IX(i, j)] = s0*(t0*src[IX(i0,j0)] + t1*src[IX(i0,j1)]) + 
                             s1 * (t0*src[IX(i1, j0)] + t1 * src[IX(i1, j1)]);
        }
    SetBounds(b, dest);
}

void Fluid::Project(std::vector<vecType>& u, std::vector<vecType>& v, std::vector<vecType>& p, std::vector<vecType>& div)
{
    vecType h = 1.0 / gridSize;

    for (int i = 1; i <= gridSize; i++)
        for (int j = 1; j <= gridSize; j++)
        {
            div[IX(i, j)] = -0.5*h*(u[IX(i + 1, j)] - u[IX(i - 1, j)] +
                v[IX(i, j + 1)] - v[IX(i, j - 1)]);
            p[IX(i, j)] = 0;
        }
    SetBounds(0, div);
    SetBounds(0, p);

    for (int k = 0; k < maxGausSeidelIterations; k++)
    {
        for (int i = 1; i <= gridSize; i++)
            for (int j = 1; j <= gridSize; j++)
            {
                p[IX(i, j)] = (div[IX(i, j)] + p[IX(i - 1, j)] + p[IX(i + 1, j)] + p[IX(i, j - 1)] + p[IX(i, j + 1)]) / 4.0;
            }
        SetBounds(0, p);
    }

    for (int i = 1; i <= gridSize; i++)
        for (int j = 1; j <= gridSize; j++)
        {
            u[IX(i, j)] -= 0.5*(p[IX(i + 1, j)] - p[IX(i - 1, j)]) / h;
            v[IX(i, j)] -= 0.5*(p[IX(i, j+1)] - p[IX(i, j-1)]) / h;
        }

    SetBounds(1, u);
    SetBounds(2, v);
}


void Fluid::SetBounds(int b, std::vector<vecType>& vec)
{
    for (int i = 1; i <= gridSize; i++)
    {
        vec[IX(0, i)] = (b == 1) ? -vec[IX(1, i)] : vec[IX(1, i)];
        vec[IX(gridSize +1, i)] = (b == 1) ? -vec[IX(gridSize, i)] : vec[IX(gridSize, i)];
        vec[IX(i,0)] = (b == 2) ? -vec[IX(i, 1)] : vec[IX(i, 1)];
        vec[IX(i, gridSize + 1)] = (b == 2) ? -vec[IX(i, gridSize)] : vec[IX(i, gridSize)];
    }
    vec[IX(0, 0)]            = 0.5 * (vec[IX(1, 0)]            + vec[IX(0, 1)]);
    vec[IX(0, gridSize + 1)] = 0.5 * (vec[IX(1, gridSize + 1)] + vec[IX(0, gridSize)]);
    vec[IX(gridSize + 1, 0)] = 0.5 * (vec[IX(gridSize,0)] + vec[IX(gridSize  + 1, 1)]);
    vec[IX(gridSize + 1, gridSize + 1)] = 0.5 * (vec[IX(gridSize, gridSize + 1)] + vec[IX(gridSize  + 1, gridSize)]);
}

#define SWAP(i,j) {std::vector<vecType> temp; temp = std::move(i); i = std::move(j); j = std::move(temp);}

void Fluid::Step()
{
    //velocity
    Diffuse(1, m_velocityX, m_velocityX_prev, m_viscosity);
    SWAP(m_velocityX, m_velocityX_prev);
    Diffuse(2, m_velocityY, m_velocityY_prev, m_viscosity);
    SWAP(m_velocityY, m_velocityY_prev);

    Project(m_velocityX, m_velocityY, m_velocityX_prev, m_velocityY_prev);
    SWAP(m_velocityX, m_velocityX_prev);
    SWAP(m_velocityY, m_velocityY_prev);

    Advect(1, m_velocityX, m_velocityX_prev, m_velocityX_prev, m_velocityY_prev);
    Advect(2, m_velocityY, m_velocityY_prev, m_velocityX_prev, m_velocityY_prev);
    Project(m_velocityX, m_velocityY, m_velocityX_prev, m_velocityY_prev);

    SWAP(m_velocityX, m_velocityX_prev);
    SWAP(m_velocityY, m_velocityY_prev);
    //density
    Diffuse(0, m_density, m_density_prev, m_diffusionRate);
    SWAP(m_density, m_density_prev);
    Advect(0, m_density, m_density_prev, m_velocityX_prev, m_velocityY_prev);
    SWAP(m_density, m_density_prev);
}

void Fluid::AddDensity(int x, int y, vecType amount)
{
    m_density_prev[IX(x, y)] += amount;
}

void Fluid::AddVelocity(int x, int y, vecType amountX, vecType amountY)
{
    m_velocityX_prev[IX(x, y)] += amountX;
    m_velocityY_prev[IX(x, y)] += amountY;
}

#endif