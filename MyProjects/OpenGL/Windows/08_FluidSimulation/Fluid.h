#pragma once
#include <vector>

constexpr int GRID_SIZE = 128;//N

typedef double vecType;
static vecType IX(int i, int j) {
    return (i + (GRID_SIZE + 2)*j);}

class Fluid
{
public:
    static constexpr int gridSize = GRID_SIZE;//N
    static constexpr int gridNumEls = (gridSize + 2)*(gridSize + 2);
private:
    static constexpr int maxGausSeidelIterations = 4;
    
    std::vector<vecType> m_velocityX;
    std::vector<vecType> m_velocityY;
    std::vector<vecType> m_density;
    std::vector<vecType> m_velocityX_prev;
    std::vector<vecType> m_velocityY_prev;
    std::vector<vecType> m_density_prev;
    
    const vecType m_diffusionRate;//diff
    const vecType m_viscosity;//visc
    const vecType m_timeStep; //dt
public:
    Fluid(vecType timeStep, vecType diffusionrate, vecType viscosity);
    ~Fluid();
private:
    void Diffuse(int b, std::vector<vecType>& dest, std::vector<vecType>& src, vecType);
    /// dest, src -> final and initial values of the property to advect. velocity* - velocity vectors to use for advection
    void Advect(int b, std::vector<vecType>& dest, std::vector<vecType>& src, std::vector<vecType>& velocityX, std::vector<vecType>& velocityY);
    void Project(std::vector<vecType>& u, std::vector<vecType>& v, std::vector<vecType>& p, std::vector<vecType>& div);
    /// vec - vector on which bounds are o be set
    void SetBounds(int b, std::vector<vecType>& vec);
public:
    void Step();
    void AddDensity(int x, int y, vecType amount);
    void AddVelocity(int x, int y, vecType amountX, vecType amountY);
    auto GetDensities()
    {
        return m_density;
    }
};

