#pragma once
#include<cstring>
constexpr int N = 128;
constexpr double DT = 0.1; //time step
constexpr int ITER = 4;//num iterations for gauss seidel
constexpr int size = (N + 2)*(N + 2);
constexpr double diffusionR = 0; //rate of diffusion
constexpr double visc = 0; //viscosity

double u[size], v[size], u_prev[size], v_prev[size];
double dens[size], dens_prev[size];

#define IX(i,j) (i + (N+2)*j)

void set_bnd(int b, double *x);

void diffuse(int b, double* x, double* x0, double diff)
{
    double a = DT * diff*N*N;

    for (int k = 0; k < ITER; k++)
    {
        for (int i = 1; i <= N; i++)
        {
            for (int j = 1; j <= N; j++)
            {
                x[IX(i, j)] = (x0[IX(i, j)] + a*(x[IX(i-1,j)] + x[IX(i+1, j)] + x[IX(i,j-1)] + x[IX(i, j+1)]))/(1+4*a);
            }
        }
        set_bnd(b, x);
    }
}

void advect(int b, double *d, double* d0, double* u, double* v)
{
    double dt0 = DT * N;

    for (int i = 1; i <= N; i++)
    {
        for (int j = 1; j <= N; j++)
        {
            double x = i - dt0 * u[IX(i, j)];
            double y = j - dt0 * v[IX(i, j)];
            
            if (x < 0.5) x = 0.5; if (x > N + 0.5) x = N + 0.5;
            int i0 = (int)x;
            int i1 = i0 + 1;

            if (y < 0.5) y = 0.5; if (y > N + 0.5) y = N + 0.5;
            int j0 = (int)x;
            int j1 = j0 + 1;

            double s1 = x - i0;
            double s0 = 1 - s1;

            double t1 = y - j0;
            double t0 = 1 - t1;

            d[IX(i, j)] = s0 * (t0*d0[IX(i0, j0)] + t1 * d0[IX(i0, j1)]) + s1*(t0*d0[IX(i1,j0)] + t1*d0[IX(i1,j1)]);
        }
    }
    set_bnd(b, d);
}

void project(double *u, double *v, double *p, double *div)
{
    double h = 1.0 / N;

    for (int i = 1; i <= N; i++)
    {
        for (int j = 1; j <= N; j++)
        {
            div[IX(i, j)] = -0.5*h*(u[IX(i + 1, j)] - u[IX(i - 1, j)] + v[IX(i, j+1)] - v[IX(i, j - 1)]);
            p[IX(i, j)] = 0;
        }
    }

    set_bnd(0, div);
    set_bnd(0, p);

    for (int k = 0; k < ITER; k++)
    {
        for (int i = 1; i <= N; i++)
        {
            for (int j = 1; j <= N; j++)
            {
                p[IX(i, j)] = (div[IX(i, j)] + p[IX(i-1,j)] + p[IX(i + 1, j)] + p[IX(i, j - 1)] + p[IX(i, j+1)])/4;
            }
        }
        set_bnd(0, p);
    }

    for (int i = 1; i <= N; i++)
    {
        for (int j = 1; j <= N; j++)
        {
            u[IX(i, j)] -= 0.5*(p[IX(i + 1, j)] - p[IX(i - 1, j)]) / h;
            v[IX(i, j)] -= 0.5*(p[IX(i, j+1)] - p[IX(i, j-1)]) / h;
        }
    }
    set_bnd(1, u);
    set_bnd(2, v);
}

void set_bnd(int b, double *x)
{
    for (int i = 0; i <= N; i++)
    {
        x[IX(0, i)] = (b == 1) ? -x[IX(1, i)] : x[IX(1, i)];
        x[IX(N+1, i)] = (b == 1) ? -x[IX(N, i)] : x[IX(N, i)];
        x[IX(i, 0)] = (b == 2) ? -x[IX(i, 1)] : x[IX(i, 1)];
        x[IX(i, N+1)] = (b == 2) ? -x[IX(i, N)] : x[IX(i, N)];
    }

    x[IX(0, 0)] = 0.5*(x[IX(1, 0)] + x[IX(0, 1)]);
    x[IX(0, N + 1)] = 0.5*(x[IX(1, N + 1)] + x[IX(0, N)]);
    x[IX(N + 1,0)] = 0.5*(x[IX(N, 0)] + x[IX(N+1, 1)]);
    x[IX(N + 1, N + 1)] = 0.5*(x[IX(N, N + 1)] + x[IX(N + 1, N)]);
}

void init()
{
    double u[size], v[size], u_prev[size], v_prev[size];
    double dens[size], dens_prev[size];
#define CLEAR(x) memset(x, 0, sizeof(x));
    CLEAR(u);
    CLEAR(v);
    CLEAR(dens);
    CLEAR(u_prev);
    CLEAR(v_prev);
    CLEAR(dens_prev);
#undef CLEAR
}

void addDensity(int x, int y, int amt)
{
    dens[IX(x, y)] = amt;
    dens_prev[IX(x, y)] = amt;
}

void addVelocity(int x, int y, double amtX, double amtY)
{
    u[IX(x, y)] = amtX;
    u_prev[IX(x, y)] = amtX;

    v[IX(x, y)] = amtY;
    v_prev[IX(x, y)] = amtY;
}


void step()
{
    diffuse(1, u_prev, u, visc);
    diffuse(2, v_prev, v, visc);

    project(u_prev, v_prev, u, v);

    advect(1, u, u_prev, u_prev, v_prev);
    advect(2, v, v_prev, u_prev, v_prev);

    project(u, v, u_prev, v_prev);

    diffuse(0, dens, dens_prev, diffusionR);
    advect(0, dens_prev, dens, u, v);
}