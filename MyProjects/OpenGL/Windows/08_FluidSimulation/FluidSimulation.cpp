#include "../../../../99_AuxStuff/MyLibs/Utility/Util.hpp"
#include "../../../../99_AuxStuff/MyLibs/Utility/stackOverflow.h"
#include <gl\GL.h>
#include <gl\GLU.h>
#include <cmath>
#include <tuple>
#include <string>
#include <vector>
#include <string>

#define FLUIDSIMNUM 3

#if FLUIDSIMNUM==1
#include "Fluid.h"
#endif

#if FLUIDSIMNUM==3
#include "Fluid3.h"
#endif


//STRICT to prevent explicit typecasting
#define STRICT


double time = 0;
using namespace util;
#if FLUIDSIMNUM==1
Fluid fl(0.1, 0, 0);
constexpr UINT WIN_WIDTH = Fluid::gridSize*2;
constexpr UINT WIN_HEIGHT = Fluid::gridSize*2;
#elif FLUIDSIMNUM==3
constexpr UINT WIN_WIDTH  = N * 2 * SCALE;
constexpr UINT WIN_HEIGHT = N * 2 * SCALE;
#endif

constexpr UINT WIN_X = 100;
constexpr UINT WIN_Y = 100;

class MyOglWindow : public util::OglWindow
{
    // Inherited via OglWindow
    virtual const char * GetMessageMap() override
    {
        return nullptr;
    }

    virtual void Display() override
    {
#if FLUIDSIMNUM==1
        fl.AddDensity(2, 2, 100);
        fl.AddVelocity(2, 2, 10, 10);
        fl.Step();
        glClear(GL_COLOR_BUFFER_BIT);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        glPointSize(3);
        auto dv = fl.GetDensities();
        glBegin(GL_POINTS);
        for(int i=1; i < Fluid::gridSize; i++)
            for (int j = 1; j < Fluid::gridSize; j++)
            {
                auto d = dv[IX(i, j)];
                d /= 255;
                glColor3f(d, d, d);
                auto x = vecType(i) / Fluid::gridSize;
                auto y = vecType(j) / Fluid::gridSize;
                glVertex3f(x, y, 0);
            }
        glEnd();
        goto end;
#endif
        ////fluid 2
        //addDensity(2, 2, 300);
        //addVelocity(2, 2, 10, 10);
        //step();
        //glBegin(GL_POINTS);
        //for(int i=1; i < N; i++)
        //    for (int j = 1; j < N; j++)
        //    {
        //        auto d = dens[IX(i, j)];
        //        d /= 255;
        //        glColor3f(d, d, d);
        //        auto x = double(i) / N;
        //        auto y = double(j) / N;
        //        glVertex3f(x, y, 0);
        //    }
        //static int runCount = 0;
        //runCount++;

#if FLUIDSIMNUM==3
#pragma region fluid 3
        static Fluid fluid(0.1, 0, 0);
        fluid.AddDensity(2, 2, 100);
        fluid.AddVelocity(2, 2, 1, 1);
        fluid.step();
        fluid.renderD();
        fluid.fadeD();
#pragma endregion
        goto end;
#endif
        end:
        SwapBuffers(m_Hdc); static int num = 0;
        saveScreenshotToFile(("screenshots\\" + std::to_string(++num) + ".tga"), WIN_WIDTH, WIN_HEIGHT);
    }

    /*virtual void Resize(int width, int height) override
    {
        if (height == 0)
            height = 1;

        glViewport(0, 0, width, height);

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        gluPerspective(45, ((GLfloat)width) / height, 0.1, 100.0f);
    }*/

    virtual void Resize(int width, int height) override
    {
        if (height == 0)
            height = 1;

        glViewport(0, 0, width, height);

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        if (width <= height)
        {
            auto factor = 1.0f*(((GLfloat)height) / ((GLfloat)width));
            glOrtho(-1.0f, 1.0f, -1 * factor, factor, -1.0f, 1.0f);
        }
        else
        {
            auto factor = 1.0f*(((GLfloat)width) / ((GLfloat)height));
            glOrtho(-1 * factor, factor, -1.0f, 1.0f, -1.0f, 1.0f);
        }
    }

    virtual LRESULT WndProcPre(HWND, UINT iMsg, WPARAM wParam, LPARAM lParam) override
    {
        switch (iMsg)
        {
        case WM_CHAR:
        {
            switch (wParam)
            {
            case 'f':
                //[[fallthrough]]
            case 'F':
                //return LRESULT(-1);
                break;
            default:
                break;
            }
        }
        break;
        }
        return LRESULT();
    }

    virtual LRESULT WndProcPost(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) override
    {
        auto that = (OglWindow*)GetProp(hwnd, PARENT_OBJECT);

        switch (iMsg)
        {
        case WM_KEYDOWN:
        {
            if (VK_ESCAPE == wParam)
            {
                DestroyWindow(hwnd);
            }
            else if (VK_DOWN == wParam)
            {
                if (GetAsyncKeyState(VK_LCONTROL))
                {
                    
                }
                else
                {
                    
                }
            }
            else if (VK_UP == wParam)
            {
                if (GetAsyncKeyState(VK_LCONTROL))
                {
                    
                }
                else
                {
                    
                }
            }
        }
        break;
        }
        return LRESULT();
    }

public:
    MyOglWindow(TCHAR * className, UINT x, UINT y, UINT width, UINT height, HINSTANCE hInstance) : OglWindow(className, x, y, width, height, hInstance)
    {
    }

    // Inherited via OglWindow
    virtual void Update() override
    {
    }
};

int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
    TCHAR AppName[] = TEXT("Fluid Simulaton");
    MyOglWindow myOglWnd(AppName, WIN_X, WIN_Y, WIN_WIDTH, WIN_HEIGHT, hInstance);
    myOglWnd.InitAndShowWindow();
    glClearColor(0, 0, 0, 0);

    //myOglWnd.ToggleFullscreeen();
    //myOglWnd.SetCapFrameRate(660, true);
    myOglWnd.AllowOutOfFocusRender(true);
    myOglWnd.RunGameLoop();
    return 0;
}