#pragma once
#include <algorithm>
#include <vector>
#include "../../../../99_AuxStuff/MyLibs/Utility/Util.hpp"
#include <gl\GL.h>
#include <gl\GLU.h>

const int N = 256;
const int iter = 10; //Gauss Seidel
const int SCALE = 1;

//prototypes
void diffuse(int b, std::vector<float>& x, std::vector<float>& x0, float diff, float dt);
void lin_solve(int b, std::vector<float>& x, std::vector<float>& x0, float a, float c);
void project(std::vector<float>& velocX, std::vector<float>& velocY, std::vector<float>& p, std::vector<float>& div);
void set_bnd(int b, std::vector<float>& x);

int IX(int x, int y)
{
    x = std::clamp(x, 0, N - 1);
    y = std::clamp(y, 0, N - 1);
    return x + y * N;
}

class Fluid
{
    int size;
    float dt; //time step
    float diff;
    float visc;

    std::vector<float> s;
    std::vector<float> density;

    std::vector<float> Vx;
    std::vector<float> Vy;

    std::vector<float> Vx0;
    std::vector<float> Vy0;
public:
    Fluid(float dt1, float diffusion, float viscosity)
    {
        size = N;
        dt = dt1;
        diff = diffusion;
        visc = viscosity;

        auto sz = N * N;
        s.reserve(sz);
        s.assign(sz, 0);

        density.reserve(sz);
        density.assign(sz, 0);

        Vx.reserve(sz);
        Vx.assign(sz, 0);
        Vy.reserve(sz);
        Vy.assign(sz, 0);

        Vx0.reserve(sz);
        Vx0.assign(sz, 0);
        Vy0.reserve(sz);
        Vy0.assign(sz, 0);
    }

    void renderD()
    {
        glPointSize(3);
        glBegin(GL_POINTS);
        for (int i = 0; i < N; i++)
        {
            for (int j = 0; j < N; j++)
            {
                float x = (float(i)/N) * SCALE;
                float y = (float(j)/N) * SCALE;
                auto d = density[IX(i, j)];
                d /= 255;
                glColor3f(d, d, d);
                //glColor3f(1, 1, 1);
                glVertex2d(x, y);
                //glVertex2d(1, 1);
            }
        }
        glEnd();
    }

    void fadeD()
    {
        for (int i = 0; i < density.size(); i++)
        {
            auto d = density[i];
            density[i] = std::clamp(d - 0.1, 0.0, 255.0);
        }
    }

    void step()
    {
        int N = this->size;
        diffuse(1, Vx0, Vx, visc, dt);
        diffuse(2, Vy0, Vy, visc, dt);

        project(Vx0, Vy0, Vx, Vy);

        advect(1, Vx, Vx0, Vx0, Vy0);
        advect(2, Vy, Vy0, Vx0, Vy0);

        project(Vx, Vy, Vx0, Vy0);

        diffuse(0, s, density, diff, dt);
        advect(0, density, s, Vx, Vy);
    }

    void AddDensity(int x, int y, float amount)
    {
        int index = IX(x, y);
        this->density[index] += amount;
    }

    void AddVelocity(int x, int y, float amountx, float amounty)
    {
        int index = IX(x, y);
        this->Vx[index] += amountx;
        this->Vy[index] += amounty;
    }

    void advect(int b, std::vector<float>& d, std::vector<float>& d0, std::vector<float>& velocX, std::vector<float>& velocY)
    {
        float i0, i1, j0, j1;

        float dtx = dt * (N - 2);
        float dty = dt * (N - 2);

        float s0, s1, t0, t1;
        float tmp1, tmp2, x, y;

        float Nfloat = N;
        float ifloat, jfloat, kfloat;

        int i, j;
        for (j = 1, jfloat = 1; j < N - 1; j++, jfloat++)
        {
            for (i = 1, ifloat = 1; i < N - 1; i++, ifloat++)
            {
                tmp1 = dtx * velocX[IX(i, j)];
                tmp2 = dty * velocY[IX(i, j)];

                x = ifloat - tmp1;
                y = jfloat - tmp2;

                if (x < 0.5) x = 0.5;
                if (x > Nfloat + 0.5) x = Nfloat + 0.5;

                i0 = floor(x);
                i1 = i0 + 1.0;
                if (y < 0.5) y = 0.5;
                if (y > Nfloat + 0.5) y = Nfloat + 0.5;
                j0 = floor(y);
                j1 = j0 + 1.0;

                s1 = x - i0;
                s0 = 1.0 - s1;
                t1 = y - j0;
                t0 = 1.0 - t1;

                int i0i = int(i0);
                int i1i = int(i1);
                int j0i = int(j0);
                int j1i = int(j1);

                d[IX(i, j)] =
                    s0 * (t0*d0[IX(i0i, j0i)] + t1 * d0[IX(i0i, j1i)])
                    + s1 * (t0*d0[IX(i1i, j0i)] + t1 * d0[IX(i1i, j1i)]);
            }
        }
        set_bnd(b, d);
    }
};//class end

void diffuse(int b, std::vector<float>& x, std::vector<float>& x0, float diff, float dt)
{
    float a = dt * diff * (N - 2)*(N - 2);
    lin_solve(b, x, x0, a, 1 + 6 * a);
}

void lin_solve(int b, std::vector<float>& x, std::vector<float>& x0, float a, float c)
{
    float cRecip = 1.0 / c;
    for (int k = 0; k < iter; k++)
    {
        for (int j = 1; j < N - 1; j++)
        {
            for (int i = 1; i < N - 1; i++)
            {
                x[IX(i, j)] =
                    (x0[IX(i, j)]
                        + a * (x[IX(i + 1, j)]
                            + x[IX(i - 1, j)]
                            + x[IX(i, j + 1)]
                            + x[IX(i, j - 1)]
                            ))*cRecip;
            }
        }
        set_bnd(b, x);
    }
}

void project(std::vector<float>& velocX, std::vector<float>& velocY, std::vector<float>& p, std::vector<float>& div)
{
    for (int j = 1; j < N - 1; j++)
    {
        for (int i = 1; i < N - 1; i++)
        {
            div[IX(i, j)] = -0.5*(
                velocX[IX(i + 1, j)]
                -velocX[IX(i - 1, j)]
                +velocX[IX(i, j + 1)]
                - velocX[IX(i, j-1)]
                ) / N;
            p[IX(i, j)] = 0;
        }
    }

    set_bnd(0, div);
    set_bnd(0, p);
    lin_solve(0, p, div, 1, 6);

    for (int j = 1; j < N - 1; j++)
    {
        for (int i = 1; i < N - 1; i++)
        {
            velocX[IX(i, j)] -= 0.5*(p[IX(i + 1, j)] - p[IX(i - 1, j)])* N;
            velocY[IX(i, j)] -= 0.5*(p[IX(i, j + 1)] - p[IX(i, j - 1)])* N;
        }
    }
    set_bnd(1, velocX);
    set_bnd(2, velocY);
}

void set_bnd(int b, std::vector<float>& x)
{
    for (int i = 1; i < N - 1; i++)
    {
        x[IX(i, 0)] = (b == 2) ? -x[IX(i, 1)] : x[IX(i, 1)];
        x[IX(i, N - 1)] = (b == 2) ? -x[IX(i, N - 2)] : x[IX(i, N - 2)];
    }

    for (int j = 1; j < N - 1; j++)
    {
        x[IX(0, j)] = (b == 1) ? -x[IX(1, j)] : x[IX(1, j)];
        x[IX(N - 1, j)] = (b == 1) ? -x[IX(N - 2, j)] : x[IX(N - 2, j)];
    }

    x[IX(0, 0)] = 0.5 *(x[IX(1, 0)]
        + x[IX(0, 1)]
        + x[IX(0, 0)]
        );
    x[IX(0, N - 1)] = 0.5 *(x[IX(1, N - 1)]
        + x[IX(0, N - 2)]
        + x[IX(0, N - 1)]
        );
    x[IX(N-1, 0)] = 0.5 *(x[IX(N-2, 0)]
        + x[IX(N-1, 1)]
        + x[IX(N-1, 0)]
        );
    x[IX(N - 1, N - 1)] = 0.5 *(x[IX(N - 2, N - 1)]
        + x[IX(N - 1, N - 2)]
        + x[IX(N - 1, N - 1)]
        );
}