﻿#include "../../../../99_AuxStuff/MyLibs/Utility/Util.hpp"
#include "../../../../99_AuxStuff/MyLibs/Utility/stackOverflow.h"
#include <gl\GL.h>
#include <gl\GLU.h>
#include <cmath>
#include <tuple>
#include <string>
#include "fluid3.h"

//STRICT to prevent explicit typecasting
#define STRICT

//constexpr UINT WIN_WIDTH = 1920;
//constexpr UINT WIN_HEIGHT = 1080;

//constexpr UINT WIN_WIDTH = N * 2 * SCALE;
//constexpr UINT WIN_HEIGHT = N * 2 * SCALE;

constexpr UINT WIN_WIDTH = N * SCALE;
constexpr UINT WIN_HEIGHT = N * SCALE;

static Fluid fluid0(0.1, 0, 0, util::ColorWheel::COLORS::INDIA_SAFRON);
static Fluid fluid1(0.1, 0, 0, util::ColorWheel::COLORS::WHITE);
static Fluid fluid2(0.1, 0, 0, util::ColorWheel::COLORS::INDIA_GREEN);

constexpr UINT WIN_X = 0;
constexpr UINT WIN_Y = 0;
UINT NUM_PTS = 100;
constexpr double CHAR_SPACING = 0.02;
constexpr double WORD_SPACING = 0;

class MyOglWindow : public util::OglWindow
{
    const double PI;
    // Inherited via OglWindow
    virtual const char * GetMessageMap() override
    {
        return nullptr;
    }
    virtual void Display() override
    {
        //glClear(GL_COLOR_BUFFER_BIT);//Dont uncomment
        //glColor3f(1, 0, 0);
        //DrawBigRectangle();
        //SwapBuffers(m_Hdc);
        //return;
        glPushMatrix();
        glTranslatef(GetMinX(), 0, 0);
        fluid0.AddDensity(2, 2, 750);
        fluid0.AddVelocity(2, 2, 0.5, 0.5);
        fluid0.step();
        fluid0.renderD();
        //fluid0.fadeD();
        glPopMatrix();
        
        glPushMatrix();
        glTranslatef(GetMinX()/2.0, 0, 0);
        fluid1.AddDensity(2, 2, 750);
        fluid1.AddVelocity(2, 2, 0, 0.5);
        fluid1.step();
        fluid1.renderD();
        //fluid1.fadeD();
        glPopMatrix();

        fluid2.AddDensity(N, N, 750);
        fluid2.AddVelocity(N, N, -0.5, 0.5);
        fluid2.step();
        fluid2.renderD();
        //fluid2.fadeD();

        SwapBuffers(m_Hdc);
        static int num = 0;
        saveScreenshotToFile(("screenshots\\" + std::to_string(++num) + ".tga"), WIN_WIDTH, WIN_HEIGHT);
        return;
    }

    void DrawBigRectangle()
    {
        glBegin(GL_QUADS);
        glVertex2f(-10, -10);
        glVertex2f(10, -10);
        glVertex2f(10, 10);
        glVertex2f(-10, 10);
        glEnd();
    }

    void DrawStaticIndiaText()
    {
        util::TextHelper th(WORD_SPACING, CHAR_SPACING, util::ColorWheel::COLORS::INDIA_SAFRON, util::ColorWheel::COLORS::INDIA_GREEN);
        //util::TextHelper th(WORD_SPACING, CHAR_SPACING, RGBA(0xFF, 0x99, 0x33, 255), RGBA(0x13, 0x88, 0x08, 255), 255);
        th.SetFontHeight(0.5);
        //std::wstring str{ L"INDIא" };
        std::wstring str{ L"גדהגו" };//Hebrew is RTL
        auto sszx = th.GetOnScreenSize(str);
        auto sszy = th.GetMaxHeightCellSize(str);
        double posX = -sszx / 2.0;
        double posY = sszy.cy / 2.0;
        th.WriteText(str, util::DataTypes::POINT3Dd{ posX, posY, 0 });
    }

    virtual void Resize(int width, int height) override
    {
        if (height == 0)
            height = 1;

        glViewport(0, 0, width, height);

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        if (width <= height)
        {
            auto factor = 1.0f*(((GLfloat)height) / ((GLfloat)width));
            glOrtho(-1.0f, 1.0f, -1 * factor, factor, -1.0f, 1.0f);
        }
        else
        {
            auto factor = 1.0f*(((GLfloat)width) / ((GLfloat)height));
            glOrtho(-1 * factor, factor, -1.0f, 1.0f, -1.0f, 1.0f);
        }

        glClearStencil(0);
        glClear(GL_COLOR_BUFFER_BIT | GL_STENCIL_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        //glEnable(GL_STENCIL_TEST);
        glColorMask(0,0,0,0);//disable writing to color buffer
        glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);
        glStencilMask(0xff);//turn on writing to stencil buffer
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        DrawStaticIndiaText();
        glStencilMask(0x00);//turn off writing to stencil buffer
        glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);//enable writing to color buffer
        glStencilFunc(GL_EQUAL, 0x1, 0x1);
        glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
        SwapBuffers(m_Hdc);
    }

    virtual LRESULT WndProcPre(HWND, UINT iMsg, WPARAM wParam, LPARAM lParam) override
    {
        switch (iMsg)
        {
        case WM_CHAR:
        {
            switch (wParam)
            {
            case 'f':
                //[[fallthrough]]
            case 'F':
                return LRESULT(-1);
                break;
            default:
                break;
            }
        }
        break;
        }
        return LRESULT();
    }

    virtual LRESULT WndProcPost(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) override
    {
        auto that = (OglWindow*)GetProp(hwnd, PARENT_OBJECT);

        switch (iMsg)
        {
        case WM_KEYDOWN:
        {
            if (VK_ESCAPE == wParam)
            {
                DestroyWindow(hwnd);
            }
        }
        break;
        }
        return LRESULT();
    }

public:
    MyOglWindow(TCHAR * className, UINT x, UINT y, UINT width, UINT height, HINSTANCE hInstance) : PI(4 * atan(1)), OglWindow(className, x, y, width, height, hInstance)
    {
    }

    // Inherited via OglWindow
    virtual void Update() override
    {
    }
};

int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
    TCHAR AppName[] = TEXT("Static INDIA");
    MyOglWindow myOglWnd(AppName, WIN_X, WIN_Y, WIN_WIDTH, WIN_HEIGHT, hInstance);
    myOglWnd.InitAndShowWindow(INIT_MASK_ENABLE_DEPTH);
    myOglWnd.EnableAlpha(true);
    myOglWnd.AllowOutOfFocusRender(true);
    //myOglWnd.ToggleFullscreeen();
    myOglWnd.RunGameLoop();
    return 0;
}