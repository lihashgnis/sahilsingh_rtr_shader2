#include <freeglut.h>
#include <fstream>
#include <vector>
#include <regex>
#include <cstdio>

using namespace std;

void display();
void initialize();
void loadData();
void processCmd(const string& pathElement);
void processData();
void flipY();
void translateY(long double Ytarns);
void translateX(long double Xtarns);
void fixCoordinates();

struct Point {
    long double x, y;
    Point()
    {
        x = y = 0;
    }

    Point operator *(long double scale)
    {
        Point p;
        p.x = x * scale;
        p.y = y * scale;
        return p;
    }
};

struct Color {
    BYTE r, g, b;
    
    Color()
    {
        r = g = b;
    }
};

struct Triangle {
    Point cord[3];
    Color color;
    Triangle operator * (long double scale)
    {
        Triangle t;
        
        t.cord[0] = cord[0] * scale;
        t.cord[1] = cord[1] * scale;
        t.cord[2] = cord[2] * scale;
        t.color = color;
        return t;
    }

    void sendToOgl()
    {
        for (auto p : cord)
        {
            glColor3ub(color.r, color.g, color.b);
            glVertex2f(p.x, p.y);
        }
    }

    void flipY()//since inkscape and Ogl have opposite directions for Y
    {
        for (auto& p : cord)
        {
            p.y *= -1;
        }
    }

    void translateY(long double Ytarns)
    {
        for (auto& p : cord)
        {
            p.y += Ytarns;
        }
    }

    void translateX(long double Xtarns)
    {
        for (auto& p : cord)
        {
            p.x += Xtarns;
        }
    }

    /*void fixY(long double scale)
    {
        for (auto& p : cord)
        {
            p.y *= scale;
        }
    }

    void fixX(long double scale)
    {
        for (auto& p : cord)
        {
            p.x *= scale;
        }
    }*/
    void operator *= (long double scale)
    {
        *this = (*this) * scale;
    }
};

constexpr unsigned int wSizeX = 1920, wSizeY = 1920; //window dimentions - 1920x1920, screen res - 3840x2160
constexpr unsigned int wPosX = 800, wPosY = 600; //initial window position
//constexpr char filePath[] = R"(C:\sahil\design\ironManLowPoly\ironManLowPolyAbsolutePathFullWithoutImage_parse.svg)";
constexpr char filePath[] = R"(ironManLowPolyAbsolutePathFullWithoutImage_parse.svg)";

constexpr long double inkscapeCordToPixelRatio = 3.7795277495195086372035848669523;
constexpr long double pixelCordToInkscapeRatio = 0.26458332;
constexpr long double pixelToOglRatio = 0.00129701686121919584954604409857;
//constexpr long double pixelToOglRatioX = 0.4015625 * 0.00129701686121919584954604409857 * wSizeX;
//constexpr long double pixelToOglRatioY = 0.4015625 * 0.00129701686121919584954604409857 * wSizeY;

float curPosX = 0, curPosY = 0, origX = 0, origY = 0;
vector<Triangle> triangles;

static float maxx = 0, maxy = 0, minx = 0, miny = 0;

vector<string> drawData;
bool dataLoadComplete = false;

int main(int argc, char* argv[])
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA);
    glutInitWindowSize(wSizeX, wSizeY);
    glutInitWindowPosition(wPosX, wPosY);
    glutCreateWindow("Iron Man Low Poly");
    initialize();

    glutDisplayFunc(display);
    glutMainLoop();
    return 0;
}

void display()
{
    if (!dataLoadComplete)
    {
        MessageBoxA(NULL, "Data not loaded", "Error", MB_OK);
        return;
    }
    //constexpr char regexPattern[] = R"###(.*?(d=".*?").*?(style=".*").*?)###";
    //regex re(regexPattern,regex_constants::icase);

    glClear(GL_COLOR_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glBegin(GL_TRIANGLES);
    for (Triangle t : triangles)
    {
        t.sendToOgl();
    }
    glEnd();
    glFlush();
}

void initialize()
{
    glClearColor(0.0, 0.0, 0.0, 1.0);
    loadData();
    processData();
    fixCoordinates();
    flipY();
    translateY(1);
    translateX(-0.5);
}

void fixCoordinates()
{
    for (auto& t : triangles)
    {
        t *= (inkscapeCordToPixelRatio * pixelToOglRatio);
       /* t.fixX(inkscapeCordToPixelRatio * pixelToOglRatioX);
        t.fixY(inkscapeCordToPixelRatio * pixelToOglRatioY);*/
    }
}

void flipY()
{
    for (auto& t : triangles)
    {
        t.flipY();
    }
}

void translateY(long double Ytarns)
{
    for (auto& t : triangles)
    {
        t.translateY(Ytarns);
    }
}

void translateX(long double Xtarns)
{
    for (auto& t : triangles)
    {
        t.translateX(Xtarns);
    }
}

void processData()
{
    for (const auto& path : drawData)
    {
        processCmd(path);
    }
}

auto getColor(const string& pathElement)
{
    const string parseStyleStr = R"(style="fill:#([0-9a-f]{6}).*?")";
    smatch styleMatches;
    if (regex_search(pathElement, styleMatches, regex(parseStyleStr, regex_constants::icase)))
    {
        return styleMatches[1].str();
    }
    {
        OutputDebugStringA("\ngetColor - doesn't match: ");
        OutputDebugStringA(pathElement.c_str());
        OutputDebugStringA("\n");
        return string("");
    }
}

void processCmd(const string& pathElement)
{
    string color = getColor(pathElement);
    
    if ("" == color)
    {
        OutputDebugString("\ncould't find color for path : ");
        OutputDebugString(pathElement.c_str());
        OutputDebugString("\n");
        return;
    }

    auto colorRed   = (BYTE)strtoul(color.substr(0, 2).c_str(), nullptr, 16);
    auto colorGreen = (BYTE)strtoul(color.substr(2, 2).c_str(), nullptr, 16);
    auto colorBlue  = (BYTE)strtoul(color.substr(4, 2).c_str(), nullptr, 16);

    const string parseCmdStr = R"(d="(M) ([0-9.,-]+) ([0-9.,-]+) ([0-9.,-]+) z)";
    smatch cmdMatches;
    if (regex_search(pathElement, cmdMatches, regex(parseCmdStr, regex_constants::icase)))
    {
        Point p[3];
        for (unsigned icord = 0; icord < 3; icord++)
        {
            auto cord = cmdMatches[2 + icord].str();
            float x, y;
            sscanf_s(cord.c_str(), "%f,%f", &x, &y);

            char cmd = cmdMatches[1].str()[0];
            switch (cmd)
            {
            case 'm':
            {
                curPosX += x;
                curPosY += y;
                MessageBoxA(NULL, "Not expecting a relative path command", "Error", MB_OK | MB_ICONERROR);

            }
            break;
            case 'M':
            {
                curPosX = x;
                curPosY = y;
            }
            break;
            default:
                break;
            }
            p[icord].x = curPosX;
            p[icord].y = curPosY;
            {
                maxx = (curPosX > maxx) ? curPosX : maxx;
                maxy = (curPosY > maxy) ? curPosY : maxy;
                minx = (curPosX < minx) ? curPosX : minx;
                miny = (curPosY < miny) ? curPosY : miny;
            }
        }
        {//store parsed data
            Triangle t;
            t.cord[0] = p[0];
            t.cord[1] = p[1];
            t.cord[2] = p[2];
            t.color.r = colorRed;
            t.color.g = colorGreen;
            t.color.b = colorBlue;
            triangles.push_back(t);
        }
    }
    else
    {
        OutputDebugStringA("\nprocessCmd - doesn't match: ");
        OutputDebugStringA(pathElement.c_str());
        OutputDebugStringA("\n");
    }
}
void loadData()
{
    ifstream ifs;
    ifs.open(filePath);
    
    if (ifs.fail())
    {
        MessageBoxA(NULL, filePath, "Error - failed to open file", MB_OK);
        return;
    }

    char buff[MAX_PATH];
    for (ifs.getline(buff, MAX_PATH); !ifs.fail();ifs.getline(buff, MAX_PATH))
    {
        drawData.push_back(buff);
    }

    dataLoadComplete = true;
}