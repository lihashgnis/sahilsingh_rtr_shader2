package in.lihas.AndroidPerspectiveTriangle_PP;

public class GLESMacros 
{
    public static final int AMC_ATTRIBUTE_POSITION      =   0;
    public static final int AMC_ATTRIBUTE_COLOR         =   1;
    public static final int AMC_ATTRIBUTE_NORMAL        =   2;
    public static final int AMC_ATTRIBUTE_TEXTCOORD0    =   3;
    public static final int AMC_ATTRIBUTE_TEXTCOORD1    =   4;
}