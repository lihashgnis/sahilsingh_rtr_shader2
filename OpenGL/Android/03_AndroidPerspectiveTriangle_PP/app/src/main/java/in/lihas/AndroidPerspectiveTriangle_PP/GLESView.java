package in.lihas.AndroidPerspectiveTriangle_PP;

//default includes
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

//imported by me
import android.content.Context;
import android.view.Gravity;
import android.view.MotionEvent;
import android.graphics.Color;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;
import android.os.SystemClock;
import java.lang.System;
import android.widget.Toast;
import android.content.Context;

//for openGL
import android.opengl.GLSurfaceView;
import android.opengl.GLES32;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

//For opengl buffer objects
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

//for matrix math
import android.opengl.Matrix;

//for reading shaders from asset manager
import android.content.res.AssetManager;
import java.io.IOException;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.StringReader;

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener{
    
    private GestureDetector gestureDetector;
    private final Context context;
    private final AssetManager assets;
    
    private int vertexShaderObject;
    private int fragmentShaderObject;
    private int shaderProgramObject;
    
    private int[] vao = new int[1];
    private int[] vbo = new int[1];
    
    private int mvp_uniform;
    private float[] perspectiveProjectionMatrix = new float[16];//4x4 matrix
    
    String versionString = new String("#version 320 es\n");
    
    public GLESView(Context drawingContext)
    {
        super(drawingContext);
        context = drawingContext;
        
        setEGLContextClientVersion(3);
        setRenderer(this);
        setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);
        
        gestureDetector = new GestureDetector(drawingContext, this, null, false);
        gestureDetector.setOnDoubleTapListener(this);
        
        assets = context.getAssets();
    }
    
    //implement GLSurfaceView.Renderer methods
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config)
    {
        String version = gl.glGetString(GL10.GL_VERSION);
        System.out.println("RTR: GL10.GL_VERSION " + version);
        version = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("RTR: GLES32.GL_SHADING_LANGUAGE_VERSION " + version);
        initialize();
    }
    
    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height)
    {
        resize(width, height);
    }
    
    @Override
    public void onDrawFrame(GL10 gl)
    {
        display();
    }
    
    
    //custom methods
    
    /// @brief Read the contents of a file present in assets folder to a string
    private String ReadAssetFileAsString(String fileName)
    {
        
        //asset tutorial - https://www.javacodegeeks.com/2012/02/android-read-file-from-assets.html
        String fileStr = null;
        try
        {
            InputStream fileIO = assets.open(fileName);
            int bytesAvailable = fileIO.available();
            byte[] byteBuffer = new byte[bytesAvailable];
            fileIO.read(byteBuffer, 0 , bytesAvailable);
            fileIO.close();
            fileStr = new String(byteBuffer);
            System.out.println("RTR: opening file " + fileName + " file size(bytes): " + bytesAvailable);
        }
        catch(IOException ex)
        {
            System.out.println("RTR: ReadAssetFileAsString() failed");
        }
        return fileStr;
    }
    
    /// @brief load shader code from assets folder
    private String LoadShaderCodeAsset(String fileName)
    {
        String code = versionString;
        
        String fileStr = ReadAssetFileAsString(fileName);
        
        //Remove C++ raw string delimiters if present
        BufferedReader bufRead = new BufferedReader(new StringReader(fileStr));
        
        try
        {
            String line = bufRead.readLine();
            while(null != line)
            {
                if(!line.contentEquals("R\"ptD7mQ2vrP8s(") && !line.contentEquals(")ptD7mQ2vrP8s") && !line.contentEquals(")ptD7mQ2vrP8s\";"))
                {
                    code += line;
                    code += "\n";
                }
                line = bufRead.readLine();
            }
        }
        catch(IOException ex)
        {
            System.out.println("RTR: LoadShaderCodeAsset() failed");
        }
        return code;
    }
    
    private void initialize()
    {
        GLES32.glClearColor(0.0f,0.0f,1.0f,1.0f);
        
        //vertex shader
        {
            vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
            final String vertexShaderSourceCode = LoadShaderCodeAsset("VertexShaders/{1D19A7B9-B345-491E-B7D7-E41FDFF697AA}.vert");
            System.out.println("RTR: vertex shader code being used: " + vertexShaderSourceCode);
            
            GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
            GLES32.glCompileShader(vertexShaderObject);
            
            //check compile status
            int[] iShaderCompileStatus = new int[1];
            int[] iInfoLogLength       = new int[1];
            String szInfoLog = null;
            iShaderCompileStatus[0] = 0;
            iInfoLogLength[0]       = 0;
            GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);
            if(GLES32.GL_FALSE == iShaderCompileStatus[0])
            {
                GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
                if(iInfoLogLength[0] > 0)
                {
                    //we have some log
                    //Toast.makeText(context,"Vertex Shader Compilation Failed", Toast.LENGTH_LONG).show();
                    szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
                    System.out.println("RTR: Vertex Shader Compilation Failed " + szInfoLog);
                    uninitialize();
                }
            }
        }
        
        //fragment shader
        {
            fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
            final String fragmentShaderSourceCode = LoadShaderCodeAsset("FragmentShaders/{C2038141-65DA-4D11-8351-304132D36416}.frag");
            System.out.println("RTR: fragment shader code being used: " + fragmentShaderSourceCode);
            
            GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
            GLES32.glCompileShader(fragmentShaderObject);
            
            //check compile status
            int[] iShaderCompileStatus = new int[1];
            int[] iInfoLogLength       = new int[1];
            String szInfoLog = null;
            iShaderCompileStatus[0] = 0;
            iInfoLogLength[0]       = 0;
            GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);
            if(GLES32.GL_FALSE == iShaderCompileStatus[0])
            {
                GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
                if(iInfoLogLength[0] > 0)
                {
                    //we have some log
                    //Toast.makeText(context,"Fragment Shader Compilation Failed", Toast.LENGTH_LONG).show();
                    szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
                    System.out.println("RTR: Fragment Shader Compilation Failed " + szInfoLog);
                    uninitialize();
                }
            }
        }
        
        //shader program object
        {
            shaderProgramObject = GLES32.glCreateProgram();
            GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
            GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);
            //pre link - bind to attributes
            {
                GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_POSITION, "vPosition");
            }
            
            GLES32.glLinkProgram(shaderProgramObject);
            //check for linking error
            int[] iProgLinkStatus = new int[1];
            int[] iInfoLogLength = new int[1];
            String szInfoLog = null;
            GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iProgLinkStatus, 0);
            if(GLES32.GL_FALSE == iProgLinkStatus[0])
            {
                GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
                if(iInfoLogLength[0] > 0)
                {
                    szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
                    //Toast.makeText(context,"Shader link Failed", Toast.LENGTH_LONG).show();
                    System.out.println("RTR: Shader link Failed " + szInfoLog);
                    uninitialize();
                }
            }
           
            
            //post link - uniforms
            {
                mvp_uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");
                final float[] triangleVertices = new float[] {
                    0.0f,   1.0f  , 0.0f,
                   -1.0f, -1.0f  , 0.0f,
                    1.0f, -1.0f  , 0.0f
                };
                
                //create vao
                GLES32.glGenVertexArrays(1, vao, 0);
                GLES32.glBindVertexArray(vao[0]);
                GLES32.glGenBuffers(1, vbo, 0);
                GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo[0]);
                
                //create a buffer which can be passed to glBufferData(); -> 5 steps
                //step 1 -> allocate native memory buffer
                ByteBuffer byteBuffer = ByteBuffer.allocateDirect(triangleVertices.length*4);
                //step 2 -> set byte order - endianess
                byteBuffer.order(ByteOrder.nativeOrder());
                //step3 - create float buffer, and convert out buffer into it
                FloatBuffer positionBuffer = byteBuffer.asFloatBuffer();
                //step 4 - put array data into this "cooked" buffer
                positionBuffer.put(triangleVertices);
                //step 5 - set array at 0th position
                //Not imp for out code, but will help when using interleaved array
                positionBuffer.position(0);
                GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, triangleVertices.length*4, positionBuffer, GLES32.GL_STATIC_DRAW);
                GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false,0,0);
                GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
                GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
                GLES32.glBindVertexArray(0);
                
                //Depth 
                GLES32.glClearDepthf(1.0f);
                //GLES32.glEnable(GLES32.GL_DEPTH_TEST);
                GLES32.glDepthFunc(GLES32.GL_LEQUAL);               
            }
        }
        Matrix.setIdentityM(perspectiveProjectionMatrix, 0);
    }
    
    private void uninitialize()
    {
        
    }
    
    private void exit()
    {
        uninitialize();
        System.exit(0);   
    }
    
    private void resize(int width, int height)
    {
        if(height <= 0)
        {
            height = 1;
        }
        GLES32.glViewport(0, 0, width, height);
        
        //m_orthoProjectionMatrix = ortho();
        float factor = ((float)width) / ((float)height);
        Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, factor, 0.1f, 100.0f);
        
    }
    
    private void display()
    {
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
        float[] modelViewMatrix = new float[16];
        float[] modelViewProjectionMatrix = new float[16];
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.translateM(modelViewMatrix, 0, 0.0f, 0.0f, -3.0f);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);
        Matrix.multiplyMM(modelViewProjectionMatrix, 0, perspectiveProjectionMatrix, 0, modelViewMatrix, 0);
        GLES32.glUseProgram(shaderProgramObject);
        GLES32.glUniformMatrix4fv(mvp_uniform, 1, false, modelViewProjectionMatrix, 0);
        System.out.println("RTR: display modelViewProjectionMatrix  " + java.util.Arrays.toString(modelViewProjectionMatrix));
        GLES32.glBindVertexArray(vao[0]);
        GLES32.glDrawArrays(GLES32.GL_TRIANGLES, 0, 3);
        requestRender();
        GLES32.glUseProgram(0);
    }
    
    //handling onTouchEvent is important, since it triggers all gesture and tap events
    @Override public boolean onTouchEvent(MotionEvent event)
    {
        //code
        int eventaction = event.getAction();//will not be used in any of our projects
        if(!gestureDetector.onTouchEvent(event))
        {
            super.onTouchEvent(event);
        }
        return true;
    }
    
    @Override public boolean onDoubleTap(MotionEvent e)
    {
        
        return true;
    }
    
    @Override public boolean onDoubleTapEvent(MotionEvent e)
    {
        return true;
    }
    
    @Override public boolean onSingleTapConfirmed(MotionEvent e)
    {
        
        return true;
    }
    
    @Override public boolean onDown(MotionEvent e)
    {
        return true;
    }
    
    @Override public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
    {
        return true;
    }
    
    @Override public void onLongPress(MotionEvent e)
    {
        
    }
    
    @Override public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
    {
        exit();
        return true;
    }
    
    @Override public void onShowPress(MotionEvent e)
    {
        
    }
    
    @Override public boolean onSingleTapUp(MotionEvent e)
    {
        return true;
    }
}