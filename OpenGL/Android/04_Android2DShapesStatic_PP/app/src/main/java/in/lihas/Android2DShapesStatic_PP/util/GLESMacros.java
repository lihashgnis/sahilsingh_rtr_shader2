package util;

//public class GLESMacros 
//{
//    public static final int AMC_ATTRIBUTE_POSITION      =   0;
//    public static final int AMC_ATTRIBUTE_COLOR         =   1;
//    public static final int AMC_ATTRIBUTE_NORMAL        =   2;
//    public static final int AMC_ATTRIBUTE_TEXTCOORD0    =   3;
//    public static final int AMC_ATTRIBUTE_TEXTCOORD1    =   4;
//}

//user ordinal() to get int value - https://stackoverflow.com/a/13792132/981766
public enum GLESMacros
{
    AMC_ATTRIBUTE_POSITION  ,
    AMC_ATTRIBUTE_COLOR     ,
    AMC_ATTRIBUTE_NORMAL    ,
    AMC_ATTRIBUTE_TEXTCOORD0,
    AMC_ATTRIBUTE_TEXTCOORD1
}