package util;

//for openGL
import android.opengl.GLES32;

//For opengl buffer objects
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import java.lang.Exception;

import java.util.*;

public class ShaderCodesContainer
{
    static Map<Integer, objectInfo> m_objectInfoMap = new HashMap<Integer, objectInfo>();//object ID with objectInfo 
    public Map<String, Integer> m_uniformLoc = new HashMap<String, Integer>();
    String m_vertexShaderCode;
    String m_fragmentShaderCode;
    
    private int m_vertexShaderObject;
    private int m_fragmentShaderObject;
    private int m_shaderProgramObject;
    
    public int GetVao(Integer objectID)
    {
        objectInfo oi = m_objectInfoMap.get(objectID);
        return oi.vao[0];
    }
    
    public int GetShaderPorgramObject()
    {
        return m_shaderProgramObject;
    }
    
    public int GetUniformLocation(String uniformName)
    {
        return m_uniformLoc.get(uniformName);
    }
    
    public void InitShaders() throws Exception
    {
        if((m_vertexShaderCode.length() == 0) || (m_fragmentShaderCode.length() == 0))
        {
            util.throw_error("vertex or fragment shader code missing");
        }
        
        //vertex shader
        {
            m_vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
            
            GLES32.glShaderSource(m_vertexShaderObject, m_vertexShaderCode);
            GLES32.glCompileShader(m_vertexShaderObject);
            
            //check compile status
            int[] iShaderCompileStatus = new int[1];
            int[] iInfoLogLength       = new int[1];
            String szInfoLog = null;
            iShaderCompileStatus[0] = 0;
            iInfoLogLength[0]       = 0;
            GLES32.glGetShaderiv(m_vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);
            if(GLES32.GL_FALSE == iShaderCompileStatus[0])
            {
                GLES32.glGetShaderiv(m_vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
                if(iInfoLogLength[0] > 0)
                {
                    //we have some log
                    //Toast.makeText(context,"Vertex Shader Compilation Failed", Toast.LENGTH_LONG).show();
                    szInfoLog = GLES32.glGetShaderInfoLog(m_vertexShaderObject);
                    util.throw_error("RTR: Vertex Shader Compilation Failed " + szInfoLog);
                    return;
                }
            }
        }//vertex shader end
        
        //fragment shader
        {
            m_fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
            
            GLES32.glShaderSource(m_fragmentShaderObject, m_fragmentShaderCode);
            GLES32.glCompileShader(m_fragmentShaderObject);
            
            //check compile status
            int[] iShaderCompileStatus = new int[1];
            int[] iInfoLogLength       = new int[1];
            String szInfoLog = null;
            iShaderCompileStatus[0] = 0;
            iInfoLogLength[0]       = 0;
            GLES32.glGetShaderiv(m_fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);
            if(GLES32.GL_FALSE == iShaderCompileStatus[0])
            {
                GLES32.glGetShaderiv(m_fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
                if(iInfoLogLength[0] > 0)
                {
                    //we have some log
                    //Toast.makeText(context,"Fragment Shader Compilation Failed", Toast.LENGTH_LONG).show();
                    szInfoLog = GLES32.glGetShaderInfoLog(m_fragmentShaderObject);
                    util.throw_error("RTR: Fragment Shader Compilation Failed " + szInfoLog);
                }
            }
        }//fragment shader
        
        //shader program object
        {
            m_shaderProgramObject = GLES32.glCreateProgram();
            GLES32.glAttachShader(m_shaderProgramObject, m_vertexShaderObject);
            GLES32.glAttachShader(m_shaderProgramObject, m_fragmentShaderObject);
            //pre link - bind to attributes
            {
                for(Map.Entry<Integer, objectInfo> entry : m_objectInfoMap.entrySet())
                {
                    Integer objID = entry.getKey();
                    objectInfo objInfo = entry.getValue();
                    Vector<VertexAttributeStruct> attribs = objInfo.m_attribs;
                    Integer numAttribs = attribs.size();
                    
                    //create vao
                    int[] vao = new int[1];
                    int[] vbo = new int[numAttribs];
                    GLES32.glGenVertexArrays(1, vao, 0);
                    GLES32.glBindVertexArray(vao[0]);
                    GLES32.glGenBuffers(numAttribs, vbo, 0);
                    
                    objInfo.vao[0] = vao[0];
                    
                    int iAttrib = -1;
                    for(VertexAttributeStruct attr : attribs)
                    {
                        iAttrib += 1;
                        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo[iAttrib]);
                        
                        //create a buffer which can be passed to glBufferData(); -> 5 steps
                        //step 1 -> allocate native memory buffer
                        //ByteBuffer byteBuffer = ByteBuffer.allocateDirect(triangleVertices.length*4);
                        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(attr.m_dataf.size()*4);
                        //step 2 -> set byte order - endianess
                        byteBuffer.order(ByteOrder.nativeOrder());
                        //step3 - create float buffer, and convert out buffer into it
                        FloatBuffer positionBuffer = byteBuffer.asFloatBuffer();
                        //step 4 - put array data into this "cooked" buffer
                        //positionBuffer.put(triangleVertices);
                        positionBuffer.put(attr.GetData());
                        //step 5 - set array at 0th position
                        //Not imp for out code, but will help when using interleaved array
                        positionBuffer.position(0);
                        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, attr.m_dataf.size()*4, positionBuffer, GLES32.GL_STATIC_DRAW);
                        //GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false,0,0);
                        GLES32.glVertexAttribPointer(attr.m_type.ordinal(), 3, GLES32.GL_FLOAT, false,0,0);
                        GLES32.glEnableVertexAttribArray(attr.m_type.ordinal());
                        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
                        
                        attr.vbo[0] = vbo[iAttrib];
                    }
                    GLES32.glBindVertexArray(0);
                }
            }
            
            GLES32.glLinkProgram(m_shaderProgramObject);
            //check for linking error
            int[] iProgLinkStatus = new int[1];
            int[] iInfoLogLength = new int[1];
            String szInfoLog = null;
            GLES32.glGetProgramiv(m_shaderProgramObject, GLES32.GL_LINK_STATUS, iProgLinkStatus, 0);
            if(GLES32.GL_FALSE == iProgLinkStatus[0])
            {
                GLES32.glGetProgramiv(m_shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
                if(iInfoLogLength[0] > 0)
                {
                    szInfoLog = GLES32.glGetProgramInfoLog(m_shaderProgramObject);
                    //Toast.makeText(context,"Shader link Failed", Toast.LENGTH_LONG).show();
                    System.out.println("RTR: Shader link Failed " + szInfoLog);
                }
            }
           
            
            //post link - uniforms
            {
                for(String uniName : m_uniformLoc.keySet())
                {
                    int uniformLoc = GLES32.glGetUniformLocation(m_shaderProgramObject, uniName);
                    m_uniformLoc.put(uniName, uniformLoc);
                    
                }
                
                //Depth 
                GLES32.glClearDepthf(1.0f);
                //GLES32.glEnable(GLES32.GL_DEPTH_TEST);
                GLES32.glDepthFunc(GLES32.GL_LEQUAL);               
            }
        }//shader program object END
    }
    
    
    
    public void SetVertexShaderCode(String shaderCode) throws Exception
    {
        m_vertexShaderCode = shaderCode;
        
        if(m_vertexShaderCode.length() == 0)
        {
            util.throw_error("vertex shader code given is empty");
        }
    }
    
    public void SetFragmentShaderCode(String shaderCode) throws Exception
    {
        m_fragmentShaderCode = shaderCode;
        
        if(m_fragmentShaderCode.length() == 0)
        {
            util.throw_error("fragment shader code given is empty");
        }
    }
    
    public void AddUniform(String shaderVariableName) throws Exception
    {
        //sanity checks
        //code of vertex shader should be set first
        if(m_vertexShaderCode.length() == 0)
        {
            String errStr = "vertex shader code must be supplied beforehand";
            System.out.println("RTR:[ERROR]: " + errStr);
            throw new Exception(errStr);
        }
        
        if(m_vertexShaderCode.indexOf(shaderVariableName) == -1)
        {
            String errStr = "shader variable must exist in vertex shader code";
            System.out.println("RTR:[ERROR]: " + errStr);
            throw new Exception(errStr);
        }
        
        m_uniformLoc.put(shaderVariableName, -1);//real location will be set during init shader
    }
    
    public void AddAttribute(Integer objectID, GLESMacros attrType, String shaderVariableName, Vector<Float> dataf, int stride, int components) throws Exception
    {
        //sanity checks
        //code of vertex shader should be set first
        if(m_vertexShaderCode.length() == 0)
        {
            String errStr = "vertex shader code must be supplied beforehand";
            System.out.println("RTR:[ERROR]: " + errStr);
            throw new Exception(errStr);
        }
        
        if(m_vertexShaderCode.indexOf(shaderVariableName) == -1)
        {
            String errStr = "shader variable must exist in vertex shader code";
            System.out.println("RTR:[ERROR]: " + errStr);
            throw new Exception(errStr);
        }
        
        /*
         * sanity check 2 - if we have n components per vertext. 
         * eg. 3 for position - x, y, z
         * eg. 4 for color - RGBA
         * Thus the number of elements in dataf should be an integral multiple
         * of number of components
         */
        if(dataf.size() % components != 0)
        {
            String errStr = "dataf.size() % components != 0. dataf.size(): " + String.valueOf(dataf.size()) + "components: " + String.valueOf(components);
            System.out.println("RTR:[ERROR]: " + errStr);
            throw new Exception(errStr);
        }
        
        VertexAttributeStruct v = new VertexAttributeStruct();
        v.m_type = attrType;
        v.m_shaderVariableName = shaderVariableName;
        v.m_dataf = dataf;
        v.stride = stride;
        v.components = components;
        v.vbo[0] = -1;//will be assigned during shader compilation and linking
        
        objectInfo objInfo = m_objectInfoMap.get(objectID);
        if(null == objInfo)
        {
            objInfo = new objectInfo();
            objInfo.objectID = objectID;
            objInfo.vao[0] = -1;
        }
        
        objInfo.m_attribs.add(v);
        m_objectInfoMap.put(objectID, objInfo);
        
        
    }
}