package util;

import java.util.Vector;

public class VertexAttributeStruct
{
    GLESMacros m_type;//one of GLESMacros
    Vector<Float> m_dataf;//using java type parameters - https://stackoverflow.com/a/11697379/981766
    String m_shaderVariableName;//Which shader variable does this attribute get bound to
    int stride;
    /*
     * components per attribute eg. for position each position attribute will have 3 components - x,y, and z.
     * for RGBA color color attribute of each vertex will have 4 components
     */
    int components;
    int[] vbo;
    
    public float[] GetData()
    {
        float[] floatArr = new float[m_dataf.size()];
        
        int index = -1;
        for(Float val: m_dataf)
        {
            index += 1;
            floatArr[index] = val;
        }
        
        return floatArr;
    }
    
    VertexAttributeStruct()
    {
        m_type = GLESMacros.AMC_ATTRIBUTE_POSITION;//default value
        m_dataf = new Vector<Float>();
        m_shaderVariableName = new String();
        stride = -1;
        components = -1;
        vbo = new int[1];
        vbo[0] = -1;
    }
}