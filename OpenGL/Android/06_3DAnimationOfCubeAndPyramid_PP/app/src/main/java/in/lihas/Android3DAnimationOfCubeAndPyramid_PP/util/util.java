package util;

import java.lang.Exception;

public class util
{
    public static void log_error(String errStr)
    {
        System.out.println("RTR:[ERROR]: " + errStr);
    }
    
    public static void throw_error(String errStr) throws Exception
    {
        log_error("RTR:[ERROR]: " + errStr);
        throw new Exception(errStr);
    }
}