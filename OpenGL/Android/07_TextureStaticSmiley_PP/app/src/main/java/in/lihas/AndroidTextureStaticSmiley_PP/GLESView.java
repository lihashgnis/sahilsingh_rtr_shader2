package in.lihas.AndroidTextureStaticSmiley_PP;

//default includes
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

//imported by me
import android.content.Context;
import android.view.Gravity;
import android.view.MotionEvent;
import android.graphics.Color;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;
import android.os.SystemClock;
import java.lang.System;
import android.widget.Toast;
import android.content.Context;

//for openGL
import android.opengl.GLSurfaceView;
import android.opengl.GLES32;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

//For opengl buffer objects
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

//for matrix math
import android.opengl.Matrix;

//for reading shaders from asset manager
import android.content.res.AssetManager;
import java.io.IOException;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.StringReader;

//Vector, etc.
import java.util.*;

//my util
import util.*;



public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener{
    
    private GestureDetector gestureDetector;
    private final Context context;
    private final AssetManager assets;
    private ShaderCodesContainer m_shaderCodes = new ShaderCodesContainer();
    
    String UNIFORM_MVP_SHADER_NAME = new String("u_mvp_matrix");
    String UNIFORM_TEXTURE_SAMPLER = new String("u_sampler");
    
    private int[] texture_smiley = new int[1];
    
    //Generated using INIT_MASK_DUMP_ATTRIBUTES in corresponding C++ program
    //Also see util::QuadToTriangle()
    private final Float[] textureCoordsArr = new Float[] {
    0.0f,0.0f,
    1.0f,0.0f,
    1.0f,1.0f,
    0.0f,1.0f
};

private final Float[] rectangleCoordsArr = new Float[]{
    -1.0f,-1.0f, 0.0f,
     1.0f,-1.0f, 0.0f,
     1.0f, 1.0f, 0.0f,
    -1.0f, 1.0f, 0.0f
};
                
    private final Vector<Float> textureCoords = new Vector<Float> (Arrays.asList(textureCoordsArr));
    private final Vector<Float> rectangleCoords = new Vector<Float> (Arrays.asList(rectangleCoordsArr));
    
    private Integer rectangleID = 0;
    
    private int mvp_uniform;
    private float[] perspectiveProjectionMatrix = new float[16];//4x4 matrix
    
    String versionString = new String("#version 320 es\n");
    
    public GLESView(Context drawingContext)
    {
        super(drawingContext);
        context = drawingContext;
        
        setEGLContextClientVersion(3);
        setRenderer(this);
        setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);
        
        gestureDetector = new GestureDetector(drawingContext, this, null, false);
        gestureDetector.setOnDoubleTapListener(this);
        
        assets = context.getAssets();
    }
    
    //implement GLSurfaceView.Renderer methods
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config)
    {
        String version = gl.glGetString(GL10.GL_VERSION);
        System.out.println("RTR: GL10.GL_VERSION " + version);
        version = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("RTR: GLES32.GL_SHADING_LANGUAGE_VERSION " + version);
        initialize();
    }
    
    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height)
    {
        resize(width, height);
    }
    
    @Override
    public void onDrawFrame(GL10 gl)
    {
        display();
    }
    
    
    //custom methods
    
    /// @brief Read the contents of a file present in assets folder to a string
    private String ReadAssetFileAsString(String fileName)
    {
        
        //asset tutorial - https://www.javacodegeeks.com/2012/02/android-read-file-from-assets.html
        String fileStr = null;
        try
        {
            InputStream fileIO = assets.open(fileName);
            int bytesAvailable = fileIO.available();
            byte[] byteBuffer = new byte[bytesAvailable];
            fileIO.read(byteBuffer, 0 , bytesAvailable);
            fileIO.close();
            fileStr = new String(byteBuffer);
            System.out.println("RTR: opening file " + fileName + " file size(bytes): " + bytesAvailable);
        }
        catch(IOException ex)
        {
            System.out.println("RTR: ReadAssetFileAsString() failed");
            uninitialize();
        }
        return fileStr;
    }
    
    /// @brief load shader code from assets folder
    private String LoadShaderCodeAsset(String fileName)
    {
        String code = versionString;
        
        String fileStr = ReadAssetFileAsString(fileName);
        
        //Remove C++ raw string delimiters if present
        BufferedReader bufRead = new BufferedReader(new StringReader(fileStr));
        
        try
        {
            String line = bufRead.readLine();
            while(null != line)
            {
                if(!line.contentEquals("R\"ptD7mQ2vrP8s(") && !line.contentEquals(")ptD7mQ2vrP8s") && !line.contentEquals(")ptD7mQ2vrP8s\";"))
                {
                    code += line;
                    code += "\n";
                }
                line = bufRead.readLine();
            }
        }
        catch(IOException ex)
        {
            System.out.println("RTR: LoadShaderCodeAsset() failed");
        }
        return code;
    }
    
    private void initialize()
    {
        GLES32.glClearColor(0.0f,0.0f,1.0f,1.0f);
        GLES32.glClearDepthf(1.0f);
        GLES32.glEnable(GLES32.GL_DEPTH_TEST);
        GLES32.glDepthFunc(GLES32.GL_LEQUAL);
        
        final String vertexShaderSourceCode = LoadShaderCodeAsset("VertexShaders/{12514A14-D0B9-4C23-92A8-06991E03A37D}.vert");
        System.out.println("RTR: vertex shader code being used: " + vertexShaderSourceCode);
        final String fragmentShaderSourceCode = LoadShaderCodeAsset("FragmentShaders/{36599102-BFFA-4413-9859-51F1513D832D}.frag");
        System.out.println("RTR: fragment shader code being used: " + fragmentShaderSourceCode);
         
try
{         
        m_shaderCodes.SetVertexShaderCode(vertexShaderSourceCode);
        m_shaderCodes.SetFragmentShaderCode(fragmentShaderSourceCode);
        
        /*Rectangle*/
        Vector<Float> rectangleCoordsTesselated = util.QuadToTriangle(rectangleCoords, 3);
        m_shaderCodes.AddAttribute(rectangleID, GLESMacros.AMC_ATTRIBUTE_POSITION, "vPosition", rectangleCoordsTesselated, 0, 3);
        
        Vector<Float> textureCoordsTesselated = util.QuadToTriangle(textureCoords, 2);
        m_shaderCodes.AddAttribute(rectangleID, GLESMacros.AMC_ATTRIBUTE_TEXTCOORD0, "vTexCoord", textureCoordsTesselated, 0, 2);
        
        texture_smiley[0] = util.LoadTexture(context, R.raw.smiley_512x512_flip);
        
        m_shaderCodes.AddUniform(UNIFORM_MVP_SHADER_NAME);
        m_shaderCodes.AddUniform(UNIFORM_TEXTURE_SAMPLER);
        m_shaderCodes.InitShaders();
        
        Matrix.setIdentityM(perspectiveProjectionMatrix, 0);
}
catch(Exception ex)
{
    util.log_error("Exception caught! " + ex.getMessage());
}
    }
    
    private void uninitialize()
    {
        
    }
    
    private void exit()
    {
        uninitialize();
        System.exit(0);   
    }
    
    private void resize(int width, int height)
    {
        if(height <= 0)
        {
            height = 1;
        }
        GLES32.glViewport(0, 0, width, height);
        
        //m_orthoProjectionMatrix = ortho();
        float factor = ((float)width) / ((float)height);
        Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, factor, 0.1f, 100.0f);
        
    }
    
    private void display()
    {
        
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
        float[] modelViewMatrix = new float[16];
        float[] modelViewProjectionMatrix = new float[16];
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.translateM(modelViewMatrix, 0, 0.0f, 0.0f, -3.0f);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);
        Matrix.multiplyMM(modelViewProjectionMatrix, 0, perspectiveProjectionMatrix, 0, modelViewMatrix, 0);
        GLES32.glUseProgram(m_shaderCodes.GetShaderPorgramObject());
        GLES32.glUniformMatrix4fv(m_shaderCodes.GetUniformLocation(UNIFORM_MVP_SHADER_NAME), 1, false, modelViewProjectionMatrix, 0);
        
        /*RECTANGLE*/
        GLES32.glBindVertexArray(m_shaderCodes.GetVao(rectangleID));
        
        try
        {
            GLES32.glActiveTexture(GLES32.GL_TEXTURE0);
            GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_smiley[0]);
            
            //connect to texture unit 0
            GLES32.glUniform1i(m_shaderCodes.GetUniformLocation(UNIFORM_TEXTURE_SAMPLER), 0);
        
            int numVertices = m_shaderCodes.GetAttribute(rectangleID, GLESMacros.AMC_ATTRIBUTE_POSITION).numVertices;
            GLES32.glDrawArrays(GLES32.GL_TRIANGLES, 0, numVertices);
        }
        catch(Exception ex)
        {
            util.log_error("Exception caught! " + ex.getMessage());
        }
        
        requestRender();
        GLES32.glUseProgram(0);
    }
    
    //handling onTouchEvent is important, since it triggers all gesture and tap events
    @Override public boolean onTouchEvent(MotionEvent event)
    {
        //code
        int eventaction = event.getAction();//will not be used in any of our projects
        if(!gestureDetector.onTouchEvent(event))
        {
            super.onTouchEvent(event);
        }
        return true;
    }
    
    @Override public boolean onDoubleTap(MotionEvent e)
    {
        
        return true;
    }
    
    @Override public boolean onDoubleTapEvent(MotionEvent e)
    {
        return true;
    }
    
    @Override public boolean onSingleTapConfirmed(MotionEvent e)
    {
        
        return true;
    }
    
    @Override public boolean onDown(MotionEvent e)
    {
        return true;
    }
    
    @Override public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
    {
        return true;
    }
    
    @Override public void onLongPress(MotionEvent e)
    {
        
    }
    
    @Override public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
    {
        exit();
        return true;
    }
    
    @Override public void onShowPress(MotionEvent e)
    {
        
    }
    
    @Override public boolean onSingleTapUp(MotionEvent e)
    {
        return true;
    }
}