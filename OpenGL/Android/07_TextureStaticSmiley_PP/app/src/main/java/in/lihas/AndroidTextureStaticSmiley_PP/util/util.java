package util;

import java.lang.Exception;

//textures
import android.graphics.BitmapFactory;
import android.graphics.Bitmap;
import android.opengl.GLUtils;
import android.content.Context;
import android.opengl.GLES32;

//map
import java.util.*;

public class util
{
    private static Map<Integer, Integer> m_textureCache = new HashMap<Integer, Integer>();//object ID with objectInfo 
    
    public static void log_error(String errStr)
    {
        System.out.println("RTR:[ERROR]: " + errStr);
    }
    
    public static void throw_error(String errStr) throws Exception
    {
        log_error("RTR:[ERROR]: " + errStr);
        throw new Exception(errStr);
    }
    
    public static int LoadTexture(final Context context, int imageFileResourceID)
    {
        Integer textureInteger = m_textureCache.get(new Integer(imageFileResourceID));
        
        //check if already in cache
        if(null != textureInteger)
        {
            return textureInteger.intValue();
        }
        
        int[] texture = new int[1];
        
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;//no prescaling 
        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), imageFileResourceID, options);
        
        GLES32.glPixelStorei(GLES32.GL_UNPACK_ALIGNMENT, 4);
        GLES32.glGenTextures(1, texture, 0);
        GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture[0]);
        
        GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_WRAP_S, GLES32.GL_REPEAT);
        GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_WRAP_T, GLES32.GL_REPEAT);
        
        GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MAG_FILTER, GLES32.GL_LINEAR);
        GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MIN_FILTER, GLES32.GL_LINEAR_MIPMAP_LINEAR);
        
        GLUtils.texImage2D(GLES32.GL_TEXTURE_2D, 0, bitmap, 0);
        
        m_textureCache.put(new Integer(imageFileResourceID), new Integer(texture[0]));
        
        
        bitmap.recycle();//https://www.learnopengles.com/android-lesson-four-introducing-basic-texturing/
        return texture[0];
    }
    
    /*GL_QUADS is depricated
* Convert vertices which represent a Quad
*/
public static Vector<Float> QuadToTriangle(Vector<Float> QuadVertices, int dimensions) throws Exception
{
    //Each quad will have 4 vertices
    //Each vertex will have 3 values - x, y, and z
    if (QuadVertices.size() % (dimensions * 4) != 0)
    {
        throw_error("QuadVertices - incorrect size");
    }

    Vector<Float> triangleVertices = new Vector<Float>();
    for (int i = 0; i < QuadVertices.size(); i += (dimensions * 4))
    {//process 1 quad at a time

        //insert 1st vertex
        int v = 0;
        //triangleVertices.insert(triangleVertices.end(), std::begin(QuadVertices) + i + v * dimensions, std::begin(QuadVertices) + i + (v + 1)* dimensions);
        
        for(int j = 0; j < dimensions ; j++)
        {
            triangleVertices.add(QuadVertices.get(i + v * dimensions + j));
        }

        //insert 2nd vertex
        v = 1;
        //triangleVertices.insert(triangleVertices.end(), std::begin(QuadVertices) + i + v * dimensions, std::begin(QuadVertices) + i + (v + 1) * dimensions);
        for(int j = 0; j < dimensions ; j++)
        {
            triangleVertices.add(QuadVertices.get(i + v * dimensions + j));
        }

        //insert 3rd vertex twice
        v = 2;
        //triangleVertices.insert(triangleVertices.end(), std::begin(QuadVertices) + i + v * dimensions, std::begin(QuadVertices) + i + (v + 1) * dimensions);
        //triangleVertices.insert(triangleVertices.end(), std::begin(QuadVertices) + i + v * dimensions, std::begin(QuadVertices) + i + (v + 1) * dimensions);
        for(int j = 0; j < dimensions ; j++)
        {
            triangleVertices.add(QuadVertices.get(i + v * dimensions + j));
        }
        
        for(int j = 0; j < dimensions ; j++)
        {
            triangleVertices.add(QuadVertices.get(i + v * dimensions + j));
        }

        //insert 4th vertex
        v = 3;
        //triangleVertices.insert(triangleVertices.end(), std::begin(QuadVertices) + i + v * dimensions, std::begin(QuadVertices) + i + (v + 1) * dimensions);
        for(int j = 0; j < dimensions ; j++)
        {
            triangleVertices.add(QuadVertices.get(i + v * dimensions + j));
        }

        //insert 1st vertex again
        v = 0;
        //triangleVertices.insert(triangleVertices.end(), std::begin(QuadVertices) + i + v * dimensions, std::begin(QuadVertices) + i + (v + 1) * dimensions);
        for(int j = 0; j < dimensions ; j++)
        {
            triangleVertices.add(QuadVertices.get(i + v * dimensions + j));
        }

    }

    return triangleVertices;
}

}