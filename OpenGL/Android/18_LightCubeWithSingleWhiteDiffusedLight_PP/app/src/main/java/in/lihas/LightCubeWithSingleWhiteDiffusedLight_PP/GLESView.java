package in.lihas.LightCubeWithSingleWhiteDiffusedLight_PP;

//default includes
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

//imported by me
import android.content.Context;
import android.view.Gravity;
import android.view.MotionEvent;
import android.graphics.Color;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;
import android.os.SystemClock;
import java.lang.System;
import android.widget.Toast;
import android.content.Context;

//for openGL
import android.opengl.GLSurfaceView;
import android.opengl.GLES32;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

//For opengl buffer objects
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

//for matrix math
import android.opengl.Matrix;

//for reading shaders from asset manager
import android.content.res.AssetManager;
import java.io.IOException;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.StringReader;

//Vector, etc.
import java.util.*;

//my util
import util.*;



public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener{
    
    private GestureDetector gestureDetector;
    private final Context context;
    private final AssetManager assets;
    Integer m_currentShaderProgramID = new Integer(-1);
    
        
    /*An application can have multiple shader programs associated with it. Each identified by a user given integer.
    * This integer is NOT the integer returned by glCreateProgram().
    */
    private Map<Integer, ShaderCodesContainer> m_shaderCodesMap = new HashMap<Integer, ShaderCodesContainer>();
    
    String UNIFORM_MV_SHADER_NAME               = new String("u_mv_matrix"         ) ;
    String UNIFORM_P_SHADER_NAME                = new String("u_p_matrix"          ) ;
    String UNIFORM_IsLightOn_SHADER_NAME        = new String("u_bIsLightOn"        ) ;
    String UNIFORM_LD_SHADER_NAME               = new String("u_LD_vec3"           ) ;
    String UNIFORM_KD_SHADER_NAME               = new String("u_KD_vec3"           ) ;
    String UNIFORM_LightPosition_SHADER_NAME    = new String("u_lightPosition_vec4") ;
    
    private boolean m_bIsLightOn = false;
    private boolean m_bIsAnimationOn = false;
    Float m_rotAngle = 0.0f;

private final Float[] cubeCoordsArr = new Float[]{
    -1.000000f, 1.000000f, 1.000000f, 1.000000f, 1.000000f, 1.000000f, 1.000000f, 1.000000f, -1.000000f, 1.000000f, 1.000000f, -1.000000f, -1.000000f, 1.000000f, -1.000000f, -1.000000f, 1.000000f, 1.000000f, -1.000000f, -1.000000f, -1.000000f, -1.000000f, -1.000000f, 1.000000f, 1.000000f, -1.000000f, 1.000000f, 1.000000f, -1.000000f, 1.000000f, 1.000000f, -1.000000f, -1.000000f, -1.000000f, -1.000000f, -1.000000f, -1.000000f, 1.000000f, 1.000000f, -1.000000f, -1.000000f, 1.000000f, 1.000000f, -1.000000f, 1.000000f, 1.000000f, -1.000000f, 1.000000f, 1.000000f, 1.000000f, 1.000000f, -1.000000f, 1.000000f, 1.000000f, -1.000000f, 1.000000f, -1.000000f, -1.000000f, -1.000000f, -1.000000f, 1.000000f, -1.000000f, -1.000000f, 1.000000f, -1.000000f, -1.000000f, 1.000000f, 1.000000f, -1.000000f, -1.000000f, 1.000000f, -1.000000f, 1.000000f, 1.000000f, 1.000000f, 1.000000f, -1.000000f, 1.000000f, 1.000000f, -1.000000f, -1.000000f, 1.000000f, -1.000000f, -1.000000f, 1.000000f, 1.000000f, -1.000000f, 1.000000f, 1.000000f, 1.000000f, -1.000000f, 1.000000f, 1.000000f, -1.000000f, -1.000000f, 1.000000f, -1.000000f, -1.000000f, -1.000000f, -1.000000f, -1.000000f, -1.000000f, -1.000000f, 1.000000f, -1.000000f, -1.000000f, 1.000000f, 1.000000f
};

private final Float[] cubeNormalsArr = new Float[]{
    0.000000f, 1.000000f, 0.000000f, 0.000000f, 1.000000f, 0.000000f, 0.000000f, 1.000000f, 0.000000f, 0.000000f, 1.000000f, 0.000000f, 0.000000f, 1.000000f, 0.000000f, 0.000000f, 1.000000f, 0.000000f, 0.000000f, -1.000000f, 0.000000f, 0.000000f, -1.000000f, 0.000000f, 0.000000f, -1.000000f, 0.000000f, 0.000000f, -1.000000f, 0.000000f, 0.000000f, -1.000000f, 0.000000f, 0.000000f, -1.000000f, 0.000000f, 0.000000f, 0.000000f, 1.000000f, 0.000000f, 0.000000f, 1.000000f, 0.000000f, 0.000000f, 1.000000f, 0.000000f, 0.000000f, 1.000000f, 0.000000f, 0.000000f, 1.000000f, 0.000000f, 0.000000f, 1.000000f, 0.000000f, 0.000000f, -1.000000f, 0.000000f, 0.000000f, -1.000000f, 0.000000f, 0.000000f, -1.000000f, 0.000000f, 0.000000f, -1.000000f, 0.000000f, 0.000000f, -1.000000f, 0.000000f, 0.000000f, -1.000000f, 1.000000f, 0.000000f, 0.000000f, 1.000000f, 0.000000f, 0.000000f, 1.000000f, 0.000000f, 0.000000f, 1.000000f, 0.000000f, 0.000000f, 1.000000f, 0.000000f, 0.000000f, 1.000000f, 0.000000f, 0.000000f, -1.000000f, 0.000000f, 0.000000f, -1.000000f, 0.000000f, 0.000000f, -1.000000f, 0.000000f, 0.000000f, -1.000000f, 0.000000f, 0.000000f, -1.000000f, 0.000000f, 0.000000f, -1.000000f, 0.000000f, 0.000000f
};
                
    private final Vector<Float> cubeCoords = new Vector<Float> (Arrays.asList(cubeCoordsArr));
    private final Vector<Float> cubeNormals = new Vector<Float> (Arrays.asList(cubeNormalsArr));
    
    private Integer cubeID = 0;
    private Integer progID_default = 0;
    
    private int mvp_uniform;
    private float[] perspectiveProjectionMatrix = new float[16];//4x4 matrix
    
    String versionString = new String("#version 320 es\n");
    
    public GLESView(Context drawingContext)
    {
        super(drawingContext);
        context = drawingContext;
        
        setEGLContextClientVersion(3);
        setRenderer(this);
        setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);
        
        gestureDetector = new GestureDetector(drawingContext, this, null, false);
        gestureDetector.setOnDoubleTapListener(this);
        
        assets = context.getAssets();
    }
    
    //implement GLSurfaceView.Renderer methods
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config)
    {
        String version = gl.glGetString(GL10.GL_VERSION);
        System.out.println("RTR: GL10.GL_VERSION " + version);
        version = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("RTR: GLES32.GL_SHADING_LANGUAGE_VERSION " + version);
        initialize();
    }
    
    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height)
    {
        resize(width, height);
    }
    
    @Override
    public void onDrawFrame(GL10 gl)
    {
        display();
    }
    
    
    //custom methods
    
    /// @brief Read the contents of a file present in assets folder to a string
    private String ReadAssetFileAsString(String fileName)
    {
        
        //asset tutorial - https://www.javacodegeeks.com/2012/02/android-read-file-from-assets.html
        String fileStr = null;
        try
        {
            InputStream fileIO = assets.open(fileName);
            int bytesAvailable = fileIO.available();
            byte[] byteBuffer = new byte[bytesAvailable];
            fileIO.read(byteBuffer, 0 , bytesAvailable);
            fileIO.close();
            fileStr = new String(byteBuffer);
            System.out.println("RTR: opening file " + fileName + " file size(bytes): " + bytesAvailable);
        }
        catch(IOException ex)
        {
            System.out.println("RTR: ReadAssetFileAsString() failed");
            uninitialize();
        }
        return fileStr;
    }
    
    /// @brief load shader code from assets folder
    private String LoadShaderCodeAsset(String fileName)
    {
        String code = versionString;
        
        String fileStr = ReadAssetFileAsString(fileName);
        
        //Remove C++ raw string delimiters if present
        BufferedReader bufRead = new BufferedReader(new StringReader(fileStr));
        
        try
        {
            String line = bufRead.readLine();
            while(null != line)
            {
                if(!line.contentEquals("R\"ptD7mQ2vrP8s(") && !line.contentEquals(")ptD7mQ2vrP8s") && !line.contentEquals(")ptD7mQ2vrP8s\";"))
                {
                    code += line;
                    code += "\n";
                }
                line = bufRead.readLine();
            }
        }
        catch(IOException ex)
        {
            System.out.println("RTR: LoadShaderCodeAsset() failed");
        }
        return code;
    }
    
    private void initialize()
    {
        GLES32.glClearColor(0.0f,0.0f,1.0f,1.0f);
        GLES32.glClearDepthf(1.0f);
        GLES32.glEnable(GLES32.GL_DEPTH_TEST);
        GLES32.glDepthFunc(GLES32.GL_LEQUAL);
        
        final String vertexShaderSourceCode = LoadShaderCodeAsset("VertexShaders/{0F1CD938-B032-421B-A697-D4FEBA0E3A28}.vert");
        System.out.println("RTR: vertex shader code being used: " + vertexShaderSourceCode);
        final String fragmentShaderSourceCode = LoadShaderCodeAsset("FragmentShaders/{D82A0051-D193-4EBC-A272-7B569539DC36}.frag");
        System.out.println("RTR: fragment shader code being used: " + fragmentShaderSourceCode);
         
try
{         
        CreateNewShaderProgram(progID_default);
        SetVertexShaderCode(vertexShaderSourceCode);
        SetFragmentShaderCode(fragmentShaderSourceCode);
        
        /*Rectangle*/
        CreateNewObject(cubeID, ObjectDrawType.DT_DrawArrays_Triangles);
        Vector<Float> cubeCoordsTesselated = cubeCoords;
        Vector<Float> cubeNormalsTesselated = cubeNormals;
        AddAttribute(cubeID, GLESMacros.AMC_ATTRIBUTE_POSITION, "vPosition", cubeCoordsTesselated, 0, 3, GLES32.GL_ARRAY_BUFFER, null);
        AddAttribute(cubeID, GLESMacros.AMC_ATTRIBUTE_NORMAL, "vNormal", cubeNormalsTesselated, 0, 3, GLES32.GL_ARRAY_BUFFER, null);
        
        AddUniform(UNIFORM_MV_SHADER_NAME            );
        AddUniform(UNIFORM_P_SHADER_NAME             );
        AddUniform(UNIFORM_IsLightOn_SHADER_NAME     );
        AddUniform(UNIFORM_LD_SHADER_NAME            );
        AddUniform(UNIFORM_KD_SHADER_NAME            );
        AddUniform(UNIFORM_LightPosition_SHADER_NAME );
        
        InitShaders();
        
        Matrix.setIdentityM(perspectiveProjectionMatrix, 0);
}
catch(Exception ex)
{
    util.log_error("Exception caught! " + ex.getMessage());
}
    }
    
    void CreateNewShaderProgram(Integer programID) throws Exception
    {
        ShaderCodesContainer shaderProgram = m_shaderCodesMap.get(programID);
        if(null != shaderProgram)
        {
            util.throw_error("program with given ID already exists");
        }
        ShaderCodesContainer prog = new ShaderCodesContainer();
        m_shaderCodesMap.put(programID, prog);
        m_currentShaderProgramID = programID;
    }
    
    void SetCurrentShaderProgram(Integer programID) throws Exception
    {
        ShaderCodesContainer shaderProgram = m_shaderCodesMap.get(programID);
        if(null == shaderProgram)
        {
            util.throw_error("program with given ID not found. Perhaps CreateNewShaderProgram() wasn't called.");
        }
        m_currentShaderProgramID = programID;
    }
    
    void CreateNewObject(Integer objectID, ObjectDrawType drawType) throws Exception
    {
        ShaderCodesContainer shaderProgram = m_shaderCodesMap.get(m_currentShaderProgramID);
        if(null == shaderProgram)
        {
            util.throw_error("m_currentShaderProgramID not found. Perhaps CreateNewShaderProgram() wasn't called.");
        }
        shaderProgram.CreateNewObject(objectID, drawType);
    }
    
    public void SetVertexShaderCode(String shaderCode) throws Exception
    {
        if(shaderCode.length() == 0)
        {
            util.throw_error("vertex shader code given is empty");
        }
        
        ShaderCodesContainer shaderProgram = m_shaderCodesMap.get(m_currentShaderProgramID);
        if(null == shaderProgram)
        {
            util.throw_error("program with given ID not found");
        }
        shaderProgram.SetVertexShaderCode(shaderCode);
    }
    
    public void SetFragmentShaderCode(String shaderCode) throws Exception
    {
        if(shaderCode.length() == 0)
        {
            util.throw_error("fragment shader code given is empty");
        }
        
        ShaderCodesContainer shaderProgram = m_shaderCodesMap.get(m_currentShaderProgramID);
        if(null == shaderProgram)
        {
            util.throw_error("program with given ID not found");
        }
        
        shaderProgram.SetFragmentShaderCode(shaderCode);
    }
    
    void DrawObject(Integer objectID) throws Exception
    {
        ShaderCodesContainer shaderProgram = m_shaderCodesMap.get(m_currentShaderProgramID);
        if(null == shaderProgram)
        {
            util.throw_error("program with ID m_currentShaderProgramID not found");
        }
        
        GLES32.glBindVertexArray(shaderProgram.GetVao(objectID));
        int numVertices = shaderProgram.GetAttribute(objectID, GLESMacros.AMC_ATTRIBUTE_POSITION).numVertices;
        
        switch(shaderProgram.GetObject(objectID).m_drawType)
        {
            case DT_DrawArrays_Triangles   :
                GLES32.glDrawArrays(GLES32.GL_TRIANGLES, 0, numVertices);
                break;
            case DT_DrawArrays_TriangleFan :
                GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, numVertices);
                break;
            case DT_DrawElements_Triangles :
                VertexAttributeStruct attr = shaderProgram.GetAttribute(objectID, GLESMacros.AMC_ATTRIBUTE_INDICES);
                GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, attr.vbo[0]);
                GLES32.glDrawElements(GLES32.GL_TRIANGLES, numVertices, GLES32.GL_UNSIGNED_INT, 0);
                break;
            default:
                util.throw_error("unknown DrawType");
                break;
        }
    }
    
    public int GetUniformLocation(String uniformName) throws Exception
    {
        ShaderCodesContainer shaderProgram = m_shaderCodesMap.get(m_currentShaderProgramID);
        if(null == shaderProgram)
        {
            util.throw_error("program with ID m_currentShaderProgramID not found");
        }
        
        return shaderProgram.GetUniformLocation(uniformName);
    }
    
    public int GetShaderPorgramObject() throws Exception
    {
        ShaderCodesContainer shaderProgram = m_shaderCodesMap.get(m_currentShaderProgramID);
        if(null == shaderProgram)
        {
            util.throw_error("program with ID m_currentShaderProgramID not found");
        }
        
        return shaderProgram.GetShaderPorgramObject();
    }
    
    public void AddAttribute(Integer objectID, GLESMacros attrType, String shaderVariableName, Vector<Float> dataf, int stride, int components, int bindTarget /*GL_ARRAY_BUFFER, GL_ELEMENT_ARRAY_BUFFER, etc.*/, Vector<Integer> datai) throws Exception
    {
        ShaderCodesContainer shaderProgram = m_shaderCodesMap.get(m_currentShaderProgramID);
        if(null == shaderProgram)
        {
            util.throw_error("program with given ID not found");
        }
        
        if((bindTarget != GLES32.GL_ARRAY_BUFFER) && (bindTarget != GLES32.GL_ELEMENT_ARRAY_BUFFER))
        {
            util.throw_error("only GL_ARRAY_BUFFER, and GL_ELEMENT_ARRAY_BUFFER supported as bind target");
        }
        
        shaderProgram.AddAttribute(objectID, attrType, shaderVariableName, dataf, stride, components, bindTarget, datai);
    }
    
    public void AddUniform(String shaderVariableName) throws Exception
    {
        ShaderCodesContainer shaderProgram = m_shaderCodesMap.get(m_currentShaderProgramID);
        if(null == shaderProgram)
        {
            util.throw_error("program with given ID not found");
        }
        shaderProgram.AddUniform(shaderVariableName);
    }
    
    private void InitShaders() throws Exception
    {
        for(Map.Entry<Integer, ShaderCodesContainer> entry : m_shaderCodesMap.entrySet())
        {
            entry.getValue().InitShaders();
        }
    }
    
    private void uninitialize()
    {
        
    }
    
    private void exit()
    {
        uninitialize();
        System.exit(0);   
    }
    
    private void resize(int width, int height)
    {
        if(height <= 0)
        {
            height = 1;
        }
        GLES32.glViewport(0, 0, width, height);
        
        //m_orthoProjectionMatrix = ortho();
        float factor = ((float)width) / ((float)height);
        Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, factor, 0.1f, 100.0f);
        
    }
    
    private void display()
    {
        if(m_bIsAnimationOn)
        {
            m_rotAngle += 0.1f;
            if(m_rotAngle >= 360.0f)
            {
                m_rotAngle = 0.0f;
            }
        }
        
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
        float[] modelViewMatrix = new float[16];
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.translateM(modelViewMatrix, 0, 0.0f, 0.0f, -6.0f);
        Matrix.rotateM(modelViewMatrix, 0, m_rotAngle, 1.0f, 1.0f, 1.0f);
        
        try
        {
            SetCurrentShaderProgram(progID_default);
            GLES32.glUseProgram(GetShaderPorgramObject());
            
            //send uniforms
            GLES32.glUniformMatrix4fv(GetUniformLocation(UNIFORM_MV_SHADER_NAME), 1, false, modelViewMatrix, 0);
            GLES32.glUniformMatrix4fv(GetUniformLocation(UNIFORM_P_SHADER_NAME), 1, false, perspectiveProjectionMatrix, 0);
            
            if(m_bIsLightOn)
            {
                GLES32.glUniform1i(GetUniformLocation(UNIFORM_IsLightOn_SHADER_NAME), 1);
                GLES32.glUniform3f(GetUniformLocation(UNIFORM_LD_SHADER_NAME), 1.0f, 1.0f, 1.0f);
                GLES32.glUniform3f(GetUniformLocation(UNIFORM_KD_SHADER_NAME), 0.5f, 0.5f, 0.5f);
                GLES32.glUniform4f(GetUniformLocation(UNIFORM_LightPosition_SHADER_NAME), 0.0f, 0.0f, 2.0f, 1.0f);
            }
            else
            {
                GLES32.glUniform1i(GetUniformLocation(UNIFORM_IsLightOn_SHADER_NAME), 0);
            }
            
            
                DrawObject(cubeID);
            }
        catch(Exception ex)
        {
            util.log_error("Exception caught! " + ex.getMessage());
        }
        
        requestRender();
        GLES32.glUseProgram(0);
    }
    
    //handling onTouchEvent is important, since it triggers all gesture and tap events
    @Override public boolean onTouchEvent(MotionEvent event)
    {
        //code
        int eventaction = event.getAction();//will not be used in any of our projects
        if(!gestureDetector.onTouchEvent(event))
        {
            super.onTouchEvent(event);
        }
        return true;
    }
    
    @Override public boolean onDoubleTap(MotionEvent e)
    {
        m_bIsAnimationOn = !m_bIsAnimationOn;
        return true;
    }
    
    @Override public boolean onDoubleTapEvent(MotionEvent e)
    {
        return true;
    }
    
    @Override public boolean onSingleTapConfirmed(MotionEvent e)
    {
        m_bIsLightOn = !m_bIsLightOn;
        return true;
    }
    
    @Override public boolean onDown(MotionEvent e)
    {
        return true;
    }
    
    @Override public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
    {
        return true;
    }
    
    @Override public void onLongPress(MotionEvent e)
    {
        
    }
    
    @Override public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
    {
        exit();
        return true;
    }
    
    @Override public void onShowPress(MotionEvent e)
    {
        
    }
    
    @Override public boolean onSingleTapUp(MotionEvent e)
    {
        return true;
    }
}