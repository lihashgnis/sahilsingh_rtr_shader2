package util;

public enum ObjectDrawType
        {
            DT_DrawArrays_Triangles,        //glDrawArrays(GL_TRIANGLES)
            DT_DrawArrays_TriangleFan,        //glDrawArrays(GL_TRIANGLE_FAN)
            DT_DrawElements_Triangles,      //glDrawElements(GL_TRIANGLES) - indexed drawing mode
        };