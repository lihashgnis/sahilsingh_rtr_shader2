/*
 * ptD7mQ2vrP8s - park tokyo DRIP 7 music QUEEN 2 visa rope PARK 8 skype
 *
 * C++ supports raw strings, other languages may not, or the way to specify raw
 * strings might be different. This special pattern ensures that raw strings
 * can be removed using a simple text search when this file is used in non C++
 * code.
 * Make sure C++ raw string delimiters are there on a line by themselves.
 *
 */
/*
 * @brief Simply gives white color to each fragment
 */
R"ptD7mQ2vrP8s(

#ifdef GL_ES
    precision highp float;
#endif
   
out vec4 FragColor;
void main()
{
    FragColor = vec4(1.0,1.0,1.0,1.0);
}
)ptD7mQ2vrP8s";