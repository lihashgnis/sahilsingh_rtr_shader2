/*
 * ptD7mQ2vrP8s - park tokyo DRIP 7 music QUEEN 2 visa rope PARK 8 skype
 *
 * C++ supports raw strings, other languages may not, or the way to specify raw
 * strings might be different. This special pattern ensures that raw strings
 * can be removed using a simple text search when this file is used in non C++
 * code.
 * Make sure C++ raw string delimiters are there on a line by themselves.
 *
 */
/*
 * @brief Not a complete shader. Goes inside main(). Contains code which can be added to other vertex shaders for supporting light
 */
R"ptD7mQ2vrP8s(
{//Light calculation
    int numLightsOn = 0;
    vec3 light_total = vec3(0.0);
    
    if(u_bGlobalLightSwitch)
    {
        mat4 mv_matrix = u_v_matrix*u_m_matrix;
        mat3 normal_matrix = mat3(transpose(inverse((mv_matrix))));
        vec3 transformed_normal =   normalize(normal_matrix*vNormal);
        vec4 eye_coords = mv_matrix*vPosition;
        
        for(int i = 0; i < u_numLightsToUse ; i++)
        {
            if(u_bIsLightOn[i])
            {
                numLightsOn++;
                
                vec3 ambient_color = u_LA_vec3[i] * u_KA_vec3;
                vec3 light_direction_from_vertex = normalize(vec3(u_lightPosition_vec4[i] - eye_coords)); //variable name S in sir's code
                vec3 diffuse_color = u_LD_vec3[i] * u_KD_vec3*max(dot(light_direction_from_vertex, transformed_normal), 0.0f);
                
                //calculate specular
                vec3 reflection_vector = normalize(reflect(-light_direction_from_vertex, transformed_normal));
                vec3 viewer_vector = normalize(-eye_coords.xyz);
                vec3 specular_color = u_LS_vec3[i] * u_KS_vec3 * pow(max(dot(reflection_vector, viewer_vector), 0), u_shininess + 1);//adding 1 to avoid 0^0 indertminate form - https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/pow.xhtml
                
                vec3 light_color = ambient_color + diffuse_color + specular_color;
                
                light_total += light_color;
            }
        }
    }
    
    if(numLightsOn > 0)
    {
        vertex_color = light_total/numLightsOn;
    }
	else
	{
		 for(int i = 0; i < u_numLightsToUse ; i++)
        {
			light_total += u_default_color[i];
		}

		vertex_color = light_total/u_numLightsToUse;
	}
}
)ptD7mQ2vrP8s";