/*
 * ptD7mQ2vrP8s - park tokyo DRIP 7 music QUEEN 2 visa rope PARK 8 skype
 *
 * C++ supports raw strings, other languages may not, or the way to specify raw
 * strings might be different. This special pattern ensures that raw strings
 * can be removed using a simple text search when this file is used in non C++
 * code.
 * Make sure C++ raw string delimiters are there on a line by themselves.
 *
 */
/*
 * @brief Not a complete shader. Contains code which can be added to other fragment shaders for supporting light
 */
R"ptD7mQ2vrP8s(
in vec3 vertex_color;//coming from vertex shader - interpolated
out vec4 FragColor;
)ptD7mQ2vrP8s";