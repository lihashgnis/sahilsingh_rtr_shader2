/*
 * ptD7mQ2vrP8s - park tokyo DRIP 7 music QUEEN 2 visa rope PARK 8 skype
 *
 * C++ supports raw strings, other languages may not, or the way to specify raw
 * strings might be different. This special pattern ensures that raw strings
 * can be removed using a simple text search when this file is used in non C++
 * code.
 * Make sure C++ raw string delimiters are there on a line by themselves.
 *
 */
/*
 * @brief Not a complete shader. Contains code which can be added to other vertex shaders for supporting light
 */
R"ptD7mQ2vrP8s(
/*NOTE: If you add any variable to this, make sure to modify util::ProgrammablePipeline::Lights::GetLightUniforms()*/
uniform bool u_bGlobalLightSwitch; //if this is false, all lights will be disabled, irrespective of the state of u_bIsLightOn
uniform int u_numLightsToUse;//user wants to use 0 to m_numLightsToUse-1 lights out of MAX_LIGHTS
uniform bool u_bIsLightOn[MAX_LIGHTS]; //per light On/Off state. send data with glUniform1i() to bool. 0 = false, else true
uniform vec3 u_default_color[MAX_LIGHTS];//default color to give when all lights are off

//ambient params
uniform vec3 u_LA_vec3[MAX_LIGHTS];
uniform vec3 u_KA_vec3;

//diffuse params
uniform vec3 u_LD_vec3[MAX_LIGHTS];
uniform vec3 u_KD_vec3;

//specular params
uniform vec3 u_LS_vec3[MAX_LIGHTS];
uniform vec3 u_KS_vec3;
uniform float u_shininess;

uniform vec4 u_lightPosition_vec4[MAX_LIGHTS];
out vec3 vertex_color;//will carry the final computed color to next stages
)ptD7mQ2vrP8s";