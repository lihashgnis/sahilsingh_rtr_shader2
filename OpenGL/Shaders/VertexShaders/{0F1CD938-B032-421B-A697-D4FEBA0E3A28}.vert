/*
 * ptD7mQ2vrP8s - park tokyo DRIP 7 music QUEEN 2 visa rope PARK 8 skype
 *
 * C++ supports raw strings, other languages may not, or the way to specify raw
 * strings might be different. This special pattern ensures that raw strings
 * can be removed using a simple text search when this file is used in non C++
 * code.
 * Make sure C++ raw string delimiters are there on a line by themselves.
 *
 */
/*
 * @brief Simply multiplies each vertex position with model view position matrix
 */
R"ptD7mQ2vrP8s(

in vec4 vPosition;
in vec3 vNormal;
out vec3 diffuse_color;

uniform mat4 u_mv_matrix;
uniform mat4 u_p_matrix;
//Light
uniform bool u_bIsLightOn; //send data with glUniform1i() to bool. 0 = false, else true
uniform vec3 u_LD_vec3;
uniform vec3 u_KD_vec3;
uniform vec4 u_lightPosition_vec4;


void main()
{
    vec4 eye_coords = u_mv_matrix*vPosition;
    
    if(u_bIsLightOn)
    {
        mat3 normal_matrix = mat3(transpose(inverse((u_mv_matrix))));
        vec3 transformed_normal =   normalize(normal_matrix*vNormal);
        vec3 light_direction_from_vertex = normalize(vec3(u_lightPosition_vec4 - eye_coords)); //variable name S in sir's code
        diffuse_color = u_LD_vec3*u_KD_vec3*max(dot(light_direction_from_vertex, transformed_normal), 0.0f);
    }
    
    gl_Position = u_p_matrix*eye_coords;

}
)ptD7mQ2vrP8s";