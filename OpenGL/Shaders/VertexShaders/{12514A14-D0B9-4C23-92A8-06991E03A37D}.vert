/*
 * ptD7mQ2vrP8s - park tokyo DRIP 7 music QUEEN 2 visa rope PARK 8 skype
 *
 * C++ supports raw strings, other languages may not, or the way to specify raw
 * strings might be different. This special pattern ensures that raw strings
 * can be removed using a simple text search when this file is used in non C++
 * code.
 * Make sure C++ raw string delimiters are there on a line by themselves.
 *
 */
/*
 * @brief Simply multiplies each vertex position with model view position matrix
 */
R"ptD7mQ2vrP8s(

in vec4 vPosition;
in vec2 vTexCoord;
uniform mat4 u_mvp_matrix;
out vec2 out_texcoord; //will just be used to pass color data to fragment shader

void main()
{
    gl_Position = u_mvp_matrix*vPosition;
    out_texcoord = vTexCoord;
}
)ptD7mQ2vrP8s";