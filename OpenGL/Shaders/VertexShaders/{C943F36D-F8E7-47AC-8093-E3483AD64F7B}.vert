/*
 * ptD7mQ2vrP8s - park tokyo DRIP 7 music QUEEN 2 visa rope PARK 8 skype
 *
 * C++ supports raw strings, other languages may not, or the way to specify raw
 * strings might be different. This special pattern ensures that raw strings
 * can be removed using a simple text search when this file is used in non C++
 * code.
 * Make sure C++ raw string delimiters are there on a line by themselves.
 *
 */
/*
 * @brief Simply multiplies each vertex position with model view position matrix
 */
R"ptD7mQ2vrP8s(

uniform mat4 u_m_matrix;
uniform mat4 u_v_matrix;
uniform mat4 u_p_matrix;

in vec4 vPosition;
in vec3 vNormal;

void main()
{
//JhrCN[Delimiter][MainSectionStart]
    gl_Position = u_p_matrix*u_v_matrix*u_m_matrix*vPosition;
//JhrCN[Delimiter][MainSectionEnd]
}
)ptD7mQ2vrP8s";