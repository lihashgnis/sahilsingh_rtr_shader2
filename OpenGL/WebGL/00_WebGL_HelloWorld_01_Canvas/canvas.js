//onload function
function main()
{
	var canvas = document.getElementById("AMC");
	if(!canvas)
	{
		console.error("obtaining canvas failed");
	}
	else
	{
		console.log("obtaining canvas success");
	}

	console.log("canvas width : " + canvas.width + " height: " + canvas.height);
	var context = canvas.getContext("2d");
	if(!context)
	{
		console.error("obtaining 2D context failed");
	}
	else
	{
		console.log("obtaining 2D context success");
	}

	context.fillStyle="black";
	context.fillRect(0,0,canvas.width, canvas.height);
	context.textAlign="center";
	context.textBaseline="middle";
	var str="Hello World";
	context.font="48px sans-serif";
	context.fillStyle="white";
	context.fillText(str, canvas.width/2, canvas.height/2);
}