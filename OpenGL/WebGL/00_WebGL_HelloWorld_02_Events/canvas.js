//onload function
gcanvas = null;
gcontext = null;

function main()
{
	gcanvas = document.getElementById("AMC");
	if(!gcanvas)
	{
		console.error("obtaining canvas failed");
	}
	else
	{
		console.log("obtaining canvas success");
	}

	console.log("canvas width : " + gcanvas.width + " height: " + gcanvas.height);
	gcontext = gcanvas.getContext("2d");
	if(!gcontext)
	{
		console.error("obtaining 2D context failed");
	}
	else
	{
		console.log("obtaining 2D context success");
	}

	gcontext.fillStyle="black";
	gcontext.fillRect(0,0,gcanvas.width, gcanvas.height);
	gcontext.textAlign="center";
	gcontext.textBaseline="middle";
	var str="Hello World";
	gcontext.font="48px sans-serif";
	gcontext.fillStyle="white";
	gcontext.fillText(str, gcanvas.width/2, gcanvas.height/2);

	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
}

function keyDown(event)
{
	alert("A key is pressed");
}

function mouseDown()
{
	alert("Mouse clicked");
}