//onload function
gcanvas = null;
gcontext = null;

function main()
{
	gcanvas = document.getElementById("AMC");
	if(!gcanvas)
	{
		console.error("obtaining canvas failed");
	}
	else
	{
		console.log("obtaining canvas success");
	}

	console.log("canvas width : " + gcanvas.width + " height: " + gcanvas.height);
	gcontext = gcanvas.getContext("2d");
	if(!gcontext)
	{
		console.error("obtaining 2D context failed");
	}
	else
	{
		console.log("obtaining 2D context success");
	}

	gcontext.fillStyle="black";
	gcontext.fillRect(0,0,gcanvas.width, gcanvas.height);
	
	drawText("Hello World!!!");

	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
}

function keyDown(event)
{
	switch(event.keyCode)
	{
		case 70:
			toggleFullScreen();
			break;
	}
}

function mouseDown()
{
}

function drawText(text)
{
	gcontext.textAlign="center";
	gcontext.textBaseline="middle";
	gcontext.font="48px sans-serif";
	gcontext.fillStyle="white";
	gcontext.fillText(text, gcanvas.width/2, gcanvas.height/2);
}

function toggleFullScreen()
{
	var fullscreen_element = 
	document.fullscreenElement			||
	document.webkitFullscreenElement	||
	document.mozFullscreenElement		||
	document.msFullscreenElement		||
	null;

	//if not in fullscreen mode
	if(fullscreen_element==null)
	{
		if(gcanvas.requestFullscreen)
		{
			gcanvas.requestFullscreen();
		}
		else if(gcanvas.mozRequestFullScreen)
		{
			gcanvas.mozRequestFullScreen();
		}
		else if(gcanvas.webkitRequestFullscreen)
		{
			gcanvas.webkitRequestFullscreen();
		}
		else if(gcanvas.msRequestFullscreen)
		{
			gcanvas.msRequestFullscreen();
		}
	}
	else
	{
		if(document.exitFullscreen)
		{
			document.exitFullscreen();
		}
		else if(document.mozCancelFullscreen)
		{
			document.mozCancelFullscreen();
		}
		else if(document.webkitExitFullscreen)
		{
			document.webkitExitFullscreen();
		}
		else if(document.msExitFullscreen)
		{
			document.msExitFullscreen();
		}
	
	}
}