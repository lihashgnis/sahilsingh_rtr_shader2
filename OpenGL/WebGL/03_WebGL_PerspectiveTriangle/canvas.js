//onload function
var gcanvas = null;
var gl = null;
var gbFullscreen=false;
var gcanvas_original_width = 0 ;
var gcanvas_original_height = 0;

const gWebGLMacros = 
{
	VDG_ATTRIBUTE_VERTEX   : 0,
	VDG_ATTRIBUTE_COLOR    : 1,
	VDG_ATTRIBUTE_NORMAL   : 2,
	VDG_ATTRIBUTE_TEXTURE0 : 3,
};

var gVertexShaderObject = null;
var gFragmentShaderObject = null;
var gShaderProgramObject = null;

var gvao = -1;
var gvbo = -1;
var gMvpuniform = null;
var gPerspectiveProjectionMatrix = null;

var requestAnimationFrame = 
window.requestAnimationFrame		||
window.webkitRequestAnimationFrame  ||
window.mozRequestAnimationFrame		||
window.oRequestAnimationFrame		||
window.msRequestAnimationFrame;

var cancelAnimationFrame = 
window.cancelAnimationFrame					||
window.webkitCancelRequestAnimationFrame	|| window.webkitCancelAnimationFrame	||
window.mozCancelRequestAnimationFrame		|| window.mozCancelAnimationFrame		||
window.oCancelRequestAnimationFrame			|| window.oCancelAnimationFrame			||
window.msCancelRequestAnimationFrame		|| window.msCancelAnimationFrame;

function main()
{
	gcanvas = document.getElementById("AMC");
	if(!gcanvas)
	{
		console.error("obtaining canvas failed");
	}
	else
	{
		console.log("obtaining canvas success");
	}

	console.log("canvas width : " + gcanvas.width + " height: " + gcanvas.height);
	gcanvas_original_width = gcanvas.width;
	gcanvas_original_height = gcanvas.height;

	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);

	init();

	resize();
	draw();
}

function keyDown(event)
{
	if(event.keyCode >= 'A'.charCodeAt(0) && event.keyCode <= 'Z'.charCodeAt(0))
	{
		var chr = String.fromCharCode(event.keyCode);
		switch(chr)
		{
			case 'F':
				toggleFullScreen();
				break;
		}
	}
	else
	{//other keys
		switch(event.keyCode)
		{
			case 27://escape
			uninitialize();
			window.close();
			break;
		}
	}
}

function mouseDown()
{
}

function drawText(text)
{
	gl.textAlign="center";
	gl.textBaseline="middle";
	gl.font="48px sans-serif";
	gl.fillStyle="white";
	gl.fillText(text, gcanvas.width/2, gcanvas.height/2);
}

function toggleFullScreen()
{
	var fullscreen_element = 
	document.fullscreenElement			||
	document.webkitFullscreenElement	||
	document.mozFullscreenElement		||
	document.msFullscreenElement		||
	null;

	//if not in fullscreen mode
	if(fullscreen_element==null)
	{
		if(gcanvas.requestFullscreen)
		{
			gcanvas.requestFullscreen();
		}
		else if(gcanvas.mozRequestFullScreen)
		{
			gcanvas.mozRequestFullScreen();
		}
		else if(gcanvas.webkitRequestFullscreen)
		{
			gcanvas.webkitRequestFullscreen();
		}
		else if(gcanvas.msRequestFullscreen)
		{
			gcanvas.msRequestFullscreen();
		}
		gbFullscreen=true;
	}
	else
	{
		if(document.exitFullscreen)
		{
			document.exitFullscreen();
		}
		else if(document.mozCancelFullscreen)
		{
			document.mozCancelFullscreen();
		}
		else if(document.webkitExitFullscreen)
		{
			document.webkitExitFullscreen();
		}
		else if(document.msExitFullscreen)
		{
			document.msExitFullscreen();
		}
		gbFullscreen=false;
	}
}

function init()
{
	gl = gcanvas.getContext("webgl2");
	if(gl==null)
	{
		console.error("failed to get webgl rendering context");
		return;
	}
	gl.viewportWidth = gcanvas.width;
	gl.viewportHeight = gcanvas.height;

	//vertex shader
	var vertexShaderSourceCode = 
	`#version 300 es

	in vec4 vPosition;
	uniform mat4 u_mvp_matrix;

	void main()
	{
		gl_Position = u_mvp_matrix * vPosition;
	}
	`;

	gVertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(gVertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(gVertexShaderObject);
	
	if(gl.getShaderParameter(gVertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gVertexShaderObject);
		if(error.length > 0)
		{
			alert("vertex shader compilation failed. see console for logs.");
			console.error(error);
			uninitialize();
		}
	}

	var fragmentShaderSourceCode = 
	`#version 300 es

	precision highp float;
	out vec4 FragColor;
	void main()
	{
		FragColor = vec4(1.0, 1.0, 1.0, 1.0);
	}
	`;
	gFragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(gFragmentShaderObject, fragmentShaderSourceCode);
	gl.compileShader(gFragmentShaderObject);
	
	if(gl.getShaderParameter(gFragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gFragmentShaderObject);
		if(error.length > 0)
		{
			alert("fragment shader compilation failed. see console for logs.");
			console.error(error);
			uninitialize();
		}
	}

	//shader program
	gShaderProgramObject = gl.createProgram();
	gl.attachShader(gShaderProgramObject, gVertexShaderObject);
	gl.attachShader(gShaderProgramObject, gFragmentShaderObject);

	//pre-link : bind attributes
	gl.bindAttribLocation(gShaderProgramObject, gWebGLMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");

	//link program
	gl.linkProgram(gShaderProgramObject);
	if(!gl.getProgramParameter(gShaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(gShaderProgramObject);
		if(error.length > 0)
		{
			alert("shader program link failed. see console logs.")
			console.error(error);
			uninitialize();
		}
	}

	gMvpuniform = gl.getUniformLocation(gShaderProgramObject, "u_mvp_matrix");

	var triangleVertices = new Float32Array([
	  0.0,  1.0, 0.0,
	 -1.0, -1.0, 0.0,
	  1.0, -1.0, 0.0,
	]);

	gvao = gl.createVertexArray();
	gl.bindVertexArray(gvao);

	gvbo = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, gvbo);
	gl.bufferData(gl.ARRAY_BUFFER, triangleVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(gWebGLMacros.VDG_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(gWebGLMacros.VDG_ATTRIBUTE_VERTEX);

	//unbind buffers
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	gl.bindVertexArray(null);

	gl.clearColor(0.0,0.0,1.0,1.0);

	gPerspectiveProjectionMatrix = mat4.create();
}

function resize()
{
	if(gbFullscreen==true)
	{
		gcanvas.width = window.innerWidth;
		gcanvas.height=window.innerHeight;
	}
	else
	{
		gcanvas.width = gcanvas_original_width;
		gcanvas.height = gcanvas_original_height;
	}

	gl.viewport(0,0,gcanvas.width, gcanvas.height);

	var factor = gcanvas.width / gcanvas.height;
	mat4.perspective(gPerspectiveProjectionMatrix, 45.0, factor, 0.1, 100.0);
}

function draw()
{
	gl.clear(gl.COLOR_BUFFER_BIT);
	gl.useProgram(gShaderProgramObject);
	
	var modelViewMatrix = mat4.create();
	var identityMatrix  = mat4.create();
	mat4.translate(modelViewMatrix, identityMatrix, vec3.fromValues(0,0,-6));
	var modelViewProjectionMatrix = mat4.create();
	mat4.multiply(modelViewProjectionMatrix, gPerspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(gMvpuniform, false, modelViewProjectionMatrix);

	gl.bindVertexArray(gvao);
	gl.drawArrays(gl.TRIANGLES, 0, 3);
	gl.bindVertexArray(null);
	gl.useProgram(null);

	//animation loop
	requestAnimationFrame(draw, gcanvas);
}

function uninitialize()
{
	//code
	if(gvao)
	{
		gl.deleteVertexArray(gvao);
		gvao = null;
	}

	if(gvbo)
	{
		gl.deleteBuffer(gvbo);
		gvbo = null;
	}

	if(gShaderProgramObject)
	{
		if(gFragmentShaderObject)
		{
			gl.detachShader(gShaderProgramObject, gFragmentShaderObject);
			gl.deleteShader(gFragmentShaderObject);
			gFragmentShaderObject = null;
		}

		if(gVertexShaderObject)
		{
			gl.detachShader(gShaderProgramObject, gVertexShaderObject);
			gl.deleteShader(gVertexShaderObject);
			gVertexShaderObject = null;
		}

		gl.deleteProgram(gShaderProgramObject);
		gShaderProgramObject = null;
	}
}