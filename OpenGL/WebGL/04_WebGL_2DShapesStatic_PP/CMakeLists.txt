# CMakeList.txt : CMake project for 04_WebGL_2DShapesStatic_PP, include source and define
# project specific logic here.
#
cmake_minimum_required (VERSION 3.8)
set(PROJ_NAME "04_WebGL_2DShapesStatic_PP")
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

set(SRC_FILES 
canvas.html
canvas.js
gl-matrix-min.js
)

source_group("SOURCES" FILES ${SRC_FILES})

add_executable ("${PROJ_NAME}" ${SRC_FILES})

SET_TARGET_PROPERTIES("${PROJ_NAME}" PROPERTIES FOLDER "03_WebGL_PP")
set_target_properties(${PROJ_NAME} PROPERTIES LINKER_LANGUAGE NONE)
