//onload function
var gcanvas = null;
var gl = null;
var gbFullscreen=false;
var gcanvas_original_width = 0 ;
var gcanvas_original_height = 0;

const gWebGLMacros = 
{
	VDG_ATTRIBUTE_VERTEX   : 0,
	VDG_ATTRIBUTE_COLOR    : 1,
	VDG_ATTRIBUTE_NORMAL   : 2,
	VDG_ATTRIBUTE_TEXTURE0 : 3,
};

var gVertexShaderObject = null;
var gFragmentShaderObject = null;
var gShaderProgramObject = null;

var gvaoTriangle = -1;
var gvboTrianglePosition = -1;
var gvboTriangleColor = -1;
var gvaoRectangle = -1;
var gvboRectanglePosition = -1;
var gvboRectangleColor = -1;
var gMvpuniform = null;
var gPerspectiveProjectionMatrix = null;

var gRotateAngleRad = 0.0; //rotation angle in radians

var requestAnimationFrame = 
window.requestAnimationFrame		||
window.webkitRequestAnimationFrame  ||
window.mozRequestAnimationFrame		||
window.oRequestAnimationFrame		||
window.msRequestAnimationFrame;

var cancelAnimationFrame = 
window.cancelAnimationFrame					||
window.webkitCancelRequestAnimationFrame	|| window.webkitCancelAnimationFrame	||
window.mozCancelRequestAnimationFrame		|| window.mozCancelAnimationFrame		||
window.oCancelRequestAnimationFrame			|| window.oCancelAnimationFrame			||
window.msCancelRequestAnimationFrame		|| window.msCancelAnimationFrame;

function main()
{
	gcanvas = document.getElementById("AMC");
	if(!gcanvas)
	{
		console.error("obtaining canvas failed");
	}
	else
	{
		console.log("obtaining canvas success");
	}

	console.log("canvas width : " + gcanvas.width + " height: " + gcanvas.height);
	gcanvas_original_width = gcanvas.width;
	gcanvas_original_height = gcanvas.height;

	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);

	init();

	resize();
	draw();
}

function keyDown(event)
{
	if(event.keyCode >= 'A'.charCodeAt(0) && event.keyCode <= 'Z'.charCodeAt(0))
	{
		var chr = String.fromCharCode(event.keyCode);
		switch(chr)
		{
			case 'F':
				toggleFullScreen();
				break;
		}
	}
	else
	{//other keys
		switch(event.keyCode)
		{
			case 27://escape
			uninitialize();
			window.close();
			break;
		}
	}
}

function mouseDown()
{
}

function drawText(text)
{
	gl.textAlign="center";
	gl.textBaseline="middle";
	gl.font="48px sans-serif";
	gl.fillStyle="white";
	gl.fillText(text, gcanvas.width/2, gcanvas.height/2);
}

function toggleFullScreen()
{
	var fullscreen_element = 
	document.fullscreenElement			||
	document.webkitFullscreenElement	||
	document.mozFullscreenElement		||
	document.msFullscreenElement		||
	null;

	//if not in fullscreen mode
	if(fullscreen_element==null)
	{
		if(gcanvas.requestFullscreen)
		{
			gcanvas.requestFullscreen();
		}
		else if(gcanvas.mozRequestFullScreen)
		{
			gcanvas.mozRequestFullScreen();
		}
		else if(gcanvas.webkitRequestFullscreen)
		{
			gcanvas.webkitRequestFullscreen();
		}
		else if(gcanvas.msRequestFullscreen)
		{
			gcanvas.msRequestFullscreen();
		}
		gbFullscreen=true;
	}
	else
	{
		if(document.exitFullscreen)
		{
			document.exitFullscreen();
		}
		else if(document.mozCancelFullscreen)
		{
			document.mozCancelFullscreen();
		}
		else if(document.webkitExitFullscreen)
		{
			document.webkitExitFullscreen();
		}
		else if(document.msExitFullscreen)
		{
			document.msExitFullscreen();
		}
		gbFullscreen=false;
	}
}

function init()
{
	gl = gcanvas.getContext("webgl2");
	if(gl==null)
	{
		console.error("failed to get webgl rendering context");
		return;
	}
	gl.viewportWidth = gcanvas.width;
	gl.viewportHeight = gcanvas.height;

	//vertex shader
	var vertexShaderSourceCode = 
	`#version 300 es

	in vec4 vPosition;
	in vec4 vColor;
	uniform mat4 u_mvp_matrix;
	out vec4 out_color;//will just be used to pass color data to fragment shader

	void main()
	{
		gl_Position = u_mvp_matrix * vPosition;
		out_color = vColor;
	}
	`;

	gVertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(gVertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(gVertexShaderObject);
	
	if(gl.getShaderParameter(gVertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gVertexShaderObject);
		if(error.length > 0)
		{
			alert("vertex shader compilation failed. see console for logs.");
			console.error(error);
			uninitialize();
		}
	}

	var fragmentShaderSourceCode = 
	`#version 300 es

	precision highp float;
	in vec4 out_color;//coming from vertex shader
	out vec4 FragColor;
	void main()
	{
		FragColor = out_color;
	}
	`;
	gFragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(gFragmentShaderObject, fragmentShaderSourceCode);
	gl.compileShader(gFragmentShaderObject);
	
	if(gl.getShaderParameter(gFragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gFragmentShaderObject);
		if(error.length > 0)
		{
			alert("fragment shader compilation failed. see console for logs.");
			console.error(error);
			uninitialize();
		}
	}

	//shader program
	gShaderProgramObject = gl.createProgram();
	gl.attachShader(gShaderProgramObject, gVertexShaderObject);
	gl.attachShader(gShaderProgramObject, gFragmentShaderObject);

	//pre-link : bind attributes
	gl.bindAttribLocation(gShaderProgramObject, gWebGLMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(gShaderProgramObject, gWebGLMacros.VDG_ATTRIBUTE_COLOR, "vColor");

	//link program
	gl.linkProgram(gShaderProgramObject);
	if(!gl.getProgramParameter(gShaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(gShaderProgramObject);
		if(error.length > 0)
		{
			alert("shader program link failed. see console logs.")
			console.error(error);
			uninitialize();
		}
	}

	gMvpuniform = gl.getUniformLocation(gShaderProgramObject, "u_mvp_matrix");


	//triangle
	var triangleVertices = new Float32Array([
	  0.0,  1.0, 0.0,
	 -1.0, -1.0, 0.0,
	  1.0, -1.0, 0.0,
	]);

	var triangleColor = new Float32Array([
	  1.0,   0.0,    0.0,
      0.0,   1.0,    0.0,
      0.0,   0.0,    1.0
	]);

	gvaoTriangle = gl.createVertexArray();
	gl.bindVertexArray(gvaoTriangle);
	gvboTrianglePosition = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, gvboTrianglePosition);
	gl.bufferData(gl.ARRAY_BUFFER, triangleVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(gWebGLMacros.VDG_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(gWebGLMacros.VDG_ATTRIBUTE_VERTEX);

	gvboTriangleColor = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, gvboTriangleColor);
	gl.bufferData(gl.ARRAY_BUFFER, triangleColor, gl.STATIC_DRAW);
	gl.vertexAttribPointer(gWebGLMacros.VDG_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(gWebGLMacros.VDG_ATTRIBUTE_COLOR);

	//rectangle
	var rectangleVertices = new Float32Array([
	  -1.0,   1.0,    0.0,
      -1.0,  -1.0,    0.0,
       1.0,  -1.0,    0.0,
       1.0,   1.0,    0.0,
	]);

	var rectangleColor = new Float32Array([
	    1.0,   0.0,    0.0,
		0.0,   1.0,    0.0,
		0.0,   0.0,    1.0,
		1.0,   0.0,    0.0
	]);

	gvaoRectangle = gl.createVertexArray();
	gl.bindVertexArray(gvaoRectangle);
	gvboRectanglePosition = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, gvboRectanglePosition);
	gl.bufferData(gl.ARRAY_BUFFER, rectangleVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(gWebGLMacros.VDG_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(gWebGLMacros.VDG_ATTRIBUTE_VERTEX);

	gvboRectangleColor = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, gvboRectangleColor);
	gl.bufferData(gl.ARRAY_BUFFER, rectangleColor, gl.STATIC_DRAW);
	gl.vertexAttribPointer(gWebGLMacros.VDG_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(gWebGLMacros.VDG_ATTRIBUTE_COLOR);

	//unbind buffers
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	gl.bindVertexArray(null);

	gl.clearColor(0.0,0.0,1.0,1.0);

	gPerspectiveProjectionMatrix = mat4.create();
}

function resize()
{
	if(gbFullscreen==true)
	{
		gcanvas.width = window.innerWidth;
		gcanvas.height=window.innerHeight;
	}
	else
	{
		gcanvas.width = gcanvas_original_width;
		gcanvas.height = gcanvas_original_height;
	}

	gl.viewport(0,0,gcanvas.width, gcanvas.height);

	var factor = gcanvas.width / gcanvas.height;
	mat4.perspective(gPerspectiveProjectionMatrix, 45.0, factor, 0.1, 100.0);
}

function draw()
{
	gRotateAngleRad += 0.01;
	if(gRotateAngleRad >= 360.0)
	{
		gRotateAngleRad = 0.0;
	}

	gl.clear(gl.COLOR_BUFFER_BIT);
	gl.useProgram(gShaderProgramObject);
	
	var modelViewMatrix = mat4.create();
	var identityMatrix  = mat4.create();

	//triangle
	mat4.translate(modelViewMatrix, identityMatrix, vec3.fromValues(-2,0,-9));
	mat4.rotate(modelViewMatrix, modelViewMatrix, gRotateAngleRad, vec3.fromValues(0,1,0));
	var modelViewProjectionMatrix = mat4.create();
	mat4.multiply(modelViewProjectionMatrix, gPerspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(gMvpuniform, false, modelViewProjectionMatrix);

	gl.bindVertexArray(gvaoTriangle);
	gl.drawArrays(gl.TRIANGLES, 0, 3);

	//rectangle
	modelViewMatrix = mat4.create();
	mat4.translate(modelViewMatrix, identityMatrix, vec3.fromValues(2,0,-9));
	mat4.rotate(modelViewMatrix, modelViewMatrix, gRotateAngleRad, vec3.fromValues(1,0,0));
	modelViewProjectionMatrix = mat4.create();
	mat4.multiply(modelViewProjectionMatrix, gPerspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(gMvpuniform, false, modelViewProjectionMatrix);
	gl.bindVertexArray(gvaoRectangle);
	gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);

	gl.bindVertexArray(null);
	gl.useProgram(null);

	//animation loop
	requestAnimationFrame(draw, gcanvas);
}

function uninitialize()
{
	if(gvaoTriangle)
	{
		gl.deleteVertexArray(gvaoTriangle);
		gvaoTriangle = null;
	}

	if(gvboTrianglePosition)
	{
		gl.deleteBuffer(gvboTrianglePosition);
		gvboTrianglePosition = null;
	}

	if(gvboTriangleColor)
	{
		gl.deleteBuffer(gvboTriangleColor);
		gvboTriangleColor = null;
	}


	if(gvaoRectangle)
	{
		gl.deleteVertexArray(gvaoRectangle);
		gvaoRectangle = null;
	}

	if(gvboRectanglePosition)
	{
		gl.deleteBuffer(gvboRectanglePosition);
		gvboRectanglePosition = null;
	}

	if(gvboRectangleColor)
	{
		gl.deleteBuffer(gvboRectangleColor);
		gvboRectangleColor = null;
	}

	if(gShaderProgramObject)
	{
		if(gFragmentShaderObject)
		{
			gl.detachShader(gShaderProgramObject, gFragmentShaderObject);
			gl.deleteShader(gFragmentShaderObject);
			gFragmentShaderObject = null;
		}

		if(gVertexShaderObject)
		{
			gl.detachShader(gShaderProgramObject, gVertexShaderObject);
			gl.deleteShader(gVertexShaderObject);
			gVertexShaderObject = null;
		}

		gl.deleteProgram(gShaderProgramObject);
		gShaderProgramObject = null;
	}
}