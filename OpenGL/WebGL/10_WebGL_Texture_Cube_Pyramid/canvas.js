//onload function
var gcanvas = null;
var gl = null;
var gbFullscreen=false;
var gcanvas_original_width = 0 ;
var gcanvas_original_height = 0;
var gRotateAngle = 0.0;

const gWebGLMacros = 
{
	VDG_ATTRIBUTE_VERTEX   : 0,
	VDG_ATTRIBUTE_COLOR    : 1,
	VDG_ATTRIBUTE_NORMAL   : 2,
	VDG_ATTRIBUTE_TEXTURE0 : 3,
};

var gVertexShaderObject = null;
var gFragmentShaderObject = null;
var gShaderProgramObject = null;

var gvaoPyramid = null;
var gvboPyramidPosition = null;
var gvaoCube = null;
var gvboCubePosition = null;
var gMvpuniform = null;
var gPerspectiveProjectionMatrix = null;

//texture
var gvboPyramidTexture = null;
var gvboCubeTexture = null;
var gu_Texture0_sampler = null;
var gStoneTexture = null;
var gKundaliTexture = null;

var requestAnimationFrame = 
window.requestAnimationFrame		||
window.webkitRequestAnimationFrame  ||
window.mozRequestAnimationFrame		||
window.oRequestAnimationFrame		||
window.msRequestAnimationFrame;

var cancelAnimationFrame = 
window.cancelAnimationFrame					||
window.webkitCancelRequestAnimationFrame	|| window.webkitCancelAnimationFrame	||
window.mozCancelRequestAnimationFrame		|| window.mozCancelAnimationFrame		||
window.oCancelRequestAnimationFrame			|| window.oCancelAnimationFrame			||
window.msCancelRequestAnimationFrame		|| window.msCancelAnimationFrame;

function main()
{
	gcanvas = document.getElementById("AMC");
	if(!gcanvas)
	{
		console.error("obtaining canvas failed");
	}
	else
	{
		console.log("obtaining canvas success");
	}

	console.log("canvas width : " + gcanvas.width + " height: " + gcanvas.height);
	gcanvas_original_width = gcanvas.width;
	gcanvas_original_height = gcanvas.height;

	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);

	init();

	resize();
	draw();
}

function keyDown(event)
{
	if(event.keyCode >= 'A'.charCodeAt(0) && event.keyCode <= 'Z'.charCodeAt(0))
	{
		var chr = String.fromCharCode(event.keyCode);
		switch(chr)
		{
			case 'F':
				toggleFullScreen();
				break;
		}
	}
	else
	{//other keys
		switch(event.keyCode)
		{
			case 27://escape
			uninitialize();
			window.close();
			break;
		}
	}
}

function mouseDown()
{
}

function drawText(text)
{
	gl.textAlign="center";
	gl.textBaseline="middle";
	gl.font="48px sans-serif";
	gl.fillStyle="white";
	gl.fillText(text, gcanvas.width/2, gcanvas.height/2);
}

function toggleFullScreen()
{
	var fullscreen_element = 
	document.fullscreenElement			||
	document.webkitFullscreenElement	||
	document.mozFullscreenElement		||
	document.msFullscreenElement		||
	null;

	//if not in fullscreen mode
	if(fullscreen_element==null)
	{
		if(gcanvas.requestFullscreen)
		{
			gcanvas.requestFullscreen();
		}
		else if(gcanvas.mozRequestFullScreen)
		{
			gcanvas.mozRequestFullScreen();
		}
		else if(gcanvas.webkitRequestFullscreen)
		{
			gcanvas.webkitRequestFullscreen();
		}
		else if(gcanvas.msRequestFullscreen)
		{
			gcanvas.msRequestFullscreen();
		}
		gbFullscreen=true;
	}
	else
	{
		if(document.exitFullscreen)
		{
			document.exitFullscreen();
		}
		else if(document.mozCancelFullscreen)
		{
			document.mozCancelFullscreen();
		}
		else if(document.webkitExitFullscreen)
		{
			document.webkitExitFullscreen();
		}
		else if(document.msExitFullscreen)
		{
			document.msExitFullscreen();
		}
		gbFullscreen=false;
	}
}

function init()
{

	gl = gcanvas.getContext("webgl2");
	if(gl==null)
	{
		console.error("failed to get webgl rendering context");
		return;
	}
	gl.viewportWidth = gcanvas.width;
	gl.viewportHeight = gcanvas.height;

	//vertex shader
	var vertexShaderSourceCode = 
	`#version 300 es

	in vec4 vPosition;
	in vec2 vTexture0_coord;
	uniform mat4 u_mvp_matrix;
	out vec2 out_vTexture0_coord;//will just be used to pass texture cords to fragment shader

	void main()
	{
		gl_Position = u_mvp_matrix * vPosition;
		out_vTexture0_coord = vTexture0_coord;
	}
	`;

	gVertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(gVertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(gVertexShaderObject);
	
	if(gl.getShaderParameter(gVertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gVertexShaderObject);
		if(error.length > 0)
		{
			alert("vertex shader compilation failed. see console for logs.");
			console.error(error);
			uninitialize();
		}
	}

	var fragmentShaderSourceCode = 
	`#version 300 es

	precision highp float;
	in vec2 out_vTexture0_coord;//coming from vertex shader
	uniform highp sampler2D u_Texture0_sampler;
	out vec4 FragColor;
	void main()
	{
		FragColor = texture(u_Texture0_sampler, out_vTexture0_coord);
	}
	`;
	gFragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(gFragmentShaderObject, fragmentShaderSourceCode);
	gl.compileShader(gFragmentShaderObject);
	
	if(gl.getShaderParameter(gFragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gFragmentShaderObject);
		if(error.length > 0)
		{
			alert("fragment shader compilation failed. see console for logs.");
			console.error(error);
			uninitialize();
		}
	}

	//shader program
	gShaderProgramObject = gl.createProgram();
	gl.attachShader(gShaderProgramObject, gVertexShaderObject);
	gl.attachShader(gShaderProgramObject, gFragmentShaderObject);

	//pre-link : bind attributes
	gl.bindAttribLocation(gShaderProgramObject, gWebGLMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(gShaderProgramObject, gWebGLMacros.VDG_ATTRIBUTE_TEXTURE0, "vTexture0_coord");

	//link program
	gl.linkProgram(gShaderProgramObject);
	if(!gl.getProgramParameter(gShaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(gShaderProgramObject);
		if(error.length > 0)
		{
			alert("shader program link failed. see console logs.")
			console.error(error);
			uninitialize();
		}
	}

	gMvpuniform = gl.getUniformLocation(gShaderProgramObject, "u_mvp_matrix");
    gu_Texture0_sampler = gl.getUniformLocation(gShaderProgramObject, "u_Texture0_sampler");

	//pyramid
	var pyramidVertices = new Float32Array([
	   0.000000, 1.000000, 0.000000, -1.000000, -1.000000, 1.000000, 1.000000, -1.000000, 1.000000, 0.000000, 1.000000, 0.000000, 1.000000, -1.000000, 1.000000, 1.000000, -1.000000, -1.000000, 0.000000, 1.000000, 0.000000, 1.000000, -1.000000, -1.000000, -1.000000, -1.000000, -1.000000, 0.000000, 1.000000, 0.000000, -1.000000, -1.000000, -1.000000, -1.000000, -1.000000, 1.000000
	]);

    var pyramidTextureCoords = new Float32Array([
            0.5,1,
            0,0,
            1,0,
            0.5,1,
            1,0,
            0,0,
            0.5,1,
            0,0,
            1,0,
            0.5,1,
            1,0,
            0,0
            ]);

	gvaoPyramid = gl.createVertexArray();
	gl.bindVertexArray(gvaoPyramid);
	gvboPyramidPosition = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, gvboPyramidPosition);
	gl.bufferData(gl.ARRAY_BUFFER, pyramidVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(gWebGLMacros.VDG_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(gWebGLMacros.VDG_ATTRIBUTE_VERTEX);

    gvboPyramidTexture = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, gvboPyramidTexture);
    gl.bufferData(gl.ARRAY_BUFFER, pyramidTextureCoords, gl.STATIC_DRAW);
    gl.vertexAttribPointer(gWebGLMacros.VDG_ATTRIBUTE_TEXTURE0, 2, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(gWebGLMacros.VDG_ATTRIBUTE_TEXTURE0);

	//cube
	var cubeVertices = new Float32Array([
	   -1.000000, 1.000000,  1.000000, 
        1.000000, 1.000000,  1.000000, 
        1.000000, 1.000000, -1.000000, 
        1.000000, 1.000000, -1.000000, 
       -1.000000, 1.000000, -1.000000, 
       -1.000000, 1.000000,  1.000000, -1.000000, -1.000000, -1.000000, -1.000000, -1.000000, 1.000000, 1.000000, -1.000000, 1.000000, 1.000000, -1.000000, 1.000000, 1.000000, -1.000000, -1.000000, -1.000000, -1.000000, -1.000000, -1.000000, 1.000000, 1.000000, -1.000000, -1.000000, 1.000000, 1.000000, -1.000000, 1.000000, 1.000000, -1.000000, 1.000000, 1.000000, 1.000000, 1.000000, -1.000000, 1.000000, 1.000000, -1.000000, 1.000000, -1.000000, -1.000000, -1.000000, -1.000000, 1.000000, -1.000000, -1.000000, 1.000000, -1.000000, -1.000000, 1.000000, 1.000000, -1.000000, -1.000000, 1.000000, -1.000000, 1.000000, 1.000000, 1.000000, 1.000000, -1.000000, 1.000000, 1.000000, -1.000000, -1.000000, 1.000000, -1.000000, -1.000000, 1.000000, 1.000000, -1.000000, 1.000000, 1.000000, 1.000000, -1.000000, 1.000000, 1.000000, -1.000000, -1.000000, 1.000000, -1.000000, -1.000000, -1.000000, -1.000000, -1.000000, -1.000000, -1.000000, 1.000000, -1.000000, -1.000000, 1.000000, 1.000000
	    ]);

    var cubeTextureCoords = new Float32Array([
        0, 0,
        1, 0,
        1, 1,
        1, 1,
        0, 1,
        0, 0,
        0, 0,
        0, 1,
        1, 1,
        1, 1,
        1, 0,
        0, 0,
        0, 1,
        0, 0,
        1, 0,
        1, 0,
        1, 1,
        0, 1,
        1, 1,
        1, 0,
        0, 0,
        0, 0,
        0, 1,
        1, 1,
        0, 1,
        0, 0,
        1, 0,
        1, 0,
        1, 1,
        0, 1,
        1, 1,
        1, 0,
        0, 0,
        0, 0,
        0, 1,
        1, 1
        ]);

	gvaoCube = gl.createVertexArray();
	gl.bindVertexArray(gvaoCube);
	gvboPyramidPosition = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, gvboPyramidPosition);
	gl.bufferData(gl.ARRAY_BUFFER, cubeVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(gWebGLMacros.VDG_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(gWebGLMacros.VDG_ATTRIBUTE_VERTEX);

    gvboCubeTexture = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, gvboCubeTexture);
    gl.bufferData(gl.ARRAY_BUFFER, cubeTextureCoords, gl.STATIC_DRAW);
    gl.vertexAttribPointer(gWebGLMacros.VDG_ATTRIBUTE_TEXTURE0, 2, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(gWebGLMacros.VDG_ATTRIBUTE_TEXTURE0);

	//unbind buffers
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	gl.bindVertexArray(null);

    gKundaliTexture = gl.createTexture();
    gKundaliTexture.image = new Image();
    gKundaliTexture.image.src = "Vijay_Kundali.png";
    gKundaliTexture.image.onload = function()
    {
        gl.bindTexture(gl.TEXTURE_2D, gKundaliTexture);
        gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, gKundaliTexture.image);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
        gl.bindTexture(gl.TEXTURE_2D, null);
    };

    gStoneTexture = gl.createTexture();
    gStoneTexture.image = new Image();
    gStoneTexture.image.src = "Stone.png";
    gStoneTexture.image.onload = function()
    {
        gl.bindTexture(gl.TEXTURE_2D, gStoneTexture);
        gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, gStoneTexture.image);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
        gl.bindTexture(gl.TEXTURE_2D, null);
    };

	gl.clearColor(0.0,0.0,1.0,1.0);
    gl.enable(gl.DEPTH_TEST);
	gl.clearDepth(1.0);

	gPerspectiveProjectionMatrix = mat4.create();
}

function resize()
{
	if(gbFullscreen==true)
	{
		gcanvas.width = window.innerWidth;
		gcanvas.height=window.innerHeight;
	}
	else
	{
		gcanvas.width = gcanvas_original_width;
		gcanvas.height = gcanvas_original_height;
	}

	gl.viewport(0,0,gcanvas.width, gcanvas.height);

	var factor = gcanvas.width / gcanvas.height;
	mat4.perspective(gPerspectiveProjectionMatrix, 45.0, factor, 0.1, 100.0);
}

function draw()
{
    gRotateAngle += 0.01;
    if(gRotateAngle >= 360.0)
    {
        gRotateAngle = 0.0;
    }

	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	gl.useProgram(gShaderProgramObject);
	
	var modelViewMatrix = mat4.create();

	//pyramid
	mat4.translate(modelViewMatrix, modelViewMatrix, vec3.fromValues(-2.5,0,-9));
    mat4.rotate(modelViewMatrix, modelViewMatrix, gRotateAngle, [0,1,0]);
	var modelViewProjectionMatrix = mat4.create();
	mat4.multiply(modelViewProjectionMatrix, gPerspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(gMvpuniform, false, modelViewProjectionMatrix);

    gl.activeTexture(gl.TEXTURE0);
	gl.bindTexture(gl.TEXTURE_2D, gStoneTexture);
	gl.uniform1i(gu_Texture0_sampler, 0);

	gl.bindVertexArray(gvaoPyramid);
	gl.drawArrays(gl.TRIANGLES, 0, 12);

	//cube
	modelViewMatrix = mat4.create();
	mat4.translate(modelViewMatrix, modelViewMatrix, vec3.fromValues(2.5,0,-9));
    mat4.rotate(modelViewMatrix, modelViewMatrix, gRotateAngle, [1,1,1]);
	modelViewProjectionMatrix = mat4.create();
	mat4.multiply(modelViewProjectionMatrix, gPerspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(gMvpuniform, false, modelViewProjectionMatrix);
	
    gl.activeTexture(gl.TEXTURE0);
	gl.bindTexture(gl.TEXTURE_2D, gKundaliTexture);
	gl.uniform1i(gu_Texture0_sampler, 0);

    gl.bindVertexArray(gvaoCube);
	gl.drawArrays(gl.TRIANGLES, 0, 36);

	gl.bindVertexArray(null);
	gl.useProgram(null);

	//animation loop
	requestAnimationFrame(draw, gcanvas);
}

function uninitialize()
{
	if(gvaoPyramid)
	{
		gl.deleteVertexArray(gvaoPyramid);
		gvaoPyramid = null;
	}

	if(gvboPyramidPosition)
	{
		gl.deleteBuffer(gvboPyramidPosition);
		gvboPyramidPosition = null;
	}


	if(gvaoCube)
	{
		gl.deleteVertexArray(gvaoCube);
		gvaoCube = null;
	}

	if(gvboCubePosition)
	{
		gl.deleteBuffer(gvboCubePosition);
		gvboCubePosition = null;
	}

	if(gShaderProgramObject)
	{
		if(gFragmentShaderObject)
		{
			gl.detachShader(gShaderProgramObject, gFragmentShaderObject);
			gl.deleteShader(gFragmentShaderObject);
			gFragmentShaderObject = null;
		}

		if(gVertexShaderObject)
		{
			gl.detachShader(gShaderProgramObject, gVertexShaderObject);
			gl.deleteShader(gVertexShaderObject);
			gVertexShaderObject = null;
		}

		gl.deleteProgram(gShaderProgramObject);
		gShaderProgramObject = null;
	}

    if(gStoneTexture)
	{
		gl.deleteTexture(gStoneTexture);
		gSmileyTexture = 0;
	}

    if(gKundaliTexture)
	{
		gl.deleteTexture(gKundaliTexture);
		gSmileyTexture = 0;
	}
}
