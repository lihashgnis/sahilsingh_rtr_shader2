//onload function
var gcanvas = null;
var gl = null;
var gbFullscreen=false;
var gcanvas_original_width = 0 ;
var gcanvas_original_height = 0;

const gWebGLMacros = 
{
	VDG_ATTRIBUTE_VERTEX   : 0,
	VDG_ATTRIBUTE_COLOR    : 1,
	VDG_ATTRIBUTE_NORMAL   : 2,
	VDG_ATTRIBUTE_TEXTURE0 : 3,
};

var gVertexShaderObject = null;
var gFragmentShaderObject = null;
var gShaderProgramObject = null;

var gvaoLine = -1;
var gvboLineColor = -1;
var gvboLinePosition = -1;
var gMvpuniform = null;
var gOrthoProjectionMatrix = null;
var gNumVertices = 0;

var gNumKeyPressed = 0;

var requestAnimationFrame = 
window.requestAnimationFrame		||
window.webkitRequestAnimationFrame  ||
window.mozRequestAnimationFrame		||
window.oRequestAnimationFrame		||
window.msRequestAnimationFrame;

var cancelAnimationFrame = 
window.cancelAnimationFrame					||
window.webkitCancelRequestAnimationFrame	|| window.webkitCancelAnimationFrame	||
window.mozCancelRequestAnimationFrame		|| window.mozCancelAnimationFrame		||
window.oCancelRequestAnimationFrame			|| window.oCancelAnimationFrame			||
window.msCancelRequestAnimationFrame		|| window.msCancelAnimationFrame;

function main()
{
	gcanvas = document.getElementById("AMC");
	if(!gcanvas)
	{
		console.error("obtaining canvas failed");
	}
	else
	{
		console.log("obtaining canvas success");
	}

	console.log("canvas width : " + gcanvas.width + " height: " + gcanvas.height);
	gcanvas_original_width = gcanvas.width;
	gcanvas_original_height = gcanvas.height;

	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);

	init();

	resize();
	draw();
}

function keyDown(event)
{
	if(event.keyCode >= 'A'.charCodeAt(0) && event.keyCode <= 'Z'.charCodeAt(0))
	{
		var chr = String.fromCharCode(event.keyCode);
		switch(chr)
		{
			case 'F':
				toggleFullScreen();
				break;
		}
	}
    else if(event.keyCode >= '0'.charCodeAt(0) && event.keyCode <= '9'.charCodeAt(0))
    {
        gNumKeyPressed = event.keyCode - 48;
        var chr = String.fromCharCode(event.keyCode);
		switch(chr)
		{
			case '0':
				break;
		}
    }
	else
	{//other keys
		switch(event.keyCode)
		{
			case 27://escape
			uninitialize();
			window.close();
			break;
		}
	}
}

function mouseDown()
{
}

function drawText(text)
{
	gl.textAlign="center";
	gl.textBaseline="middle";
	gl.font="48px sans-serif";
	gl.fillStyle="white";
	gl.fillText(text, gcanvas.width/2, gcanvas.height/2);
}

function toggleFullScreen()
{
	var fullscreen_element = 
	document.fullscreenElement			||
	document.webkitFullscreenElement	||
	document.mozFullscreenElement		||
	document.msFullscreenElement		||
	null;

	//if not in fullscreen mode
	if(fullscreen_element==null)
	{
		if(gcanvas.requestFullscreen)
		{
			gcanvas.requestFullscreen();
		}
		else if(gcanvas.mozRequestFullScreen)
		{
			gcanvas.mozRequestFullScreen();
		}
		else if(gcanvas.webkitRequestFullscreen)
		{
			gcanvas.webkitRequestFullscreen();
		}
		else if(gcanvas.msRequestFullscreen)
		{
			gcanvas.msRequestFullscreen();
		}
		gbFullscreen=true;
	}
	else
	{
		if(document.exitFullscreen)
		{
			document.exitFullscreen();
		}
		else if(document.mozCancelFullscreen)
		{
			document.mozCancelFullscreen();
		}
		else if(document.webkitExitFullscreen)
		{
			document.webkitExitFullscreen();
		}
		else if(document.msExitFullscreen)
		{
			document.msExitFullscreen();
		}
		gbFullscreen=false;
	}
}

function init()
{
	gl = gcanvas.getContext("webgl2");
	if(gl==null)
	{
		console.error("failed to get webgl rendering context");
		return;
	}
	gl.viewportWidth = gcanvas.width;
	gl.viewportHeight = gcanvas.height;

	//vertex shader
	var vertexShaderSourceCode = 
	`#version 300 es

	in vec4 vPosition;
	in vec4 vColor;
	uniform mat4 u_mvp_matrix;
	out vec4 out_color;//will just be used to pass color data to fragment shader

	void main()
	{
		gl_Position = u_mvp_matrix * vPosition;
		out_color = vColor;
	}
	`;

	gVertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(gVertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(gVertexShaderObject);
	
	if(gl.getShaderParameter(gVertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gVertexShaderObject);
		if(error.length > 0)
		{
			alert("vertex shader compilation failed. see console for logs.");
			console.error(error);
			uninitialize();
		}
	}

	var fragmentShaderSourceCode = 
	`#version 300 es

	precision highp float;
	in vec4 out_color;//coming from vertex shader
	out vec4 FragColor;
	void main()
	{
		FragColor = out_color;
	}
	`;
	gFragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(gFragmentShaderObject, fragmentShaderSourceCode);
	gl.compileShader(gFragmentShaderObject);
	
	if(gl.getShaderParameter(gFragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gFragmentShaderObject);
		if(error.length > 0)
		{
			alert("fragment shader compilation failed. see console for logs.");
			console.error(error);
			uninitialize();
		}
	}

	//shader program
	gShaderProgramObject = gl.createProgram();
	gl.attachShader(gShaderProgramObject, gVertexShaderObject);
	gl.attachShader(gShaderProgramObject, gFragmentShaderObject);

	//pre-link : bind attributes
	gl.bindAttribLocation(gShaderProgramObject, gWebGLMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(gShaderProgramObject, gWebGLMacros.VDG_ATTRIBUTE_COLOR, "vColor");

	//link program
	gl.linkProgram(gShaderProgramObject);
	if(!gl.getProgramParameter(gShaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(gShaderProgramObject);
		if(error.length > 0)
		{
			alert("shader program link failed. see console logs.")
			console.error(error);
			uninitialize();
		}
	}

	gMvpuniform = gl.getUniformLocation(gShaderProgramObject, "u_mvp_matrix");


	//lines
    var vertices = [];
    var colors = [];
    var abscissa = 0, ordinate = 0;
    var factor = 2.0 /40.0;
    var x = 0;
    for (x = -1.0; x <= 1.0; x += factor)
        {
            if (x > (-factor / 2) && x < (factor / 2))
            {
                abscissa = x;
            }

            if (x > (-factor / 2) && x < (factor / 2))
            {
                colors.push(0.0, 1.0, 0.0);
                colors.push(0.0, 1.0, 0.0);
            }
            else
            {
                colors.push(1.0, 1.0, 1.0); //glColor3f(1.0f, 1.0f, 1.0f);
                colors.push(1.0, 1.0, 1.0); //glColor3f(1.0f, 1.0f, 1.0f);
            }
            vertices.push(x,-1.0, 0.0); //xyz - glVertex2f(x, -1);
            vertices.push(x, 1.0, 0.0); //xyz - glVertex2f(x,  1);
        }

        colors.push(1.0, 1.0, 1.0);
        colors.push(1.0, 1.0, 1.0);
        vertices.push(1,-1.0, 0.0);
        vertices.push(1, 1.0, 0.0);

        var y = 0;
        for (y = -1.0; y <= 1.0; y += factor)
        {
            if (y > (-factor / 2) && y < (factor / 2))
            {
                ordinate = y;
            }

            if (y > (-factor / 2) && y < (factor / 2))
            {
                colors.push(1.0, 0.0, 0.0); //glColor3f(1.0f, 0.0f, 0.0f);
                colors.push(1.0, 0.0, 0.0); //glColor3f(1.0f, 0.0f, 0.0f);
            }
            else
            {
                colors.push(1.0, 1.0, 1.0);  //glColor3f(1.0f, 1.0f, 1.0f);
                colors.push(1.0, 1.0, 1.0);  //glColor3f(1.0f, 1.0f, 1.0f);
            }

            vertices.push(-1.0, y, 0.0);
            vertices.push( 1.0, y, 0.0);
        }

        colors.push(1.0, 1.0, 1.0); 
        colors.push(1.0, 1.0, 1.0);
        vertices.push(-1.0, 1, 0.0); //glVertex2f(-1, y);
        vertices.push( 1.0, 1, 0.0); //glVertex2f(1, y);

        //Y-axis
        colors.push(0.0, 1.0, 0.0);
        vertices.push(abscissa, -1.0, 0.0); //glVertex2f(abscissa, -1);
        colors.push(0.0, 1.0, 0.0);
        vertices.push(abscissa,  1.0, 0.0); //glVertex2f(abscissa, 1);

        //X-axis
        colors.push(1.0, 0.0, 0.0); //glColor3f(1.0f, 0.0f, 0.0f);
        vertices.push(-1.0, ordinate, 0.0); //glVertex2f(-1, ordinate);
        colors.push(1.0, 0.0, 0.0); //glColor3f(1.0f, 0.0f, 0.0f);
        vertices.push(1.0, ordinate, 0.0); //glVertex2f(1, ordinate);


	var lineVertices = new Float32Array(vertices);

	var lineColor = new Float32Array(colors);

    gNumVertices = lineVertices.length;
    console.log("lineVertices.length: ", lineVertices.length);

    if(lineColor.length != lineVertices.length)
    {
        alert("list size mismatch - (lineColor.length != lineVertices.length)\nSee console for info");
        console.error(lineColor.length, lineVertices.length);
    }

	gvaoLine = gl.createVertexArray();
	gl.bindVertexArray(gvaoLine);
	gvboLinePosition = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, gvboLinePosition);
	gl.bufferData(gl.ARRAY_BUFFER, lineVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(gWebGLMacros.VDG_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(gWebGLMacros.VDG_ATTRIBUTE_VERTEX);

	gvboLineColor = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, gvboLineColor);
	gl.bufferData(gl.ARRAY_BUFFER, lineColor, gl.STATIC_DRAW);
	gl.vertexAttribPointer(gWebGLMacros.VDG_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(gWebGLMacros.VDG_ATTRIBUTE_COLOR);

	//unbind buffers
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	gl.bindVertexArray(null);

	gl.clearColor(0.0,0.0,0.0,1.0);

	gOrthoProjectionMatrix = mat4.create();
}

function resize()
{
	if(gbFullscreen==true)
	{
		gcanvas.width = window.innerWidth;
		gcanvas.height=window.innerHeight;
	}
	else
	{
		gcanvas.width = gcanvas_original_width;
		gcanvas.height = gcanvas_original_height;
	}

	gl.viewport(0,0,gcanvas.width, gcanvas.height);

    var scale = 1.0;
	if(gcanvas.width <= gcanvas.height)
	{
		var factor = scale*(gcanvas.height/gcanvas.width);
		mat4.ortho(gOrthoProjectionMatrix,
		-scale, scale,
		-factor, factor,
		-scale, scale
		);
	}
	else
	{
		var factor = scale*(gcanvas.width/gcanvas.height);
		mat4.ortho(gOrthoProjectionMatrix,
		-factor, factor,
		-scale, scale,
		-scale, scale
		);
	}
}

function draw()
{

	gl.clear(gl.COLOR_BUFFER_BIT);
	gl.useProgram(gShaderProgramObject);
	
	var modelViewMatrix = mat4.create();
	var identityMatrix  = mat4.create();

	//triangle
	var modelViewProjectionMatrix = mat4.create();
	mat4.multiply(modelViewProjectionMatrix, gOrthoProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(gMvpuniform, false, modelViewProjectionMatrix);

	gl.bindVertexArray(gvaoLine);
	gl.drawArrays(gl.LINES, 0, gNumVertices);

	gl.bindVertexArray(null);
	gl.useProgram(null);

	//animation loop
	requestAnimationFrame(draw, gcanvas);
}

function uninitialize()
{
	//code
	if(gvaoLine)
	{
		gl.deleteVertexArray(gvaoLine);
		gvaoLine = null;
	}

	if(gvboLinePosition)
	{
		gl.deleteBuffer(gvboLinePosition);
		gvboLinePosition = null;
	}

    if(gvboLineColor)
	{
		gl.deleteBuffer(gvboLineColor);
		gvboLineColor = null;
	}

	if(gShaderProgramObject)
	{
		if(gFragmentShaderObject)
		{
			gl.detachShader(gShaderProgramObject, gFragmentShaderObject);
			gl.deleteShader(gFragmentShaderObject);
			gFragmentShaderObject = null;
		}

		if(gVertexShaderObject)
		{
			gl.detachShader(gShaderProgramObject, gVertexShaderObject);
			gl.deleteShader(gVertexShaderObject);
			gVertexShaderObject = null;
		}

		gl.deleteProgram(gShaderProgramObject);
		gShaderProgramObject = null;
	}
}