//onload function
var gcanvas = null;
var gl = null;
var gbFullscreen=false;
var gcanvas_original_width = 0 ;
var gcanvas_original_height = 0;

const gWebGLMacros = 
{
	VDG_ATTRIBUTE_VERTEX   : 0,
	VDG_ATTRIBUTE_COLOR    : 1,
	VDG_ATTRIBUTE_NORMAL   : 2,
	VDG_ATTRIBUTE_TEXTURE0 : 3,
};

var gVertexShaderObject = null;
var gFragmentShaderObject = null;
var gShaderProgramObject = null;

var gvaoLine = -1;
var gvboLineColor = -1;
var gvboLinePosition = -1;
var gMvpuniform = null;
var gOrthoProjectionMatrix = null;

var gNumKeyPressed = 0;
var gNumVerticesTriangle = 0;
var gNumVerticesCircle = 0;
var gNumVerticesLine = 0;
var gInitPos = -2.0;
var gTrianglePos = gInitPos;
var gCirclePos = -gInitPos;
var gLinePos = -gInitPos;
var gRotationAngle = 0.0;

var requestAnimationFrame = 
window.requestAnimationFrame		||
window.webkitRequestAnimationFrame  ||
window.mozRequestAnimationFrame		||
window.oRequestAnimationFrame		||
window.msRequestAnimationFrame;

var cancelAnimationFrame = 
window.cancelAnimationFrame					||
window.webkitCancelRequestAnimationFrame	|| window.webkitCancelAnimationFrame	||
window.mozCancelRequestAnimationFrame		|| window.mozCancelAnimationFrame		||
window.oCancelRequestAnimationFrame			|| window.oCancelAnimationFrame			||
window.msCancelRequestAnimationFrame		|| window.msCancelAnimationFrame;

function main()
{
	gcanvas = document.getElementById("AMC");
	if(!gcanvas)
	{
		console.error("obtaining canvas failed");
	}
	else
	{
		console.log("obtaining canvas success");
	}

	console.log("canvas width : " + gcanvas.width + " height: " + gcanvas.height);
	gcanvas_original_width = gcanvas.width;
	gcanvas_original_height = gcanvas.height;

	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);

	init();

	resize();
	draw();
}

function keyDown(event)
{
	if(event.keyCode >= 'A'.charCodeAt(0) && event.keyCode <= 'Z'.charCodeAt(0))
	{
		var chr = String.fromCharCode(event.keyCode);
		switch(chr)
		{
			case 'F':
				toggleFullScreen();
				break;
		}
	}
    else if(event.keyCode >= '0'.charCodeAt(0) && event.keyCode <= '9'.charCodeAt(0))
    {
        gNumKeyPressed = event.keyCode - 48;
        var chr = String.fromCharCode(event.keyCode);
		switch(chr)
		{
			case '0':
				break;
		}
    }
	else
	{//other keys
		switch(event.keyCode)
		{
			case 27://escape
			uninitialize();
			window.close();
			break;
		}
	}
}

function mouseDown()
{
}

function drawText(text)
{
	gl.textAlign="center";
	gl.textBaseline="middle";
	gl.font="48px sans-serif";
	gl.fillStyle="white";
	gl.fillText(text, gcanvas.width/2, gcanvas.height/2);
}

function toggleFullScreen()
{
	var fullscreen_element = 
	document.fullscreenElement			||
	document.webkitFullscreenElement	||
	document.mozFullscreenElement		||
	document.msFullscreenElement		||
	null;

	//if not in fullscreen mode
	if(fullscreen_element==null)
	{
		if(gcanvas.requestFullscreen)
		{
			gcanvas.requestFullscreen();
		}
		else if(gcanvas.mozRequestFullScreen)
		{
			gcanvas.mozRequestFullScreen();
		}
		else if(gcanvas.webkitRequestFullscreen)
		{
			gcanvas.webkitRequestFullscreen();
		}
		else if(gcanvas.msRequestFullscreen)
		{
			gcanvas.msRequestFullscreen();
		}
		gbFullscreen=true;
	}
	else
	{
		if(document.exitFullscreen)
		{
			document.exitFullscreen();
		}
		else if(document.mozCancelFullscreen)
		{
			document.mozCancelFullscreen();
		}
		else if(document.webkitExitFullscreen)
		{
			document.webkitExitFullscreen();
		}
		else if(document.msExitFullscreen)
		{
			document.msExitFullscreen();
		}
		gbFullscreen=false;
	}
}

function init()
{
	gl = gcanvas.getContext("webgl2");
	if(gl==null)
	{
		console.error("failed to get webgl rendering context");
		return;
	}
	gl.viewportWidth = gcanvas.width;
	gl.viewportHeight = gcanvas.height;

	//vertex shader
	var vertexShaderSourceCode = 
	`#version 300 es

	in vec4 vPosition;
	in vec4 vColor;
	uniform mat4 u_mvp_matrix;
	out vec4 out_color;//will just be used to pass color data to fragment shader

	void main()
	{
		gl_Position = u_mvp_matrix * vPosition;
		out_color = vColor;
	}
	`;

	gVertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(gVertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(gVertexShaderObject);
	
	if(gl.getShaderParameter(gVertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gVertexShaderObject);
		if(error.length > 0)
		{
			alert("vertex shader compilation failed. see console for logs.");
			console.error(error);
			uninitialize();
		}
	}

	var fragmentShaderSourceCode = 
	`#version 300 es

	precision highp float;
	in vec4 out_color;//coming from vertex shader
	out vec4 FragColor;
	void main()
	{
		FragColor = out_color;
	}
	`;
	gFragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(gFragmentShaderObject, fragmentShaderSourceCode);
	gl.compileShader(gFragmentShaderObject);
	
	if(gl.getShaderParameter(gFragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gFragmentShaderObject);
		if(error.length > 0)
		{
			alert("fragment shader compilation failed. see console for logs.");
			console.error(error);
			uninitialize();
		}
	}

	//shader program
	gShaderProgramObject = gl.createProgram();
	gl.attachShader(gShaderProgramObject, gVertexShaderObject);
	gl.attachShader(gShaderProgramObject, gFragmentShaderObject);

	//pre-link : bind attributes
	gl.bindAttribLocation(gShaderProgramObject, gWebGLMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(gShaderProgramObject, gWebGLMacros.VDG_ATTRIBUTE_COLOR, "vColor");

	//link program
	gl.linkProgram(gShaderProgramObject);
	if(!gl.getProgramParameter(gShaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(gShaderProgramObject);
		if(error.length > 0)
		{
			alert("shader program link failed. see console logs.")
			console.error(error);
			uninitialize();
		}
	}

	gMvpuniform = gl.getUniformLocation(gShaderProgramObject, "u_mvp_matrix");


	//lines
    var vertices = [];
    var colors = [];

        //add vertices for triangle
        var inRadius = 0.3;
        var p1 = [0, inRadius/Math.sin(Math.PI/6),0];
        var p2 = [-1.0*inRadius/Math.tan(Math.PI/6), -1.0*inRadius,0];
        var p3 = [ inRadius/Math.tan(Math.PI/6), -1*inRadius,0];

        colors.push(1.0, 1.0, 1.0); 
        vertices = vertices.concat(p1);
        colors.push(1.0, 1.0, 1.0); 
        //vertices.push( -scale, -scale, 0.0);
        vertices = vertices.concat(p2);
        colors.push(1.0, 1.0, 1.0); 
        //vertices.push( -scale, -scale, 0.0);
        vertices = vertices.concat(p2);
        colors.push(1.0, 1.0, 1.0); 
        //vertices.push( scale, -scale, 0.0);
        vertices = vertices.concat(p3);
        colors.push(1.0, 1.0, 1.0); 
        //vertices.push( scale, -scale, 0.0);
        vertices = vertices.concat(p3);
        colors.push(1.0, 1.0, 1.0); 
        //vertices.push( 0.0, scale, 0.0);
        vertices = vertices.concat(p1);

        gNumVerticesTriangle = vertices.length;
        gNumVerticesTriangle /= 3;
        console.log("vertices.length: ", vertices.length, " gNumVerticesTriangle: ", gNumVerticesTriangle);
        if(vertices.length % 2 != 0)
        {
            alert("vertices.length % 2 != 0");
        }

        //vertices for circle
        var i = 0;
        colors.push(1.0, 1.0, 1.0);
        vertices.push( inRadius*Math.cos(0), inRadius*Math.sin(0), 0.0);
        for(i = 0; i <= 2*Math.PI ; i+=0.1)
        {
            var y = inRadius*Math.sin(i);
            var x = inRadius*Math.cos(i);
            colors.push(1.0, 1.0, 1.0);
            vertices.push( x, y, 0.0);
            colors.push(1.0, 1.0, 1.0);
            vertices.push( x, y, 0.0);
        }
        colors.push(1.0, 1.0, 1.0);
        vertices.push( inRadius*Math.cos(0), inRadius*Math.sin(0), 0.0);

        gNumVerticesCircle = vertices.length - 3*gNumVerticesTriangle;
        gNumVerticesCircle /= 3;
        console.log("vertices.length: ", vertices.length, " gNumVerticesCircle: ", gNumVerticesCircle);
        if(vertices.length % 2 != 0)
        {
            alert("vertices.length % 2 != 0");
        }

        //vertices for line
        colors.push(1.0, 1.0, 1.0);
        //vertices.push( inRadius*Math.cos(Math.PI/2.0), inRadius*Math.sin(Math.PI/2.0), 0.0);
        vertices = vertices.concat(p1);
        colors.push(1.0, 1.0, 1.0);
        vertices.push( inRadius*Math.cos(Math.PI/2.0), inRadius*Math.sin(3.0*Math.PI/2.0), 0.0);

        gNumVerticesLine = vertices.length - 3*gNumVerticesCircle - 3*gNumVerticesTriangle
        gNumVerticesLine /= 3;
        console.log("vertices.length: ", vertices.length, " gNumVerticesLine: ", gNumVerticesLine);
        if(vertices.length % 2 != 0)
        {
            alert("vertices.length % 2 != 0");
        }

	var lineVertices = new Float32Array(vertices);

	var lineColor = new Float32Array(colors);

    console.log("lineVertices.length: ", lineVertices.length);

    if(lineColor.length != lineVertices.length)
    {
        alert("list size mismatch - (lineColor.length != lineVertices.length)\nSee console for info");
        console.error(lineColor.length, lineVertices.length);
    }

	gvaoLine = gl.createVertexArray();
	gl.bindVertexArray(gvaoLine);

	gvboLinePosition = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, gvboLinePosition);
	gl.bufferData(gl.ARRAY_BUFFER, lineVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(gWebGLMacros.VDG_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(gWebGLMacros.VDG_ATTRIBUTE_VERTEX);

	gvboLineColor = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, gvboLineColor);
	gl.bufferData(gl.ARRAY_BUFFER, lineColor, gl.STATIC_DRAW);
	gl.vertexAttribPointer(gWebGLMacros.VDG_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(gWebGLMacros.VDG_ATTRIBUTE_COLOR);

	//unbind buffers
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	gl.bindVertexArray(null);

	gl.clearColor(0.0,0.0,0.0,1.0);

	gOrthoProjectionMatrix = mat4.create();
}

function resize()
{
	if(gbFullscreen==true)
	{
		gcanvas.width = window.innerWidth;
		gcanvas.height=window.innerHeight;
	}
	else
	{
		gcanvas.width = gcanvas_original_width;
		gcanvas.height = gcanvas_original_height;
	}

	gl.viewport(0,0,gcanvas.width, gcanvas.height);

    var scale = 1.0;
	if(gcanvas.width <= gcanvas.height)
	{
		var factor = scale*(gcanvas.height/gcanvas.width);
		mat4.ortho(gOrthoProjectionMatrix,
		-scale, scale,
		-factor, factor,
		-scale, scale
		);
	}
	else
	{
		var factor = scale*(gcanvas.width/gcanvas.height);
		mat4.ortho(gOrthoProjectionMatrix,
		-factor, factor,
		-scale, scale,
		-scale, scale
		);
	}
}

function draw()
{

	gl.clear(gl.COLOR_BUFFER_BIT);
	gl.useProgram(gShaderProgramObject);
	
	var modelViewMatrix = mat4.create();
	var identityMatrix  = mat4.create();

	//triangle
	gl.bindVertexArray(gvaoLine);
    var modelViewProjectionMatrix = mat4.create();
    mat4.translate(modelViewMatrix, modelViewMatrix, [gTrianglePos, gTrianglePos, 0]);
    mat4.rotate(modelViewMatrix, modelViewMatrix, gRotationAngle, [0,1,0]);
	mat4.multiply(modelViewProjectionMatrix, gOrthoProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(gMvpuniform, false, modelViewProjectionMatrix);
	gl.drawArrays(gl.LINES, 0, gNumVerticesTriangle);//triangle

    mat4.identity(modelViewMatrix);
    mat4.translate(modelViewMatrix, modelViewMatrix, [gCirclePos, -gCirclePos, 0]);
    mat4.rotate(modelViewMatrix, modelViewMatrix, gRotationAngle, [0,1,0]);
	mat4.multiply(modelViewProjectionMatrix, gOrthoProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(gMvpuniform, false, modelViewProjectionMatrix);
	gl.drawArrays(gl.LINES, gNumVerticesTriangle, gNumVerticesCircle);

    mat4.identity(modelViewMatrix);
    mat4.translate(modelViewMatrix, modelViewMatrix, [0, gLinePos, 0]);
    mat4.rotate(modelViewMatrix, modelViewMatrix, gRotationAngle, [0,1,0]);
	mat4.multiply(modelViewProjectionMatrix, gOrthoProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(gMvpuniform, false, modelViewProjectionMatrix);
    gl.drawArrays(gl.LINES, gNumVerticesTriangle + gNumVerticesCircle, gNumVerticesLine);

    var step        = 0.01;

    var dist0 = vec2.dist([gTrianglePos, gTrianglePos], [0.0,0.0]);
    //var dist1 = Math.abs(gTrianglePos-gCirclePos);
    var dist1 = vec2.dist([gTrianglePos, gTrianglePos], [gCirclePos, -gCirclePos]);
    //var dist2 = Math.abs(gTrianglePos-gLinePos);
    var dist2 = vec2.dist([gTrianglePos, gTrianglePos], [0, gLinePos]);

    if(dist0 < step)
    {
        gTrianglePos = 0.0;
        gCirclePos     -= step;
    }
    else
    {
        gTrianglePos   += step;
    }

    if(dist1 <= step)
    {
        gTrianglePos = 0.0;
        gCirclePos = 0.0;
        gLinePos -= step;
    }

    if(dist2 <= step)
    {
        gLinePos = 0.0;
        gRotationAngle += step;
    }

	gl.bindVertexArray(null);
	gl.useProgram(null);

	//animation loop
	requestAnimationFrame(draw, gcanvas);
}

function uninitialize()
{
	//code
	if(gvaoLine)
	{
		gl.deleteVertexArray(gvaoLine);
		gvaoLine = null;
	}

	if(gvboLinePosition)
	{
		gl.deleteBuffer(gvboLinePosition);
		gvboLinePosition = null;
	}

    if(gvboLineColor)
	{
		gl.deleteBuffer(gvboLineColor);
		gvboLineColor = null;
	}

	if(gShaderProgramObject)
	{
		if(gFragmentShaderObject)
		{
			gl.detachShader(gShaderProgramObject, gFragmentShaderObject);
			gl.deleteShader(gFragmentShaderObject);
			gFragmentShaderObject = null;
		}

		if(gVertexShaderObject)
		{
			gl.detachShader(gShaderProgramObject, gVertexShaderObject);
			gl.deleteShader(gVertexShaderObject);
			gVertexShaderObject = null;
		}

		gl.deleteProgram(gShaderProgramObject);
		gShaderProgramObject = null;
	}
}