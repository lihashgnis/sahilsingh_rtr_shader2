//onload function
var gcanvas = null;
var gl = null;
var gbFullscreen=false;
var gcanvas_original_width = 0 ;
var gcanvas_original_height = 0;

const gWebGLMacros = 
{
	VDG_ATTRIBUTE_VERTEX   : 0,
	VDG_ATTRIBUTE_COLOR    : 1,
	VDG_ATTRIBUTE_NORMAL   : 2,
	VDG_ATTRIBUTE_TEXTURE0 : 3,
};

//TEXT
//monospace font - letters drawn in 4th quadrant
var gCellHeight = 0.4;
var gCellWidth  = 0.2;
var gGap = gCellWidth/2.0;

var gVertexShaderObject = null;
var gFragmentShaderObject = null;
var gShaderProgramObject = null;

var gvaoLine = -1;
var gvboLineColor = -1;
var gvboLinePosition = -1;
var gMvpuniform = null;
var gOrthoProjectionMatrix = null;
var gColorsList = [];

var gNumKeyPressed = 0;
var gStartNumVertices = {};//eg. gStartNumVertices['I']=[a,b] a - vertex start. b- no. of vertex required for rendering I

var gRotationAngle = 0.0;
var gI1Pos = [0,0];
var gNPos  = [0,0];
var gDPos  = [0,0];
var gI2Pos = [0,0];
var gAPos  = [0,0];
var gDAlpha = 0;
var gPlane1Pos = [0,0];
var gPlane2Pos = [0,0];
var gPlane3Pos = [0,0];

var gPlaneMeetX = 0;

var gStep = gGap/10.0;

var gDeltaPlane1 = [gStep, -gStep];
var gDeltaPlane2 = [gStep,  0];
var gDeltaPlane3 = [gStep,  gStep];

var gbPlanesMet = false;

var gI1PosFinal = [0, 0];
var gNPosFinal  = [0, 0];
var gDPosFinal  = [0, 0];
var gI2PosFinal = [0, 0];
var gAPosFinal  = [0, 0];

var requestAnimationFrame = 
window.requestAnimationFrame		||
window.webkitRequestAnimationFrame  ||
window.mozRequestAnimationFrame		||
window.oRequestAnimationFrame		||
window.msRequestAnimationFrame;

var cancelAnimationFrame = 
window.cancelAnimationFrame					||
window.webkitCancelRequestAnimationFrame	|| window.webkitCancelAnimationFrame	||
window.mozCancelRequestAnimationFrame		|| window.mozCancelAnimationFrame		||
window.oCancelRequestAnimationFrame			|| window.oCancelAnimationFrame			||
window.msCancelRequestAnimationFrame		|| window.msCancelAnimationFrame;

function main()
{
	gcanvas = document.getElementById("AMC");
	if(!gcanvas)
	{
		console.error("obtaining canvas failed");
	}
	else
	{
		console.log("obtaining canvas success");
	}

	console.log("canvas width : " + gcanvas.width + " height: " + gcanvas.height);
	gcanvas_original_width = gcanvas.width;
	gcanvas_original_height = gcanvas.height;

	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);

	init();

	resize();
	draw();
}

function keyDown(event)
{
	if(event.keyCode >= 'A'.charCodeAt(0) && event.keyCode <= 'Z'.charCodeAt(0))
	{
		var chr = String.fromCharCode(event.keyCode);
		switch(chr)
		{
			case 'F':
				toggleFullScreen();
				break;
		}
	}
    else if(event.keyCode >= '0'.charCodeAt(0) && event.keyCode <= '9'.charCodeAt(0))
    {
        gNumKeyPressed = event.keyCode - 48;
        var chr = String.fromCharCode(event.keyCode);
		switch(chr)
		{
			case '0':
				break;
		}
    }
	else
	{//other keys
		switch(event.keyCode)
		{
			case 27://escape
			uninitialize();
			window.close();
			break;
		}
	}
}

function mouseDown()
{
}

function drawText(text)
{
	gl.textAlign="center";
	gl.textBaseline="middle";
	gl.font="48px sans-serif";
	gl.fillStyle="white";
	gl.fillText(text, gcanvas.width/2, gcanvas.height/2);
}

function toggleFullScreen()
{
	var fullscreen_element = 
	document.fullscreenElement			||
	document.webkitFullscreenElement	||
	document.mozFullscreenElement		||
	document.msFullscreenElement		||
	null;

	//if not in fullscreen mode
	if(fullscreen_element==null)
	{
		if(gcanvas.requestFullscreen)
		{
			gcanvas.requestFullscreen();
		}
		else if(gcanvas.mozRequestFullScreen)
		{
			gcanvas.mozRequestFullScreen();
		}
		else if(gcanvas.webkitRequestFullscreen)
		{
			gcanvas.webkitRequestFullscreen();
		}
		else if(gcanvas.msRequestFullscreen)
		{
			gcanvas.msRequestFullscreen();
		}
		gbFullscreen=true;
	}
	else
	{
		if(document.exitFullscreen)
		{
			document.exitFullscreen();
		}
		else if(document.mozCancelFullscreen)
		{
			document.mozCancelFullscreen();
		}
		else if(document.webkitExitFullscreen)
		{
			document.webkitExitFullscreen();
		}
		else if(document.msExitFullscreen)
		{
			document.msExitFullscreen();
		}
		gbFullscreen=false;
	}
}

function init()
{
	gl = gcanvas.getContext("webgl2");
	if(gl==null)
	{
		console.error("failed to get webgl rendering context");
		return;
	}
	gl.viewportWidth = gcanvas.width;
	gl.viewportHeight = gcanvas.height;

	//vertex shader
	var vertexShaderSourceCode = 
	`#version 300 es

	in vec4 vPosition;
	in vec4 vColor;
	uniform mat4 u_mvp_matrix;
	out vec4 out_color;//will just be used to pass color data to fragment shader

	void main()
	{
		gl_Position = u_mvp_matrix * vPosition;
		out_color = vColor;
	}
	`;

	gVertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(gVertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(gVertexShaderObject);
	
	if(gl.getShaderParameter(gVertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gVertexShaderObject);
		if(error.length > 0)
		{
			alert("vertex shader compilation failed. see console for logs.");
			console.error(error);
			uninitialize();
		}
	}

	var fragmentShaderSourceCode = 
	`#version 300 es

	precision highp float;
	in vec4 out_color;//coming from vertex shader
	out vec4 FragColor;
	void main()
	{
		FragColor = out_color;
        if(FragColor.a <= 0.0)
        {
            discard;
        }
        FragColor.rgb *= FragColor.a;//will work as long as background is black
        FragColor.a = 1.0;
	}
	`;
	gFragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(gFragmentShaderObject, fragmentShaderSourceCode);
	gl.compileShader(gFragmentShaderObject);
	
	if(gl.getShaderParameter(gFragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gFragmentShaderObject);
		if(error.length > 0)
		{
			alert("fragment shader compilation failed. see console for logs.");
			console.error(error);
			uninitialize();
		}
	}

	//shader program
	gShaderProgramObject = gl.createProgram();
	gl.attachShader(gShaderProgramObject, gVertexShaderObject);
	gl.attachShader(gShaderProgramObject, gFragmentShaderObject);

	//pre-link : bind attributes
	gl.bindAttribLocation(gShaderProgramObject, gWebGLMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(gShaderProgramObject, gWebGLMacros.VDG_ATTRIBUTE_COLOR, "vColor");

	//link program
	gl.linkProgram(gShaderProgramObject);
	if(!gl.getProgramParameter(gShaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(gShaderProgramObject);
		if(error.length > 0)
		{
			alert("shader program link failed. see console logs.")
			console.error(error);
			uninitialize();
		}
	}

	gMvpuniform = gl.getUniformLocation(gShaderProgramObject, "u_mvp_matrix");


	//lines
    var vertices = [];
    var colors = [];

    var vertStart = 0;
    var vertNum = 0;

    //INDIA_SAFRON = RGB(0xFF, 0x99, 0x33);
    //INDIA_GREEN = RGB(0x13, 0x88, 0x08);
    var INDIA_SAFRON = [0xFF/255.0, 0x99/255.0, 0x33/255.0, 1.0];
    var INDIA_GREEN = [0x13/255.0, 0x88/255.0, 0x08/255.0, 1.0];

        //add vertices for I
        vertStart = 0;
        vertEnd = 0;
        colors=colors.concat(INDIA_SAFRON);
        vertices.push(gCellWidth,0,0);
        colors=colors.concat(INDIA_SAFRON);
        vertices.push(0,0,0);
        colors=colors.concat(INDIA_SAFRON);
        vertices.push(gCellWidth/2.0,0,0);
        colors=colors.concat(INDIA_GREEN);
        vertices.push(gCellWidth/2.0,-gCellHeight,0);
        colors=colors.concat(INDIA_GREEN);
        vertices.push(gCellWidth,-gCellHeight,0);
        colors=colors.concat(INDIA_GREEN);
        vertices.push(0,-gCellHeight,0);
        vertNum = vertices.length;
        gStartNumVertices["I"] = [vertStart/3.0, vertNum/3.0];

        console.log("vertices.length: ", vertices.length, "I: ", vertStart/3.0, vertNum/3.0);
        if(vertices.length % 2 != 0)
        {
            alert("vertices.length % 2 != 0");
        }
        
        //add vertices for N
        vertStart = vertices.length;
        vertEnd = 0;
        colors=colors.concat(INDIA_GREEN);
        vertices.push(gCellWidth,-gCellHeight,0);
        colors=colors.concat(INDIA_SAFRON);
        vertices.push(gCellWidth,0,0);
        colors=colors.concat(INDIA_GREEN);
        vertices.push(gCellWidth,-gCellHeight,0);
        colors=colors.concat(INDIA_SAFRON);
        vertices.push(0,0,0);
        colors=colors.concat(INDIA_GREEN);
        vertices.push(0,-gCellHeight,0);
        colors=colors.concat(INDIA_SAFRON);
        vertices.push(0,0,0);
        vertNum = vertices.length - vertStart;
        gStartNumVertices["N"] = [vertStart/3.0, vertNum/3.0];

        console.log("vertices.length: ", vertices.length, "N: ", vertStart/3.0, vertNum/3.0);
        if(vertices.length % 2 != 0)
        {
            alert("vertices.length % 2 != 0");
        }


        //add vertices for D
        INDIA_SAFRON[3] = gDAlpha;//D is hidden initially
        INDIA_GREEN[3] =  gDAlpha;
        vertStart = vertices.length;
        vertEnd = 0;
        colors=colors.concat(INDIA_GREEN);
        vertices.push(gCellWidth,-gCellHeight,0);
        colors=colors.concat(INDIA_SAFRON);
        vertices.push(gCellWidth,0,0);
        colors=colors.concat(INDIA_SAFRON);
        vertices.push(gCellWidth,0,0);
        colors=colors.concat(INDIA_SAFRON);
        vertices.push(0,0,0);
        colors=colors.concat(INDIA_SAFRON);
        vertices.push(0,0,0);
        colors=colors.concat(INDIA_GREEN);
        vertices.push(0,-gCellHeight,0);
        colors=colors.concat(INDIA_GREEN);
        vertices.push(0,-gCellHeight,0);
        colors=colors.concat(INDIA_GREEN);
        vertices.push(gCellWidth,-gCellHeight,0);

        INDIA_SAFRON[3] = 1.0;//D is hidden initially, other chars are not
        INDIA_GREEN[3] =  1.0;

        vertNum = vertices.length - vertStart;
        gStartNumVertices["D"] = [vertStart/3.0, vertNum/3.0];

        console.log("vertices.length: ", vertices.length, "D: ", vertStart/3.0, vertNum/3.0);
        if(vertices.length % 2 != 0)
        {
            alert("vertices.length % 2 != 0");
        }

        //add vertices for A
        vertStart = vertices.length;
        vertEnd = 0;
        colors=colors.concat(INDIA_GREEN);
        vertices.push(gCellWidth,-gCellHeight,0);
        colors=colors.concat(INDIA_SAFRON);
        vertices.push(gCellWidth/2.0,0,0);
        colors=colors.concat(INDIA_SAFRON);
        vertices.push(gCellWidth/2.0,0,0);
        colors=colors.concat(INDIA_GREEN);
        vertices.push(0,-gCellHeight,0);

        vertNum = vertices.length - vertStart;
        gStartNumVertices["A"] = [vertStart/3.0, vertNum/3.0];

        console.log("vertices.length: ", vertices.length, "A: ", vertStart/3.0, vertNum/3.0);
        if(vertices.length % 2 != 0)
        {
            alert("vertices.length % 2 != 0");
        }

        //Airplane Triangles
        let factor = 0.2;
        let AIRPLANE_COLOR = [186 / 255.0, 226 / 255.0, 238 / 255.0, 1.0];
        vertStart = vertices.length;
        //plane triangle 1
        colors=colors.concat(AIRPLANE_COLOR);
        vertices.push(gCellWidth, -gCellHeight/2.0, 0);
        colors=colors.concat(AIRPLANE_COLOR);
        vertices.push(0, 0, 0);
        colors=colors.concat(AIRPLANE_COLOR);
        vertices.push(factor*gCellWidth, -gCellHeight/2.0, 0);
        //plane triangle 2
        colors=colors.concat(AIRPLANE_COLOR);
        vertices.push(factor*gCellWidth, -gCellHeight/2.0, 0);
        colors=colors.concat(AIRPLANE_COLOR);
        vertices.push(0, -gCellHeight, 0);
        colors=colors.concat(AIRPLANE_COLOR);
        vertices.push(gCellWidth, -gCellHeight/2.0, 0);

        vertNum = vertices.length - vertStart;
        gStartNumVertices["PLANE"] = [vertStart/3.0, vertNum/3.0];
        console.log("vertices.length: ", vertices.length, "PLANE: ", vertStart/3.0, vertNum/3.0);

    gColorsList = colors.slice();//copy to global variable

	var lineVertices = new Float32Array(vertices);

	var lineColor = new Float32Array(colors);

    console.log("lineVertices.length: ", lineVertices.length);

    if(lineColor.length/4.0 != lineVertices.length/3.0)
    {
        alert("list size mismatch - (lineColor.length != lineVertices.length)\nSee console for info");
        console.error(lineColor.length, lineVertices.length);
    }

	gvaoLine = gl.createVertexArray();
	gl.bindVertexArray(gvaoLine);

	gvboLinePosition = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, gvboLinePosition);
	gl.bufferData(gl.ARRAY_BUFFER, lineVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(gWebGLMacros.VDG_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(gWebGLMacros.VDG_ATTRIBUTE_VERTEX);

	gvboLineColor = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, gvboLineColor);
	gl.bufferData(gl.ARRAY_BUFFER, lineColor, gl.DYNAMIC_DRAW);
	gl.vertexAttribPointer(gWebGLMacros.VDG_ATTRIBUTE_COLOR, 4, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(gWebGLMacros.VDG_ATTRIBUTE_COLOR);

	//unbind buffers
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	gl.bindVertexArray(null);

	gl.clearColor(0.0,0.0,0.0,1.0);

	gOrthoProjectionMatrix = mat4.create();

    //init positions
    let finalY = gCellHeight/2.0;
    //let initXLeft    = -window.innerWidth/2.0  - gCellWidth  - gGap;
    //let initXRight   =  window.innerWidth/2.0  + gCellWidth  + gGap;
    //let initYTop     =  window.innerHeight/2.0 + gCellHeight + gGap;
    //let initYBottom  = -window.innerHeight/2.0 - gCellHeight - gGap;

    let initXLeft    = -2;
    let initXRight   =  2;
    let initYTop     =  2;
    let initYBottom  = -2;

    let chrNum = 0;
    let totWidth = gCellWidth*5 + gGap*4;

    gI1PosFinal = [-totWidth/2.0 + chrNum*(gCellWidth + gGap), finalY];
    chrNum++;
    gI1Pos = gI1PosFinal.slice();
    gI1Pos[0] = initXLeft;

    gNPosFinal = [-totWidth/2.0 + chrNum*(gCellWidth + gGap), finalY];
    chrNum++;
    gNPos = gNPosFinal.slice();
    gNPos[1] = initYTop;

    gDPosFinal = [-totWidth/2.0 + chrNum*(gCellWidth + gGap), finalY];
    chrNum++;
    gDPos = gDPosFinal.slice();

    gI2PosFinal = [-totWidth/2.0 + chrNum*(gCellWidth + gGap), finalY];
    chrNum++;
    gI2Pos = gI2PosFinal.slice();
    gI2Pos[1] = initYBottom;

    gAPosFinal = [-totWidth/2.0 + chrNum*(gCellWidth + gGap), finalY];
    chrNum++;
    gAPos = gAPosFinal.slice();
    gAPos[0] = initXRight;

    gPlane1Pos = [initXLeft, initYTop];
    gPlane2Pos = [initXLeft, 0];
    gPlane3Pos = [initXLeft, initYBottom];

    gl.enable(gl.BLEND);
}

function resize()
{
	if(gbFullscreen==true)
	{
		gcanvas.width = window.innerWidth;
		gcanvas.height = window.innerHeight;
	}
	else
	{
		gcanvas.width = gcanvas_original_width;
		gcanvas.height = gcanvas_original_height;
	}

	gl.viewport(0,0,gcanvas.width, gcanvas.height);

    var scale = 1.0;
	if(gcanvas.width <= gcanvas.height)
	{
		var factor = scale*(gcanvas.height/gcanvas.width);
		mat4.ortho(gOrthoProjectionMatrix,
		-scale, scale,
		-factor, factor,
		-scale, scale
		);
	}
	else
	{
		var factor = scale*(gcanvas.width/gcanvas.height);
		mat4.ortho(gOrthoProjectionMatrix,
		-factor, factor,
		-scale, scale,
		-scale, scale
		);
	}
}

function draw()
{

	gl.clear(gl.COLOR_BUFFER_BIT);
	gl.useProgram(gShaderProgramObject);

	var modelViewMatrix = mat4.create();
	var identityMatrix  = mat4.create();
	
    let chrNum = 0;
    //I
    var chr = "I";
    pos = gI1Pos.slice();
	gl.bindVertexArray(gvaoLine);
    mat4.identity(modelViewMatrix);
    var modelViewProjectionMatrix = mat4.create();
    mat4.translate(modelViewMatrix, modelViewMatrix, [pos[0], pos[1], 0]);
    //mat4.rotate(modelViewMatrix, modelViewMatrix, gRotationAngle, [0,1,0]);
	mat4.multiply(modelViewProjectionMatrix, gOrthoProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(gMvpuniform, false, modelViewProjectionMatrix);
	gl.drawArrays(gl.LINES, gStartNumVertices[chr][0], gStartNumVertices[chr][1]);
    chrNum++;

    //N
    var chr = "N";
    pos = gNPos.slice();
	gl.bindVertexArray(gvaoLine);
    mat4.identity(modelViewMatrix);
    var modelViewProjectionMatrix = mat4.create();
    mat4.translate(modelViewMatrix, modelViewMatrix, [pos[0], pos[1], 0]);
    //mat4.rotate(modelViewMatrix, modelViewMatrix, gRotationAngle, [0,1,0]);
	mat4.multiply(modelViewProjectionMatrix, gOrthoProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(gMvpuniform, false, modelViewProjectionMatrix);
	gl.drawArrays(gl.LINES, gStartNumVertices[chr][0], gStartNumVertices[chr][1]);
    chrNum++;

    //D
    var chr = "D";
    pos = gDPos.slice();
	gl.bindVertexArray(gvaoLine);
    mat4.identity(modelViewMatrix);
    var modelViewProjectionMatrix = mat4.create();
    mat4.translate(modelViewMatrix, modelViewMatrix, [pos[0], pos[1], 0]);
    //mat4.rotate(modelViewMatrix, modelViewMatrix, gRotationAngle, [0,1,0]);
	mat4.multiply(modelViewProjectionMatrix, gOrthoProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(gMvpuniform, false, modelViewProjectionMatrix);
	gl.drawArrays(gl.LINES, gStartNumVertices[chr][0], gStartNumVertices[chr][1]);
    chrNum++;

    //I
    var chr = "I";
    pos = gI2Pos.slice();
	gl.bindVertexArray(gvaoLine);
    mat4.identity(modelViewMatrix);
    var modelViewProjectionMatrix = mat4.create();
    mat4.translate(modelViewMatrix, modelViewMatrix, [pos[0], pos[1], 0]);
    //mat4.rotate(modelViewMatrix, modelViewMatrix, gRotationAngle, [0,1,0]);
	mat4.multiply(modelViewProjectionMatrix, gOrthoProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(gMvpuniform, false, modelViewProjectionMatrix);
	gl.drawArrays(gl.LINES, gStartNumVertices[chr][0], gStartNumVertices[chr][1]);
    chrNum++;

    //A
    var chr = "A";
    pos = gAPos.slice();
	gl.bindVertexArray(gvaoLine);
    mat4.identity(modelViewMatrix);
    var modelViewProjectionMatrix = mat4.create();
    mat4.translate(modelViewMatrix, modelViewMatrix, [pos[0], pos[1], 0]);
    //mat4.rotate(modelViewMatrix, modelViewMatrix, gRotationAngle, [0,1,0]);
	mat4.multiply(modelViewProjectionMatrix, gOrthoProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(gMvpuniform, false, modelViewProjectionMatrix);
	gl.drawArrays(gl.LINES, gStartNumVertices[chr][0], gStartNumVertices[chr][1]);
    chrNum++;


    //Airplanes
    //Plane1
    var chr = "PLANE";
    pos = gPlane1Pos;
	gl.bindVertexArray(gvaoLine);
    mat4.identity(modelViewMatrix);
    var modelViewProjectionMatrix = mat4.create();
    mat4.translate(modelViewMatrix, modelViewMatrix, [gCellWidth/2.0, gCellHeight/2.0, 0]);//bring plane from 4th qudrant to origin
    mat4.translate(modelViewMatrix, modelViewMatrix, [pos[0], pos[1], 0]);
    //mat4.rotate(modelViewMatrix, modelViewMatrix, gRotationAngle, [0,1,0]);
	mat4.multiply(modelViewProjectionMatrix, gOrthoProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(gMvpuniform, false, modelViewProjectionMatrix);
	gl.drawArrays(gl.TRIANGLES, gStartNumVertices[chr][0], gStartNumVertices[chr][1]);
    chrNum++;

    //Plane2
    var chr = "PLANE";
    pos = gPlane2Pos;
	gl.bindVertexArray(gvaoLine);
    mat4.identity(modelViewMatrix);
    var modelViewProjectionMatrix = mat4.create();
    mat4.translate(modelViewMatrix, modelViewMatrix, [gCellWidth/2.0, gCellHeight/2.0, 0]);//bring plane from 4th qudrant to origin
    mat4.translate(modelViewMatrix, modelViewMatrix, [pos[0], pos[1], 0]);
    //mat4.rotate(modelViewMatrix, modelViewMatrix, gRotationAngle, [0,1,0]);
	mat4.multiply(modelViewProjectionMatrix, gOrthoProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(gMvpuniform, false, modelViewProjectionMatrix);
	gl.drawArrays(gl.TRIANGLES, gStartNumVertices[chr][0], gStartNumVertices[chr][1]);
    chrNum++;

    //Plane3
    var chr = "PLANE";
    pos = gPlane3Pos;
	gl.bindVertexArray(gvaoLine);
    mat4.identity(modelViewMatrix);
    var modelViewProjectionMatrix = mat4.create();
    mat4.translate(modelViewMatrix, modelViewMatrix, [gCellWidth/2.0, gCellHeight/2.0, 0]);//bring plane from 4th qudrant to origin
    mat4.translate(modelViewMatrix, modelViewMatrix, [pos[0], pos[1], 0]);
    //mat4.rotate(modelViewMatrix, modelViewMatrix, gRotationAngle, [0,1,0]);
	mat4.multiply(modelViewProjectionMatrix, gOrthoProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(gMvpuniform, false, modelViewProjectionMatrix);
	gl.drawArrays(gl.TRIANGLES, gStartNumVertices[chr][0], gStartNumVertices[chr][1]);
    chrNum++;

    //move alphabets
    if(Math.abs(gI1Pos[0] - gI1PosFinal[0]) > gStep / 2.0)
    {
        gI1Pos[0]+= gStep;
    }
    else
    {
        gI1Pos = gI1PosFinal;

        if(Math.abs(gAPos[0] - gAPosFinal[0]) > gStep / 2.0)
        {
            gAPos[0]-= gStep;
        }
        else
        {
            gAPos = gAPosFinal;

            if(Math.abs(gNPos[1] - gNPosFinal[1]) > gStep / 2.0)
            {
                gNPos[1]-= gStep;
            }
            else
            {
                gNPos = gNPosFinal;

                if(Math.abs(gI2Pos[1] - gI2PosFinal[1]) > gStep / 2.0)
                {
                    gI2Pos[1]+= gStep;
                }
                else
                {
                    gI2Pos = gI2PosFinal;

                    if(gDAlpha < 1.0)
                    {
                        gDAlpha += gStep/5.0;
                        let index = 0;
                        for(index = 3; index < gStartNumVertices["D"][1]*4.0; index+=4.0)
                        {
                            let index2 = index + gStartNumVertices["D"][0]*4;
                            gColorsList[index2] = gDAlpha;
                        }
                        colorsArr = new Float32Array( gColorsList);
                        gl.bindBuffer(gl.ARRAY_BUFFER, gvboLineColor);
                        gl.bufferData(gl.ARRAY_BUFFER, colorsArr, gl.DYNAMIC_DRAW);
                    }
                    else
                    {
                        gDAlpha = 1.0;

                        if(false == gbPlanesMet)
                        {
                            if(gPlane1Pos[1] > gStep/2.0)
                            {
                                gPlane1Pos[0] += gDeltaPlane1[0];
                                gPlane2Pos[0] += gDeltaPlane2[0];
                                gPlane3Pos[0] += gDeltaPlane3[0];

                                gPlane1Pos[1] += gDeltaPlane1[1];
                                gPlane2Pos[1] += gDeltaPlane2[1];
                                gPlane3Pos[1] += gDeltaPlane3[1];

                                gPlaneMeetX = gPlane1Pos[0];
                            }
                            else
                            {
                                gbPlanesMet = true;
                                gPlane1Pos[1] = 0;
                                gPlane2Pos[1] = 0;
                                gPlane3Pos[1] = 0;
                                console.log(gPlane1Pos, "gPlane1Pos");
                            }
                        }
                        else
                        {
                            //TODO: overflow possible
                            gDeltaPlane1[1] = 0;
                            gDeltaPlane2[1] = 0;
                            gDeltaPlane3[1] = 0;

                            gPlane1Pos[0] += gDeltaPlane1[0];
                            gPlane2Pos[0] += gDeltaPlane2[0];
                            gPlane3Pos[0] += gDeltaPlane3[0];

                            gPlane1Pos[1] += gDeltaPlane1[1];
                            gPlane2Pos[1] += gDeltaPlane2[1];
                            gPlane3Pos[1] += gDeltaPlane3[1];
                        }
                    }
                }
            }
        }
    }

    //var chr = "D";
    //pos = gDPos.slice();
	//gl.bindVertexArray(gvaoLine);
    //mat4.identity(modelViewMatrix);
    //var modelViewProjectionMatrix = mat4.create();
    //mat4.translate(modelViewMatrix, modelViewMatrix, [pos[0], pos[1], 0]);
    ////mat4.rotate(modelViewMatrix, modelViewMatrix, gRotationAngle, [0,1,0]);
	//mat4.multiply(modelViewProjectionMatrix, gOrthoProjectionMatrix, modelViewMatrix);
	//gl.uniformMatrix4fv(gMvpuniform, false, modelViewProjectionMatrix);
	//gl.drawArrays(gl.LINES, gStartNumVertices[chr][0], gStartNumVertices[chr][1]);
    //chrNum++;

    gl.bindVertexArray(null);
	gl.useProgram(null);

	//animation loop
	requestAnimationFrame(draw, gcanvas);
}

function uninitialize()
{
	//code
	if(gvaoLine)
	{
		gl.deleteVertexArray(gvaoLine);
		gvaoLine = null;
	}

	if(gvboLinePosition)
	{
		gl.deleteBuffer(gvboLinePosition);
		gvboLinePosition = null;
	}

    if(gvboLineColor)
	{
		gl.deleteBuffer(gvboLineColor);
		gvboLineColor = null;
	}

	if(gShaderProgramObject)
	{
		if(gFragmentShaderObject)
		{
			gl.detachShader(gShaderProgramObject, gFragmentShaderObject);
			gl.deleteShader(gFragmentShaderObject);
			gFragmentShaderObject = null;
		}

		if(gVertexShaderObject)
		{
			gl.detachShader(gShaderProgramObject, gVertexShaderObject);
			gl.deleteShader(gVertexShaderObject);
			gVertexShaderObject = null;
		}

		gl.deleteProgram(gShaderProgramObject);
		gShaderProgramObject = null;
	}
}