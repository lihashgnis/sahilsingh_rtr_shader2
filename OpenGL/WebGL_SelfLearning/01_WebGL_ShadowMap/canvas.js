//Tutorial - https://webglfundamentals.org/webgl/lessons/webgl-shadows.html
//onload function
var gcanvas = null;
var gcanvas_debug = null;
var gbEnableDebug = true;
var gl = null;
var gl_Debug = null;
var gl_Debug_UniformLocs = {};
var gl_Debug_vao = null;
var gl_Debug_vars = {};
var gbFullscreen=false;
var gcanvas_original_width = 0 ;
var gcanvas_original_height = 0;

const gWebGLMacros = 
{
	VDG_ATTRIBUTE_VERTEX   : 0,
	VDG_ATTRIBUTE_COLOR    : 1,
	VDG_ATTRIBUTE_NORMAL   : 2,
	VDG_ATTRIBUTE_TEXTURE0 : 3,
};


var gCONFIG_PLANE_NEAR = 1;
var gCONFIG_PLANE_FAR  = 100;
var gCONFIG_LIGHT_SHADOW_DISTANCE = 100;
var gCONFIG_PERSPECTIVE_FOVY = 45.0;
var gCONFIG_SHADOW_DISTANCE = 100;

var gVertexShaderObject = null;
var gFragmentShaderObject = null;
var gShaderProgramObject = null;

var gvaoCube = -1;
var gvboCubePosition = -1;
var gvboCubeNormal = -1;

var gu_m_matrixUniform = null;
var gu_v_matrixUniform = null;
var gu_p_matrixUniform = null;
var gu_bShadowCalc = null;
var gu_LD_vec3 = null;
var gu_KD_vec3 = null;
var gu_lightPosition_vec4 = null;

var texture_shadow_depth = null;
var texture_shadow_color = null;

var gShaderInfo = {};

var gPerspectiveProjectionMatrix = null;

var gRotateAngleRad = 0.0; //rotation angle in radians

var requestAnimationFrame = 
window.requestAnimationFrame		||
window.webkitRequestAnimationFrame  ||
window.mozRequestAnimationFrame		||
window.oRequestAnimationFrame		||
window.msRequestAnimationFrame;

var cancelAnimationFrame = 
window.cancelAnimationFrame					||
window.webkitCancelRequestAnimationFrame	|| window.webkitCancelAnimationFrame	||
window.mozCancelRequestAnimationFrame		|| window.mozCancelAnimationFrame		||
window.oCancelRequestAnimationFrame			|| window.oCancelAnimationFrame			||
window.msCancelRequestAnimationFrame		|| window.msCancelAnimationFrame;

function main()
{
	gcanvas = document.getElementById("AMC");
	if(!gcanvas)
	{
		console.error("obtaining canvas failed");
	}
	else
	{
		console.log("obtaining canvas success");
	}

	console.log("canvas width : " + gcanvas.width + " height: " + gcanvas.height);
	gcanvas_original_width = gcanvas.width;
	gcanvas_original_height = gcanvas.height;

	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);

    gcanvas_debug = document.getElementById("AMC_Debug");

	init();

    if(gbEnableDebug)
    {
        init_Debug();
    }

	resize();
	draw();
}

function keyDown(event)
{
	if(event.keyCode >= 'A'.charCodeAt(0) && event.keyCode <= 'Z'.charCodeAt(0))
	{
		var chr = String.fromCharCode(event.keyCode);
		switch(chr)
		{
			case 'F':
				toggleFullScreen();
				break;
		}
	}
	else
	{//other keys
		switch(event.keyCode)
		{
			case 27://escape
			uninitialize();
			window.close();
			break;
		}
	}
}

function mouseDown()
{
}

function drawText(text)
{
	gl.textAlign="center";
	gl.textBaseline="middle";
	gl.font="48px sans-serif";
	gl.fillStyle="white";
	gl.fillText(text, gcanvas.width/2, gcanvas.height/2);
}

function toggleFullScreen()
{
	var fullscreen_element = 
	document.fullscreenElement			||
	document.webkitFullscreenElement	||
	document.mozFullscreenElement		||
	document.msFullscreenElement		||
	null;

	//if not in fullscreen mode
	if(fullscreen_element==null)
	{
		if(gcanvas.requestFullscreen)
		{
			gcanvas.requestFullscreen();
		}
		else if(gcanvas.mozRequestFullScreen)
		{
			gcanvas.mozRequestFullScreen();
		}
		else if(gcanvas.webkitRequestFullscreen)
		{
			gcanvas.webkitRequestFullscreen();
		}
		else if(gcanvas.msRequestFullscreen)
		{
			gcanvas.msRequestFullscreen();
		}
		gbFullscreen=true;
	}
	else
	{
		if(document.exitFullscreen)
		{
			document.exitFullscreen();
		}
		else if(document.mozCancelFullscreen)
		{
			document.mozCancelFullscreen();
		}
		else if(document.webkitExitFullscreen)
		{
			document.webkitExitFullscreen();
		}
		else if(document.msExitFullscreen)
		{
			document.msExitFullscreen();
		}
		gbFullscreen=false;
	}
}

function init_Debug()
{
    //gl_Debug = gcanvas_debug.getContext("webgl2");

    var vertexShaderCode =
    `#version 300 es

    in vec4 vPosition;
    in vec2 vTexCord;

    uniform mat4 u_m_matrix;
	uniform mat4 u_v_matrix;
	uniform mat4 u_p_matrix;

    out vec2 out_vTexCord;

    void main()
    {
        out_vTexCord = vTexCord;
        gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * vPosition;
    }
    `;
    //let gld = gl_Debug;
    gld = gl;
    vertexShaderObject_Debug = gld.createShader(gld.VERTEX_SHADER);
    gld.shaderSource(vertexShaderObject_Debug, vertexShaderCode);
    gld.compileShader(vertexShaderObject_Debug);

    if(gld.getShaderParameter(vertexShaderObject_Debug, gld.COMPILE_STATUS) == false)
    {
        let error = gld.getShaderInfoLog(vertexShaderObject_Debug);
        if(error.length > 0)
        {
            console.warn("Debug canvas vertex shader compile failed. ", error);
        }
    }

    var fragmentShaderCode_Debug = 
    `#version 300 es

    precision highp float;

    uniform highp sampler2D u_texSampler;
    in vec2 out_vTexCord;
    out vec4 FragColor;
    void main()
    {
        FragColor = vec4(1.0);
        FragColor.r = texture(u_texSampler, out_vTexCord).r;
        FragColor = texture(u_texSampler, out_vTexCord).rrrr;
        FragColor.a = 1.0;
        FragColor = texture(u_texSampler, out_vTexCord);
        FragColor.a = 1.0;
    }
    `;

    fragmentShaderObject_Debug = gld.createShader(gld.FRAGMENT_SHADER);
    gld.shaderSource(fragmentShaderObject_Debug, fragmentShaderCode_Debug);
    gld.compileShader(fragmentShaderObject_Debug);

    if(gld.getShaderParameter(fragmentShaderObject_Debug, gld.COMPILE_STATUS) == false)
    {
        let error = gld.getShaderInfoLog(fragmentShaderObject_Debug);
        if(error.length > 0)
        {
            console.warn("Debug canvas fragment shader compile failed. ", error);
        }
    }

    //shader program
    shaderProgramObject_Debug = gld.createProgram();
    gld.attachShader(shaderProgramObject_Debug, vertexShaderObject_Debug);
    gld.attachShader(shaderProgramObject_Debug, fragmentShaderObject_Debug);

    //pre-link : bind attributes
    gld.bindAttribLocation(shaderProgramObject_Debug, gWebGLMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");
    gld.bindAttribLocation(shaderProgramObject_Debug, gWebGLMacros.VDG_ATTRIBUTE_TEXTURE0, "vTexCord");

    //link program
    gld.linkProgram(shaderProgramObject_Debug);
    if(gld.getProgramParameter(shaderProgramObject_Debug, gld.LINK_STATUS) == false)
    {
        let error = gld.getProgramInfoLog(shaderProgramObject_Debug);
        if(error.length > 0)
        {
            console.warn("Debug canvas shader link failed. ", error);
        }
    }

    gl_Debug_vars["shader_program"] = shaderProgramObject_Debug;

    gl_Debug_UniformLocs["u_m_matrix"] = gld.getUniformLocation(shaderProgramObject_Debug, "u_m_matrix");
    gl_Debug_UniformLocs["u_v_matrix"] = gld.getUniformLocation(shaderProgramObject_Debug, "u_v_matrix");
    gl_Debug_UniformLocs["u_p_matrix"] = gld.getUniformLocation(shaderProgramObject_Debug, "u_p_matrix");
    gl_Debug_UniformLocs["u_texSampler"] = gld.getUniformLocation(shaderProgramObject_Debug, "u_texSampler");

    let quadVertices = new Float32Array([
         1, 1,0,
        -1, 1,0,
        -1,-1,0,
        -1,-1,0,
         1,-1,0,
         1, 1,0,
    ]);

    let quadTexCords = new Float32Array([
    1,1,
    0,1,
    0,0,
    0,0,
    1,0,
    1,1
    ]);

    gl_Debug_vao = gld.createVertexArray();
    gld.bindVertexArray(gl_Debug_vao);
    let vboQuad = gld.createBuffer();
    gld.bindBuffer(gld.ARRAY_BUFFER, vboQuad);
    gld.bufferData(gld.ARRAY_BUFFER, quadVertices, gld.STATIC_DRAW);
    gld.vertexAttribPointer(gWebGLMacros.VDG_ATTRIBUTE_VERTEX, 3, gld.FLOAT, false, 0, 0);
    gld.enableVertexAttribArray(gWebGLMacros.VDG_ATTRIBUTE_VERTEX);

    vboQuadtex = gld.createBuffer();
    gld.bindBuffer(gld.ARRAY_BUFFER, vboQuadtex);
    gld.bufferData(gld.ARRAY_BUFFER, quadTexCords, gld.STATIC_DRAW);
    gld.vertexAttribPointer(gWebGLMacros.VDG_ATTRIBUTE_TEXTURE0, 2, gld.FLOAT, false, 0, 0);
    gld.enableVertexAttribArray(gWebGLMacros.VDG_ATTRIBUTE_TEXTURE0);

    gld.bindBuffer(gld.ARRAY_BUFFER, null);
    gld.bindVertexArray(null);

    gld.clearColor(0.0,0.0,1.0,1.0);
	gld.enable(gld.DEPTH_TEST);
	gld.clearDepth(1.0);

    //gld.viewport(0,0,gcanvas_debug.width, gcanvas_debug.height);
    gld.viewport(0,0,gcanvas.width, gcanvas.height);
}

function init()
{
	gl = gcanvas.getContext("webgl2");
	if(gl==null)
	{
		console.error("failed to get webgl rendering context");
		return;
	}
	gl.viewportWidth = gcanvas.width;
	gl.viewportHeight = gcanvas.height;

	//vertex shader
	var vertexShaderSourceCode = 
	`#version 300 es

	in vec4 vPosition;
    in vec3 vNormal;

	uniform mat4 u_m_matrix;
	uniform mat4 u_v_matrix;
	uniform mat4 u_p_matrix;

    //shadow map
    uniform int u_bShadowCalc;
    uniform mat4 u_m_lightView;
    uniform mat4 u_m_lightProj;
    uniform sampler2D depth_map_texture;

    uniform vec3 u_LD_vec3;
    uniform vec3 u_KD_vec3;
    uniform vec4 u_lightPosition_vec4;
	out vec3 out_color;//will just be used to pass color data to fragment shader

    out vec4 lightSpacePos;

	void main()
	{
        mat4 mv_matrix = u_v_matrix*u_m_matrix;
        mat3 normal_matrix = mat3(transpose(inverse(mv_matrix)));
        vec4 eye_cords = mv_matrix*vPosition;
        vec3 transformed_normal = normalize(normal_matrix*vNormal);

        vec3 light_direction = vec3(1.0);
        if(0.0==u_lightPosition_vec4[3])
        {//directional light - light at inifinity
            vec4 lightPosition_vec4 = u_lightPosition_vec4;
            lightPosition_vec4 = u_v_matrix*lightPosition_vec4;
            light_direction = normalize(vec3(lightPosition_vec4));
        }
        else
        {
            light_direction = normalize(vec3(u_v_matrix*u_lightPosition_vec4 - eye_cords));
        }
        
        vec3 diffuse_color = u_LD_vec3 * u_KD_vec3 * max(dot(light_direction, transformed_normal), 0.0f);
		gl_Position = u_p_matrix * eye_cords;
		out_color = diffuse_color;
        lightSpacePos = u_m_lightProj * u_m_lightView * u_m_matrix * vPosition;

        if(u_bShadowCalc == 0)
        {
            gl_Position = lightSpacePos;
        }
	}
	`;

	gVertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(gVertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(gVertexShaderObject);
	
	if(gl.getShaderParameter(gVertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gVertexShaderObject);
		if(error.length > 0)
		{
			alert("vertex shader compilation failed. see console for logs.");
			console.error(error);
			uninitialize();
		}
	}

	var fragmentShaderSourceCode = 
	`#version 300 es

    precision highp float;
    precision highp int;

    in vec4 lightSpacePos;
    uniform int u_bShadowCalc;
    uniform sampler2D depth_map_texture;

	in vec3 out_color;//coming from vertex shader
	out vec4 FragColor;
	
    void main()
	{
        vec3 lsPosNDC;
        float depth;

        if(u_bShadowCalc == 1)
        {
            lsPosNDC = lightSpacePos.xyz / lightSpacePos.w;
            lsPosNDC = lsPosNDC*0.5 + 0.5;
            depth = texture(depth_map_texture, lsPosNDC.xy).r;
            
            if(depth < lsPosNDC.z -0.006)
            {//shadow
                FragColor = vec4(0.0);
            }
            else
            {
                FragColor = vec4(out_color, 1.0);
            }
            FragColor.a = 1.0;

        }
        else
        {
            FragColor = vec4(out_color, 1.0);
        }
        //FragColor = vec4(1.0);
        //FragColor = vec4(depth);
        //FragColor = vec4(lsPosNDC.z);
       //FragColor = vec4(out_color, 1.0);

	}
	`;

	gFragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(gFragmentShaderObject, fragmentShaderSourceCode);
	gl.compileShader(gFragmentShaderObject);
	
	if(gl.getShaderParameter(gFragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gFragmentShaderObject);
		if(error.length > 0)
		{
			alert("fragment shader compilation failed. see console for logs.");
			console.error(error);
			uninitialize();
		}
	}

	//shader program
	gShaderProgramObject = gl.createProgram();
	gl.attachShader(gShaderProgramObject, gVertexShaderObject);
	gl.attachShader(gShaderProgramObject, gFragmentShaderObject);

	//pre-link : bind attributes
	gl.bindAttribLocation(gShaderProgramObject, gWebGLMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(gShaderProgramObject, gWebGLMacros.VDG_ATTRIBUTE_NORMAL, "vNormal");

	//link program
	gl.linkProgram(gShaderProgramObject);
	if(!gl.getProgramParameter(gShaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(gShaderProgramObject);
		if(error.length > 0)
		{
			alert("shader program link failed. see console logs.")
			console.error(error);
			uninitialize();
		}
	}

	gu_m_matrixUniform = gl.getUniformLocation(gShaderProgramObject, "u_m_matrix");
	gu_v_matrixUniform = gl.getUniformLocation(gShaderProgramObject, "u_v_matrix");
	gu_p_matrixUniform = gl.getUniformLocation(gShaderProgramObject, "u_p_matrix");
	gu_bShadowCalc = gl.getUniformLocation(gShaderProgramObject, "u_bShadowCalc");
	gShaderInfo["u_m_lightView"] = gl.getUniformLocation(gShaderProgramObject, "u_m_lightView");
	gShaderInfo["u_m_lightProj"] = gl.getUniformLocation(gShaderProgramObject, "u_m_lightProj");
	gShaderInfo["depth_map_texture"] = gl.getUniformLocation(gShaderProgramObject, "depth_map_texture");
	
    gu_LD_vec3 = gl.getUniformLocation(gShaderProgramObject, "u_LD_vec3");
    gu_KD_vec3 = gl.getUniformLocation(gShaderProgramObject, "u_KD_vec3");
    gu_lightPosition_vec4 = gl.getUniformLocation(gShaderProgramObject, "u_lightPosition_vec4");


	//Cube
	//Generated using INIT_MASK_DUMP_ATTRIBUTES in corresponding C++ program
    //Also see util::QuadToTriangle()
	var cubeVertices = new Float32Array ([
//-1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, -1.0, -1.0, 1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, 1.0, 1.0, -1.0, 1.0, 1.0, -1.0, -1.0, -1.0, 1.0, 1.0, -1.0, -1.0, 1.0, 1.0, -1.0, 1.0, 1.0, 1.0, 1.0, -1.0, 1.0, -1.0, -1.0, -1.0, -1.0, 1.0, -1.0, -1.0, 1.0, 1.0, -1.0, 1.0, 1.0, 1.0, 1.0, -1.0, 1.0, 1.0, -1.0, -1.0, 1.0, 1.0, -1.0, -1.0, 1.0, 1.0, -1.0, -1.0, 1.0, -1.0, -1.0, -1.0, -1.0, 1.0, -1.0
-1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, -1.000000, 1.000000, 1.000000, -1.000000, -1.000000, 1.000000, -1.000000, -1.000000, 1.000000, 1.000000, -1.000000, -1.000000, -1.000000, -1.000000, -1.000000, 1.000000, 1.000000, -1.000000, 1.000000, 1.000000, -1.000000, 1.000000, 1.000000, -1.000000, -1.000000, -1.000000, -1.000000, -1.000000, -1.000000, 1.000000, 1.000000, -1.000000, -1.000000, 1.000000, 1.000000, -1.000000, 1.000000, 1.000000, -1.000000, 1.000000, 1.000000, 1.000000, 1.000000, -1.000000, 1.000000, 1.000000, -1.000000, 1.000000, -1.000000, -1.000000, -1.000000, -1.000000, 1.000000, -1.000000, -1.000000, 1.000000, -1.000000, -1.000000, 1.000000, 1.000000, -1.000000, -1.000000, 1.000000, -1.000000, 1.000000, 1.000000, 1.000000, 1.000000, -1.000000, 1.000000, 1.000000, -1.000000, -1.000000, 1.000000, -1.000000, -1.000000, 1.000000, 1.000000, -1.000000, 1.000000, 1.000000, 1.000000, -1.000000, 1.000000, 1.000000, -1.000000, -1.000000, 1.000000, -1.000000, -1.000000, -1.000000, -1.000000, -1.000000, -1.000000, -1.000000, 1.000000, -1.000000, -1.000000, 1.000000, 1.000000
                ]);

    var cubeNormal = new Float32Array ([
0.000000, 1.000000, 0.000000, 0.000000, 1.000000, 0.000000, 0.000000, 1.000000, 0.000000, 0.000000, 1.000000, 0.000000, 0.000000, 1.000000, 0.000000, 0.000000, 1.000000, 0.000000, 0.000000, -1.000000, 0.000000, 0.000000, -1.000000, 0.000000, 0.000000, -1.000000, 0.000000, 0.000000, -1.000000, 0.000000, 0.000000, -1.000000, 0.000000, 0.000000, -1.000000, 0.000000, 0.000000, 0.000000, 1.000000, 0.000000, 0.000000, 1.000000, 0.000000, 0.000000, 1.000000, 0.000000, 0.000000, 1.000000, 0.000000, 0.000000, 1.000000, 0.000000, 0.000000, 1.000000, 0.000000, 0.000000, -1.000000, 0.000000, 0.000000, -1.000000, 0.000000, 0.000000, -1.000000, 0.000000, 0.000000, -1.000000, 0.000000, 0.000000, -1.000000, 0.000000, 0.000000, -1.000000, 1.000000, 0.000000, 0.000000, 1.000000, 0.000000, 0.000000, 1.000000, 0.000000, 0.000000, 1.000000, 0.000000, 0.000000, 1.000000, 0.000000, 0.000000, 1.000000, 0.000000, 0.000000, -1.000000, 0.000000, 0.000000, -1.000000, 0.000000, 0.000000, -1.000000, 0.000000, 0.000000, -1.000000, 0.000000, 0.000000, -1.000000, 0.000000, 0.000000, -1.000000, 0.000000, 0.000000
               ]);

	gvaoCube = gl.createVertexArray();
	gl.bindVertexArray(gvaoCube);
	gvboCubePosition = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, gvboCubePosition);
	gl.bufferData(gl.ARRAY_BUFFER, cubeVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(gWebGLMacros.VDG_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(gWebGLMacros.VDG_ATTRIBUTE_VERTEX);

	gvboCubeNormal = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, gvboCubeNormal);
	gl.bufferData(gl.ARRAY_BUFFER, cubeNormal, gl.STATIC_DRAW);
	gl.vertexAttribPointer(gWebGLMacros.VDG_ATTRIBUTE_NORMAL, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(gWebGLMacros.VDG_ATTRIBUTE_NORMAL);

	//unbind buffers
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	gl.bindVertexArray(null);

	gl.clearColor(0.0,0.0,1.0,1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.clearDepth(1.0);

	gPerspectiveProjectionMatrix = mat4.create();
}

function resize()
{
	if(gbFullscreen==true)
	{
		gcanvas.width = window.innerWidth;
		gcanvas.height=window.innerHeight;
	}
	else
	{
		gcanvas.width = gcanvas_original_width;
		gcanvas.height = gcanvas_original_height;
	}

	gl.viewport(0,0,gcanvas.width, gcanvas.height);

	var factor = gcanvas.width / gcanvas.height;
	mat4.perspective(gPerspectiveProjectionMatrix, gCONFIG_PERSPECTIVE_FOVY, factor, gCONFIG_PLANE_NEAR, gCONFIG_PLANE_FAR);
}

var lightView = null;
var lightProjection = null;

var lightRadius = 10;
var rotAngle = 0.0;
function RenderScene(bShadowCalc)
{
    let xpos = lightRadius*Math.cos(rotAngle);
    let zpos = lightRadius*Math.sin(rotAngle);
    rotAngle += 0.01;
    //let lightPos = [5, 0.0, -6.0, 0.0];
    let lightPos = [50, 0.0, 0, 1.0];
    //let lightPos = [xpos, 0.0, zpos, 0.0];
    //let camPos = [5, 15.0, -10.0, 1.0];
    let camPos = [3, 15.0, -10.0, 1.0];
 

    gRotateAngleRad += 0.01;
	if(gRotateAngleRad >= 360.0)
	{
		gRotateAngleRad = 0.0;
	}

	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	gl.useProgram(gShaderProgramObject);
	
	var modelMatrix = mat4.create();
	var viewMatrix = mat4.create();
	var identityMatrix  = mat4.create();
    var projectionMatrix = mat4.create();

    //let lkAT1 = vec3.fromValues(1.5,0,-6);
    let lkAT1 = vec3.fromValues(0,0,0);
    let lkAT2 = vec3.fromValues(0,0,0);
    if(lightProjection == null)
    {
        lightProjection = mat4.create();
        lightView = mat4.create();
    }

        //mat4.lookAt(lightView, vec3.fromValues(lightPos[0], lightPos[1], lightPos[2]), lkAT1, vec3.fromValues(0,1,0));
        //let lightOrthoScale = 5;
        //mat4.ortho(lightProjection, -10*lightOrthoScale, 10*lightOrthoScale, -10*lightOrthoScale, 10*lightOrthoScale, gCONFIG_PLANE_NEAR, gCONFIG_PLANE_FAR*lightOrthoScale);

     camView = mat4.create();
     mat4.lookAt(camView, vec3.fromValues(camPos[0], camPos[1], camPos[2]), lkAT2, vec3.fromValues(0,0,1));

     let lightPos2 = lightPos.slice();

    if(bShadowCalc == 0)
    {
        //we are generating shadow map
          let t = gl.createTexture();
          gl.activeTexture(gl.TEXTURE0);
	      gl.bindTexture(gl.TEXTURE_2D, t);
          gl.bindTexture(gl.TEXTURE_2D, null);
          gl.deleteTexture(t);

          mat4.identity(lightProjection);
          mat4.identity(lightView);

    if(0.0 == lightPos[3])
    {//simulate directional light
        let scale = 1000;
        lightPos2[0] *= scale;
        lightPos2[1] *= scale;
        lightPos2[2] *= scale;
        lightPos2[3] = 1.0;
    }

        mat4.lookAt(lightView, vec3.fromValues(lightPos2[0], lightPos2[1], lightPos2[2]), lkAT1, vec3.fromValues(0,1,0));

        let aspectRatio = gcanvas.width/gcanvas.height;

        let nt =  gCONFIG_PLANE_NEAR * Math.tan(gCONFIG_PERSPECTIVE_FOVY*0.5*(Math.PI/180.0));
        let nb = -nt;
        let nr = nt*aspectRatio;
        let nl = -nr;

        let ft =  gCONFIG_SHADOW_DISTANCE * Math.tan(gCONFIG_PERSPECTIVE_FOVY*0.5*(Math.PI/180.0));
        let fb = -ft;
        let fr =  ft*aspectRatio;
        let fl = -fr;

        //cc - camera coordinates
        let ntrcc = vec4.fromValues(nt,nr,gCONFIG_PLANE_NEAR, 1.0);
        let ntlcc = vec4.fromValues(nt,nl,gCONFIG_PLANE_NEAR, 1.0);
        let nbrcc = vec4.fromValues(nb,nr,gCONFIG_PLANE_NEAR, 1.0);
        let nblcc = vec4.fromValues(nb,nr,gCONFIG_PLANE_NEAR, 1.0);

        let ftrcc = vec4.fromValues(ft,fr,gCONFIG_SHADOW_DISTANCE,1.0);
        let ftlcc = vec4.fromValues(ft,fl,gCONFIG_SHADOW_DISTANCE,1.0);
        let fbrcc = vec4.fromValues(fb,fr,gCONFIG_SHADOW_DISTANCE,1.0);
        let fblcc = vec4.fromValues(fb,fr,gCONFIG_SHADOW_DISTANCE,1.0);

        let camViewInv = mat4.create();
        mat4.invert(camViewInv, camView);

        //wc - world coordinate
        let ntrwc = vec4.create();
        let ntlwc = vec4.create();
        let nbrwc = vec4.create();
        let nblwc = vec4.create();

        let ftrwc = vec4.create();
        let ftlwc = vec4.create();
        let fbrwc = vec4.create();
        let fblwc = vec4.create();

        vec4.transformMat4(ntrwc, ntrcc, camViewInv);
        vec4.transformMat4(ntlwc, ntlcc, camViewInv);
        vec4.transformMat4(nbrwc, nbrcc, camViewInv);
        vec4.transformMat4(nblwc, nblcc, camViewInv);

        vec4.transformMat4(ftrwc, ftrcc, camViewInv);
        vec4.transformMat4(ftlwc, ftlcc, camViewInv);
        vec4.transformMat4(fbrwc, fbrcc, camViewInv);
        vec4.transformMat4(fblwc, fblcc, camViewInv);

        //now convert them into lc - light coordinates - treating light as camera

        let ntrlc = vec4.create();
        let ntllc = vec4.create();
        let nbrlc = vec4.create();
        let nbllc = vec4.create();

        let ftrlc = vec4.create();
        let ftllc = vec4.create();
        let fbrlc = vec4.create();
        let fbllc = vec4.create();

        vec4.transformMat4(ntrlc, ntrwc, lightView);
        vec4.transformMat4(ntllc, ntlwc, lightView);
        vec4.transformMat4(nbrlc, nbrwc, lightView);
        vec4.transformMat4(nbllc, nblwc, lightView);

        vec4.transformMat4(ftrlc, ftrwc, lightView);
        vec4.transformMat4(ftllc, ftlwc, lightView);
        vec4.transformMat4(fbrlc, fbrwc, lightView);
        vec4.transformMat4(fbllc, fblwc, lightView);


        vec4.scale(ntrlc,ntrlc,1.0/ntrlc[3]);
        vec4.scale(ntllc,ntllc,1.0/ntllc[3]);
        vec4.scale(nbrlc,nbrlc,1.0/nbrlc[3]);
        vec4.scale(nbllc,nbllc,1.0/nbllc[3]);

        vec4.scale(ftrlc,ftrlc,1.0/ftrlc[3]);
        vec4.scale(ftllc,ftllc,1.0/ftllc[3]);
        vec4.scale(fbrlc,fbrlc,1.0/fbrlc[3]);
        vec4.scale(fbllc,fbllc,1.0/fbllc[3]);

        let points = [ntrlc, ntllc, nbrlc, nbllc, ftrlc, ftllc, fbrlc, fbllc];

        let minx, maxx, miny, maxy, minz, maxz;
        minx = maxx = ntrlc[0];
        miny = maxy = ntrlc[1];
        minz = maxz = ntrlc[2];

        points.forEach(
        function(item, index)
        {
            if(minx > item[0])
            {
                minx = item[0];
            }

            if(maxx < item[0])
            {
                maxx = item[0];
            }

            if(miny > item[1])
            {
                miny = item[1];
            }

            if(maxy < item[1])
            {
                maxy = item[1];
            }

            if(minz > item[2])
            {
                minz = item[2];
            }

            if(maxz < item[2])
            {
                maxz = item[2];
            }

        });

        let cx = maxx - minx;
        let cy = maxy - miny;
        let cz = maxz - minz;

        let xm = (minx + maxx)/2.0;
        let ym = (miny + maxy)/2.0;
        let zm = (minz + maxz)/2.0;

        let shadowBoxCentre = vec4.fromValues(xm, ym, zm, 1.0);
        let shadowBoxCentreWC = vec4.create();

        let lightDir = vec4.create();
        vec4.normalize(lightDir, lightPos2);
        let lightDist = 10;
        let lightCamPos = vec4.create();
        vec4.scaleAndAdd(lightCamPos, shadowBoxCentreWC, lightDir, lightDist);
        let lightViewInv = mat4.create();
        mat4.invert(lightViewInv, lightView);
        vec4.transformMat4(shadowBoxCentreWC, shadowBoxCentre, lightViewInv);

        //mat4.lookAt(lightView, vec3.fromValues(shadowBoxCentreWC[0], shadowBoxCentreWC[1], shadowBoxCentreWC[2]), lkAT1, vec3.fromValues(0,1,0));
        mat4.lookAt(lightView, vec3.fromValues(lightCamPos[0], lightCamPos[1], lightCamPos[2]), lkAT1, vec3.fromValues(0,1,0));
        //mat4.ortho(lightProjection, -cx/2, cx/2, -cy/2, cy/2, 0, cz);
        
        let scale = 1;
        mat4.ortho(lightProjection, -10*scale, 10*scale, -10*scale, 10*scale, gCONFIG_PLANE_NEAR, 100);
 
        viewMatrix = lightView.slice();
        projectionMatrix = lightProjection.slice();
        
    }
    else
    {
      gl.activeTexture(gl.TEXTURE0);
	  gl.bindTexture(gl.TEXTURE_2D, texture_shadow_depth);
      gl.uniform1i(gShaderInfo["depth_map_texture"], 0);

        projectionMatrix = gPerspectiveProjectionMatrix.slice();
        viewMatrix = camView.slice();
    }
    
    gl.uniformMatrix4fv(gShaderInfo["u_m_lightView"], false, lightView);
    gl.uniformMatrix4fv(gShaderInfo["u_m_lightProj"], false, lightProjection);

	//Cube
    let xx = 0;
    let yy = 0;
    let zz = 0;
	mat4.translate(modelMatrix, modelMatrix, vec3.fromValues(xx+2,yy,zz));
	mat4.rotate(modelMatrix, modelMatrix, gRotateAngleRad, vec3.fromValues(1,1,1));

;	gl.uniformMatrix4fv(gu_m_matrixUniform, false, modelMatrix);
	gl.uniformMatrix4fv(gu_v_matrixUniform, false, viewMatrix);
	gl.uniformMatrix4fv(gu_p_matrixUniform, false, projectionMatrix);
    gl.uniform3f(gu_LD_vec3, 1.0, 1.0, 1.0);
    gl.uniform3f(gu_KD_vec3, 1.0, 0.0, 0.0);
    gl.uniform4f(gu_lightPosition_vec4, lightPos2[0], lightPos2[1], lightPos2[2], lightPos2[3]);
    gl.uniform1i(gu_bShadowCalc, bShadowCalc);

	gl.bindVertexArray(gvaoCube);
	gl.drawArrays(gl.TRIANGLES, 0, 36);

    gl.uniform3f(gu_KD_vec3, 1.0, 0.0, 1.0);
    mat4.identity(modelMatrix);
    mat4.translate(modelMatrix, modelMatrix, vec3.fromValues(-xx-2,yy,zz));
    //mat4.rotate(modelMatrix, modelMatrix, gRotateAngleRad, vec3.fromValues(1,1,1));
    mat4.scale(modelMatrix, modelMatrix, vec3.fromValues(2,2,2));
    gl.uniformMatrix4fv(gu_m_matrixUniform, false, modelMatrix);
    gl.drawArrays(gl.TRIANGLES, 0, 36);

	gl.bindVertexArray(null);
    gl.bindTexture(gl.TEXTURE_2D, null);
	gl.useProgram(null);

}

function draw()
{
    gl.enable(gl.DEPTH_TEST);
    //gl.disable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    //RenderScene();
    //requestAnimationFrame(draw, gcanvas);
    //return;

    let fbo_shadow = gl.createFramebuffer();
    gl.bindFramebuffer(gl.FRAMEBUFFER, fbo_shadow);

    //texture attachments
    //color
    texture_shadow_color = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D, texture_shadow_color);
    
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGB, gcanvas.width, gcanvas.height, 0, gl.RGB, gl.UNSIGNED_BYTE, null);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
    gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, texture_shadow_color, 0);
    //deph
    texture_shadow_depth = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D, texture_shadow_depth);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.DEPTH_COMPONENT16, gcanvas.width, gcanvas.height, 0, gl.DEPTH_COMPONENT, gl.UNSIGNED_INT, null);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);

    //gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.DEPTH_STENCIL_ATTACHMENT, gl.TEXTURE_2D, texture_shadow_depth, 0);
    gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.TEXTURE_2D, texture_shadow_depth, 0);
    //gl.deleteFramebuffer(fbo_shadow);

    if(gl.checkFramebufferStatus(gl.FRAMEBUFFER) !== gl.FRAMEBUFFER_COMPLETE){
		console.error("failed to get framebuffer");
		return;
	}
	RenderScene(0);
    //gl.bindFramebuffer(gl.FRAME_BUFFER, null);
    gl.deleteFramebuffer(fbo_shadow);
    //RenderScene();

    RenderScene(1);
    gbEnableDebug = false;
    if(gbEnableDebug)
    {
        //let gld = gl_Debug;
        let gld = gl;

        gld.enable(gld.DEPTH_TEST);

        gld.clear(gld.COLOR_BUFFER_BIT | gld.DEPTH_BUFFER_BIT);
        gld.useProgram(gl_Debug_vars["shader_program"]);

        let modelMatrix = mat4.create();
	    let viewMatrix = mat4.create();
	    let identityMatrix  = mat4.create();

        mat4.translate(modelMatrix, identityMatrix, vec3.fromValues(0,0,-3));

        gld.uniformMatrix4fv(gl_Debug_UniformLocs["u_m_matrix"], false, modelMatrix);
        gld.uniformMatrix4fv(gl_Debug_UniformLocs["u_v_matrix"], false, viewMatrix);
        gld.uniformMatrix4fv(gl_Debug_UniformLocs["u_p_matrix"], false, gPerspectiveProjectionMatrix);

        gld.activeTexture(gld.TEXTURE0);
	    //gld.bindTexture(gld.TEXTURE_2D, texture_shadow_depth);
	    gld.bindTexture(gld.TEXTURE_2D, texture_shadow_color);
	    gld.uniform1i(gl_Debug_UniformLocs["u_texSampler"], 0);

        gld.bindVertexArray(gl_Debug_vao);
        gld.drawArrays(gld.TRIANGLES, 0, 6);

        gld.bindVertexArray(null);
        gld.useProgram(null);

    }

    gl.deleteTexture(texture_shadow_color);
    gl.deleteTexture(texture_shadow_depth);

	//animation loop
	requestAnimationFrame(draw, gcanvas);
}

function uninitialize()
{

	if(gvboCubeNormal)
	{
		//gl.deleteBuffer(gvboCubeNormal);
		//gvboCubeNormal = null;
	}

    if(gvboCubePosition)
	{
		gl.deleteBuffer(gvboCubePosition);
		gvboCubePosition = null;
	}

	if(gvaoCube)
	{
		gl.deleteVertexArray(gvaoCube);
		gvaoCube = null;
	}

	if(gShaderProgramObject)
	{
		if(gFragmentShaderObject)
		{
			gl.detachShader(gShaderProgramObject, gFragmentShaderObject);
			gl.deleteShader(gFragmentShaderObject);
			gFragmentShaderObject = null;
		}

		if(gVertexShaderObject)
		{
			gl.detachShader(gShaderProgramObject, gVertexShaderObject);
			gl.deleteShader(gVertexShaderObject);
			gVertexShaderObject = null;
		}

		gl.deleteProgram(gShaderProgramObject);
		gShaderProgramObject = null;
	}
}