#include <freeglut.h>

bool isFullScreen = false;

int main(int argc, char* argv[])
{
    //function declaration
    void initialize();
    void uninitialize();
    void reshape(int, int);
    void display();
    void keyboard(unsigned char, int, int);
    void mouse(int, int, int, int);

    //code
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA);
    glutInitWindowSize(800, 600);
    glutInitWindowPosition(100, 100);
    glutCreateWindow("My First OpenGL Program");
    initialize();

    //callbacks
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutKeyboardFunc(keyboard);
    glutMouseFunc(mouse);
    glutCloseFunc(uninitialize);
    glutMainLoop();
    return 0;
}

void initialize()
{
    glClearColor(0.0, 0.0, 0.0, 1.0);

}

void uninitialize()
{

}

void reshape(int width, int height)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
}

void display()
{
    glClear(GL_COLOR_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
    glBegin(GL_TRIANGLES);
    
    glColor3f(1.0, 0.0, 0.0);
    glVertex2f(0.0, 1.0);
    
    glColor3f(0.0, 1.0, 0.0);
    glVertex2f(-1.0, -1.0);

    glColor3f(0.0, 0.0, 1.0);
    glVertex2f(1.0, -1.0);
    glEnd();
    //glBegin(GL_LINES);
    //glVertex2f(0.0, 0.0);
    //glVertex2f(1.0, 0.0);
    //glEnd();
    glFlush();
}

void keyboard(unsigned char key, int x, int y)
{
    switch (key)
    {
    case 27://escape
    {
        glutLeaveMainLoop();
    }
    break;
    case 'f':
    case 'F':
        if (isFullScreen == false)
        {
            glutFullScreen();
            isFullScreen = true;
        }
        else
        {
            glutLeaveFullScreen();
            isFullScreen = false;
        }
        break;
    default:
        break;
    }
}

void mouse(int button, int state, int x, int y)
{
    switch (button)
    {
    case GLUT_LEFT_BUTTON:
        break;
    case GLUT_RIGHT_BUTTON:
    {
        glutLeaveMainLoop();
    }
    break;
    default:
        break;
    }
}