#include "../../../../99_AuxStuff/MyLibs/Utility/Util.hpp"
#include <gl\GL.h>
#include <gl/GLU.h>
#include<cmath>
#include <tuple>

//STRICT to prevent explicit typecasting
#define STRICT

constexpr UINT WIN_WIDTH = 800;
constexpr UINT WIN_HEIGHT = 800;
constexpr UINT WIN_X = 100;
constexpr UINT WIN_Y = 100;
UINT NUM_PTS = 100;
UINT NUM_TRIANGLES = 8;
double RADIUS_MAX = 0.9;
double RADIUS_MIN = 0.3;

class MyOglWindow : public util::OglWindow
{
    const double PI;
    // Inherited via OglWindow
    virtual const char * GetMessageMap() override
    {
        return nullptr;
    }
    virtual void Display() override
    {
        glClear(GL_COLOR_BUFFER_BIT);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

        const double theta_delta = (2.0*PI) / NUM_PTS;
        glEnable(GL_LINE_SMOOTH);

        static util::ColorWheel cw(NUM_TRIANGLES);

        double coord = -0.9;
        for (int i = 0; i < NUM_TRIANGLES; i++)
        {
            util::ColorWheel::RED r;
            util::ColorWheel::GREEN g;
            util::ColorWheel::BLUE b;
            std::tie(r, g, b) = cw.MapIndexToColor(i);
            util::DrawShapes::DrawTriangleLines(util::DataTypes::POINTd{ coord, coord },
                util::DataTypes::POINTd{ -coord, coord },
                util::DataTypes::POINTd{ 0, -coord }, RGB(r, g, b));
            coord += 0.1;
        }

        SwapBuffers(m_Hdc);
    }

    virtual void Resize(int width, int height) override
    {
        if (height == 0)
            height = 1;

        glViewport(0, 0, width, height);

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        if (width <= height)
        {
            auto factor = 1.0f*(((GLfloat)height) / ((GLfloat)width));
            glOrtho(-1.0f, 1.0f, -1 * factor, factor, -1.0f, 1.0f);
        }
        else
        {
            auto factor = 1.0f*(((GLfloat)width) / ((GLfloat)height));
            glOrtho(-1 * factor, factor, -1.0f, 1.0f, -1.0f, 1.0f);
        }
    }

    LRESULT WndProcPost(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
    {
        return LRESULT(0);
    }

    virtual LRESULT WndProcPre(HWND, UINT, WPARAM, LPARAM) override
    {
        return LRESULT();
    }
public:
    MyOglWindow(TCHAR * className, UINT x, UINT y, UINT width, UINT height, HINSTANCE hInstance) : PI(4 * atan(1)), OglWindow(className, x, y, width, height, hInstance)
    {
    }

    // Inherited via OglWindow
    virtual void Update() override
    {
    }
};

int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
    TCHAR AppName[] = TEXT("Concentric Triangles");
    MyOglWindow myOglWnd(AppName, WIN_X, WIN_Y, WIN_WIDTH, WIN_HEIGHT, hInstance);
    myOglWnd.InitAndShowWindow();
    myOglWnd.RunGameLoop();
    return 0;
}