#include "../../../../99_AuxStuff/MyLibs/Utility/Util.hpp"
#include <gl\GL.h>
#include <gl/GLU.h>
#include<cmath>
#include <tuple>

//STRICT to prevent explicit typecasting
#define STRICT

constexpr UINT WIN_WIDTH = 800;
constexpr UINT WIN_HEIGHT = 800;
constexpr UINT WIN_X = 100;
constexpr UINT WIN_Y = 100;
UINT NUM_PTS = 100;

class MyOglWindow : public util::OglWindow
{
    const double PI;
    // Inherited via OglWindow
    virtual const char * GetMessageMap() override
    {
        return nullptr;
    }
    virtual void Display() override
    {
        glClear(GL_COLOR_BUFFER_BIT);
        glMatrixMode(GL_MODELVIEW);

        double coord = -0.9;
        static double rotate = 0;
        if (rotate >= 360)
        {
            rotate = 0;
        }
        double rotate_radian = (PI / 180.0)*rotate;
        double s = sin(rotate_radian);
        double c = cos(rotate_radian);

        //write column major
        GLfloat identity_matrix[4][4] = {};
        for (int i = 0; i < 4; i++)
        {
            identity_matrix[i][i] = 1;
        }
        GLfloat rot_matrix[4][4] = { {c, 0, -s, 0}, {0, 1, 0, 0}, {s, 0, c, 0}, {0, 0, 0, 1}};
        
        glLoadMatrixf((GLfloat*)identity_matrix);
        glMultMatrixf((GLfloat*)rot_matrix);

        rotate += 0.02;

        //triangle
        coord += 0.1;
        util::DataTypes::POINTd p1{ coord, coord };
        util::DataTypes::POINTd p2{ -coord, coord };
        util::DataTypes::POINTd p3{ 0, 0.66 };
        util::DrawShapes::DrawTriangleLines(p1, p2, p3);
        util::DataTypes::Centre inCentre;
        util::DataTypes::Radius inRadius;
        SwapBuffers(m_Hdc);
    }

    virtual void Resize(int width, int height) override
    {
        if (height == 0)
            height = 1;

        glViewport(0, 0, width, height);

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        if (width <= height)
        {
            auto factor = 1.0f*(((GLfloat)height) / ((GLfloat)width));
            glOrtho(-1.0f, 1.0f, -1 * factor, factor, -1.0f, 1.0f);
        }
        else
        {
            auto factor = 1.0f*(((GLfloat)width) / ((GLfloat)height));
            glOrtho(-1 * factor, factor, -1.0f, 1.0f, -1.0f, 1.0f);
        }
    }

    LRESULT WndProcPost(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
    {
        return LRESULT(0);
    }

    virtual LRESULT WndProcPre(HWND, UINT, WPARAM, LPARAM) override
    {
        return LRESULT();
    }
public:
    MyOglWindow(TCHAR * className, UINT x, UINT y, UINT width, UINT height, HINSTANCE hInstance) : PI(4 * atan(1)), OglWindow(className, x, y, width, height, hInstance)
    {
    }

    // Inherited via OglWindow
    virtual void Update() override
    {
    }
};

int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
    TCHAR AppName[] = TEXT("Rotate Triangle Matrix Manipulation");
    MyOglWindow myOglWnd(AppName, WIN_X, WIN_Y, WIN_WIDTH, WIN_HEIGHT, hInstance);
    myOglWnd.InitAndShowWindow();
    myOglWnd.RunGameLoop();
    return 0;
}