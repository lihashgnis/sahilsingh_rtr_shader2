#include "../../../../99_AuxStuff/MyLibs/Utility/Util.hpp"
#include <gl\GL.h>
#include <gl/GLU.h>
#include<cmath>
#include <tuple>

//STRICT to prevent explicit typecasting
#define STRICT

constexpr UINT WIN_WIDTH = 800;
constexpr UINT WIN_HEIGHT = 800;
constexpr UINT WIN_X = 100;
constexpr UINT WIN_Y = 100;
UINT NUM_PTS = 100;

class MyOglWindow : public util::OglWindow
{
    const double PI;
    // Inherited via OglWindow
    virtual const char * GetMessageMap() override
    {
        return nullptr;
    }
    virtual void Display() override
    {
        glClear(GL_COLOR_BUFFER_BIT);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

        constexpr double initPos = -2.0;
        static double coord = 0, translate_delta = 0.0005;
        static double trInitPos = initPos;
        static double rotate = 0;
        static bool stopTriangleTranslate = false;
        static bool stopCircleTranslate = true;
        static bool stopLineTranslate = true;

        if (rotate >= 360)
        {
            rotate = 0;
        }

        if (stopTriangleTranslate && stopCircleTranslate && stopLineTranslate)
        {
            rotate += 0.05;
            glRotatef(rotate, 0, 1.0, 0);
        }

        //triangle
        if (!stopTriangleTranslate)
        {
            trInitPos += translate_delta;
            glTranslatef(trInitPos, trInitPos, 0);
        }

        static const double trSide = 2;
        util::DataTypes::POINTd p1{ coord, coord };
        util::DataTypes::Centre inCentre = p1;;
        util::DataTypes::Radius inRadius = 0.2;
        util::DrawShapes::DrawEqTriangleByIncentre(inCentre, inRadius);
        
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        
        static double circleInitPos = -initPos;
        if (!stopCircleTranslate)
        {
            circleInitPos -= translate_delta;
        }

        //circle
        glPushMatrix();
        if (stopTriangleTranslate && stopCircleTranslate && stopLineTranslate)
        {
            glRotatef(rotate, 0, 1.0, 0);
        }
        glTranslatef(circleInitPos, -circleInitPos, 0);
        util::DrawShapes::DrawCircleLines(NUM_PTS, inRadius);
        glPopMatrix();

        static double lineMidInitPosY = -initPos;

        //Line - wand
        if (!stopLineTranslate)
        {
            lineMidInitPosY -= translate_delta;
        }

        if (stopTriangleTranslate && stopCircleTranslate && stopLineTranslate)
        {
            glRotatef(rotate, 0, 1.0, 0);
        }

        glTranslatef(0, lineMidInitPosY, 0);
        glBegin(GL_LINES);
        glVertex2f(inCentre.x, inCentre.y + (inRadius / sin(PI / 6)));
        glVertex2f(inCentre.x, (inCentre.y -   inRadius));
        glEnd();
        SwapBuffers(m_Hdc);

        if (!stopLineTranslate)
        if (lineMidInitPosY <= translate_delta / 2 && lineMidInitPosY >= -translate_delta / 2)
        {
            stopTriangleTranslate = true;
            stopCircleTranslate = true;
            stopLineTranslate = true;
            lineMidInitPosY = inCentre.y;
        }

        if (!stopCircleTranslate)
        if (circleInitPos <= translate_delta / 2 && circleInitPos >= -translate_delta / 2)
        {
            stopTriangleTranslate = true;
            stopCircleTranslate = true;
            stopLineTranslate = false;
        }

        if(!stopTriangleTranslate)
        if (trInitPos <= translate_delta / 2 && trInitPos >= -translate_delta / 2)
        {
            stopTriangleTranslate = true;
            stopCircleTranslate = false;
            stopLineTranslate = true;
        }
    }

    virtual void Resize(int width, int height) override
    {
        if (height == 0)
            height = 1;

        glViewport(0, 0, width, height);

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        if (width <= height)
        {
            auto factor = 1.0f*(((GLfloat)height) / ((GLfloat)width));
            glOrtho(-1.0f, 1.0f, -1 * factor, factor, -1.0f, 1.0f);
        }
        else
        {
            auto factor = 1.0f*(((GLfloat)width) / ((GLfloat)height));
            glOrtho(-1 * factor, factor, -1.0f, 1.0f, -1.0f, 1.0f);
        }
    }

    LRESULT WndProcPost(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
    {
        return LRESULT(0);
    }

    virtual LRESULT WndProcPre(HWND, UINT, WPARAM, LPARAM) override
    {
        return LRESULT();
    }
public:
    MyOglWindow(TCHAR * className, UINT x, UINT y, UINT width, UINT height, HINSTANCE hInstance) : PI(4 * atan(1)), OglWindow(className, x, y, width, height, hInstance)
    {
    }

    // Inherited via OglWindow
    virtual void Update() override
    {
    }
};

int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
    TCHAR AppName[] = TEXT("Deathly Hallows Spinning");
    MyOglWindow myOglWnd(AppName, WIN_X, WIN_Y, WIN_WIDTH, WIN_HEIGHT, hInstance);
    myOglWnd.InitAndShowWindow();
    myOglWnd.RunGameLoop();
    return 0;
}