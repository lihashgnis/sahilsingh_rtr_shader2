﻿#include "../../../../99_AuxStuff/MyLibs/Utility/Util.hpp"
#include "../../../../99_AuxStuff/MyLibs/Utility/Animation.hpp"
#include "../../../../99_AuxStuff/MyLibs/Utility/Airplane.h"
#include "../../../../99_AuxStuff/MyLibs/Utility/EmptyObject.h"
#include "../../../../99_AuxStuff/MyLibs/Utility/AirplaneExhaustParticle.h"
#include <gl\GL.h>
#include <gl\GLU.h>
#include <cmath>
#include <tuple>
#include <string>
#include "resource.h"


//STRICT to prevent explicit typecasting
#define STRICT

constexpr UINT WIN_WIDTH = 1920;
constexpr UINT WIN_HEIGHT = 1080;
constexpr UINT WIN_X = 100;
constexpr UINT WIN_Y = 100;
UINT NUM_PTS = 100;
constexpr double CHAR_SPACING = 0.2;
constexpr double WORD_SPACING = 0;
constexpr double ANIMATION_START_T = 0.1;
constexpr double ANIMATION_DURATION = 200;
constexpr double ANIMATION_STEP_SIZE = 0.025;
constexpr double FONT_HEIGHT = 0.5;
constexpr double CHAR_SPACING_X = 0.1; //spacing along X axis
constexpr double CHAR_SPACING_T = 0.01; //spacing in time
constexpr double CHAR_DURATION_T = 2; //time each char occupies

std::shared_ptr<util::Timeline> tl;

void MultiObjectAnimationDemo(std::shared_ptr<util::Timeline> tl);//for testing animation util

class MyOglWindow : public util::OglWindow
{
    const double PI;
    // Inherited via OglWindow
    virtual const char * GetMessageMap() override
    {
        return nullptr;
    }
    virtual void Display() override
    {
        glClear(GL_COLOR_BUFFER_BIT);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        tl->advance();
        SwapBuffers(m_Hdc);
    }

    virtual void Resize(int width, int height) override
    {
        if (height == 0)
            height = 1;

        glViewport(0, 0, width, height);

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        if (width <= height)
        {
            auto factor = 1.0f*(((GLfloat)height) / ((GLfloat)width));
            m_minX = -1.0;
            m_maxX = 1.0;
            m_minY = -factor;
            m_maxY = factor;
            m_minZ = -1.0;
            m_maxZ = 1.0;
        }
        else
        {
            auto factor = 1.0f*(((GLfloat)width) / ((GLfloat)height));
            m_minX = -factor;
            m_maxX = factor;
            m_minY = -1.0;
            m_maxY = 1.0f;
            m_minZ = -1.0;
            m_maxZ = 1.0;
        }
        glOrtho(m_minX, m_maxX, m_minY, m_maxY, m_minZ, m_maxZ);
    }

    virtual LRESULT WndProcPre(HWND, UINT iMsg, WPARAM wParam, LPARAM lParam) override
    {
        switch (iMsg)
        {
        case WM_CHAR:
        {
            switch (wParam)
            {
            case 'f':
                //[[fallthrough]]
            case 'F':
                return LRESULT(0);//return -1 to disable 'f' press action
                break;
            default:
                break;
            }
        }
        break;
        }
        return LRESULT();
    }

    virtual LRESULT WndProcPost(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) override
    {
        auto that = (OglWindow*)GetProp(hwnd, PARENT_OBJECT);

        switch (iMsg)
        {
        case WM_KEYDOWN:
        {
            if (VK_ESCAPE == wParam)
            {
                DestroyWindow(hwnd);
            }
        }
        break;
        }
        return LRESULT();
    }

public:
    MyOglWindow(TCHAR * className, UINT x, UINT y, UINT width, UINT height, HINSTANCE hInstance) : PI(4 * atan(1)), OglWindow(className, x, y, width, height, hInstance)
    {
    }

    // Inherited via OglWindow
    virtual void Update() override
    {
    }
};

int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
    TCHAR AppName[] = TEXT("Dynamic INDIA");
    MyOglWindow myOglWnd(AppName, WIN_X, WIN_Y, WIN_WIDTH, WIN_HEIGHT, hInstance);
    myOglWnd.InitAndShowWindow();
    myOglWnd.EnableAlpha(true);
    myOglWnd.ToggleFullscreeen();

    //set stage
#pragma region working sample code
    //Working sample code
    //tl.reset(new util::Timeline(ANIMATION_DURATION));
    //util::DataTypes::POINT3Dd spawnPos = { 0, 0, 0 };
    //util::AnimationObject ao(tl, std::shared_ptr<util::SceneObject>(new util::TextHelper::Alphabet(L'I')),spawnPos);
    //util::KeyFrame kf0(0, util::KeyFrame::ACTION::OBJECT_SHOW, 2, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE);
    //util::KeyFrame kf1(1, util::KeyFrame::ACTION::OBJECT_HIDE, 8, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE);
    //util::KeyFrame kf2(2, util::KeyFrame::ACTION::TRANSLATE_ABSOLUTE, 4, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::LINEAR, util::DataTypes::POINT3Dd{0.5, 0.5, 0});
    //ao.AddKeyFrame(kf0);
    //ao.AddKeyFrame(kf1);
    //ao.AddKeyFrame(kf2);
    //tl->AddObject(ao);
#pragma endregion

    tl.reset(new util::Timeline(ANIMATION_DURATION, ANIMATION_STEP_SIZE));
    util::ParticleHelper::AttachTimeline(tl);

    util::DataTypes::POINT3Dd spawnPos = { 0, 0, 0 }, charFinalPos;
    std::shared_ptr<util::TextHelper::Alphabet> alpha = std::shared_ptr<util::TextHelper::Alphabet>(new util::TextHelper::Alphabet(L'I'));//To get char stats
    alpha->SetFontHeight(FONT_HEIGHT);
    double charWidth = alpha->GetCellSize().cx; //for simplicity assume all char have same width
    double charHeight = alpha->GetCellSize().cy; //for simplicity assume all char have same height
    double charSpacing = CHAR_SPACING_X, time_delta = CHAR_DURATION_T, charStartTime = ANIMATION_START_T;

    util::DataTypes::POINT3Dd stringStartPos = {};//top left of I's cell
    double stringWidth = 5 * (charWidth + charSpacing);
    stringStartPos.x = -stringWidth / 2.0;
    stringStartPos.y = charHeight / 2.0;
    
    auto firstIPos = spawnPos;//for airplanes
    auto airplaneMeetingPoint = firstIPos;

    {//I
        charStartTime = ANIMATION_START_T;
        alpha = std::shared_ptr<util::TextHelper::Alphabet>(new util::TextHelper::Alphabet(L'I', util::ColorWheel::COLORS::INDIA_SAFRON,util::ColorWheel::COLORS::INDIA_GREEN, 255));
        alpha->SetFontHeight(FONT_HEIGHT);
        spawnPos = { myOglWnd.GetMinX() - charWidth, stringStartPos.y, stringStartPos.z }; //I starts from left of screen
        charFinalPos = stringStartPos;
        firstIPos = charFinalPos;//here is the place where final value gets applied
        util::AnimationObject ao(tl, alpha, spawnPos);
        ao.AddKeyFrame(util::KeyFrame(0, util::KeyFrame::ACTION::OBJECT_SHOW, charStartTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, spawnPos));
        ao.AddKeyFrame(util::KeyFrame(1, util::KeyFrame::ACTION::TRANSLATE_ABSOLUTE, charStartTime + time_delta, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::LINEAR, charFinalPos));
        tl->AddObject(ao);
    }

    airplaneMeetingPoint.y = firstIPos.y - charHeight / 2.0;

    util::DataTypes::POINT3Dd AfinalPos = {};
    {//A
        charStartTime += time_delta + CHAR_SPACING_T;
        alpha = std::shared_ptr<util::TextHelper::Alphabet>(new util::TextHelper::Alphabet(L'Λ', util::ColorWheel::COLORS::INDIA_SAFRON, util::ColorWheel::COLORS::INDIA_GREEN, 255));
        alpha->SetFontHeight(FONT_HEIGHT);
        spawnPos = { myOglWnd.GetMaxX() + charWidth, stringStartPos.y, stringStartPos.z }; //A starts from right of screen
        charFinalPos = stringStartPos;
        charFinalPos.x = stringStartPos.x + 4*(charWidth + charSpacing);//After INDI
        util::AnimationObject ao(tl, alpha, spawnPos);
        ao.AddKeyFrame(util::KeyFrame(0, util::KeyFrame::ACTION::OBJECT_SHOW, charStartTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, spawnPos));
        ao.AddKeyFrame(util::KeyFrame(1, util::KeyFrame::ACTION::TRANSLATE_ABSOLUTE, charStartTime + time_delta, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::LINEAR, charFinalPos));
        AfinalPos = charFinalPos;
        tl->AddObject(ao);
    }
    
    {//N
        charStartTime += time_delta + CHAR_SPACING_T;
        alpha = std::shared_ptr<util::TextHelper::Alphabet>(new util::TextHelper::Alphabet(L'N', util::ColorWheel::COLORS::INDIA_SAFRON, util::ColorWheel::COLORS::INDIA_GREEN, 255));
        alpha->SetFontHeight(FONT_HEIGHT);
        spawnPos = stringStartPos;
        spawnPos.x = stringStartPos.x + 1 * (charWidth + charSpacing);//After I
        spawnPos.y = myOglWnd.GetMaxY() + charHeight;
        charFinalPos = stringStartPos;
        charFinalPos.x = spawnPos.x;
        util::AnimationObject ao(tl, alpha, spawnPos);
        ao.AddKeyFrame(util::KeyFrame(0, util::KeyFrame::ACTION::OBJECT_SHOW, charStartTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, spawnPos));
        ao.AddKeyFrame(util::KeyFrame(1, util::KeyFrame::ACTION::TRANSLATE_ABSOLUTE, charStartTime + time_delta, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::LINEAR, charFinalPos));
        tl->AddObject(ao);

        airplaneMeetingPoint.x = charFinalPos.x + 2*charWidth;
    }

    {//I
        charStartTime += time_delta + CHAR_SPACING_T;
        alpha = std::shared_ptr<util::TextHelper::Alphabet>(new util::TextHelper::Alphabet(L'I', util::ColorWheel::COLORS::INDIA_SAFRON, util::ColorWheel::COLORS::INDIA_GREEN, 255));
        alpha->SetFontHeight(FONT_HEIGHT);
        spawnPos = stringStartPos;
        spawnPos.x = stringStartPos.x + 3 * (charWidth + charSpacing);//After IND
        spawnPos.y = myOglWnd.GetMinY() - charHeight;
        charFinalPos = stringStartPos;
        charFinalPos.x = spawnPos.x;
        util::AnimationObject ao(tl, alpha, spawnPos);
        ao.AddKeyFrame(util::KeyFrame(0, util::KeyFrame::ACTION::OBJECT_SHOW, charStartTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, spawnPos));
        ao.AddKeyFrame(util::KeyFrame(1, util::KeyFrame::ACTION::TRANSLATE_ABSOLUTE, charStartTime + time_delta, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::LINEAR, charFinalPos));
        tl->AddObject(ao);
    }

    {//D
        charStartTime += time_delta + CHAR_SPACING_T;
        alpha = std::shared_ptr<util::TextHelper::Alphabet>(new util::TextHelper::Alphabet(L'ב', util::ColorWheel::COLORS::INDIA_SAFRON, util::ColorWheel::COLORS::INDIA_GREEN, 255));
        alpha->SetFontHeight(FONT_HEIGHT);
        spawnPos = stringStartPos;
        spawnPos.x = stringStartPos.x + 2 * (charWidth + charSpacing);//After IN
        charFinalPos = stringStartPos;
        charFinalPos.x = spawnPos.x;
        util::AnimationObject ao(tl, alpha, spawnPos);
        ao.AddKeyFrame(util::KeyFrame(0, util::KeyFrame::ACTION::OBJECT_SHOW, charStartTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, spawnPos));
        //ao.AddKeyFrame(util::KeyFrame(1, util::KeyFrame::ACTION::TRANSLATE_ABSOLUTE, charStartTime + time_delta, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::LINEAR, charFinalPos));
        tl->AddObject(ao);
    }
    
    charStartTime += time_delta + CHAR_SPACING_T;
    auto airplaneStartTime = charStartTime;
    util::DataTypes::POINT3Dd airplaneStartPos = {};
    double airplaneFinTime = 0.0;

#pragma region comon stuff
//#define RATIOT 1.5//0.05
//#define RATIOT2 1//0.02
//#define RATIOT3 1//0.01
#define RATIO_0_5 0.5
#define AIRPLANE_TOTAL_TIME 10; //Time airplanes are on screen - start to finish
#define RATIO1 0.1763 //Airplanes reache I
#define RATIO2 0.3601 //Airplanes meet at D end
#define RATIO3 0.1557 //Aiplanes reach start of A
#define RATIO4 0.1010 //Airplanes reach end of A
#define RATIO5 0.2059 //Airplanes exit the screen
#define RATIO5_2 (RATIO5/2.0) // 5/2
#define RATIO5_5 (RATIO5/5.0) // 5/5

//Move to A code is same for all 3 planes
    util::KeyFrame::CALLBACKFUNC callBackF1 = [](std::shared_ptr<util::SceneObject> obj, util::AnimationObjectState state) {
        auto airplane = std::dynamic_pointer_cast<util::Airplane>(obj);
        if (airplane)
        {
            airplane->SetParticlesDontDie(true);
        }
    };

    util::KeyFrame::CALLBACKFUNC callBackF2 = [](std::shared_ptr<util::SceneObject> obj, util::AnimationObjectState state) {
        auto airplane = std::dynamic_pointer_cast<util::Airplane>(obj);
        if (airplane)
        {
            airplane->SetParticlesDontDie(false);
        }
    };


#define MOVE_TO_A_START \
    airplaneTime += RATIO3*AIRPLANE_TOTAL_TIME; \
    airplanePos = airplaneMeetingPoint; \
    airplanePos.x = AfinalPos.x; \
    ao.AddKeyFrame(util::KeyFrame(6, util::KeyFrame::ACTION::TRANSLATE_ABSOLUTE, airplaneTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::LINEAR, airplanePos)); \
    ao.AddKeyFrame(util::KeyFrame(7, util::KeyFrame::ACTION::EXECUTE_ARBITRARY_CALLBACK, airplaneTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, airplanePos, 0, callBackF1)); 

#define MOVE_TO_A_END \
    airplaneTime += RATIO4*AIRPLANE_TOTAL_TIME; \
    airplanePos = airplaneMeetingPoint; \
    airplanePos.x = AfinalPos.x + charWidth; \
    ao.AddKeyFrame(util::KeyFrame(6, util::KeyFrame::ACTION::TRANSLATE_ABSOLUTE, airplaneTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::LINEAR, airplanePos)); \
    ao.AddKeyFrame(util::KeyFrame(7, util::KeyFrame::ACTION::EXECUTE_ARBITRARY_CALLBACK, airplaneTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, airplanePos, 0, callBackF2));

#define MOVE_TO_A \
    MOVE_TO_A_START \
    MOVE_TO_A_END

//common time calculations
#define REACH_I double airplaneTime = airplaneStartTime + RATIO1*AIRPLANE_TOTAL_TIME;
#define AIRPLANES_MEET airplaneTime += RATIO2*AIRPLANE_TOTAL_TIME;

//sign will be +1 for top plane, 0 for middle, and -1 for bottom
#define EXITSCREEN \
    airplanePos = airplaneMeetingPoint; \
    airplanePos.x = AfinalPos.x + 2*charWidth; \
    airplanePos.y = RATIO_0_5*sign*myOglWnd.GetMaxY(); \
    airplaneTime += RATIO5_2*AIRPLANE_TOTAL_TIME; \
    ao.AddKeyFrame(util::KeyFrame(8, util::KeyFrame::ACTION::ROTATE_ANGLE_ABSOLUTE, airplaneTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::LINEAR, { 0,0,1 }, sign*45)); \
    ao.AddKeyFrame(util::KeyFrame(9, util::KeyFrame::ACTION::TRANSLATE_ABSOLUTE, airplaneTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::LINEAR, airplanePos)); \
    airplanePos.x = myOglWnd.GetMaxX(); \
    airplanePos.y = sign*myOglWnd.GetMaxY(); \
        airplaneTime += RATIO5_2*AIRPLANE_TOTAL_TIME; \
    ao.AddKeyFrame(util::KeyFrame(10, util::KeyFrame::ACTION::TRANSLATE_ABSOLUTE, airplaneTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::LINEAR, airplanePos)); \
    airplanePos.x += charWidth; \
    airplaneTime += RATIO5_5*AIRPLANE_TOTAL_TIME; \
    ao.AddKeyFrame(util::KeyFrame(11, util::KeyFrame::ACTION::TRANSLATE_ABSOLUTE, airplaneTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::LINEAR, airplanePos));\
    airplaneFinTime = airplaneTime;
#pragma endregion

#pragma region Airplane top
    {//Airplane bottom
        airplaneStartPos.x = myOglWnd.GetMinX();
        airplaneStartPos.y = myOglWnd.GetMaxY();
        auto airplanePos = airplaneStartPos;
        airplanePos = firstIPos;

        std::shared_ptr<util::Airplane> plane(new util::Airplane);
        util::AnimationObject ao(tl, plane, {});
        ao.AddKeyFrame(util::KeyFrame(0, util::KeyFrame::ACTION::OBJECT_SHOW, airplaneStartTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, airplaneStartPos));
        ao.AddKeyFrame(util::KeyFrame(1, util::KeyFrame::ACTION::ROTATE_ANGLE_ABSOLUTE, airplaneStartTime + CHAR_SPACING_T / 10.0, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::LINEAR, { 0,0,1 }, -45));

        //airplanes reach I
        REACH_I
        ao.AddKeyFrame(util::KeyFrame(2, util::KeyFrame::ACTION::TRANSLATE_ABSOLUTE, airplaneTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::LINEAR, airplanePos));
        ao.AddKeyFrame(util::KeyFrame(3, util::KeyFrame::ACTION::ROTATE_ANGLE_ABSOLUTE, airplaneTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::LINEAR, { 0,0,1 }, -10));

        //Airplanes meet
        AIRPLANES_MEET
        ao.AddKeyFrame(util::KeyFrame(4, util::KeyFrame::ACTION::TRANSLATE_ABSOLUTE, airplaneTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::LINEAR, airplaneMeetingPoint));
        ao.AddKeyFrame(util::KeyFrame(5, util::KeyFrame::ACTION::ROTATE_ANGLE_ABSOLUTE, airplaneTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::LINEAR, { 0,0,1 }, 0));

        MOVE_TO_A;

        int sign = 1;
        EXITSCREEN;

        tl->AddObject(ao);
    }
#pragma endregion

#pragma region Airplane middle
    {//Airplane bottom
        airplaneStartPos.x = myOglWnd.GetMinX();
        airplaneStartPos.y = airplaneMeetingPoint.y;
        auto airplanePos = airplaneStartPos;
        airplanePos = firstIPos;
        airplanePos.y = airplaneStartPos.y;

        std::shared_ptr<util::Airplane> plane(new util::Airplane);
        util::AnimationObject ao(tl, plane, {});
        ao.AddKeyFrame(util::KeyFrame(0, util::KeyFrame::ACTION::OBJECT_SHOW, airplaneStartTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, airplaneStartPos));
        //ao.AddKeyFrame(util::KeyFrame(2, util::KeyFrame::ACTION::ROTATE_ANGLE_ABSOLUTE, airplaneStartTime + CHAR_SPACING_T / 10.0, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::LINEAR, { 0,0,1 }, -45));

        //Reach I
        REACH_I
        ao.AddKeyFrame(util::KeyFrame(1, util::KeyFrame::ACTION::TRANSLATE_ABSOLUTE, airplaneTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::LINEAR, airplanePos));

        //Airplanes meet
        AIRPLANES_MEET
        ao.AddKeyFrame(util::KeyFrame(2, util::KeyFrame::ACTION::TRANSLATE_ABSOLUTE, airplaneTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::LINEAR, airplaneMeetingPoint));

        MOVE_TO_A;
        int sign = 0;
        EXITSCREEN;

        tl->AddObject(ao);
    }
#pragma endregion

#pragma region Airplane bottom
    {//Airplane bottom
        airplaneStartPos.x = myOglWnd.GetMinX();
        airplaneStartPos.y = myOglWnd.GetMinY();
        auto airplanePos = airplaneStartPos;
        airplanePos = firstIPos;
        airplanePos.y -= charHeight;

        std::shared_ptr<util::Airplane> plane(new util::Airplane);
        util::AnimationObject ao(tl, plane, {});
        ao.AddKeyFrame(util::KeyFrame(0, util::KeyFrame::ACTION::OBJECT_SHOW, airplaneStartTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, airplaneStartPos));
        ao.AddKeyFrame(util::KeyFrame(2, util::KeyFrame::ACTION::ROTATE_ANGLE_ABSOLUTE, airplaneStartTime + CHAR_SPACING_T/10.0, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::LINEAR, { 0,0,1 }, 45));

        //Reach I
        REACH_I
        ao.AddKeyFrame(util::KeyFrame(3, util::KeyFrame::ACTION::TRANSLATE_ABSOLUTE, airplaneTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::LINEAR, airplanePos));
        ao.AddKeyFrame(util::KeyFrame(5, util::KeyFrame::ACTION::ROTATE_ANGLE_ABSOLUTE, airplaneTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::LINEAR, { 0,0,1 }, 10));

        //Airplanes meet
        AIRPLANES_MEET
        ao.AddKeyFrame(util::KeyFrame(4, util::KeyFrame::ACTION::TRANSLATE_ABSOLUTE, airplaneTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::LINEAR, airplaneMeetingPoint));
        ao.AddKeyFrame(util::KeyFrame(5, util::KeyFrame::ACTION::ROTATE_ANGLE_ABSOLUTE, airplaneTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::LINEAR, { 0,0,1 }, 0));

        MOVE_TO_A;

        int sign = -1;
        EXITSCREEN;

        tl->AddObject(ao);
    }
#pragma endregion

#undef MOVE_TO_A

    {
        util::KeyFrame::CALLBACKFUNC callBackMusicStart = [](std::shared_ptr<util::SceneObject> obj, util::AnimationObjectState state) {
            auto object = std::dynamic_pointer_cast<util::EmptyObject>(obj);
            if (object)
            {
                auto retval = PlaySound(MAKEINTRESOURCE(DULHAN_SONG), GetModuleHandle(NULL), SND_RESOURCE | SND_ASYNC | SND_NODEFAULT);
                return retval;//TODO: Log error
            }
        };

        util::KeyFrame::CALLBACKFUNC callBackMusicEnd = [](std::shared_ptr<util::SceneObject> obj, util::AnimationObjectState state) {
            auto object = std::dynamic_pointer_cast<util::EmptyObject>(obj);
            if (object)
            {
                PlaySound(NULL, 0, 0);
            }
        };

        std::shared_ptr<util::EmptyObject> music(new util::EmptyObject);
        util::AnimationObject ao(tl, music, {});
        ao.AddKeyFrame(util::KeyFrame(0, util::KeyFrame::ACTION::EXECUTE_ARBITRARY_CALLBACK, ANIMATION_START_T, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, {}, 0, callBackMusicStart));
        ao.AddKeyFrame(util::KeyFrame(1, util::KeyFrame::ACTION::EXECUTE_ARBITRARY_CALLBACK, airplaneFinTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, {}, 0, callBackMusicEnd));
        tl->AddObject(ao);
    }

    //MultiObjectAnimationDemo(tl);
    myOglWnd.SetCapFrameRate(33, true);

    //TODO: sahild-debug remove this
    OutputDebugStringA("\n\n\n\n");
    OutputDebugStringA("myOglWnd.GetMaxX2() "); OutputDebugStringA(std::to_string(myOglWnd.GetMaxX2()).c_str()); OutputDebugStringA("\n");
    OutputDebugStringA("myOglWnd.GetMaxY2() "); OutputDebugStringA(std::to_string(myOglWnd.GetMaxY2()).c_str()); OutputDebugStringA("\n");
    OutputDebugStringA("myOglWnd.GetMinX2() "); OutputDebugStringA(std::to_string(myOglWnd.GetMinX2()).c_str()); OutputDebugStringA("\n");
    OutputDebugStringA("myOglWnd.GetMinY2() "); OutputDebugStringA(std::to_string(myOglWnd.GetMinY2()).c_str()); OutputDebugStringA("\n");


    OutputDebugStringA("\n\n\n\n");
    OutputDebugStringA("myOglWnd.GetMaxX() "); OutputDebugStringA(std::to_string(myOglWnd.GetMaxX()).c_str()); OutputDebugStringA("\n");
    OutputDebugStringA("myOglWnd.GetMaxY() "); OutputDebugStringA(std::to_string(myOglWnd.GetMaxY()).c_str()); OutputDebugStringA("\n");
    OutputDebugStringA("myOglWnd.GetMinX() "); OutputDebugStringA(std::to_string(myOglWnd.GetMinX()).c_str()); OutputDebugStringA("\n");
    OutputDebugStringA("myOglWnd.GetMinY() "); OutputDebugStringA(std::to_string(myOglWnd.GetMinY()).c_str()); OutputDebugStringA("\n");

    myOglWnd.RunGameLoop();
    return 0;
}

void MultiObjectAnimationDemo(std::shared_ptr<util::Timeline> tl)
{
    util::DataTypes::POINT3Dd spawnPos = {};
    auto alpha2 = std::shared_ptr<util::TextHelper::Alphabet>(new util::TextHelper::Alphabet(L'D', util::ColorWheel::COLORS::INDIA_SAFRON, util::ColorWheel::COLORS::INDIA_GREEN, 255));
    util::AnimationObject ao2(tl, alpha2, spawnPos);
    ao2.AddKeyFrame(util::KeyFrame(0, util::KeyFrame::ACTION::OBJECT_SHOW, 1, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, spawnPos));
    ao2.AddKeyFrame(util::KeyFrame(1, util::KeyFrame::ACTION::TRANSLATE_ABSOLUTE, 6, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::LINEAR, util::DataTypes::POINT3Dd{ 0.5, 0.5, 0 }));
    tl->AddObject(ao2);

    auto alpha3 = std::shared_ptr<util::TextHelper::Alphabet>(new util::TextHelper::Alphabet(L'Λ', util::ColorWheel::COLORS::INDIA_SAFRON, util::ColorWheel::COLORS::INDIA_GREEN, 255));
    util::AnimationObject ao3(tl, alpha3, spawnPos);

    ao3.AddKeyFrame(util::KeyFrame(0, util::KeyFrame::ACTION::OBJECT_SHOW, 3, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, spawnPos));
    ao3.AddKeyFrame(util::KeyFrame(1, util::KeyFrame::ACTION::TRANSLATE_ABSOLUTE, 8, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::LINEAR, util::DataTypes::POINT3Dd{ -0.5, 0.5, 0.5 }));

    tl->AddObject(ao3);
}