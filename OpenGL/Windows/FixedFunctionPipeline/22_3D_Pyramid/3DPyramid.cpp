#include "../../../../99_AuxStuff/MyLibs/Utility/Util.hpp"
#include <gl\GL.h>
#include <gl/GLU.h>
#include<cmath>

constexpr UINT WIN_WIDTH = 800;
constexpr UINT WIN_HEIGHT = 800;
constexpr UINT WIN_X = 100;
constexpr UINT WIN_Y = 100;
UINT NUM_PTS = 100;
double RADIUS = 0.5;

double curAngle = 0;

class MyOglWindow : public util::OglWindow
{
    const double PI;
    // Inherited via OglWindow
    virtual const char * GetMessageMap() override
    {
        return nullptr;
    }
    virtual void Display() override
    {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        glTranslatef(0, 0, -6);
        glRotatef(curAngle, 0, 1, 0);
        glBegin(GL_TRIANGLES);
            //Triangle 1
            glColor3f(1, 0, 0);
            glVertex3f(0, 1, 0);
            glColor3f(0, 0, 1);
            glVertex3f(-1, -1, 1);
            glColor3f(0, 1, 0);
            glVertex3f(1, -1, 1);

            //Triangle 2
            glColor3f(1, 0, 0);
            glVertex3f(0, 1, 0);
            glColor3f(0, 1, 0);
            glVertex3f(1, -1, 1);
            glColor3f(0, 0, 1);
            glVertex3f(1, -1, -1);
            
            //Triangle 3
            glColor3f(1, 0, 0);
            glVertex3f(0, 1, 0);
            glColor3f(0, 0, 1);
            glVertex3f(1, -1, -1);
            glColor3f(0, 1, 0);
            glVertex3f(-1, -1, -1);
            
            //Triangle 4
            glColor3f(1, 0, 0);
            glVertex3f(0, 1, 0);
            glColor3f(0, 1, 0);
            glVertex3f(-1, -1, -1);
            glColor3f(0, 0, 1);
            glVertex3f(-1, -1, 1);
        glEnd();
        SwapBuffers(m_Hdc);
    }

    virtual void Resize(int width, int height) override
    {
        if (height == 0)
            height = 1;

        glViewport(0, 0, width, height);

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        gluPerspective(45, ((GLfloat)width) / height, 0.1, 100.0f);
    }

    LRESULT WndProcPost(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
    {
        return LRESULT(0);
    }

    virtual LRESULT WndProcPre(HWND, UINT, WPARAM, LPARAM) override
    {
        return LRESULT();
    }
public:
    MyOglWindow(TCHAR * className, UINT x, UINT y, UINT width, UINT height, HINSTANCE hInstance) : PI(4 * atan(1)), OglWindow(className, x, y, width, height, hInstance)
    {
    }

    // Inherited via OglWindow
    virtual void Update() override
    {
        curAngle += 0.03;
        curAngle = (curAngle > 360) ? curAngle - 360 : curAngle;
        curAngle = (curAngle < -360) ? curAngle + 360 : curAngle;
    }
};

int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
    TCHAR AppName[] = TEXT("3D Pyramid");
    MyOglWindow myOglWnd(AppName, WIN_X, WIN_Y, WIN_WIDTH, WIN_HEIGHT, hInstance);
    myOglWnd.InitAndShowWindow(INIT_MASK_ENABLE_DEPTH);
    myOglWnd.RunGameLoop();
    return 0;
}