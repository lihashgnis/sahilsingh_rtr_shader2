#include "../../../../99_AuxStuff/MyLibs/Utility/Util.hpp"
#include <gl\GL.h>
#include <gl/GLU.h>
#include<cmath>

constexpr UINT WIN_WIDTH = 800;
constexpr UINT WIN_HEIGHT = 800;
constexpr UINT WIN_X = 100;
constexpr UINT WIN_Y = 100;
UINT NUM_PTS = 100;
double RADIUS = 0.5;

double curAngle = 0;

//Vertices of the following Cube
constexpr util::DataTypes::POINT3Dd g_pts_cube[] = {
    {-1,1,1}, {-1,-1,1}, {1,-1,1}, {1,1,1},     //front face: P0 to P3
    {-1,1,-1}, {-1,-1,-1}, {1,-1,-1}, {1,1,-1}  //back face : P4 to P7
};

/*
                        P4                     P7
                         ---------------------------         -
                       -/|                       -/|
                      /  |                      /  |
                    -/   |                    -/   |
                   /     |                   /     |
              P0 -/      |             P3  -/      |
                /--------|----------------/        |
                |        |                |        |
                |        |                |        |
                |        |                |        |
                |     P5 |                |     P6 |
                |        ---------------------------
                |     --/                 |      -/
                |     /                   |     /
                |   -/                    |   -/
                |  /                      |  /       -
             P1 |-/                    P2 |-/
                /-------------------------/
*/

//Vertices of the following Pyramid
constexpr util::DataTypes::POINT3Dd g_pts_Pyramid[] = {
    {0, 1, 0}, //P0
    {-1, -1, -1}, //P1
    {-1, -1, 1}, //P2
    {1, -1, 1}, //P3
    {1, -1, -1}, //P4
};
/*

               P0

                |
              -/|\-
             //  \ \
           -/ |   \ \-
         -/  /     \  \-
        /   /      |    \
 P1   -/   /        \    \-    P4
     /----/----------\-----\
     |    |           \    |
     |   /             \   |
     |  /               \  |                   -
     | /                |  |             -
     | |                 \ |
 P2  |/                   \|   P3
     /---------------------\

*/

#define VQ(i) glVertex3f(g_pts_cube[i].x, g_pts_cube[i].y, g_pts_cube[i].z); //Vertex Quads
#define Q(a,b,c,d) VQ(a) VQ(b) VQ(c) VQ(d)                       //Quad
#define VT(i) glVertex3f(g_pts_Pyramid[i].x, g_pts_Pyramid[i].y, g_pts_Pyramid[i].z); //Vertex Triangles
#define T(a,b,c) VT(a) VT(b) VT(c)                                 //Triangle

class MyOglWindow : public util::OglWindow
{
    const double PI;
    // Inherited via OglWindow
    virtual const char * GetMessageMap() override
    {
        return nullptr;
    }
    virtual void Display() override
    {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        glTranslatef(0, 0, -12);
        constexpr double sideShift = 2.5;
        constexpr double cubeScale = 0.75;
#pragma region Pyramid
        glPushMatrix();
            glTranslatef(-sideShift, 0, 0);
            glRotatef(curAngle, 0, 1, 0);
            glBegin(GL_TRIANGLES);
                //T(0, 1, 2);
                //T(0, 2, 3);
                //T(0, 3, 4);
                //T(0, 4, 1);
            //Triangle 1
            glColor3f(1, 0, 0);
            glVertex3f(0, 1, 0);
            glColor3f(0, 0, 1);
            glVertex3f(-1, -1, 1);
            glColor3f(0, 1, 0);
            glVertex3f(1, -1, 1);

            //Triangle 2
            glColor3f(1, 0, 0);
            glVertex3f(0, 1, 0);
            glColor3f(0, 1, 0);
            glVertex3f(1, -1, 1);
            glColor3f(0, 0, 1);
            glVertex3f(1, -1, -1);

            //Triangle 3
            glColor3f(1, 0, 0);
            glVertex3f(0, 1, 0);
            glColor3f(0, 0, 1);
            glVertex3f(1, -1, -1);
            glColor3f(0, 1, 0);
            glVertex3f(-1, -1, -1);

            //Triangle 4
            glColor3f(1, 0, 0);
            glVertex3f(0, 1, 0);
            glColor3f(0, 1, 0);
            glVertex3f(-1, -1, -1);
            glColor3f(0, 0, 1);
            glVertex3f(-1, -1, 1);
            glEnd();
        glPopMatrix();
#pragma endregion

#pragma region Cube
        glPushMatrix();
            glTranslatef(sideShift, 0, 0);
            glScalef(cubeScale, cubeScale, cubeScale);
            glRotatef(curAngle, 1, 1, 1);
            glBegin(GL_QUADS);
            //top
            glColor3f(1, 0, 0);
            Q(0, 3, 7, 4);
            //bottom
            glColor3f(0, 1, 0);
            Q(5, 1, 2, 6);
            //front
            glColor3f(0, 0, 1);
            Q(0, 1, 2, 3);
            //back
            glColor3f(0, 1, 1);
            Q(4, 5, 6, 7);
            //right
            glColor3f(1, 0, 1);
            Q(3, 2, 6, 7);
            //left
            glColor3f(1, 1, 0);
            Q(0, 1, 5, 4);
            glEnd();
        glPopMatrix();
#pragma endregion
        SwapBuffers(m_Hdc);
    }

    virtual void Resize(int width, int height) override
    {
        if (height == 0)
            height = 1;

        glViewport(0, 0, width, height);

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        gluPerspective(45, ((GLfloat)width) / height, 0.1, 100.0f);
    }

    LRESULT WndProcPost(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
    {
        return LRESULT(0);
    }

    virtual LRESULT WndProcPre(HWND, UINT, WPARAM, LPARAM) override
    {
        return LRESULT();
    }
public:
    MyOglWindow(TCHAR * className, UINT x, UINT y, UINT width, UINT height, HINSTANCE hInstance) : PI(4 * atan(1)), OglWindow(className, x, y, width, height, hInstance)
    {
    }

    // Inherited via OglWindow
    virtual void Update() override
    {
        curAngle += 0.03;
        curAngle = (curAngle > 360) ? curAngle - 360 : curAngle;
        curAngle = (curAngle < -360) ? curAngle + 360 : curAngle;
    }
};

int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
    TCHAR AppName[] = TEXT("3D Cube");
    MyOglWindow myOglWnd(AppName, WIN_X, WIN_Y, WIN_WIDTH, WIN_HEIGHT, hInstance);
    myOglWnd.InitAndShowWindow(INIT_MASK_ENABLE_DEPTH);
    myOglWnd.RunGameLoop();
    return 0;
}