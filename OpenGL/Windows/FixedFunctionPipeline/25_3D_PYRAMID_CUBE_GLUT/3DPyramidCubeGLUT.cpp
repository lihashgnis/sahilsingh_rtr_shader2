#include <freeglut.h>
#include "../../../../99_AuxStuff/MyLibs/Utility/Util.hpp"

bool isFullScreen = false;

double g_curAngle = 0.0;

//Vertices of the following Cube
constexpr util::DataTypes::POINT3Dd g_pts_cube[] = {
    {-1,1,1}, {-1,-1,1}, {1,-1,1}, {1,1,1},     //front face: P0 to P3
    {-1,1,-1}, {-1,-1,-1}, {1,-1,-1}, {1,1,-1}  //back face : P4 to P7
};

/*
                        P4                     P7
                         ---------------------------         -
                       -/|                       -/|
                      /  |                      /  |
                    -/   |                    -/   |
                   /     |                   /     |
              P0 -/      |             P3  -/      |
                /--------|----------------/        |
                |        |                |        |
                |        |                |        |
                |        |                |        |
                |     P5 |                |     P6 |
                |        ---------------------------
                |     --/                 |      -/
                |     /                   |     /
                |   -/                    |   -/
                |  /                      |  /       -
             P1 |-/                    P2 |-/
                /-------------------------/
*/


#define VQ(i) glVertex3f(g_pts_cube[i].x, g_pts_cube[i].y, g_pts_cube[i].z); //Vertex Quads
#define Q(a,b,c,d) VQ(a) VQ(b) VQ(c) VQ(d)                       //Quad
#define VT(i) glVertex3f(g_pts_Pyramid[i].x, g_pts_Pyramid[i].y, g_pts_Pyramid[i].z); //Vertex Triangles
#define T(a,b,c) VT(a) VT(b) VT(c)                                 //Triangle


int main(int argc, char* argv[])
{
    //function declaration
    void initialize();
    void uninitialize();
    void reshape(int, int);
    void display();
    void keyboard(unsigned char, int, int);
    void mouse(int, int, int, int);
    void update();

    //code
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
    glutInitWindowSize(800, 600);
    glutInitWindowPosition(100, 100);
    glutCreateWindow("My First OpenGL Program");
    initialize();

    //callbacks
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutKeyboardFunc(keyboard);
    glutMouseFunc(mouse);
    glutCloseFunc(uninitialize);
    glutIdleFunc(update);
    glutMainLoop();
    return 0;
}

void update()
{
    g_curAngle += 0.03;
    g_curAngle = (g_curAngle > 360) ? g_curAngle - 360 : g_curAngle;
    g_curAngle = (g_curAngle < -360) ? g_curAngle + 360 : g_curAngle;
    glutPostRedisplay();
}

void initialize()
{
    glClearColor(0.0, 0.0, 0.0, 1.0);

    glClearDepth(1.0);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glShadeModel(GL_SMOOTH);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

}

void uninitialize()
{

}

void reshape(int width, int height)
{
    if (height == 0)
        height = 1;

    glViewport(0, 0, width, height);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    gluPerspective(45, ((GLfloat)width) / height, 0.1, 100.0f);
}

void display()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(0, 0, -12);
    constexpr double sideShift = 2.5;
    constexpr double cubeScale = 0.75;
#pragma region Pyramid
    glPushMatrix();
    glTranslatef(-sideShift, 0, 0);
    glRotatef(g_curAngle, 0, 1, 0);
    glBegin(GL_TRIANGLES);
    //T(0, 1, 2);
    //T(0, 2, 3);
    //T(0, 3, 4);
    //T(0, 4, 1);
//Triangle 1
    glColor3f(1, 0, 0);
    glVertex3f(0, 1, 0);
    glColor3f(0, 0, 1);
    glVertex3f(-1, -1, 1);
    glColor3f(0, 1, 0);
    glVertex3f(1, -1, 1);

    //Triangle 2
    glColor3f(1, 0, 0);
    glVertex3f(0, 1, 0);
    glColor3f(0, 1, 0);
    glVertex3f(1, -1, 1);
    glColor3f(0, 0, 1);
    glVertex3f(1, -1, -1);

    //Triangle 3
    glColor3f(1, 0, 0);
    glVertex3f(0, 1, 0);
    glColor3f(0, 0, 1);
    glVertex3f(1, -1, -1);
    glColor3f(0, 1, 0);
    glVertex3f(-1, -1, -1);

    //Triangle 4
    glColor3f(1, 0, 0);
    glVertex3f(0, 1, 0);
    glColor3f(0, 1, 0);
    glVertex3f(-1, -1, -1);
    glColor3f(0, 0, 1);
    glVertex3f(-1, -1, 1);
    glEnd();
    glPopMatrix();
#pragma endregion

#pragma region Cube
    glPushMatrix();
    glTranslatef(sideShift, 0, 0);
    glScalef(cubeScale, cubeScale, cubeScale);
    glRotatef(g_curAngle, 1, 1, 1);
    glBegin(GL_QUADS);
    //top
    glColor3f(1, 0, 0);
    Q(0, 3, 7, 4);
    //bottom
    glColor3f(0, 1, 0);
    Q(5, 1, 2, 6);
    //front
    glColor3f(0, 0, 1);
    Q(0, 1, 2, 3);
    //back
    glColor3f(0, 1, 1);
    Q(4, 5, 6, 7);
    //right
    glColor3f(1, 0, 1);
    Q(3, 2, 6, 7);
    //left
    glColor3f(1, 1, 0);
    Q(0, 1, 5, 4);
    glEnd();
    glPopMatrix();
#pragma endregion
    glutSwapBuffers();
}

void keyboard(unsigned char key, int x, int y)
{
    switch (key)
    {
    case 27://escape
    {
        glutLeaveMainLoop();
    }
    break;
    case 'f':
    case 'F':
        if (isFullScreen == false)
        {
            glutFullScreen();
            isFullScreen = true;
        }
        else
        {
            glutLeaveFullScreen();
            isFullScreen = false;
        }
        break;
    default:
        break;
    }
}

void mouse(int button, int state, int x, int y)
{
    switch (button)
    {
    case GLUT_LEFT_BUTTON:
        break;
    case GLUT_RIGHT_BUTTON:
    {
        glutLeaveMainLoop();
    }
    break;
    default:
        break;
    }
}