#include <freeglut.h>

bool bIsFullScreen = false;

void initialize();
void uninitialize();
void reshape(int, int);
void display();
void keyboard(unsigned char, int , int);
void mouse(int, int, int, int);

int g_year = 0;
int g_day = 0;

int main(int argc, char* argv[])
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_DEPTH | GLUT_RGBA);
    glutInitWindowSize(800, 600);
    glutInitWindowPosition(100, 100);
    glutCreateWindow("Push Matrix Demo");
    initialize();

    //callbacks
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutKeyboardFunc(keyboard);
    glutMouseFunc(mouse);
    glutCloseFunc(uninitialize);
    glutMainLoop();
    return 0;
}

void initialize()
{
    glClearColor(0, 0, 0, 1);
    glShadeModel(GL_FLAT);
}

void uninitialize()
{

}

void reshape(int w, int h)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60, GLfloat(w) / h, 1, 20);
}

void display()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(0, 0, 5, 0, 0, 0, 0, 1, 0);
    glPushMatrix();
    glutWireSphere(1, 20, 16);//sun
    glRotatef(g_year, 0, 1, 0);
    glTranslatef(2, 0, 0);
    glRotatef(g_day, 0, 1, 0);
    glutWireSphere(0.2, 10, 8);
    glPopMatrix();
    glutSwapBuffers();
}

void keyboard(unsigned char key, int x, int y)
{
    switch (key)
    {
    case 27://ESC
        glutLeaveMainLoop();
        break;
    case 'f':
    case 'F':
        if (bIsFullScreen)
        {
            glutLeaveFullScreen();
            bIsFullScreen = false;
        }
        else
        {
            glutFullScreen();
            bIsFullScreen = true;
        }
        break;
    case 'Y':
    {
        g_year = (g_year + 5) % 360;
        glutPostRedisplay();
    }
    break;
    case 'y':
    {
        g_year = (g_year - 5) % 360;
        glutPostRedisplay();
    }
    break;
    case 'D':
    {
        g_day = (g_day + 5) % 360;
        glutPostRedisplay();
    }
    break;
    case 'd':
    {
        g_day = (g_day - 5) % 360;
        glutPostRedisplay();
    }
    break;
    default:
        break;
    }
}

void mouse(int button, int state, int x, int y)
{

}