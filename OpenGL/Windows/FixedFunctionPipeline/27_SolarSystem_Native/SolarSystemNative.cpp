#include "../../../../99_AuxStuff/MyLibs/Utility/Util.hpp"
#include <gl\GL.h>
#include <gl/GLU.h>
#include<cmath>

constexpr UINT WIN_WIDTH = 800;
constexpr UINT WIN_HEIGHT = 600;
constexpr UINT WIN_X = 100;
constexpr UINT WIN_Y = 100;

int g_year = 0;
int g_day = 0;

class MyOglWindow : public util::OglWindow
{
    const double PI;
    // Inherited via OglWindow
    virtual const char * GetMessageMap() override
    {
        return nullptr;
    }
    virtual void Display() override
    {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        gluLookAt(0, 0, 5, 0, 0, 0, 0, 1, 0);
        auto object  = gluNewQuadric();
        gluQuadricDrawStyle(object, GLU_LINE);
        glPushMatrix();
        glRotatef(90, 1, 0, 0);
        gluSphere(object, 1, 20, 20);//sun
        glPopMatrix();
        glRotatef(g_year, 0, 1, 0);
        glTranslatef(2, 0, 0);
        glRotatef(g_day, 0, 1, 0);
        glPushMatrix();
        glRotatef(90, 1, 0, 0);
        gluSphere(object, 0.2, 20, 20);//earth
        glPopMatrix();
        SwapBuffers(m_Hdc);
        gluDeleteQuadric(object);
    }

    virtual void Resize(int width, int height) override
    {
        if (height == 0)
            height = 1;

        glViewport(0, 0, width, height);

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        gluPerspective(45, ((GLfloat)width) / height, 1, 20.0f);
    }

    LRESULT WndProcPost(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
    {
        switch (iMsg)
        {
        case WM_CHAR:
        {
            switch (wParam)
            {
            case 'Y':
                g_year = (g_year + 5) % 360;
                break;
            case 'y':
                g_year = (g_year - 5) % 360;
                break;
            case 'D':
            {
                g_day = (g_day + 5) % 360;
            }
            break;
            case 'd':
            {
                g_day = (g_day - 5) % 360;
            }
            break;
            }
        }
        break;
        default:
            break;
        }
        return LRESULT(0);
    }

    virtual LRESULT WndProcPre(HWND, UINT, WPARAM, LPARAM) override
    {
        return LRESULT();
    }
public:
    MyOglWindow(TCHAR * className, UINT x, UINT y, UINT width, UINT height, HINSTANCE hInstance) : PI(4 * atan(1)), OglWindow(className, x, y, width, height, hInstance)
    {
    }

    // Inherited via OglWindow
    virtual void Update() override
    {
        
    }
};

int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
    TCHAR AppName[] = TEXT("Solar System Native");
    MyOglWindow myOglWnd(AppName, WIN_X, WIN_Y, WIN_WIDTH, WIN_HEIGHT, hInstance);
    myOglWnd.InitAndShowWindow(INIT_MASK_ENABLE_DEPTH);
    myOglWnd.RunGameLoop();
    return 0;
}