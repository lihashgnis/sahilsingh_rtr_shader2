#include "../../../../99_AuxStuff/MyLibs/Utility/Util.hpp"
#include <gl\GL.h>
#include <gl/GLU.h>
#include <cmath>
#include <algorithm>

constexpr UINT WIN_WIDTH = 800;
constexpr UINT WIN_HEIGHT = 600;
constexpr UINT WIN_X = 100;
constexpr UINT WIN_Y = 100;

double g_shoulder = 0;
double g_elbow = 0;

class MyOglWindow : public util::OglWindow
{
    const double PI;
    // Inherited via OglWindow
    virtual const char * GetMessageMap() override
    {
        return nullptr;
    }
    virtual void Display() override
    {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        glColor3f(0.5, 0.35, 0.05);
        glTranslatef(0, 0, -12);
        glPushMatrix();
            glRotatef(g_shoulder, 0, 0, 1);
            glTranslatef(1, 0, 0);
            glPushMatrix();
                glScalef(2, 0.5, 1);
                auto quadric = gluNewQuadric();
                gluSphere(quadric, 0.5, 10, 10);//shoulder
                gluDeleteQuadric(quadric);
            glPopMatrix();
            glTranslatef(1, 0, 0);
            glRotatef(g_elbow, 0, 0, 1);
            glTranslatef(1, 0, 0);
            glPushMatrix();
                glScalef(2, 0.5, 1);
                quadric = gluNewQuadric();
                gluSphere(quadric, 0.5, 10, 10);//arm
                gluDeleteQuadric(quadric);
            glPopMatrix();
            glPopMatrix();
        SwapBuffers(m_Hdc);
    }

    virtual void Resize(int width, int height) override
    {
        if (height == 0)
            height = 1;

        glViewport(0, 0, width, height);

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        gluPerspective(45, ((GLfloat)width) / height, 1, 20.0f);
    }

    LRESULT WndProcPost(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
    {
        switch (iMsg)
        {
        case WM_CHAR:
        {
            switch (wParam)
            {
            case 's':
            {
                g_shoulder = std::clamp(g_shoulder + 5, -45.0, 45.0);
            }
            break;
            case 'S':
            {
                g_shoulder = std::clamp(g_shoulder - 5, -45.0, 45.0);
            }
            break;
            case 'e':
            {
                g_elbow = std::clamp(g_elbow + 5, -45.0, 45.0);
            }
            break;
            case 'E':
            {
                g_elbow = std::clamp(g_elbow - 5, 0.0, 45.0);
            }
            break;
            }
        }
        break;
        default:
            break;
        }
        return LRESULT(0);
    }

    virtual LRESULT WndProcPre(HWND, UINT, WPARAM, LPARAM) override
    {
        return LRESULT();
    }
public:
    MyOglWindow(TCHAR * className, UINT x, UINT y, UINT width, UINT height, HINSTANCE hInstance) : PI(4 * atan(1)), OglWindow(className, x, y, width, height, hInstance)
    {
    }

    // Inherited via OglWindow
    virtual void Update() override
    {

    }
};

int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
    TCHAR AppName[] = TEXT("Robotic ARM");
    MyOglWindow myOglWnd(AppName, WIN_X, WIN_Y, WIN_WIDTH, WIN_HEIGHT, hInstance);
    myOglWnd.InitAndShowWindow(INIT_MASK_ENABLE_DEPTH);
    myOglWnd.RunGameLoop();
    return 0;
}