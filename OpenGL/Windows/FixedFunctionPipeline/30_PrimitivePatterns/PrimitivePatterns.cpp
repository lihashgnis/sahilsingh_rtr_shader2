#include "../../../../99_AuxStuff/MyLibs/Utility/Util.hpp"
#include <gl\GL.h>
#include <gl/GLU.h>
#include <cmath>
#include <algorithm>

constexpr UINT WIN_WIDTH = 800;
constexpr UINT WIN_HEIGHT = 800;
constexpr UINT WIN_X = 100;
constexpr UINT WIN_Y = 100;

double dbg_y = 0;

class MyOglWindow : public util::OglWindow
{
    const double PI;
    // Inherited via OglWindow
    virtual const char * GetMessageMap() override
    {
        return nullptr;
    }
    virtual void Display() override
    {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glColor3f(1, 1, 1);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        glTranslatef(0, 0, -3);
#pragma region Pattern 0,0
        {//Patern 0,0
            glLoadIdentity();
            glTranslatef(0, 0, -3);
            glTranslatef(-0.8, 0.5, 0);
            static BYTE stipplePattern[128] = {1};//32x32 bits
            glEnable(GL_POLYGON_STIPPLE);
            glPolygonStipple(stipplePattern);
            glBegin(GL_QUADS);
            glVertex2f(0, 0);
            glVertex2f(0.4, 0);
            glVertex2f(0.4, 0.4);
            glVertex2f(0, 0.4);
            glEnd();
            glDisable(GL_POLYGON_STIPPLE);
        }
#pragma endregion

#pragma region Pattern 1,0
        {//Pattern 1,0
            glLoadIdentity();
            glTranslatef(0.04, 0, -3);//We have to be very carefull with position of quad within window as the stipple pattern rendering is dependent on it
            glTranslatef(-0.8, -0.49, 0);
            static BYTE stipplePattern[128] = { 
                0xff, 0xff, 0xff, 0xff,
                0x40, 0x00, 0x00, 0x01, 
                0x20, 0x00, 0x00, 0x01, 
                0x10, 0x00, 0x00, 0x01, 
                0x08, 0x00, 0x00, 0x01, 
                0x04, 0x00, 0x00, 0x01, 
                0x02, 0x00, 0x00, 0x01, 
                0x01, 0x00, 0x00, 0x01, 
                0x00, 0x80, 0x00, 0x01,
                0x00, 0x40, 0x00, 0x01,
                0x00, 0x20, 0x00, 0x01,
                0x00, 0x10, 0x00, 0x01,
                0x00, 0x08, 0x00, 0x01,
                0x00, 0x04, 0x00, 0x01,
                0x00, 0x02, 0x00, 0x01,
                0x00, 0x01, 0x00, 0x01,
                0x00, 0x00, 0x80, 0x01,
                0x00, 0x00, 0x40, 0x01,
                0x00, 0x00, 0x20, 0x01,
                0x00, 0x00, 0x10, 0x01,
                0x00, 0x00, 0x08, 0x01,
                0x00, 0x00, 0x04, 0x01,
                0x00, 0x00, 0x02, 0x01,
                0x00, 0x00, 0x01, 0x01,
                0x00, 0x00, 0x00, 0x81,
                0x00, 0x00, 0x00, 0x41,
                0x00, 0x00, 0x00, 0x21,
                0x00, 0x00, 0x00, 0x11,
                0x00, 0x00, 0x00, 0x09,
                0x00, 0x00, 0x00, 0x05,
                0x00, 0x00, 0x00, 0x03,
                0x00, 0x00, 0x00, 0x01
            };
            glEnable(GL_POLYGON_STIPPLE);
            glPolygonStipple(stipplePattern);
            glBegin(GL_QUADS);
            glVertex2f(0, -0.02);
            glVertex2f(0.4 + 0.02, 0 - 0.02);
            glVertex2f(0.4 + 0.02, 0.4);
            glVertex2f(0, 0.4);
            glEnd();
            glDisable(GL_POLYGON_STIPPLE);
        }
#pragma endregion

#pragma region Pattern 0,2
        {//Pattern 0,2
            glLoadIdentity();
            glTranslatef(0.04, 0, -3);//We have to be very carefull with position of quad within window as the stipple pattern rendering is dependent on it
            glTranslatef(0.8 - 0.031, 0.49 - 0.04, 0);
            static BYTE stipplePattern[128] = {
                0xff, 0xff, 0xff, 0xff,
                0x00, 0x00, 0x00, 0x01,
                0x00, 0x00, 0x00, 0x01,
                0x00, 0x00, 0x00, 0x01,
                0x00, 0x00, 0x00, 0x01,
                0x00, 0x00, 0x00, 0x01,
                0x00, 0x00, 0x00, 0x01,
                0x00, 0x00, 0x00, 0x01,
                0x00, 0x00, 0x00, 0x01,
                0x00, 0x00, 0x00, 0x01,
                0x00, 0x00, 0x00, 0x01,
                0x00, 0x00, 0x00, 0x01,
                0x00, 0x00, 0x00, 0x01,
                0x00, 0x00, 0x00, 0x01,
                0x00, 0x00, 0x00, 0x01,
                0x00, 0x00, 0x00, 0x01,
                0x00, 0x00, 0x00, 0x01,
                0x00, 0x00, 0x00, 0x01,
                0x00, 0x00, 0x00, 0x01,
                0x00, 0x00, 0x00, 0x01,
                0x00, 0x00, 0x00, 0x01,
                0x00, 0x00, 0x00, 0x01,
                0x00, 0x00, 0x00, 0x01,
                0x00, 0x00, 0x00, 0x01,
                0x00, 0x00, 0x00, 0x01,
                0x00, 0x00, 0x00, 0x01,
                0x00, 0x00, 0x00, 0x01,
                0x00, 0x00, 0x00, 0x01,
                0x00, 0x00, 0x00, 0x01,
                0x00, 0x00, 0x00, 0x01,
                0x00, 0x00, 0x00, 0x01,
                0x00, 0x00, 0x00, 0x01
            };
            glEnable(GL_POLYGON_STIPPLE);
            glPolygonStipple(stipplePattern);
            glBegin(GL_QUADS);
            glVertex2f(-0.001, -0.022);
            glVertex2f(0.4 + 0.02, -0.022);
            glVertex2f(0.4 + 0.02 , 0.4);
            glVertex2f(-0.001, 0.4);
            glEnd();
            glDisable(GL_POLYGON_STIPPLE);
        }
#pragma endregion
#pragma region Pattern 0,1
        {
            glLoadIdentity();
            glTranslatef(0.04, 0, -3);
            glTranslatef(0, 0.49 - 0.04, 0);
            for (int i = 0; i < 4; i++)
            {
                glPushMatrix();
                for (int j = 0; j < 4; j++)
                {
                    glBegin(GL_LINE_LOOP);
                    glVertex2f(0, 0);
                    glVertex2f(0.1, 0.1);
                    glVertex2f(0, 0.1);
                    glEnd();
                    glTranslatef(0.1, 0, 0);
                }
                glPopMatrix();
                glTranslatef(0, 0.1, 0);
            }
        }
#pragma endregion

#pragma region Pattern 1,1
        {
            glLoadIdentity();
            glTranslatef(0.04, 0, -3);
            glTranslatef(0, -0.49, 0);
            glBegin(GL_LINE_LOOP);
            glVertex2f(0, 0);
            glVertex2f(0.4, 0);
            glVertex2f(0.4, 0.4);
            glVertex2f(0, 0.4);
            glEnd();
            glBegin(GL_LINES);
            glVertex2f(0, 0.4);
            glVertex2f(0.4, 0.4*(2.0/3.0));

            glVertex2f(0, 0.4);
            glVertex2f(0.4, 0.4*(1.0 / 3.0));

            glVertex2f(0, 0.4);
            glVertex2f(0.4, 0);

            //####

            glVertex2f(0, 0.4);
            glVertex2f(0.4*(2.0 / 3.0), 0);

            glVertex2f(0, 0.4);
            glVertex2f(0.4*(1.0 / 3.0), 0);
            glEnd();
        }
#pragma endregion

#pragma region Pattern 1,2
        {
            glLoadIdentity();
            glTranslatef(0.04, 0, -3);
            glTranslatef(0.8, -0.49, 0);
            
            glBegin(GL_QUADS);
            glColor3f(1, 0, 0);
            glVertex2f(0, 0);
            glVertex2f(0.4/3, 0);
            glVertex2f(0.4/3, 0.4);
            glVertex2f(0, 0.4);

            glColor3f(0, 1, 0);
            glVertex2f(0.4 / 3, 0);
            glVertex2f(0.4 *(2.0 / 3.0), 0);
            glVertex2f(0.4 *(2.0 / 3.0), 0.4);
            glVertex2f(0.4 / 3, 0.4);
            
            glColor3f(0, 0, 1);
            glVertex2f(0.4 *(2.0 / 3.0), 0);
            glVertex2f(0.4 , 0);
            glVertex2f(0.4 , 0.4);
            glVertex2f(0.4 *(2.0 / 3.0), 0.4);
            glEnd();

            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    glBegin(GL_LINES);
                    glColor3f(1, 1, 1);
                    glVertex2f(0.4*(j / 3.0), 0);
                    glVertex2f(0.4*(j / 3.0), 0.4);
                    glEnd();
                }
                glTranslatef(0.4, 0, 0);
                glRotatef(90, 0, 0, 1);
            }
        }
#pragma endregion
        SwapBuffers(m_Hdc);
    }

    virtual void Resize(int width, int height) override
    {
        if (height == 0)
            height = 1;

        glViewport(0, 0, width, height);

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        gluPerspective(45, ((GLfloat)width) / height, 1, 20.0f);
    }

    LRESULT WndProcPost(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
    {
        switch (iMsg)
        {
        case WM_CHAR:
        {

        }
        break;
        case WM_KEYDOWN:
        {
            switch (wParam)
            {
            case VK_UP:
                dbg_y += 0.001;
                break;
            case VK_DOWN:
                dbg_y -= 0.001;
                break;
            default:
                break;
            }
        }
        default:
            break;
        }
        return LRESULT(0);
    }

    virtual LRESULT WndProcPre(HWND, UINT iMsg, WPARAM, LPARAM) override
    {
        switch (iMsg)
        {
        case WM_CHAR:
            return LRESULT(-1);
            break;
        default:
            break;
        }
        return LRESULT();
    }
public:
    MyOglWindow(TCHAR * className, UINT x, UINT y, UINT width, UINT height, HINSTANCE hInstance) : PI(4 * atan(1)), OglWindow(className, x, y, width, height, hInstance)
    {
    }

    // Inherited via OglWindow
    virtual void Update() override
    {

    }
};

int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
    TCHAR AppName[] = TEXT("Primitive Patterns");
    MessageBoxA(NULL, "Stippling pattern appearance depends on position of quad within window", "Carefull", MB_OK);
    MyOglWindow myOglWnd(AppName, WIN_X, WIN_Y, WIN_WIDTH, WIN_HEIGHT, hInstance);
    myOglWnd.InitAndShowWindow(INIT_MASK_ENABLE_DEPTH);
    //Disable resizing - messes with stipple pattern
    auto style = ::GetWindowLong(myOglWnd.GetHwnd(), GWL_STYLE);
    style ^= WS_THICKFRAME;
    style ^= WS_MAXIMIZEBOX;
    style ^= WS_MAXIMIZE;
    ::SetWindowLong(myOglWnd.GetHwnd(), GWL_STYLE, style);
    myOglWnd.RunGameLoop();
    return 0;
}