#include "../../../../99_AuxStuff/MyLibs/Utility/Util.hpp"
#include <gl\GL.h>
#include <gl/GLU.h>
#include<cmath>

constexpr UINT WIN_WIDTH = 800;
constexpr UINT WIN_HEIGHT = 800;
constexpr UINT WIN_X = 100;
constexpr UINT WIN_Y = 100;
UINT NUM_PTS = 100;
double RADIUS = 0.5;

double curAngle = 0;

GLuint texture_stone;

BOOL LoadTexture(GLuint *texture, const TCHAR imageResourceID[]);

class MyOglWindow : public util::OglWindow
{
    const double PI;
    // Inherited via OglWindow
    virtual const char * GetMessageMap() override
    {
        return nullptr;
    }

    virtual void Display() override
    {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        glTranslatef(0, 0, -6);
        glRotatef(curAngle, 0, 1, 0);
        glBindTexture(GL_TEXTURE_2D, texture_stone);

        glBegin(GL_TRIANGLES);
        //Triangle 1
        //glColor3f(1, 0, 0);
        glTexCoord2f(0.5, 1);
        glVertex3f(0, 1, 0);
        //glColor3f(0, 0, 1);
        glTexCoord2f(0, 0);
        glVertex3f(-1, -1, 1);
        //glColor3f(0, 1, 0);
        glTexCoord2f(1, 0);
        glVertex3f(1, -1, 1);

        //Triangle 2
        //glColor3f(1, 0, 0);
        glTexCoord2f(0.5, 1);
        glVertex3f(0, 1, 0);
        //glColor3f(0, 1, 0);
        glTexCoord2f(1, 0);
        glVertex3f(1, -1, 1);
        //glColor3f(0, 0, 1);
        glTexCoord2f(0, 0);
        glVertex3f(1, -1, -1);

        //Triangle 3
        //glColor3f(1, 0, 0);
        glTexCoord2f(0.5, 1);
        glVertex3f(0, 1, 0);
        //glColor3f(0, 0, 1);
        glTexCoord2f(0, 0);
        glVertex3f(1, -1, -1);
        //glColor3f(0, 1, 0);
        glTexCoord2f(1, 0);
        glVertex3f(-1, -1, -1);

        //Triangle 4
        //glColor3f(1, 0, 0);
        glTexCoord2f(0.5, 1);
        glVertex3f(0, 1, 0);
        //glColor3f(0, 1, 0);
        glTexCoord2f(1, 0);
        glVertex3f(-1, -1, -1);
        //glColor3f(0, 0, 1);
        glTexCoord2f(0, 0);
        glVertex3f(-1, -1, 1);
        glEnd();
        SwapBuffers(m_Hdc);
    }

    virtual void Resize(int width, int height) override
    {
        if (height == 0)
            height = 1;

        glViewport(0, 0, width, height);

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        gluPerspective(45, ((GLfloat)width) / height, 0.1, 100.0f);
    }

    LRESULT WndProcPost(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
    {
        return LRESULT(0);
    }

    virtual LRESULT WndProcPre(HWND, UINT, WPARAM, LPARAM) override
    {
        return LRESULT();
    }
public:
    MyOglWindow(TCHAR * className, UINT x, UINT y, UINT width, UINT height, HINSTANCE hInstance) : PI(4 * atan(1)), OglWindow(className, x, y, width, height, hInstance)
    {
    }

    // Inherited via OglWindow
    virtual void Update() override
    {
        curAngle += 0.03;
        curAngle = (curAngle > 360) ? curAngle - 360 : curAngle;
        curAngle = (curAngle < -360) ? curAngle + 360 : curAngle;
    }
};

int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
    TCHAR AppName[] = TEXT("Textured 3D Pyramid");
    MyOglWindow myOglWnd(AppName, WIN_X, WIN_Y, WIN_WIDTH, WIN_HEIGHT, hInstance);
    myOglWnd.InitAndShowWindow(INIT_MASK_ENABLE_DEPTH | INIT_MASK_ENABLE_TEXTURE_2D);
    LoadTexture(&texture_stone, TEXT("ID_BITMAP_STONE"));
    myOglWnd.RunGameLoop();
    glDeleteTextures(1, &texture_stone);
    return 0;
}

BOOL LoadTexture(GLuint *texture, const TCHAR imageResourceID[])
{
    HBITMAP hBitmap = NULL;
    BITMAP bmp;
    BOOL bStatus = FALSE;
    hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL), imageResourceID, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);

    if (hBitmap != NULL)
    {
        bStatus = TRUE;
        GetObject(hBitmap, sizeof(bmp), &bmp);
        //Now we have image data in bmp
        glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
        glGenTextures(1, texture);
        glBindTexture(GL_TEXTURE_2D, *texture);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        gluBuild2DMipmaps(GL_TEXTURE_2D, 3, bmp.bmWidth, bmp.bmHeight, GL_BGR_EXT, GL_UNSIGNED_BYTE, bmp.bmBits);
        DeleteObject(hBitmap);
    }
    return bStatus;
}