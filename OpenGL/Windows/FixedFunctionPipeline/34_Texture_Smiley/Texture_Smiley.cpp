#include "../../../../99_AuxStuff/MyLibs/Utility/Util.hpp"
#include <gl\GL.h>
#include <gl/GLU.h>
#include<cmath>
#include <vector>

constexpr UINT WIN_WIDTH = 800;
constexpr UINT WIN_HEIGHT = 800;
constexpr UINT WIN_X = 100;
constexpr UINT WIN_Y = 100;
UINT NUM_PTS = 100;

UINT g_KeyPressed = 0;

double RADIUS = 0.5;

double curAngle = 0;

GLuint texture_smiley;

BOOL LoadTexture(GLuint *texture, const TCHAR imageResourceID[]);

//Vertices of the following cube
constexpr util::DataTypes::POINT3Dd g_pts[] = {
    {-1,1,1}, {-1,-1,1}, {1,-1,1}, {1,1,1},     //front face: P0 to P3
    {-1,1,-1}, {-1,-1,-1}, {1,-1,-1}, {1,1,-1}  //back face : P4 to P7
};

/*
                        P4                     P7
                         ---------------------------         -
                       -/|                       -/|
                      /  |                      /  |
                    -/   |                    -/   |
                   /     |                   /     |
              P0 -/      |             P3  -/      |
                /--------|----------------/        |
                |        |                |        |
                |        |                |        |
                |        |                |        |
                |     P5 |                |     P6 |
                |        ---------------------------
                |     --/                 |      -/
                |     /                   |     /
                |   -/                    |   -/
                |  /                      |  /       -
             P1 |-/                    P2 |-/
                /-------------------------/
*/

#define V(i) glVertex3f(g_pts[i].x, g_pts[i].y, g_pts[i].z); //Vertex
#define Q(a,b,c,d) V(a) V(b) V(c) V(d)                       //Quad

class MyOglWindow : public util::OglWindow
{
    const double PI;
    // Inherited via OglWindow
    virtual const char * GetMessageMap() override
    {
        return nullptr;
    }

    virtual void Display() override
    {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        glTranslatef(0, 0, -3);

        if(g_KeyPressed >=1 && g_KeyPressed <= 4)
        {
            glEnable(GL_TEXTURE_2D);
            glBindTexture(GL_TEXTURE_2D, texture_smiley);
            std::vector<util::DataTypes::POINTd> texCords;
            switch (g_KeyPressed)
            {
            case 2:
                texCords = { {0,0},{1,0},{1,1},{0,1} };
                break;
            case 1:
                texCords = { {0,0},{0.5,0},{0.5,0.5},{0,0.5} };
                break;
            case 3:
                texCords = { {0,0},{2,0},{2,2},{0,2} };
                break;
            case 4:
                texCords = { {0.5,0.5}, {0.5,0.5},{0.5,0.5},{0.5,0.5} };
                break;
            default:
                THROW_NOT_IMPLEMENTED;
                break;
            }
            glBegin(GL_QUADS);
            glTexCoord2f(texCords[0].x, texCords[0].y);
            glVertex2f(-1, -1);
            glTexCoord2f(texCords[1].x, texCords[1].y);
            glVertex2f(1, -1);
            glTexCoord2f(texCords[2].x, texCords[2].y);
            glVertex2f(1, 1);
            glTexCoord2f(texCords[3].x, texCords[3].y);
            glVertex2f(-1, 1);
            glEnd();
        }
        else
        {
            glDisable(GL_TEXTURE_2D);
            glBegin(GL_QUADS);
            glColor3f(1, 1, 1);
            glVertex2f(-1, -1);
            glVertex2f(1, -1);
            glVertex2f(1, 1);
            glVertex2f(-1, 1);
            glEnd();
        }
        SwapBuffers(m_Hdc);
    }

    virtual void Resize(int width, int height) override
    {
        if (height == 0)
            height = 1;

        glViewport(0, 0, width, height);

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        gluPerspective(45, ((GLfloat)width) / height, 0.1, 100.0f);
    }

    LRESULT WndProcPost(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
    {
        return LRESULT(0);
    }

    virtual LRESULT WndProcPre(HWND, UINT iMsg, WPARAM wParam, LPARAM) override
    {
        switch (iMsg)
        {
        case WM_CHAR:
        {
            switch (wParam)
            {
            case '0':
                g_KeyPressed = 0;
                break;
            case '1':
                g_KeyPressed = 1;
                break;
            case '2':
                g_KeyPressed = 2;
                break;
            case '3':
                g_KeyPressed = 3;
                break;
            case '4':
                g_KeyPressed = 4;
                break;
            default:
                g_KeyPressed = 0;
                break;
            }

        }
        break;
        default:
            break;
        }
        return LRESULT();
    }
public:
    MyOglWindow(TCHAR * className, UINT x, UINT y, UINT width, UINT height, HINSTANCE hInstance) : PI(4 * atan(1)), OglWindow(className, x, y, width, height, hInstance)
    {
    }

    // Inherited via OglWindow
    virtual void Update() override
    {
        curAngle += 0.03;
        curAngle = (curAngle > 360) ? curAngle - 360 : curAngle;
        curAngle = (curAngle < -360) ? curAngle + 360 : curAngle;
    }
};

int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
    MessageBoxA(NULL, "Press keys 0-4", "Info", MB_OK);
    TCHAR AppName[] = TEXT("Textured Smiley");
    MyOglWindow myOglWnd(AppName, WIN_X, WIN_Y, WIN_WIDTH, WIN_HEIGHT, hInstance);
    myOglWnd.InitAndShowWindow(INIT_MASK_ENABLE_DEPTH | INIT_MASK_ENABLE_TEXTURE_2D);
    LoadTexture(&texture_smiley, TEXT("ID_BITMAP_SMILEY"));
    myOglWnd.RunGameLoop();
    glDeleteTextures(1, &texture_smiley);
    return 0;
}

BOOL LoadTexture(GLuint *texture, const TCHAR imageResourceID[])
{
    HBITMAP hBitmap = NULL;
    BITMAP bmp;
    BOOL bStatus = FALSE;
    hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL), imageResourceID, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);

    if (hBitmap != NULL)
    {
        bStatus = TRUE;
        GetObject(hBitmap, sizeof(bmp), &bmp);
        //Now we have image data in bmp
        glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
        glGenTextures(1, texture);
        glBindTexture(GL_TEXTURE_2D, *texture);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        gluBuild2DMipmaps(GL_TEXTURE_2D, 3, bmp.bmWidth, bmp.bmHeight, GL_BGR_EXT, GL_UNSIGNED_BYTE, bmp.bmBits);
        DeleteObject(hBitmap);
    }
    return bStatus;
}