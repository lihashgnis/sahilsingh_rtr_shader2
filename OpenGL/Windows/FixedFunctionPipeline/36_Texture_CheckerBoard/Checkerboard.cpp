#include "../../../../99_AuxStuff/MyLibs/Utility/Util.hpp"
#include <gl\GL.h>
#include <gl/GLU.h>
#include<cmath>

constexpr UINT WIN_WIDTH = 800;
constexpr UINT WIN_HEIGHT = 800;
constexpr UINT WIN_X = 100;
constexpr UINT WIN_Y = 100;
UINT NUM_PTS = 100;
double RADIUS = 0.5;

double curAngle = 0;

GLuint texture_checkerboard;

constexpr UINT CHECK_IMAGE_WIDTH = 64;
constexpr UINT CHECK_IMAGE_HEIGHT = 64;
GLubyte g_check_image[CHECK_IMAGE_HEIGHT][CHECK_IMAGE_WIDTH][4];

void LoadTexture();

class MyOglWindow : public util::OglWindow
{
    const double PI;
    // Inherited via OglWindow
    virtual const char * GetMessageMap() override
    {
        return nullptr;
    }

    virtual void Display() override
    {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        glTranslatef(0, 0, -3.6);
        glBindTexture(GL_TEXTURE_2D, texture_checkerboard);

        glBegin(GL_QUADS);
#pragma region quad1
        glTexCoord2f(0, 0);
        glVertex3f(-2, -1, 0);

        glTexCoord2f(0, 1);
        glVertex3f(-2, 1, 0);

        glTexCoord2f(1, 1);
        glVertex3f(0, 1, 0);

        glTexCoord2f(1, 0);
        glVertex3f(0, -1, 0);
#pragma endregion
#pragma region quad2
        glTexCoord2f(0, 0);
        glVertex3f(1, -1, 0);

        glTexCoord2f(0, 1);
        glVertex3f(1, 1, 0);

        glTexCoord2f(1, 1);
        glVertex3f(2.41421, 1, -1.41421);

        glTexCoord2f(1, 0);
        glVertex3f(2.41421, -1, -1.41421);
#pragma endregion
        glEnd();
        
        SwapBuffers(m_Hdc);
    }

    virtual void Resize(int width, int height) override
    {
        if (height == 0)
            height = 1;

        glViewport(0, 0, width, height);

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        gluPerspective(60, ((GLfloat)width) / height, 1, 30);
    }

    LRESULT WndProcPost(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
    {
        return LRESULT(0);
    }

    virtual LRESULT WndProcPre(HWND, UINT, WPARAM, LPARAM) override
    {
        return LRESULT();
    }
public:
    MyOglWindow(TCHAR * className, UINT x, UINT y, UINT width, UINT height, HINSTANCE hInstance) : PI(4 * atan(1)), OglWindow(className, x, y, width, height, hInstance)
    {
    }

    // Inherited via OglWindow
    virtual void Update() override
    {
        curAngle += 0.03;
        curAngle = (curAngle > 360) ? curAngle - 360 : curAngle;
        curAngle = (curAngle < -360) ? curAngle + 360 : curAngle;
    }
};

int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
    TCHAR AppName[] = TEXT("Textured 3D Pyramid");
    MyOglWindow myOglWnd(AppName, WIN_X, WIN_Y, WIN_WIDTH, WIN_HEIGHT, hInstance);
    myOglWnd.InitAndShowWindow(INIT_MASK_ENABLE_DEPTH | INIT_MASK_ENABLE_TEXTURE_2D);
    LoadTexture();
    myOglWnd.RunGameLoop();
    glBindTexture(GL_TEXTURE_2D, 0);
    glDeleteTextures(1, &texture_checkerboard);
    return 0;
}

void MakeCheckImage()
{
    for (int i = 0; i < CHECK_IMAGE_HEIGHT; i++)
    {
        for (int j = 0; j < CHECK_IMAGE_WIDTH; j++)
        {
            GLubyte c = (((i & 0x8) == 0) ^ ((j & 0x8) == 0)) * 255;
            g_check_image[i][j][0] = c;
            g_check_image[i][j][1] = c;
            g_check_image[i][j][2] = c;
            g_check_image[i][j][3] = 255;
        }
    }
}

void LoadTexture()
{
    MakeCheckImage();
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glGenTextures(1, &texture_checkerboard);
    glBindTexture(GL_TEXTURE_2D, texture_checkerboard);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, CHECK_IMAGE_WIDTH, CHECK_IMAGE_HEIGHT, 0, GL_RGBA, GL_UNSIGNED_BYTE, g_check_image);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
}