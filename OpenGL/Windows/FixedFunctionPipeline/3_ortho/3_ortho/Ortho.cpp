#include <Windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include <cstdio>
//#include "../../../../../99_AuxStuff/MyLibs/Logging/Logger.hpp"

#pragma comment (lib, "OpenGl32.lib")
#pragma comment (lib, "glu32.lib")
//CREATE_LOG()
//#define LOG(x,y) 0;

constexpr UINT WIN_WIDTH  = 800;
constexpr UINT WIN_HEIGHT = 600;
constexpr UINT WIN_X      = 100;
constexpr UINT WIN_Y      = 100;

HDC gHdc = NULL;
HGLRC gHrc = NULL;
bool gbActiveWindow = false;
bool gbIsFullScreen = false;

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam);
void ToggleFullscreeen(HWND hwnd);
void Resize(int width, int height);
void Uninitialize(HWND hwnd);
int Initialize(HWND hwnd);
void Display();

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
    TCHAR AppName[] = TEXT("OpenGLOrtho");

	WNDCLASSEX wc = {};
	wc.cbSize = sizeof(wc);
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.lpfnWndProc = WndProc;
	wc.hInstance = hInstance;
	wc.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wc.lpszClassName = AppName;
	wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wc.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);

    if (!RegisterClassEx(&wc))
    {
        MessageBoxA(NULL, "RegisterClassEx() failed", "Error", MB_OK | MB_ICONERROR);
		//LOG(Error, "RegisterClassEx() failed");
        return -1;
    }

	HWND hwnd = CreateWindowEx(WS_EX_APPWINDOW, AppName, AppName, WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE, WIN_X, WIN_Y, WIN_WIDTH, WIN_HEIGHT, NULL, NULL, hInstance, NULL);

	if (!hwnd)
	{
		//LOG(Error, "CreateWindowEx() failed");
		return - 1;
	}

	int iRet = Initialize(hwnd);

	const char *str = nullptr;

	if (iRet < 0)
	{
		switch (iRet)
		{
		case -1:
			str = "choose pixel failed";
			break;
		case -2:
			str = "Set pixel format failed";
			break;
		case -3:
			str = "wglCreateContext failed";
			break;
		case -4:
			str = "wglMakeCurrent failed";
			break;
		default:
			break;
		}
	}
	else
	{
		str = "Initialize successful";
	}

	//LOG(Debug, str);

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Game loop

	bool bDone = false;
	MSG msg;

	while (bDone == false)
	{
		if (PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				Display();
			}
		}
	}
	return 0;
}

int Initialize(HWND hwnd)
{
	PIXELFORMATDESCRIPTOR pfd = {};
	pfd.nSize = sizeof(pfd);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	gHdc = GetDC(hwnd);
	auto iPixelFormatIndex = ChoosePixelFormat(gHdc, &pfd);

	if (iPixelFormatIndex == 0)
		return -1;
	if (SetPixelFormat(gHdc, iPixelFormatIndex, &pfd) == false)
		return -2;

	gHrc = wglCreateContext(gHdc);

	if (gHrc == NULL)
		return 3;

	if (wglMakeCurrent(gHdc, gHrc) == false)
		return -4;

	glClearColor(0.0f, 0, 0, 0);
	Resize(WIN_WIDTH, WIN_HEIGHT);
	return 0;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;
	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_ERASEBKGND://dont want this since defwindow proc will post WM_PAINT after processing this
		return 0;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_CHAR:
		switch (wParam)
		{
		case 'f':
			//[[fallthrough]]
		case 'F':
		{
			ToggleFullscreeen(hwnd);
		}
		break;
		default:
			break;
		}
		break;
	case WM_DESTROY:
		Uninitialize(hwnd);
		PostQuitMessage(0);
		break;
	default:
		break;
	}

	return DefWindowProc(hwnd, iMsg, wParam, lParam);
}


void Display()
{
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glBegin(GL_TRIANGLES);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex2f(0.0f, 50.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex2f(-50.0f, -50.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex2f(50.0f, -50.0f);

	glEnd();
	SwapBuffers(gHdc);
}

void Uninitialize(HWND hwnd)
{
	if (gbIsFullScreen)
	{
		ToggleFullscreeen(hwnd);
	}

	if (wglGetCurrentContext() == gHrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (gHrc)
	{
		wglDeleteContext(gHrc);
		gHrc = NULL;
	}

	if (gHdc)
	{
		ReleaseDC(hwnd, gHdc);
		gHdc = NULL;
	}

	//LOG(Debug, "Log file close");
}

void ToggleFullscreeen(HWND hwnd)
{
	static WINDOWPLACEMENT wpPrev;
	DWORD dwStyle = 0;

	if (gbIsFullScreen == false)
	{
		dwStyle = GetWindowLong(hwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			MONITORINFO mi = {};
			mi.cbSize = sizeof(mi);

			if (GetWindowPlacement(hwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(hwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(hwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(hwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
				ShowCursor(FALSE);
				gbIsFullScreen = true;
			}
		}
	}
	else
	{
		SetWindowLong(hwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(hwnd, &wpPrev);
		SetWindowPos(hwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
		gbIsFullScreen = FALSE;
	}
}

void Resize(int width, int height)
{
	if (height == 0)
		height = 1;
	
	glViewport(0, 0, width, height);
	
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	if (width <= height)
	{
		auto factor = 100.0f*(((GLfloat)height) / ((GLfloat)width));
		glOrtho(-100.0f, 100.0f, -1 * factor, factor, -100.0f, 100.0f);
	}
	else
	{
		auto factor = 100.0f*(((GLfloat)width) / ((GLfloat)height));
		glOrtho(-1*factor, factor, -100.0f, 100.0f, -100.0f, 100.0f);
	}
}
