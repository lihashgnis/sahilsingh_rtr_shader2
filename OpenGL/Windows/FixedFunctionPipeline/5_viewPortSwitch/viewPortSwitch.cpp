#include "../../../../99_AuxStuff/MyLibs/Utility/Util.hpp"
#include <gl\GL.h>


constexpr UINT WIN_WIDTH = 800;
constexpr UINT WIN_HEIGHT = 600;
constexpr UINT WIN_X = 100;
constexpr UINT WIN_Y = 100;


class MyOglWindow : public util::OglWindow
{
    LRESULT OnCharPress(HWND, UINT, WPARAM, LPARAM)
    {

    }
    // Inherited via OglWindow
    const char * GetMessageMap() override
    {
        return nullptr;
    }

    void Display() override
    {


        glClear(GL_COLOR_BUFFER_BIT);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        glBegin(GL_TRIANGLES);

        glColor3f(1.0f, 0.0f, 0.0f);
        glVertex2f(0.0f, 1.0f);

        glColor3f(0.0f, 1.0f, 0.0f);
        glVertex2f(-1.0f, -1.0f);

        glColor3f(0.0f, 0.0f, 1.0f);
        glVertex2f(1.0f, -1.0f);

        glEnd();
        SwapBuffers(m_Hdc);
    }

    LRESULT WndProcPost(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
    {
        return LRESULT(0);
    }

    LRESULT WndProcPre(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
    {
        switch (iMsg)
        {
        case WM_CHAR:
        {
            RECT rc;
            GetClientRect(hwnd, &rc);
            auto width = rc.right - rc.left;
            auto height = rc.bottom - rc.top;

            switch (wParam)
            {
            case '1':
                glViewport(0, 0, width / 2, height / 2);
                break;
            case '2':
                glViewport(width / 2, 0, width / 2, height / 2);
                break;
            case '3':
                glViewport(width / 2, height / 2, width / 2, height / 2);
                break;
            case '4':
                glViewport(0, height / 2, width / 2, height / 2);
                break;
            case '5':
                glViewport(0, 0, width / 2, height);
                break;
            case '6':
                glViewport(width / 2, 0, width / 2, height);
                break;
            case '7':
                glViewport(0, height / 2, width, height / 2);
                break;
            case '8':
                glViewport(0, 0, width, height / 2);
                break;
            case '9':
                glViewport(width / 4, height / 4, width / 2, height / 2);
                break;
            default:
                glViewport(0, 0, width, height);
                break;
            }
        }
        break;
        default:
            break;
        }
        return LRESULT();
    }

    virtual void Resize(int width, int height) override
    {
        glViewport(0, 0, width, height);
    }
public:
    MyOglWindow(TCHAR * className, UINT x, UINT y, UINT width, UINT height, HINSTANCE hInstance) : OglWindow(className, x, y, width, height, hInstance)
    {

    }

    // Inherited via OglWindow
    virtual void Update() override
    {
    }
};

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
    TCHAR AppName[] = TEXT("ViewPortChange");
    MyOglWindow myOglWnd(AppName, WIN_X, WIN_Y, WIN_WIDTH, WIN_HEIGHT, hInstance);
    myOglWnd.InitAndShowWindow();
    myOglWnd.RunGameLoop();
    return 0;
}