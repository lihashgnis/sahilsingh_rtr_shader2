#include "../../../../99_AuxStuff/MyLibs/Utility/Util.hpp"
#include <gl\GL.h>
#include <tuple>


constexpr UINT WIN_WIDTH = 800;
constexpr UINT WIN_HEIGHT = 600;
constexpr UINT WIN_X = 100;
constexpr UINT WIN_Y = 100;


class MyOglWindow : public util::OglWindow
{
    std::tuple<float, float, float> m_color = std::make_tuple(1.0f, 1.0f, 1.0f);//RGB
    // Inherited via OglWindow
    const char * GetMessageMap() override
    {
        return nullptr;
    }

    void Display() override
    {


        glClear(GL_COLOR_BUFFER_BIT);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

        float r, g, b;
        std::tie(r, g, b) = m_color;

        glBegin(GL_LINE_LOOP);
        glColor3f(r, g, b);
        glVertex2f(0.8f, 0.8f);
        glVertex2f(-0.8f, 0.8f);
        glVertex2f(-0.8f, -0.8f);
        glVertex2f(0.8f, -0.8f);
        glEnd();
        SwapBuffers(m_Hdc);
    }

    LRESULT WndProcPost(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
    {
        return LRESULT(0);
    }

    LRESULT WndProcPre(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
    {
        switch (iMsg)
        {
        case WM_KEYDOWN:
        {
            switch (wParam)
            {
            case VK_UP:
            {//Up arrow key

            }
            break;
            case VK_DOWN:
            {//Down key

            }
            break;
            default:
                break;
            }
        }
        break;
        case WM_CHAR:
        {
            switch (wParam)
            {
            case 'b':
            {
                m_color = std::make_tuple(0.0f, 0.0f, 1.0f);
            }
            break;
            case 'w':
            {
                m_color = std::make_tuple(1.0f, 1.0f, 1.0f);
            }
            break;
            default:
                break;
            }
        }
        break;
        default:
            break;
        }
        return LRESULT();
    }

    virtual void Resize(int width, int height) override
    {
        glViewport(0, 0, width, height);
    }
public:
    MyOglWindow(TCHAR * className, UINT x, UINT y, UINT width, UINT height, HINSTANCE hInstance) : OglWindow(className, x, y, width, height, hInstance)
    {

    }

    // Inherited via OglWindow
    virtual void Update() override
    {
    }
};

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
    TCHAR AppName[] = TEXT("Line_RectangleTriangle");
    MessageBoxA(NULL, " b - blue \n w - white \n Up - zoom out \n Down - zoom in", "Info", MB_OK);
    MyOglWindow myOglWnd(AppName, WIN_X, WIN_Y, WIN_WIDTH, WIN_HEIGHT, hInstance);
    myOglWnd.InitAndShowWindow();
    myOglWnd.RunGameLoop();
    return 0;
}