#include "../../../../99_AuxStuff/MyLibs/Utility/Util.hpp"
#include <gl\GL.h>
#include <gl\GLU.h>
#include <tuple>

constexpr UINT WIN_WIDTH = 800;
constexpr UINT WIN_HEIGHT = 600;
constexpr UINT WIN_X = 100;
constexpr UINT WIN_Y = 100;


class MyOglWindow : public util::OglWindow
{
    std::tuple<float, float, float> m_color = std::make_tuple(1.0f, 1.0f, 1.0f);//RGB
    std::tuple<float, float, float> m_triangleTranslate = std::make_tuple(0.0f, 0.0f, 0.0f), m_rectangleTranslate = std::make_tuple(0.0f, 0.0f, 0.0f);

    enum class shape {
        TRIANGLE,
        RECTANGLE
    } m_shapeSelected;
    // Inherited via OglWindow
    const char * GetMessageMap() override
    {
        return nullptr;
    }

    void Display() override
    {


        glClear(GL_COLOR_BUFFER_BIT);

        float r, g, b;
        std::tie(r, g, b) = m_color;

        float x, y, z;

        //rectangle
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        std::tie(x, y, z) = m_rectangleTranslate;
        glTranslatef(x, y, z);

        glBegin(GL_LINE_LOOP);
        glColor3f(r, g, b);
        glVertex3f( 0.8f,  0.8f, -2.0f);
        glVertex3f(-0.8f,  0.8f, -2.0f);
        glVertex3f(-0.8f, -0.8f, -2.0f);
        glVertex3f( 0.8f, -0.8f, -2.0f);
        glEnd();

        //triangle
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        std::tie(x, y, z) = m_triangleTranslate;
        glTranslatef(x, y, z);

        glBegin(GL_LINE_LOOP);
        glColor3f(r, g, b);
        glVertex3f( 0.0f,  0.8f, -2.0f);
        glVertex3f(-0.8f, -0.8f, -2.0f);
        glVertex3f( 0.8f, -0.8f, -2.0f);
        glEnd();
        SwapBuffers(m_Hdc);
    }

    LRESULT WndProcPost(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
    {
        return LRESULT(0);
    }

    LRESULT WndProcPre(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
    {
        switch (iMsg)
        {
        case WM_KEYDOWN:
        {
            float x, y, z;

            switch (m_shapeSelected)
            {
            case shape::TRIANGLE:
                std::tie(x, y, z) = m_triangleTranslate;
                break;
            case shape::RECTANGLE:
                std::tie(x, y, z) = m_rectangleTranslate;
                break;
            default:
                break;
            }

            switch (wParam)
            {
            case VK_UP:
            {//Up arrow key
                z -= 0.1;
            }
            break;
            case VK_DOWN:
            {//Down key
                z += 0.1;
            }
            break;
            case VK_RIGHT:
            {//Down key
                x += 0.1;
            }
            break;
            case VK_LEFT:
            {//Down key
                x -= 0.1;
            }
            break;
            default:
                break;
            }

            switch (m_shapeSelected)
            {
            case shape::TRIANGLE:
                m_triangleTranslate = std::make_tuple(x, y, z);
                break;
            case shape::RECTANGLE:
                m_rectangleTranslate = std::make_tuple(x, y, z);
                break;
            default:
                break;
            }
        }
        break;
        case WM_CHAR:
        {
            switch (wParam)
            {
            case 'b':
            {
                m_color = std::make_tuple(0.0f, 0.0f, 1.0f);
            }
            break;
            case 'w':
            {
                m_color = std::make_tuple(1.0f, 1.0f, 1.0f);
            }
            break;
            case 't':
            {
                SetWindowText(hwnd, TEXT("Triangle selected"));
                m_shapeSelected = shape::TRIANGLE;
            }
            break;
            case 'r':
            {
                SetWindowText(hwnd, TEXT("Rectangle selected"));
                m_shapeSelected = shape::RECTANGLE;
            }
            break;
            default:
                break;
            }
        }
        break;
        default:
            break;
        }
        return LRESULT();
    }

    void Resize(int width, int height) override
    {
        if (height == 0)
            height = 1;

        glViewport(0, 0, width, height);

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        gluPerspective(45, ((GLfloat)width) / height, 0.0, 100.0f);
    }
public:
    MyOglWindow(TCHAR * className, UINT x, UINT y, UINT width, UINT height, HINSTANCE hInstance) : OglWindow(className, x, y, width, height, hInstance)
    {
        m_shapeSelected = shape::TRIANGLE;
    }

    // Inherited via OglWindow
    virtual void Update() override
    {
    }
};

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
    TCHAR AppName[] = TEXT("Line_RectangleTriangle");
    MessageBoxA(NULL, " t - triangle \n r - rectangle \n b - blue \n w - white \n Up - zoom out \n Down - zoom in", "Info", MB_OK);
    MyOglWindow myOglWnd(AppName, WIN_X, WIN_Y, WIN_WIDTH, WIN_HEIGHT, hInstance);
    myOglWnd.InitAndShowWindow();
    myOglWnd.RunGameLoop();
    return 0;
}