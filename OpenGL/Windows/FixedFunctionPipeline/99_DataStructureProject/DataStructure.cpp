﻿#include "../../../../99_AuxStuff/MyLibs/Utility/Util.hpp"
#include "../../../../99_AuxStuff/MyLibs/Utility/Animation.hpp"
#include "../../../../99_AuxStuff/MyLibs/Utility/Airplane.h"
#include "../../../../99_AuxStuff/MyLibs/Utility/EmptyObject.h"
#include "../../../../99_AuxStuff/MyLibs/Utility/AirplaneExhaustParticle.h"
#include <GL\GL.h>
#include <GL\GLU.h>
#include <cmath>
#include <tuple>
#include <string>
#include <vector>
#include <atomic>
#include "../../../../99_AuxStuff/MyLibs/Utility/faces.h"
#include "resource.h"
#include "../../../../99_AuxStuff/MyLibs/Utility/Textures.h"
#include "../../../../99_AuxStuff/MyLibs/Utility/TextureSceneObject.h"
#include "../../../../99_AuxStuff/MyLibs/Utility/FreetypeWrapper.h"


//STRICT to prevent explicit typecasting
#define STRICT

#pragma region animation config
constexpr UINT WIN_WIDTH = 1920;
constexpr UINT WIN_HEIGHT = 1080;
constexpr UINT WIN_X = 100;
constexpr UINT WIN_Y = 100;
constexpr double CHAR_SPACING = 0.2;
constexpr double WORD_SPACING = 0;
//constexpr double ANIMATION_START_T = 5; //debug-fast
constexpr double ANIMATION_START_T = 10;//IMPORTANT - If init - loading textures, etc. takes more time, then keep this at a value sufficiently large. Time starts incrementing even before init starts, in fact that is necessary for init since it is also sequenced in timeline, thus when first object is set to appear, the current should not be time more than the time it is exected to be shown. This is never a concern when all init is done in main, since time only increments in display(), which gets called later. Time doesn't increment during init though since it is called during execution of draw
constexpr double ANIMATION_DURATION = 200;
constexpr double ANIMATION_STEP_SIZE = 0.025;
//constexpr double ANIMATION_STEP_SIZE = 0.190; //debug-fast
#pragma endregion

#pragma region kate and mum config
constexpr double KATE_FACE_CELL_ASPECT_RATION = 207.0 / 162.0;
constexpr double MUM_FACE_CELL_ASPECT_RATION = 132.0 / 164.0;
//constexpr double BUBBLE_ASPECT_RATIO = 426.0 / 285.0;//width/height /*YELLOW SPEECH BUBBLE*/
constexpr double KATE_BUBBLE_ASPECT_RATIO = 796.0 / 455.0;//width/height *NEW*
constexpr double MOM_BUBBLE_ASPECT_RATIO = 796.0 / 455.0;//width/height *NEW*
constexpr double BUBBLE_HEIGHT = 0.8;//width/height
constexpr double KATE_FACE_HEIGTH = 0.6;
constexpr double MUM_FACE_HEIGTH = 0.8;
util::DataTypes::POINT3Dd MUM_SPAWN_POSITION = { 1.75,-0.25,0 };
util::DataTypes::POINT3Dd KATE_SPAWN_POSITION = { -MUM_SPAWN_POSITION.x,MUM_SPAWN_POSITION.y - (MUM_FACE_HEIGTH - KATE_FACE_HEIGTH),0 };//adjust Y so that their bottoms are in line
constexpr util::DataTypes::POINT2Dd BUBBLE_RELATIVE_POS_KATE = { 0.6*KATE_FACE_HEIGTH*KATE_FACE_CELL_ASPECT_RATION,-0.3*KATE_FACE_HEIGTH }; //Position of bubble relative to Kate's face
constexpr util::DataTypes::POINT2Dd BUBBLE_RELATIVE_POS_MOM = { -1.8 * MUM_FACE_HEIGTH* MUM_FACE_CELL_ASPECT_RATION , -0.3*MUM_FACE_HEIGTH };
constexpr util::DataTypes::POINT2Dd KATE_RELATIVE_TEXT_DISPLACEMENT = { 0.1, -0.2 };
constexpr util::DataTypes::POINT2Dd MUM_RELATIVE_TEXT_DISPLACEMENT = { 0.15, -0.22 };
double SPEECH_BUBBLE_TEXT_HEIGHT = 0.068;
constexpr double INTER_SPEECH_BUBBLE_TIME = 3.5;//Time elapsed between each speech bubble - when kate and mum are talking
COLORREF BACKGROUNDCOLOR = RGB(83, 137, 121);
COLORREF CREDITS = RGB(0, 0, 136);
//COLORREF KATE_TEXT_COLOR = RGB(255, 255, 255);
COLORREF KATE_TEXT_COLOR = RGB(0, 0, 0);
//COLORREF MOM_TEXT_COLOR = RGB(255, 255, 255);
COLORREF MOM_TEXT_COLOR = RGB(0, 0, 0);
UINT KATE_NUM_SPEECH_BUBBLES = 18;
UINT MOM_NUM_SPEECH_BUBBLES = 23;
#pragma endregion

#pragma region island and kids config
double ENABLE_ADJUSTEMENTS = 1.0;//set it to 0.0 to disable all
double SHIF_FACTOR = 0.3*ENABLE_ADJUSTEMENTS;
//double ISLAND_HEIGHT = 0.9037;
util::DataTypes::POINT3Dd ISLAND_POS = { -0.75,1.2,0 };
double ISLAND_HEIGHT = 0.9037;// +0.9037*SHIF_FACTOR;
//double ISLAND_KIDS_HEIGHT1 = 0.3;
double ISLAND_KIDS_HEIGHT1 = 0.3 + 0.3*SHIF_FACTOR;
double ISLAND_KIDS_HEIGHT2 = 0.42 + 0.42*SHIF_FACTOR;
double ISLAND_KIDS_HEIGHT3 = 0.5936 + 0.5936*SHIF_FACTOR;
double ISLAND_KIDS_HEIGHT4 = 0.5472 + 0.5472*SHIF_FACTOR;
double KIDS_BUST_HEIGHT = 0.3;
util::DataTypes::POINT3Dd KIDS_LINE_POS = { -0.55 - 0.55*SHIF_FACTOR,0.77 + 0.57*SHIF_FACTOR / 2.0,0 };

//util::DataTypes::POINT3Dd JOHN_TEXT_POS = { -0.20,0.85,0 };
util::DataTypes::POINT3Dd JOHN_TEXT_POS = { -0.20 - 0.20*SHIF_FACTOR/2.0,0.85 + 0.85*SHIF_FACTOR / 4.0,0 };
double JOHN_EXCLAMATIONS_TEXT_SCALE = 1.0;
double JOHN_TEXT_TROUBLE_HEIGHT = 0.2*JOHN_EXCLAMATIONS_TEXT_SCALE;
double JOHN_TEXT_WANTOUT_HEIGHT = 0.1*JOHN_EXCLAMATIONS_TEXT_SCALE;
double JOHN_TEXT_SNAKE_HEIGHT = 0.15*JOHN_EXCLAMATIONS_TEXT_SCALE;
util::DataTypes::POINT3Dd JOHN_TEXT_WANTOUT_POS = JOHN_TEXT_POS;
util::DataTypes::POINT3Dd JOHN_TEXT_TROUBLE_POS = { JOHN_TEXT_POS.x + 0.025*ENABLE_ADJUSTEMENTS, JOHN_TEXT_POS.y, JOHN_TEXT_POS.z };

util::DataTypes::POINT3Dd JOHN_TEXT_SNAKE_POS = { JOHN_TEXT_POS.x + 0.015*ENABLE_ADJUSTEMENTS, JOHN_TEXT_POS.y, JOHN_TEXT_POS.z };
#pragma endregion

//frames in which KATE speaks
const auto KATE_TIME_FRAMES = {1,3,6,7,8,9,10,13,17,19,20,23,25,31,33,38,40,42};
const auto MOM_TIME_FRAMES = {2,4,5,11,14,16,18,21,22,24,27,28,29,30,32,34,35,36,37,39,41,43,44};

//for synchronization 
std::atomic<bool> bShowSplashScreen = true;//will be set to false once texture loading is complete

//Image IDs
constexpr UINT ID_ISLAND = 101;
constexpr UINT ID_LEAF = 102;
//Kids
//Ted
constexpr UINT ID_KIDS_TED_NORMAL = 111;
constexpr UINT ID_KIDS_TED_L_HAND = 112;
constexpr UINT ID_KIDS_TED_BUST   = 113;

//Bet
constexpr UINT ID_KIDS_BET_NORMAL = 121;
constexpr UINT ID_KIDS_BET_R_HAND = 122;
constexpr UINT ID_KIDS_BET_RL_HAND = 123;
constexpr UINT ID_KIDS_BET_BUST    = 124;

//John
constexpr UINT ID_KIDS_JOHN_NORMAL = 131;
constexpr UINT ID_KIDS_JOHN_R_HAND = 132;
constexpr UINT ID_KIDS_JOHN_RL_HAND = 133;
constexpr UINT ID_KIDS_JOHN_TEXT_1 = 134;
constexpr UINT ID_KIDS_JOHN_TEXT_2 = 135;
constexpr UINT ID_KIDS_JOHN_TEXT_3 = 136;
constexpr UINT ID_KIDS_JOHN_BUST   = 137;

//Dal
constexpr UINT ID_KIDS_DAL_NORMAL = 141;
constexpr UINT ID_KIDS_DAL_R_HAND = 142;
constexpr UINT ID_KIDS_DAL_RL_HAND = 143;
constexpr UINT ID_KIDS_DAL_BUST    = 144;

//Gale
constexpr UINT ID_KIDS_GALE_NORMAL = 151;
constexpr UINT ID_KIDS_GALE_R_HAND = 152;
constexpr UINT ID_KIDS_GALE_BUST   = 153;

#pragma region Aux functions
util::KeyFrame::CALLBACKFUNC callBackNextSpeechBubble = [](std::shared_ptr<util::SceneObject> obj, util::AnimationObjectState state) {
    auto person = std::dynamic_pointer_cast<util::faces2D>(obj);
    if (person)//This function would work for both kate, and mom
    {
        person->IncrementSpeechBubbleIndex();
        person->SetCurrentExpression(person->GetCurrentSpeechBuble().GetFaceExpression());
    }
};

util::KeyFrame::CALLBACKFUNC callBackEnableSpeechBubble = [](std::shared_ptr<util::SceneObject> obj, util::AnimationObjectState state) {
    auto person = std::dynamic_pointer_cast<util::faces2D>(obj);
    if (person)//This function would work for both kate, and mom
    {
        person->SetCurrentExpression(person->GetCurrentSpeechBuble().GetFaceExpression());
        person->EnableSpeechBubble(true);
    }
};

util::KeyFrame::CALLBACKFUNC callBackDisableSpeechBubble = [](std::shared_ptr<util::SceneObject> obj, util::AnimationObjectState state) {
    auto person = std::dynamic_pointer_cast<util::faces2D>(obj);
    if (person)//This function would work for both kate, and mom
    {
        person->EnableSpeechBubble(false);
    }
};

util::KeyFrame::CALLBACKFUNC callBackSetClearColorWhite = [](std::shared_ptr<util::SceneObject> obj, util::AnimationObjectState state) {
    if (nullptr != state.m_windowObject)
    {
        state.m_windowObject->SetClearColor(RGBA(255, 255, 255, 255));
    }
};

util::KeyFrame::CALLBACKFUNC callBackSetClearColorGreen = [](std::shared_ptr<util::SceneObject> obj, util::AnimationObjectState state) {
    if (nullptr != state.m_windowObject)
    {
        state.m_windowObject->SetClearColor(BACKGROUNDCOLOR);
    }
};

util::KeyFrame::CALLBACKFUNC callBackSetClearColorForCredits = [](std::shared_ptr<util::SceneObject> obj, util::AnimationObjectState state) {
    if (nullptr != state.m_windowObject)
    {
        state.m_windowObject->SetClearColor(CREDITS);
    }
};
#pragma endregion

std::shared_ptr<util::Timeline> tl;

class MyOglWindow : public util::OglWindow
{
    const double PI;
    // Inherited via OglWindow
    virtual const char * GetMessageMap() override
    {
        return nullptr;
    }

    virtual void Display() override
    {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        glTranslatef(0, 0, -3);
        glEnable(GL_TEXTURE_2D);
        {
            if (bShowSplashScreen)
            {
                static auto LogoKatieAndMom = std::shared_ptr<util::TextureSceneObject>(new util::TextureSceneObject(ID_LEAF, R"(res/katieMomLogo_flip.png)", util::Textures::TextureType::PNG));
                LogoKatieAndMom->SetCellHeight(this->GetWindowHeightNDC());
                util::DataTypes::POINT2Dd pos = { -LogoKatieAndMom->GetCurrentImageCellWidth() / 2.0, this->GetWindowHeightNDC() / 2.0 };
                glTranslatef(pos.x, pos.y, 0);
                util::AnimationObjectState s;
                LogoKatieAndMom->Draw(s);
            }
        }
        tl->advance();
        SwapBuffers(m_Hdc);
    }

    virtual void Resize(int width, int height) override
    {
        if (height == 0)
            height = 1;

        glViewport(0, 0, width, height);

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        gluPerspective(45, ((GLfloat)width) / height, 0.1, 100.0f);
    }

    virtual LRESULT WndProcPre(HWND, UINT iMsg, WPARAM wParam, LPARAM lParam) override
    {
        switch (iMsg)
        {
        case WM_CHAR:
        {
            switch (wParam)
            {
            case 'f':
                //[[fallthrough]]
            case 'F':
                return LRESULT(0);//return -1 to disable 'f' press action
                break;
            default:
                break;
            }
        }
        break;
        }
        return LRESULT();
    }

    virtual LRESULT WndProcPost(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) override
    {
        auto that = (OglWindow*)GetProp(hwnd, PARENT_OBJECT);

        switch (iMsg)
        {
        case WM_KEYDOWN:
        {
            if (VK_ESCAPE == wParam)
            {
                DestroyWindow(hwnd);
            }
        }
        break;
        }
        return LRESULT();
    }

public:
    MyOglWindow(TCHAR * className, UINT x, UINT y, UINT width, UINT height, HINSTANCE hInstance) : PI(4 * atan(1)), OglWindow(className, x, y, width, height, hInstance)
    {
    }

    // Inherited via OglWindow
    virtual void Update() override
    {
    }
};

#pragma region more aux functions
void InitAnimation(MyOglWindow* myOglWnd);

util::KeyFrame::CALLBACKFUNC callBackInitAnimation = [](std::shared_ptr<util::SceneObject> obj, util::AnimationObjectState state) {
    if (nullptr != state.m_windowObject)
    {
        auto window = dynamic_cast<MyOglWindow*>(state.m_windowObject);
        if (window)
        {
            InitAnimation(window);
        }
    }
};

util::KeyFrame::CALLBACKFUNC callBackHideSplashScreen = [](std::shared_ptr<util::SceneObject> obj, util::AnimationObjectState state) {
    bShowSplashScreen = false;
};
#pragma endregion

int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
    TCHAR AppName[] = TEXT("Data Structure - Linked Lists");
    MyOglWindow myOglWnd(AppName, WIN_X, WIN_Y, WIN_WIDTH, WIN_HEIGHT, hInstance);
    myOglWnd.InitAndShowWindow(INIT_MASK_ENABLE_DEPTH | INIT_MASK_ENABLE_TEXTURE_2D);

    //glClearColor();//#FFF9A7
    glClearColor(GetRValuef(BACKGROUNDCOLOR), GetGValuef(BACKGROUNDCOLOR), GetBValuef(BACKGROUNDCOLOR), 1);//#FFF9A7
    //glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    myOglWnd.EnableAlpha(true);
    myOglWnd.ToggleFullscreeen();
    myOglWnd.AllowOutOfFocusRender(true);
    myOglWnd.SetCapFrameRate(33, true);

    tl.reset(new util::Timeline(ANIMATION_DURATION, ANIMATION_STEP_SIZE));

    //init
    auto initF = std::shared_ptr<util::EmptyObject>(new util::EmptyObject());
    util::AnimationObject ao(tl, initF, {}, &myOglWnd);
    ao.AddKeyFrame(util::KeyFrame(1, util::KeyFrame::ACTION::EXECUTE_ARBITRARY_CALLBACK, 0.1, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, { 0,0,0 }, 0, callBackInitAnimation));
    //Hide splash screen just before showing 1st object
    ao.AddKeyFrame(util::KeyFrame(1, util::KeyFrame::ACTION::EXECUTE_ARBITRARY_CALLBACK, ANIMATION_START_T - 0.1, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, { 0,0,0 }, 0, callBackHideSplashScreen));
    tl->AddObject(ao);

    myOglWnd.RunGameLoop();
    return 0;
}

void InitAnimation(MyOglWindow* myOglWnd)
{
#pragma region local vars
    double JohnWanOutTextTime = ANIMATION_START_T + 12 * INTER_SPEECH_BUBBLE_TIME;
    double JohnTroubleTextTime = ANIMATION_START_T + 15 * INTER_SPEECH_BUBBLE_TIME;
    double JohnSnakeTextTime = ANIMATION_START_T + 26 * INTER_SPEECH_BUBBLE_TIME;
    double Line3Time = ANIMATION_START_T + 31 * INTER_SPEECH_BUBBLE_TIME;
    double Line4Time = ANIMATION_START_T + 32 * INTER_SPEECH_BUBBLE_TIME;
    double JohnInsertTime = ANIMATION_START_T + 34 * INTER_SPEECH_BUBBLE_TIME;
    double JohnInsertTime2 = ANIMATION_START_T + 36 * INTER_SPEECH_BUBBLE_TIME;
    double JohnBackTime = ANIMATION_START_T + 37 * INTER_SPEECH_BUBBLE_TIME;
    double ShowLeafTime = ANIMATION_START_T + 20 * INTER_SPEECH_BUBBLE_TIME;
    double HideLeafTime = ANIMATION_START_T + 22 * INTER_SPEECH_BUBBLE_TIME;
    double CreditsTime = ANIMATION_START_T + (*(MOM_TIME_FRAMES.end() - 1))*INTER_SPEECH_BUBBLE_TIME + INTER_SPEECH_BUBBLE_TIME;
#pragma endregion

#pragma region KATE FACE
    auto kate = std::shared_ptr<util::faces2D>(new util::faces2D());
    //expression
    kate->AddExpression(util::faces2D::FaceExpressions::HAPPY_NEUTRAL, R"(res/faces/kate_happy_neutral_flip.png)", util::Textures::TextureType::PNG);
    kate->AddExpression(util::faces2D::FaceExpressions::HAPPY, R"(res/faces/kate_happy_flip.png)", util::Textures::TextureType::PNG);
    kate->AddExpression(util::faces2D::FaceExpressions::INQUISITIVE, R"(res/faces/kate_inquisitive_flip.png)", util::Textures::TextureType::PNG);
    kate->AddExpression(util::faces2D::FaceExpressions::ANNOYED, R"(res/faces/kate_annoyed_flip.png)", util::Textures::TextureType::PNG);
    kate->AddExpression(util::faces2D::FaceExpressions::NEUTRAL_EYESCLOSED, R"(res/faces/kate_neutral_eyesclosed_flip.png)", util::Textures::TextureType::PNG);
    kate->SetCurrentExpression(util::faces2D::FaceExpressions::HAPPY_NEUTRAL);

#pragma region speech bubbles kate
#pragma region //bubble1 - HAPPY
    {
        util::faces2D::SpeechBubble bbl(BUBBLE_RELATIVE_POS_KATE, { BUBBLE_HEIGHT*KATE_BUBBLE_ASPECT_RATIO,BUBBLE_HEIGHT }, R"(res/SpeechBubbleLeft_flip.png)",
            util::Textures::TextureType::PNG, util::faces2D::SpeechBubble::SpeechBubleState::SHOWING, "Hi Mum!");
        bbl.SetFontColor(KATE_TEXT_COLOR);
        bbl.SetFontHeightNDC(SPEECH_BUBBLE_TEXT_HEIGHT);
        bbl.SetRelativeFontDisplacement(KATE_RELATIVE_TEXT_DISPLACEMENT);
        bbl.SetFaceExpression(util::faces2D::FaceExpressions::HAPPY);
        kate->AddSpeechBubble(bbl);
    }
#pragma endregion 

#pragma region //bubble2 - HAPPY_NEUTRAL
    {
        util::faces2D::SpeechBubble bbl(BUBBLE_RELATIVE_POS_KATE, { BUBBLE_HEIGHT*KATE_BUBBLE_ASPECT_RATIO,BUBBLE_HEIGHT }, R"(res/SpeechBubbleLeft_flip.png)",
            util::Textures::TextureType::PNG, util::faces2D::SpeechBubble::SpeechBubleState::SHOWING, "What is a linked list?");
        bbl.SetFontColor(KATE_TEXT_COLOR);
        bbl.SetFontHeightNDC(SPEECH_BUBBLE_TEXT_HEIGHT);
        bbl.SetRelativeFontDisplacement(KATE_RELATIVE_TEXT_DISPLACEMENT);
        bbl.SetFaceExpression(util::faces2D::FaceExpressions::HAPPY_NEUTRAL);
        kate->AddSpeechBubble(bbl);
    }
#pragma endregion 

#pragma region //bubble3 - NEUTRAL_EYESCLOSED - SPLIT INTO 5 - "So if Ted holds hands with bet, and \nbet holds hands with John, \nJohn holds hands with Dal, \nand dal holds hands with gale; \nit will be a linked list?"
    {
        util::faces2D::SpeechBubble bbl(BUBBLE_RELATIVE_POS_KATE, { BUBBLE_HEIGHT*KATE_BUBBLE_ASPECT_RATIO,BUBBLE_HEIGHT }, R"(res/SpeechBubbleLeft_flip.png)",
            util::Textures::TextureType::PNG, util::faces2D::SpeechBubble::SpeechBubleState::SHOWING, "So if Ted holds hands \nwith bet, and ...");
        bbl.SetFontColor(KATE_TEXT_COLOR);
        bbl.SetFontHeightNDC(SPEECH_BUBBLE_TEXT_HEIGHT);
        bbl.SetRelativeFontDisplacement(KATE_RELATIVE_TEXT_DISPLACEMENT);
        bbl.SetFaceExpression(util::faces2D::FaceExpressions::NEUTRAL_EYESCLOSED);
        kate->AddSpeechBubble(bbl);
        bbl.SetText("... Bet holds hands \nwith John, and ..."); kate->AddSpeechBubble(bbl);
        bbl.SetText("... John holds hands \nwith Dal, and ..."); kate->AddSpeechBubble(bbl);
        bbl.SetText("... Dal holds hands \nwith Gale ..."); kate->AddSpeechBubble(bbl);
        bbl.SetText("... it will be a \nLinked List?"); kate->AddSpeechBubble(bbl);
    }
#pragma endregion 

#pragma region //bubble4 - HAPPY_NEUTRAL
    {
        util::faces2D::SpeechBubble bbl(BUBBLE_RELATIVE_POS_KATE, { BUBBLE_HEIGHT*KATE_BUBBLE_ASPECT_RATIO,BUBBLE_HEIGHT }, R"(res/SpeechBubbleLeft_flip.png)",
            util::Textures::TextureType::PNG, util::faces2D::SpeechBubble::SpeechBubleState::SHOWING, "Okay John, you can go.");
        bbl.SetFontColor(KATE_TEXT_COLOR);
        bbl.SetFontHeightNDC(SPEECH_BUBBLE_TEXT_HEIGHT);
        bbl.SetRelativeFontDisplacement(KATE_RELATIVE_TEXT_DISPLACEMENT);
        bbl.SetFaceExpression(util::faces2D::FaceExpressions::HAPPY_NEUTRAL);
        kate->AddSpeechBubble(bbl);
    }
#pragma endregion 

#pragma region //bubble5 - NEUTRAL_EYESCLOSED
    {
        util::faces2D::SpeechBubble bbl(BUBBLE_RELATIVE_POS_KATE, { BUBBLE_HEIGHT*KATE_BUBBLE_ASPECT_RATIO,BUBBLE_HEIGHT }, R"(res/SpeechBubbleLeft_flip.png)",
            util::Textures::TextureType::PNG, util::faces2D::SpeechBubble::SpeechBubleState::SHOWING, "What mum?");
        bbl.SetFontColor(KATE_TEXT_COLOR);
        bbl.SetFontHeightNDC(SPEECH_BUBBLE_TEXT_HEIGHT);
        bbl.SetRelativeFontDisplacement(KATE_RELATIVE_TEXT_DISPLACEMENT);
        bbl.SetFaceExpression(util::faces2D::FaceExpressions::NEUTRAL_EYESCLOSED);
        kate->AddSpeechBubble(bbl);
    }
#pragma endregion 

#pragma region //bubble6 - HAPPY_NEUTRAL //TODO: Check grammar
    {
        util::faces2D::SpeechBubble bbl(BUBBLE_RELATIVE_POS_KATE, { BUBBLE_HEIGHT*KATE_BUBBLE_ASPECT_RATIO,BUBBLE_HEIGHT }, R"(res/SpeechBubbleLeft_flip.png)",
            util::Textures::TextureType::PNG, util::faces2D::SpeechBubble::SpeechBubleState::SHOWING, "Like in a stack, \nyou can add or remove \nanything only at the \ntop?");
        bbl.SetFontColor(KATE_TEXT_COLOR);
        bbl.SetFontHeightNDC(SPEECH_BUBBLE_TEXT_HEIGHT);
        bbl.SetRelativeFontDisplacement(KATE_RELATIVE_TEXT_DISPLACEMENT);
        bbl.SetFaceExpression(util::faces2D::FaceExpressions::HAPPY_NEUTRAL);
        kate->AddSpeechBubble(bbl);
    }
#pragma endregion 

#pragma region //bubble7 - NEUTRAL_EYESCLOSED
    {
        util::faces2D::SpeechBubble bbl(BUBBLE_RELATIVE_POS_KATE, { BUBBLE_HEIGHT*KATE_BUBBLE_ASPECT_RATIO,BUBBLE_HEIGHT }, R"(res/SpeechBubbleLeft_flip.png)",
            util::Textures::TextureType::PNG, util::faces2D::SpeechBubble::SpeechBubleState::SHOWING, "What'd you say?\nA [COLORREF:RGBA(0,155,0,255)]\'Leaf-O\'[COLORREF:RGBA(255,255,255,255)]?");
        bbl.SetFontColor(KATE_TEXT_COLOR);
        bbl.SetFontHeightNDC(SPEECH_BUBBLE_TEXT_HEIGHT);
        bbl.SetRelativeFontDisplacement(KATE_RELATIVE_TEXT_DISPLACEMENT);
        bbl.SetFaceExpression(util::faces2D::FaceExpressions::NEUTRAL_EYESCLOSED);
        kate->AddSpeechBubble(bbl);
    }
#pragma endregion 

#pragma region //bubble8 - HAPPY_NEUTRAL
    {
        util::faces2D::SpeechBubble bbl(BUBBLE_RELATIVE_POS_KATE, { BUBBLE_HEIGHT*KATE_BUBBLE_ASPECT_RATIO,BUBBLE_HEIGHT }, R"(res/SpeechBubbleLeft_flip.png)",
            util::Textures::TextureType::PNG, util::faces2D::SpeechBubble::SpeechBubleState::SHOWING, "So [COLORREF:RGBA(255,0,0,255)]Ted [COLORREF:RGBA(255,255,255,255)]is our [COLORREF:RGBA(255,0,0,255)]Head[COLORREF:RGBA(255,255,255,255)], and \n[COLORREF:RGBA(0,0,255,255)]Gail[COLORREF:RGBA(255,255,255,255)] our [COLORREF:RGBA(0,0,255,255)]Tail[COLORREF:RGBA(255,255,255,255)]?");
        bbl.SetFontColor(KATE_TEXT_COLOR);
        bbl.SetFontHeightNDC(SPEECH_BUBBLE_TEXT_HEIGHT);
        bbl.SetRelativeFontDisplacement(KATE_RELATIVE_TEXT_DISPLACEMENT);
        bbl.SetFaceExpression(util::faces2D::FaceExpressions::HAPPY_NEUTRAL);
        kate->AddSpeechBubble(bbl);
    }
#pragma endregion 

#pragma region //bubble9 - HAPPY_NEUTRAL
    {
        util::faces2D::SpeechBubble bbl(BUBBLE_RELATIVE_POS_KATE, { BUBBLE_HEIGHT*KATE_BUBBLE_ASPECT_RATIO,BUBBLE_HEIGHT }, R"(res/SpeechBubbleLeft_flip.png)",
            util::Textures::TextureType::PNG, util::faces2D::SpeechBubble::SpeechBubleState::SHOWING, "Like a snake?");
        bbl.SetFontColor(KATE_TEXT_COLOR);
        bbl.SetFontHeightNDC(SPEECH_BUBBLE_TEXT_HEIGHT);
        bbl.SetRelativeFontDisplacement(KATE_RELATIVE_TEXT_DISPLACEMENT);
        bbl.SetFaceExpression(util::faces2D::FaceExpressions::HAPPY_NEUTRAL);
        kate->AddSpeechBubble(bbl);
    }
#pragma endregion 

#pragma region //bubble10 - NEUTRAL_EYESCLOSED for thoughtful
    {
        util::faces2D::SpeechBubble bbl(BUBBLE_RELATIVE_POS_KATE, { BUBBLE_HEIGHT*KATE_BUBBLE_ASPECT_RATIO,BUBBLE_HEIGHT }, R"(res/SpeechBubbleLeft_flip.png)",
            util::Textures::TextureType::PNG, util::faces2D::SpeechBubble::SpeechBubleState::SHOWING, "So, to remove John, \nI'll have to start with \nTed, then go to Bet, \nand from Bet to John.");
        bbl.SetFontColor(KATE_TEXT_COLOR);
        bbl.SetFontHeightNDC(SPEECH_BUBBLE_TEXT_HEIGHT);
        bbl.SetRelativeFontDisplacement(KATE_RELATIVE_TEXT_DISPLACEMENT);
        bbl.SetFaceExpression(util::faces2D::FaceExpressions::NEUTRAL_EYESCLOSED);
        kate->AddSpeechBubble(bbl);
    }
#pragma endregion 

#pragma region //bubble11 - INQUISITIVE
    {
        util::faces2D::SpeechBubble bbl(BUBBLE_RELATIVE_POS_KATE, { BUBBLE_HEIGHT*KATE_BUBBLE_ASPECT_RATIO,BUBBLE_HEIGHT }, R"(res/SpeechBubbleLeft_flip.png)",
            util::Textures::TextureType::PNG, util::faces2D::SpeechBubble::SpeechBubleState::SHOWING, "What if John wants to \ncome back?");
        bbl.SetFontColor(KATE_TEXT_COLOR);
        bbl.SetFontHeightNDC(SPEECH_BUBBLE_TEXT_HEIGHT);
        bbl.SetRelativeFontDisplacement(KATE_RELATIVE_TEXT_DISPLACEMENT);
        bbl.SetFaceExpression(util::faces2D::FaceExpressions::INQUISITIVE);
        kate->AddSpeechBubble(bbl);
    }
#pragma endregion 

#pragma region //bubble12 - ANNOYED
    {
        util::faces2D::SpeechBubble bbl(BUBBLE_RELATIVE_POS_KATE, { BUBBLE_HEIGHT*KATE_BUBBLE_ASPECT_RATIO,BUBBLE_HEIGHT }, R"(res/SpeechBubbleLeft_flip.png)",
            util::Textures::TextureType::PNG, util::faces2D::SpeechBubble::SpeechBubleState::SHOWING, "That's one crooked \nsnake.");
        bbl.SetFontColor(KATE_TEXT_COLOR);
        bbl.SetFontHeightNDC(SPEECH_BUBBLE_TEXT_HEIGHT);
        bbl.SetRelativeFontDisplacement(KATE_RELATIVE_TEXT_DISPLACEMENT);
        bbl.SetFaceExpression(util::faces2D::FaceExpressions::ANNOYED);
        kate->AddSpeechBubble(bbl);
    }
#pragma endregion 

#pragma region //bubble13 - HAPPY_NEUTRAL
    {
        util::faces2D::SpeechBubble bbl(BUBBLE_RELATIVE_POS_KATE, { BUBBLE_HEIGHT*KATE_BUBBLE_ASPECT_RATIO,BUBBLE_HEIGHT }, R"(res/SpeechBubbleLeft_flip.png)",
            util::Textures::TextureType::PNG, util::faces2D::SpeechBubble::SpeechBubleState::SHOWING, "But then...\nWhat is a doubly \nlinked list?");
        bbl.SetFontColor(KATE_TEXT_COLOR);
        bbl.SetFontHeightNDC(SPEECH_BUBBLE_TEXT_HEIGHT);
        bbl.SetRelativeFontDisplacement(KATE_RELATIVE_TEXT_DISPLACEMENT);
        bbl.SetFaceExpression(util::faces2D::FaceExpressions::HAPPY_NEUTRAL);
        kate->AddSpeechBubble(bbl);
    }
#pragma endregion 

#pragma region //bubble14 - NEUTRAL_EYESCLOSED
    {
        util::faces2D::SpeechBubble bbl(BUBBLE_RELATIVE_POS_KATE, { BUBBLE_HEIGHT*KATE_BUBBLE_ASPECT_RATIO,BUBBLE_HEIGHT }, R"(res/SpeechBubbleLeft_flip.png)",
            util::Textures::TextureType::PNG, util::faces2D::SpeechBubble::SpeechBubleState::SHOWING, "Alright!\nGoodbye, Mum.");
        bbl.SetFontColor(KATE_TEXT_COLOR);
        bbl.SetFontHeightNDC(SPEECH_BUBBLE_TEXT_HEIGHT);
        bbl.SetRelativeFontDisplacement(KATE_RELATIVE_TEXT_DISPLACEMENT);
        bbl.SetFaceExpression(util::faces2D::FaceExpressions::NEUTRAL_EYESCLOSED);
        kate->AddSpeechBubble(bbl);
    }
#pragma endregion 

#pragma endregion
    kate->EnableSpeechBubble(false);
    //face coordinates
    kate->SetCellAspectRatio(KATE_FACE_CELL_ASPECT_RATION);
    kate->SetCellHeight(KATE_FACE_HEIGTH);
    util::DataTypes::POINT3Dd kateSpawnPos = KATE_SPAWN_POSITION;
    kateSpawnPos.x -= kate->GetCellSize().cx / 2.0;
    {
        util::AnimationObject ao(tl, kate, kateSpawnPos, myOglWnd);
        ao.AddKeyFrame(util::KeyFrame(0, util::KeyFrame::ACTION::OBJECT_SHOW, ANIMATION_START_T, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, kateSpawnPos));
        ao.AddKeyFrame(util::KeyFrame(1, util::KeyFrame::ACTION::EXECUTE_ARBITRARY_CALLBACK, ANIMATION_START_T + (*KATE_TIME_FRAMES.begin())*INTER_SPEECH_BUBBLE_TIME, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, { 0,0,0 }, 0, callBackEnableSpeechBubble));
        const auto& frameList1 = KATE_TIME_FRAMES;
        for (int i = 1; i < frameList1.size(); i++)
        {
            int time = *(frameList1.begin() + i);
            ao.AddKeyFrame(util::KeyFrame(1, util::KeyFrame::ACTION::EXECUTE_ARBITRARY_CALLBACK, ANIMATION_START_T + (time)*INTER_SPEECH_BUBBLE_TIME, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, { 0,0,0 }, 0, callBackEnableSpeechBubble));
            ao.AddKeyFrame(util::KeyFrame(1, util::KeyFrame::ACTION::EXECUTE_ARBITRARY_CALLBACK, ANIMATION_START_T + (time)*INTER_SPEECH_BUBBLE_TIME, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, { 0,0,0 }, 0, callBackNextSpeechBubble));
        }

        const auto& frameList2 = MOM_TIME_FRAMES;
        for (int i = 0; i < frameList2.size(); i++)
        {
            int time = *(frameList2.begin() + i);
            ao.AddKeyFrame(util::KeyFrame(1, util::KeyFrame::ACTION::EXECUTE_ARBITRARY_CALLBACK, ANIMATION_START_T + (time)*INTER_SPEECH_BUBBLE_TIME, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, { 0,0,0 }, 0, callBackDisableSpeechBubble));
        }

        ao.AddKeyFrame(util::KeyFrame(1, util::KeyFrame::ACTION::EXECUTE_ARBITRARY_CALLBACK, JohnSnakeTextTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, { 0,0,0 }, 0, callBackDisableSpeechBubble));
        ao.AddKeyFrame(util::KeyFrame(1, util::KeyFrame::ACTION::EXECUTE_ARBITRARY_CALLBACK, Line4Time, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, { 0,0,0 }, 0, callBackDisableSpeechBubble));
        ao.AddKeyFrame(util::KeyFrame(1, util::KeyFrame::ACTION::OBJECT_HIDE, CreditsTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE));
        tl->AddObject(ao);
    }
#pragma endregion

#pragma region MOM FACE
    auto mom = std::shared_ptr<util::faces2D>(new util::faces2D());
    mom->AddExpression(util::faces2D::FaceExpressions::HAPPY_EYESCLOSED, R"(res\faces\mum_happy_eyes_closed_flip.png)", util::Textures::TextureType::PNG);
    mom->AddExpression(util::faces2D::FaceExpressions::NEUTRAL, R"(res\faces\mum_neutral_flip.png)", util::Textures::TextureType::PNG);
    mom->AddExpression(util::faces2D::FaceExpressions::NEUTRAL_EYESCLOSED, R"(res\faces\mum_neutral_eyes_closed_flip.png)", util::Textures::TextureType::PNG);
    mom->AddExpression(util::faces2D::FaceExpressions::NEUTRAL_HAPPY, R"(res\faces\mum_neutral_happy_flip.png)", util::Textures::TextureType::PNG);
    mom->AddExpression(util::faces2D::FaceExpressions::ANNOYED_SLIGHT, R"(res\faces\mum_annoyed_slight_flip.png)", util::Textures::TextureType::PNG);
    mom->AddExpression(util::faces2D::FaceExpressions::ANGRY, R"(res\faces\mum_angry_flip.png)", util::Textures::TextureType::PNG);
    mom->SetCurrentExpression(util::faces2D::FaceExpressions::NEUTRAL);

#pragma region speech bubbles mom

#pragma region //bubble1 - face : ANNOYED_SLIGHT
    {
        util::faces2D::SpeechBubble bbl(BUBBLE_RELATIVE_POS_MOM, { BUBBLE_HEIGHT * MOM_BUBBLE_ASPECT_RATIO,BUBBLE_HEIGHT }, R"(res/SpeechBubbleRight_flip.png)",
            util::Textures::TextureType::PNG, util::faces2D::SpeechBubble::SpeechBubleState::SHOWING, "Not Again ...");
        bbl.SetFontColor(MOM_TEXT_COLOR);
        bbl.SetFontHeightNDC(SPEECH_BUBBLE_TEXT_HEIGHT);
        bbl.SetRelativeFontDisplacement(MUM_RELATIVE_TEXT_DISPLACEMENT);
        bbl.SetFaceExpression(util::faces2D::FaceExpressions::ANNOYED_SLIGHT);
        mom->AddSpeechBubble(bbl);
    }
#pragma endregion

#pragma region //bubble2 - face : NEUTRAL_EYESCLOSED - this will also work for thoughtful expression
    {
        util::faces2D::SpeechBubble bbl(BUBBLE_RELATIVE_POS_MOM, { BUBBLE_HEIGHT * MOM_BUBBLE_ASPECT_RATIO,BUBBLE_HEIGHT }, R"(res/SpeechBubbleRight_flip.png)",
            util::Textures::TextureType::PNG, util::faces2D::SpeechBubble::SpeechBubleState::SHOWING, "Hmm ... Let's See");
        bbl.SetFontColor(MOM_TEXT_COLOR);
        bbl.SetFontHeightNDC(SPEECH_BUBBLE_TEXT_HEIGHT);
        bbl.SetRelativeFontDisplacement(MUM_RELATIVE_TEXT_DISPLACEMENT);
        bbl.SetFaceExpression(util::faces2D::FaceExpressions::NEUTRAL_EYESCLOSED);
        mom->AddSpeechBubble(bbl);
    }
#pragma endregion

#pragma region //bubble3 - face : NEUTRAL_HAPPY
    {
        util::faces2D::SpeechBubble bbl(BUBBLE_RELATIVE_POS_MOM, { BUBBLE_HEIGHT * MOM_BUBBLE_ASPECT_RATIO,BUBBLE_HEIGHT }, R"(res/SpeechBubbleRight_flip.png)",
            util::Textures::TextureType::PNG, util::faces2D::SpeechBubble::SpeechBubleState::SHOWING, "A linked list is like a \nchain in which one ring \nconnects to another.");
        bbl.SetFontColor(MOM_TEXT_COLOR);
        bbl.SetFontHeightNDC(SPEECH_BUBBLE_TEXT_HEIGHT);
        bbl.SetRelativeFontDisplacement(MUM_RELATIVE_TEXT_DISPLACEMENT);
        bbl.SetFaceExpression(util::faces2D::FaceExpressions::NEUTRAL_HAPPY);
        mom->AddSpeechBubble(bbl);
    }
#pragma endregion

#pragma region //bubble4 - face : HAPPY_EYESCLOSED
    {
        util::faces2D::SpeechBubble bbl(BUBBLE_RELATIVE_POS_MOM, { BUBBLE_HEIGHT * MOM_BUBBLE_ASPECT_RATIO,BUBBLE_HEIGHT }, R"(res/SpeechBubbleRight_flip.png)",
            util::Textures::TextureType::PNG, util::faces2D::SpeechBubble::SpeechBubleState::SHOWING, "Absolutely!");
        bbl.SetFontColor(MOM_TEXT_COLOR);
        bbl.SetFontHeightNDC(SPEECH_BUBBLE_TEXT_HEIGHT);
        bbl.SetRelativeFontDisplacement(MUM_RELATIVE_TEXT_DISPLACEMENT);
        bbl.SetFaceExpression(util::faces2D::FaceExpressions::HAPPY_EYESCLOSED);
        mom->AddSpeechBubble(bbl);
    }
#pragma endregion

#pragma region //bubble5 - face : ANGRY
    {
        util::faces2D::SpeechBubble bbl(BUBBLE_RELATIVE_POS_MOM, { BUBBLE_HEIGHT * MOM_BUBBLE_ASPECT_RATIO,BUBBLE_HEIGHT }, R"(res/SpeechBubbleRight_flip.png)",
            util::Textures::TextureType::PNG, util::faces2D::SpeechBubble::SpeechBubleState::SHOWING, "Hold it John! \nYou are not going \nanywhere.");
        bbl.SetFontColor(MOM_TEXT_COLOR);
        bbl.SetFontHeightNDC(SPEECH_BUBBLE_TEXT_HEIGHT);
        bbl.SetRelativeFontDisplacement(MUM_RELATIVE_TEXT_DISPLACEMENT);
        bbl.SetFaceExpression(util::faces2D::FaceExpressions::ANGRY);
        mom->AddSpeechBubble(bbl);
    }
#pragma endregion

#pragma region //bubble6 - face : NEUTRAL_HAPPY
    {
        util::faces2D::SpeechBubble bbl(BUBBLE_RELATIVE_POS_MOM, { BUBBLE_HEIGHT * MOM_BUBBLE_ASPECT_RATIO,BUBBLE_HEIGHT }, R"(res/SpeechBubbleRight_flip.png)",
            util::Textures::TextureType::PNG, util::faces2D::SpeechBubble::SpeechBubleState::SHOWING, "Katie, Do you remember \nwhat I told you about \ndata structures?");
        bbl.SetFontColor(MOM_TEXT_COLOR);
        bbl.SetFontHeightNDC(SPEECH_BUBBLE_TEXT_HEIGHT);
        bbl.SetRelativeFontDisplacement(MUM_RELATIVE_TEXT_DISPLACEMENT);
        bbl.SetFaceExpression(util::faces2D::FaceExpressions::NEUTRAL_HAPPY);
        mom->AddSpeechBubble(bbl);
    }
#pragma endregion

#pragma region //bubble7 - face : NEUTRAL_EYESCLOSED
    {
        util::faces2D::SpeechBubble bbl(BUBBLE_RELATIVE_POS_MOM, { BUBBLE_HEIGHT * MOM_BUBBLE_ASPECT_RATIO,BUBBLE_HEIGHT }, R"(res/SpeechBubbleRight_flip.png)",
            util::Textures::TextureType::PNG, util::faces2D::SpeechBubble::SpeechBubleState::SHOWING, "My child, every data \nstructure has to follow \nsome rules.");
        bbl.SetFontColor(MOM_TEXT_COLOR);
        bbl.SetFontHeightNDC(SPEECH_BUBBLE_TEXT_HEIGHT);
        bbl.SetRelativeFontDisplacement(MUM_RELATIVE_TEXT_DISPLACEMENT);
        bbl.SetFaceExpression(util::faces2D::FaceExpressions::NEUTRAL_EYESCLOSED);
        mom->AddSpeechBubble(bbl);
    }
#pragma endregion

#pragma region //bubble8 - face : NEUTRAL_HAPPY
    {
        util::faces2D::SpeechBubble bbl(BUBBLE_RELATIVE_POS_MOM, { BUBBLE_HEIGHT * MOM_BUBBLE_ASPECT_RATIO,BUBBLE_HEIGHT }, R"(res/SpeechBubbleRight_flip.png)",
            util::Textures::TextureType::PNG, util::faces2D::SpeechBubble::SpeechBubleState::SHOWING, "Yes kate, and it's [COLORREF:RGBA(0,0,255,255)]LIFO[COLORREF:RGBA(255,255,255,255)]\n - [COLORREF:RGBA(0,0,255,255)]ELL[COLORREF:RGBA(255,255,255,255)]-[COLORREF:RGBA(0,0,255,255)]AAI[COLORREF:RGBA(255,255,255,255)]-[COLORREF:RGBA(0,0,255,255)]EFF[COLORREF:RGBA(255,255,255,255)]-[COLORREF:RGBA(0,0,255,255)]O[COLORREF:RGBA(255,255,255,255)] - . \nWhich stands for \n[COLORREF:RGBA(0,0,255,255)]L[COLORREF:RGBA(255,255,255,255)]ast [COLORREF:RGBA(0,0,255,255)]I[COLORREF:RGBA(255,255,255,255)]n [COLORREF:RGBA(0,0,255,255)]F[COLORREF:RGBA(255,255,255,255)]irst [COLORREF:RGBA(0,0,255,255)]O[COLORREF:RGBA(255,255,255,255)]ut.");
        bbl.SetFontColor(MOM_TEXT_COLOR);
        bbl.SetFontHeightNDC(SPEECH_BUBBLE_TEXT_HEIGHT);
        bbl.SetRelativeFontDisplacement(MUM_RELATIVE_TEXT_DISPLACEMENT);
        bbl.SetFaceExpression(util::faces2D::FaceExpressions::NEUTRAL_HAPPY);
        mom->AddSpeechBubble(bbl);
    }
#pragma endregion

#pragma region //bubble9 - face : NEUTRAL_HAPPY
    {
        util::faces2D::SpeechBubble bbl(BUBBLE_RELATIVE_POS_MOM, { BUBBLE_HEIGHT * MOM_BUBBLE_ASPECT_RATIO,BUBBLE_HEIGHT }, R"(res/SpeechBubbleRight_flip.png)",
            util::Textures::TextureType::PNG, util::faces2D::SpeechBubble::SpeechBubleState::SHOWING, "A linked list has 2 \nparts : \nHead, where it starts & \nTail, where it ends.");
        bbl.SetFontColor(MOM_TEXT_COLOR);
        bbl.SetFontHeightNDC(SPEECH_BUBBLE_TEXT_HEIGHT);
        bbl.SetRelativeFontDisplacement(MUM_RELATIVE_TEXT_DISPLACEMENT);
        bbl.SetFaceExpression(util::faces2D::FaceExpressions::NEUTRAL_HAPPY);
        mom->AddSpeechBubble(bbl);
    }
#pragma endregion

#pragma region //bubble10 - face : HAPPY_EYESCLOSED
    {
        util::faces2D::SpeechBubble bbl(BUBBLE_RELATIVE_POS_MOM, { BUBBLE_HEIGHT * MOM_BUBBLE_ASPECT_RATIO,BUBBLE_HEIGHT }, R"(res/SpeechBubbleRight_flip.png)",
            util::Textures::TextureType::PNG, util::faces2D::SpeechBubble::SpeechBubleState::SHOWING, "Yes. [COLORREF:RGBA(255,0,0,255)]Ted the Head[COLORREF:RGBA(255,255,255,255)], and \n[COLORREF:RGBA(0,0,255,255)]Gale the Tail[COLORREF:RGBA(255,255,255,255)]!");
        bbl.SetFontColor(MOM_TEXT_COLOR);
        bbl.SetFontHeightNDC(SPEECH_BUBBLE_TEXT_HEIGHT);
        bbl.SetRelativeFontDisplacement(MUM_RELATIVE_TEXT_DISPLACEMENT);
        bbl.SetFaceExpression(util::faces2D::FaceExpressions::HAPPY_EYESCLOSED);
        mom->AddSpeechBubble(bbl);
    }
#pragma endregion

#pragma region //bubble11 - face : NEUTRAL_HAPPY
    {
        util::faces2D::SpeechBubble bbl(BUBBLE_RELATIVE_POS_MOM, { BUBBLE_HEIGHT * MOM_BUBBLE_ASPECT_RATIO,BUBBLE_HEIGHT }, R"(res/SpeechBubbleRight_flip.png)",
            util::Textures::TextureType::PNG, util::faces2D::SpeechBubble::SpeechBubleState::SHOWING, "Yes. Just like a snake.");
        bbl.SetFontColor(MOM_TEXT_COLOR);
        bbl.SetFontHeightNDC(SPEECH_BUBBLE_TEXT_HEIGHT);
        bbl.SetRelativeFontDisplacement(MUM_RELATIVE_TEXT_DISPLACEMENT);
        bbl.SetFaceExpression(util::faces2D::FaceExpressions::NEUTRAL_HAPPY);
        mom->AddSpeechBubble(bbl);
    }
#pragma endregion

#pragma region //bubble12 - face : NEUTRAL_EYESCLOSED
    {
        util::faces2D::SpeechBubble bbl(BUBBLE_RELATIVE_POS_MOM, { BUBBLE_HEIGHT * MOM_BUBBLE_ASPECT_RATIO,BUBBLE_HEIGHT }, R"(res/SpeechBubbleRight_flip.png)",
            util::Textures::TextureType::PNG, util::faces2D::SpeechBubble::SpeechBubleState::SHOWING, "So, to get to any part \nof a linked list you \nmust start from head, \nand move towards tail.");
        bbl.SetFontColor(MOM_TEXT_COLOR);
        bbl.SetFontHeightNDC(SPEECH_BUBBLE_TEXT_HEIGHT);
        bbl.SetRelativeFontDisplacement(MUM_RELATIVE_TEXT_DISPLACEMENT);
        bbl.SetFaceExpression(util::faces2D::FaceExpressions::NEUTRAL_EYESCLOSED);
        mom->AddSpeechBubble(bbl);
    }
#pragma endregion

#pragma region //bubble13 - face : NEUTRAL_EYESCLOSED
    {
        util::faces2D::SpeechBubble bbl(BUBBLE_RELATIVE_POS_MOM, { BUBBLE_HEIGHT * MOM_BUBBLE_ASPECT_RATIO,BUBBLE_HEIGHT }, R"(res/SpeechBubbleRight_flip.png)",
            util::Textures::TextureType::PNG, util::faces2D::SpeechBubble::SpeechBubleState::SHOWING, "You stop on finding \nthe element you were \nlooking for, otherwise \nyou reach the tail ...");
        bbl.SetFontColor(MOM_TEXT_COLOR);
        bbl.SetFontHeightNDC(SPEECH_BUBBLE_TEXT_HEIGHT);
        bbl.SetRelativeFontDisplacement(MUM_RELATIVE_TEXT_DISPLACEMENT);
        bbl.SetFaceExpression(util::faces2D::FaceExpressions::NEUTRAL_EYESCLOSED);
        mom->AddSpeechBubble(bbl);
        bbl.SetText("... beyond which you \ncannot go.");
        mom->AddSpeechBubble(bbl);
    }
#pragma endregion

#pragma region //bubble14 - face : NEUTRAL
    {
        util::faces2D::SpeechBubble bbl(BUBBLE_RELATIVE_POS_MOM, { BUBBLE_HEIGHT * MOM_BUBBLE_ASPECT_RATIO,BUBBLE_HEIGHT }, R"(res/SpeechBubbleRight_flip.png)",
            util::Textures::TextureType::PNG, util::faces2D::SpeechBubble::SpeechBubleState::SHOWING, "Don't forget to tell \nBet & Dal to hold hands \nSo that our chain is \nconnected again.");
        bbl.SetFontColor(MOM_TEXT_COLOR);
        bbl.SetFontHeightNDC(SPEECH_BUBBLE_TEXT_HEIGHT);
        bbl.SetRelativeFontDisplacement(MUM_RELATIVE_TEXT_DISPLACEMENT);
        bbl.SetFaceExpression(util::faces2D::FaceExpressions::NEUTRAL);
        mom->AddSpeechBubble(bbl);
    }
#pragma endregion

#pragma region //bubble15 - face : NEUTRAL_EYESCLOSED
    {
        util::faces2D::SpeechBubble bbl(BUBBLE_RELATIVE_POS_MOM, { BUBBLE_HEIGHT * MOM_BUBBLE_ASPECT_RATIO,BUBBLE_HEIGHT }, R"(res/SpeechBubbleRight_flip.png)",
            util::Textures::TextureType::PNG, util::faces2D::SpeechBubble::SpeechBubleState::SHOWING, "Let's say you want to \nput John between Dal, \nand Gale.");
        bbl.SetFontColor(MOM_TEXT_COLOR);
        bbl.SetFontHeightNDC(SPEECH_BUBBLE_TEXT_HEIGHT);
        bbl.SetRelativeFontDisplacement(MUM_RELATIVE_TEXT_DISPLACEMENT);
        bbl.SetFaceExpression(util::faces2D::FaceExpressions::NEUTRAL_EYESCLOSED);
        mom->AddSpeechBubble(bbl);
    }
#pragma endregion

#pragma region //bubble16 - face : NEUTRAL
    {
        util::faces2D::SpeechBubble bbl(BUBBLE_RELATIVE_POS_MOM, { BUBBLE_HEIGHT * MOM_BUBBLE_ASPECT_RATIO,BUBBLE_HEIGHT }, R"(res/SpeechBubbleRight_flip.png)",
            util::Textures::TextureType::PNG, util::faces2D::SpeechBubble::SpeechBubleState::SHOWING, "Starting from \nTed-the-Head, you move \ntowards Gale-the-Tail.");
        bbl.SetFontColor(MOM_TEXT_COLOR);
        bbl.SetFontHeightNDC(SPEECH_BUBBLE_TEXT_HEIGHT);
        bbl.SetRelativeFontDisplacement(MUM_RELATIVE_TEXT_DISPLACEMENT);
        bbl.SetFaceExpression(util::faces2D::FaceExpressions::NEUTRAL);
        mom->AddSpeechBubble(bbl);
    }
#pragma endregion

#pragma region //bubble17 - face : NEUTRAL_EYESCLOSED
    {
        util::faces2D::SpeechBubble bbl(BUBBLE_RELATIVE_POS_MOM, { BUBBLE_HEIGHT * MOM_BUBBLE_ASPECT_RATIO,BUBBLE_HEIGHT }, R"(res/SpeechBubbleRight_flip.png)",
            util::Textures::TextureType::PNG, util::faces2D::SpeechBubble::SpeechBubleState::SHOWING, "You stop at Dal. Tell \nDal to leave Gale's \nhand and to hold John's \nhand instead.");
        bbl.SetFontColor(MOM_TEXT_COLOR);
        bbl.SetFontHeightNDC(SPEECH_BUBBLE_TEXT_HEIGHT);
        bbl.SetRelativeFontDisplacement(MUM_RELATIVE_TEXT_DISPLACEMENT);
        bbl.SetFaceExpression(util::faces2D::FaceExpressions::NEUTRAL_EYESCLOSED);
        mom->AddSpeechBubble(bbl);
    }
#pragma endregion

#pragma region //bubble18 - face : NEUTRAL
    {
        util::faces2D::SpeechBubble bbl(BUBBLE_RELATIVE_POS_MOM, { BUBBLE_HEIGHT * MOM_BUBBLE_ASPECT_RATIO,BUBBLE_HEIGHT }, R"(res/SpeechBubbleRight_flip.png)",
            util::Textures::TextureType::PNG, util::faces2D::SpeechBubble::SpeechBubleState::SHOWING, "Then tell John to hold \nGayle's hand.");
        bbl.SetFontColor(MOM_TEXT_COLOR);
        bbl.SetFontHeightNDC(SPEECH_BUBBLE_TEXT_HEIGHT);
        bbl.SetRelativeFontDisplacement(MUM_RELATIVE_TEXT_DISPLACEMENT);
        bbl.SetFaceExpression(util::faces2D::FaceExpressions::NEUTRAL);
        mom->AddSpeechBubble(bbl);
    }
#pragma endregion

#pragma region //bubble19 - face : ANGRY
    {
        util::faces2D::SpeechBubble bbl(BUBBLE_RELATIVE_POS_MOM, { BUBBLE_HEIGHT * MOM_BUBBLE_ASPECT_RATIO,BUBBLE_HEIGHT }, R"(res/SpeechBubbleRight_flip.png)",
            util::Textures::TextureType::PNG, util::faces2D::SpeechBubble::SpeechBubleState::SHOWING, "Mind your language \nKate!");
        bbl.SetFontColor(MOM_TEXT_COLOR);
        bbl.SetFontHeightNDC(SPEECH_BUBBLE_TEXT_HEIGHT);
        bbl.SetRelativeFontDisplacement(MUM_RELATIVE_TEXT_DISPLACEMENT);
        bbl.SetFaceExpression(util::faces2D::FaceExpressions::ANGRY);
        mom->AddSpeechBubble(bbl);
    }
#pragma endregion

#pragma region //bubble20 - face : NEUTRAL
    {
        util::faces2D::SpeechBubble bbl(BUBBLE_RELATIVE_POS_MOM, { BUBBLE_HEIGHT * MOM_BUBBLE_ASPECT_RATIO,BUBBLE_HEIGHT }, R"(res/SpeechBubbleRight_flip.png)",
            util::Textures::TextureType::PNG, util::faces2D::SpeechBubble::SpeechBubleState::SHOWING, "That's a lesson for \nanother day. Right now \nyou've to go to school.\nIt's already late.");
        bbl.SetFontColor(MOM_TEXT_COLOR);
        bbl.SetFontHeightNDC(SPEECH_BUBBLE_TEXT_HEIGHT);
        bbl.SetRelativeFontDisplacement(MUM_RELATIVE_TEXT_DISPLACEMENT);
        bbl.SetFaceExpression(util::faces2D::FaceExpressions::NEUTRAL);
        mom->AddSpeechBubble(bbl);
    }
#pragma endregion

#pragma region //bubble21 - face : HAPPY_EYESCLOSED
    {
        util::faces2D::SpeechBubble bbl(BUBBLE_RELATIVE_POS_MOM, { BUBBLE_HEIGHT * MOM_BUBBLE_ASPECT_RATIO,BUBBLE_HEIGHT }, R"(res/SpeechBubbleRight_flip.png)",
            util::Textures::TextureType::PNG, util::faces2D::SpeechBubble::SpeechBubleState::SHOWING, "Bye, Kate");
        bbl.SetFontColor(MOM_TEXT_COLOR);
        bbl.SetFontHeightNDC(SPEECH_BUBBLE_TEXT_HEIGHT);
        bbl.SetRelativeFontDisplacement(MUM_RELATIVE_TEXT_DISPLACEMENT);
        bbl.SetFaceExpression(util::faces2D::FaceExpressions::HAPPY_EYESCLOSED);
        mom->AddSpeechBubble(bbl);
    }
#pragma endregion

#pragma region //bubble22 - face : NEUTRAL_EYESCLOSED
    {
        util::faces2D::SpeechBubble bbl(BUBBLE_RELATIVE_POS_MOM, { BUBBLE_HEIGHT * MOM_BUBBLE_ASPECT_RATIO,BUBBLE_HEIGHT }, R"(res/SpeechBubbleRight_flip.png)",
            util::Textures::TextureType::PNG, util::faces2D::SpeechBubble::SpeechBubleState::SHOWING, "She's too curious \nfor a 4yo.");
        bbl.SetFontColor(MOM_TEXT_COLOR);
        bbl.SetFontHeightNDC(SPEECH_BUBBLE_TEXT_HEIGHT);
        bbl.SetRelativeFontDisplacement(MUM_RELATIVE_TEXT_DISPLACEMENT);
        bbl.SetFaceExpression(util::faces2D::FaceExpressions::NEUTRAL_EYESCLOSED);
        mom->AddSpeechBubble(bbl);
    }
#pragma endregion

#pragma endregion
    mom->EnableSpeechBubble(false);
    //face coordinates
    mom->SetCellAspectRatio(MUM_FACE_CELL_ASPECT_RATION);
    mom->SetCellHeight(MUM_FACE_HEIGTH);
    util::DataTypes::POINT3Dd momSpawnPos = MUM_SPAWN_POSITION;
    momSpawnPos.x -= mom->GetCellSize().cx / 2.0;
    {
        util::AnimationObject ao(tl, mom, momSpawnPos);
        ao.AddKeyFrame(util::KeyFrame(0, util::KeyFrame::ACTION::OBJECT_SHOW, ANIMATION_START_T, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, momSpawnPos));
        ao.AddKeyFrame(util::KeyFrame(1, util::KeyFrame::ACTION::EXECUTE_ARBITRARY_CALLBACK, ANIMATION_START_T + (*MOM_TIME_FRAMES.begin())*INTER_SPEECH_BUBBLE_TIME, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, { 0,0,0 }, 0, callBackEnableSpeechBubble));

        const auto& frameList1 = MOM_TIME_FRAMES;
        for (int i = 1; i < frameList1.size(); i++)
        {
            int time = *(frameList1.begin() + i);
            ao.AddKeyFrame(util::KeyFrame(1, util::KeyFrame::ACTION::EXECUTE_ARBITRARY_CALLBACK, ANIMATION_START_T + (time)*INTER_SPEECH_BUBBLE_TIME, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, { 0,0,0 }, 0, callBackEnableSpeechBubble));
            ao.AddKeyFrame(util::KeyFrame(1, util::KeyFrame::ACTION::EXECUTE_ARBITRARY_CALLBACK, ANIMATION_START_T + (time)*INTER_SPEECH_BUBBLE_TIME, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, { 0,0,0 }, 0, callBackNextSpeechBubble));
        }

        const auto& frameList2 = KATE_TIME_FRAMES;
        for (int i = 0; i < frameList2.size(); i++)
        {
            int time = *(frameList2.begin() + i);
            ao.AddKeyFrame(util::KeyFrame(1, util::KeyFrame::ACTION::EXECUTE_ARBITRARY_CALLBACK, ANIMATION_START_T + (time)*INTER_SPEECH_BUBBLE_TIME, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, { 0,0,0 }, 0, callBackDisableSpeechBubble));
        }

        ao.AddKeyFrame(util::KeyFrame(1, util::KeyFrame::ACTION::EXECUTE_ARBITRARY_CALLBACK, JohnWanOutTextTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, { 0,0,0 }, 0, callBackDisableSpeechBubble));
        ao.AddKeyFrame(util::KeyFrame(1, util::KeyFrame::ACTION::EXECUTE_ARBITRARY_CALLBACK, JohnTroubleTextTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, { 0,0,0 }, 0, callBackDisableSpeechBubble));
        ao.AddKeyFrame(util::KeyFrame(1, util::KeyFrame::ACTION::EXECUTE_ARBITRARY_CALLBACK, JohnSnakeTextTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, { 0,0,0 }, 0, callBackDisableSpeechBubble));
        ao.AddKeyFrame(util::KeyFrame(1, util::KeyFrame::ACTION::OBJECT_HIDE, CreditsTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE));
        tl->AddObject(ao);
    }
#pragma endregion

#pragma region island
    double kidsLine1Time = ANIMATION_START_T + 6 * INTER_SPEECH_BUBBLE_TIME;
    {
        auto island = std::shared_ptr<util::TextureSceneObject>(new util::TextureSceneObject(ID_ISLAND, R"(res/island_flip.png)", util::Textures::TextureType::PNG));
        island->SetCellHeight(ISLAND_HEIGHT);
        util::AnimationObject ao(tl, island, { 0,0,0 });
        ao.SetDebugName("island1");
        ao.AddKeyFrame(util::KeyFrame(0, util::KeyFrame::ACTION::OBJECT_SHOW, kidsLine1Time, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, ISLAND_POS));
        ao.AddKeyFrame(util::KeyFrame(1, util::KeyFrame::ACTION::OBJECT_HIDE, CreditsTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE));
        tl->AddObject(ao);
    }

    {
        auto kidsLine1 = std::shared_ptr<util::TextureSceneObject>(new util::TextureSceneObject(ID_LEAF, R"(res/kids_line1_flip.png)", util::Textures::TextureType::PNG));
        kidsLine1->SetCellHeight(ISLAND_KIDS_HEIGHT1);
        util::AnimationObject ao(tl, kidsLine1, { 0,0,0 });
        ao.AddKeyFrame(util::KeyFrame(0, util::KeyFrame::ACTION::OBJECT_SHOW, kidsLine1Time, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, KIDS_LINE_POS));
        ao.AddKeyFrame(util::KeyFrame(0, util::KeyFrame::ACTION::OBJECT_HIDE, JohnWanOutTextTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, KIDS_LINE_POS));
        ao.AddKeyFrame(util::KeyFrame(0, util::KeyFrame::ACTION::OBJECT_SHOW, HideLeafTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, KIDS_LINE_POS));
        ao.AddKeyFrame(util::KeyFrame(0, util::KeyFrame::ACTION::OBJECT_HIDE, JohnSnakeTextTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, KIDS_LINE_POS));
        tl->AddObject(ao);
    }

    {
        auto kidsLine2 = std::shared_ptr<util::TextureSceneObject>(new util::TextureSceneObject(ID_LEAF, R"(res/kids_line2_john_want_out_flip.png)", util::Textures::TextureType::PNG));
        kidsLine2->SetCellHeight(ISLAND_KIDS_HEIGHT1);
        util::AnimationObject ao(tl, kidsLine2, { 0,0,0 });
        ao.AddKeyFrame(util::KeyFrame(0, util::KeyFrame::ACTION::OBJECT_SHOW, JohnWanOutTextTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, KIDS_LINE_POS));
        ao.AddKeyFrame(util::KeyFrame(0, util::KeyFrame::ACTION::OBJECT_HIDE, ShowLeafTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, KIDS_LINE_POS));
        ao.AddKeyFrame(util::KeyFrame(0, util::KeyFrame::ACTION::OBJECT_SHOW, JohnSnakeTextTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, KIDS_LINE_POS));
        ao.AddKeyFrame(util::KeyFrame(0, util::KeyFrame::ACTION::OBJECT_HIDE, Line3Time, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, KIDS_LINE_POS));
        tl->AddObject(ao);
    }

    {
        auto johnWantOutText = std::shared_ptr<util::TextureSceneObject>(new util::TextureSceneObject(ID_LEAF, R"(res/kids_john_want_out_text_flip.png)", util::Textures::TextureType::PNG));
        johnWantOutText->SetCellHeight(JOHN_TEXT_WANTOUT_HEIGHT);
        util::AnimationObject ao(tl, johnWantOutText, { 0,0,0 });
        ao.AddKeyFrame(util::KeyFrame(0, util::KeyFrame::ACTION::OBJECT_SHOW, JohnWanOutTextTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, JOHN_TEXT_WANTOUT_POS));
        ao.AddKeyFrame(util::KeyFrame(0, util::KeyFrame::ACTION::OBJECT_HIDE, JohnTroubleTextTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, JOHN_TEXT_WANTOUT_POS));
        tl->AddObject(ao);
    }

    {
        auto johnTroubleText = std::shared_ptr<util::TextureSceneObject>(new util::TextureSceneObject(ID_LEAF, R"(res/kids_john_trouble_text_flip.png)", util::Textures::TextureType::PNG));
        johnTroubleText->SetCellHeight(JOHN_TEXT_TROUBLE_HEIGHT);
        util::AnimationObject ao(tl, johnTroubleText, { 0,0,0 });
        auto text2Pos = JOHN_TEXT_POS;
        text2Pos.x *= 1.1;
        text2Pos.y *= 1.11;
        ao.AddKeyFrame(util::KeyFrame(0, util::KeyFrame::ACTION::OBJECT_SHOW, JohnTroubleTextTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, text2Pos));
        ao.AddKeyFrame(util::KeyFrame(0, util::KeyFrame::ACTION::OBJECT_HIDE, ShowLeafTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, text2Pos));
        tl->AddObject(ao);
    }

    //TODO: fix - using same image - texture caching is not working
    {
        //island 2 to temporarily hide kids, when showing leaf
        //auto island2 = std::shared_ptr<util::TextureSceneObject>(new util::TextureSceneObject(ID_ISLAND, R"(res/island_flip2.png)", util::Textures::TextureType::PNG));
        //island2->SetCellHeight(ISLAND_HEIGHT);
        //util::AnimationObject ao1(tl, island2, { 0,0,0 });
        //ao1.SetDebugName("island2");
        //ao1.AddKeyFrame(util::KeyFrame(0, util::KeyFrame::ACTION::OBJECT_SHOW, ShowLeafTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, { -0.7,1.2,0.001 }));
        //ao1.AddKeyFrame(util::KeyFrame(0, util::KeyFrame::ACTION::OBJECT_HIDE, HideLeafTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, { -0.7,1.2,0.001 }));
        //tl->AddObject(ao1);

        auto leaf = std::shared_ptr<util::TextureSceneObject>(new util::TextureSceneObject(ID_LEAF, R"(res/leafo_flip.png)", util::Textures::TextureType::PNG));
        leaf->SetCellHeight(0.5);
        util::AnimationObject ao2(tl, leaf, { 0,0,0 });
        ao2.AddKeyFrame(util::KeyFrame(0, util::KeyFrame::ACTION::OBJECT_SHOW, ShowLeafTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, { -0.2,0.95,0.00101 }));
        ao2.AddKeyFrame(util::KeyFrame(0, util::KeyFrame::ACTION::OBJECT_HIDE, HideLeafTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, { -0.2,0.95,0.00101 }));
        tl->AddObject(ao2);
    }

    {
        auto JohnSnakeText = std::shared_ptr<util::TextureSceneObject>(new util::TextureSceneObject(ID_LEAF, R"(res/kids_john_snake_text_flip.png)", util::Textures::TextureType::PNG));
        JohnSnakeText->SetCellHeight(JOHN_TEXT_SNAKE_HEIGHT);
        util::AnimationObject ao(tl, JohnSnakeText, { 0,0,0 });
        auto text2Pos = JOHN_TEXT_SNAKE_POS;
        text2Pos.x *= 1.4;
        text2Pos.y *= 1.05;
        ao.AddKeyFrame(util::KeyFrame(0, util::KeyFrame::ACTION::OBJECT_SHOW, JohnSnakeTextTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, text2Pos));
        ao.AddKeyFrame(util::KeyFrame(0, util::KeyFrame::ACTION::OBJECT_HIDE, Line3Time, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, text2Pos));
        tl->AddObject(ao);
    }

    {
        auto kidsLine3 = std::shared_ptr<util::TextureSceneObject>(new util::TextureSceneObject(ID_LEAF, R"(res/kids_line3_john_out_flip.png)", util::Textures::TextureType::PNG));
        kidsLine3->SetCellHeight(ISLAND_KIDS_HEIGHT2);
        util::AnimationObject ao(tl, kidsLine3, { 0,0,0 });
        auto pos = KIDS_LINE_POS;
        pos.y += 0.124;
        ao.AddKeyFrame(util::KeyFrame(0, util::KeyFrame::ACTION::OBJECT_SHOW, Line3Time, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, pos));
        ao.AddKeyFrame(util::KeyFrame(0, util::KeyFrame::ACTION::OBJECT_HIDE, Line3Time, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, pos));
        ao.AddKeyFrame(util::KeyFrame(0, util::KeyFrame::ACTION::OBJECT_SHOW, Line3Time, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, pos));
        ao.AddKeyFrame(util::KeyFrame(0, util::KeyFrame::ACTION::OBJECT_HIDE, Line4Time, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, pos));
        tl->AddObject(ao);
    }

    {
        auto kidsLine4 = std::shared_ptr<util::TextureSceneObject>(new util::TextureSceneObject(ID_LEAF, R"(res/kids_line4_no_john_flip.png)", util::Textures::TextureType::PNG));
        kidsLine4->SetCellHeight(ISLAND_KIDS_HEIGHT1);
        util::AnimationObject ao(tl, kidsLine4, { 0,0,0 });
        auto pos = KIDS_LINE_POS;
        ao.AddKeyFrame(util::KeyFrame(0, util::KeyFrame::ACTION::OBJECT_SHOW, Line4Time, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, pos));
        ao.AddKeyFrame(util::KeyFrame(0, util::KeyFrame::ACTION::OBJECT_HIDE, Line4Time, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, pos));
        ao.AddKeyFrame(util::KeyFrame(0, util::KeyFrame::ACTION::OBJECT_SHOW, Line4Time, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, pos));
        ao.AddKeyFrame(util::KeyFrame(0, util::KeyFrame::ACTION::OBJECT_HIDE, JohnInsertTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, pos));
        tl->AddObject(ao);
    }

    {
        auto kidsLine5 = std::shared_ptr<util::TextureSceneObject>(new util::TextureSceneObject(ID_LEAF, R"(res/kids_line5_john_insert_flip.png)", util::Textures::TextureType::PNG));
        kidsLine5->SetCellHeight(ISLAND_KIDS_HEIGHT3);
        util::AnimationObject ao(tl, kidsLine5, { 0,0,0 });
        auto pos = KIDS_LINE_POS;
        pos.y += 0.298;
        ao.AddKeyFrame(util::KeyFrame(0, util::KeyFrame::ACTION::OBJECT_SHOW, JohnInsertTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, pos));
        ao.AddKeyFrame(util::KeyFrame(0, util::KeyFrame::ACTION::OBJECT_HIDE, JohnInsertTime2, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, pos));
        tl->AddObject(ao);
    }

    {
        auto kidsLine6 = std::shared_ptr<util::TextureSceneObject>(new util::TextureSceneObject(ID_LEAF, R"(res/kids_line6_john_insert2_flip.png)", util::Textures::TextureType::PNG));
        kidsLine6->SetCellHeight(ISLAND_KIDS_HEIGHT3);
        util::AnimationObject ao(tl, kidsLine6, { 0,0,0 });
        auto pos = KIDS_LINE_POS;
        pos.y += 0.298;
        ao.AddKeyFrame(util::KeyFrame(0, util::KeyFrame::ACTION::OBJECT_SHOW, JohnInsertTime2, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, pos));
        ao.AddKeyFrame(util::KeyFrame(0, util::KeyFrame::ACTION::OBJECT_HIDE, JohnBackTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, pos));
        tl->AddObject(ao);
    }

    {
        auto kidsLine7 = std::shared_ptr<util::TextureSceneObject>(new util::TextureSceneObject(ID_LEAF, R"(res/kids_line7_john_back_flip.png)", util::Textures::TextureType::PNG));
        kidsLine7->SetCellHeight(ISLAND_KIDS_HEIGHT4);
        util::AnimationObject ao(tl, kidsLine7, { 0,0,0 });
        auto pos = KIDS_LINE_POS;
        pos.y += 0.252;
        ao.AddKeyFrame(util::KeyFrame(0, util::KeyFrame::ACTION::OBJECT_SHOW, JohnBackTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, pos));
        ao.AddKeyFrame(util::KeyFrame(1, util::KeyFrame::ACTION::OBJECT_HIDE, CreditsTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE));
        tl->AddObject(ao);
    }
#pragma endregion

    const double bustXAdjust = 0.4;
    util::DataTypes::POINT3Dd tedBustPos = { myOglWnd->GetMinX2() - bustXAdjust, myOglWnd->GetMaxY2() + 0.2,0 };
    {
        auto ted = std::shared_ptr<util::TextureSceneObject>(new util::TextureSceneObject(ID_KIDS_TED_BUST, R"(res/Ted_Bust_flip.png)", util::Textures::TextureType::PNG));
        ted->SetCellHeight(KIDS_BUST_HEIGHT);
        util::AnimationObject ao(tl, ted, { 0,0,0 });
        ao.SetDebugName("ted");
        ao.AddKeyFrame(util::KeyFrame(0, util::KeyFrame::ACTION::OBJECT_SHOW, kidsLine1Time, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, tedBustPos));
        ao.AddKeyFrame(util::KeyFrame(1, util::KeyFrame::ACTION::OBJECT_HIDE, CreditsTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE));
        tl->AddObject(ao);
    }

    util::DataTypes::POINT3Dd betBustPos = tedBustPos;
    betBustPos.y -= KIDS_BUST_HEIGHT*1.10;
    {
        auto bet = std::shared_ptr<util::TextureSceneObject>(new util::TextureSceneObject(ID_KIDS_BET_BUST, R"(res/Bet_Bust_flip.png)", util::Textures::TextureType::PNG));
        bet->SetCellHeight(KIDS_BUST_HEIGHT);
        util::AnimationObject ao(tl, bet, { 0,0,0 });
        ao.SetDebugName("bet");
        ao.AddKeyFrame(util::KeyFrame(0, util::KeyFrame::ACTION::OBJECT_SHOW, kidsLine1Time, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, betBustPos));
        ao.AddKeyFrame(util::KeyFrame(1, util::KeyFrame::ACTION::OBJECT_HIDE, CreditsTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE));
        tl->AddObject(ao);
    }

    util::DataTypes::POINT3Dd johnBustPos = betBustPos;
    johnBustPos.y -= KIDS_BUST_HEIGHT * 1.10;
    {
        auto john = std::shared_ptr<util::TextureSceneObject>(new util::TextureSceneObject(ID_KIDS_BET_BUST, R"(res/John_Bust_flip.png)", util::Textures::TextureType::PNG));
        john->SetCellHeight(KIDS_BUST_HEIGHT);
        util::AnimationObject ao(tl, john, { 0,0,0 });
        ao.SetDebugName("john");
        ao.AddKeyFrame(util::KeyFrame(0, util::KeyFrame::ACTION::OBJECT_SHOW, kidsLine1Time, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, johnBustPos));
        ao.AddKeyFrame(util::KeyFrame(1, util::KeyFrame::ACTION::OBJECT_HIDE, CreditsTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE));
        tl->AddObject(ao);
    }


    util::DataTypes::POINT3Dd dalBustPos = tedBustPos;
    dalBustPos.x = myOglWnd->GetMaxX2() + bustXAdjust;
    {
        auto dal = std::shared_ptr<util::TextureSceneObject>(new util::TextureSceneObject(ID_KIDS_BET_BUST, R"(res/Dal_Bust_flip.png)", util::Textures::TextureType::PNG));
        dal->SetCellHeight(KIDS_BUST_HEIGHT);
        dalBustPos.x -= dal->GetCurrentImageCellWidth();
        util::AnimationObject ao(tl, dal, { 0,0,0 });
        ao.SetDebugName("dal");
        ao.AddKeyFrame(util::KeyFrame(0, util::KeyFrame::ACTION::OBJECT_SHOW, kidsLine1Time, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, dalBustPos));
        ao.AddKeyFrame(util::KeyFrame(1, util::KeyFrame::ACTION::OBJECT_HIDE, CreditsTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE));
        tl->AddObject(ao);
    }

    util::DataTypes::POINT3Dd galeBustPos = dalBustPos;
    galeBustPos.y -= KIDS_BUST_HEIGHT * 1.10;
    {
        auto gale = std::shared_ptr<util::TextureSceneObject>(new util::TextureSceneObject(ID_KIDS_BET_BUST, R"(res/Gale_Bust_flip.png)", util::Textures::TextureType::PNG));
        gale->SetCellHeight(KIDS_BUST_HEIGHT);
        util::AnimationObject ao(tl, gale, { 0,0,0 });
        ao.SetDebugName("gale");
        ao.AddKeyFrame(util::KeyFrame(0, util::KeyFrame::ACTION::OBJECT_SHOW, kidsLine1Time, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, galeBustPos));
        ao.AddKeyFrame(util::KeyFrame(1, util::KeyFrame::ACTION::OBJECT_HIDE, CreditsTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE));
        tl->AddObject(ao);
    }

    //music
    {
        util::KeyFrame::CALLBACKFUNC callBackMusicStart = [](std::shared_ptr<util::SceneObject> obj, util::AnimationObjectState state) {
            auto object = std::dynamic_pointer_cast<util::EmptyObject>(obj);
            if (object)
            {
                auto retval = PlaySound(TEXT("res/Memo.wav"), NULL, SND_FILENAME | SND_ASYNC | SND_NODEFAULT);
                return retval;//TODO: Log error
            }
        };

        util::KeyFrame::CALLBACKFUNC callBackMusicEnd = [](std::shared_ptr<util::SceneObject> obj, util::AnimationObjectState state) {
            auto object = std::dynamic_pointer_cast<util::EmptyObject>(obj);
            if (object)
            {
                PlaySound(NULL, 0, 0);
            }
        };

        std::shared_ptr<util::EmptyObject> music(new util::EmptyObject);
        util::AnimationObject ao(tl, music, {});
        ao.AddKeyFrame(util::KeyFrame(0, util::KeyFrame::ACTION::EXECUTE_ARBITRARY_CALLBACK, ANIMATION_START_T, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, {}, 0, callBackMusicStart));
        ao.AddKeyFrame(util::KeyFrame(1, util::KeyFrame::ACTION::EXECUTE_ARBITRARY_CALLBACK, CreditsTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, {}, 0, callBackMusicEnd));
        tl->AddObject(ao);
    }


    //credits
    {
        auto credits = std::shared_ptr<util::TextureSceneObject>(new util::TextureSceneObject(ID_LEAF, R"(res/credits_flip.png)", util::Textures::TextureType::PNG));
        credits->SetCellHeight(myOglWnd->GetWindowHeightNDC());
        util::AnimationObject ao(tl, credits, { 0,0,0 }, myOglWnd);
        util::DataTypes::POINT3Dd pos = { -credits->GetCurrentImageCellWidth() / 2.0, myOglWnd->GetWindowHeightNDC() / 2.0, 0.01 };
        ao.AddKeyFrame(util::KeyFrame(0, util::KeyFrame::ACTION::EXECUTE_ARBITRARY_CALLBACK, CreditsTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, { 0,0,0 }, 0, callBackSetClearColorForCredits));
        ao.AddKeyFrame(util::KeyFrame(1, util::KeyFrame::ACTION::OBJECT_SHOW, CreditsTime, util::KeyFrame::TIMETYPE::ABSOLUTETIME, util::KeyFrame::TWEENTYPE::NONE, pos));
        tl->AddObject(ao);
    }

}

/*
REFERENCES -
KATE FACE -
https://www.freepik.com/free-vector/woman-faces-with-different-expressions_1250765.htm -> <a href="https://www.freepik.com/free-photos-vectors/background">Background vector created by brgfx - www.freepik.com</a>

MUM FACE -
https://www.freepik.com/free-vector/people-showing-emotions_4186822.htm
<a href="https://www.freepik.com/free-photos-vectors/people">People vector created by pikisuperstar - www.freepik.com</a>

Fretype library -
Portions of this software are copyright © 2019 The FreeType
    Project (www.freetype.org).  All rights reserved.

Nehe tutorial on text rendering
http://nehe.gamedev.net/tutorial/freetype_fonts_in_opengl/24001/

https://fonts.google.com/specimen/Roboto+Mono?selection.family=Roboto+Mono

https://dcinoot.bandcamp.com/track/memo
*/