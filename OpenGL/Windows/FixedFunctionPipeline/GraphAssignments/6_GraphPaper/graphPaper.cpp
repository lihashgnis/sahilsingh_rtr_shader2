#include "../../../../../99_AuxStuff/MyLibs/Utility/Util.hpp"
#include <gl\GL.h>
#include <iostream>

constexpr UINT WIN_WIDTH = 800;
constexpr UINT WIN_HEIGHT = 600;
constexpr UINT WIN_X = 100;
constexpr UINT WIN_Y = 100;

class MyOglWindow : public util::OglWindow
{
	LRESULT OnCharPress(HWND, UINT, WPARAM, LPARAM)
	{

	}
	// Inherited via OglWindow
	const char * GetMessageMap() override
	{
		return nullptr;
	}

	void Display() override
	{


		glClear(GL_COLOR_BUFFER_BIT);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		glLineWidth(1.0f);

		float factor = 2.0 / 40.0;
        DrawGraph(factor);

		SwapBuffers(m_Hdc);
	}

    void DrawGraph(float factor) 
    {
        util::DrawShapes::DrawGraph(factor);
    }

    LRESULT WndProcPost(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
    {
        return LRESULT(0);
    }

	LRESULT WndProcPre(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
	{
		switch (iMsg)
		{
		case WM_CHAR:
		{
			switch (wParam)
			{
			case 'a':
				glEnable(GL_POINT_SMOOTH);
				SetWindowTextA(hwnd, "GRAPH Paper -- AA Enabled");
				break;
			case 'd':
				glDisable(GL_POINT_SMOOTH);
				SetWindowTextA(hwnd, "GRAPH Paper -- AA Disabled");
				break;
			default:
				break;
			}
		}
		}
		return LRESULT();
	}

	virtual void Resize(int width, int height) override
	{
		glViewport(0, 0, width, height);
	}
public:
	MyOglWindow(TCHAR * className, UINT x, UINT y, UINT width, UINT height, HINSTANCE hInstance) : OglWindow(className, x, y, width, height, hInstance)
	{

	}

    // Inherited via OglWindow
    virtual void Update() override
    {
    }
};

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	TCHAR AppName[] = TEXT("GRAPH_POINT");
	MessageBoxA(NULL, " a - enable AA \n d - disable AA", "Info", MB_OK);
	MyOglWindow myOglWnd(AppName, WIN_X, WIN_Y, WIN_WIDTH, WIN_HEIGHT, hInstance);
	myOglWnd.InitAndShowWindow();
	glEnable(GL_POINT_SMOOTH);
	SetWindowTextA(myOglWnd.GetHwnd(), "GRAPH Paper -- AA Enabled");
	myOglWnd.RunGameLoop();
	return 0;
}