#include "../../../../../99_AuxStuff/MyLibs/Utility/Util.hpp"
#include <gl\GL.h>
#include <iostream>

constexpr UINT WIN_WIDTH = 800;
constexpr UINT WIN_HEIGHT = 600;
constexpr UINT WIN_X = 100;
constexpr UINT WIN_Y = 100;

class MyOglWindow : public util::OglWindow
{
    LRESULT OnCharPress(HWND, UINT, WPARAM, LPARAM)
    {

    }
    // Inherited via OglWindow
    const char * GetMessageMap() override
    {
        return nullptr;
    }

    void Display() override
    {

        glClear(GL_COLOR_BUFFER_BIT);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

        glLineWidth(1.0f);

        float factor = 2.0 / 40.0;
        float abscissa = 0;
        float ordinate = 0;

        for (float x = -1.0; x <= 1.0; x += factor)
        {
            if (x > (-factor / 2) && x < (factor / 2))
            {
                glLineWidth(3.0f);
                abscissa = x;
            }
            else
            {
                glLineWidth(1.0f);
            }

            glBegin(GL_LINES);

            if (x > (-factor / 2) && x < (factor / 2))
            {
                glColor3f(0.0f, 1.0f, 0.0f);
            }
            else
            {
                glColor3f(1.0f, 1.0f, 1.0f);
            }

            glVertex2f(x, -1);
            glVertex2f(x, 1);
            glEnd();
        }

        for (float y = -1.0; y <= 1.0; y += factor)
        {
            if (y > (-factor / 2) && y < (factor / 2))
            {
                glLineWidth(3.0f);
                ordinate = y;
            }
            else
            {
                glLineWidth(1.0f);
            }

            glBegin(GL_LINES);

            if (y > (-factor / 2) && y < (factor / 2))
            {
                glColor3f(1.0f, 0.0f, 0.0f);
            }
            else
            {
                glColor3f(1.0f, 1.0f, 1.0f);
            }

            glVertex2f(-1, y);
            glVertex2f(1, y);
            glEnd();
        }

        glLineWidth(3.0f);

        //Y-axis
        glBegin(GL_LINES);
        glColor3f(0.0f, 1.0f, 0.0f);
        glVertex2f(abscissa, -1);
        glVertex2f(abscissa, 1);
        glEnd();

        //X-axis
        glBegin(GL_LINES);
        glColor3f(1.0f, 0.0f, 0.0f);
        glVertex2f(-1, ordinate);
        glVertex2f(1, ordinate);
        glEnd();

        //origin
        glPointSize(3.0f);
        glBegin(GL_POINTS);
        glColor3f(1.0f, 1.0f, 0.0);
        glVertex2f(abscissa, ordinate);
        glEnd();


        //triangle
        glLineWidth(3.0f);
        glBegin(GL_LINE_LOOP);
        glColor3f(1.0f, 1.0f, 0.0f);
        glVertex2f(0.0f, 0.8f);
        glVertex2f(-0.8f, -0.8f);
        glVertex2f(0.8f, -0.8f);
        glEnd();

        SwapBuffers(m_Hdc);
    }

    LRESULT WndProcPost(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
    {
        return LRESULT(0);
    }

    LRESULT WndProcPre(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
    {
        switch (iMsg)
        {
        case WM_CHAR:
        {
            switch (wParam)
            {
            case 'a':
                glEnable(GL_POINT_SMOOTH);
                SetWindowTextA(hwnd, "triangleOnGraph -- AA Enabled");
                break;
            case 'd':
                glDisable(GL_POINT_SMOOTH);
                SetWindowTextA(hwnd, "triangleOnGraph -- AA Disabled");
                break;
            default:
                break;
            }
        }
        }
        return LRESULT();
    }

    virtual void Resize(int width, int height) override
    {
        glViewport(0, 0, width, height);
    }
public:
    MyOglWindow(TCHAR * className, UINT x, UINT y, UINT width, UINT height, HINSTANCE hInstance) : OglWindow(className, x, y, width, height, hInstance)
    {

    }

    // Inherited via OglWindow
    virtual void Update() override
    {
    }
};

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
    TCHAR AppName[] = TEXT("triangleOnGraph");
    MessageBoxA(NULL, " a - enable AA \n d - disable AA", "Info", MB_OK);
    MyOglWindow myOglWnd(AppName, WIN_X, WIN_Y, WIN_WIDTH, WIN_HEIGHT, hInstance);
    myOglWnd.InitAndShowWindow();
    glEnable(GL_POINT_SMOOTH);
    SetWindowTextA(myOglWnd.GetHwnd(), "triangleOnGraph -- AA Enabled");
    myOglWnd.RunGameLoop();
    return 0;
}