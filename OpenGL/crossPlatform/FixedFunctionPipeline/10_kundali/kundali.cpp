#include "UtilCrossPlatform.hpp"
#include <GL/gl.h>
#include <GL/glu.h>
#include <cmath>

constexpr UINT WIN_WIDTH = 800;
constexpr UINT WIN_HEIGHT = 800;
constexpr UINT WIN_X = 100;
constexpr UINT WIN_Y = 100;
UINT NUM_PTS = 100;
double RADIUS = 0.5;

class MyOglWindow : public util::OglWindow
{
    const double PI;
    // Inherited via OglWindow
    virtual const char * GetMessageMap() override
    {
        return nullptr;
    }
    virtual void Display() override
    {
        glClear(GL_COLOR_BUFFER_BIT);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        const double theta_delta = (2.0*PI) / NUM_PTS;
        glEnable(GL_LINE_SMOOTH);
        glBegin(GL_LINE_LOOP);
        glColor3f(255, 255, 255);
        glVertex2f(0.8, 0.8);
        glVertex2f(-0.8, 0.8);
        glVertex2f(-0.8, -0.8);
        glVertex2f(0.8, -0.8);
        glEnd();
       
        glBegin(GL_LINE_LOOP);
        glColor3f(255, 255, 255);
        glVertex2f(0, 0.8);
        glVertex2f(-0.8, 0);
        glVertex2f(0, -0.8);
        glVertex2f(0.8, 0);
        glEnd();
        glBegin(GL_LINES);
        glVertex2f(0.8, 0.8);
        glVertex2f(-0.8, -0.8);
        glVertex2f(-0.8, 0.8);
        glVertex2f(0.8, -0.8);
        glEnd();
        util::SwapBuffersCompat();
    }

    virtual void Resize(int width, int height) override
    {
        //code
        if (height == 0)
            height = 1;

        //Viewport/window of OpenGL	- Parameters x,y,width,height
        glViewport(0, 0, width, height);

        //Set projection matrix
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        if (width <= height)
        {
            auto factor = 1.0f*(((GLfloat)height) / ((GLfloat)width));
            glOrtho(-1.0f, 1.0f, -1 * factor, factor, -1.0f, 1.0f);
        }
        else
        {
            auto factor = 1.0f*(((GLfloat)width) / ((GLfloat)height));
            glOrtho(-1 * factor, factor, -1.0f, 1.0f, -1.0f, 1.0f);
        }
    }

    LRESULT WndProcPost(util::WindowProps&, UINT iMsg, WPARAM wParam, LPARAM lParam) override
    {
        return LRESULT(0);
    }

    virtual LRESULT WndProcPre(util::WindowProps&, UINT, WPARAM, LPARAM) override
    {
        return LRESULT();
    }
public:
    MyOglWindow(TCHAR * applicationName, UINT x, UINT y, UINT width, UINT height) : PI(4 * atan(1)), OglWindow(applicationName, x, y, width, height)
    {
    }

    // Inherited via OglWindow
    virtual void Update() override
    {
    }
};

int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
//int main()
{
    TCHAR AppName[] = TEXT("Kundali");
    MyOglWindow myOglWnd(AppName, WIN_X, WIN_Y, WIN_WIDTH, WIN_HEIGHT);
    myOglWnd.InitAndShowWindow();
    myOglWnd.AllowOutOfFocusRender(true);
    myOglWnd.RunGameLoop();
    return 0;
}