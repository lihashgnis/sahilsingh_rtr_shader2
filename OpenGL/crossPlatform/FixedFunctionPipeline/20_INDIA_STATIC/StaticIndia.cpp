﻿#include "UtilCrossPlatform.hpp"
#include <GL/gl.h>
#include <GL/glu.h>
#include <cmath>
#include <tuple>
#include <string>

//STRICT to prevent explicit typecasting
#define STRICT

constexpr UINT WIN_WIDTH = 1920;
constexpr UINT WIN_HEIGHT = 1080;
constexpr UINT WIN_X = 100;
constexpr UINT WIN_Y = 100;
UINT NUM_PTS = 100;
constexpr double CHAR_SPACING = 0.2;
constexpr double WORD_SPACING = 0;

class MyOglWindow : public util::OglWindow
{
    const double PI;
    // Inherited via OglWindow
    virtual const char * GetMessageMap() override
    {
        return nullptr;
    }
    virtual void Display() override
    {
        glClear(GL_COLOR_BUFFER_BIT);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        
        //util::TextHelper th(WORD_SPACING, CHAR_SPACING, util::ColorWheel::COLORS::INDIA_SAFRON, util::ColorWheel::COLORS::INDIA_GREEN);
        util::TextHelper th(WORD_SPACING, CHAR_SPACING, RGBA(0xFF, 0x99, 0x33, 255), RGBA(0x13, 0x88, 0x08, 255), 255);
        th.SetFontHeight(0.5);
        std::wstring str{ L"INDIא" };
        auto sszx = th.GetOnScreenSize(str);
        auto sszy = th.GetMaxHeightCellSize(str);
        double posX = -sszx / 2.0;
        double posY = sszy.cy / 2.0;
        th.WriteText(str, util::DataTypes::POINT3Dd{ posX, posY, 0 });
        util::SwapBuffersCompat();
    }

    virtual void Resize(int width, int height) override
    {
        if (height == 0)
            height = 1;

        glViewport(0, 0, width, height);

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        if (width <= height)
        {
            auto factor = 1.0f*(((GLfloat)height) / ((GLfloat)width));
            glOrtho(-1.0f, 1.0f, -1 * factor, factor, -1.0f, 1.0f);
        }
        else
        {
            auto factor = 1.0f*(((GLfloat)width) / ((GLfloat)height));
            glOrtho(-1 * factor, factor, -1.0f, 1.0f, -1.0f, 1.0f);
        }
    }
    virtual LRESULT WndProcPre(util::WindowProps&, UINT iMsg, WPARAM wParam, LPARAM lParam) override
    {
        switch (iMsg)
        {
        case WM_CHAR:
        {
            switch (wParam)
            {
            case 'f':
                //[[fallthrough]]
            case 'F':
                return LRESULT(-1);
                break;
            default:
                break;
            }
        }
        break;
        }
        return LRESULT();
    }

    LRESULT WndProcPost(util::WindowProps& props, UINT iMsg, WPARAM wParam, LPARAM lParam) override
    {
        auto that = props.GetOGLWindow();

        switch (iMsg)
        {
        case WM_KEYDOWN:
        {
            if (VK_ESCAPE == wParam)
            {
                that->DestroyWindow();
            }
        }
        break;
        }
        return LRESULT();
    }

public:
    MyOglWindow(TCHAR* applicationName, UINT x, UINT y, UINT width, UINT height) : PI(4 * atan(1)), OglWindow(applicationName, x, y, width, height)
    {
    }

    // Inherited via OglWindow
    virtual void Update() override
    {
    }
};

int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
    TCHAR AppName[] = TEXT("Static INDIA");
    MyOglWindow myOglWnd(AppName, WIN_X, WIN_Y, WIN_WIDTH, WIN_HEIGHT);
    myOglWnd.InitAndShowWindow();
    myOglWnd.EnableAlpha(true);
    myOglWnd.ToggleFullscreeen();
    myOglWnd.RunGameLoop();
    return 0;
}