Cross Platform
1. convert 
#include "../../../../99_AuxStuff/MyLibs/Utility/Util.hpp"
to
#include "Util.hpp"

2.convert 
    virtual LRESULT WndProcPost(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) override
	to
	LRESULT WndProcPost(util::WindowProps& props, UINT iMsg, WPARAM wParam, LPARAM lParam) override

3.
convert
virtual LRESULT WndProcPre(HWND, UINT iMsg, WPARAM wParam, LPARAM lParam) override
to
virtual LRESULT WndProcPre(util::WindowProps& props, UINT iMsg, WPARAM wParam, LPARAM lParam) override

4. change
MyOglWindow(TCHAR * className, UINT x, UINT y, UINT width, UINT height, HINSTANCE hInstance) : PI(4 * atan(1)), OglWindow(className, x, y, width, height, hInstance)
to
MyOglWindow(TCHAR * applicationName, UINT x, UINT y, UINT width, UINT height) : PI(4 * atan(1)), OglWindow(applicationName, x, y, width, height)

5. change
MyOglWindow myOglWnd(AppName, WIN_X, WIN_Y, WIN_WIDTH, WIN_HEIGHT, hInstance);
to
MyOglWindow myOglWnd(AppName, WIN_X, WIN_Y, WIN_WIDTH, WIN_HEIGHT);

6. 
change
#include <gl/GL.h>
#include <gl/GLU.h>
to 
#include <GL/gl.h>
#include <GL/glu.h>

7.
instead of 
SwapBuffers(m_Hdc);
use
util::SwapBuffersCompat();

8.
convert 
auto that = (OglWindow*)GetProp(hwnd, PARENT_OBJECT);
to
auto that = props.GetOGLWindow();

4. convert any platform specific function which uses HWND, to one which uses util::WindowProps

Note that things work well on 1920x1080, but may not work well for other resolutions/aspect ratios

Neutral APIs-
DestroyWindowLinux
ToggleFullscreeenLinux
//InitializeLinux

GOTCHAS
1. If any keydown even is not working - check if it is handled in LinuxCompas.cpp:BOOL TranslateMessage(_In_ MSG* lpMsg):case KeyPress

TESTS
1. DestroyWindowLinux() - should land in destroywindow in window proc 