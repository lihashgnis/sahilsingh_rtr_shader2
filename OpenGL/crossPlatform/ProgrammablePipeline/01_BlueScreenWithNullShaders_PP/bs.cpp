#include "UtilCrossPlatform.hpp"
#include <GL/glew.h>
#include <GL/gl.h>
#include <cmath>

constexpr UINT WIN_WIDTH = 800;
constexpr UINT WIN_HEIGHT = 800;
constexpr UINT WIN_X = 100;
constexpr UINT WIN_Y = 100;

enum : util::DataTypes::ProgramID {
    defaultProgram
};

class MyOglWindow : public util::OglWindow
{
    const double PI;
    // Inherited via OglWindow
    virtual const char* GetMessageMap() override
    {
        return nullptr;
    }

    virtual void Display() override
    {
        SetCurrentShaderProgram(defaultProgram);
        glUseProgram(GetShaderProgram().value());
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            glMatrixMode(GL_MODELVIEW);
            glLoadIdentity();
            util::SwapBuffersCompat();
        glUseProgram(0);
    }

    virtual void Resize(int width, int height) override
    {
        if (height == 0)
            height = 1;

        glViewport(0, 0, width, height);
    }

    LRESULT WndProcPost(util::WindowProps&, UINT, WPARAM, LPARAM) override
    {
        return LRESULT(0);
    }

    virtual LRESULT WndProcPre(util::WindowProps&, UINT, WPARAM, LPARAM) override
    {
        return LRESULT();
    }
public:
    MyOglWindow(TCHAR* className, UINT x, UINT y, UINT width, UINT height) : PI(4 * atan(1)), OglWindow(className, x, y, width, height)
    {
    }

    // Inherited via OglWindow
    virtual void Update() override
    {
    }
};

const char* vertextShader = 
#include "OpenGL/Shaders/VertexShaders/{00000000-0000-0000-0000-000000000000}.vert"
const char* fragmentShader = 
#include "OpenGL/Shaders/FragmentShaders/{00000000-0000-0000-0000-000000000000}.frag"

int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
    TCHAR AppName[] = TEXT("Programmable Pipeline Helloworld");
    MyOglWindow myOglWnd(AppName, WIN_X, WIN_Y, WIN_WIDTH, WIN_HEIGHT);
    myOglWnd.InitAndShowWindow(INIT_MASK_ENABLE_DEPTH | INIT_MASK_ENABLE_PROGRAMMABLE_PIPELINE);
    glClearColor(0, 0, 1, 1);
    myOglWnd.CreateNewShaderProgram(defaultProgram);
    myOglWnd.SetVertexShaderCode(vertextShader);
    myOglWnd.SetFragmentShaderCode(fragmentShader);
    myOglWnd.InitShaders();
    myOglWnd.RunGameLoop();
    return 0;
}