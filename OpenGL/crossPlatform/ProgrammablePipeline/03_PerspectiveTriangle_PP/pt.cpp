#include "UtilCrossPlatform.hpp"
#include <GL/glew.h>
#include <GL/gl.h>
#include "vmath.h"

using namespace vmath;

constexpr UINT WIN_WIDTH = 800;
constexpr UINT WIN_HEIGHT = 800;
constexpr UINT WIN_X = 100;
constexpr UINT WIN_Y = 100;

const char* UNIFORM_MVP_SHADER_NAME = "u_mvp_matrix";

std::vector<GLfloat> triangleVertices = {
      0.0,   1.0,    0.0,
     -1.0,  -1.0,    0.0,
      1.0,  -1.0,    0.0
};

enum : util::DataTypes::ProgramID {
    defaultProgram
};

//creating enum so that object IDs auto increment
enum : util::DataTypes::ObjectID {
    triangleID,
};

class MyOglWindow : public util::OglWindow
{
    vmath::mat4 m_perspectiveProjectionMatrix;
    const double PI;
    // Inherited via OglWindow
    virtual const char* GetMessageMap() override
    {
        return nullptr;
    }

    virtual void Display() override
    {
        auto err = glGetError();
        SetCurrentShaderProgram(defaultProgram);
        glUseProgram(GetShaderProgram().value());
        err = glGetError();
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        vmath::mat4 modelViewMatrix = translate(0.0f,0.0f,-3.0f), modelViewProjectionMatrix = vmath::mat4::identity();

        //do necessary transformations if any

        //do necessary matrix multiplication
        //modelViewMatrix =  modelViewMatrix * translateMatrix;
        modelViewProjectionMatrix = m_perspectiveProjectionMatrix * modelViewMatrix;

        //send matrices to shaders via uniforms
        glUniformMatrix4fv(GetUniformLocation(UNIFORM_MVP_SHADER_NAME), 1, GL_FALSE, modelViewProjectionMatrix);
        err = glGetError();

        //bind with textures if any

        //Draw scene
        DrawObject(triangleID);
        err = glGetError();

        util::SwapBuffersCompat();
        glUseProgram(0);
    }

    virtual void Resize(int width, int height) override
    {
        if (height == 0)
            height = 1;
        glViewport(0, 0, width, height);

        auto factor = ((GLfloat)width) / ((GLfloat)height);
        m_perspectiveProjectionMatrix = perspective(45.0, factor, 0.1, 100);
    }

    LRESULT WndProcPost(util::WindowProps&, UINT, WPARAM, LPARAM) override
    {
        return LRESULT(0);
    }

    virtual LRESULT WndProcPre(util::WindowProps&, UINT, WPARAM, LPARAM) override
    {
        return LRESULT();
    }
public:
    MyOglWindow(TCHAR* className, UINT x, UINT y, UINT width, UINT height) : PI(4 * atan(1)), OglWindow(className, x, y, width, height)
    {
        m_perspectiveProjectionMatrix = vmath::mat4::identity();
    }

    // Inherited via OglWindow
    virtual void Update() override
    {
    }
};

const char* vertextShader =
#include "OpenGL/Shaders/VertexShaders/{1D19A7B9-B345-491E-B7D7-E41FDFF697AA}.vert"
const char* fragmentShader =
#include "OpenGL/Shaders/FragmentShaders/{C2038141-65DA-4D11-8351-304132D36416}.frag"


int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
    TCHAR AppName[] = TEXT("Programmable Pipeline Perspective triangle");
    MyOglWindow myOglWnd(AppName, WIN_X, WIN_Y, WIN_WIDTH, WIN_HEIGHT);
    myOglWnd.InitAndShowWindow(INIT_MASK_ENABLE_DEPTH | INIT_MASK_ENABLE_PROGRAMMABLE_PIPELINE);
    myOglWnd.AllowOutOfFocusRender(true);
    auto err = glGetError();
    glClearColor(0, 0, 1, 1);

    myOglWnd.CreateNewShaderProgram(defaultProgram);
    myOglWnd.SetVertexShaderCode(vertextShader);
    myOglWnd.SetFragmentShaderCode(fragmentShader);

    myOglWnd.CreateNewObject(triangleID, util::ShaderCodesContainer::ObjectDrawType::DT_DrawArrays_Triangles);
    myOglWnd.AddAttribute(triangleID, util::ShaderCodesContainer::VertextAttributesEnum::AMC_ATTRIBUTE_POSITION, "vPosition", triangleVertices, 0, 3);
    myOglWnd.AddUniform(UNIFORM_MVP_SHADER_NAME);
    myOglWnd.InitShaders();
    myOglWnd.RunGameLoop();
    return 0;
}