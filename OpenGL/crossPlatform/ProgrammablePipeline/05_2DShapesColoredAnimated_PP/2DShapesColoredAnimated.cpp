#include "UtilCrossPlatform.hpp"
#include <GL/glew.h>
#include <GL/gl.h>
#include "vmath.h"
#include <vector>
#include "ProgrammablePipeline.h"

using namespace vmath;

constexpr UINT WIN_WIDTH = 800;
constexpr UINT WIN_HEIGHT = 800;
constexpr UINT WIN_X = 100;
constexpr UINT WIN_Y = 100;

const char* UNIFORM_MVP_SHADER_NAME = "u_mvp_matrix";

std::vector<GLfloat> triangleVertices = {
      0.0,   1.0,    0.0,
     -1.0,  -1.0,    0.0,
      1.0,  -1.0,    0.0
};

std::vector<GLfloat> triangleColor = {
      1.0,   0.0,    0.0,
      0.0,   1.0,    0.0,
      0.0,   0.0,    1.0
};

std::vector<GLfloat> rectangleVertices = {
     -1.0,   1.0,    0.0,
     -1.0,  -1.0,    0.0,
      1.0,  -1.0,    0.0,
      1.0,   1.0,    0.0,
};

std::vector<GLfloat> rectangleColor = {
      1.0,   0.0,    0.0,
      0.0,   1.0,    0.0,
      0.0,   0.0,    1.0,
      1.0,   0.0,    0.0
};


enum : util::DataTypes::ProgramID {
    defaultProgram
};


//creating enum so that object IDs auto increment
enum : util::DataTypes::ObjectID {
    triangleID,
    rectangleID
};

class MyOglWindow : public util::OglWindow
{
    PP::MatrixUniform4 m_perspectiveProjectionMatrix;
    float m_rotAngle = 0;//for rotating the 2D shapes
    const double PI;
    // Inherited via OglWindow
    virtual const char* GetMessageMap() override
    {
        return nullptr;
    }

    virtual void Display() override
    {
        auto err = glGetError();
        SetCurrentShaderProgram(defaultProgram);
        glUseProgram(GetShaderProgram().value());
        err = glGetError();
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        PP::MatrixUniform4 modelViewMatrix, modelViewProjectionMatrix(GetUniformLocation(UNIFORM_MVP_SHADER_NAME));

        /*TRIANGLE*/
        //do necessary transformations if any
        modelViewMatrix.Translate(-2, 0, -9);
        modelViewMatrix.Rotate(m_rotAngle, 0, 1, 0);
        

        //do necessary matrix multiplication
        modelViewProjectionMatrix.LoadMatrixData(m_perspectiveProjectionMatrix * modelViewMatrix);

        //send matrices to shaders via uniforms
        modelViewProjectionMatrix.Flush();
        err = glGetError();


        //bind with textures if any

        //Draw scene
        DrawObject(triangleID);
        err = glGetError();

        /*RECTANGLE*/
        modelViewMatrix.LoadIdentiy();
        modelViewProjectionMatrix.LoadIdentiy();
        modelViewMatrix.Translate(2.0f, 0.0f, -9.0f);
        modelViewMatrix.Rotate(m_rotAngle, 1, 0, 0);
        modelViewProjectionMatrix.LoadMatrixData(m_perspectiveProjectionMatrix * modelViewMatrix);

        //send matrices to shaders via uniforms
        modelViewProjectionMatrix.Flush();
        err = glGetError();

        DrawObject(rectangleID);

        util::SwapBuffersCompat();
        glUseProgram(0);
    }

    virtual void Resize(int width, int height) override
    {
        if (height == 0)
            height = 1;
        glViewport(0, 0, width, height);

        auto factor = ((GLfloat)width) / ((GLfloat)height);
        m_perspectiveProjectionMatrix.LoadMatrixData(PP::MatrixUniform4::Perspective(45.0, factor, 0.1, 100));
    }

    LRESULT WndProcPost(util::WindowProps&, UINT, WPARAM, LPARAM) override
    {
        return LRESULT(0);
    }

    virtual LRESULT WndProcPre(util::WindowProps&, UINT, WPARAM, LPARAM) override
    {
        return LRESULT();
    }
public:
    MyOglWindow(TCHAR* className, UINT x, UINT y, UINT width, UINT height) : PI(4 * atan(1)), OglWindow(className, x, y, width, height)
    {
        m_perspectiveProjectionMatrix.LoadIdentiy();
    }

    // Inherited via OglWindow
    virtual void Update() override
    {
        m_rotAngle += 0.1;
        if (m_rotAngle >= 360.0)
        {
            m_rotAngle = 0.0;
        }
    }
};

const char* vertextShader =
#include "OpenGL/Shaders/VertexShaders/{F0B737D1-E415-4222-B97F-A058F8ECBF80}.vert"
const char* fragmentShader =
#include "OpenGL/Shaders/FragmentShaders/{FAC70A64-D9ED-46F7-9EC6-534E9642FB2A}.frag"


int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
    TCHAR AppName[] = TEXT("Programmable Pipeline 2 2D shapes Animated");
    MyOglWindow myOglWnd(AppName, WIN_X, WIN_Y, WIN_WIDTH, WIN_HEIGHT);
    myOglWnd.InitAndShowWindow(INIT_MASK_ENABLE_DEPTH | INIT_MASK_ENABLE_PROGRAMMABLE_PIPELINE);
    myOglWnd.AllowOutOfFocusRender(true);
    auto err = glGetError();
    glClearColor(0, 0, 1, 1);

    myOglWnd.CreateNewShaderProgram(defaultProgram);
    myOglWnd.SetVertexShaderCode(vertextShader);
    myOglWnd.SetFragmentShaderCode(fragmentShader);

    /*TRIANGLE*/
    myOglWnd.CreateNewObject(triangleID, util::ShaderCodesContainer::ObjectDrawType::DT_DrawArrays_Triangles);
    myOglWnd.AddAttribute(triangleID, util::ShaderCodesContainer::VertextAttributesEnum::AMC_ATTRIBUTE_POSITION, "vPosition", triangleVertices, 0, 3);
    myOglWnd.AddAttribute(triangleID, util::ShaderCodesContainer::VertextAttributesEnum::AMC_ATTRIBUTE_COLOR, "vColor", triangleColor, 0, 3);

    /*RECTANGLE*/
    myOglWnd.CreateNewObject(rectangleID, util::ShaderCodesContainer::ObjectDrawType::DT_DrawArrays_TriangleFan);
    myOglWnd.AddAttribute(rectangleID, util::ShaderCodesContainer::VertextAttributesEnum::AMC_ATTRIBUTE_POSITION, "vPosition", rectangleVertices, 0, 3);
    myOglWnd.AddAttribute(rectangleID, util::ShaderCodesContainer::VertextAttributesEnum::AMC_ATTRIBUTE_COLOR, "vColor", rectangleColor, 0, 3);

    myOglWnd.AddUniform(UNIFORM_MVP_SHADER_NAME);
    myOglWnd.InitShaders();
    myOglWnd.RunGameLoop();
    return 0;
}