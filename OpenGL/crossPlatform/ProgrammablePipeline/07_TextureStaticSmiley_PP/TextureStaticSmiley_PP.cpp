#include "UtilCrossPlatform.hpp"
#include <GL/glew.h>
#include <GL/gl.h>
#include "vmath.h"
#include <vector>
#include "ProgrammablePipeline.h"
#include "Textures.h"

using namespace vmath;

constexpr UINT WIN_WIDTH = 800;
constexpr UINT WIN_HEIGHT = 800;
constexpr UINT WIN_X = 100;
constexpr UINT WIN_Y = 100;

GLuint texture_smiley = 0;

const char* UNIFORM_MVP_SHADER_NAME = "u_mvp_matrix";
const char* UNIFORM_TEXTURE_SAMPLER = "u_sampler";

const char imagePath[] = "smiley_512x512_flip.png";

enum : util::DataTypes::ProgramID {
    defaultProgram
};

//creating enum so that object IDs auto increment
enum : util::DataTypes::ObjectID {
    rectangleID
};

const std::vector<GLfloat> textureCoords = {
    0,0,
    1,0,
    1,1,
    0,1
};

const std::vector<GLfloat> rectangleCoords = {
    -1,-1, 0,
     1,-1, 0,
     1, 1, 0,
    -1, 1, 0
};

class MyOglWindow : public util::OglWindow
{
    PP::MatrixUniform4 m_perspectiveProjectionMatrix;
    const double PI;
    // Inherited via OglWindow
    virtual const char* GetMessageMap() override
    {
        return nullptr;
    }

    virtual void Display() override
    {
        auto err = glGetError();
        SetCurrentShaderProgram(defaultProgram);
        glUseProgram(GetShaderProgram().value());
        err = glGetError();
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        PP::MatrixUniform4 modelViewMatrix, modelViewProjectionMatrix(GetUniformLocation(UNIFORM_MVP_SHADER_NAME));

        //do necessary transformations if any
        modelViewMatrix.Translate(0, 0, -3);

        //do necessary matrix multiplication
        modelViewProjectionMatrix.LoadMatrixData(m_perspectiveProjectionMatrix * modelViewMatrix);

        //send matrices to shaders via uniforms
        modelViewProjectionMatrix.Flush();
        err = glGetError();

        //bind with vao
        glBindVertexArray(GetObjectVao(rectangleID));
        err = glGetError();

        //bind with textures if any

        //Draw scene
        {
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, texture_smiley);

            //connect to texture unit 0
            glUniform1i(GetUniformLocation(UNIFORM_TEXTURE_SAMPLER), 0);

            DrawObject(rectangleID);
        }

        util::SwapBuffersCompat();
        glUseProgram(0);
    }

    virtual void Resize(int width, int height) override
    {
        if (height == 0)
            height = 1;
        glViewport(0, 0, width, height);

        auto factor = ((GLfloat)width) / ((GLfloat)height);
        m_perspectiveProjectionMatrix.LoadMatrixData(PP::MatrixUniform4::Perspective(45.0, factor, 0.1, 100));
    }

    LRESULT WndProcPost(util::WindowProps&, UINT, WPARAM, LPARAM) override
    {
        return LRESULT(0);
    }

    virtual LRESULT WndProcPre(util::WindowProps&, UINT, WPARAM, LPARAM) override
    {
        return LRESULT();
    }
public:
    MyOglWindow(TCHAR* className, UINT x, UINT y, UINT width, UINT height) : PI(4 * atan(1)), OglWindow(className, x, y, width, height)
    {
        m_perspectiveProjectionMatrix.LoadIdentiy();
    }

    // Inherited via OglWindow
    virtual void Update() override
    {

    }
};

const char* vertextShader =
#include "OpenGL/Shaders/VertexShaders/{12514A14-D0B9-4C23-92A8-06991E03A37D}.vert"
const char* fragmentShader =
#include "OpenGL/Shaders/FragmentShaders/{36599102-BFFA-4413-9859-51F1513D832D}.frag"


int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
    TCHAR AppName[] = TEXT("Programmable Pipeline: Texture Static Simley");
    MyOglWindow myOglWnd(AppName, WIN_X, WIN_Y, WIN_WIDTH, WIN_HEIGHT);
    myOglWnd.InitAndShowWindow(INIT_MASK_ENABLE_DEPTH | INIT_MASK_ENABLE_PROGRAMMABLE_PIPELINE | INIT_MASK_DUMP_ATTRIBUTES);
    myOglWnd.AllowOutOfFocusRender(true);
    auto err = glGetError();
    glClearColor(0, 0, 0, 1);

    myOglWnd.CreateNewShaderProgram(defaultProgram);
    myOglWnd.SetVertexShaderCode(vertextShader);
    myOglWnd.SetFragmentShaderCode(fragmentShader);

    auto rectangleCoordsTesselated = util::QuadToTriangle(rectangleCoords);
    myOglWnd.CreateNewObject(rectangleID, util::ShaderCodesContainer::ObjectDrawType::DT_DrawArrays_Triangles);
    myOglWnd.AddAttribute(rectangleID, util::ShaderCodesContainer::VertextAttributesEnum::AMC_ATTRIBUTE_POSITION, "vPosition", rectangleCoordsTesselated, 0, 3);

    auto textureCoordsTesselated = util::QuadToTriangle(textureCoords, 2);
    myOglWnd.AddAttribute(rectangleID, util::ShaderCodesContainer::VertextAttributesEnum::AMC_ATTRIBUTE_TEXTCOORD0, "vTexCoord", textureCoordsTesselated, 0, 2);
    util::Textures::LoadTexture(&texture_smiley, imagePath, util::Textures::TextureType::PNG);
    myOglWnd.AddUniform(UNIFORM_MVP_SHADER_NAME);
    myOglWnd.AddUniform(UNIFORM_TEXTURE_SAMPLER);
    myOglWnd.InitShaders();
    myOglWnd.RunGameLoop();
    return 0;
}