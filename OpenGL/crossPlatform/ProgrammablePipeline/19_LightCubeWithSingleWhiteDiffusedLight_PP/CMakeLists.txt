# CMakeList.txt : CMake project for 19_LightCubeWithSingleWhiteDiffusedLight_PP, include source and define
# project specific logic here.
#

cmake_minimum_required (VERSION 3.8)
set(PROJ_NAME "19_LightCubeWithSingleWhiteDiffusedLight_PP")
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

include_directories(${CMAKE_SOURCE_DIR}/99_AuxStuff/MyLibs/UtilityCrossPlatform)

set(SRC_FILES 
LightCubeWithSingleWhiteDiffusedLight_PP.cpp
)

set(RESOURCE_FILES
)

source_group("SOURCES" FILES ${SRC_FILES})

source_group("RESOURCES" FILES ${RESOURCE_FILES})

add_executable ("${PROJ_NAME}" ${SRC_FILES})

SET_TARGET_PROPERTIES("${PROJ_NAME}" PROPERTIES FOLDER "02_OpenGL_PP")

target_link_libraries("${PROJ_NAME}" UtilityCrossPlatform)


if(WIN32 OR CYGWIN)
    target_sources("${PROJ_NAME}" PRIVATE "${RESOURCE_FILES}")
    add_custom_command(TARGET "${PROJ_NAME}"
    POST_BUILD
    WORKING_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}"
    COMMAND if NOT EXIST "glew32.dll" (echo "glew32.dll does not exist. Creating symlink" && mklink glew32.dll "..\\..\\..\\..\\..\\..\\..\\99_AuxStuff\\3rdPartyLibs\\glew-2.1.0-win32\\glew-2.1.0\\bin\\Release\\x64\\glew32.dll") ELSE (echo "glew32.dll exists, not creating symlink")
    )
endif()