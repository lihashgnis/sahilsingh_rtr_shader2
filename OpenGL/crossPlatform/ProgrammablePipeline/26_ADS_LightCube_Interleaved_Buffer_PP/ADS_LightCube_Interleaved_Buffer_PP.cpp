#include "UtilCrossPlatform.hpp"
#include <GL/glew.h>
#include <GL/gl.h>
#include "vmath.h"
#include <vector>
#include "ProgrammablePipeline.h"
#include "Textures.h"

using namespace vmath;

constexpr UINT WIN_WIDTH = 800;
constexpr UINT WIN_HEIGHT = 800;
constexpr UINT WIN_X = 100;
constexpr UINT WIN_Y = 100;

GLuint texture_smiley = 0;

const char* UNIFORM_M_SHADER_NAME = "u_m_matrix";
const char* UNIFORM_V_SHADER_NAME = "u_v_matrix";
const char* UNIFORM_P_SHADER_NAME = "u_p_matrix";

enum : util::DataTypes::ProgramID {
    defaultProgram
};

enum : int
{
	Light0 = 0,
	Light1,
	Light2
};

//creating enum so that object IDs auto increment
enum : util::DataTypes::ObjectID {
    cubeID
};

/*
                        P4                     P7
                         ---------------------------         -
                       -/|                       -/|
                      /  |                      /  |
                    -/   |                    -/   |
                   /     |                   /     |
              P0 -/      |             P3  -/      |
                /--------|----------------/        |
                |        |                |        |
                |        |                |        |
                |        |                |        |
                |     P5 |                |     P6 |
                |        ---------------------------
                |     --/                 |      -/
                |     /                   |     /
                |   -/                    |   -/
                |  /                      |  /       -
             P1 |-/                    P2 |-/
                /-------------------------/
*/

constexpr util::DataTypes::POINT3Df g_pts_cube[] = {
    {-1.0f,1.0f,1.0f}, {-1.0f,-1.0f,1.0f}, {1.0f,-1.0f,1.0f}, {1.0f,1.0f,1.0f},     //front face: P0 to P3
    {-1.0f,1.0f,-1.0f}, {-1.0f,-1.0f,-1.0f}, {1.0f,-1.0f,-1.0f}, {1.0f,1.0f,-1.0f}  //back face : P4 to P7
};

#define VQ(i) g_pts_cube[i].x, g_pts_cube[i].y, g_pts_cube[i].z //Vertex Quads
#define Q(a,b,c,d) VQ(a), VQ(b), VQ(c), VQ(d),                         //Quad
#define QC(r,g,b)  r,g,b, r,g,b, r,g,b, r,g,b,

const std::vector<GLfloat> cubeVertices = {
Q(0, 3, 7, 4) //TOP
Q(5, 1, 2, 6) //Bottom
Q(0, 1, 2, 3) //Front
Q(4, 5, 6, 7) //Back
Q(3, 2, 6, 7) //Right
Q(0, 1, 5, 4) //Left
};

const std::vector<GLfloat> cubeNormals = {
QC(  0.0f,  1.0f,  0.0f) //TOP
QC(  0.0f, -1.0f,  0.0f) //Bottom
QC(  0.0f,  0.0f,  1.0f) //Front
QC(  0.0f,  0.0f, -1.0f) //Back
QC(  1.0f,  0.0f,  0.0f) //Right
QC( -1.0f,  0.0f,  0.0f) //Left
};

class MyOglWindow : public util::OglWindow
{
    PP::MatrixUniform4 m_perspectiveProjectionMatrix;
    bool m_bIsLightOn = false;
    bool m_bIsAnimationOn = false;
    float m_rotAngle = 0;//for rotating

    const double PI;
    // Inherited via OglWindow
    virtual const char* GetMessageMap() override
    {
        return nullptr;
    }

    virtual void Display() override
    {
        auto err = glGetError();

        SetCurrentShaderProgram(defaultProgram);
        glUseProgram(GetShaderProgram().value());
        err = glGetError();
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        PP::MatrixUniform4 modelMatrix(GetUniformLocation(UNIFORM_M_SHADER_NAME)), viewMatrix(GetUniformLocation(UNIFORM_V_SHADER_NAME)), projectionMatrix(GetUniformLocation(UNIFORM_P_SHADER_NAME));

        projectionMatrix.LoadMatrixData(m_perspectiveProjectionMatrix);
        //do necessary transformations if any
        modelMatrix.Translate(0, 0, -9);
        modelMatrix.Rotate(m_rotAngle, 1, 1, 1);

        //do necessary matrix multiplication

        //send matrices to shaders via uniforms
        modelMatrix.Flush();
        viewMatrix.Flush();
        projectionMatrix.Flush();
        err = glGetError();

		GlobalLightSwich(m_bIsLightOn);

        //bind with textures if any
        //Draw scene
        {
            DrawObject(cubeID);
        }
        err = glGetError();
        util::SwapBuffersCompat();
        glUseProgram(0);
    }

    virtual void Resize(int width, int height) override
    {
        if (height == 0)
            height = 1;
        glViewport(0, 0, width, height);

        auto factor = ((GLfloat)width) / ((GLfloat)height);
        m_perspectiveProjectionMatrix.LoadMatrixData(PP::MatrixUniform4::Perspective(45.0, factor, 0.1, 100));
    }

    LRESULT WndProcPost(util::WindowProps&, UINT iMsg, WPARAM wParam, LPARAM) override
    {
        switch (iMsg)
        {
        case WM_CHAR:
        {
            switch (wParam)
            {
            case 'l':
                [[fallthrough]];
            case 'L':
            {
                m_bIsLightOn = !m_bIsLightOn;
            }
            break;
            case 'a':
                [[fallthrough]];
            case 'A':
            {
                m_bIsAnimationOn = !m_bIsAnimationOn;
            }
            break;
            }
        }
        break;
        default:
            break;
        }
        return LRESULT(0);
    }

    virtual LRESULT WndProcPre(util::WindowProps&, UINT, WPARAM, LPARAM) override
    {
        return LRESULT();
    }
public:
    MyOglWindow(TCHAR* className, UINT x, UINT y, UINT width, UINT height) : PI(4 * atan(1)), OglWindow(className, x, y, width, height)
    {
        m_perspectiveProjectionMatrix.LoadIdentiy();
    }

    // Inherited via OglWindow
    virtual void Update() override
    {
        if (m_bIsAnimationOn)
        {
            m_rotAngle += 0.1;
            if (m_rotAngle >= 360.0)
            {
                m_rotAngle = 0.0;
            }
        }

    }
};

const char* vertextShader =
R"(
#version 140

out vec3 transformed_normal;
out vec3 light_direction_from_vertex;
out vec3 viewer_vector;
//out vec4 eye_coords;

uniform mat4 u_m_matrix;
uniform mat4 u_v_matrix;
uniform mat4 u_p_matrix;

uniform vec4 u_lightPosition_vec4;

//specular params
uniform vec3 u_LS_vec3;
uniform vec3 u_KS_vec3;
uniform float u_shininess;

//diffuse params
uniform vec3 u_LD_vec3;
uniform vec3 u_KD_vec3;

//ambient params
uniform vec3 u_LA_vec3;
uniform vec3 u_KA_vec3;

void main()
{
    light_direction_from_vertex = normalize(vec3(u_lightPosition_vec4 - eye_coords)); //variable name S in sir's code

    mat4 mv_matrix = u_v_matrix*u_m_matrix;
    mat3 normal_matrix = mat3(transpose(inverse((mv_matrix))));
    transformed_normal =   normalize(normal_matrix*vNormal);

    vec4 eye_coords = mv_matrix*vPosition;
    viewer_vector = normalize(-eye_coords.xyz);

    gl_Position = u_p_matrix*u_v_matrix*u_m_matrix*vPosition;
}

)";

const char* fragmentShader =
R"(
#version 140

in vec3 transformed_normal;
in vec3 light_direction_from_vertex;
in vec3 viewer_vector;
//in vec4 eye_coords;

uniform mat4 u_m_matrix;
uniform mat4 u_v_matrix;
uniform mat4 u_p_matrix;

uniform vec4 u_lightPosition_vec4;

//specular params
uniform vec3 u_LS_vec3;
uniform vec3 u_KS_vec3;
uniform float u_shininess;

//diffuse params
uniform vec3 u_LD_vec3;
uniform vec3 u_KD_vec3;

//ambient params
uniform vec3 u_LA_vec3;
uniform vec3 u_KA_vec3;


void main()
{
    vec3 ambient_color = u_LA_vec3 * u_KA_vec3;
    //vec3 light_direction_from_vertex = normalize(vec3(u_lightPosition_vec4 - eye_coords)); //variable name S in sir's code
    light_direction_from_vertex = normalize(light_direction_from_vertex);
    transformed_normal = normalize(transformed_normal);
    vec3 diffuse_color = u_LD_vec3 * u_KD_vec3 * max(dot(light_direction_from_vertex, transformed_normal), 0.0f);

    //calculate specular
    vec3 reflection_vector = normalize(reflect(-light_direction_from_vertex, transformed_normal));
    //vec3 viewer_vector = normalize(-eye_coords.xyz);
    viewer_vector = normalize(viewer_vector);
    vec3 specular_color = u_LS_vec3 * u_KS_vec3 * pow(max(dot(reflection_vector, viewer_vector), 0), u_shininess + 1);//adding 1 to avoid 0^0 indertminate form - https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/pow.xhtml

    vec3 light_color = ambient_color + diffuse_color + specular_color;
}
)";


int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
    TCHAR AppName[] = TEXT("Programmable Pipeline: Cube With Single White Diffused Light");
    MyOglWindow myOglWnd(AppName, WIN_X, WIN_Y, WIN_WIDTH, WIN_HEIGHT);
    //myOglWnd.InitAndShowWindow(INIT_MASK_ENABLE_DEPTH | INIT_MASK_ENABLE_PROGRAMMABLE_PIPELINE | INIT_MASK_DUMP_ATTRIBUTES | INIT_XWINDOWS_DISABLE_ADVANCED_FB_CONFIG_INIT, 4, 5);
    myOglWnd.InitAndShowWindow(INIT_MASK_ENABLE_DEPTH | INIT_MASK_ENABLE_PROGRAMMABLE_PIPELINE | INIT_MASK_DUMP_ATTRIBUTES | INIT_XWINDOWS_DISABLE_ADVANCED_FB_CONFIG_INIT, 3, 1);
    myOglWnd.AllowOutOfFocusRender(true);
    auto err = glGetError();
    glClearColor(0, 0, 1, 1);

    myOglWnd.CreateNewShaderProgram(defaultProgram, true, 2);
    //myOglWnd.CreateNewShaderProgram(defaultProgram, true, 1);
    util::ProgrammablePipeline::LightProps props0({0,0,2,1}, {0,0,0}, {1,0,0}, {0,0,0});
    util::ProgrammablePipeline::LightProps props1({0,2,-8,1}, {0,0,0}, {0,1,0}, {0,0,0});
    myOglWnd.ConfigureLight(Light0, props0);
    myOglWnd.ConfigureLight(Light1, props1);
    myOglWnd.EnableLight(Light0, true);
    myOglWnd.EnableLight(Light1, true);
    myOglWnd.GlobalLightSwich(true);

    myOglWnd.SetVertexShaderCode(vertextShader);
    myOglWnd.SetFragmentShaderCode(fragmentShader);

    ASSERT(cubeNormals.size() == cubeVertices.size(), "dimentions must match");

    auto cubeVerticesTesselated = util::QuadToTriangle(cubeVertices);
    auto cubeNormalsTesselated = util::QuadToTriangle(cubeNormals);

	util::ProgrammablePipeline::ObjectLightProps objLightProps;
    objLightProps.m_KD = {1, 1, 1};
    myOglWnd.CreateNewObject(cubeID, util::ShaderCodesContainer::ObjectDrawType::DT_DrawArrays_Triangles, objLightProps);
    myOglWnd.AddAttribute(cubeID, util::ShaderCodesContainer::VertextAttributesEnum::AMC_ATTRIBUTE_POSITION, "vPosition", cubeVerticesTesselated, 0, 3);
    myOglWnd.AddAttribute(cubeID, util::ShaderCodesContainer::VertextAttributesEnum::AMC_ATTRIBUTE_NORMAL, "vNormal", cubeNormalsTesselated, 0, 3);

    myOglWnd.AddUniform(UNIFORM_M_SHADER_NAME);
    myOglWnd.AddUniform(UNIFORM_V_SHADER_NAME);
    myOglWnd.AddUniform(UNIFORM_P_SHADER_NAME);

    myOglWnd.InitShaders();
    myOglWnd.RunGameLoop();
    return 0;
}