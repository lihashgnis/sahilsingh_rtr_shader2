#include<iostream>
#include<iostream>
#include<stdlib.h>
#include<memory.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

using namespace std;

//global variables
bool bFullscreen=false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;

int giWindowWidth=800;
int giWindowHeight=600;

//function prototypes
void CreateWindow();
void ToggleFullscreen();
void uninitialize();

int main()
{
int winWidth = giWindowWidth;
int winHeight=giWindowHeight;

//code
CreateWindow();

//Message Loop
XEvent event;
KeySym keysym;

while(true)
{
	XNextEvent(gpDisplay, &event);
	switch(event.type)
	{
		case MapNotify:
			break;
		case KeyPress:
			keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode,0,0);
			switch(keysym)
			{
				case XK_Escape:
					uninitialize();
					exit(0);
					break;
				case XK_F:
				case XK_f:
					if(bFullscreen==false)
					{
						ToggleFullscreen();
						bFullscreen=true;
					}
					else
					{
						ToggleFullscreen();
						bFullscreen=false;
					}
					break;
				default:
					break;
			}
			break;
		case ButtonPress:
			{
				switch(event.xbutton.button)
				{
					case 1:// LB Down
						break;
					case 2://MB Down
						break;
					case 3://RB Down
						break;
					default:
						break;
				}
			}
			break;
		case MotionNotify://Mouse move
			break;
		case ConfigureNotify://Resize
			winWidth=event.xconfigure.width;
			winHeight=event.xconfigure.height;
			break;
		case Expose://WM_PAINT
			break;
		case DestroyNotify://WM_CLOSE
			break;

		case 33:
			uninitialize();
			exit(0);
			break;
		default:
			break;
	}
	}
uninitialize();
return 0;
}

void CreateWindow()
{
	//variables
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int defaultDepth;
	int styleMask;

	gpDisplay = XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		cout<<"Unable to open X display, exiting...";
		uninitialize();
		exit(1);
	}

	defaultScreen = XDefaultScreen(gpDisplay);
	defaultDepth = XDefaultDepth(gpDisplay, defaultScreen);
	gpXVisualInfo = new XVisualInfo;
	if(gpXVisualInfo==nullptr)
	{
		cerr<<"memory error, exiting...";
		uninitialize();
		exit(1);
	}

	XMatchVisualInfo(gpDisplay, defaultScreen, defaultDepth, TrueColor, gpXVisualInfo);	
	if(gpXVisualInfo==NULL)
	{
		cerr<<"Unable to get matching visual, exiting...";
		uninitialize();
		exit(1);
	}

	winAttribs.border_pixel = 0;
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,RootWindow(gpDisplay, gpXVisualInfo->screen), gpXVisualInfo->visual, AllocNone);
	gColormap=winAttribs.colormap;
	winAttribs.background_pixel=BlackPixel(gpDisplay, defaultScreen);
	winAttribs.event_mask = ExposureMask|VisibilityChangeMask|ButtonPressMask|KeyPressMask|PointerMotionMask|StructureNotifyMask;
	styleMask = CWBorderPixel|CWBackPixel|CWEventMask|CWColormap;

	gWindow = XCreateWindow(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen),0,0,giWindowWidth,giWindowHeight,0,gpXVisualInfo->depth, InputOutput, gpXVisualInfo->visual, styleMask, &winAttribs);
	if(!gWindow)
	{
		cerr<<"Failed to create main window";
		uninitialize();
		exit(1);
	}

	XStoreName(gpDisplay, gWindow, "First X Window");

	Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete,1);

	XMapWindow(gpDisplay, gWindow);
}

void ToggleFullscreen()
{
	//variables
	Atom wm_state;
	Atom fullscreen;
	XEvent xev = {0};

	//code
	wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
	memset(&xev,0,sizeof(xev));

	xev.type = ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.message_type=wm_state;
	xev.xclient.format=32;
	xev.xclient.data.l[0]=bFullscreen?0:1;
	fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1]=fullscreen;
	XSendEvent(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen), False, StructureNotifyMask, &xev);
}

void uninitialize()
{
	if(gWindow)
	{
		XDestroyWindow(gpDisplay, gWindow);
		gWindow=NULL;
	}

	if(gColormap)
	{
		XFreeColormap(gpDisplay, gColormap);
		gColormap = NULL;
	}

	if(gpXVisualInfo)
	{
		delete gpXVisualInfo;
		gpXVisualInfo = nullptr;
	}

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}
}
