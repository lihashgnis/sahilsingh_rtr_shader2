﻿# Assignments 

# Win32 Basics
1. **Full Screen**  
1. **Centre Window on desktop**  
**output gif:**  
![Centre Window on desktop GIF](1_Basics/Output_ScreenCapture/3_centreWindowOnDesktop.gif)
**Info:**  
 A simple program to centre window on the desktop when c is pressed, and to revert back to original position when c is pressed again.
1. **TextColor**  
**output gif:**  
![An exercise in text color from project 4_TextColor](1_Basics/Output_ScreenCapture/4_TextColor.gif)
**Info:**  
An excersize in DrawText(), SetBkColor(), SetTextColor() GDI functions.
WM_CREATE, and WM_PAINT versions both done together in this one project.
DrawText() doesn't work in WM_CREATE (and we have nothing in WM_PAIN). Maybe it is becase window gets a WM_PAINT, and DefWinwoProc
Paints an empty surface. Adding UpdateWindow() before drawing text didn't make a difference. However adding ShowWindow(), and 
UpdateWidow() both before drawing text in WM_CREATE did the trick.

# Windows OpenGl
## Fixed function pipeline

1. **Single Buffer**  
First OpenGL code written without glut. Uses single buffering. We learned how to convert normal device context to a context capable of rendering OpenGL content. With about 7 modifications this single buffering code can be converted to double buffering.
In the GIF notice that the code is run 2 time. first time Resize() is called from Initialize() and we see cropped triangle. Second time Resize() in Initialize() is commented out,
and we see a complete triangle. The program also goes to full screen on pressing f.
![FFP Single buffering](OpenGL/Windows/FixedFunctionPipeline/Output_ScreenCapture/3_SingleBuffer.gif)

1. **Double buffer**  
Everything same as single buffer, except that this program uses double buffer instead of single buffer to render on screen.
![FFP Double buffering](OpenGL/Windows/FixedFunctionPipeline/Output_ScreenCapture/4_doubleBuffer.gif)

1. FFP Ortho, Perpective

1. viewPortSwitch - switches view port when pressing numbers between [0-9]

1. Line_Rectangle - draw a rectangle using line primitive
	- 'w' - white color (default)
	- 'b' - blue color

1. Line_RectangleAndTriangle
 - Draw a rectangle, and a triangle using lines
 - move the 2 with arrow keys - L(move along -x axis), R(move along +x axis), U (zoom out), Down (zoom in)
 - Press 't' to select triangle, 'r' to select rectangle
 - eg. to zoom out rectangle - press 'r', then press Up arrow
![Line Rectangle And Triangle](OpenGL/Windows/FixedFunctionPipeline/Output_ScreenCapture/7_Line_RectangleAndTriangle.gif)

1. triangleOnGraph
![triangleOnGraph](OpenGL/Windows/FixedFunctionPipeline/Output_ScreenCapture/7_triangleOnGraph.png)

1. rectangleOnGraph
![rectangleOnGraph](OpenGL/Windows/FixedFunctionPipeline/Output_ScreenCapture/8_rectangleOnGraph.PNG)

1. Static India
![Static India](OpenGL/Windows/FixedFunctionPipeline/Output_ScreenCapture/20_INDIA_STATIC.jpeg)

1. 3D Pyramid.
3D drawing, use of depth Buffer are synonymous.
This is first 3D drawing. Sir started 3D today (yesterday, since it is post midnight).

1. Dynamic India
![Dynamic India](OpenGL/Windows/FixedFunctionPipeline/Output_ScreenCapture/21_INDIA_DYNAMIC.gif)

1. Primitive Shapes
Figures (0,0), (0,2), and (1,0) of the 6 figures in total have been drawn using polygon stipple.
It is quite tricky to get stipples right - Whether a particular pixel of a polygon
will be off or on (stippling) depends on position of the pixel within window, thus if the polygon is moved around we will
see it being rendered differently.
![Primitive Shapes](OpenGL/Windows/FixedFunctionPipeline/Output_ScreenCapture/30_primitiveShapes.PNG)

1. Data Structures Project
To mark ending of Fixed Function Pipeline, we were asked to make a project.
We had to choose a data structure, and make something which would help a viewer understand that data structure.
I chose Linked Lists. In the project I explain linked lists via a dialog between a mother, and her daughter.
Katie is a curious 4 year old child, who asks her mother about linked lists, after which the mother explains.
![Data Structures Linked Lists](OpenGL/Windows/FixedFunctionPipeline/99_DataStructureProject/screenCaptures/DataStructuresProject.gif)
[Link to Video](OpenGL/Windows/FixedFunctionPipeline/99_DataStructureProject/screenCaptures/DataStructuresProject.mp4)


# XWindows Assignments/CrossPlatform

After XWindows was taught in class, it was expected to convert all windows Fixed Function Pipeline (FFP) assignments to XWindows (We had only covered FFP till then).
Instead of doing so, I started creating cross platform code - so that same project could produce outputs for both Windows and Linux.
Also instead of overwriting existing windows projects, I created a cross platform copy of each.
These new projects are created using cmake. Visual studio does support cmake.
clang is used for compiling code on Linux, and not GCC. GCC desn't support Unicode identifiers, clang doesn.
Note that supporting unicode identifiers is different than supporting unicode strings. GCC does support latter.
Unicode identifiers support means that my function names, and variable names can have unicode characters.
GCC requres Universal Character Name, which is not a very natural way of using unicode identifiers.
Linux VM, Linux Machine, or even WSL can be used for compiling. Though WSL was having trouble running code.
Code doesn't work with X forwarding right now, hence use VNC.
See "OpenGL\crossPlatform\FixedFunctionPipeline\README.md" for instructions on making a windows FFP program corss platform.

# OpenGl ProgrammablePipeline
## Shaders
### Vertex shaders

1. {00000000-0000-0000-0000-000000000000}.vert - passtrough vertex shader
1. {1D19A7B9-B345-491E-B7D7-E41FDFF697AA}.vert - applies transformation to every vertex according to u_mvp_matrix model view projecttion matrix
### Fragment shaders

1. {00000000-0000-0000-0000-000000000000}.frag - passtrough fragment shader
1. {C2038141-65DA-4D11-8351-304132D36416}.frag - gives all fragments white color

### Shaders' combinations used for various projects

|Sno. | Vertex Shader | Fragment Shader | Program |
|---  | ---			  |---			    |---      |
|1. |{00000000-0000-0000-0000-000000000000}.vert|{00000000-0000-0000-0000-000000000000}.frag|01_BlueScreenWithNullShaders_PP|
|2. |{1D19A7B9-B345-491E-B7D7-E41FDFF697AA}.vert|{C2038141-65DA-4D11-8351-304132D36416}.frag|02_OrthoGraphicTriangle_PP|
|3. |{1D19A7B9-B345-491E-B7D7-E41FDFF697AA}.vert|{C2038141-65DA-4D11-8351-304132D36416}.frag|03_PerspectiveTriangle_PP|
|4. |{1D19A7B9-B345-491E-B7D7-E41FDFF697AA}.vert|{C2038141-65DA-4D11-8351-304132D36416}.frag|04_2DShapesStatic_PP|
|5. |{F0B737D1-E415-4222-B97F-A058F8ECBF80}.vert|{FAC70A64-D9ED-46F7-9EC6-534E9642FB2A}.frag|05_2DShapesColoredAnimated_PP|
|6. |{F0B737D1-E415-4222-B97F-A058F8ECBF80}.vert|{FAC70A64-D9ED-46F7-9EC6-534E9642FB2A}.frag|06_3DAnimationOfCubeAndPyramid_PP|
|7. |{0F1CD938-B032-421B-A697-D4FEBA0E3A28}.vert|{D82A0051-D193-4EBC-A272-7B569539DC36}.frag|18_LightCubeWithSingleWhiteDiffusedLight_PP|
|8. |{0F1CD938-B032-421B-A697-D4FEBA0E3A28}.vert|{D82A0051-D193-4EBC-A272-7B569539DC36}.frag|17_LightSpereWithSingleWhiteLight_PP|

# Util library

During the course I will move several reusable pieces of code to this project.

1. class OglWindow : This creates a skeleton to create OGL window. Initialization, etc. is handled automatically. An application which wants
to create OpenGL window can create a class which inherits from this class, and then override the virtual methods. Things like full screen, etc. are automatically
handled. One of the most frequently used overridden method will be Display(). Write all your drawing code here.
1. class DrawShapes : functions to draw basic shapes - circle, etc.
1. class Alphabet, class TextHelper : Drawing text. An object of class Alphabet represents a unicode character. TextHelper builds on top of Alphabet to provide additional capabilities line writing strings
    1. class Alphabet - Each character occupies a cell. usage - Alphabet alpha('Λ', top /*top to bottom gradient colors*/, m_bottom); alpha.Draw()/*Draws the unicode alphabet*/;
  Hebrew letters represent special hardcoded drawings that have to be done in the font cell.
        1. SetFontHeight() - sets font cell size by using given height and m_monospaceRatio.
    1. class textHelper
        1. GetOnScreenSize() - get the size (in normalized device coordinates) that the given string will occupy on screen.
        1. GetMaxHeightCellSize() - returns the cell size for the cell which has largest height out of the cells corresponding to all alphabets of the string

# Animation library

# Freetype Wrapper

A convenient wrapper over freetype library. Created by following Nehe tutorial.
Unlike the tutoril however, the wrapper understands NDC coordinates, and font, sizes, etc. can be specified
in NDC, and not in pixels.
Monospace fonts are rendering find. Proportional fonts are not. Font file must be present in
current working directory of the program.
```C++
//Sample code -
util::FreetypeWrapper ft("RobotoMono-Regular.ttf", 0.3, RGBA(255,255,0,255), this);
ft.printft((float)-2.0, (float)-1.0, (float)1.0, (float)5.0, "SsAaghil sangh", 2,"hegllogo");
```
The strings can be interspersed with arbitrary commands which can change rendering of the text following.
eg. to print `hil` in red -
```C++
ft.printft((float)-2.0, (float)-1.0, (float)1.0, (float)5.0, "SsAag[COLORREF:RGBA(255,0,0,255)]hil[COLORREF:RGBA(255,255,255,255)] sangh", 2,"hegllogo");
```
'[' and ']' are used as command markers. To print them use eascapes - '\'
eg.
```C++
R"(\\\[\])"
```
will print `\\[]`

# BOOKS
# OpenGL Programming Guide 3edition (RedBook)
## Chapter 10
1. code 10.1

    * Things to learn
        * display lists
        * material, light properties

# known issues
1. void DrawShapes::DrawCircleArcTriangleFan(double RADIUS, double startAngle, double endAngle, COLORREF color, UINT NUM_PTS) doesn't work when angles are negative
1. Clang address sanitizers are failing. To prevent abort on encountering an error user ASAN_OPTIONS=halt_on_error=0. Eg. ASAN_OPTIONS=halt_on_error=0 ./a.out

# Log - Class assignments and projects

## Windows + OpenGL FFP (Fixed Function Pipeline)

1. 16 Jan 2019 - Centre Window on Desktop
1. 17 Jan 2019 - TextColor - an exercise in Win32 text output
1. 26 Jan 2019 - Fixed function pipeline (FFP) single buffer
1. 26 Jan 2019 - Fixed function pipeline double buffer
1. 10 Feb 2019 - FFP Ortho, Perpective
1. 16 Feb 2019 - viewPortSwitch, primitive point, primitive line horizontal, primitive line vertical, primitive horizontal lines (20 on each side of axis),  primitive vertical lines (20 on each side of axis), graph paper
1. 27 feb 2019 - Line_Rectangle, Line_rectangleAndTriangle, triangleOnGraph, rectangleOnGraph
1. 09 Mar 2019 - circle with points, circle with lines, kundali, concentric circles, concentric rectangles, concentric triangles, Incircle of a triangle, draw all geometric shapes studied so far, Deathly Hallows triangle, Rotate traingle using matrix operations (manipulations), Deathly Hallows animate
1. 17 Mar 2019 - Static India
1. 24 Mar 2019 - 3D Pyramid
1. 27 Mar 2019 - Dynamic India
1. 30 Mar 2019 - 3D Cube, 3D Pyramid and Cube combined (Vanilla OpenGL), 3D Pyramid and Cube combined (GLUT)
1. 07 APR 2019 - Solar system glut (glPushMatrix/glPopMatrix exercise)
1. 27 APR 2019 - Solar System Native, Solar System Native with moon, robotic arm, Primitive Shapes
1. 11 May 2019 - Textured Pyramid, Textured Cube, Textured Pyramid Cube, Texture Smiley
1. 12 May 2019 - Light Cube, Texture Checkerboard
1. 13 Jun 2019 - Finished Data Structures project (had been on it for more than 2 weeks - on and off)

## Windows + OpenGL PP (Programmable Pipeline)

1. 13 Jun 2019 - First OGL PP window.

## XWindows + OpenGL FFP/Cross Platform

1. 20 Jun, 2019 - black Xwindow - XWindows Pure
1. 21 Jun, 2019 - hello world - XWindows Pure
1. 03 Aug, 2019 - (10_kundali, 20_Static_India, 21_INDIA_DYNAMIC) /*10_kundali - took several weeks of effort - first cross platform code - compiles for Linux, and Windows, 20_Static_India - took just few hours of effort*/

## OpenGL Programmable Pipeline (PP) - Cross Platform

1. 04 Aug, 2019 - 01_BlueScreenWithNullShaders_PP
1. 25 Aug, 2019 - 02_OrthoGraphicTriangle_PP

## Android

### Without OpenGL

1. 27 Jun, 2019 - Hello world using IDE
2. 28 Jun, 2019 - Hello world using commandline (no text was actually displayed) - 02_helloWordlByCmndline
3. 29 Jun, 2019 - 03_HelloWorld - Hellow world, creating our own view in code
4. 30 Jun, 2019 - 04_WindowWithEvents. Also did Refactor - deleted previous 3 projects. Recreates a new template from IDE. Use androidx:* artifacts was checked this time I created project in Android Studio. Made the changes to template as suggested by sir. This was done so that my projects use androidx.* namespace for packages, and not android.support.v7.*. Refractoring from IDE didn't work (Refactor->Migrate to androidx), so recreated from scratch.
5. 08 Jul, 2019 - 05_WindowWithEventsWithExit, 06_WindowWithEventsWithLogcat, 07_WindowWithViewAsObject

### OpenGL programmable pipeline

1. 17 Aug 2019  - 01_AndroidBlueScreen_PP
1. 25 Aug 2019  - 02_AndroidOrthoTriangle_PP

## ProgrammablePipeline: CrossPlatform (Windows + Linux), Android, WebGL

Since the list of assignments which have to be done on all platforms is the same, hence creating a common log

|Sno. | Assignment                                    | Windows                                                                                                                                               | Linux                                                                                                                                             | Android                                                                                                                                               |Webgl                                                                                                                              |macOS                                                                                                                              | Remarks                                                                                                                                                                                                       | Date                                                                                                    |
|---  | ---                                           |---                                                                                                                                                    |---                                                                                                                                                |---                                                                                                                                                    |---                                                                                                                                |---                                                                                                                                |---                                                                                                                                                                                                            |---                                                                                                      |
|1.   |01_*_BlueScreen                                |                                                                                                                                                       |                                                                                                                                                   |                                                                                                                                                       |![Blue screen WebGL](OpenGL/WebGL/01_WebGL_BlueScreen/screenshot.jpg)                                                              |![Blue screen macOS](OpenGL/macOS/01_macOS_BlueScreen/screenshot.jpg)                                                              |                                                                                                                                                                                                               |webgl - 15 Nov 2019. macOS - 19 Dec 2019                                                                 |
|2.   |02_*_OrthoTriangle                             |![OrthoTriangle Windows](OpenGL/crossPlatform/ProgrammablePipeline/02_OrthoGraphicTriangle_PP/Windows.png)                                             |![OrthoTriangle Linux](OpenGL/crossPlatform/ProgrammablePipeline/02_OrthoGraphicTriangle_PP/Linux.jpeg)                                            |![OrthoTriangle Android](OpenGL/crossPlatform/ProgrammablePipeline/02_OrthoGraphicTriangle_PP/Android.jpeg)                                            |![OrthoTriangle WebGL](OpenGL/WebGL/02_WebGL_OrthoTriangle/screenshot.jpg)                                                         |![OrthoTriangle macOS](OpenGL/macOS/02_macOS_OrthoTriangle/screenshot.jpg)                                                         |                                                                                                                                                                                                               |webgl - 15 Nov 2019. macOS - 19 Dec 2019                                                                 |
|3.   |03_*_PerspectiveTriangle                       |![PerspectiveTriangle Windows](OpenGL/crossPlatform/ProgrammablePipeline/03_PerspectiveTriangle_PP/Windows.jpg)                                        |![PerspectiveTriangle Linux](OpenGL/crossPlatform/ProgrammablePipeline/03_PerspectiveTriangle_PP/Linux.jpg)                                        |![PerspectiveTriangle Android](OpenGL/crossPlatform/ProgrammablePipeline/03_PerspectiveTriangle_PP/Android.jpeg)                                       |![PerspectiveTriangle WebGL](OpenGL/WebGL/03_WebGL_PerspectiveTriangle/screenshot.jpg)                                             |                                                                                                                                   |                                                                                                                                                                                                               |webgl - 15 Nov 2019                                                                                      |
|4.   |04_*_2DShapesStatic_PP                         |![2 2D shapes Windows](OpenGL/crossPlatform/ProgrammablePipeline/04_2DShapesStatic_PP/Windows.jpg)                                                     |![2 2D shapes Linux](OpenGL/crossPlatform/ProgrammablePipeline/04_2DShapesStatic_PP/Linux.jpg)                                                     |![2 2D shapes Android](OpenGL/crossPlatform/ProgrammablePipeline/04_2DShapesStatic_PP/Android.jpg)                                                     |![2 2D shapes WebGL](OpenGL/WebGL/04_WebGL_2DShapesStatic_PP/screenshot.jpg)                                                       |                                                                                                                                   |Drawing two 2D shapes (static)                                                                                                                                                                                 |webgl - 15 Nov 2019                                                                                      |
|5.   |05_*_2DShapesColoredAnimated_PP                |![2 2D shapes color animated Windows](OpenGL/crossPlatform/ProgrammablePipeline/05_2DShapesColoredAnimated_PP/Windows.jpg)                             |![2 2D shapes color animated Linux](OpenGL/crossPlatform/ProgrammablePipeline/05_2DShapesColoredAnimated_PP/Linux.jpg)                             |![2 2D shapes color animated Android](OpenGL/crossPlatform/ProgrammablePipeline/05_2DShapesColoredAnimated_PP/Android.jpg)                             |![2 2D shapes color animated WebGL](OpenGL/WebGL/05_WebGL_2DShapesColoredAnimated_PP/screenshot.jpg)                               |                                                                                                                                   |Drawing two 2D shapes (colored, animated)                                                                                                                                                                      |WebGL - 24 Nov 2019                                                                                      |
|6.   |06_*_3DAnimationOfCubeAndPyramid_PP            |![3D animation of cube and pyramid Windows](OpenGL/crossPlatform/ProgrammablePipeline/06_3DAnimationOfCubeAndPyramid_PP/Windows.gif)                   |![3D animation of cube and pyramid Linux](OpenGL/crossPlatform/ProgrammablePipeline/06_3DAnimationOfCubeAndPyramid_PP/Linux.gif)                   |![3D animation of cube and pyramid Android](OpenGL/crossPlatform/ProgrammablePipeline/06_3DAnimationOfCubeAndPyramid_PP/Android.gif)                   |![3D animation of cube and pyramid WebGL](OpenGL/WebGL/06_WebGL_3DAnimationOfCubeAndPyramid_PP/WebGL.gif)                          |                                                                                                                                   |                                                                                                                                                                                                               |WebGL - 24 Nov 2019                                                                                      |
|7.   |07_*_StaticSmiley_PP                           |                                                                                                                                                       |                                                                                                                                                   |                                                                                                                                                       |![Static Smiley WebGL](OpenGL/WebGL/07_WebGL_StaticSmiley_PP/WebGL.jpg)                                                            |                                                                                                                                   |Use RunServerHere.bat to get around CORS                                                                                                                                                                       |WebGL - 28 Nov 2019                                                                                      |
|8.   |08_*_TweakedSmiley_PP                          |                                                                                                                                                       |                                                                                                                                                   |                                                                                                                                                       |![Tweaked Smiley WebGL](OpenGL/WebGL/08_WebGL_TweakedSmiley_PP/WebGL.gif)                                                          |                                                                                                                                   |Use RunServerHere.bat to get around CORS                                                                                                                                                                       |WebGL - 28 Nov 2019                                                                                      |
|9.   |09_*_CheckerBoard_PP                           |                                                                                                                                                       |                                                                                                                                                   |                                                                                                                                                       |![Checkerboard WebGL](OpenGL/WebGL/09_WebGL_CheckerBoard_PP/WebGL.jpg)                                                             |                                                                                                                                   |Use RunServerHere.bat to get around CORS                                                                                                                                                                       |WebGL - 29 Nov 2019                                                                                      |
|10   |10_*_Texture_Cube_Pyramid                      |                                                                                                                                                       |                                                                                                                                                   |                                                                                                                                                       |![texture cube pyramid WebGL](OpenGL/WebGL/10_WebGL_Texture_Cube_Pyramid/WebGL.gif)                                                |                                                                                                                                   |Use RunServerHere.bat to get around CORS                                                                                                                                                                       |WebGL - 01 Dec 2019                                                                                      |
|11   |11_*_MultipleViewports                         |                                                                                                                                                       |                                                                                                                                                   |                                                                                                                                                       |![Multiview ports WebGL](OpenGL/WebGL/11_WebGL_MultipleViewports/WebGL.gif)                                                        |                                                                                                                                   |Use RunServerHere.bat to get around CORS                                                                                                                                                                       |WebGL - 01 Dec 2019                                                                                      |
|12   |12_*_Graph                                     |                                                                                                                                                       |                                                                                                                                                   |                                                                                                                                                       |![Graph WebGL](OpenGL/WebGL/12_WebGL_Graph/WebGLGraph.jpg)                                                                         |                                                                                                                                   |Use RunServerHere.bat to get around CORS                                                                                                                                                                       |WebGL - 02 Dec 2019                                                                                      |
|13   |13_*_GraphWithShapes                           |                                                                                                                                                       |                                                                                                                                                   |                                                                                                                                                       |![Graph with shapes WebGL](OpenGL/WebGL/13_WebGL_GraphWithShapes/WebGL.jpg)                                                        |                                                                                                                                   |Use RunServerHere.bat to get around CORS                                                                                                                                                                       |WebGL - 03 Dec 2019                                                                                      |
|14   |14_*_DeathlyHallows                            |                                                                                                                                                       |                                                                                                                                                   |                                                                                                                                                       |![Deathly hallows WebGL](OpenGL/WebGL/14_WebGL_DeathlyHallows/WebGL.gif)                                                           |                                                                                                                                   |Use RunServerHere.bat to get around CORS                                                                                                                                                                       |WebGL - 06 Dec 2019                                                                                      |
|15   |15_*_StaticIndia                               |                                                                                                                                                       |                                                                                                                                                   |                                                                                                                                                       |![Static India WebGL](OpenGL/WebGL/15_WebGL_StaticIndia/WebGL.jpg)                                                                 |                                                                                                                                   |Use RunServerHere.bat to get around CORS                                                                                                                                                                       |WebGL - 10 Dec 2019                                                                                      |
|16   |16_*_DynamicIndia                              |                                                                                                                                                       |                                                                                                                                                   |                                                                                                                                                       |![Dynamic India WebGL](OpenGL/WebGL/16_WebGL_DynamicIndia/WebGL.gif)                                                               |                                                                                                                                   |Use RunServerHere.bat to get around CORS                                                                                                                                                                       |WebGL - 12 Dec 2019                                                                                      |
|17.  |17_*_LightSpereWithSingleWhiteLight_PP         |![Sphere With Single White Diffused Light](OpenGL/crossPlatform/ProgrammablePipeline/17_LightSpereWithSingleWhiteLight_PP/Windows.gif)                 |![Sphere With Single White Diffused Light](OpenGL/crossPlatform/ProgrammablePipeline/17_LightSpereWithSingleWhiteLight_PP/Linux.gif)               |![Sphere With Single White Diffused Light](OpenGL/crossPlatform/ProgrammablePipeline/17_LightSpereWithSingleWhiteLight_PP/Android.gif)                 |                                                                                                                                   |                                                                                                                                   |Android's implementation is different. Android code is using sir's lib to get coordinates, Win/Linux code aren't. See readmes in RTR2018_Sphere_Files_For_Windows-Linux-Android_11.10.2019 folder              |20 Oct 2019                                                                                              |
|18.  |18_*_LightCubeWithSingleWhiteDiffusedLight_PP  |![Cube With Single White Diffused Light](OpenGL/crossPlatform/ProgrammablePipeline/18_LightCubeWithSingleWhiteDiffusedLight_PP/Windows.gif)            |![Cube With Single White Diffused Light](OpenGL/crossPlatform/ProgrammablePipeline/18_LightCubeWithSingleWhiteDiffusedLight_PP/Linux.gif)          |![Cube With Single White Diffused Light](OpenGL/crossPlatform/ProgrammablePipeline/18_LightCubeWithSingleWhiteDiffusedLight_PP/Android.gif)            |                                                                                                                                   |                                                                                                                                   |                                                                                                                                                                                                               |15 Oct 2019                                                                                              |

## Log - Mac assignments
1.21 Nov 2019 - Hello world

## Log - Remaining Assignments

Tracking them separately from the main list of assignments

1. 4 April 2019
    * Glut multi color triangle (Was already done - OpenGL\Windows\2_FreeGlutHelloWorld)
    * White Filled rectangle (RemainingAssignments\Windows\FFP\1_White_FilledRectangle)
    * White filled rectanlge and triangle (RemainingAssignments\Windows\FFP\2_WhiteFilledRectangleTriangle)
    * Device Context Begin Paint (RemainingAssignments\Windows\Basics\1_DeviceContext_BeginPaint)
    * Device Context getDC (RemainingAssignments\Windows\Basics\2_DeviceContextGetDC)

## Log - My projects

1.	Simulating Ocean Water Paper by Jerry Tessendorf (2001)
	1. Gerstner Wave 01
	1. Gerstner Wave 02
	1. Waves using FFP
1. Fluid Simulation - wrote a simeple fluid simulation code following youtube -  https://www.youtube.com/watch?v=alhpH6ECFvQ Terribly slow. 3 Implementations - The code in fluid.h is written by me, following Jos Stam's paper; Fluid3.h is simple conversion of processing code from the video. Both work. Fluid2.h was another attempt at writing fluid.h since I initially thought that fluid.h wasn't working, which later was discovered to be false. Fluid2.h hasn't been tested.
1. Shadow mapping - self learning project in WebGL for midterm project.
Idea - render scene from light's position. This will give a depth map - the contents of depth buffer.
Obtain the contents of depth buffer as a texture.
Use Orthographic projection when rendering screen from light's point of view.
In case of orthographic projection, the depth values in depth buffer are linear.
When rendering the scene second time, project each fragment to light space using light's projection, and view matrices. Check if this projected depth is less than
the value stored in depth texture. If it then render normal color, else set FragColor to black.
<br/>Tutorial followed - https://webglfundamentals.org/webgl/lessons/webgl-shadows.html
![WebGL shadow mapping](OpenGL/WebGL_SelfLearning/01_WebGL_ShadowMap/shadowMap.gif)

# TODO

1. Read doc
		1. glLoadIdentity, glMatrixMode, glViewport
1. Assignments remaining
	1. full screen via change display setting.
1. Online reading
	1.1 https://web.archive.org/web/20100601090300/http://windows-programming.suite101.com/article.cfm/win32_message_processing_primer

# Bookmarks

1. CS_OWNDC - when to use, and when not to - [stackoverflow](https://stackoverflow.com/questions/48663815/getdc-releasedc-cs-owndc-with-opengl-and-gdi). See Ekzuzy's comment (here)[https://stackoverflow.com/a/48670747/981766].

# Modern C++ Usage

1. Variadic templates - FreeType Wrapper
Using Variadic templates I created a type safe version of printf().

# Errors and solutions

1. Compiling SOIL library for Linux 
error - can't create obj/image_helper.o: No such file or directory
On Linux machine -
cd projects/makefile
mkdir obj
make
see my comment here - https://stackoverflow.com/q/18886598/981766

1. compiling freetype library Linux
On Linux machine -
mkdir build
cd build
ccmake ..
make


# Learnings

1. glBindAttribLocation() and glBindUniformLocation()
    
    Instead of using glBindAttribLocation() one can use "(location=0) in vec4 position;" in shader to specify the
    location of attribute directly (OpenGL/GLSL 3.3 onwards, GLES 3.0/GLSL 300 es onwards).
    https://stackoverflow.com/a/4638906/981766
    https://stackoverflow.com/a/24485141/981766
    It is possible to bind locations of uniform variables too (if so, this must be done before linking) using
    glBindUniformLocation()
    https://src.chromium.org/viewvc/chrome/trunk/src/gpu/GLES2/extensions/CHROMIUM/CHROMIUM_bind_uniform_location.txt
	In general all binding of locations are done before linking (whether before or after compiling, it doesn't matter).
	If we do not bind a location of a variable then we'll have to get it's location using glGetAttribLocation(),
	glGetUniformLocation(). Getting of locations should be done after linking, as locations assignment is done during 
	linking phase


#DOC
Things for which the online documentation, etc. were not clear.
##OpenGL
###Programmable Pipeline
1. glDrawArrays (GLenum mode, GLint first, GLsizei count) : count parameter specifies number of vertices to use for drawing primitive specified
by mode. Eg. if mode is GL_TRIANGLES, and we have to draw 2 triangles, then count will be 6.
1. glDrawElements() - give a set of points in a mech, then just tell the indices to be used to create each trianlge. Advantage is vertices common between triangles don't have to be repeated, hence this leads to memory saving, and faster data
transfer to the GPU. The disadvantage (AFAIK) is that since vertices are common, thus so are normals. Consider the following figure. To draw front face I would tell glDrawElements() to use
vertices 0,1,2, and 3. To draw the left face I would tell it to use vertices 0,1,5,4. The problem is that although vertices are common, the normals at each vertex need not be same for different primitives
sharing that vertex. One solution would be to average all the different normals for a vertex, and use it as the normal for the vertex (after normalization). This doesn't look so well in case of cubes, but may be it
will look better for more flatter surfaces - which do not have much divergence between different normals possible at a vertex. Maybe there is a way out, I just dont know it at the time.

/*
                        P4                     P7
                         ---------------------------         -
                       -/|                       -/|
                      /  |                      /  |
                    -/   |                    -/   |
                   /     |                   /     |
              P0 -/      |             P3  -/      |
                /--------|----------------/        |
                |        |                |        |
                |        |                |        |
                |        |                |        |
                |     P5 |                |     P6 |
                |        ---------------------------
                |     --/                 |      -/
                |     /                   |     /
                |   -/                    |   -/
                |  /                      |  /       -
             P1 |-/                    P2 |-/
                /-------------------------/
*/

1. glVertexAttribIPointer() vs glVertexAttribPointer() - note the missing "I". When vertex data is of ivec type, instead of vec type (ivec4, ivec3, etc. instead of vec4, vec3, etc.) use glVertexAttribIPointe().
That is for integral data use glVertexAttribIPointer(), for float data use glVertexAttribPointer(). For me the both were giving different results for an ivec4, and issue got resolved 
on using glVertexAttribIPointer().
eg. https://gitlab.com/rtr2018_shaders/projectworld/-/blob/053875587ccb407ba0aa142a9693a63e28cd6de0/ProjectWorld/TerrainMesh/AnimMesh.cpp
this version also copied at 99_AuxStuff/CodeBackup/AnimMesh.cpp
See comment - 
https://community.khronos.org/t/strange-problm-accessing-an-ivec4-in-vertex-shdr/63066/4?u=lihas
https://community.khronos.org/t/strange-problm-accessing-an-ivec4-in-vertex-shdr/63066/3
http://ogldev.atspace.co.uk/www/tutorial38/tutorial38.html

#TODO
1. implement OpenGlDebugCallBack() for xwindows