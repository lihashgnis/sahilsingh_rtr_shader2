#include <Windows.h>

LRESULT WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam);

int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR cmdLine, int iCmdShow)
{
    TCHAR AppName[] = TEXT("DeviceContextGetDC");
    WNDCLASSEX wc = {};
    wc.cbSize = sizeof(wc);
    wc.hInstance = hInstance;
    wc.lpszClassName = AppName;
    wc.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
    wc.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
    wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
    wc.lpfnWndProc = WndProc;
    wc.cbWndExtra = 0;
    wc.cbClsExtra = 0;

    if (!RegisterClassEx(&wc))
    {
        MessageBoxA(NULL, "Error registering the class", "Error", MB_OK);
    }
    auto hwnd = CreateWindow(AppName, AppName, WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, NULL, NULL, hInstance, NULL);
    ShowWindow(hwnd, iCmdShow);
    UpdateWindow(hwnd);
    MSG msg;
    while (GetMessage(&msg, NULL, 0, 0))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return msg.wParam;
}

LRESULT WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
    switch (iMsg)
    {
    case WM_KEYDOWN:
    {
        auto hDC = GetDC(hwnd);
        RECT rc;
        GetClientRect(hwnd, &rc);
        DrawText(hDC, TEXT("Hello World"), -1, &rc, DT_SINGLELINE | DT_VCENTER | DT_CENTER);
        ReleaseDC(hwnd, hDC);
    }
    break;
    default:
        break;
    }
    return DefWindowProc(hwnd, iMsg, wParam, lParam);
}