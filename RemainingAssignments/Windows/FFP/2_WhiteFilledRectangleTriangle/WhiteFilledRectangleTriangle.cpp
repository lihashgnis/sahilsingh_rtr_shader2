#include <Windows.h>
#include <cstdio>
#include <gl/GL.h>

#pragma comment (lib, "OpenGl32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600
#define WIN_X 100
#define WIN_Y 100

HDC gHdc = NULL;
HGLRC gHrc = NULL;
bool gbActiveWindow = false;
FILE* gLogFile = nullptr;
bool gbIsFullScreen = false;

//function prototypes
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam);
void ToggleFullscreeen(HWND hwnd);
void Resize(int width, int height);
void Uninitialize(HWND hwnd);
int Initialize(HWND hwnd);
void Display();

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
    TCHAR AppName[] = "OpenGlSingleBuffer";

    WNDCLASSEX wc = {};
    wc.cbSize = sizeof(wc);
    wc.cbClsExtra = 0;
    wc.cbWndExtra = 0;
    wc.lpfnWndProc = WndProc;
    wc.hInstance = hInstance;
    wc.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
    wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
    wc.lpszClassName = AppName;
    wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
    wc.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);

    if (!RegisterClassEx(&wc))
    {
        MessageBoxA(NULL, "RegisterClass failed", "Error", MB_OK);
        return -1;
    }

    if (fopen_s(&gLogFile, "log.txt", "w") != 0)
    {
        MessageBoxA(NULL, "logfile open failed", "Error", MB_OK);
        return -2;
    }

    HWND hwnd = CreateWindowEx(WS_EX_APPWINDOW, AppName, AppName, WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE, WIN_X, WIN_Y, WIN_WIDTH, WIN_HEIGHT, NULL, NULL, hInstance, NULL);

    int iRet = Initialize(hwnd);

    const char *str = nullptr;

    if (iRet < 0)
    {
        switch (iRet)
        {
        case -1:
            str = "choose pixel failed";
            break;
        case -2:
            str = "Set pixel format failed";
            break;
        case -3:
            str = "wglCreateContext failed";
            break;
        case -4:
            str = "wglMakeCurrent failed";
            break;
        default:
            break;
        }
    }
    else
    {
        str = "Initialize successful";
    }

    auto len = strlen(str);
    fwrite(str, sizeof(str[0]), len, gLogFile);

    ShowWindow(hwnd, iCmdShow);
    SetForegroundWindow(hwnd);
    SetFocus(hwnd);

    // Game loop

    bool bDone = false;
    MSG msg;

    while (bDone == false)
    {
        if (PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
        {
            if (msg.message == WM_QUIT)
                bDone = true;
            else
            {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
            }
        }
        else
        {
            if (gbActiveWindow == true)
            {
                Display();
            }
            else
            {

            }
        }
    }
}

void Resize(int width, int height)
{
    glViewport(0, 0, width, height);
}


int Initialize(HWND hwnd)
{
    PIXELFORMATDESCRIPTOR pfd;
    ZeroMemory((void*)&pfd, NULL, sizeof(PIXELFORMATDESCRIPTOR));
    pfd.nSize = sizeof(pfd);
    pfd.nVersion = 1;
    pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
    pfd.iPixelType = PFD_TYPE_RGBA;
    pfd.cColorBits = 32;
    pfd.cRedBits = 8;
    pfd.cGreenBits = 8;
    pfd.cBlueBits = 8;
    pfd.cAlphaBits = 8;

    gHdc = GetDC(hwnd);
    auto iPixelFormatIndex = ChoosePixelFormat(gHdc, &pfd);

    if (iPixelFormatIndex == 0)
    {
        return -1;
    }

    if (SetPixelFormat(gHdc, iPixelFormatIndex, &pfd) == false)
    {
        return -2;
    }

    gHrc = wglCreateContext(gHdc);//create rendering context

    if (gHrc == NULL)
    {
        return -3;
    }

    if (wglMakeCurrent(gHdc, gHrc) == false)//associate the rendering context to current thread
    {
        return -4;
    }

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    Resize(WIN_WIDTH, WIN_HEIGHT);
    return 0;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
    switch (iMsg)
    {
    case WM_SETFOCUS:
        gbActiveWindow = true;
        break;
    case WM_KILLFOCUS:
        gbActiveWindow = false;
        break;
    case WM_SIZE:
        Resize(LOWORD(lParam), HIWORD(lParam));
        break;
    case WM_ERASEBKGND://dont want this since defwindow proc will post WM_PAINT after processing this
        return 0;
    case WM_CLOSE:
        DestroyWindow(hwnd);
        break;
    case WM_CHAR:
        switch (wParam)
        {
        case 'f':
            //[[fallthrough]]
        case 'F':
        {
            ToggleFullscreeen(hwnd);
        }
        break;
        default:
            break;
        }
        break;
    case WM_DESTROY:
        Uninitialize(hwnd);
        PostQuitMessage(0);
        break;
    default:
        break;
    }

    return DefWindowProc(hwnd, iMsg, wParam, lParam);
}

void Display()
{
    glClear(GL_COLOR_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(-0.5, 0, 0);
    glBegin(GL_QUADS);
    glColor3f(1.0f, 1.0f, 1.0f);
    glVertex2f(-0.4f, 0.4f);
    glVertex2f(-0.4f, -0.4f);
    glVertex2f(0.4f, -0.4f);
    glVertex2f(0.4f, 0.4f);
    glEnd();

    glLoadIdentity();
    glTranslatef(0.5, 0, 0);

    glBegin(GL_TRIANGLES);
    glVertex2f(0, 0.4);
    glVertex2f(-0.4f, -0.4f);
    glVertex2f(0.4f, -0.4f);
    glEnd();
    SwapBuffers(gHdc);
}

void Uninitialize(HWND hwnd)
{
    if (gbIsFullScreen)
    {
        ToggleFullscreeen(hwnd);
    }

    if (wglGetCurrentContext() == gHrc)
    {
        wglMakeCurrent(NULL, NULL);
    }

    if (gHrc)
    {
        wglDeleteContext(gHrc);
        gHrc = NULL;
    }

    if (gHdc)
    {
        ReleaseDC(hwnd, gHdc);
        gHdc = NULL;
    }

    if (gLogFile != nullptr)
    {
        const char *str = "log file close";
        auto len = strlen(str);
        fwrite(str, sizeof(str[0]), len, gLogFile);
        fclose(gLogFile);
    }
}

void ToggleFullscreeen(HWND hwnd)
{
    static WINDOWPLACEMENT wpPrev;
    DWORD dwStyle = 0;

    if (gbIsFullScreen == false)
    {
        dwStyle = GetWindowLong(hwnd, GWL_STYLE);

        if (dwStyle & WS_OVERLAPPEDWINDOW)
        {
            MONITORINFO mi = {};
            mi.cbSize = sizeof(mi);

            if (GetWindowPlacement(hwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(hwnd, MONITORINFOF_PRIMARY), &mi))
            {
                SetWindowLong(hwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
                SetWindowPos(hwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left,
                    mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
                ShowCursor(FALSE);
                gbIsFullScreen = true;
            }
        }
    }
    else
    {
        SetWindowLong(hwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
        SetWindowPlacement(hwnd, &wpPrev);
        SetWindowPos(hwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
        ShowCursor(TRUE);
        gbIsFullScreen = FALSE;
    }
}