﻿#include "../99_AuxStuff/MyLibs/Utility/Util.hpp"
#include <gl\GL.h>
#include <gl\GLU.h>
#include <cmath>
#include <tuple>
#include <string>
#include "../99_AuxStuff/MyLibs/Utility/Animation.hpp"
#include "../99_AuxStuff/MyLibs/Utility/Airplane.h"

//STRICT to prevent explicit typecasting
#define STRICT

constexpr UINT WIN_WIDTH = 1920;
constexpr UINT WIN_HEIGHT = 1080;
constexpr UINT WIN_X = 100;
constexpr UINT WIN_Y = 100;
UINT NUM_PTS = 100;
constexpr double CHAR_SPACING = 0.2;
constexpr double WORD_SPACING = 0;
std::shared_ptr<util::Timeline> tl;

using namespace util;

class MyOglWindow : public util::OglWindow
{
    const double PI;
    // Inherited via OglWindow
    virtual const char * GetMessageMap() override
    {
        return nullptr;
    }

    virtual void Display() override
    {
        glClear(GL_COLOR_BUFFER_BIT);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        TextHelper::Alphabet f(L'F', util::ColorWheel::COLORS::INDIA_SAFRON, util::ColorWheel::COLORS::INDIA_GREEN, 255);
        util::AnimationObjectState s{};
        f.Draw(s);
        SwapBuffers(m_Hdc);
    }

    virtual void Resize(int width, int height) override
    {
        if (height == 0)
            height = 1;

        glViewport(0, 0, width, height);

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        if (width <= height)
        {
            auto factor = 1.0f*(((GLfloat)height) / ((GLfloat)width));
            m_minX = -1.0;
            m_maxX = 1.0;
            m_minY = -factor;
            m_maxY = factor;
            m_minZ = -1.0;
            m_maxZ = 1.0;
        }
        else
        {
            auto factor = 1.0f*(((GLfloat)width) / ((GLfloat)height));
            m_minX = -factor;
            m_maxX = factor;
            m_minY = -1.0;
            m_maxY = 1.0f;
            m_minZ = -1.0;
            m_maxZ = 1.0;
        }
        glOrtho(m_minX, m_maxX, m_minY, m_maxY, m_minZ, m_maxZ);
    }

    virtual LRESULT WndProcPre(HWND, UINT iMsg, WPARAM wParam, LPARAM lParam) override
    {
        switch (iMsg)
        {
        case WM_CHAR:
        {
            switch (wParam)
            {
            case 'f':
                //[[fallthrough]]
            case 'F':
                //return LRESULT(-1);
                break;
            default:
                break;
            }
        }
        break;
        }
        return LRESULT();
    }

    virtual LRESULT WndProcPost(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) override
    {
        auto that = (OglWindow*)GetProp(hwnd, PARENT_OBJECT);

        switch (iMsg)
        {
        case WM_KEYDOWN:
        {
            if (VK_ESCAPE == wParam)
            {
                DestroyWindow(hwnd);
            }
        }
        break;
        }
        return LRESULT();
    }

public:
    MyOglWindow(TCHAR * className, UINT x, UINT y, UINT width, UINT height, HINSTANCE hInstance) : PI(4 * atan(1)), OglWindow(className, x, y, width, height, hInstance)
    {
    }

    // Inherited via OglWindow
    virtual void Update() override
    {
    }
};

int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
    TCHAR AppName[] = TEXT("Test Coordinate System");
    MyOglWindow myOglWnd(AppName, WIN_X, WIN_Y, WIN_WIDTH, WIN_HEIGHT, hInstance);
    myOglWnd.InitAndShowWindow();
    glClearColor(0, 0, 0, 0);
    //myOglWnd.EnableAlpha(true);


    //myOglWnd.ToggleFullscreeen();
    myOglWnd.RunGameLoop();
    return 0;
}