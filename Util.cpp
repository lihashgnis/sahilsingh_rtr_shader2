#include "stdafx.h"
#include "Util.hpp"

inline void Utility::InitWndClassEx(WNDCLASSEX & wc, TCHAR * AppName, WndProc wp, HINSTANCE hInstance)
{
	wc = {};
	wc.cbSize = sizeof(wc);
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.lpfnWndProc = wp;
	wc.hInstance = hInstance;
	wc.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wc.lpszClassName = AppName;
	wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wc.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
}
