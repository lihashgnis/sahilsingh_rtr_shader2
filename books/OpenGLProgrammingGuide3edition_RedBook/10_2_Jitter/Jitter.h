#define PI_ 3.14159265358979323846

struct pt
{
    GLdouble x, y;
};


//Jitter values from table 10.5 RED BOOK
pt j8[8] = {
    {0.5625, 0.4375},
    {0.0625, 0.9375},
    {0.3125, 0.6875},
    {0.6875, 0.8125},
    {0.8125, 0.1875},
    {0.9375, 0.5625},
    {0.4375, 0.0625},
    {0.1875, 0.3125}
};

pt j2[2] = {
    {0.25, 0.75},
    {0.75, 0.25}
};

void accFrustum(
    GLdouble left, GLdouble right, GLdouble bottom, 
    GLdouble top, GLdouble zNear, GLdouble zFar,
    GLdouble pixdx,GLdouble pixdy, GLdouble eyedx, 
    GLdouble eyedy, GLdouble focus)
{
    GLdouble xwsize, ywsize;
    GLdouble dx, dy;
    GLint viewport[4];

    glGetIntegerv(GL_VIEWPORT, viewport);

    xwsize = right - left;
    ywsize = top - bottom;
    dx = -(pixdx*xwsize / (GLdouble)viewport[2] + eyedx * zNear / focus);
    dy = -(pixdy*ywsize / (GLdouble)viewport[3] + eyedy * zNear / focus);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glFrustum(left + dx, right + dx, bottom + dy, top + dy, zNear, zFar);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(-eyedx, -eyedy, 0.0);
}

void accPerspective(
    GLdouble fovy, GLdouble aspect,
    GLdouble zNear, GLdouble zFar, GLdouble pixdx,
    GLdouble pixdy, GLdouble eyedx, GLdouble eyedy,
    GLdouble focus
)
{
    GLdouble fov2, left, right, bottom, top;
    fov2 = ((fovy*PI_) / 180.0) / 2.0;
    top = zNear / (cos(fov2) / sin(fov2));
    bottom = -top;
    right = top * aspect;
    left = -right;

    accFrustum(left, right, bottom, top, zNear, zFar, pixdx, pixdy, eyedx, eyedy, focus);
}