#include <freeglut.h>
#include <stdlib.h>
#include <math.h>
#include "../10_2_Jitter/Jitter.h"

void init(void)
{
    GLfloat mat_ambient[] = {1.0, 1.0, 1.0, 1.0};
    GLfloat mat_specular[] = {1.0, 1.0, 1.0, 1.0};
    GLfloat light_position[] = {0.0, 0.0, 10.0, 1.0};
    GLfloat lm_ambient[] = {0.2, 0.2, 0.2, 1.0};

    glMaterialfv(GL_FRONT, GL_AMBIENT, mat_ambient);
    glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
    glMaterialf(GL_FRONT, GL_SHININESS, 50.0);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lm_ambient);

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_DEPTH_TEST);
    glShadeModel(GL_FLAT);

    glClearColor(0.0, 0.0, 0.0, 0.0);
    glClearAccum(0.0, 0.0, 0.0, 0.0);
}

void displayObjects()
{
    GLfloat torus_diffuse[] = { 0.7, 0.7,  0.0, 1.0 };
    GLfloat cube_diffuse[] =  { 0.0, 0.7,  0.7, 1.0 };
    GLfloat sphere_diffuse[] =  { 0.7, 0.0,  0.7, 1.0 };
    GLfloat octa_diffuse[] =  { 0.7, 0.4,  0.4, 1.0 };

    glPushMatrix();
        glTranslatef(0, 0, -5.0);
        glRotatef(30.0, 1.0, 0.0, 0.0);

        glPushMatrix();
            glTranslatef(-0.80, 0.35, 0.0);
            glRotatef(100, 1.0, 0.0, 0.0);
            glMaterialfv(GL_FRONT, GL_DIFFUSE, torus_diffuse);
            glutSolidTorus(0.275, 0.85, 16, 16);
        glPopMatrix();

        glPushMatrix();
            glTranslatef(-0.75, -0.50, 0.0);
            glRotatef(45.0, 0.0, 0.0, 1.0);
            glRotatef(45.0, 1.0, 0.0, 0.0);
            glMaterialfv(GL_FRONT, GL_DIFFUSE, cube_diffuse);
            glutSolidCube(1.5);
        glPopMatrix();

        glPushMatrix();
            glTranslatef(0.75, 0.60, 0);
            glRotatef(30.0, 1.0, 0, 0);
            glMaterialfv(GL_FRONT, GL_DIFFUSE, sphere_diffuse);
            glutSolidSphere(1.0, 16, 16);
        glPopMatrix();

        glPushMatrix();
            glTranslatef(0.70, -0.90, 0.25);
            glMaterialfv(GL_FRONT, GL_DIFFUSE, octa_diffuse);
            glutSolidOctahedron();
        glPopMatrix();
    glPopMatrix();
}

#define ACSIZE 8
#define jit j8

void display()
{
    GLint viewport[4];
    int jitter;

    glGetIntegerv(GL_VIEWPORT, viewport);
    glClear(GL_ACCUM_BUFFER_BIT);
    for (jitter = 0; jitter < ACSIZE; jitter++)
    {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        accPerspective(50.0, (GLdouble)viewport[2] / (GLdouble)viewport[3], 1.0, 15.0, jit[jitter].x, jit[jitter].y, 0.0, 0.0, 1.0);
        displayObjects();
        glAccum(GL_ACCUM, 1.0 / ACSIZE);
    }
    glAccum(GL_RETURN, 1.0);
    glFlush();
}

void reshape(int w, int h)
{
    glViewport(0, 0, (GLsizei)w, (GLsizei)h);
}

//Main Loop
//Be carefull to request accumulation buffer

int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB | GLUT_ACCUM | GLUT_DEPTH);
    glutInitWindowSize(250, 250);
    glutInitWindowPosition(100, 100);
    glutCreateWindow(argv[0]);
    init();
    glutReshapeFunc(reshape);
    glutDisplayFunc(display);
    glutMainLoop();
    return 0;
}