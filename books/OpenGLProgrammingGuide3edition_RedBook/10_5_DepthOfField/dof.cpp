#include <freeglut.h>
#include <stdlib.h>
#include <math.h>
#include "../10_2_Jitter/Jitter.h"

GLuint g_teapotList;

void init()
{
    GLfloat ambient[] = {0.0, 0.0, 0.0, 1.0};
    GLfloat diffuse[] = {1.0, 1.0, 1.0, 1.0};
    GLfloat specular[] = {1.0, 1.0, 1.0, 1.0};
    GLfloat position[] = {0.0, 3.0, 3.0, 0.0};

    GLfloat lmodel_ambient[] = { 0.2, 0.2, 0.2, 0.1 };
    GLfloat local_view[] = { 0.0 };

    glLightfv(GL_LIGHT0, GL_AMBIENT, ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse);
    glLightfv(GL_LIGHT0, GL_POSITION, position);

    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);
    glLightModelfv(GL_LIGHT_MODEL_LOCAL_VIEWER, local_view);

    glFrontFace(GL_CW);//by defaul it is CCW -> the windin order of specifying vertices of any primitive
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_AUTO_NORMAL);
    glEnable(GL_NORMALIZE);
    glEnable(GL_DEPTH_TEST);

    glClearColor(0.0, 0.0, 0.0, 0.0);
    glClearAccum(0.0, 0.0, 0.0, 0.0);

    /*Make teapot display list*/
    g_teapotList = glGenLists(1);
    glNewList(g_teapotList, GL_COMPILE);
    glutSolidTeapot(0.5);
    glEndList();
}

void RenderTeapot(GLfloat x, GLfloat y, GLfloat z,
    GLfloat ambr, GLfloat ambg, GLfloat ambb,
    GLfloat difr, GLfloat difg, GLfloat difb,
    GLfloat specr, GLfloat specg, GLfloat specb, GLfloat shine)
{
    GLfloat mat[4];
    glPushMatrix();
        glTranslatef(x, y, z);
        mat[0] = ambr;  mat[1] = ambg;  mat[2] = ambb;  mat[3] = 1.0;

        glMaterialfv(GL_FRONT, GL_AMBIENT, mat);
        mat[0] = ambr;  mat[1] = ambg;  mat[2] = ambb;  mat[3] = 1.0;

        glMaterialfv(GL_FRONT, GL_DIFFUSE, mat);
        mat[0] = specr;  mat[1] = specg;  mat[2] = specb;  mat[3] = 1.0;

        glMaterialfv(GL_FRONT, GL_SPECULAR, mat);
        glMaterialf(GL_FRONT, GL_SHININESS, shine*128.0);
        glCallList(g_teapotList);
    glPopMatrix();
}

void display()
{
    int jitter;
    GLint viewport[4];

    glGetIntegerv(GL_VIEWPORT, viewport);
    glClear(GL_ACCUM_BUFFER_BIT);

    for (jitter = 0; jitter < 8; jitter++)
    {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        accPerspective(45.0, (GLdouble)viewport[2] / (GLdouble)viewport[3], 1.0, 15.0, 0.0, 0.0, 0.33*j8[jitter].x, 0.33*j8[jitter].y, 5.0);

        /*ruby, gold, silver, emerald, and cyan teapots*/
        RenderTeapot(-1.1, -0.5, -4.5, 0.1745, 0.01175, 0.01175, 0.61424, 0.04136, 0.04136, 0.727811, 0.626959, 0.626959, 0.6);
        RenderTeapot(-0.5, -0.5, -0.5, 0.24725, 0.1995, 0.0745, 0.75164, 0.60648, 0.22648, 0.628281, 0.555802, 0.366065, 0.4);
        RenderTeapot(0.2, -0.5, -5.5, 0.19225, 0.19225, 0.19225, 0.50754, 0.50754, 0.50754, 0.508273, 0.508273, 0.508273, 0.4);
        RenderTeapot(1.0, -0.5, -6.0, 0.0215, 0.1745, 0.0215, 0.07568, 0.61424, 0.07568, 0.633, 0.727811, 0.633, 0.6);
        RenderTeapot(1.8, -0.5, -6.5, 0.0, 0.1, 0.06, 0.0, 0.50980392, 0.50980392, 0.50196078, 0.50196078, 0.50196078, 0.25);
        glAccum(GL_ACCUM, 0.125);
    }
    glAccum(GL_RETURN, 1.0);
    glFlush();
}

void Reshape(int w, int h)
{
    glViewport(0, 0, (GLsizei)w, (GLsizei)h);
}

/*
Main Loop
Be certain you request an accumulation buffer
*/

int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB | GLUT_ACCUM | GLUT_DEPTH);
    glutInitWindowSize(400, 400);
    glutInitWindowPosition(100, 100);
    glutCreateWindow(argv[0]);
    init();
    glutReshapeFunc(Reshape);
    glutDisplayFunc(display);
    glutMainLoop();
    return 0;
}