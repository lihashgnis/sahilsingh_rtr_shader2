#include <freeglut.h>
#include <stdlib.h>

GLubyte mipmapImage32[32][32][4];
GLubyte mipmapImage16[16][16][4];
GLubyte mipmapImage8[8][8][4];
GLubyte mipmapImage4[4][4][4];
GLubyte mipmapImage2[2][2][4];
GLubyte mipmapImage1[1][1][4];

static GLuint texName;

void makeImages(void)
{
    int i, j;

    for (int i = 0; i < 32; i++)
    {
        for (int j = 0; j < 32; j++)
        {
            mipmapImage32[i][j][0] = 255;
            mipmapImage32[i][j][1] = 255;
            mipmapImage32[i][j][2] = 000;
            mipmapImage32[i][j][3] = 255;
        }
    }

    for (int i = 0; i < 16; i++)
    {
        for (int j = 0; j < 16; j++)
        {
            mipmapImage16[i][j][0] = 255;
            mipmapImage16[i][j][1] = 0;
            mipmapImage16[i][j][2] = 255;
            mipmapImage16[i][j][3] = 255;
        }
    }

    for (int i = 0; i < 8; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            mipmapImage8[i][j][0] = 255;
            mipmapImage8[i][j][1] = 000;
            mipmapImage8[i][j][2] = 000;
            mipmapImage8[i][j][3] = 255;
        }
    }

    for (int i = 0; i < 4; i++)
    {
        for (int j = 0; j < 4; j++)
        {
            mipmapImage4[i][j][0] = 000;
            mipmapImage4[i][j][1] = 255;
            mipmapImage4[i][j][2] = 000;
            mipmapImage4[i][j][3] = 255;
        }
    }

    for (int i = 0; i < 2; i++)
    {
        for (int j = 0; j < 2; j++)
        {
            mipmapImage2[i][j][0] = 000;
            mipmapImage2[i][j][1] = 000;
            mipmapImage2[i][j][2] = 255;
            mipmapImage2[i][j][3] = 255;
        }
    }

    mipmapImage1[0][0][0] = 255;
    mipmapImage1[0][0][1] = 255;
    mipmapImage1[0][0][2] = 255;
    mipmapImage1[0][0][3] = 255;
}

void init(void)
{
    glEnable(GL_DEPTH_TEST);
    glShadeModel(GL_FLAT);

    glTranslatef(0, 0, -3.6);
    makeImages();
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

    glGenTextures(1, &texName);
    glBindTexture(GL_TEXTURE_2D, texName);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);

    //specify mipmaps
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 32, 32, 0, GL_RGBA, GL_UNSIGNED_BYTE, mipmapImage32);
    glTexImage2D(GL_TEXTURE_2D, 1, GL_RGBA, 16, 16, 0, GL_RGBA, GL_UNSIGNED_BYTE, mipmapImage16);
    glTexImage2D(GL_TEXTURE_2D, 2, GL_RGBA, 8, 8, 0, GL_RGBA, GL_UNSIGNED_BYTE, mipmapImage8);
    glTexImage2D(GL_TEXTURE_2D, 3, GL_RGBA, 4, 4, 0, GL_RGBA, GL_UNSIGNED_BYTE, mipmapImage4);
    glTexImage2D(GL_TEXTURE_2D, 4, GL_RGBA, 2, 2, 0, GL_RGBA, GL_UNSIGNED_BYTE, mipmapImage2);
    glTexImage2D(GL_TEXTURE_2D, 5, GL_RGBA, 1, 1, 0, GL_RGBA, GL_UNSIGNED_BYTE, mipmapImage1);

    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
    glEnable(GL_TEXTURE_2D);
}

float z = -10;
int mode = 0;
void display(void)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glBindTexture(GL_TEXTURE_2D, texName);
    
    if (mode == 0)
    {
        glBegin(GL_QUADS);
        glTexCoord2f(0, 0); glVertex3f(-2, -1, 0);
        glTexCoord2f(0, 1); glVertex3f(-2, 1, 0);
        glTexCoord2f(1, 1); glVertex3f(2000, 1, -6000);
        glTexCoord2f(1, 0); glVertex3f(2000, -1, -6000);
        glEnd();
    }
    else
    {
        glBegin(GL_QUADS);
        glTexCoord2f(0, 0); glVertex3f(0, 0, z);
        glTexCoord2f(0, 1); glVertex3f(0, 1, z);
        glTexCoord2f(1, 1); glVertex3f(1, 1, z);
        glTexCoord2f(1, 0); glVertex3f(1, 0, z);
        glEnd();
    }

    glFlush();
}

void reshape(int w, int h)
{
    glViewport(0, 0, (GLsizei)w, (GLsizei)h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60, (GLfloat)w / (GLfloat)h, 1.0, 3000);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void keyboard(unsigned char key, int x, int y)
{
    switch (key)
    {
    case 27:
    {
        exit(0);
    }
    break;
    case 'm':
        mode = mode ^ 1;
        if (mode == 1)
        {
            MessageBoxA(NULL, " Use w/s to move rectangle towards +z/-z \n notice that rectangle will change color as it moves towards/away from screen", "Mode 1", MB_OK);
        }
        glutPostRedisplay();
        break;
    case 'w':
        z += 1;
        glutPostRedisplay();
        break;
    case 's':
        z -= 1;
        glutPostRedisplay();
        break;
    default:
        break;
    }
}

int main(int argc, char** argv)
{
    MessageBoxA(NULL, " Use m to switch modes\n 0 - default mode (code as given in 9_5)\n 1 - move rectangle around mode", "Info", MB_OK);
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(500, 500);
    glutInitWindowPosition(50, 50);
    glutCreateWindow(argv[0]);
    init();
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutKeyboardFunc(keyboard);
    glutMainLoop();
    return 0;
}