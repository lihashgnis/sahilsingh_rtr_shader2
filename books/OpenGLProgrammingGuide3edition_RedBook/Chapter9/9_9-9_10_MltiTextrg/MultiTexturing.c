#include <freeglut.h>
#include <stdlib.h>
#include <stdio.h>
#include "resource.h"
#include <freeglut_ext.h>
#include <gl/GL.h>

BOOL LoadTexture(GLuint *texture, const TCHAR imageResourceID[]);

//Butterfly image from - https://www.therange.co.uk/_m4/8/4/1521708867_2345.jpg
//Checker from - https://cdn.wallpapersafari.com/22/31/h6EUP5.jpg
unsigned int butterfly, checker;

//https://www.gamedev.net/forums/topic/191794-gl_clamp_to_edge---non-existing/
#define GL_CLAMP_TO_EDGE 0x812F

//https://www.khronos.org/registry/OpenGL/api/GL/glext.h
#define GL_TEXTURE0_ARB                   0x84C0
#define GL_TEXTURE1_ARB                   0x84C1
typedef void (* PFNGLMULTITEXCOORD2FARBPROC) (GLenum target, GLfloat s, GLfloat t);
PFNGLMULTITEXCOORD2FARBPROC __myglMultiTexCoord2fARB;

//https://stackoverflow.com/a/6240085/981766
//https://linux.die.net/man/3/glactivetexturearb

typedef void (*glActiveTextureARB_type)(GLenum texture);
glActiveTextureARB_type __myglextActiveTextureARB;

void initGLextensions() {
    __myglextActiveTextureARB = (glActiveTextureARB_type)wglGetProcAddress("glActiveTextureARB");
    __myglMultiTexCoord2fARB = (PFNGLMULTITEXCOORD2FARBPROC)wglGetProcAddress("glMultiTexCoord2fARB");
}


void init(void)
{
    initGLextensions();

    glClearColor(0, 0, 0, 0);
    glShadeModel(GL_FLAT);
    glEnable(GL_DEPTH_TEST);

    HBITMAP hBitmap = NULL;
    BITMAP bmp1;
    BOOL bStatus = FALSE;
    hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL), IDB_BITMAP1, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);

    if (hBitmap != NULL)
    {
        GetObject(hBitmap, sizeof(bmp1), &bmp1);
    }

    BITMAP bmp2;

    hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL), IDB_BITMAP1, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);

    if (hBitmap != NULL)
    {
        GetObject(hBitmap, sizeof(bmp2), &bmp2);
    }

    GLuint texNames[2];
    glGenTextures(2, texNames);
    butterfly = texNames[0];
    checker = texNames[1];

    glBindTexture(GL_TEXTURE_2D, butterfly);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 32, 32, 0, GL_RGBA, GL_UNSIGNED_BYTE, bmp1.bmBits);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    glBindTexture(GL_TEXTURE_2D, checker);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 32, 32, 0, GL_RGBA, GL_UNSIGNED_BYTE, bmp2.bmBits);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    //Use the 2 texture objects to define 2 texture units for use in multitexturing

    __myglextActiveTextureARB(GL_TEXTURE0_ARB);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, butterfly);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
    glMatrixMode(GL_TEXTURE);
        glLoadIdentity();
        glTranslatef(0.5, 0.5, 0);
        glRotatef(45, 0, 0, 1);
        glTranslatef(-0.5, -0.5, 0);
    glMatrixMode(GL_MODELVIEW);
    
    __myglextActiveTextureARB(GL_TEXTURE1_ARB);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, checker);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

}

void display(void)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glBegin(GL_TRIANGLES);
        __myglMultiTexCoord2fARB(GL_TEXTURE0_ARB, 0, 0);
        __myglMultiTexCoord2fARB(GL_TEXTURE1_ARB, 1, 0);
        glVertex2f(0, 0);

        __myglMultiTexCoord2fARB(GL_TEXTURE0_ARB, 0.5, 1);
        __myglMultiTexCoord2fARB(GL_TEXTURE1_ARB, 0.5, 0);
        glVertex2f(50, 100);

        __myglMultiTexCoord2fARB(GL_TEXTURE0_ARB, 1, 0);
        __myglMultiTexCoord2fARB(GL_TEXTURE1_ARB, 1, 1);
        glVertex2f(100, 0);
    glEnd();

    glFlush();
}

void reshape(int w, int h)
{
    glViewport(0, 0, (GLsizei)w, (GLsizei)h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.0, ((GLfloat)w) / ((GLfloat)h), 1, 500);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(0, 0, -300);
}

void keyboard(unsigned char key, int x, int y)
{
    switch (key)
    {
    case 27:
        exit(0);
        break;
    default:
        break;
    }
}

int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(250, 250);
    glutInitWindowPosition(100, 100);
    glutCreateWindow(argv[0]);
    init();
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutKeyboardFunc(keyboard);
    glutMainLoop();
    return 0;
}
