# Install script for directory: C:/R

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "C:/RTR2018Batch_GokhaleSir_Pune/out/install/Windows-x64-Debug")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Debug")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("C:/RTR2018Batch_GokhaleSir_Pune/out/build/Windows-x64-Debug/99_AuxStuff/MyLibs/UtilityCrossPlatform/cmake_install.cmake")
  include("C:/RTR2018Batch_GokhaleSir_Pune/out/build/Windows-x64-Debug/OpenGL/crossPlatform/FixedFunctionPipeline/10_kundali/cmake_install.cmake")
  include("C:/RTR2018Batch_GokhaleSir_Pune/out/build/Windows-x64-Debug/OpenGL/crossPlatform/FixedFunctionPipeline/20_INDIA_STATIC/cmake_install.cmake")
  include("C:/RTR2018Batch_GokhaleSir_Pune/out/build/Windows-x64-Debug/OpenGL/crossPlatform/FixedFunctionPipeline/21_INDIA_DYNAMIC/cmake_install.cmake")
  include("C:/RTR2018Batch_GokhaleSir_Pune/out/build/Windows-x64-Debug/OpenGL/crossPlatform/ProgrammablePipeline/01_BlueScreenWithNullShaders_PP/cmake_install.cmake")
  include("C:/RTR2018Batch_GokhaleSir_Pune/out/build/Windows-x64-Debug/OpenGL/crossPlatform/ProgrammablePipeline/02_OrthoGraphicTriangle_PP/cmake_install.cmake")
  include("C:/RTR2018Batch_GokhaleSir_Pune/out/build/Windows-x64-Debug/OpenGL/crossPlatform/ProgrammablePipeline/03_PerspectiveTriangle_PP/cmake_install.cmake")
  include("C:/RTR2018Batch_GokhaleSir_Pune/out/build/Windows-x64-Debug/OpenGL/crossPlatform/ProgrammablePipeline/04_2DShapesStatic_PP/cmake_install.cmake")
  include("C:/RTR2018Batch_GokhaleSir_Pune/out/build/Windows-x64-Debug/OpenGL/crossPlatform/ProgrammablePipeline/05_2DShapesColoredAnimated_PP/cmake_install.cmake")
  include("C:/RTR2018Batch_GokhaleSir_Pune/out/build/Windows-x64-Debug/OpenGL/crossPlatform/ProgrammablePipeline/06_3DAnimationOfCubeAndPyramid_PP/cmake_install.cmake")
  include("C:/RTR2018Batch_GokhaleSir_Pune/out/build/Windows-x64-Debug/OpenGL/crossPlatform/ProgrammablePipeline/07_TextureStaticSmiley_PP/cmake_install.cmake")
  include("C:/RTR2018Batch_GokhaleSir_Pune/out/build/Windows-x64-Debug/OpenGL/crossPlatform/ProgrammablePipeline/18_LightCubeWithSingleWhiteDiffusedLight_PP/cmake_install.cmake")
  include("C:/RTR2018Batch_GokhaleSir_Pune/out/build/Windows-x64-Debug/OpenGL/crossPlatform/ProgrammablePipeline/17_LightSpereWithSingleWhiteLight_PP/cmake_install.cmake")
  include("C:/RTR2018Batch_GokhaleSir_Pune/out/build/Windows-x64-Debug/OpenGL/crossPlatform/ProgrammablePipeline/19_LightCubeWithSingleWhiteDiffusedLight_PP/cmake_install.cmake")
  include("C:/RTR2018Batch_GokhaleSir_Pune/out/build/Windows-x64-Debug/OpenGL/crossPlatform/ProgrammablePipeline/17_1_LightSpereWithADSLight_PP/cmake_install.cmake")
  include("C:/RTR2018Batch_GokhaleSir_Pune/out/build/Windows-x64-Debug/OpenGL/crossPlatform/ProgrammablePipeline/26_ADS_LightCube_Interleaved_Buffer_PP/cmake_install.cmake")
  include("C:/RTR2018Batch_GokhaleSir_Pune/out/build/Windows-x64-Debug/OpenGL/WebGL/00_WebGL_HelloWorld_01_Canvas/cmake_install.cmake")
  include("C:/RTR2018Batch_GokhaleSir_Pune/out/build/Windows-x64-Debug/OpenGL/WebGL/00_WebGL_HelloWorld_02_Events/cmake_install.cmake")
  include("C:/RTR2018Batch_GokhaleSir_Pune/out/build/Windows-x64-Debug/OpenGL/WebGL/00_WebGL_HelloWorld_03_FullScreen/cmake_install.cmake")
  include("C:/RTR2018Batch_GokhaleSir_Pune/out/build/Windows-x64-Debug/OpenGL/WebGL/01_WebGL_BlueScreen/cmake_install.cmake")
  include("C:/RTR2018Batch_GokhaleSir_Pune/out/build/Windows-x64-Debug/OpenGL/WebGL/02_WebGL_OrthoTriangle/cmake_install.cmake")
  include("C:/RTR2018Batch_GokhaleSir_Pune/out/build/Windows-x64-Debug/OpenGL/WebGL/03_WebGL_PerspectiveTriangle/cmake_install.cmake")
  include("C:/RTR2018Batch_GokhaleSir_Pune/out/build/Windows-x64-Debug/OpenGL/WebGL/04_WebGL_2DShapesStatic_PP/cmake_install.cmake")
  include("C:/RTR2018Batch_GokhaleSir_Pune/out/build/Windows-x64-Debug/OpenGL/WebGL/05_WebGL_2DShapesColoredAnimated_PP/cmake_install.cmake")
  include("C:/RTR2018Batch_GokhaleSir_Pune/out/build/Windows-x64-Debug/OpenGL/WebGL/06_WebGL_3DAnimationOfCubeAndPyramid_PP/cmake_install.cmake")
  include("C:/RTR2018Batch_GokhaleSir_Pune/out/build/Windows-x64-Debug/OpenGL/WebGL/07_WebGL_StaticSmiley_PP/cmake_install.cmake")
  include("C:/RTR2018Batch_GokhaleSir_Pune/out/build/Windows-x64-Debug/OpenGL/WebGL/08_WebGL_TweakedSmiley_PP/cmake_install.cmake")
  include("C:/RTR2018Batch_GokhaleSir_Pune/out/build/Windows-x64-Debug/OpenGL/WebGL/09_WebGL_CheckerBoard_PP/cmake_install.cmake")
  include("C:/RTR2018Batch_GokhaleSir_Pune/out/build/Windows-x64-Debug/OpenGL/WebGL/10_WebGL_Texture_Cube_Pyramid/cmake_install.cmake")
  include("C:/RTR2018Batch_GokhaleSir_Pune/out/build/Windows-x64-Debug/OpenGL/WebGL/11_WebGL_MultipleViewports/cmake_install.cmake")
  include("C:/RTR2018Batch_GokhaleSir_Pune/out/build/Windows-x64-Debug/OpenGL/WebGL/12_WebGL_Graph/cmake_install.cmake")
  include("C:/RTR2018Batch_GokhaleSir_Pune/out/build/Windows-x64-Debug/OpenGL/WebGL/13_WebGL_GraphWithShapes/cmake_install.cmake")
  include("C:/RTR2018Batch_GokhaleSir_Pune/out/build/Windows-x64-Debug/OpenGL/WebGL/14_WebGL_DeathlyHallows/cmake_install.cmake")
  include("C:/RTR2018Batch_GokhaleSir_Pune/out/build/Windows-x64-Debug/OpenGL/WebGL/15_WebGL_StaticIndia/cmake_install.cmake")
  include("C:/RTR2018Batch_GokhaleSir_Pune/out/build/Windows-x64-Debug/OpenGL/WebGL/16_WebGL_DynamicIndia/cmake_install.cmake")
  include("C:/RTR2018Batch_GokhaleSir_Pune/out/build/Windows-x64-Debug/OpenGL/WebGL/17_WebGL_LightSpereWithSingleWhiteLight_PP/cmake_install.cmake")
  include("C:/RTR2018Batch_GokhaleSir_Pune/out/build/Windows-x64-Debug/OpenGL/WebGL_SelfLearning/01_WebGL_ShadowMap/cmake_install.cmake")
  include("C:/RTR2018Batch_GokhaleSir_Pune/out/build/Windows-x64-Debug/DirectX/05_2DShapesColoredAnimated_DX/cmake_install.cmake")

endif()

if(CMAKE_INSTALL_COMPONENT)
  set(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
else()
  set(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
endif()

string(REPLACE ";" "\n" CMAKE_INSTALL_MANIFEST_CONTENT
       "${CMAKE_INSTALL_MANIFEST_FILES}")
file(WRITE "C:/RTR2018Batch_GokhaleSir_Pune/out/build/Windows-x64-Debug/${CMAKE_INSTALL_MANIFEST}"
     "${CMAKE_INSTALL_MANIFEST_CONTENT}")
